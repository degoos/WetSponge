Logging your plugin errors or messages
======================================

Sending and logging your plugin messages to the Server Console is as easy as
using `InternalLogger <https://degoos.com/javadocs/WetSponge/index.html?com/degoos/wetsponge/util/InternalLogger.html>`_.

Since WetSponge 0.14 WSPlugin comes with a Custom Logger, just use *YourPluginInstance.getLogger()*.

All of the sendType methods recognize a String or a WSText. We recommend using a String
so the colors would be the default ones, but if you want to change the Logger colours you can use a *WSText.builder()*.

.. toctree::
    :maxdepth: 4
    :caption: Contents:



Send 'Done'
-----------

'Done' messages will appear on `Green Color <https://degoos.com/javadocs/WetSponge/index.html?com/degoos/wetsponge/enums/EnumTextColor.html#GREEN>`_.
We'll use it to send a message telling the user that everything is correct.

.. code-block:: java

    // Sending a "Config loaded" done-log to the console.
    InternalLogger.sendDone("Configuration loaded without problems!");

Send 'Info'
-----------

'Info' messages will appear on `Aqua Color <https://degoos.com/javadocs/WetSponge/index.html?com/degoos/wetsponge/enums/EnumTextColor.html#AQUA>`_.
We'll use it to send a message telling the user some information he should know.

.. code-block:: java

    // Sending a "Default config, not changed" info-log to the console.
    InternalLogger.sendDone("You are using the default configuration, you should take a look if you want to customize something.");

Send 'Warning'
--------------

'Warning' messages will appear on `Light Purple Color <https://degoos.com/javadocs/WetSponge/index.html?com/degoos/wetsponge/enums/EnumTextColor.html#LIGHT_PURPLE>`_.
We'll use it to send a message telling the user something he should fix as soon as possible.

.. code-block:: java

    // Sending a "Config not found" info-log to the console.
    InternalLogger.sendDone("Configuration not found, creating the default one.");

Send 'Error'
------------

'Error' messages will appear on `Red Color <https://degoos.com/javadocs/WetSponge/index.html?com/degoos/wetsponge/enums/EnumTextColor.html#RED>`_.
We'll use it to send a message telling the user something went wrong or is not working.

.. code-block:: java

    // Sending a "Config corrupted" info-log to the console.
    InternalLogger.sendDone("Something bad is happening to your config file, check it or create a new one.");

Send 'Exception'
----------------

WetSponge allows you to manage exceptions in a simple way. Call InternalLogger.printException(Exception, String) instead of Exception.printStackTrace()
to use it. Exceptions will also be uploaded to https://paste.degoos.com/ along with useful information.

.. code-block:: java

    try {
        //Your code.
    } catch (Exception ex) {
        InternalLogger.printException(ex, "An error has occurred while WetSponge was loading your code!");
    }

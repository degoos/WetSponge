Start coding your own WetSponge plugin
======================================

You can see some WetSponge plugins on our `Samples Git`_ but here you
have the basic knowledge you need to start using WetSponge

.. toctree::
    :maxdepth: 4
    :caption: Contents:



Main Class - Hello World Plugin
-------------------------------

.. code-block:: java

    public class HelloWorld extends WSPlugin {
        // This will be our main plugin class
        private static HelloWorld instance;

        @Override
        public void onEnable() {
            instance = this;

            // Show a text in console in yellow
            WetSponge.getServer().getConsole().sendMessage(WSText.builder("Hello, I'm a yellow text in your server console.").color(EnumTextColor.YELLOW).build());
            // Add our MainListener class to the WetSponge Event Manager
            WetSponge.getEventManager().registerListener(new MainListener(), this);
        }

        public static HelloWorld getInstance() {
            return instance;
        }
    }

    public class MainListener {
        // This will be our Listener class, here we will register all our listeners

        @WSListener
        public void onPlayerJoin(WSPlayerJoinEvent event) {
            // Send a "Hello world!" message to the joined player
            event.getPlayer().sendMessage("Hello world!");
        }
    }

Plugin.yml - Yes, it’s required too
-----------------------------------

+-------------+-----------+---------------+------------------------------------+
| Node        | Required? | Description   | Example                            |
+=============+===========+===============+====================================+
| main        | Yes       | Points to     | your.package.HelloWorld            |
|             |           | the class     |                                    |
|             |           | that extends  |                                    |
|             |           | WSPlugin      |                                    |
+-------------+-----------+---------------+------------------------------------+
| name        | Yes       | The name of   | HelloWorld                         |
|             |           | your plugin   |                                    |
+-------------+-----------+---------------+------------------------------------+
| description | Yes       | A human       | Send a                             |
|             |           | friendly      | "Hello                             |
|             |           | description   | world!"                            |
|             |           | of the        | message to                         |
|             |           | functionality | joining                            |
|             |           | your plugin   | targets                            |
|             |           | provides      |                                    |
|             |           |               |                                    |
+-------------+-----------+---------------+------------------------------------+
| version     | Yes       | The version   | 1.0                                |
|             |           | of this       |                                    |
|             |           | plugin.       |                                    |
+-------------+-----------+---------------+------------------------------------+
| authors     | No        | Allows you    | authors:                           |
|             |           | to list one   | [You,                              |
|             |           | or multiple   | YourFriend]                        |
|             |           | authors       |                                    |
+-------------+-----------+---------------+------------------------------------+
| website     | No        | The plugin's  | website: https://yoursite.com      |
|             |           | or author's   |                                    |
|             |           | website.      |                                    |
+-------------+-----------+---------------+------------------------------------+
| depend      | No        | A list of     | depend:                            |
|             |           | plugins that  | [Plugin1]                          |
|             |           | your plugin   |                                    |
|             |           | requires to   |                                    |
|             |           | load          |                                    |
+-------------+-----------+---------------+------------------------------------+
| softdepend  | No        | A list of     | softdepend:                        |
|             |           | plugins that  | [Plugin2,                          |
|             |           | are required  | Plugin3]                           |
|             |           | for your      |                                    |
|             |           | plugin to     |                                    |
|             |           | have full     |                                    |
|             |           | functionality |                                    |
|             |           |               |                                    |
+-------------+-----------+---------------+------------------------------------+

Example
~~~~~~~

.. code-block:: yaml

    main: your.package.HelloWorld
    name: HelloWorld
    description: Send a "Hello world!" message to joining targets
    version: '1.0'
    authors: [You]
    website: https://yoursite.com
    depend: []
    softdepend: []

Showcase of this information on the WetSponge Plugins List

.. figure:: https://pbs.twimg.com/media/DDaJgmRXUAECPfI.jpg
   :alt: /wetSponge plugin list

   /wetSponge plugin list

.. _Samples Git: https://gitlab.com/degoos/WS-Sample-Plugins/tree/master


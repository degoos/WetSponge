package com.degoos.wetsponge.packet.play.client;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumEntityAction;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.packet.Spigot13Packet;
import com.degoos.wetsponge.util.reflection.NMSUtils;

import java.lang.reflect.Field;
import java.util.Arrays;

public class Spigot13CPacketEntityAction extends Spigot13Packet implements WSCPacketEntityAction {

	private boolean changed;
	private int entityId, jumpBoost;
	private EnumEntityAction entityAction;

	public Spigot13CPacketEntityAction(int entityId, EnumEntityAction entityAction, int jumpBoost) throws IllegalAccessException, InstantiationException {
		super(NMSUtils.getNMSClass("PacketPlayInEntityAction").newInstance());
		this.entityId = entityId;
		this.entityAction = entityAction;
		this.jumpBoost = jumpBoost;
		update();
	}

	public Spigot13CPacketEntityAction(Object packet) {
		super(packet);
		refresh();
	}

	@Override
	public void update() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));
			fields[0].setInt(getHandler(), entityId);
			fields[2].setInt(getHandler(), jumpBoost);
			if (entityAction != null) fields[1].set(getHandler(), Enum.valueOf((Class<? extends Enum>) NMSUtils.getNMSClass("PacketPlayInEntityAction$EnumPlayerAction"),
				WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD) ? entityAction.name() : entityAction.getOldSpigotName()));
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void refresh() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));
			entityId = fields[0].getInt(getHandler());
			entityAction = EnumEntityAction.getById(((Enum) fields[1].get(getHandler())).ordinal()).orElse(null);
			jumpBoost = fields[2].getInt(getHandler());
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public boolean hasChanged() {
		return changed;
	}

	@Override
	public int getEntityId() {
		return entityId;
	}

	@Override
	public void setEntityId(int entityId) {
		this.entityId = entityId;
		changed = true;
	}

	@Override
	public EnumEntityAction getAction() {
		return entityAction;
	}

	@Override
	public void setAction(EnumEntityAction entityAction) {
		this.entityAction = entityAction;
		changed = true;
	}

	@Override
	public int getJumpBoost() {
		return jumpBoost;
	}

	@Override
	public void setJumpBoost(int jumpBoost) {
		this.jumpBoost = jumpBoost;
		changed = true;
	}
}

package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockTypeStructureBlockMode;
import com.degoos.wetsponge.material.block.Spigot13BlockType;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.StructureBlock;

import java.util.Objects;

public class Spigot13BlockTypeStructureBlock extends Spigot13BlockType implements WSBlockTypeStructureBlock {

	private EnumBlockTypeStructureBlockMode mode;

	public Spigot13BlockTypeStructureBlock(EnumBlockTypeStructureBlockMode mode) {
		super(255, "minecraft:structure_block", "minecraft:structure_block", 64);
		Validate.notNull(mode, "Mode cannot be null!");
		this.mode = mode;
	}

	@Override
	public EnumBlockTypeStructureBlockMode getMode() {
		return mode;
	}

	@Override
	public void setMode(EnumBlockTypeStructureBlockMode mode) {
		Validate.notNull(mode, "Mode cannot be null!");
		this.mode = mode;
	}

	@Override
	public Spigot13BlockTypeStructureBlock clone() {
		return new Spigot13BlockTypeStructureBlock(mode);
	}

	@Override
	public BlockData toBlockData() {
		BlockData blockData = super.toBlockData();
		if (blockData instanceof StructureBlock) {
			((StructureBlock) blockData).setMode(StructureBlock.Mode.valueOf(mode.name()));
		}
		return blockData;
	}

	@Override
	public Spigot13BlockTypeStructureBlock readBlockData(BlockData blockData) {
		super.readBlockData(blockData);
		if (blockData instanceof StructureBlock) {
			mode = EnumBlockTypeStructureBlockMode.valueOf(((StructureBlock) blockData).getMode().name());
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeStructureBlock that = (Spigot13BlockTypeStructureBlock) o;
		return mode == that.mode;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), mode);
	}
}

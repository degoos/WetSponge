package com.degoos.wetsponge.entity.projectile;

import com.degoos.wetsponge.util.InternalLogger;
import com.flowpowered.math.vector.Vector3d;
import org.bukkit.entity.Fireball;
import org.bukkit.util.Vector;

public class Spigot13Fireball extends Spigot13Projectile implements WSFireball {

	public Spigot13Fireball(Fireball entity) {
		super(entity);
	}

	@Override
	public Vector3d getDirection() {
		Vector vector = getHandled().getDirection();
		return new Vector3d(vector.getX(), vector.getY(), vector.getZ());
	}

	@Override
	public void setDirection(Vector3d direction) {
		getHandled().setDirection(new Vector(direction.getX(), direction.getY(), direction.getZ()));
	}

	@Override
	public int setExplosionRadius() {
		return (int) getHandled().getYield();
	}

	@Override
	public void setExplosionRadius(int explosionRadius) {
		getHandled().setYield(explosionRadius);
	}

	@Override
	public void detonate() {
		InternalLogger.sendWarning("Method WSFireball#detonate() is not supported.");
	}

	@Override
	public Fireball getHandled() {
		return (Fireball) super.getHandled();
	}
}

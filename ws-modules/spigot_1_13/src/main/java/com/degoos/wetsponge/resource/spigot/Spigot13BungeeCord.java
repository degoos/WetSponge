package com.degoos.wetsponge.resource.spigot;

import com.degoos.wetsponge.entity.living.player.Spigot13Player;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.parser.player.PlayerParser;
import com.degoos.wetsponge.resource.WSBungeeCord;
import com.degoos.wetsponge.text.WSText;
import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.messaging.PluginMessageListener;

import java.net.InetSocketAddress;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

public class Spigot13BungeeCord implements WSBungeeCord, PluginMessageListener {

	private List<Spigot13BungeeRequest> requests;
	private JavaPlugin javaPlugin;

	public Spigot13BungeeCord(JavaPlugin javaPlugin) {
		Bukkit.getServer().getMessenger().registerOutgoingPluginChannel(javaPlugin, "BungeeCord");
		Bukkit.getServer().getMessenger().registerIncomingPluginChannel(javaPlugin, "BungeeCord", this);
		requests = new ArrayList<>();
		this.javaPlugin = javaPlugin;
	}

	private WSPlayer getWSPlayer() throws IllegalStateException {
		try {
			return PlayerParser.getPlayer(Bukkit.getOnlinePlayers().iterator().next().getUniqueId()).orElse(null);
		} catch (NoSuchElementException ex) {
			throw new NoWSPlayerOnlineException();
		}
	}

	private Player getPlayer() throws IllegalStateException {
		try {
			return Bukkit.getOnlinePlayers().iterator().next();
		} catch (NoSuchElementException ex) {
			throw new NoWSPlayerOnlineException();
		}
	}

	@Override
	public void connectWSPlayer(WSPlayer player, String server) {
		ByteArrayDataOutput out = ByteStreams.newDataOutput();
		out.writeUTF("Connect");
		out.writeUTF(server);
		((Spigot13Player) player).getHandled().sendPluginMessage(javaPlugin, "BungeeCord", out.toByteArray());
	}

	@Override
	public void connectWSPlayer(String player, String server) {
		ByteArrayDataOutput out = ByteStreams.newDataOutput();
		out.writeUTF("ConnectOther");
		out.writeUTF(player);
		out.writeUTF(server);
		getPlayer().sendPluginMessage(javaPlugin, "BungeeCord", out.toByteArray());
	}

	@Override
	public CompletableFuture<InetSocketAddress> getWSPlayerIP(WSPlayer player) {

		ByteArrayDataOutput out = ByteStreams.newDataOutput();
		out.writeUTF("IP");
		((Spigot13Player) player).getHandled().sendPluginMessage(javaPlugin, "BungeeCord", out.toByteArray());
		CompletableFuture<InetSocketAddress> addressCompletableFuture = new CompletableFuture<>();

		Spigot13BungeeRequest<InetSocketAddress> request = new Spigot13BungeeRequest<>
				(((Spigot13Player) player).getHandled(), "IP", addressCompletableFuture);
		requests.add(request);
		return addressCompletableFuture;
	}

	@Override
	public CompletableFuture<Integer> getWSPlayerCount(String server) {
		ByteArrayDataOutput out = ByteStreams.newDataOutput();
		out.writeUTF("PlayerCount");
		out.writeUTF(server);
		Player player = getPlayer();
		player.sendPluginMessage(javaPlugin, "BungeeCord", out.toByteArray());
		CompletableFuture<Integer> completableFuture = new CompletableFuture<>();
		Spigot13BungeeRequest<Integer> request = new Spigot13BungeeRequest<>(player, "PlayerCount;" + server, completableFuture);
		requests.add(request);
		return completableFuture;
	}

	@Override
	public CompletableFuture<Integer> getGlobalWSPlayerCount() {
		return getWSPlayerCount("ALL");
	}

	@Override
	public CompletableFuture<List<String>> getOnlineWSPlayers(String server) {
		ByteArrayDataOutput out = ByteStreams.newDataOutput();
		out.writeUTF("PlayerList");
		out.writeUTF(server);
		Player player = getPlayer();
		player.sendPluginMessage(javaPlugin, "BungeeCord", out.toByteArray());
		CompletableFuture<List<String>> completableFuture = new CompletableFuture<>();
		Spigot13BungeeRequest<List<String>> request = new Spigot13BungeeRequest<>(player, "PlayerList;" + server, completableFuture);
		requests.add(request);
		return completableFuture;
	}

	@Override
	public CompletableFuture<List<String>> getAllOnlineWSPlayers() {
		return getOnlineWSPlayers("ALL");
	}

	@Override
	public CompletableFuture<List<String>> getServerList() {
		ByteArrayDataOutput out = ByteStreams.newDataOutput();
		out.writeUTF("GetServers");
		Player player = getPlayer();
		player.sendPluginMessage(javaPlugin, "BungeeCord", out.toByteArray());
		CompletableFuture<List<String>> completableFuture = new CompletableFuture<>();
		Spigot13BungeeRequest<List<String>> request = new Spigot13BungeeRequest<>(player, "GetServers", completableFuture);
		requests.add(request);
		return completableFuture;
	}

	@Override
	public void sendMessage(String player, WSText message) {
		ByteArrayDataOutput out = ByteStreams.newDataOutput();
		out.writeUTF("Message");
		out.writeUTF(player);
		out.writeUTF(message.toFormattingText());
		getPlayer().sendPluginMessage(javaPlugin, "BungeeCord", out.toByteArray());
	}

	@Override
	public CompletableFuture<String> getServerName() {
		ByteArrayDataOutput out = ByteStreams.newDataOutput();
		out.writeUTF("GetServer");
		Player player = getPlayer();
		player.sendPluginMessage(javaPlugin, "BungeeCord", out.toByteArray());
		CompletableFuture<String> completableFuture = new CompletableFuture<>();
		Spigot13BungeeRequest<String> request = new Spigot13BungeeRequest<>(player, "GetServer", completableFuture);
		requests.add(request);
		return completableFuture;
	}

	@Override
	public void sendServerPluginMessage(byte[] payload, String channel, String server) {
		ByteArrayDataOutput out = ByteStreams.newDataOutput();
		out.writeUTF("Forward");
		out.writeUTF(server);
		out.writeUTF(channel);
		getPlayer().sendPluginMessage(javaPlugin, "BungeeCord", out.toByteArray());
	}

	@Override
	public void sendGlobalPluginMessage(byte[] payload, String channel) {
		sendServerPluginMessage(payload, channel, "ALL");
	}

	@Override
	public void sendWSPlayerPluginMessage(byte[] payload, String channel, String player) {
		ByteArrayDataOutput out = ByteStreams.newDataOutput();
		out.writeUTF("ForwardToPlayer");
		out.writeUTF(player);
		out.writeUTF(channel);
		getPlayer().sendPluginMessage(javaPlugin, "BungeeCord", out.toByteArray());
	}

	@Override
	public CompletableFuture<UUID> getRealUUID(WSPlayer player) {
		ByteArrayDataOutput out = ByteStreams.newDataOutput();
		out.writeUTF("UUID");
		((Spigot13Player) player).getHandled().sendPluginMessage(javaPlugin, "BungeeCord", out.toByteArray());
		CompletableFuture<UUID> completableFuture = new CompletableFuture<>();
		Spigot13BungeeRequest<UUID> request = new Spigot13BungeeRequest<>(((Spigot13Player) player).getHandled(), "UUID", completableFuture);
		requests.add(request);
		return completableFuture;
	}

	@Override
	public CompletableFuture<UUID> getRealUUID(String player) {
		ByteArrayDataOutput out = ByteStreams.newDataOutput();
		out.writeUTF("UUIDOther");
		out.writeUTF(player);
		Player sendPlayer = getPlayer();
		sendPlayer.sendPluginMessage(javaPlugin, "BungeeCord", out.toByteArray());
		CompletableFuture<UUID> completableFuture = new CompletableFuture<>();
		Spigot13BungeeRequest<UUID> request = new Spigot13BungeeRequest<>(sendPlayer, "UUIDOther;" + player, completableFuture);
		requests.add(request);
		return completableFuture;
	}

	@Override
	public CompletableFuture<InetSocketAddress> getServerIP(String server) {
		ByteArrayDataOutput out = ByteStreams.newDataOutput();
		out.writeUTF("ServerIP");
		out.writeUTF(server);
		Player player = getPlayer();
		player.sendPluginMessage(javaPlugin, "BungeeCord", out.toByteArray());
		CompletableFuture<InetSocketAddress> completableFuture = new CompletableFuture<>();
		Spigot13BungeeRequest<InetSocketAddress> request = new Spigot13BungeeRequest<>(player, "ServerIP;" + server, completableFuture);
		requests.add(request);
		return completableFuture;
	}

	@Override
	public void kickWSPlayer(String player, WSText reason) {
		ByteArrayDataOutput out = ByteStreams.newDataOutput();
		out.writeUTF("KickPlayer");
		out.writeUTF(player);
		out.writeUTF(reason.toFormattingText());
		getPlayer().sendPluginMessage(javaPlugin, "BungeeCord", out.toByteArray());
	}


	@Override
	public void onPluginMessageReceived(String channel, Player player, byte[] message) {
		if (channel.equals("BungeeCord")) return;
		ByteArrayDataInput in = ByteStreams.newDataInput(message);

		String requestString = in.readUTF();

		List<Spigot13BungeeRequest> playerRequests = requests.stream().filter(request -> request.getPlayer().equals(player)).collect(Collectors.toList());

		if (requestString.equalsIgnoreCase("IP")) {
			String ip = in.readUTF();
			int port = in.readInt();
			playerRequests.stream().filter(request -> request.getRequest().equalsIgnoreCase("IP"))
					.collect(Collectors.toList()).forEach(request -> {
				request.getCompletableFuture().complete(new InetSocketAddress(ip, port));
				requests.remove(request);
			});
		} else if (requestString.equalsIgnoreCase("PlayerCount")) {
			String server = in.readUTF();
			int count = in.readInt();
			playerRequests.stream().filter(request -> request.getRequest().startsWith("PlayerCount") && request.getRequest().split(";")[1].equals(server))
					.forEach(request -> {
						request.getCompletableFuture().complete(count);
						requests.remove(request);
					});
		} else if (requestString.equalsIgnoreCase("PlayerList")) {
			String server = in.readUTF();
			List<String> list = Arrays.asList(in.readUTF().split(", "));
			playerRequests.stream().filter(request -> request.getRequest().startsWith("PlayerList") && request.getRequest().split(";")[1].equals(server))
					.forEach(request -> {
						request.getCompletableFuture().complete(list);
						requests.remove(request);
					});
		} else if (requestString.equalsIgnoreCase("GetServers")) {
			List<String> list = Arrays.asList(in.readUTF().split(", "));
			playerRequests.stream().filter(request -> request.getRequest().equalsIgnoreCase("GetServers"))
					.forEach(request -> {
						request.getCompletableFuture().complete(list);
						requests.remove(request);
					});
		} else if (requestString.equalsIgnoreCase("GetServer")) {
			String server = in.readUTF();
			playerRequests.stream().filter(request -> request.getRequest().equalsIgnoreCase("GetServer"))
					.forEach(request -> {
						request.getCompletableFuture().complete(server);
						requests.remove(request);
					});
		} else if (requestString.equalsIgnoreCase("UUID")) {
			UUID uuid = UUID.fromString(in.readUTF());
			playerRequests.stream().filter(request -> request.getRequest().equalsIgnoreCase("UUID"))
					.forEach(request -> {
						request.getCompletableFuture().complete(uuid);
						requests.remove(request);
					});
		} else if (requestString.equalsIgnoreCase("UUIDOther")) {
			String playerName = in.readUTF();
			UUID uuid = UUID.fromString(in.readUTF());
			playerRequests.stream().filter(request -> request.getRequest().startsWith("UUIDOther") && request.getRequest().split(";")[1].equals(playerName))
					.forEach(request -> {
						request.getCompletableFuture().complete(uuid);
						requests.remove(request);
					});
		} else if (requestString.equalsIgnoreCase("ServerIp")) {
			String serverName = in.readUTF();
			String ip = in.readUTF();
			int port = in.readUnsignedShort();
			playerRequests.stream().filter(request -> request.getRequest().startsWith("ServerIp") && request.getRequest().split(";")[1].equals(serverName))
					.forEach(request -> {
						request.getCompletableFuture().complete(new InetSocketAddress(ip, port));
						requests.remove(request);
					});
		}
	}
}

package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.Spigot13BlockTypeMultipleFacing;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.Tripwire;

import java.util.Objects;
import java.util.Set;

public class Spigot13BlockTypeTripwire extends Spigot13BlockTypeMultipleFacing implements WSBlockTypeTripwire {

	private boolean disarmed, attached, powered;

	public Spigot13BlockTypeTripwire(Set<EnumBlockFace> faces, Set<EnumBlockFace> allowedFaces, boolean disarmed, boolean attached, boolean powered) {
		super(132, "minecraft:tripwire", "minecraft:tripwire", 64, faces, allowedFaces);
		this.disarmed = disarmed;
		this.attached = attached;
		this.powered = powered;
	}

	@Override
	public boolean isDisarmed() {
		return disarmed;
	}

	@Override
	public void setDisarmed(boolean disarmed) {
		this.disarmed = disarmed;
	}

	@Override
	public boolean isAttached() {
		return attached;
	}

	@Override
	public void setAttached(boolean attached) {
		this.attached = attached;
	}

	@Override
	public boolean isPowered() {
		return powered;
	}

	@Override
	public void setPowered(boolean powered) {
		this.powered = powered;
	}

	@Override
	public Spigot13BlockTypeTripwire clone() {
		return new Spigot13BlockTypeTripwire(getFaces(), getAllowedFaces(), disarmed, attached, powered);
	}

	@Override
	public BlockData toBlockData() {
		BlockData blockData = super.toBlockData();
		if (blockData instanceof Tripwire) {
			((Tripwire) blockData).setDisarmed(disarmed);
			((Tripwire) blockData).setAttached(attached);
			((Tripwire) blockData).setPowered(powered);
		}
		return blockData;
	}

	@Override
	public Spigot13BlockTypeTripwire readBlockData(BlockData blockData) {
		super.readBlockData(blockData);
		if (blockData instanceof Tripwire) {
			disarmed = ((Tripwire) blockData).isDisarmed();
			attached = ((Tripwire) blockData).isAttached();
			powered = ((Tripwire) blockData).isPowered();
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeTripwire that = (Spigot13BlockTypeTripwire) o;
		return disarmed == that.disarmed &&
				attached == that.attached &&
				powered == that.powered;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), disarmed, attached, powered);
	}
}

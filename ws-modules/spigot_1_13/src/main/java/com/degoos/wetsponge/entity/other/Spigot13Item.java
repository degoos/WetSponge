package com.degoos.wetsponge.entity.other;

import com.degoos.wetsponge.entity.Spigot13Entity;
import com.degoos.wetsponge.item.Spigot13ItemStack;
import com.degoos.wetsponge.item.WSItemStack;
import org.bukkit.entity.Item;

public class Spigot13Item extends Spigot13Entity implements WSItem {


	public Spigot13Item(Item entity) {
		super(entity);
	}

	@Override
	public WSItemStack getItemStack() {
		return new Spigot13ItemStack(getHandled().getItemStack());
	}

	@Override
	public void setItemStack(WSItemStack itemStack) {
		getHandled().setItemStack(((Spigot13ItemStack) itemStack).getHandled());
	}

	@Override
	public int getPickupDelay() {
		return getHandled().getPickupDelay();
	}

	@Override
	public void setPickupDelay(int pickupDelay) {
		getHandled().setPickupDelay(pickupDelay);
	}

	@Override
	public Item getHandled() {
		return (Item) super.getHandled();
	}
}

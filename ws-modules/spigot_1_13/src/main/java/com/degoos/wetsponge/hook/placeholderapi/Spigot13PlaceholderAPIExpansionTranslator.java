package com.degoos.wetsponge.hook.placeholderapi;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.parser.player.PlayerParser;
import me.clip.placeholderapi.expansion.PlaceholderExpansion;
import org.bukkit.entity.Player;

public class Spigot13PlaceholderAPIExpansionTranslator extends PlaceholderExpansion {

	private WSPlaceholderAPIExpansion expansion;

	public Spigot13PlaceholderAPIExpansionTranslator(WSPlaceholderAPIExpansion expansion) {
		this.expansion = expansion;
	}

	@Override
	public String getIdentifier() {
		return expansion.getIdentifier();
	}

	@Override
	public String getPlugin() {
		return expansion.getPlugin();
	}

	@Override
	public String getAuthor() {
		return expansion.getAuthor();
	}

	@Override
	public String getVersion() {
		return expansion.getVersion();
	}

	@Override
	public String onPlaceholderRequest(Player player, String identifier) {
		return expansion.onPlaceholderRequest(PlayerParser.getOrCreatePlayer(player, player.getUniqueId()), identifier);
	}

	@Override
	public boolean canRegister() {
		return expansion.getPlugin() == null || WetSponge.getPluginManager().getPlugin(expansion.getPlugin()) != null;
	}
}

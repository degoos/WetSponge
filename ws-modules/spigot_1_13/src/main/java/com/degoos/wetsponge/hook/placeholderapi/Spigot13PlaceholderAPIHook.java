package com.degoos.wetsponge.hook.placeholderapi;

import com.degoos.wetsponge.entity.living.player.WSPlayer;
import me.clip.placeholderapi.PlaceholderHook;
import org.bukkit.entity.Player;

public class Spigot13PlaceholderAPIHook implements WSPlaceholderAPIHook {

	private PlaceholderHook hook;
	private String identifier;

	public Spigot13PlaceholderAPIHook(PlaceholderHook hook, String identifier) {
		this.hook = hook;
	}

	@Override
	public String getIdentifier() {
		return identifier;
	}

	@Override
	public String onPlaceholderRequest(WSPlayer player, String identifier) {
		return hook.onPlaceholderRequest((Player) player.getHandled(), identifier);
	}
}

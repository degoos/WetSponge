package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.packet.Spigot13Packet;
import com.degoos.wetsponge.util.reflection.NMSUtils;

import java.lang.reflect.Field;
import java.util.Arrays;

public class Spigot13SPacketCloseWindows extends Spigot13Packet implements WSSPacketCloseWindows {

    private int windowsId;
    private boolean changed;

    public Spigot13SPacketCloseWindows(int windowsId) throws IllegalAccessException, InstantiationException {
        super(NMSUtils.getNMSClass("PacketPlayOutCloseWindow").newInstance());
        this.windowsId = windowsId;
        update();
    }

    public Spigot13SPacketCloseWindows(Object packet) {
        super(packet);
        refresh();
    }

    @Override
    public void update() {
        try {
            Field[] fields = getHandler().getClass().getDeclaredFields();
            Arrays.stream(fields).forEach(field -> field.setAccessible(true));
            fields[0].setInt(getHandler(), windowsId);
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void refresh() {
        try {
            Field[] fields = getHandler().getClass().getDeclaredFields();
            Arrays.stream(fields).forEach(field -> field.setAccessible(true));
            windowsId = fields[0].getInt(getHandler());
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public boolean hasChanged() {
        return changed;
    }

    @Override
    public int getWindowsId() {
        return windowsId;
    }

    @Override
    public void setWindowsId(int windowsId) {
        this.windowsId = windowsId;
        changed = true;
    }
}

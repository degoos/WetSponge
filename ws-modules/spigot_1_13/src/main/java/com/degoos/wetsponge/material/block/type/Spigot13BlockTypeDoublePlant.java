package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockTypeBisectedHalf;
import com.degoos.wetsponge.enums.block.EnumBlockTypeDoublePlantType;
import com.degoos.wetsponge.material.block.Spigot13BlockTypeBisected;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.block.data.BlockData;

import java.util.Objects;

public class Spigot13BlockTypeDoublePlant extends Spigot13BlockTypeBisected implements WSBlockTypeDoublePlant {

	private EnumBlockTypeDoublePlantType doublePlantType;

	public Spigot13BlockTypeDoublePlant(EnumBlockTypeBisectedHalf half, EnumBlockTypeDoublePlantType doublePlantType) {
		super(175, "minecraft:double_plant", "minecraft:sunflower", 64, half);
		Validate.notNull(doublePlantType, "Double plant type cannot be null!");
		this.doublePlantType = doublePlantType;
	}

	@Override
	public String getNewStringId() {
		switch (doublePlantType) {
			case LILAC:
				return "minecraft:lilac";
			case DOUBLE_TALLGRASS:
				return "minecraft:tall_grass";
			case LARGE_FERN:
				return "minecraft:large_fern";
			case ROSE_BUSH:
				return "minecraft:rose_bush";
			case PEONY:
				return "minecraft:peony";
			case SUNFLOWER:
			default:
				return "minecraft:sunflower";
		}
	}

	@Override
	public EnumBlockTypeDoublePlantType getDoublePlantType() {
		return doublePlantType;
	}

	@Override
	public void setDoublePlantType(EnumBlockTypeDoublePlantType doublePlantType) {
		Validate.notNull(doublePlantType, "Double plant type cannot be null!");
		this.doublePlantType = doublePlantType;
	}

	@Override
	public Spigot13BlockTypeDoublePlant clone() {
		return new Spigot13BlockTypeDoublePlant(getHalf(), doublePlantType);
	}

	@Override
	public Spigot13BlockTypeDoublePlant readBlockData(BlockData blockData) {
		super.readBlockData(blockData);
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeDoublePlant that = (Spigot13BlockTypeDoublePlant) o;
		return doublePlantType == that.doublePlantType;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), doublePlantType);
	}
}

package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockTypeStoneType;
import com.degoos.wetsponge.material.block.Spigot13BlockType;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.block.data.BlockData;

import java.util.Objects;

public class Spigot13BlockTypeStone extends Spigot13BlockType implements WSBlockTypeStone {

	private EnumBlockTypeStoneType stoneType;

	public Spigot13BlockTypeStone(EnumBlockTypeStoneType stoneType) {
		super(1, "minecraft:stone", "minecraft:stone", 64);
		Validate.notNull(stoneType, "Stone type cannot be null!");
		this.stoneType = stoneType;
	}

	@Override
	public String getNewStringId() {
		switch (stoneType) {
			case DIORITE:
				return "minecraft:diorite";
			case GRANITE:
				return "minecraft:granite";
			case ANDESITE:
				return "minecraft:andesite";
			case SMOOTH_DIORITE:
				return "minecraft:polished_diorite";
			case SMOOTH_GRANITE:
				return "minecraft:polished_granite";
			case SMOOTH_ANDESITE:
				return "minecraft:polished_andesite";
			case STONE:
			default:
				return "minecraft:stone";
		}
	}

	@Override
	public EnumBlockTypeStoneType getStoneType() {
		return stoneType;
	}

	@Override
	public void setStoneType(EnumBlockTypeStoneType stoneType) {
		Validate.notNull(stoneType, "Stone type cannot be null!");
		this.stoneType = stoneType;
	}

	@Override
	public Spigot13BlockTypeStone clone() {
		return new Spigot13BlockTypeStone(stoneType);
	}

	@Override
	public Spigot13BlockTypeStone readBlockData(BlockData blockData) {
		super.readBlockData(blockData);
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeStone that = (Spigot13BlockTypeStone) o;
		return stoneType == that.stoneType;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), stoneType);
	}
}

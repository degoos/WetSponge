package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.packet.Spigot13Packet;
import com.degoos.wetsponge.util.Validate;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import com.degoos.wetsponge.util.reflection.Spigot13HandledUtils;
import com.flowpowered.math.vector.Vector3d;

import java.lang.reflect.Field;
import java.util.Arrays;

public class Spigot13SPacketSignEditorOpen extends Spigot13Packet implements WSSPacketSignEditorOpen {

    private Vector3d position;
    private boolean changed;

    public Spigot13SPacketSignEditorOpen(Vector3d position) throws IllegalAccessException, InstantiationException {
        super(NMSUtils.getNMSClass("PacketPlayOutOpenSignEditor").newInstance());
        Validate.notNull(position, "Position cannot be null!");
        this.position = position;
        update();
    }

    public Spigot13SPacketSignEditorOpen(Object packet) {
        super(packet);
        refresh();
    }

    @Override
    public void update() {
        try {
            Field[] fields = getHandler().getClass().getDeclaredFields();
            Arrays.stream(fields).forEach(field -> field.setAccessible(true));
            fields[0].set(getHandler(), Spigot13HandledUtils.getBlockPosition(position));
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void refresh() {
        try {
            Field[] fields = getHandler().getClass().getDeclaredFields();
            Arrays.stream(fields).forEach(field -> field.setAccessible(true));
            Object blockPos = fields[0].get(getHandler());
            position = new Vector3d((int) blockPos.getClass().getMethod("getX").invoke(blockPos),
                    (int) blockPos.getClass().getMethod("getY").invoke(blockPos),
                    (int) blockPos.getClass().getMethod("getZ").invoke(blockPos));
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public boolean hasChanged() {
        return changed;
    }


    @Override
    public Vector3d getPosition() {
        return position;
    }

    @Override
    public void setPosition(Vector3d position) {
        Validate.notNull(position, "Position cannot be null!");
        this.position = position;
        changed = true;
    }
}

package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockTypeDirtType;
import com.degoos.wetsponge.material.block.Spigot13BlockType;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.block.data.BlockData;

import java.util.Objects;

public class Spigot13BlockTypeDirt extends Spigot13BlockType implements WSBlockTypeDirt {

	private EnumBlockTypeDirtType dirtType;

	public Spigot13BlockTypeDirt(EnumBlockTypeDirtType dirtType) {
		super(3, "minecraft:dirt", "minecraft:dirt", 64);
		Validate.notNull(dirtType, "Dirt type cannot be null!");
		this.dirtType = dirtType;
	}

	@Override
	public String getNewStringId() {
		switch (dirtType) {
			case PODZOL:
				return "minecraft:podzol";
			case COARSE_DIRT:
				return "minecraft:coarse_dirt";
			case DIRT:
			default:
				return "minecraft:dirt";
		}
	}

	@Override
	public EnumBlockTypeDirtType getDirtType() {
		return dirtType;
	}

	@Override
	public void setDirtType(EnumBlockTypeDirtType dirtType) {
		Validate.notNull(dirtType, "Dirt type cannot be null!");
		this.dirtType = dirtType;
	}

	@Override
	public Spigot13BlockTypeDirt clone() {
		return new Spigot13BlockTypeDirt(dirtType);
	}

	@Override
	public Spigot13BlockTypeDirt readBlockData(BlockData blockData) {
		super.readBlockData(blockData);
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeDirt that = (Spigot13BlockTypeDirt) o;
		return dirtType == that.dirtType;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), dirtType);
	}
}

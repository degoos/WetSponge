package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.material.block.Spigot13BlockType;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.BrewingStand;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class Spigot13BlockTypeBrewingStand extends Spigot13BlockType implements WSBlockTypeBrewingStand {

	private Set<Integer> bottles;
	private int maximumBottles;

	public Spigot13BlockTypeBrewingStand(Set<Integer> bottles, int maximumBottles) {
		super(117, "minecraft:brewing_stand", "minecraft:brewing_stand", 64);
		this.bottles = bottles == null ? new HashSet<>() : bottles;
		this.maximumBottles = maximumBottles;
	}

	@Override
	public boolean hasBottle(int index) {
		return bottles.contains(index);
	}

	@Override
	public void setBottle(int index, boolean bottle) {
		if (bottle) bottles.add(index);
		else bottles.remove(index);
	}

	@Override
	public Set<Integer> getBottles() {
		return new HashSet<>(bottles);
	}

	@Override
	public int getMaximumBottles() {
		return maximumBottles;
	}

	@Override
	public Spigot13BlockTypeBrewingStand clone() {
		return new Spigot13BlockTypeBrewingStand(new HashSet<>(bottles), maximumBottles);
	}

	@Override
	public BlockData toBlockData() {
		BlockData blockData = super.toBlockData();
		if (blockData instanceof BrewingStand) {
			((BrewingStand) blockData).getBottles().forEach(target -> ((BrewingStand) blockData).setBottle(target, false));
			bottles.forEach(target -> ((BrewingStand) blockData).setBottle(target, true));
		}
		return blockData;
	}

	@Override
	public Spigot13BlockTypeBrewingStand readBlockData(BlockData blockData) {
		if (blockData instanceof BrewingStand) {
			bottles = ((BrewingStand) blockData).getBottles();
			maximumBottles = ((BrewingStand) blockData).getMaximumBottles();
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeBrewingStand that = (Spigot13BlockTypeBrewingStand) o;
		return maximumBottles == that.maximumBottles &&
				Objects.equals(bottles, that.bottles);
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), bottles, maximumBottles);
	}
}

package com.degoos.wetsponge.entity.living.ambient;


import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.entity.living.Spigot13LivingEntity;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.util.reflection.Spigot13EntityUtils;
import org.bukkit.entity.Ambient;

import java.util.Optional;

public class Spigot13Ambient extends Spigot13LivingEntity implements WSAmbient {

	public Spigot13Ambient(Ambient entity) {
		super(entity);
	}


	@Override
	public void setAI(boolean ai) {
		if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD))
			getHandled().setAI(ai);
		else Spigot13EntityUtils.setAI(getHandled(), ai);
	}


	@Override
	public boolean hasAI() {
		if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD))
			return getHandled().hasAI();
		else return Spigot13EntityUtils.hasAI(getHandled());
	}


	@Override
	public Optional<WSEntity> getTarget () {
		return null;
	}


	@Override
	public void setTarget (WSEntity entity) {
	}


	@Override
	public Ambient getHandled () {
		return (Ambient) super.getHandled();
	}
}

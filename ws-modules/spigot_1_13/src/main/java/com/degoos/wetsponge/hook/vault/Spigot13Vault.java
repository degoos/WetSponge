package com.degoos.wetsponge.hook.vault;

import com.degoos.wetsponge.enums.EnumServerType;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.plugin.RegisteredServiceProvider;

import java.util.Optional;

public class Spigot13Vault extends WSVault {

	private WSVaultEconomy economy;

	public Spigot13Vault() {
		super(EnumServerType.SPIGOT, EnumServerType.PAPER_SPIGOT);
		RegisteredServiceProvider<Economy> provider = Bukkit.getServer().getServicesManager().getRegistration(Economy.class);
		Economy rawEconomy = provider == null ? null : provider.getProvider();
		economy = rawEconomy == null ? null : new Spigot13VaultEconomy(rawEconomy);
	}

	@Override
	public void onUnload() {
		economy = null;
	}

	@Override
	public Optional<WSVaultEconomy> getEconomy() {
		return Optional.ofNullable(economy);
	}
}

package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.enums.block.EnumBlockTypeSkullType;
import com.degoos.wetsponge.material.block.Spigot13BlockTypeDirectional;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.Rotatable;

import java.util.Objects;
import java.util.Set;

public class Spigot13BlockTypeSkull extends Spigot13BlockTypeDirectional implements WSBlockTypeSkull {

	private EnumBlockFace rotation;
	private EnumBlockTypeSkullType skullType;

	public Spigot13BlockTypeSkull(EnumBlockFace facing, Set<EnumBlockFace> faces, EnumBlockFace rotation, EnumBlockTypeSkullType skullType) {
		super(144, "minecraft:skull", "minecraft:skull", 64, facing, faces);
		Validate.notNull(rotation, "Rotation cannot be null!");
		Validate.notNull(skullType, "Skull type cannot be null!");
		this.rotation = rotation;
		this.skullType = skullType;
	}

	@Override
	public String getNewStringId() {
		return "minecraft:" + skullType.getMinecraftName() + (getFacing() != EnumBlockFace.DOWN ? "_wall" : "") + "_head";
	}

	@Override
	public EnumBlockTypeSkullType getSkullType() {
		return skullType;
	}

	@Override
	public void setSkullType(EnumBlockTypeSkullType skullType) {
		Validate.notNull(skullType, "Skull type cannot be null!");
		this.skullType = skullType;
	}

	@Override
	public EnumBlockFace getRotation() {
		return rotation;
	}

	@Override
	public void setRotation(EnumBlockFace rotation) {
		Validate.notNull(rotation, "Rotation cannot be null!");
		this.rotation = rotation;
	}

	@Override
	public Set<EnumBlockFace> getFaces() {
		Set<EnumBlockFace> faces = super.getFaces();
		faces.add(EnumBlockFace.UP);
		return faces;
	}

	@Override
	public Spigot13BlockTypeSkull clone() {
		return new Spigot13BlockTypeSkull(getFacing(), getFaces(), rotation, skullType);
	}

	@Override
	public BlockData toBlockData() {
		BlockData blockData = super.toBlockData();
		if (blockData instanceof Rotatable) {
			((Rotatable) blockData).setRotation(BlockFace.valueOf(rotation.name()));
		}
		return blockData;
	}

	@Override
	public Spigot13BlockTypeSkull readBlockData(BlockData blockData) {
		super.readBlockData(blockData);
		if (blockData instanceof Rotatable) {
			rotation = EnumBlockFace.valueOf(((Rotatable) blockData).getRotation().name());
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeSkull that = (Spigot13BlockTypeSkull) o;
		return rotation == that.rotation &&
				skullType == that.skullType;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), rotation, skullType);
	}
}

package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.Spigot13BlockTypeDirectional;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.Ladder;

import java.util.Objects;
import java.util.Set;

public class Spigot13BlockTypeLadder extends Spigot13BlockTypeDirectional implements WSBlockTypeLadder {

	private boolean waterlogged;

	public Spigot13BlockTypeLadder(EnumBlockFace facing, Set<EnumBlockFace> faces, boolean waterlogged) {
		super(65, "minecraft:ladder", "minecraft:ladder", 64, facing, faces);
		this.waterlogged = waterlogged;
	}

	@Override
	public boolean isWaterlogged() {
		return waterlogged;
	}

	@Override
	public void setWaterlogged(boolean waterlogged) {
		this.waterlogged = waterlogged;
	}

	@Override
	public Spigot13BlockTypeLadder clone() {
		return new Spigot13BlockTypeLadder(getFacing(), getFaces(), waterlogged);
	}

	@Override
	public BlockData toBlockData() {
		BlockData blockData = super.toBlockData();
		if (blockData instanceof Ladder) {
			((Ladder) blockData).setWaterlogged(waterlogged);
		}
		return blockData;
	}

	@Override
	public Spigot13BlockTypeLadder readBlockData(BlockData blockData) {
		super.readBlockData(blockData);
		if (blockData instanceof Ladder) {
			waterlogged = ((Ladder) blockData).isWaterlogged();
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeLadder that = (Spigot13BlockTypeLadder) o;
		return waterlogged == that.waterlogged;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), waterlogged);
	}
}

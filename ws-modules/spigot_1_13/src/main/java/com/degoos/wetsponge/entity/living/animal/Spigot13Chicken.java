package com.degoos.wetsponge.entity.living.animal;


import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;
import com.degoos.wetsponge.util.reflection.Spigot13HandledUtils;
import org.bukkit.entity.Chicken;

public class Spigot13Chicken extends Spigot13Animal implements WSChicken {

	public Spigot13Chicken(Chicken entity) {
		super(entity);
	}

	@Override
	public int getTimeUntilNextEgg() {
		try {
			return ReflectionUtils.getFirstField(NMSUtils.getNMSClass("EntityChicken"), int.class).getInt(Spigot13HandledUtils.getEntityHandle(getHandled()));
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was getting the time until next egg of a chicken!");
			return 0;
		}
	}

	@Override
	public void setTimeUntilNextEgg(int timeUntilNextEgg) {
		try {
			ReflectionUtils.getFirstField(NMSUtils.getNMSClass("EntityChicken"), int.class).setInt(Spigot13HandledUtils.getEntityHandle(getHandled()), timeUntilNextEgg);
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was setting the time until next egg of a chicken!");
		}
	}

	@Override
	public Chicken getHandled() {
		return (Chicken) super.getHandled();
	}
}

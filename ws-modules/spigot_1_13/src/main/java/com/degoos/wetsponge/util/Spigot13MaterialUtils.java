package com.degoos.wetsponge.util;

import org.bukkit.Material;

import java.util.Arrays;
import java.util.Optional;

public class Spigot13MaterialUtils {


	public static Optional<Material> getByKey(String key) {
		String newKey = key.toLowerCase();
		return Arrays.stream(Material.values()).filter(target -> target.getKey().toString().equals(newKey)).findAny();
	}
}

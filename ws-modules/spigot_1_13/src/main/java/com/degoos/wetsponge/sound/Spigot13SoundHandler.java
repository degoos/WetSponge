package com.degoos.wetsponge.sound;

import com.degoos.wetsponge.entity.living.player.Spigot13Player;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.enums.EnumSoundCategory;
import com.flowpowered.math.vector.Vector3d;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.SoundCategory;
import org.bukkit.entity.Player;
import us.myles.ViaVersion.api.ViaVersion;

public class Spigot13SoundHandler {


	public static void playSound(WSSound sound, EnumSoundCategory category, WSPlayer player, Vector3d position, float volume, float pitch) {
		playSound(sound.getMinecraft1_9Sound(), category, player, position, volume, pitch);

	}


	public static void playSound(String sound, EnumSoundCategory category, WSPlayer player, Vector3d position, float volume, float pitch) {
		Player spigotPlayer = ((Spigot13Player) player).getHandled();
		Location location = new Location(spigotPlayer.getWorld(), position.getX(), position.getY(), position.getZ());
		if (Bukkit.getPluginManager().isPluginEnabled("ViaVersion")) {
			spigotPlayer.playSound(location,
					ViaVersion.getInstance().getPlayerVersion(spigotPlayer) == 47 ? sound : sound, volume, pitch);
		}
		spigotPlayer.playSound(location, sound, SoundCategory.valueOf(category.getSpigotName()), volume, pitch);

	}
}

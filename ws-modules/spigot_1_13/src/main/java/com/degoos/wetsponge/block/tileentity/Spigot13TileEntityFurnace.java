package com.degoos.wetsponge.block.tileentity;


import com.degoos.wetsponge.block.Spigot13Block;
import org.bukkit.block.Furnace;

public class Spigot13TileEntityFurnace extends Spigot13TileEntityInventory implements WSTileEntityFurnace {

    public Spigot13TileEntityFurnace(Spigot13Block block) {
        super(block);
    }


    @Override
    public int getBurnTime() {
        return getHandled().getBurnTime();
    }

    @Override
    public void setBurnTime(int burnTime) {
        getHandled().setBurnTime((short) burnTime);
        update();
    }

    @Override
    public int getCookTime() {
        return getHandled().getCookTime();
    }

    @Override
    public void setCookTime(int cookTime) {
        getHandled().setCookTime((short) cookTime);
        update();
    }

    @Override
    public Furnace getHandled() {
        return (Furnace) super.getHandled();
    }

}

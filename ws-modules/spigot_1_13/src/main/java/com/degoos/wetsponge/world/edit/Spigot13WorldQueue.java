package com.degoos.wetsponge.world.edit;

import com.degoos.wetsponge.SpigotWetSponge;
import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.material.block.WSBlockType;
import com.degoos.wetsponge.packet.play.server.WSSPacketBlockChange;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import com.degoos.wetsponge.util.reflection.Spigot13HandledUtils;
import com.degoos.wetsponge.world.Spigot13Chunk;
import com.degoos.wetsponge.world.WSWorld;
import com.flowpowered.math.vector.Vector2i;
import com.flowpowered.math.vector.Vector3i;
import org.bukkit.scheduler.BukkitRunnable;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Spigot13WorldQueue implements WSWorldQueue {

	private Map<Vector2i, Map<Vector3i, WSBlockType>> types;
	private WSWorld world;

	public Spigot13WorldQueue(WSWorld world) {
		this.world = world;
		types = new HashMap<>();
	}

	@Override
	public void setBlock(Vector3i position, WSBlockType blockType) {
		if (position.getY() < 0 || position.getY() > world.getProperties().getMaxHeight()) return;
		Vector2i chunk = new Vector2i(position.getX() >> 4, position.getZ() >> 4);
		Map<Vector3i, WSBlockType> chunkMap = types.get(chunk);
		if (chunkMap == null) chunkMap = new HashMap<>();
		chunkMap.put(position, blockType);
		types.put(chunk, chunkMap);
	}

	@Override
	public void flush(boolean sendPacket, boolean async) {
		if (Thread.currentThread() != WetSponge.getMainThread()) {
			new BukkitRunnable() {
				@Override
				public void run() {
					flush(sendPacket, async);
				}
			}.runTaskLater(SpigotWetSponge.getInstance(), 0);
			return;
		}
		try {
			Class<?> chunkClass = NMSUtils.getNMSClass("Chunk");
			Class<?> blockPositionClass = NMSUtils.getNMSClass("BlockPosition");
			Class<?> iBlockDataClass = NMSUtils.getNMSClass("IBlockData");
			Method setBlockState = chunkClass.getMethod("a", blockPositionClass, iBlockDataClass);

			Map<Vector2i, Spigot13Chunk> chunks = new HashMap<>();
			types.keySet().forEach(vector -> {
				try {
					Spigot13Chunk chunk = (Spigot13Chunk) world.loadChunk(vector.getX(), vector.getY(), true).orElse(null);
					chunk.setCanBeUnloaded(false);
					chunks.put(vector, chunk);
				} catch (Exception e) {
					e.printStackTrace();
				}
			});

			Thread thread = new Thread(() -> {
				if (sendPacket) {
					Set<WSPlayer> players = world.getPlayers();
					types.forEach((vector2i, blocks) -> {
						try {
							Spigot13Chunk chunk = chunks.get(vector2i);
							blocks.forEach((vector3i, blockType) -> {
								try {
									setBlockState.invoke(chunk.getBukkitHandled(), Spigot13HandledUtils.getBlockPosition(vector3i), Spigot13HandledUtils.getBlockState(blockType));
									WSSPacketBlockChange packet = WSSPacketBlockChange.of(blockType, vector3i);
									players.forEach(player -> player.sendPacket(packet));
								} catch (Exception ex) {
									InternalLogger.printException(ex, "An error has occurred while WetSponge was flushing a queue!");
								}
							});
							chunk.setCanBeUnloaded(true);
						} catch (Exception ex) {
							InternalLogger.printException(ex, "An error has occurred while WetSponge was flushing a queue!");
						}
					});
					return;
				}
				types.forEach((vector2i, blocks) -> {
					try {
						Spigot13Chunk chunk = chunks.get(vector2i);
						blocks.forEach((vector3i, blockType) -> {
							try {
								setBlockState.invoke(chunk.getBukkitHandled(), Spigot13HandledUtils.getBlockPosition(vector3i), Spigot13HandledUtils.getBlockState(blockType));
							} catch (Exception ex) {
								InternalLogger.printException(ex, "An error has occurred while WetSponge was flushing a queue!");
							}
						});
						chunk.setCanBeUnloaded(true);
					} catch (Exception ex) {
						InternalLogger.printException(ex, "An error has occurred while WetSponge was flushing a queue!");
					}
				});
			});
			if (async) thread.start();
			else thread.run();
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was flushing a queue!");
		}
	}
}

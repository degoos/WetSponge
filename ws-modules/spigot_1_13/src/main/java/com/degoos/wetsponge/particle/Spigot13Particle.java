package com.degoos.wetsponge.particle;


import com.degoos.wetsponge.entity.living.player.Spigot13Player;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.exception.particle.WSInvalidParticleException;
import com.degoos.wetsponge.resource.spigot.Spigot13ParticleEffect;
import com.degoos.wetsponge.world.Spigot13Location;
import com.degoos.wetsponge.world.WSLocation;
import com.flowpowered.math.vector.Vector3d;
import com.flowpowered.math.vector.Vector3f;

import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

public class Spigot13Particle implements WSParticle {

    private String id, minecraftId, spigotName;
    private Spigot13ParticleEffect effect;


    public Spigot13Particle(String id, String minecraftId, String spigotName) {
        this.id = id;
        this.minecraftId = minecraftId;
        this.spigotName = spigotName;
        effect = Spigot13ParticleEffect.fromName(id);
        if (effect == null) throw new WSInvalidParticleException("Cannot found particle " + id + "!");
    }


    @Override
    public void spawnParticle(WSLocation location, float speed, int amount, WSPlayer... players) {
        spawnParticle(location, speed, amount, new Vector3f(1, 1, 1), players);
    }


    @Override
    public void spawnParticle(WSLocation location, float speed, int amount, Collection<WSPlayer> players) {
        spawnParticle(location, speed, amount, new Vector3f(1, 1, 1), players);
    }


    @Override
    public void spawnParticle(WSLocation location, float speed, int amount, Vector3d playerRadius) {
        spawnParticle(location, speed, amount, new Vector3f(1, 1, 1), playerRadius);
    }


    @Override
    public void spawnParticle(WSLocation location, float speed, int amount, Vector3f radius, WSPlayer... players) {
        spawnParticle(location, speed, amount, radius, Arrays.asList(players));
    }


    @Override
    public void spawnParticle(WSLocation location, float speed, int amount, Vector3f radius, Collection<WSPlayer> players) {
        if(players.isEmpty()) return;
        effect.display(radius.getFloorX(), radius.getY(), radius.getZ(), speed, amount, ((Spigot13Location) location).getLocation(),
                players.stream().map(player -> ((Spigot13Player)player).getHandled()).collect(Collectors.toList()));
    }


    @Override
    public void spawnParticle(WSLocation location, float speed, int amount, Vector3f radius, Vector3d playerRadius) {
        spawnParticle(location, speed, amount, radius, location.getNearbyPlayers(playerRadius));
    }


    @Override
    public String getOldMinecraftId() {
        return id;
    }


    @Override
    public String getMinecraftId() {
        return minecraftId;
    }

    @Override
    public String getSpigotName() {
        return spigotName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Spigot13Particle that = (Spigot13Particle) o;

        return id.equals(that.id);
    }


    @Override
    public int hashCode() {
        return id.hashCode();
    }
}

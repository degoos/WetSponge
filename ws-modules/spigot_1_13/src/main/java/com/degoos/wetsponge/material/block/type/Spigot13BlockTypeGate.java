package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.Spigot13BlockTypeDirectional;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.Gate;

import java.util.Objects;
import java.util.Set;

public class Spigot13BlockTypeGate extends Spigot13BlockTypeDirectional implements WSBlockTypeGate {

	private boolean inWall, open, powered;

	public Spigot13BlockTypeGate(int numericalId, String oldStringId, String newStringId, int maxStackSize, EnumBlockFace facing, Set<EnumBlockFace> faces,
								 boolean inWall, boolean open, boolean powered) {
		super(numericalId, oldStringId, newStringId, maxStackSize, facing, faces);
		this.inWall = inWall;
		this.open = open;
		this.powered = powered;
	}

	@Override
	public boolean isInWall() {
		return inWall;
	}

	@Override
	public void setInWall(boolean inWall) {
		this.inWall = inWall;
	}

	@Override
	public boolean isOpen() {
		return open;
	}

	@Override
	public void setOpen(boolean open) {
		this.open = open;
	}

	@Override
	public boolean isPowered() {
		return powered;
	}

	@Override
	public void setPowered(boolean powered) {
		this.powered = powered;
	}

	@Override
	public Spigot13BlockTypeGate clone() {
		return new Spigot13BlockTypeGate(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), getFacing(), getFaces(), inWall, open, powered);
	}

	@Override
	public BlockData toBlockData() {
		BlockData blockData = super.toBlockData();
		if (blockData instanceof Gate) {
			((Gate) blockData).setInWall(inWall);
			((Gate) blockData).setOpen(open);
			((Gate) blockData).setPowered(powered);
		}
		return blockData;
	}

	@Override
	public Spigot13BlockTypeGate readBlockData(BlockData blockData) {
		super.readBlockData(blockData);
		if (blockData instanceof Gate) {
			inWall = ((Gate) blockData).isInWall();
			open = ((Gate) blockData).isOpen();
			powered = ((Gate) blockData).isPowered();
		}
		return this;
	}


	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeGate that = (Spigot13BlockTypeGate) o;
		return inWall == that.inWall &&
				open == that.open &&
				powered == that.powered;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), inWall, open, powered);
	}
}

package com.degoos.wetsponge.world;

import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;
import com.flowpowered.math.vector.Vector2d;
import org.bukkit.Location;
import org.bukkit.WorldBorder;

public class Spigot13WorldBorder implements WSWorldBorder {

	private WorldBorder worldBorder;

	public Spigot13WorldBorder(WorldBorder worldBorder) {
		this.worldBorder = worldBorder;
	}

	@Override
	public void copyPropertiesFrom(WSWorldBorder worldBorder) {
		worldBorder.setCenter(getCenter());
		worldBorder.setDamageAmount(getDamageAmount());
		worldBorder.setDamageThreshold(getDamageThreshold());
		worldBorder.setDiameter(getDiameter());
		worldBorder.setWarningDistance(getWarningDistance());
		worldBorder.setWaningTime(getWarningTime());
	}

	@Override
	public Vector2d getCenter() {
		Location location = worldBorder.getCenter();
		return new Vector2d(location.getX(), location.getZ());
	}

	@Override
	public void setCenter(Vector2d center) {
		worldBorder.setCenter(center.getX(), center.getY());
	}

	@Override
	public void setCenter(double x, double z) {
		worldBorder.setCenter(x, z);
	}

	@Override
	public double getDamageAmount() {
		return worldBorder.getDamageAmount();
	}

	@Override
	public void setDamageAmount(double damageAmount) {
		worldBorder.setDamageAmount(damageAmount);
	}

	@Override
	public double getDamageThreshold() {
		return worldBorder.getDamageBuffer();
	}

	@Override
	public void setDamageThreshold(double damageThreshold) {
		worldBorder.setDamageBuffer(damageThreshold);
	}

	@Override
	public double getDiameter() {
		return worldBorder.getSize();
	}

	@Override
	public void setDiameter(double diameter) {
		setDiameter(getDiameter(), diameter, 0);
	}

	@Override
	public void setDiameter(double diameter, long time) {
		setDiameter(getDiameter(), diameter, time);
	}

	@Override
	public void setDiameter(double startDiameter, double endDiameter, long time) {
		try {
			Object handled = ReflectionUtils.getObject(getHandled(), "handle");
			ReflectionUtils
				.invokeMethod(handled, "transitionSizeBetween", Math.min(6.0E7D, Math.max(1.0D, startDiameter)), Math.min(6.0E7D, Math.max(1.0D, endDiameter)), Math
					.min(9223372036854775L, Math.max(0L, time)));
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error had occurred while WetSponge was trying to set the diameter of a WorldBorder!");
		}
	}

	@Override
	public double getNewDiameter() {
		try {
			Object handled = ReflectionUtils.getObject(getHandled(), "handle");
			return (double) ReflectionUtils.invokeMethod(handled, "j");
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error had occurred while WetSponge was trying to set the diameter of a WorldBorder!");
			return 0;
		}
	}

	@Override
	public int getWarningDistance() {
		return worldBorder.getWarningDistance();
	}

	@Override
	public void setWarningDistance(int warningDistance) {
		worldBorder.setWarningDistance(warningDistance);
	}

	@Override
	public int getWarningTime() {
		return worldBorder.getWarningTime();
	}

	@Override
	public void setWaningTime(int waningTime) {
		worldBorder.setWarningTime(waningTime);
	}

	@Override
	public WorldBorder getHandled() {
		return worldBorder;
	}
}

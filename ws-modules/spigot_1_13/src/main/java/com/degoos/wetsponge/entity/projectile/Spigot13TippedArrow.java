package com.degoos.wetsponge.entity.projectile;

import com.degoos.wetsponge.effect.potion.Spigot13PotionEffect;
import com.degoos.wetsponge.effect.potion.WSPotionEffect;
import com.degoos.wetsponge.enums.EnumPotionEffectType;
import org.bukkit.entity.TippedArrow;
import org.bukkit.potion.PotionEffectType;

import java.util.List;
import java.util.stream.Collectors;

public class Spigot13TippedArrow extends Spigot13Arrow implements WSTippedArrow {


    public Spigot13TippedArrow(TippedArrow entity) {
        super(entity);
    }

    @Override
    public void addPotionEffect(WSPotionEffect effect) {
        getHandled().addCustomEffect(((Spigot13PotionEffect) effect).getHandled(), true);
    }

    @Override
    public List<WSPotionEffect> getPotionEffects() {
        return getHandled().getCustomEffects().stream().map(Spigot13PotionEffect::new).collect(Collectors.toList());
    }

    @Override
    public void clearAllPotionEffects() {
        getHandled().clearCustomEffects();
    }

    @Override
    public void removePotionEffect(EnumPotionEffectType potionEffectType) {
        getHandled().removeCustomEffect(PotionEffectType.getById(potionEffectType.getValue()));
    }

    @Override
    public TippedArrow getHandled() {
        return (TippedArrow) super.getHandled();
    }

}

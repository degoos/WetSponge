package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import org.bukkit.block.data.BlockData;

import java.util.Objects;
import java.util.Set;

public class Spigot13BlockTypeCobblestoneWall extends Spigot13BlockTypeFence implements WSBlockTypeCobblestoneWall {

	private boolean mossy;

	public Spigot13BlockTypeCobblestoneWall(Set<EnumBlockFace> faces, Set<EnumBlockFace> allowedFaces, boolean waterlogged, boolean mossy) {
		super(139, "minecraft:cobblestone_wall", "minecraft:cobblestone_wall", 64, faces, allowedFaces, waterlogged);
		this.mossy = mossy;
	}

	@Override
	public String getNewStringId() {
		return mossy ? "minecraft:mossy_cobblestone_wall" : "minecraft:cobblestone_wall";
	}

	@Override
	public boolean isMossy() {
		return mossy;
	}

	@Override
	public void setMossy(boolean mossy) {
		this.mossy = mossy;
	}

	@Override
	public Spigot13BlockTypeCobblestoneWall clone() {
		return new Spigot13BlockTypeCobblestoneWall(getFaces(), getAllowedFaces(), isWaterlogged(), mossy);
	}

	@Override
	public Spigot13BlockTypeCobblestoneWall readBlockData(BlockData blockData) {
		super.readBlockData(blockData);
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeCobblestoneWall that = (Spigot13BlockTypeCobblestoneWall) o;
		return mossy == that.mossy;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), mossy);
	}
}

package com.degoos.wetsponge.material.item.type;

import com.degoos.wetsponge.material.item.Spigot13ItemType;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

import java.util.Objects;

public class Spigot13ItemTypeDamageable extends Spigot13ItemType implements WSItemTypeDamageable {

	private int damage, maxUses;

	public Spigot13ItemTypeDamageable(int numericalId, String oldStringId, String newStringId, int damage, int maxUses) {
		super(numericalId, oldStringId, newStringId, 1);
		this.damage = damage;
		this.maxUses = maxUses;
	}

	@Override
	public int getDamage() {
		return damage;
	}

	@Override
	public void setDamage(int damage) {
		this.damage = damage;
	}

	@Override
	public int getMaxUses() {
		return maxUses;
	}

	@Override
	public Spigot13ItemTypeDamageable clone() {
		return new Spigot13ItemTypeDamageable(getNumericalId(), getOldStringId(), getNewStringId(), damage, maxUses);
	}

	@Override
	public void writeNBTTag(WSNBTTagCompound compound) {
		super.writeNBTTag(compound);
		compound.setInteger("Damage", damage);
	}

	@Override
	public void readNBTTag(WSNBTTagCompound compound) {
		super.readNBTTag(compound);
		damage = compound.getInteger("Damage");
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13ItemTypeDamageable that = (Spigot13ItemTypeDamageable) o;
		return damage == that.damage &&
				maxUses == that.maxUses;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), damage, maxUses);
	}
}

package com.degoos.wetsponge.entity.living.animal;


import com.degoos.wetsponge.enums.EnumLlamaType;
import com.degoos.wetsponge.inventory.Spigot13Inventory;
import com.degoos.wetsponge.inventory.WSInventory;
import org.bukkit.Bukkit;
import org.bukkit.entity.AnimalTamer;
import org.bukkit.entity.Llama;

import java.util.Arrays;
import java.util.Optional;
import java.util.UUID;

public class Spigot13Llama extends Spigot13AbstractHorse implements WSLlama {


	public Spigot13Llama(Llama entity) {
		super(entity);
	}


	@Override
	public boolean isTamed() {
		return getHandled().isTamed();
	}


	@Override
	public Optional<UUID> getTamer() {
		AnimalTamer tamer = getHandled().getOwner();
		if (tamer == null) return Optional.empty();
		return Optional.of(tamer.getUniqueId());
	}


	@Override
	public void setTamer(UUID tamer) {
		getHandled().setOwner(tamer == null ? null : Bukkit.getOfflinePlayer(tamer));
	}


	@Override
	public Llama getHandled() {
		return (Llama) super.getHandled();
	}


	@Override
	public EnumLlamaType getLlamaType() {
		return EnumLlamaType.getByName(getHandled().getColor().toString()).orElse(EnumLlamaType.CREAMY);
	}


	@Override
	public void setLlamaType(EnumLlamaType type) {
		getHandled().setColor(Arrays.stream(Llama.Color.values()).filter(target -> target.name().equalsIgnoreCase(type.toString())).findAny().orElse(Llama.Color
			.CREAMY));
	}

	@Override
	public WSInventory getInventory() {
		return new Spigot13Inventory(getHandled().getInventory());
	}

	@Override
	public boolean hasChest() {
		return getHandled().isCarryingChest();
	}

	@Override
	public void setChested(boolean chested) {
		getHandled().setCarryingChest(chested);
	}
}

package com.degoos.wetsponge.material.block;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.util.Spigot13MaterialUtils;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.Material;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.Directional;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class Spigot13BlockTypeDirectional extends Spigot13BlockType implements WSBlockTypeDirectional {

	private EnumBlockFace facing;
	private Set<EnumBlockFace> faces;

	public Spigot13BlockTypeDirectional(int numericalId, String oldStringId, String newStringId, int maxStackSize, EnumBlockFace facing, Set<EnumBlockFace> faces) {
		super(numericalId, oldStringId, newStringId, maxStackSize);
		Validate.notNull(facing, "Facing cannot be null!");
		this.facing = facing;
		this.faces = faces;
	}

	@Override
	public EnumBlockFace getFacing() {
		return facing;
	}

	@Override
	public void setFacing(EnumBlockFace blockFace) {
		Validate.notNull(blockFace, "Facing cannot be null!");
		this.facing = blockFace;
	}

	@Override
	public Set<EnumBlockFace> getFaces() {
		return new HashSet<>(faces);
	}

	@Override
	public Spigot13BlockTypeDirectional clone() {
		return new Spigot13BlockTypeDirectional(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), facing, new HashSet<>(faces));
	}

	@Override
	public BlockData toBlockData() {
		Material material = Spigot13MaterialUtils.getByKey(getNewStringId()).orElse(null);
		if (material == null) return null;
		BlockData data = material.createBlockData();
		if (data instanceof Directional) {
			((Directional) data).setFacing(BlockFace.valueOf(facing.name()));
		}
		return data;
	}

	@Override
	public Spigot13BlockTypeDirectional readBlockData(BlockData blockData) {
		if (blockData instanceof Directional) {
			facing = EnumBlockFace.valueOf(((Directional) blockData).getFacing().name());
			faces = ((Directional) blockData).getFaces().stream().map(target -> EnumBlockFace.valueOf(target.name())).collect(Collectors.toSet());
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeDirectional that = (Spigot13BlockTypeDirectional) o;
		return facing == that.facing;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), facing);
	}
}

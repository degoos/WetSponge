package com.degoos.wetsponge.material.item.type;

import com.degoos.wetsponge.enums.EnumDyeColor;

public class Spigot13ItemTypeDye extends Spigot13ItemTypeDyeColored implements WSItemTypeDye {

	public Spigot13ItemTypeDye(EnumDyeColor dyeColor) {
		super(351, "minecraft:dye", "dye", 64, dyeColor);
	}

	@Override
	public String getNewStringId() {
		switch (getDyeColor()) {
			case RED:
				return "minecraft:rose_red";
			case BLUE:
				return "minecraft:lapis_lazuli";
			case CYAN:
				return "minecraft:cyan_dye";
			case GRAY:
				return "minecraft:gray_dye";
			case LIME:
				return "minecraft:lime_dye";
			case PINK:
				return "minecraft:pink_dye";
			case BLACK:
				return "minecraft:ink_sac";
			case BROWN:
				return "minecraft:cocoa_beans";
			case GREEN:
				return "minecraft:cactus_green";
			case ORANGE:
				return "minecraft:orange_dye";
			case PURPLE:
				return "minecraft:purple_dye";
			case YELLOW:
				return "minecraft:dandelion_yellow";
			case MAGENTA:
				return "minecraft:magenta_dye";
			case LIGHT_BLUE:
				return "minecraft:light_blue_dye";
			case LIGHT_GRAY:
				return "minecraft:light_gray_dye";
			case WHITE:
			default:
				return "minecraft:bone_meal";
		}
	}

	@Override
	public Spigot13ItemTypeDye clone() {
		return new Spigot13ItemTypeDye(getDyeColor());
	}
}

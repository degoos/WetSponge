package com.degoos.wetsponge.nbt;


import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;

import java.lang.reflect.InvocationTargetException;

public class Spigot13NBTTagByte extends Spigot13NBTBase implements WSNBTTagByte {

	private static final Class<?> clazz = NMSUtils.getNMSClass("NBTTagByte");

	public Spigot13NBTTagByte(byte b) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
		this(clazz.getConstructor(byte.class).newInstance(b));
	}

	public Spigot13NBTTagByte(Object object) {
		super(object);
	}

	private byte getValue() {
		try {
			return ReflectionUtils.getFirstObject(clazz, byte.class, getHandled());
		} catch (Exception e) {
			InternalLogger.printException(e, "An error has occurred while WetSponge was getting the value of a NBTTag!");
			return 0;
		}
	}

	@Override
	public long getLong() {
		return getValue();
	}

	@Override
	public int getInt() {
		return getValue();
	}

	@Override
	public short getShort() {
		return getValue();
	}

	@Override
	public byte getByte() {
		return getValue();
	}

	@Override
	public double getDouble() {
		return getValue();
	}

	@Override
	public float getFloat() {
		return getValue();
	}

	@Override
	public WSNBTTagByte copy() {
		try {
			return new Spigot13NBTTagByte(getValue());
		} catch (Exception e) {
			InternalLogger.printException(e, "An error has occurred while WetSponge was copying a NBTTag!");
			return null;
		}
	}

	@Override
	public Object getHandled() {
		return super.getHandled();
	}
}

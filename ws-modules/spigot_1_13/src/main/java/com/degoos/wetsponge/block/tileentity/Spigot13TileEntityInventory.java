package com.degoos.wetsponge.block.tileentity;


import com.degoos.wetsponge.block.Spigot13Block;
import com.degoos.wetsponge.inventory.Spigot13Inventory;
import org.bukkit.block.BlockState;
import org.bukkit.inventory.InventoryHolder;

public class Spigot13TileEntityInventory extends Spigot13TileEntity implements WSTileEntityInventory {

	public Spigot13TileEntityInventory(Spigot13Block block) {
		super(block);
	}

	public Spigot13TileEntityInventory(BlockState blockState) {
		super(blockState);
	}

	@Override
	public Spigot13Inventory getInventory() {
		return new Spigot13Inventory(getHandled().getInventory());
	}

	@Override
	public InventoryHolder getHandled() {
		return (InventoryHolder) super.getHandled();
	}
}

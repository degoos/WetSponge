package com.degoos.wetsponge.material.block;

import com.degoos.wetsponge.enums.block.EnumBlockTypeRailShape;
import com.degoos.wetsponge.util.Spigot13MaterialUtils;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.Material;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.Rail;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class Spigot13BlockTypeRail extends Spigot13BlockType implements WSBlockTypeRail {

	private EnumBlockTypeRailShape shape;
	private Set<EnumBlockTypeRailShape> allowedShapes;

	public Spigot13BlockTypeRail(int numericalId, String oldStringId, String newStringId, int maxStackSize, EnumBlockTypeRailShape shape, Set<EnumBlockTypeRailShape> allowedShapes) {
		super(numericalId, oldStringId, newStringId, maxStackSize);
		Validate.notNull(shape, "Shape cannot be null!");
		this.shape = shape;
		this.allowedShapes = allowedShapes;
	}

	@Override
	public EnumBlockTypeRailShape getShape() {
		return shape;
	}

	@Override
	public void setShape(EnumBlockTypeRailShape shape) {
		Validate.notNull(shape, "Shape cannot be null!");
		this.shape = shape;
	}

	@Override
	public Set<EnumBlockTypeRailShape> allowedShapes() {
		return new HashSet<>(allowedShapes);
	}

	@Override
	public Spigot13BlockTypeRail clone() {
		return new Spigot13BlockTypeRail(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), shape, new HashSet<>(allowedShapes));
	}

	@Override
	public BlockData toBlockData() {
		Material material = Spigot13MaterialUtils.getByKey(getNewStringId()).orElse(null);
		if (material == null) return null;
		BlockData data = material.createBlockData();
		if (data instanceof Rail) {
			((Rail) data).setShape(Rail.Shape.valueOf(shape.name()));
		}
		return data;
	}

	@Override
	public Spigot13BlockTypeRail readBlockData(BlockData blockData) {
		if (blockData instanceof Rail) {
			shape = EnumBlockTypeRailShape.valueOf(((Rail) blockData).getShape().name());
			allowedShapes = ((Rail) blockData).getShapes().stream().map(target -> EnumBlockTypeRailShape.valueOf(target.name())).collect(Collectors.toSet());
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeRail that = (Spigot13BlockTypeRail) o;
		return shape == that.shape &&
				Objects.equals(allowedShapes, that.allowedShapes);
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), shape, allowedShapes);
	}
}

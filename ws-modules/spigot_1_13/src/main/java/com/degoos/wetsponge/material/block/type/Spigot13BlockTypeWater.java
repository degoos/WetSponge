package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.material.block.Spigot13BlockTypeLevelled;
import org.bukkit.block.data.BlockData;

public class Spigot13BlockTypeWater extends Spigot13BlockTypeLevelled implements WSBlockTypeWater {

	public Spigot13BlockTypeWater(int level, int maximumLevel) {
		super(9, "minecraft:water", "minecraft:water", 64, level, maximumLevel);
	}

	@Override
	public int getNumericalId() {
		return getLevel() == 0 ? 9 : 8;
	}

	@Override
	public String getOldStringId() {
		return getLevel() == 0 ? "minecraft:water" : "minecraft:flowing_water";
	}

	@Override
	public Spigot13BlockTypeWater clone() {
		return new Spigot13BlockTypeWater(getLevel(), getMaximumLevel());
	}

	@Override
	public Spigot13BlockTypeWater readBlockData(BlockData blockData) {
		super.readBlockData(blockData);
		return this;
	}
}

package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockTypePottedPlant;
import com.degoos.wetsponge.material.block.Spigot13BlockType;
import org.bukkit.block.data.BlockData;

import java.util.Objects;

public class Spigot13BlockTypeFlowerPot extends Spigot13BlockType implements WSBlockTypeFlowerPot {

	private EnumBlockTypePottedPlant pottedPlant;

	public Spigot13BlockTypeFlowerPot(EnumBlockTypePottedPlant pottedPlant) {
		super(140, "minecraft:flower_pot", "minecraft:flower_pot", 64);
		this.pottedPlant = pottedPlant == null ? EnumBlockTypePottedPlant.NONE : pottedPlant;
	}

	@Override
	public String getNewStringId() {
		return pottedPlant == EnumBlockTypePottedPlant.NONE ? "minecraft:flower_pot" : "minecraft:potted_" + pottedPlant.name().toLowerCase();
	}

	@Override
	public EnumBlockTypePottedPlant getPottedPlant() {
		return pottedPlant;
	}

	@Override
	public void setPottedPlant(EnumBlockTypePottedPlant pottedPlant) {
		this.pottedPlant = pottedPlant == null ? EnumBlockTypePottedPlant.NONE : pottedPlant;
	}

	@Override
	public Spigot13BlockTypeFlowerPot clone() {
		return new Spigot13BlockTypeFlowerPot(pottedPlant);
	}

	@Override
	public Spigot13BlockTypeFlowerPot readBlockData(BlockData blockData) {
		super.readBlockData(blockData);
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeFlowerPot that = (Spigot13BlockTypeFlowerPot) o;
		return pottedPlant == that.pottedPlant;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), pottedPlant);
	}
}

package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.Spigot13BlockTypeDirectional;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.Repeater;

import java.util.Objects;
import java.util.Set;

public class Spigot13BlockTypeRepeater extends Spigot13BlockTypeDirectional implements WSBlockTypeRepeater {

	private int delay, minimumDelay, maximumDelay;
	private boolean locked, powered;

	public Spigot13BlockTypeRepeater(EnumBlockFace facing, Set<EnumBlockFace> faces, int delay, int minimumDelay, int maximumDelay, boolean locked, boolean powered) {
		super(93, "minecraft:unpowered_repeater", "minecraft:repeater", 64, facing, faces);
		this.delay = delay;
		this.minimumDelay = minimumDelay;
		this.maximumDelay = maximumDelay;
		this.locked = locked;
		this.powered = powered;
	}

	@Override
	public int getNumericalId() {
		return powered ? 94 : 93;
	}

	@Override
	public String getOldStringId() {
		return powered ? "minecraft:powered_repeater" : "minecraft:unpowered_repeater";
	}

	@Override
	public int getDelay() {
		return delay;
	}

	@Override
	public void setDelay(int delay) {
		this.delay = Math.max(minimumDelay, Math.min(minimumDelay, delay));
	}

	@Override
	public int getMinimumDelay() {
		return minimumDelay;
	}

	@Override
	public int getMaximumDelay() {
		return maximumDelay;
	}

	@Override
	public boolean isLocked() {
		return locked;
	}

	@Override
	public void setLocked(boolean locked) {
		this.locked = locked;
	}

	@Override
	public boolean isPowered() {
		return powered;
	}

	@Override
	public void setPowered(boolean powered) {
		this.powered = powered;
	}

	@Override
	public Spigot13BlockTypeRepeater clone() {
		return new Spigot13BlockTypeRepeater(getFacing(), getFaces(), delay, minimumDelay, maximumDelay, locked, powered);
	}

	@Override
	public BlockData toBlockData() {
		BlockData blockData = super.toBlockData();
		if (blockData instanceof Repeater) {
			((Repeater) blockData).setDelay(delay);
			((Repeater) blockData).setPowered(powered);
			((Repeater) blockData).setLocked(locked);
		}
		return blockData;
	}

	@Override
	public Spigot13BlockTypeRepeater readBlockData(BlockData blockData) {
		super.readBlockData(blockData);
		if (blockData instanceof Repeater) {
			delay = ((Repeater) blockData).getDelay();
			minimumDelay = ((Repeater) blockData).getMinimumDelay();
			maximumDelay = ((Repeater) blockData).getMaximumDelay();
			locked = ((Repeater) blockData).isLocked();
			powered = ((Repeater) blockData).isPowered();
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeRepeater that = (Spigot13BlockTypeRepeater) o;
		return delay == that.delay &&
				minimumDelay == that.minimumDelay &&
				maximumDelay == that.maximumDelay &&
				locked == that.locked &&
				powered == that.powered;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), delay, minimumDelay, maximumDelay, locked, powered);
	}
}

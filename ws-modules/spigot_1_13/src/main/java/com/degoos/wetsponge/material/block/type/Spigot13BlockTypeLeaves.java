package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumWoodType;
import com.degoos.wetsponge.material.block.Spigot13BlockType;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.Leaves;

import java.util.Objects;

public class Spigot13BlockTypeLeaves extends Spigot13BlockType implements WSBlockTypeLeaves {

	private boolean persistent;
	private int distance;
	private EnumWoodType woodType;

	public Spigot13BlockTypeLeaves(boolean persistent, int distance, EnumWoodType woodType) {
		super(18, "minecraft_leaves", "minecraft:leaves", 64);
		Validate.notNull(woodType, "Wood type cannot be null!");
		this.persistent = persistent;
		this.distance = Math.min(7, Math.max(1, distance));
		this.woodType = woodType;
	}


	@Override
	public int getNumericalId() {
		return getWoodType() == EnumWoodType.ACACIA || getWoodType() == EnumWoodType.DARK_OAK ? 161 : 18;
	}

	@Override
	public String getNewStringId() {
		return getWoodType() == EnumWoodType.ACACIA || getWoodType() == EnumWoodType.DARK_OAK ? "minecraft:leaves2" : "minecraft:leaves";
	}

	@Override
	public String getOldStringId() {
		return "minecraft:" + getWoodType().name().toLowerCase() + "_leaves";
	}

	@Override
	public boolean isPersistent() {
		return persistent;
	}

	@Override
	public void setPersistent(boolean persistent) {
		this.persistent = persistent;
	}

	@Override
	public int getDistance() {
		return distance;
	}

	@Override
	public void setDistance(int distance) {
		this.distance = Math.min(7, Math.max(1, distance));
	}

	@Override
	public EnumWoodType getWoodType() {
		return woodType;
	}

	@Override
	public void setWoodType(EnumWoodType woodType) {
		Validate.notNull(woodType, "Wood type cannot be null!");
		this.woodType = woodType;
	}

	@Override
	public Spigot13BlockTypeLeaves clone() {
		return new Spigot13BlockTypeLeaves(persistent, distance, woodType);
	}

	@Override
	public BlockData toBlockData() {
		BlockData blockData = super.toBlockData();
		if (blockData instanceof Leaves) {
			((Leaves) blockData).setDistance(distance);
			((Leaves) blockData).setPersistent(persistent);
		}
		return blockData;
	}

	@Override
	public Spigot13BlockTypeLeaves readBlockData(BlockData blockData) {
		super.readBlockData(blockData);
		if (blockData instanceof Leaves) {
			distance = ((Leaves) blockData).getDistance();
			persistent = ((Leaves) blockData).isPersistent();
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeLeaves that = (Spigot13BlockTypeLeaves) o;
		return persistent == that.persistent &&
				distance == that.distance &&
				woodType == that.woodType;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), persistent, distance, woodType);
	}
}

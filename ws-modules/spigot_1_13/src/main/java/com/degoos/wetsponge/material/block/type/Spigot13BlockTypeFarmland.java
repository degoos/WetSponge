package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.material.block.Spigot13BlockType;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.Farmland;

import java.util.Objects;

public class Spigot13BlockTypeFarmland extends Spigot13BlockType implements WSBlockTypeFarmland {

	private int moisture, maximumMoisture;

	public Spigot13BlockTypeFarmland(int moisture, int maximumMoisture) {
		super(60, "minecraft:farmland", "minecraft:farmland", 64);
		this.moisture = moisture;
		this.maximumMoisture = maximumMoisture;
	}

	@Override
	public int getMoisture() {
		return moisture;
	}

	@Override
	public void setMoisture(int moisture) {
		this.moisture = moisture;
	}

	@Override
	public int getMaximumMoisture() {
		return maximumMoisture;
	}

	@Override
	public Spigot13BlockTypeFarmland clone() {
		return new Spigot13BlockTypeFarmland(moisture, maximumMoisture);
	}

	@Override
	public BlockData toBlockData() {
		BlockData blockData = super.toBlockData();
		if (blockData instanceof Farmland) {
			((Farmland) blockData).setMoisture(moisture);
		}
		return blockData;
	}

	@Override
	public Spigot13BlockTypeFarmland readBlockData(BlockData blockData) {
		if (blockData instanceof Farmland) {
			moisture = ((Farmland) blockData).getMoisture();
			maximumMoisture = ((Farmland) blockData).getMaximumMoisture();
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeFarmland that = (Spigot13BlockTypeFarmland) o;
		return moisture == that.moisture &&
				maximumMoisture == that.maximumMoisture;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), moisture, maximumMoisture);
	}
}

package com.degoos.wetsponge.entity.living.merchant;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.entity.living.Spigot13Ageable;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.enums.EnumVillagerProfession;
import com.degoos.wetsponge.merchant.Spigot13Trade;
import com.degoos.wetsponge.merchant.WSTrade;
import com.degoos.wetsponge.parser.player.PlayerParser;
import com.degoos.wetsponge.resource.spigot.Spigot13MerchantUtils;
import org.bukkit.entity.Villager;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

public class Spigot13Villager extends Spigot13Ageable implements WSVillager {


	public Spigot13Villager(Villager entity) {
		super(entity);
	}

	@Override
	public EnumVillagerProfession getProfession() {
		return EnumVillagerProfession.getBySpigotName(getHandled().getProfession().name()).orElseThrow(NullPointerException::new);
	}

	@Override
	public void setProfession(EnumVillagerProfession profession) {
		getHandled().setProfession(Villager.Profession.valueOf(profession.getSpigotName()));
	}

	@Override
	public Optional<WSPlayer> getCustomer() {
		return WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD) ? Optional.ofNullable(getHandled().getTrader())
			.map(target -> PlayerParser.getPlayer(target.getUniqueId()).orElse(null)).filter(Objects::nonNull) : Optional.empty();
	}

	@Override
	public void setCustomer(WSPlayer player) {
		if (getCustomer().isPresent()) return;
		Spigot13MerchantUtils.openGUI(player, this);
	}

	@Override
	public List<WSTrade> getTrades() {
		if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD))
			return getHandled().getRecipes().stream().map(Spigot13Trade::new).collect(Collectors.toList());
		else return Spigot13MerchantUtils.getTrades(this);
	}

	@Override
	public void setTrades(List<WSTrade> trades) {
		if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD))
			getHandled().setRecipes(trades.stream().map(trade -> ((Spigot13Trade) trade).getHandled()).collect(Collectors.toList()));
		else Spigot13MerchantUtils.setTrades(this, trades);
	}

	@Override
	public void addTrade(WSTrade trade) {
		if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD)) {
			List<org.bukkit.inventory.MerchantRecipe> recipes = new ArrayList<>(getHandled().getRecipes());
			recipes.add(((Spigot13Trade) trade).getHandled());
			getHandled().setRecipes(recipes);
		} else {
			List<WSTrade> trades = Spigot13MerchantUtils.getTrades(this);
			trades.add(trade);
			Spigot13MerchantUtils.setTrades(this, trades);
		}
	}

	@Override
	public void removeTrade(WSTrade trade) {
		if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD))
			getHandled().setRecipes(getHandled().getRecipes().stream().filter(target -> target.equals(((Spigot13Trade) trade).getHandled())).collect(Collectors.toList()));
		else {
			List<WSTrade> trades = Spigot13MerchantUtils.getTrades(this);
			trades.remove(trade);
			Spigot13MerchantUtils.setTrades(this, trades);
		}
	}

	@Override
	public void clearTrades() {
		if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD)) getHandled().setRecipes(new ArrayList<>());
		else Spigot13MerchantUtils.setTrades(this, new ArrayList<>());
	}

	@Override
	public Villager getHandled() {
		return (Villager) super.getHandled();
	}

}

package com.degoos.wetsponge.resource.spigot;

import com.degoos.wetsponge.user.Spigot13GameProfile;
import com.degoos.wetsponge.user.WSGameProfile;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import org.bukkit.Material;
import org.bukkit.block.Skull;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import java.net.URL;
import java.util.Base64;
import java.util.UUID;

public class Spigot13SkullBuilder {

	public String getTexture(GameProfile gameProfile) {
		return gameProfile.getProperties().get("textures").stream().findAny().map(Property::getValue).orElse(null);
	}

	public static void setTexture(URL texture, WSGameProfile profile) {
		Spigot13SkullBuilder.setTexture(
				profile == null ? Spigot13SkullBuilder.getEmptyGameProfile() : ((Spigot13GameProfile) profile).getHandled(), Spigot13SkullBuilder.getTexture(texture));

	}

	public static void setTexture(String texture, WSGameProfile profile) {
		Spigot13SkullBuilder.setTexture(profile == null ? Spigot13SkullBuilder.getEmptyGameProfile() : ((Spigot13GameProfile) profile).getHandled(), texture);
	}

	public static GameProfile setTexture(GameProfile gameProfile, String texture) {
		if (texture == null) gameProfile.getProperties().removeAll("textures");
		else {
			gameProfile.getProperties().put("textures", new Property("textures", new String(texture.getBytes())));
		}
		return gameProfile;
	}

	public static String getTexture(URL url) {
		if (url == null) return null;
		return new String(Base64.getEncoder().encode(String.format("{\"textures\":{\"SKIN\":{\"url\":\"%s\"}}}", url.toExternalForm()).getBytes()));
	}

	public static String getTexture(String url) {
		if (url == null) return null;
		return new String(Base64.getEncoder().encode(String.format("{\"textures\":{\"SKIN\":{\"url\":\"%s\"}}}", url).getBytes()));
	}

	public static GameProfile getGameProfileByName(String name) {
		return new GameProfile(UUID.randomUUID(), name);
	}

	public static WSGameProfile getWSGameProfileByName(String name) {
		return new Spigot13GameProfile(getGameProfile(name));
	}

	public static GameProfile getEmptyGameProfile() {
		return new GameProfile(UUID.randomUUID(), null);
	}

	public static void injectGameProfile(Object object, GameProfile gameProfile) {
		try {
			ReflectionUtils.setFirstObject(object.getClass(), GameProfile.class, object, gameProfile);
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	public static GameProfile getGameProfile(Object object) {
		return ReflectionUtils.getFirstObjectOrElse(object.getClass(), GameProfile.class, object, getEmptyGameProfile());
	}

	//ITEMS

	public static ItemStack createItemStackByUnknownFormat(String string) {
		if (string.toLowerCase().startsWith("http:") || string.toLowerCase().startsWith("https:")) return createItemStackByTexture(getTexture(string));
		else if (string.length() <= 16) return createItemStackByPlayerName(string);
		else return createItemStackByTexture(string);
	}

	public static ItemStack createItemStackByTexture(String texture) {
		ItemStack itemStack = new ItemStack(Material.PLAYER_HEAD, 1, (short) 3);
		SkullMeta meta = (SkullMeta) itemStack.getItemMeta();
		injectGameProfile(meta, setTexture(getGameProfile(meta), texture));
		itemStack.setItemMeta(meta);
		return itemStack;
	}

	public static ItemStack createItemStackByURL(URL url) {
		ItemStack itemStack = new ItemStack(Material.PLAYER_HEAD, 1, (short) 3);
		SkullMeta meta = (SkullMeta) itemStack.getItemMeta();
		injectGameProfile(meta, setTexture(getGameProfile(meta), getTexture(url)));
		itemStack.setItemMeta(meta);
		return itemStack;
	}

	public static ItemStack createItemStackByPlayerName(String name) {
		//todo: check if working
		ItemStack itemStack = new ItemStack(Material.PLAYER_HEAD, 1);
		SkullMeta meta = (SkullMeta) itemStack.getItemMeta();
		meta.setOwner(name);
		injectGameProfile(meta, getGameProfileByName(name));
		itemStack.setItemMeta(meta);
		return itemStack;
	}

	//ITEM UPDATE

	public static ItemStack updateItemStackByUnknownFormat(ItemStack itemStack, String string) {
		if (string.toLowerCase().startsWith("http:") || string.toLowerCase().startsWith("https:")) return updateItemStackByTexture(itemStack, getTexture(string));
		else if (string.length() <= 16) return updateItemStackByPlayerName(itemStack, string);
		else return updateItemStackByTexture(itemStack, string);
	}

	public static ItemStack updateItemStackByTexture(ItemStack itemStack, String texture) {
		SkullMeta meta = (SkullMeta) itemStack.getItemMeta();
		injectGameProfile(meta, setTexture(getGameProfile(meta), texture));
		itemStack.setItemMeta(meta);
		return itemStack;
	}

	public static ItemStack updateItemStackByURL(ItemStack itemStack, URL url) {
		SkullMeta meta = (SkullMeta) itemStack.getItemMeta();
		injectGameProfile(meta, setTexture(getGameProfile(meta), getTexture(url)));
		itemStack.setItemMeta(meta);
		return itemStack;
	}

	public static ItemStack updateItemStackByPlayerName(ItemStack itemStack, String name) {
		SkullMeta meta = (SkullMeta) itemStack.getItemMeta();
		injectGameProfile(meta, getGameProfileByName(name));
		itemStack.setItemMeta(meta);
		return itemStack;
	}

	//BLOCKS

	public static void updateSkullByUnknownFormat(Skull skull, String string) {
		if (string.toLowerCase().startsWith("http:") || string.toLowerCase().startsWith("https:")) updateSkullByTexture(skull, getTexture(string));
		else if (string.length() <= 16) updateSkullByPlayerName(skull, string);
		else updateSkullByTexture(skull, string);
	}

	public static void updateSkullByTexture(Skull skull, String texture) {
		injectGameProfile(skull, setTexture(getGameProfile(skull), texture));
	}

	public static void updateSkullByURL(Skull skull, URL url) {
		injectGameProfile(skull, setTexture(getGameProfile(skull), getTexture(url)));
	}

	public static void updateSkullByPlayerName(Skull skull, String name) {
		injectGameProfile(skull, getGameProfileByName(name));
	}
}

package com.degoos.wetsponge.block.tileentity;

import com.degoos.wetsponge.block.Spigot13Block;
import com.degoos.wetsponge.text.WSText;
import org.bukkit.Nameable;

public class Spigot13TileEntityNameable extends Spigot13TileEntity implements WSTileEntityNameable {


    public Spigot13TileEntityNameable(Spigot13Block block) {
        super(block);
    }

    @Override
    public WSText getCustomName() {
        return WSText.getByFormattingText(getHandled().getCustomName());
    }

    @Override
    public void setCustomName(WSText customName) {
        if (customName == null) getHandled().setCustomName(null);
        else getHandled().setCustomName(customName.toFormattingText());
        update();
    }

    @Override
    public Nameable getHandled() {
        return (Nameable) super.getHandled();
    }

}

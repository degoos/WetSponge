package com.degoos.wetsponge.text.action.click;

import com.degoos.wetsponge.text.action.Spigot13TextAction;
import net.md_5.bungee.api.chat.ClickEvent;

public class Spigot13ClickAction extends Spigot13TextAction implements WSClickAction {

    public static Spigot13ClickAction of(ClickEvent event) {
        switch (event.getAction()) {
            case CHANGE_PAGE:
                return new Spigot13ChangePageAction(event);
            case OPEN_URL:
            case OPEN_FILE:
                return new Spigot13OpenURLAction(event);
            case RUN_COMMAND:
                return new Spigot13RunCommandAction(event);
            case SUGGEST_COMMAND:
                return new Spigot13SuggestCommandAction(event);
            default:
                return new Spigot13ClickAction(event);
        }
    }


    private ClickEvent clickEvent;


    public Spigot13ClickAction(ClickEvent clickEvent) {
        this.clickEvent = clickEvent;
    }

    @Override
    public ClickEvent getHandled() {
        return clickEvent;
    }
}

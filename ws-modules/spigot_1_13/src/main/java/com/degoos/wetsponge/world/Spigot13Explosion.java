package com.degoos.wetsponge.world;

import com.degoos.wetsponge.entity.explosive.WSExplosive;
import com.degoos.wetsponge.util.Validate;

import javax.annotation.Nullable;
import java.util.Optional;

public class Spigot13Explosion implements WSExplosion {

	private WSExplosive explosive;
	private WSLocation location;
	private float radius;
	private boolean canCauseFire, shouldPlaySmoke, shouldBreakBlocks, shouldDamageEntities;

	public Spigot13Explosion(WSExplosive explosive, WSLocation location, float radius, boolean canCauseFire, boolean shouldPlaySmoke, boolean shouldBreakBlocks,
							 boolean shouldDamageEntities) {
		Validate.notNull(location, "Location cannot be null!");
		this.explosive = explosive;
		this.location = location;
		this.radius = radius;
		this.canCauseFire = canCauseFire;
		this.shouldPlaySmoke = shouldPlaySmoke;
		this.shouldBreakBlocks = shouldBreakBlocks;
		this.shouldDamageEntities = shouldDamageEntities;
	}

	@Override
	public Optional<WSExplosive> getSourceExplosive() {
		return Optional.ofNullable(explosive);
	}

	@Override
	public float getRadius() {
		return radius;
	}

	@Override
	public boolean canCauseFire() {
		return canCauseFire;
	}

	@Override
	public boolean shouldPlaySmoke() {
		return shouldPlaySmoke;
	}

	@Override
	public boolean shouldBreakBlocks() {
		return shouldBreakBlocks;
	}

	@Override
	public boolean shouldDamageEntities() {
		return shouldDamageEntities;
	}

	@Override
	public Builder toBuilder() {
		return null;
	}

	@Override
	public Object getHandler() {
		return null;
	}

	@Override
	public WSLocation getLocation() {
		return location.clone();
	}

	@Override
	public WSWorld getWorld() {
		return getLocation().getWorld();
	}

	public static class Builder implements WSExplosion.Builder {

		private WSExplosive explosive = null;
		private WSLocation location = null;
		private float radius;
		private boolean canCauseFire, shouldPlaySmoke, shouldBreakBlocks, shouldDamageEntities;

		@Override
		public WSExplosion.Builder location(WSLocation location) {
			this.location = location;
			return this;
		}

		@Override
		public WSExplosion.Builder sourceExplosive(@Nullable WSExplosive explosive) {
			this.explosive = explosive;
			return this;
		}

		@Override
		public WSExplosion.Builder radius(float radius) {
			this.radius = radius;
			return this;
		}

		@Override
		public WSExplosion.Builder canCauseFire(boolean canCauseFire) {
			this.canCauseFire = canCauseFire;
			return this;
		}

		@Override
		public WSExplosion.Builder shouldDamageEntities(boolean shouldDamageEntities) {
			this.shouldDamageEntities = shouldDamageEntities;
			return this;
		}

		@Override
		public WSExplosion.Builder shouldPlaySmoke(boolean shouldPlaySmoke) {
			this.shouldPlaySmoke = shouldPlaySmoke;
			return this;
		}

		@Override
		public WSExplosion.Builder shouldBreakBlocks(boolean shouldBreakBlocks) {
			this.shouldBreakBlocks = shouldBreakBlocks;
			return this;
		}

		@Override
		public WSExplosion build() {
			return new Spigot13Explosion(explosive, location, radius, canCauseFire, shouldPlaySmoke, shouldBreakBlocks, shouldDamageEntities);
		}
	}
}

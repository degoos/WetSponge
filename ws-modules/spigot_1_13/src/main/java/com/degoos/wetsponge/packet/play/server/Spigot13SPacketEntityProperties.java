package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.entity.Spigot13Entity;
import com.degoos.wetsponge.entity.living.WSLivingEntity;
import com.degoos.wetsponge.packet.Spigot13Packet;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.Validate;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;
import com.degoos.wetsponge.util.reflection.Spigot13HandledUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class Spigot13SPacketEntityProperties extends Spigot13Packet implements WSSPacketEntityProperties {

	private int entityId;
	private List<Object> entries = new ArrayList<>();
	private boolean changed;

	public Spigot13SPacketEntityProperties(WSLivingEntity entity) throws IllegalAccessException, InstantiationException {
		super(NMSUtils.getNMSClass("PacketPlayOutUpdateAttributes").newInstance());
		setPropertiesOf(entity);
		refresh();
	}

	public Spigot13SPacketEntityProperties(Object packet) {
		super(packet);
		refresh();
	}

	@Override
	public int getEntityId() {
		return entityId;
	}

	@Override
	public void setEntityId(int entityId) {
		if (entityId != this.entityId) {
			this.entityId = entityId;
			changed = true;
		}
	}

	@Override
	public void setPropertiesOf(WSLivingEntity entity) {
		Validate.notNull(entity, "Entity cannot be null!");
		this.entries.clear();

		try {
			Object entityHandle = Spigot13HandledUtils.getEntityHandle(((Spigot13Entity) entity).getHandled());
			Object attributeMap = ReflectionUtils.invokeMethod(entityHandle, "getAttributeMap");
			Collection<?> attributes = (Collection<?>) ReflectionUtils.invokeMethod(attributeMap, "a");
			attributes.forEach(attribute -> {
				try {
					Object iAttribute = ReflectionUtils.invokeMethod(attribute, "getAttribute");
					entries.add(getHandler().getClass().getDeclaredClasses()[0].getConstructors()[0]
						.newInstance(getHandler(), ReflectionUtils.invokeMethod(iAttribute, "getName"), ReflectionUtils.invokeMethod(attribute, "b"), ReflectionUtils
							.invokeMethod(attribute, "c")));
				} catch (Exception ex) {
					InternalLogger.printException(ex, "An error has occurred while WetSponge was setting the properties of a packet!");
				}
			});
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was setting the properties of a packet!");
		}
		changed = true;
	}


	@Override
	public void update() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));
			fields[0].setInt(getHandler(), entityId);
			Collection<?> snapshots = (Collection<?>) fields[1].get(getHandler());
			snapshots.clear();
			Method addMethod = ReflectionUtils.getMethodByName(List.class, "add");
			for (Object o : entries) addMethod.invoke(snapshots, o);
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void refresh() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));
			this.entityId = fields[0].getInt(getHandler());
			List<?> snapshots = (List<?>) fields[1].get(getHandler());
			entries.clear();
			entries.addAll(snapshots);
		} catch (Throwable ex) {
			ex.printStackTrace();
		}

	}

	@Override
	public boolean hasChanged() {
		return changed;
	}
}

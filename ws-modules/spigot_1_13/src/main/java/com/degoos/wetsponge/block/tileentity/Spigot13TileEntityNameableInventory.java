package com.degoos.wetsponge.block.tileentity;

import com.degoos.wetsponge.block.Spigot13Block;
import com.degoos.wetsponge.text.WSText;
import org.bukkit.Nameable;
import org.bukkit.inventory.InventoryHolder;

public class Spigot13TileEntityNameableInventory extends Spigot13TileEntityInventory implements WSTileEntityNameableInventory {


    public Spigot13TileEntityNameableInventory(Spigot13Block block) {
        super(block);
    }

    @Override
    public WSText getCustomName() {
        return WSText.getByFormattingText(((Nameable) getHandled()).getCustomName());
    }

    @Override
    public void setCustomName(WSText customName) {
        ((Nameable) getHandled()).setCustomName(customName.toFormattingText());
        update();
    }

    @Override
    public InventoryHolder getHandled() {
        return super.getHandled();
    }

}

package com.degoos.wetsponge.parser.player;

import com.degoos.wetsponge.entity.living.player.Spigot13Player;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.Optional;
import java.util.UUID;

public class Spigot13PlayerParser {

	protected static Optional<WSPlayer> checkPlayer(String name) {
		return Optional.ofNullable(Bukkit.getServer().getPlayer(name)).map(Spigot13Player::new);
	}

	protected static Optional<WSPlayer> checkPlayer(UUID uuid) {
		return Optional.ofNullable(Bukkit.getServer().getPlayer(uuid)).map(Spigot13Player::new);
	}

	protected static WSPlayer newInstance(Object player) {
		return new Spigot13Player((Player) player);
	}
}

package com.degoos.wetsponge.text;

import com.degoos.wetsponge.text.translation.Spigot13Translation;
import com.degoos.wetsponge.text.translation.WSTranslation;
import net.md_5.bungee.api.chat.TranslatableComponent;

public class Spigot13TranslatableText extends Spigot13Text implements WSTranslatableText {

	private WSTranslation translation;

	public Spigot13TranslatableText(TranslatableComponent text) {
		super(text);
		translation = new Spigot13Translation(text.getTranslate());
	}

	public Spigot13TranslatableText(WSTranslation translation, Object... objects) {
		super(new TranslatableComponent(translation.getId(), objects));
		this.translation = translation;
	}

	@Override
	public WSTranslation getTranslation() {
		return translation;
	}
}

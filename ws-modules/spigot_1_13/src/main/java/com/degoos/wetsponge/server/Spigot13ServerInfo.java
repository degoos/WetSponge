package com.degoos.wetsponge.server;


import com.degoos.wetsponge.resource.B64;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.util.InternalLogger;
import com.google.common.io.Files;
import org.bukkit.Server;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Optional;

public class Spigot13ServerInfo implements WSServerInfo {

	private Server server;


	public Spigot13ServerInfo(Server server) {
		this.server = server;
	}


	@Override
	public int getOnlinePlayers() {
		return server.getOnlinePlayers().size();
	}


	@Override
	public int getMaxPlayers() {
		return server.getMaxPlayers();
	}


	@Override
	public int getIdleTimeout() {
		return server.getIdleTimeout();
	}


	@Override
	public void setIdleTimeout(int idleTimeout) {
		server.setIdleTimeout(idleTimeout);
	}


	@Override
	public boolean isOnlineMode() {
		return server.getOnlineMode();
	}


	@Override
	public boolean isFull() {
		return getMaxPlayers() <= getOnlinePlayers();
	}


	@Override
	public boolean hasWhiteList() {
		return server.hasWhitelist();
	}


	@Override
	public String getServerName() { return server.getName(); }


	@Override
	public WSText getMotd() {
		return WSText.getByFormattingText(server.getMotd());
	}


	@Override
	public String getBase64ServerIcon() {
		String icon = "";

		File file = new File("server-icon.png");
		if (file.isFile()) {
			try {
				icon = "data:image/png;base64," + B64.encode(Files.toByteArray(file));
			} catch (IOException ex) {
				InternalLogger.printException(ex, "An error has occurred while WetSponge was trying to get the server icon!");
			}
		}

		return icon;
	}

	@Override
	public Optional<WSFavicon> getServerIcon() {
		try {
			File file = new File("server-icon.png");
			if (file.isFile()) return Optional.of(new Spigot13Favicon(ImageIO.read(file)));
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was trying to get the server icon!");
			return Optional.empty();
		}
		return Optional.empty();
	}

	@Override
	public Optional<InetSocketAddress> getBoundAddress() {
		return Optional.of(new InetSocketAddress(server.getIp(), server.getPort()));
	}
}

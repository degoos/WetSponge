package com.degoos.wetsponge.entity.living;


import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.parser.entity.Spigot13EntityParser;
import com.degoos.wetsponge.util.reflection.Spigot13EntityUtils;
import org.bukkit.entity.Creature;

import java.util.Optional;

public class Spigot13Creature extends Spigot13LivingEntity implements WSCreature {


	public Spigot13Creature(Creature entity) {
		super(entity);
	}


	@Override
	public void setAI(boolean ai) {
		if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD)) getHandled().setAI(ai);
		else Spigot13EntityUtils.setAI(getHandled(), ai);
	}


	@Override
	public boolean hasAI() {
		if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD)) return getHandled().hasAI();
		else return Spigot13EntityUtils.hasAI(getHandled());
	}


	@Override
	public Optional<WSEntity> getTarget() {
		return Optional.ofNullable(getHandled().getTarget()).map(Spigot13EntityParser::getWSEntity);
	}


	@Override
	public void setTarget(WSEntity entity) {
		if (entity == null) getHandled().setTarget(null);
		else if (entity instanceof Spigot13LivingEntity) getHandled().setTarget(((Spigot13LivingEntity) entity).getHandled());
	}


	@Override
	public Creature getHandled() {
		return (Creature) super.getHandled();
	}
}

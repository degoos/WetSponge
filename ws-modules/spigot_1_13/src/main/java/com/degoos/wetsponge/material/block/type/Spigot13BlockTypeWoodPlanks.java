package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumWoodType;
import com.degoos.wetsponge.material.block.Spigot13BlockType;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.block.data.BlockData;

import java.util.Objects;

public class Spigot13BlockTypeWoodPlanks extends Spigot13BlockType implements WSBlockTypeWoodPlanks {

	private EnumWoodType woodType;

	public Spigot13BlockTypeWoodPlanks(EnumWoodType woodType) {
		super(5, "minecraft:planks", "minecraft:planks", 64);
		Validate.notNull(woodType, "Wood type cannot be null!");
		this.woodType = woodType;
	}

	@Override
	public String getNewStringId() {
		switch (getWoodType()) {
			case SPRUCE:
				return "minecraft:spruce_planks";
			case BIRCH:
				return "minecraft:birch_planks";
			case JUNGLE:
				return "minecraft:jungle_planks";
			case ACACIA:
				return "minecraft:acacia_planks";
			case DARK_OAK:
				return "minecraft:dark_oak_planks";
			case OAK:
			default:
				return "minecraft:oak_planks";
		}
	}

	@Override
	public EnumWoodType getWoodType() {
		return woodType;
	}

	@Override
	public void setWoodType(EnumWoodType woodType) {
		Validate.notNull(woodType, "Wood type cannot be null");
		this.woodType = woodType;
	}

	@Override
	public Spigot13BlockTypeWoodPlanks clone() {
		return new Spigot13BlockTypeWoodPlanks(woodType);
	}

	@Override
	public Spigot13BlockTypeWoodPlanks readBlockData(BlockData blockData) {
		super.readBlockData(blockData);
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeWoodPlanks that = (Spigot13BlockTypeWoodPlanks) o;
		return woodType == that.woodType;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), woodType);
	}
}

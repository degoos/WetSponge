package com.degoos.wetsponge.nbt;


import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;

import java.lang.reflect.InvocationTargetException;

public class Spigot13NBTTagLongArray extends Spigot13NBTBase implements WSNBTTagLongArray {

	private static final Class<?> clazz = NMSUtils.getNMSClass("NBTTagLongArray");

	public Spigot13NBTTagLongArray(long[] longs) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
		this(clazz.getConstructor(long[].class).newInstance((Object) longs));
	}

	public Spigot13NBTTagLongArray(Object nbtTagByteArray) {
		super(nbtTagByteArray);
	}

	@Override
	public long[] getLongArray() {
		try {
			return ReflectionUtils.getFirstObject(clazz, long[].class, getHandled());
		} catch (Exception e) {
			InternalLogger.printException(e, "An error has occurred while WetSponge was getting the value of a NBTTag!");
			return new long[0];
		}
	}


	@Override
	public WSNBTTagLongArray copy() {
		try {
			long[] values = getLongArray();
			long[] var1 = new long[values.length];
			System.arraycopy(values, 0, var1, 0, values.length);
			return new Spigot13NBTTagLongArray(var1);
		} catch (Exception e) {
			InternalLogger.printException(e, "An exception has occurred while WetSponge was cloning a NBTTag!");
			return null;
		}
	}

	@Override
	public Object getHandled() {
		return super.getHandled();
	}
}

package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.enums.block.EnumBlockTypeBisectedHalf;
import com.degoos.wetsponge.enums.block.EnumBlockTypeDoorHinge;
import com.degoos.wetsponge.material.block.Spigot13BlockTypeDirectional;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.block.data.Bisected;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.Door;

import java.util.Objects;
import java.util.Set;

public class Spigot13BlockTypeDoor extends Spigot13BlockTypeDirectional implements WSBlockTypeDoor {

	private EnumBlockTypeDoorHinge hinge;
	private EnumBlockTypeBisectedHalf half;
	private boolean open, powered;

	public Spigot13BlockTypeDoor(int numericalId, String oldStringId, String newStringId, int maxStackSize, EnumBlockFace facing, Set<EnumBlockFace> faces,
								 EnumBlockTypeDoorHinge hinge, EnumBlockTypeBisectedHalf half, boolean open, boolean powered) {
		super(numericalId, oldStringId, newStringId, maxStackSize, facing, faces);
		Validate.notNull(hinge, "Hinge cannot be null!");
		Validate.notNull(half, "Half cannot be null!");
		this.hinge = hinge;
		this.half = half;
		this.open = open;
		this.powered = powered;
	}

	@Override
	public EnumBlockTypeDoorHinge getHinge() {
		return hinge;
	}

	@Override
	public void setHinge(EnumBlockTypeDoorHinge hinge) {
		Validate.notNull(hinge, "Hinge cannot be null!");
		this.hinge = hinge;
	}

	@Override
	public EnumBlockTypeBisectedHalf getHalf() {
		return half;
	}

	@Override
	public void setHalf(EnumBlockTypeBisectedHalf half) {
		Validate.notNull(half, "Half cannot be null!");
		this.half = half;
	}

	@Override
	public boolean isOpen() {
		return open;
	}

	@Override
	public void setOpen(boolean open) {
		this.open = open;
	}

	@Override
	public boolean isPowered() {
		return powered;
	}

	@Override
	public void setPowered(boolean powered) {
		this.powered = powered;
	}

	@Override
	public Spigot13BlockTypeDoor clone() {
		return new Spigot13BlockTypeDoor(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), getFacing(), getFaces(), hinge, half, open, powered);
	}

	@Override
	public BlockData toBlockData() {
		BlockData blockData = super.toBlockData();
		if (blockData instanceof Door) {
			((Door) blockData).setPowered(powered);
			((Door) blockData).setOpen(open);
			((Door) blockData).setHinge(Door.Hinge.valueOf(hinge.name()));
			((Door) blockData).setHalf(Bisected.Half.valueOf(half.name()));
		}
		return blockData;
	}

	@Override
	public Spigot13BlockTypeDirectional readBlockData(BlockData blockData) {
		super.readBlockData(blockData);
		if (blockData instanceof Door) {
			half = EnumBlockTypeBisectedHalf.valueOf(((Door) blockData).getHalf().name());
			hinge = EnumBlockTypeDoorHinge.valueOf(((Door) blockData).getHinge().name());
			open = ((Door) blockData).isOpen();
			powered = ((Door) blockData).isPowered();
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeDoor that = (Spigot13BlockTypeDoor) o;
		return open == that.open &&
				powered == that.powered &&
				hinge == that.hinge &&
				half == that.half;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), hinge, half, open, powered);
	}
}

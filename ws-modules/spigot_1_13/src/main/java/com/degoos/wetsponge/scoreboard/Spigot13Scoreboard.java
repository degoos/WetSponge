package com.degoos.wetsponge.scoreboard;

import com.degoos.wetsponge.enums.EnumCriteria;
import com.degoos.wetsponge.enums.EnumDisplaySlot;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

import javax.annotation.Nullable;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class Spigot13Scoreboard implements WSScoreboard {

	private Scoreboard scoreboard;

	public Spigot13Scoreboard(Scoreboard scoreboard) {
		Validate.notNull(scoreboard, "Scoreboard cannot be null!");
		this.scoreboard = scoreboard;
	}


	@Override
	public Optional<WSObjective> getObjective(String name) {
		Validate.notNull(name, "Name cannot be null!");
		return Optional.ofNullable(scoreboard.getObjective(name)).map(Spigot13Objective::new);
	}

	@Override
	public Optional<WSObjective> getObjective(EnumDisplaySlot displaySlot) {
		Validate.notNull(displaySlot, "DisplaySlot cannot be null!");
		return Optional.ofNullable(scoreboard.getObjective(DisplaySlot.valueOf(displaySlot.getSpigotName()))).map(Spigot13Objective::new);
	}

	@Override
	public WSObjective getOrCreateObjective(String name, @Nullable WSText displayName, EnumCriteria criteria) {
		Validate.notNull(criteria, "Criteria cannot be null!");
		Validate.notNull(name, "Name cannot be null!");
		Optional<WSObjective> optional = getObjective(name);
		if (optional.isPresent()) return optional.get();
		Spigot13Objective objective = new Spigot13Objective(scoreboard.registerNewObjective(name, criteria.getSpigotName()));
		objective.getHandled().setDisplayName(displayName == null ? name : displayName.toFormattingText());
		return objective;
	}

	@Override
	public void updateDisplaySlot(@Nullable WSObjective objective, EnumDisplaySlot displaySlot) {
		Validate.notNull(displaySlot, "DisplaySlot cannot be null!");
		if (objective == null) scoreboard.clearSlot(DisplaySlot.valueOf(displaySlot.getSpigotName()));
		else {
			if(objective instanceof WSUpdatableObjective) objective = ((WSUpdatableObjective) objective).getObjective();
			((Spigot13Objective) objective).getHandled().setDisplaySlot(DisplaySlot.valueOf(displaySlot.getSpigotName()));
		}
	}

	@Override
	public Set<WSObjective> getObjectives() {
		return scoreboard.getObjectives().stream().map(Spigot13Objective::new).collect(Collectors.toSet());
	}

	@Override
	public void removeObjective(WSObjective objective) {
		Validate.notNull(objective, "Objective cannot be null!");
		if(objective instanceof WSUpdatableObjective) objective = ((WSUpdatableObjective) objective).getObjective();
		if (getObjective(objective.getName()).isPresent()) ((Spigot13Objective) objective).getHandled().unregister();
	}

	@Override
	public void clearObjectives() {
		new HashSet<>(scoreboard.getObjectives()).forEach(Objective::unregister);
	}

	@Override
	public Optional<WSTeam> getTeam(String name) {
		Validate.notNull(name, "Name cannot be null!");
		return Optional.ofNullable(scoreboard.getTeam(name)).map(Spigot13Team::new);
	}

	@Override
	public boolean hasTeam(String name) {
		Validate.notNull(name, "Name cannot be null!");
		return getTeam(name).isPresent();
	}

	@Override
	public WSTeam getOrCreateTeam(String name) {
		Validate.notNull(name, "Name cannot be null!");
		Optional<WSTeam> optional = getTeam(name);
		return optional.orElseGet(() -> new Spigot13Team(scoreboard.registerNewTeam(name)));
	}

	@Override
	public Set<WSTeam> getTeams() {
		return scoreboard.getTeams().stream().map(Spigot13Team::new).collect(Collectors.toSet());
	}

	@Override
	public Optional<WSTeam> getMemberTeam(WSText member) {
		Validate.notNull(member, "Member cannot be null!");
		return getTeams().stream().filter(team -> team.hasMember(member)).findAny();
	}

	@Override
	public boolean unregisterTeam(WSTeam team) {
		Validate.notNull(team, "Team cannot be null!");
		return team.unregister();
	}

	@Override
	public void unregisterAllTeams() {
		getTeams().forEach(WSTeam::unregister);
	}

	@Override
	public Scoreboard getHandled() {
		return scoreboard;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Spigot13Scoreboard that = (Spigot13Scoreboard) o;

		return scoreboard.equals(that.scoreboard);
	}

	@Override
	public int hashCode() {
		return scoreboard.hashCode();
	}
}

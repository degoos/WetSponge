package com.degoos.wetsponge.listener.spigot;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.entity.living.player.Spigot13Player;
import com.degoos.wetsponge.event.message.WSChatEvent;
import com.degoos.wetsponge.parser.player.PlayerParser;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import java.lang.reflect.Field;
import java.util.stream.Collectors;

public class Spigot13ChatListener implements Listener {

	private Field formatField;

	{
		try {
			formatField = ReflectionUtils.setAccessible(AsyncPlayerChatEvent.class.getDeclaredField("format"));
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onChat(AsyncPlayerChatEvent event) {
		try {
			event.setFormat(event.getFormat());
			WSChatEvent wetSpongeEvent = new WSChatEvent(PlayerParser.getOrCreatePlayer(event.getPlayer(), event.getPlayer().getUniqueId()), WSText.getByFormattingText(event.getFormat()), WSText.getByFormattingText(event.getMessage()), WSText.of("<" + event.getPlayer().getName() + "> "), WSText
				.getByFormattingText(event.getMessage()), WSText.empty(), true, event.getRecipients().stream().map(target -> PlayerParser.getOrCreatePlayer(target, target.getUniqueId())).collect(Collectors.toSet()));
			WetSponge.getEventManager().callEvent(wetSpongeEvent);
			if (wetSpongeEvent.hasTextChanged()) {
				formatField.set(event, wetSpongeEvent.getMessage().toFormattingText());
				event.setMessage(wetSpongeEvent.getBody().toFormattingText());
			}
			event.getRecipients().clear();
			event.getRecipients().addAll(wetSpongeEvent.getTargets().stream().map(t -> ((Spigot13Player) t).getHandled()).collect(Collectors.toSet()));
			event.setCancelled(wetSpongeEvent.isCancelled());
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Spigot-AsyncPlayerChatEvent!");
		}
	}
}

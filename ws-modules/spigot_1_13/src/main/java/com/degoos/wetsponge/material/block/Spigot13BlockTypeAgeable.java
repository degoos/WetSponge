package com.degoos.wetsponge.material.block;

import com.degoos.wetsponge.util.Spigot13MaterialUtils;
import org.bukkit.Material;
import org.bukkit.block.data.Ageable;
import org.bukkit.block.data.BlockData;

import java.util.Objects;

public class Spigot13BlockTypeAgeable extends Spigot13BlockType implements WSBlockTypeAgeable {

	private int age, maximumAge;

	public Spigot13BlockTypeAgeable(int numericalId, String oldStringId, String newStringId, int maxStackSize, int age, int maximumAge) {
		super(numericalId, oldStringId, newStringId, maxStackSize);
		this.age = age;
		this.maximumAge = maximumAge;
	}

	@Override
	public int getAge() {
		return age;
	}

	@Override
	public void setAge(int age) {
		this.age = Math.min(maximumAge, Math.max(0, age));
	}

	@Override
	public int getMaximumAge() {
		return maximumAge;
	}

	@Override
	public Spigot13BlockTypeAgeable clone() {
		return new Spigot13BlockTypeAgeable(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), age, maximumAge);
	}

	@Override
	public BlockData toBlockData() {
		Material material = Spigot13MaterialUtils.getByKey(getNewStringId()).orElse(null);
		if (material == null) return null;
		BlockData data = material.createBlockData();
		if (data instanceof Ageable) {
			((Ageable) data).setAge(age);
		}
		return data;
	}

	@Override
	public Spigot13BlockTypeAgeable readBlockData(BlockData blockData) {
		if (blockData instanceof Ageable) {
			age = ((Ageable) blockData).getAge();
			maximumAge = ((Ageable) blockData).getMaximumAge();
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeAgeable that = (Spigot13BlockTypeAgeable) o;
		return age == that.age &&
				maximumAge == that.maximumAge;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), age, maximumAge);
	}
}

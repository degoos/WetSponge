package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockTypeSandType;
import com.degoos.wetsponge.material.block.Spigot13BlockType;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.block.data.BlockData;

import java.util.Objects;

public class Spigot13BlockTypeSand extends Spigot13BlockType implements WSBlockTypeSand {

	private EnumBlockTypeSandType sandType;

	public Spigot13BlockTypeSand(EnumBlockTypeSandType sandType) {
		super(12, "minecraft:sand", "minecraft:sand", 64);
		Validate.notNull(sandType, "Sand type cannot be null!");
		this.sandType = sandType;
	}

	@Override
	public String getNewStringId() {
		return sandType == EnumBlockTypeSandType.NORMAL ? "minecraft:sand" : "minecraft:red_sand";
	}

	@Override
	public EnumBlockTypeSandType getSandType() {
		return sandType;
	}

	@Override
	public void setSandType(EnumBlockTypeSandType sandType) {
		Validate.notNull(sandType, "Sand type cannot be null!");
		this.sandType = sandType;
	}

	@Override
	public Spigot13BlockTypeSand clone() {
		return new Spigot13BlockTypeSand(sandType);
	}

	@Override
	public Spigot13BlockTypeSand readBlockData(BlockData blockData) {
		super.readBlockData(blockData);
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeSand that = (Spigot13BlockTypeSand) o;
		return sandType == that.sandType;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), sandType);
	}
}

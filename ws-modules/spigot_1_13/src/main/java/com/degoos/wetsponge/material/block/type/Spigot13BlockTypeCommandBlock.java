package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.Spigot13BlockTypeDirectional;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.CommandBlock;

import java.util.Objects;
import java.util.Set;

public class Spigot13BlockTypeCommandBlock extends Spigot13BlockTypeDirectional implements WSBlockTypeCommandBlock {

	private boolean conditional;

	public Spigot13BlockTypeCommandBlock(int numericalId, String oldStringId, String newStringId, int maxStackSize, EnumBlockFace facing, Set<EnumBlockFace> faces, boolean conditional) {
		super(numericalId, oldStringId, newStringId, maxStackSize, facing, faces);
		this.conditional = conditional;
	}

	@Override
	public boolean isConditional() {
		return conditional;
	}

	@Override
	public void setConditional(boolean conditional) {
		this.conditional = conditional;
	}

	@Override
	public Spigot13BlockTypeCommandBlock clone() {
		return new Spigot13BlockTypeCommandBlock(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), getFacing(), getFaces(), conditional);
	}

	@Override
	public BlockData toBlockData() {
		BlockData blockData = super.toBlockData();
		if (blockData instanceof CommandBlock) {
			((CommandBlock) blockData).setConditional(conditional);
		}
		return blockData;
	}

	@Override
	public Spigot13BlockTypeDirectional readBlockData(BlockData blockData) {
		super.readBlockData(blockData);
		if (blockData instanceof CommandBlock) {
			conditional = ((CommandBlock) blockData).isConditional();
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeCommandBlock that = (Spigot13BlockTypeCommandBlock) o;
		return conditional == that.conditional;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), conditional);
	}
}

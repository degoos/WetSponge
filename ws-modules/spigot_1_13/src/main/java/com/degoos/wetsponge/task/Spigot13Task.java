package com.degoos.wetsponge.task;


import com.degoos.wetsponge.SpigotWetSponge;
import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumTextColor;
import com.degoos.wetsponge.plugin.WSPlugin;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.util.InternalLogger;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.UUID;
import java.util.function.Consumer;

public class Spigot13Task implements WSTask {

	private BukkitRunnable task;
	private UUID uniqueId;
	private long times, timesExecuted;
	private WSPlugin plugin;
	private StackTraceElement caller;
	private boolean asynchronous, timer, instantaneous, later;
	private long delay, interval;


	public Spigot13Task(Runnable runnable) {
		Consumer<WSTask> consumer = (wsTask -> runnable.run());
		this.times = -1;
		this.timesExecuted = 0;
		caller = Thread.currentThread().getStackTrace()[4];
		task = new BukkitRunnable() {
			@Override
			public void run() {
				try {
					if (times > -1) {
						if (times <= timesExecuted) {
							cancel();
							return;
						}
					}

					WSPlugin caller = WetSponge.getTimings().getAssignedPlugin();

					WetSponge.getTimings().assignPluginToThread(plugin);
					WetSponge.getTimings().startTiming(Spigot13Task.this);

					consumer.accept(Spigot13Task.this);

					WetSponge.getTimings().stopTiming();
					WetSponge.getTimings().assignPluginToThread(caller);

					Spigot13Task.this.timesExecuted++;

				} catch (Throwable ex) {
					InternalLogger.printException(ex, WSText.builder("An error has occurred while WetSponge was executing the task ")
							.append(WSText.of(String.valueOf(task.getTaskId()), EnumTextColor.YELLOW)).append(WSText.of("!")).build());
				}
			}
		};
		uniqueId = UUID.randomUUID();
	}


	public Spigot13Task(Consumer<WSTask> consumer) {
		this.times = -1;
		this.timesExecuted = 0;
		caller = Thread.currentThread().getStackTrace()[4];
		task = new BukkitRunnable() {
			@Override
			public void run() {
				try {
					if (times > -1) {
						if (times <= timesExecuted) {
							cancel();
							return;
						}
					}

					WSPlugin caller = WetSponge.getTimings().getAssignedPlugin();

					WetSponge.getTimings().assignPluginToThread(plugin);
					WetSponge.getTimings().startTiming(Spigot13Task.this);

					consumer.accept(Spigot13Task.this);

					WetSponge.getTimings().stopTiming();
					WetSponge.getTimings().assignPluginToThread(caller);

					Spigot13Task.this.timesExecuted++;

				} catch (Throwable ex) {
					InternalLogger.printException(ex, WSText.builder("An error has occurred while WetSponge was executing the task ")
							.append(WSText.of(String.valueOf(task.getTaskId()), EnumTextColor.YELLOW)).append(WSText.of("!")).build());
				}
			}
		};
		uniqueId = UUID.randomUUID();
	}


	@Override
	public void run(WSPlugin plugin) {
		this.plugin = plugin;
		this.asynchronous = false;
		this.timer = false;
		this.later = false;
		this.instantaneous = true;

		task.run();
	}


	@Override
	public void runAsynchronously(WSPlugin plugin) {
		this.plugin = plugin;
		this.asynchronous = true;
		this.timer = false;
		this.later = false;
		this.instantaneous = true;

		task.runTaskAsynchronously(SpigotWetSponge.getInstance());
	}


	@Override
	public void runTaskLater(long delay, WSPlugin plugin) {
		if (!WSTaskPool.addTask(plugin, this)) return;
		this.plugin = plugin;
		this.asynchronous = false;
		this.timer = false;
		this.later = true;
		this.instantaneous = false;
		this.delay = delay;
		task.runTaskLater(SpigotWetSponge.getInstance(), delay);
	}


	@Override
	public void runTaskLaterAsynchronously(long delay, WSPlugin plugin) {
		if (!WSTaskPool.addTask(plugin, this)) return;
		this.plugin = plugin;
		this.asynchronous = true;
		this.timer = false;
		this.later = true;
		this.instantaneous = false;
		this.delay = delay;
		task.runTaskLaterAsynchronously(SpigotWetSponge.getInstance(), delay);
	}


	@Override
	public void runTaskTimer(long delay, long interval, WSPlugin plugin) {
		if (!WSTaskPool.addTask(plugin, this)) return;
		this.plugin = plugin;
		this.asynchronous = false;
		this.timer = true;
		this.later = false;
		this.instantaneous = false;
		this.delay = delay;
		this.interval = interval;
		task.runTaskTimer(SpigotWetSponge.getInstance(), delay, interval);
	}

	@Override
	public void runTaskTimer(long delay, long interval, long times, WSPlugin plugin) {
		this.times = times;
		this.plugin = plugin;
		this.asynchronous = false;
		this.timer = true;
		this.later = false;
		this.instantaneous = false;
		this.delay = delay;
		this.interval = interval;
		runTaskTimer(delay, interval, plugin);
	}


	@Override
	public void runTaskTimerAsynchronously(long delay, long interval, WSPlugin plugin) {
		if (!WSTaskPool.addTask(plugin, this)) return;
		this.plugin = plugin;
		this.asynchronous = true;
		this.timer = true;
		this.later = false;
		this.instantaneous = false;
		this.delay = delay;
		this.interval = interval;
		task.runTaskTimerAsynchronously(SpigotWetSponge.getInstance(), delay, interval);
	}

	@Override
	public void runTaskTimerAsynchronously(long delay, long interval, long times, WSPlugin plugin) {
		this.times = times;
		this.plugin = plugin;
		this.asynchronous = true;
		this.timer = true;
		this.later = false;
		this.instantaneous = false;
		this.delay = delay;
		this.interval = interval;
		runTaskTimerAsynchronously(delay, interval, plugin);
	}

	@Override
	public UUID getUniqueId() {
		return uniqueId;
	}

	@Override
	public StackTraceElement getCallerStackTraceElement() {
		return caller;
	}

	@Override
	public long getTimesExecuted() {
		return timesExecuted;
	}

	@Override
	public boolean isAsynchronous() {
		return asynchronous;
	}

	@Override
	public boolean isTimer() {
		return timer;
	}

	@Override
	public boolean isInstantaneous() {
		return instantaneous;
	}

	@Override
	public boolean isLater() {
		return later;
	}

	@Override
	public long getDelay() {
		return delay;
	}

	@Override
	public long getInterval() {
		return interval;
	}

	@Override
	public long getTimesToExecute() {
		return times;
	}

	@Override
	public WSPlugin getPlugin() {
		return plugin;
	}


	@Override
	public void cancel() {
		task.cancel();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Spigot13Task that = (Spigot13Task) o;

		return uniqueId.equals(that.uniqueId);
	}

	@Override
	public int hashCode() {
		return uniqueId.hashCode();
	}

	/*public Timing getTimingsHandler() {
		if (this.taskTimer == null) {
			this.taskTimer = WetSpongeTimings.getPluginTaskTimings(this, times);
		}
		return this.taskTimer;
	}*/
}

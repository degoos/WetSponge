package com.degoos.wetsponge.bar;

import com.degoos.wetsponge.entity.living.player.Spigot13Player;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.enums.EnumBossBarColor;
import com.degoos.wetsponge.enums.EnumBossBarOverlay;
import com.degoos.wetsponge.parser.player.PlayerParser;
import com.degoos.wetsponge.text.WSText;
import org.bukkit.Bukkit;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarFlag;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;

import java.util.Set;
import java.util.stream.Collectors;

public class Spigot13BossBar implements WSBossBar {

	private BossBar bossBar;

	public Spigot13BossBar(BossBar bossBar) {
		this.bossBar = bossBar;
	}

	@Override
	public Spigot13BossBar addPlayer(WSPlayer player) {
		bossBar.addPlayer(((Spigot13Player) player).getHandled());
		return this;
	}

	@Override
	public Spigot13BossBar removePlayer(WSPlayer player) {
		bossBar.removePlayer(((Spigot13Player) player).getHandled());
		return this;
	}

	@Override
	public Spigot13BossBar clearPlayers() {
		bossBar.getPlayers().forEach(bossBar::removePlayer);
		return this;
	}

	@Override
	public Set<WSPlayer> getPlayers() {
		return bossBar.getPlayers().stream().map(player -> PlayerParser.getOrCreatePlayer(player, player.getUniqueId())).collect(Collectors.toSet());
	}

	@Override
	public EnumBossBarColor getColor() {
		return EnumBossBarColor.getByName(bossBar.getColor().name()).orElse(EnumBossBarColor.PURPLE);
	}

	@Override
	public Spigot13BossBar setColor(EnumBossBarColor color) {
		bossBar.setColor(BarColor.valueOf(color.name()));
		return this;
	}

	@Override
	public EnumBossBarOverlay getOverlay() {
		return EnumBossBarOverlay.getBySpigotName(bossBar.getStyle().name()).orElse(EnumBossBarOverlay.PROGRESS);
	}

	@Override
	public Spigot13BossBar setOverlay(EnumBossBarOverlay overlay) {
		bossBar.setStyle(BarStyle.valueOf(overlay.getSpigotName()));
		return this;
	}

	@Override
	public float getPercent() {
		return (float) bossBar.getProgress();
	}

	@Override
	public Spigot13BossBar setPercent(float percent) {
		bossBar.setProgress(percent);
		return this;
	}

	@Override
	public boolean shouldCreateFog() {
		return bossBar.hasFlag(BarFlag.CREATE_FOG);
	}

	@Override
	public Spigot13BossBar setCreateFog(boolean createFog) {
		if (createFog) bossBar.addFlag(BarFlag.CREATE_FOG);
		else bossBar.removeFlag(BarFlag.CREATE_FOG);
		return this;
	}

	@Override
	public boolean shouldDarkenSky(boolean darkenSky) {
		return bossBar.hasFlag(BarFlag.DARKEN_SKY);
	}

	@Override
	public Spigot13BossBar setDarkenSky(boolean darkenSky) {
		if (darkenSky) bossBar.addFlag(BarFlag.DARKEN_SKY);
		else bossBar.removeFlag(BarFlag.DARKEN_SKY);
		return this;
	}

	@Override
	public boolean shouldPlayEndBossMusic() {
		return bossBar.hasFlag(BarFlag.PLAY_BOSS_MUSIC);
	}

	@Override
	public Spigot13BossBar setPlayEndBossMusic(boolean playEndBossMusic) {
		if (playEndBossMusic) bossBar.addFlag(BarFlag.PLAY_BOSS_MUSIC);
		else bossBar.removeFlag(BarFlag.PLAY_BOSS_MUSIC);
		return this;
	}

	@Override
	public boolean isVisible() {
		return bossBar.isVisible();
	}

	@Override
	public Spigot13BossBar setVisible(boolean visible) {
		bossBar.setVisible(visible);
		return this;
	}

	@Override
	public WSText getName() {
		return WSText.getByFormattingText(bossBar.getTitle());
	}

	@Override
	public Spigot13BossBar setName(WSText name) {
		bossBar.setTitle(name.toFormattingText());
		return this;
	}

	@Override
	public BossBar getHandled() {
		return bossBar;
	}

	public static class Builder implements WSBossBar.Builder {

		private EnumBossBarColor color;
		private EnumBossBarOverlay overlay;
		private float percent;
		private boolean createFog, darkenSky, endBossMusic, visible;
		private WSText name;

		public Builder() {
			color = EnumBossBarColor.PURPLE;
			overlay = EnumBossBarOverlay.PROGRESS;
			percent = 1;
			createFog = darkenSky = endBossMusic = visible = false;
			name = WSText.of("");
		}

		@Override
		public Builder color(EnumBossBarColor color) {
			this.color = color;
			return this;
		}

		@Override
		public Builder overlay(EnumBossBarOverlay overlay) {
			this.overlay = overlay;
			return this;
		}

		@Override
		public Builder percent(float percent) {
			this.percent = percent;
			return this;
		}

		@Override
		public Builder createFog(boolean createFog) {
			this.createFog = createFog;
			return this;
		}

		@Override
		public Builder darkenSky(boolean darkenSky) {
			this.darkenSky = darkenSky;
			return this;
		}

		@Override
		public Builder playEndBossMusic(boolean endBossMusic) {
			this.endBossMusic = endBossMusic;
			return this;
		}

		@Override
		public Builder visible(boolean visible) {
			this.visible = visible;
			return this;
		}

		@Override
		public Builder name(WSText name) {
			this.name = name;
			return this;
		}

		@Override
		public WSBossBar build() {
			BossBar bar = Bukkit.createBossBar(name.toFormattingText(), BarColor.valueOf(color.name()), BarStyle.valueOf(overlay.getSpigotName()));
			bar.setVisible(visible);
			bar.setProgress(percent);
			if (createFog) bar.addFlag(BarFlag.CREATE_FOG);
			if (darkenSky) bar.addFlag(BarFlag.DARKEN_SKY);
			if (endBossMusic) bar.addFlag(BarFlag.PLAY_BOSS_MUSIC);
			return new Spigot13BossBar(bar);
		}
	}
}

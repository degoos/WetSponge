package com.degoos.wetsponge.block.tileentity;


import com.degoos.wetsponge.block.Spigot13Block;
import com.degoos.wetsponge.block.WSBlock;
import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.enums.block.EnumBlockTypeChestType;
import com.degoos.wetsponge.inventory.Spigot13Inventory;
import com.degoos.wetsponge.inventory.WSInventory;
import com.degoos.wetsponge.material.block.type.WSBlockTypeChest;
import org.bukkit.block.Chest;
import org.bukkit.inventory.DoubleChestInventory;
import org.bukkit.inventory.Inventory;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

public class Spigot13TileEntityChest extends Spigot13TileEntityInventory implements WSTileEntityChest {

	public Spigot13TileEntityChest(Spigot13Block block) {
		super(block);
	}

	private Spigot13TileEntityChest(Chest tileEntity) {
		super(tileEntity);
	}

	@Override
	public WSInventory getSoloInventory() {
		return new Spigot13Inventory(getHandled().getBlockInventory());
	}

	@Override
	public Optional<WSInventory> getDoubleChestInventory() {
		Inventory inventory = getHandled().getInventory();
		if (inventory instanceof DoubleChestInventory) return Optional.of(new Spigot13Inventory(inventory));
		return Optional.empty();
	}

	@Override
	public Set<WSTileEntityChest> getConnectedChests() {

		Set<WSTileEntityChest> chests = new HashSet<>();
		WSBlock block = getBlock();
		WSBlockTypeChest chest = (WSBlockTypeChest) block.getBlockType();
		EnumBlockTypeChestType chestType = chest.getType();
		if (chestType == EnumBlockTypeChestType.SINGLE) return chests;
		EnumBlockFace direction;
		if (chestType == EnumBlockTypeChestType.LEFT) {
			switch (chest.getFacing()) {
				case EAST:
					direction = EnumBlockFace.SOUTH;
					break;
				case WEST:
					direction = EnumBlockFace.NORTH;
					break;
				case NORTH:
					direction = EnumBlockFace.EAST;
					break;
				default:
					direction = EnumBlockFace.WEST;
			}
		} else {
			switch (chest.getFacing()) {
				case EAST:
					direction = EnumBlockFace.NORTH;
					break;
				case WEST:
					direction = EnumBlockFace.SOUTH;
					break;
				case NORTH:
					direction = EnumBlockFace.WEST;
					break;
				default:
					direction = EnumBlockFace.EAST;
			}
		}
		WSTileEntity tileEntity = getBlock().getLocation().add(direction.getRelative()).getBlock().getTileEntity();
		if (tileEntity instanceof WSTileEntityChest) chests.add((WSTileEntityChest) tileEntity);
		return chests;
	}

	@Override
	public Chest getHandled() {
		return (Chest) super.getHandled();
	}
}

package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.enums.block.EnumBlockTypeBisectedHalf;
import com.degoos.wetsponge.material.block.Spigot13BlockTypeDirectional;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.block.data.Bisected;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.TrapDoor;

import java.util.Objects;
import java.util.Set;

public class Spigot13BlockTypeTrapDoor extends Spigot13BlockTypeDirectional implements WSBlockTypeTrapDoor {

	private EnumBlockTypeBisectedHalf half;
	private boolean open, powered, waterlogged;

	public Spigot13BlockTypeTrapDoor(int numericalId, String oldStringId, String newStringId, int maxStackSize, EnumBlockFace facing,
									 Set<EnumBlockFace> faces, EnumBlockTypeBisectedHalf half, boolean open, boolean powered, boolean waterlogged) {
		super(numericalId, oldStringId, newStringId, maxStackSize, facing, faces);
		Validate.notNull(half, "Half cannot be null!");
		this.half = half;
		this.open = open;
		this.powered = powered;
		this.waterlogged = waterlogged;
	}

	@Override
	public EnumBlockTypeBisectedHalf getHalf() {
		return half;
	}

	@Override
	public void setHalf(EnumBlockTypeBisectedHalf half) {
		Validate.notNull(half, "Half cannot be null!");
		this.half = half;
	}

	@Override
	public boolean isOpen() {
		return open;
	}

	@Override
	public void setOpen(boolean open) {
		this.open = open;
	}

	@Override
	public boolean isPowered() {
		return powered;
	}

	@Override
	public void setPowered(boolean powered) {
		this.powered = powered;
	}

	@Override
	public boolean isWaterlogged() {
		return waterlogged;
	}

	@Override
	public void setWaterlogged(boolean waterlogged) {
		this.waterlogged = waterlogged;
	}

	@Override
	public Spigot13BlockTypeTrapDoor clone() {
		return new Spigot13BlockTypeTrapDoor(getNumericalId(), getOldStringId(), getNewStringId(),
				getMaxStackSize(), getFacing(), getFaces(), half, open, powered, waterlogged);
	}

	@Override
	public BlockData toBlockData() {
		BlockData blockData = super.toBlockData();
		if (blockData instanceof TrapDoor) {
			((TrapDoor) blockData).setHalf(Bisected.Half.valueOf(half.name()));
			((TrapDoor) blockData).setPowered(powered);
			((TrapDoor) blockData).setOpen(open);
			((TrapDoor) blockData).setWaterlogged(waterlogged);
		}
		return blockData;
	}

	@Override
	public Spigot13BlockTypeTrapDoor readBlockData(BlockData blockData) {
		super.readBlockData(blockData);
		if (blockData instanceof TrapDoor) {
			half = EnumBlockTypeBisectedHalf.valueOf(((TrapDoor) blockData).getHalf().name());
			powered = ((TrapDoor) blockData).isPowered();
			open = ((TrapDoor) blockData).isOpen();
			waterlogged = ((TrapDoor) blockData).isWaterlogged();
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeTrapDoor that = (Spigot13BlockTypeTrapDoor) o;
		return open == that.open &&
				powered == that.powered &&
				waterlogged == that.waterlogged &&
				half == that.half;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), half, open, powered, waterlogged);
	}
}

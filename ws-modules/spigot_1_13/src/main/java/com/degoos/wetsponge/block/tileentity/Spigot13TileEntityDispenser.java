package com.degoos.wetsponge.block.tileentity;

import com.degoos.wetsponge.block.Spigot13Block;
import com.degoos.wetsponge.entity.projectile.WSProjectile;
import com.degoos.wetsponge.enums.EnumEntityType;
import com.degoos.wetsponge.parser.entity.Spigot13EntityParser;
import com.flowpowered.math.vector.Vector3d;
import org.bukkit.block.Dispenser;
import org.bukkit.entity.Projectile;
import org.bukkit.util.Vector;

import java.util.Optional;

public class Spigot13TileEntityDispenser extends Spigot13TileEntityNameableInventory implements WSTileEntityDispenser {


    public Spigot13TileEntityDispenser(Spigot13Block block) {
        super(block);
    }

    @Override
    public <T extends WSProjectile> Optional<T> launchProjectile(Class<T> projectile) {
        return launchProjectile(projectile, new Vector3d(0, 0, 0));
    }

    @Override
    public <T extends WSProjectile> Optional<T> launchProjectile(Class<T> projectile, Vector3d velocity) {
        EnumEntityType type = EnumEntityType.getByClass(projectile).orElse(EnumEntityType.UNKNOWN);
        if (type == EnumEntityType.UNKNOWN) return Optional.empty();
        try {
            Class<? extends Projectile> spigotClass = (Class<? extends Projectile>) Spigot13EntityParser.getEntityData(type).getEntityClass();
            return Optional.ofNullable(getHandled().getBlockProjectileSource().launchProjectile(spigotClass, new Vector(velocity.getX(), velocity.getY(),
                    velocity.getZ())))
                    .map(entity -> (T) Spigot13EntityParser.getWSEntity(entity));
        } catch (Throwable ex) {
            ex.printStackTrace();
            return Optional.empty();
        }
    }

    @Override
    public Dispenser getHandled() {
        return (Dispenser) super.getHandled();
    }


}

package com.degoos.wetsponge.server;

import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;
import com.degoos.wetsponge.util.reflection.Spigot13HandledUtils;
import org.bukkit.Server;

public class Spigot13ServerProperties implements WSServerProperties {

	private Object server;

	public Spigot13ServerProperties(Server server) {
		this.server = Spigot13HandledUtils.getServerHandle(server);
	}

	@Override
	public boolean isFlightAllowed() {
		return invokeMethod(server, "getAllowFlight", false);
	}

	@Override
	public WSServerProperties setFlightAllowed(boolean flightAllowed) {
		invokeMethod(server, "setAllowFlight", null, flightAllowed);
		return this;
	}

	@Override
	public boolean isPVPEnabled() {
		return invokeMethod(server, "getPVP", false);
	}

	@Override
	public WSServerProperties setPVPEnabled(boolean pvpEnabled) {
		invokeMethod(server, "setPVP", null, pvpEnabled);
		return this;
	}

	@Override
	public boolean canSpawnNPCs() {
		return invokeMethod(server, "getSpawnNPCs", false);
	}

	@Override
	public WSServerProperties setCanSpawnNPCs(boolean canSpawnNPCs) {
		invokeMethod(server, "setSpawnNPCs", null, canSpawnNPCs);
		return this;
	}

	@Override
	public boolean canSpawnAnimals() {
		return invokeMethod(server, "getSpawnAnimals", false);
	}

	@Override
	public WSServerProperties setCanSpawnAnimals(boolean canSpawnAnimals) {
		invokeMethod(server, "setSpawnAnimals", null, canSpawnAnimals);
		return this;
	}

	@Override
	public boolean preventProxyConnections() {
		return true;
	}

	@Override
	public WSServerProperties setPreventProxyConnections(boolean preventProxyConnections) {
		return this;
	}

	@Override
	public boolean isServerInOnlineMode() {
		return invokeMethod(server, "getOnlineMode", false);
	}

	@Override
	public WSServerProperties setOnlineMode(boolean onlineMode) {
		invokeMethod(server, "setOnlineMode", null, onlineMode);
		return this;
	}

	@Override
	public WSText getMOTD() {
		String string = invokeMethod(server, "getMotd", "");
		return string == null ? WSText.empty() : WSText.getByFormattingText(string);
	}

	@Override
	public WSServerProperties setMOTD(WSText motd) {
		invokeMethod(server, "setMotd", null, motd == null ? null : motd.toFormattingText());
		return this;
	}

	@Override
	public int getBuildLimit() {
		return invokeMethod(server, "getMaxBuildHeight", 255);
	}

	@Override
	public WSServerProperties setBuildLimit(int buildLimit) {
		invokeMethod(server, "c", null, buildLimit);
		return this;
	}

	private <T> T invokeMethod(Object instance, String methodName, T defaultValue, Object... arguments) {
		try {
			Object o = ReflectionUtils.invokeMethod(instance, methodName, arguments);
			if (o == null) return defaultValue;
			return (T) o;
		} catch (Exception ex) {
			InternalLogger
				.printException(ex, "An error has occurred while WetSponge was finding the property " + methodName + "! Returning default value " + defaultValue);
			return defaultValue;
		}
	}
}

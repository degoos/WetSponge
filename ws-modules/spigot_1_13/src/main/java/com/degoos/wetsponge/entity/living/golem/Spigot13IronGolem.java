package com.degoos.wetsponge.entity.living.golem;

import com.degoos.wetsponge.entity.living.Spigot13Creature;
import org.bukkit.entity.IronGolem;

public class Spigot13IronGolem extends Spigot13Creature implements WSIronGolem {


    public Spigot13IronGolem(IronGolem entity) {
        super(entity);
    }

    @Override
    public boolean isPlayerCreated() {
        return getHandled().isPlayerCreated();
    }

    @Override
    public void setPlayerCreated(boolean playerCreated) {
        getHandled().setPlayerCreated(playerCreated);
    }

    @Override
    public IronGolem getHandled() {
        return (IronGolem) super.getHandled();
    }


}

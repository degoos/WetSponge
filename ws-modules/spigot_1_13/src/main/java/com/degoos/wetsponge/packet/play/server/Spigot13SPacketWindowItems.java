package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.item.Spigot13ItemStack;
import com.degoos.wetsponge.item.WSItemStack;
import com.degoos.wetsponge.packet.Spigot13Packet;
import com.degoos.wetsponge.resource.spigot.Spigot13MerchantUtils;
import com.degoos.wetsponge.util.ListUtils;
import com.degoos.wetsponge.util.reflection.NMSUtils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Spigot13SPacketWindowItems extends Spigot13Packet implements WSSPacketWindowItems {

	private int windowId;
	private List<WSItemStack> itemStacks;
	private boolean changed;

	public Spigot13SPacketWindowItems(int windowsId, List<WSItemStack> itemStacks) {
		super(NMSUtils.getNMSClass("PacketPlayOutWindowItems"));
		this.windowId = windowsId;
		this.itemStacks = itemStacks;
		update();
	}

	public Spigot13SPacketWindowItems(Object packet) {
		super(packet);
		refresh();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void update() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));
			fields[0].setInt(getHandler(), windowId);
			if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD)) fields[1]
				.set(getHandler(), itemStacks.stream().map(item -> item == null ? null : Spigot13MerchantUtils.asNMSCopy(((Spigot13ItemStack) item).getHandled()))
					.collect(Collectors.toList()));
			else fields[1].set(getHandler(), ListUtils.toArray((Class<Object>) NMSUtils.getNMSClass("ItemStack"), itemStacks.stream()
				.map(item -> item == null ? null : Spigot13MerchantUtils.asNMSCopy(((Spigot13ItemStack) item).getHandled())).collect(Collectors.toList())));
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void refresh() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));
			windowId = fields[0].getInt(getHandler());

			if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD)) {
				itemStacks = ((List<?>) fields[1].get(getHandler())).stream()
					.map(item -> item == null ? null : new Spigot13ItemStack(Spigot13MerchantUtils.asBukkitCopy(item).clone())).collect(Collectors.toList());
			} else {
				itemStacks = new ArrayList<>();
				Object[] array = (Object[]) fields[1].get(getHandler());
				int index = 0;
				for (Object o : array) {
					itemStacks.add(index, o == null ? null : new Spigot13ItemStack(Spigot13MerchantUtils.asBukkitCopy(o).clone()));
					index++;
				}
			}

		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public boolean hasChanged() {
		return changed;
	}

	@Override
	public int getWindowId() {
		return windowId;
	}

	@Override
	public void setWindowId(int windowsId) {
		this.windowId = windowsId;
		changed = true;
	}

	@Override
	public List<WSItemStack> getItemStacks() {
		changed = true;
		return itemStacks;
	}
}

package com.degoos.wetsponge.item;


import com.degoos.wetsponge.item.enchantment.Spigot13Enchantment;
import com.degoos.wetsponge.item.enchantment.WSEnchantment;
import com.degoos.wetsponge.material.Spigot13Material;
import com.degoos.wetsponge.material.WSMaterial;
import com.degoos.wetsponge.material.item.Spigot13ItemType;
import com.degoos.wetsponge.nbt.Spigot13NBTTagCompound;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;
import com.degoos.wetsponge.resource.spigot.Spigot13SkullBuilder;
import com.degoos.wetsponge.text.Spigot13Text;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.text.translation.Spigot13Translation;
import com.degoos.wetsponge.text.translation.WSTranslation;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.Validate;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;
import com.degoos.wetsponge.util.reflection.Spigot13ItemStackUtils;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;

public class Spigot13ItemStack implements WSItemStack {

	private ItemStack itemStack;
	private WSText displayName;
	private List<WSText> lore;
	private WSMaterial material;
	private int quantity;
	private boolean unbreakable, hideEnchantments, hideAttributes, hideUnbreakable, hideCanDestroy, hideCanBePlacedOn, hidePotionEffects;
	private WSTranslation translation;

	public static Spigot13ItemStack fromFormat(String format) {
		return new Spigot13ItemStack(Spigot13SkullBuilder.createItemStackByUnknownFormat(format));
	}

	public Spigot13ItemStack(ItemStack itemStack) {
		Validate.notNull(itemStack, "ItemStack cannot be null!");
		this.itemStack = itemStack;
		refresh();
	}


	public Spigot13ItemStack(WSMaterial material) {
		Validate.notNull(material, "Material cannot be null!");
		this.material = material;
		this.displayName = null;
		this.lore = new ArrayList<>();
		this.quantity = 1;


		WSNBTTagCompound compound = WSNBTTagCompound.of();
		compound.setInteger("Count", 1);
		compound.setString("id", material.getStringId());

		WSNBTTagCompound tag = WSNBTTagCompound.of();
		tag.setInteger("Unbreakable", 0);

		((Spigot13Material) material).writeNBTTag(tag);

		compound.setTag("tag", tag);

		try {
			Object nmsItemStack = NMSUtils.getNMSClass("ItemStack").getMethod("a", compound.getHandled().getClass()).invoke(null, compound.getHandled());
			itemStack = (ItemStack) NMSUtils.getOBCClass("inventory.CraftItemStack").getMethod("asBukkitCopy",
					nmsItemStack.getClass()).invoke(null, nmsItemStack);

			ItemMeta itemMeta = itemStack.getItemMeta();
			((Spigot13Material) material).writeItemMeta(itemMeta);
			itemStack.setItemMeta(itemMeta);

			refresh();
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was creating an ItemStack!");
			itemStack = new ItemStack(Material.AIR);
		}
	}


	public Spigot13ItemStack(String nbt) throws Exception {
		this(WSNBTTagCompound.of(nbt));
	}

	public Spigot13ItemStack(WSNBTTagCompound nbt) throws Exception {
		Validate.notNull(nbt, "NBT cannot be null!");

		if (!nbt.hasKey("id")) throw new NullPointerException("NBTTagCompound doesn't contain key id!");
		WSMaterial.getById(nbt.getString("id")).ifPresent(material -> nbt.setString("id", material.getStringId()));

		Object nmsItemStack = NMSUtils.getNMSClass("ItemStack").getMethod("a", nbt.getHandled().getClass()).invoke(null, nbt.getHandled());
		itemStack = (ItemStack) NMSUtils.getOBCClass("inventory.CraftItemStack").getMethod("asBukkitCopy", nmsItemStack.getClass()).invoke(null, nmsItemStack);
		if (itemStack.getType() == Material.AIR) {
			InternalLogger.sendWarning("WetSponge has created a new ItemStack with id " +
					itemStack.getType().getKey() + " using the id " + nbt.getString("id"));
		}
		refresh();
	}


	public static WSItemStack of(WSMaterial material) {
		return new Spigot13ItemStack(material);
	}


	@Override
	public Spigot13Text getDisplayName() {
		return (Spigot13Text) displayName;
	}


	@Override
	public Spigot13ItemStack setDisplayName(WSText displayName) {
		this.displayName = displayName;
		return this;
	}


	@Override
	public List<WSText> getLore() {
		return lore;
	}


	@Override
	public Spigot13ItemStack setLore(List<WSText> lore) {
		this.lore = lore == null ? new ArrayList<>() : new ArrayList<>(lore);
		return this;
	}

	@Override
	public WSItemStack addLoreLine(WSText line) {
		lore.add(line == null ? WSText.empty() : line);
		return this;
	}

	@Override
	public WSItemStack clearLore() {
		lore.clear();
		return this;
	}

	@Override
	public Map<WSEnchantment, Integer> getEnchantments() {
		Map<WSEnchantment, Integer> newMap = new HashMap<>();
		getHandled().getEnchantments().forEach((enchantment, level) -> newMap.put(new Spigot13Enchantment(enchantment), level));
		return newMap;
	}

	@Override
	public Optional<Integer> getEnchantmentLevel(WSEnchantment enchantment) {
		if (!containsEnchantment(enchantment)) return Optional.empty();
		return Optional.of(getHandled().getEnchantmentLevel((Enchantment) enchantment.getHandled()));
	}

	@Override
	public boolean containsEnchantment(WSEnchantment enchantment) {
		return getHandled().containsEnchantment((Enchantment) enchantment.getHandled());
	}

	@Override
	public WSItemStack addEnchantment(WSEnchantment enchantment, int level) {
		try {
			getHandled().addUnsafeEnchantment((Enchantment) enchantment.getHandled(), level);
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An exception has occurred while WetSponge was adding the enchantment " +
					enchantment.getName() + " to the item " + toSerializedNBTTag());
		}
		return this;
	}

	@Override
	public WSItemStack removeEnchantment(WSEnchantment enchantment) {
		try {
			getHandled().removeEnchantment((Enchantment) enchantment.getHandled());
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An exception has occurred while WetSponge was adding the enchantment " +
					enchantment.getName() + " to the item " + toSerializedNBTTag());
		}
		return this;
	}

	@Override
	public WSItemStack clearEnchantments() {
		getHandled().getEnchantments().forEach((enchantment, integer) -> getHandled().removeEnchantment(enchantment));
		return this;
	}


	public WSMaterial getMaterial() {
		return material;
	}

	@Override
	public WSItemStack setMaterial(WSMaterial material) {
		this.material = material;
		return this;
	}


	@Override
	public int getQuantity() {
		return quantity;
	}


	@Override
	public Spigot13ItemStack setQuantity(int quantity) {
		this.quantity = quantity;
		return this;
	}

	@Override
	public boolean isUnbreakable() {
		return unbreakable;
	}

	@Override
	public WSItemStack setUnbreakable(boolean unbreakable) {
		this.unbreakable = unbreakable;
		return this;
	}

	@Override
	public boolean isHidingEnchantments() {
		return hideEnchantments;
	}

	@Override
	public WSItemStack hideEnchantments(boolean hideEnchantments) {
		this.hideEnchantments = hideEnchantments;
		return this;
	}

	@Override
	public boolean isHidingAttributes() {
		return hideAttributes;
	}

	@Override
	public WSItemStack hideAttributes(boolean hideAttributes) {
		this.hideAttributes = hideAttributes;
		return this;
	}

	@Override
	public boolean isHidingUnbreakable() {
		return hideUnbreakable;
	}

	@Override
	public WSItemStack hideUnbreakable(boolean hideUnbreakable) {
		this.hideUnbreakable = hideUnbreakable;
		return this;
	}

	@Override
	public boolean isHidingCanDestroy() {
		return hideCanDestroy;
	}

	@Override
	public WSItemStack hideCanDestroy(boolean hideCanDestroy) {
		this.hideCanDestroy = hideCanDestroy;
		return this;
	}

	@Override
	public boolean isHidingCanBePlacedOn() {
		return hideCanBePlacedOn;
	}

	@Override
	public WSItemStack hideCanBePlacedOn(boolean hideCanBePlacedOn) {
		this.hideCanBePlacedOn = hideCanBePlacedOn;
		return this;
	}

	@Override
	public boolean isHidingPotionEffects() {
		return hidePotionEffects;
	}

	@Override
	public WSItemStack hidePotionEffects(boolean hidePotionEffects) {
		this.hidePotionEffects = hidePotionEffects;
		return this;
	}

	@Override
	public WSNBTTagCompound toNBTTag() {
		try {
			Object nbtTag = NMSUtils.getNMSClass("NBTTagCompound").newInstance();
			Object nmsItemStack = NMSUtils.getOBCClass("inventory.CraftItemStack").getMethod("asNMSCopy", ItemStack.class).invoke(null, itemStack);
			ReflectionUtils.getMethod(nmsItemStack.getClass(), "save", nbtTag.getClass()).invoke(nmsItemStack, nbtTag);
			return new Spigot13NBTTagCompound(nbtTag);
		} catch (Throwable ex) {
			ex.printStackTrace();
			return WSNBTTagCompound.of();
		}
	}

	@Override
	public String toSerializedNBTTag() {
		return toNBTTag().toString();
	}


	@Override
	public Spigot13ItemStack update() {
		WSNBTTagCompound compound = WSNBTTagCompound.of();
		compound.setInteger("Count", quantity);
		compound.setString("id", material.getStringId());

		WSNBTTagCompound tag = WSNBTTagCompound.of();

		((Spigot13Material) material).writeNBTTag(tag);

		compound.setTag("tag", tag);

		try {
			Class<?> clazz = NMSUtils.getOBCClass("inventory.CraftItemStack");
			if (itemStack != null && clazz.isAssignableFrom(itemStack.getClass())) {
				Object handle = ReflectionUtils.setAccessible(clazz.getDeclaredField("handle")).get(itemStack);
				if (handle == null) return this;
				Method method = handle.getClass().getMethod("load", compound.getHandled().getClass());
				method.setAccessible(true);
				method.invoke(handle, compound.getHandled());
			} else {
				Object nmsItemStack = NMSUtils.getNMSClass("ItemStack").getMethod("a",
						compound.getHandled().getClass()).invoke(null, compound.getHandled());
				itemStack = (ItemStack) NMSUtils.getOBCClass("inventory.CraftItemStack").getMethod("asBukkitCopy",
						nmsItemStack.getClass()).invoke(null, nmsItemStack);
			}
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was creating an ItemStack!");
			itemStack = new ItemStack(Material.AIR);
		}

		if (!itemStack.getType().equals(Material.AIR)) {
			ItemMeta meta = itemStack.getItemMeta();
			meta.setDisplayName(displayName == null ? null : displayName.toFormattingText());
			meta.setLore(lore.stream().map(WSText::toFormattingText).collect(Collectors.toList()));

			try {
				meta.setUnbreakable(unbreakable);
			} catch (Exception ignore) {
			}

			meta.getItemFlags().forEach(meta::removeItemFlags);
			if (hideEnchantments) meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
			if (hideAttributes) meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
			if (hideUnbreakable) meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
			if (hideCanDestroy) meta.addItemFlags(ItemFlag.HIDE_DESTROYS);
			if (hideCanBePlacedOn) meta.addItemFlags(ItemFlag.HIDE_PLACED_ON);
			if (hidePotionEffects) meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);

			((Spigot13Material) material).writeItemMeta(meta);

			itemStack.setItemMeta(meta);
		}
		try {
			translation = new Spigot13Translation(Spigot13ItemStackUtils.getDisplayName(itemStack));
		} catch (Exception ignore) {
		}
		return this;
	}


	@Override
	public Spigot13ItemStack refresh() {
		Optional<? extends WSMaterial> optional = WSMaterial.getById(itemStack.getType().getKey().toString()/*, itemStack.getDamage()*/);
		this.material = optional.isPresent() ? optional.get() : new Spigot13ItemType(-1, itemStack.getType().getKey().toString(),
				itemStack.getType().getKey().toString(), itemStack.getMaxStackSize());
		this.quantity = itemStack.getAmount();

		((Spigot13Material) material).readNBTTag(toNBTTag());

		if (itemStack.hasItemMeta()) {
			ItemMeta meta = itemStack.getItemMeta();
			this.displayName = meta.hasDisplayName() ? WSText.getByFormattingText(meta.getDisplayName()) : null;
			this.lore = meta.hasLore() ? meta.getLore().stream().map(Spigot13Text::getByFormattingText).collect(Collectors.toList()) : new ArrayList<>();
			try {
				this.unbreakable = meta.isUnbreakable();
			} catch (Exception ex) {
				this.unbreakable = false;
			}

			this.hideEnchantments = meta.hasItemFlag(ItemFlag.HIDE_ENCHANTS);
			this.hideAttributes = meta.hasItemFlag(ItemFlag.HIDE_ATTRIBUTES);
			this.hideUnbreakable = meta.hasItemFlag(ItemFlag.HIDE_UNBREAKABLE);
			this.hideCanDestroy = meta.hasItemFlag(ItemFlag.HIDE_DESTROYS);
			this.hideCanBePlacedOn = meta.hasItemFlag(ItemFlag.HIDE_PLACED_ON);
			this.hidePotionEffects = meta.hasItemFlag(ItemFlag.HIDE_POTION_EFFECTS);

			((Spigot13Material) material).readItemMeta(meta);

		} else {
			this.displayName = null;
			this.lore = new ArrayList<>();
			this.unbreakable = false;
			this.hideEnchantments = false;
			this.hideAttributes = false;
			this.hideUnbreakable = false;
			this.hideCanDestroy = false;
			this.hideCanBePlacedOn = false;
			this.hidePotionEffects = false;
		}
		try {
			translation = new Spigot13Translation(Spigot13ItemStackUtils.getDisplayName(itemStack));
		} catch (Exception ignore) {
		}
		return this;
	}


	@Override
	public WSItemStack clone() {
		return new Spigot13ItemStack(itemStack.clone());
	}

	@Override
	public boolean isSimilar(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Spigot13ItemStack that = (Spigot13ItemStack) o;

		if (unbreakable != that.unbreakable) return false;
		if (hideEnchantments != that.hideEnchantments) return false;
		if (hideAttributes != that.hideAttributes) return false;
		if (hideUnbreakable != that.hideUnbreakable) return false;
		if (hideCanDestroy != that.hideCanDestroy) return false;
		if (hideCanBePlacedOn != that.hideCanBePlacedOn) return false;
		if (hidePotionEffects != that.hidePotionEffects) return false;
		if (displayName != null ? !displayName.equals(that.displayName) : that.displayName != null) return false;
		if (lore != null ? !lore.equals(that.lore) : that.lore != null) return false;
		return material != null ? material.equals(that.material) : that.material == null;
	}

	@Override
	public WSTranslation getTranslation() {
		return translation;
	}

	@Override
	public ItemStack getHandled() {
		return itemStack;
	}


	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Spigot13ItemStack that = (Spigot13ItemStack) o;

		if (quantity != that.quantity) return false;
		if (unbreakable != that.unbreakable) return false;
		if (hideEnchantments != that.hideEnchantments) return false;
		if (hideAttributes != that.hideAttributes) return false;
		if (hideUnbreakable != that.hideUnbreakable) return false;
		if (hideCanDestroy != that.hideCanDestroy) return false;
		if (hideCanBePlacedOn != that.hideCanBePlacedOn) return false;
		if (hidePotionEffects != that.hidePotionEffects) return false;
		if (displayName != null ? !displayName.equals(that.displayName) : that.displayName != null) return false;
		if (lore != null ? !lore.equals(that.lore) : that.lore != null) return false;
		return material != null ? material.equals(that.material) : that.material == null;
	}

	@Override
	public int hashCode() {
		int result = displayName != null ? displayName.hashCode() : 0;
		result = 31 * result + (lore != null ? lore.hashCode() : 0);
		result = 31 * result + (material != null ? material.hashCode() : 0);
		result = 31 * result + quantity;
		result = 31 * result + (unbreakable ? 1 : 0);
		result = 31 * result + (hideEnchantments ? 1 : 0);
		result = 31 * result + (hideAttributes ? 1 : 0);
		result = 31 * result + (hideUnbreakable ? 1 : 0);
		result = 31 * result + (hideCanDestroy ? 1 : 0);
		result = 31 * result + (hideCanBePlacedOn ? 1 : 0);
		result = 31 * result + (hidePotionEffects ? 1 : 0);
		return result;
	}
}

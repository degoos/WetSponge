package com.degoos.wetsponge.entity.living.animal;

import com.degoos.wetsponge.enums.EnumDyeColor;
import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.entity.AnimalTamer;
import org.bukkit.entity.Wolf;

import java.util.Optional;
import java.util.UUID;

public class Spigot13Wolf extends Spigot13Animal implements WSWolf {


    public Spigot13Wolf(Wolf entity) {
        super(entity);
    }

    @Override
    public EnumDyeColor getCollarColor() {
        return EnumDyeColor.getByDyeData(getHandled().getCollarColor().getDyeData()).orElse(EnumDyeColor.RED);
    }

    @Override
    public void setCollarColor(EnumDyeColor collarColor) {
        getHandled().setCollarColor(DyeColor.getByDyeData(collarColor.getDyeData()));
    }

    @Override
    public boolean isAngry() {
        return getHandled().isAngry();
    }

    @Override
    public void setAngry(boolean angry) {
        getHandled().setAngry(angry);
    }

    @Override
    public boolean isSitting() {
        return getHandled().isSitting();
    }

    @Override
    public void setSitting(boolean sitting) {
        getHandled().setSitting(sitting);
    }

    @Override
    public boolean isTamed() {
        return getHandled().isTamed();
    }

    @Override
    public Optional<UUID> getTamer() {
        AnimalTamer tamer = getHandled().getOwner();
        if (tamer == null) return Optional.empty();
        return Optional.of(tamer.getUniqueId());
    }

    @Override
    public void setTamer(UUID tamer) {
        getHandled().setOwner(tamer == null ? null : Bukkit.getOfflinePlayer(tamer));
    }

    @Override
    public Wolf getHandled() {
        return (Wolf) super.getHandled();
    }
}

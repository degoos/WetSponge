package com.degoos.wetsponge.entity.living.animal;


import com.degoos.wetsponge.enums.EnumRabbitType;
import org.bukkit.entity.Rabbit;

import java.util.Optional;

public class Spigot13Rabbit extends Spigot13Animal implements WSRabbit {


	public Spigot13Rabbit(Rabbit entity) {
		super(entity);
	}


	@Override
	public Optional<EnumRabbitType> getRabbitType () {
		return EnumRabbitType.getRabbitType(getHandled().getRabbitType().name());
	}


	@Override
	public void setRabbitType (EnumRabbitType rabbitType) {
		getHandled().setRabbitType(Rabbit.Type.valueOf(rabbitType.getSpigotName()));
	}


	@Override
	public Rabbit getHandled () {
		return (Rabbit) super.getHandled();
	}
}

package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.Spigot13BlockTypeDirectional;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.TripwireHook;

import java.util.Objects;
import java.util.Set;

public class Spigot13BlockTypeTripwireHook extends Spigot13BlockTypeDirectional implements WSBlockTypeTripwireHook {

	private boolean attached, powered;

	public Spigot13BlockTypeTripwireHook(EnumBlockFace facing, Set<EnumBlockFace> faces, boolean attached, boolean powered) {
		super(131, "minecraft:tripwire_hook", "minecraft:tripwire_hook", 64, facing, faces);
		this.attached = attached;
		this.powered = powered;
	}

	@Override
	public boolean isAttached() {
		return attached;
	}

	@Override
	public void setAttached(boolean attached) {
		this.attached = attached;
	}

	@Override
	public boolean isPowered() {
		return powered;
	}

	@Override
	public void setPowered(boolean powered) {
		this.powered = powered;
	}

	@Override
	public Spigot13BlockTypeTripwireHook clone() {
		return new Spigot13BlockTypeTripwireHook(getFacing(), getFaces(), attached, powered);
	}

	@Override
	public BlockData toBlockData() {
		BlockData blockData = super.toBlockData();
		if (blockData instanceof TripwireHook) {
			((TripwireHook) blockData).setPowered(powered);
			((TripwireHook) blockData).setAttached(attached);
		}
		return blockData;
	}

	@Override
	public Spigot13BlockTypeTripwireHook readBlockData(BlockData blockData) {
		super.readBlockData(blockData);
		if (blockData instanceof TripwireHook) {
			powered = ((TripwireHook) blockData).isPowered();
			attached = ((TripwireHook) blockData).isAttached();
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeTripwireHook that = (Spigot13BlockTypeTripwireHook) o;
		return attached == that.attached &&
				powered == that.powered;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), attached, powered);
	}
}

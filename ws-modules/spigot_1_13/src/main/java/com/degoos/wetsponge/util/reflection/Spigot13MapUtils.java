package com.degoos.wetsponge.util.reflection;

import com.degoos.wetsponge.enums.EnumMapBaseColor;
import com.degoos.wetsponge.material.WSBlockTypes;
import com.degoos.wetsponge.util.InternalLogger;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class Spigot13MapUtils {

	private static Field field;
	private static Method method;

	static {
		try {
			method = NMSUtils.getNMSClass("IBlockData").getMethod("d", NMSUtils.getNMSClass("IBlockAccess"), NMSUtils.getNMSClass("BlockPosition"));
			field = ReflectionUtils.setAccessible(NMSUtils.getNMSClass("MaterialMapColor").getField("ac"));
			AccessibleObject.setAccessible(new AccessibleObject[]{field}, true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static EnumMapBaseColor getMapBaseColor(Object iBlockData, Object iBlockAccess, Object blockPosition, String id) {
		try {
			if (id.equalsIgnoreCase(WSBlockTypes.AIR.getStringId())) return EnumMapBaseColor.AIR;
			return EnumMapBaseColor.getById(field.getInt(method.invoke(iBlockData, iBlockAccess, blockPosition))).orElse(EnumMapBaseColor.AIR);
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was getting the map base color of the block " + id + "!");
			return EnumMapBaseColor.AIR;
		}
	}

}

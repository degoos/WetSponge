package com.degoos.wetsponge.text.action.click;

import net.md_5.bungee.api.chat.ClickEvent;

public class Spigot13SuggestCommandAction extends Spigot13ClickAction implements WSSuggestCommandAction {

    public Spigot13SuggestCommandAction(ClickEvent event) {
        super(event);
    }

    public Spigot13SuggestCommandAction(String command) {
        super(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, command));
    }

    @Override
    public String getCommand() {
        return getHandled().getValue();
    }
}

package com.degoos.wetsponge.material.item.type;

import com.degoos.wetsponge.enums.EnumDyeColor;
import com.degoos.wetsponge.material.item.Spigot13ItemType;
import com.degoos.wetsponge.util.Validate;

import java.util.Objects;

public class Spigot13ItemTypeDyeColored extends Spigot13ItemType implements WSItemTypeDyeColored {

	private EnumDyeColor dyeColor;

	public Spigot13ItemTypeDyeColored(int numericalId, String oldStringId, String newStringId, int maxStackSize, EnumDyeColor dyeColor) {
		super(numericalId, oldStringId, newStringId.startsWith("minecraft:") ? newStringId.substring(10) : newStringId, maxStackSize);
		Validate.notNull(dyeColor, "Dye color cannot be null!");
		this.dyeColor = dyeColor;
	}

	@Override
	public String getNewStringId() {
		return dyeColor.getMinecraftName().toLowerCase() + "_" + super.getNewStringId();
	}

	@Override
	public EnumDyeColor getDyeColor() {
		return dyeColor;
	}

	@Override
	public void setDyeColor(EnumDyeColor dyeColor) {
		Validate.notNull(dyeColor, "Dye color cannot be null!");
		this.dyeColor = dyeColor;
	}

	@Override
	public Spigot13ItemTypeDyeColored clone() {
		return new Spigot13ItemTypeDyeColored(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), dyeColor);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13ItemTypeDyeColored that = (Spigot13ItemTypeDyeColored) o;
		return dyeColor == that.dyeColor;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), dyeColor);
	}
}

package com.degoos.wetsponge.entity.living.monster;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.entity.living.Spigot13LivingEntity;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.util.reflection.Spigot13EntityUtils;
import org.bukkit.entity.Slime;

import java.util.Optional;

public class Spigot13Slime extends Spigot13LivingEntity implements WSSlime {


	public Spigot13Slime(Slime entity) {
		super(entity);
	}


	@Override
	public int getSize() {
		return getHandled().getSize();
	}

	@Override
	public void setSize(int size) {
		getHandled().setSize(size);
	}


	@Override
	public void setAI(boolean ai) {
		if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD))
			getHandled().setAI(ai);
		else Spigot13EntityUtils.setAI(getHandled(), ai);
	}


	@Override
	public boolean hasAI() {
		if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD))
			return getHandled().hasAI();
		else return Spigot13EntityUtils.hasAI(getHandled());
	}

	@Override
	public Optional<WSEntity> getTarget() {
		return null;
	}

	@Override
	public void setTarget(WSEntity entity) {
	}

	@Override
	public Slime getHandled() {
		return (Slime) super.getHandled();
	}
}

package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.EnumDyeColor;
import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.enums.block.EnumBlockTypeBedPart;
import com.degoos.wetsponge.material.block.Spigot13BlockTypeDirectional;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.Bed;

import java.util.Objects;
import java.util.Set;

public class Spigot13BlockTypeBed extends Spigot13BlockTypeDirectional implements WSBlockTypeBed {

	private EnumBlockTypeBedPart bedPart;
	private boolean occupied;
	private EnumDyeColor dyeColor;

	public Spigot13BlockTypeBed(EnumBlockFace facing, Set<EnumBlockFace> faces, EnumBlockTypeBedPart bedPart, boolean occupied, EnumDyeColor dyeColor) {
		super(26, "minecraft:bed", "bed", 1, facing, faces);
		Validate.notNull(bedPart, "Bed part cannot be null!");
		Validate.notNull(dyeColor, "Dye color cannot be null!");
		this.bedPart = bedPart;
		this.occupied = occupied;
		this.dyeColor = dyeColor;
	}

	@Override
	public String getNewStringId() {
		return "minecraft:" + dyeColor.getMinecraftName().toLowerCase() + "_" + super.getNewStringId();
	}

	@Override
	public EnumBlockTypeBedPart getPart() {
		return bedPart;
	}

	@Override
	public void setPart(EnumBlockTypeBedPart part) {
		Validate.notNull(part, "Bed part cannot be null!");
		this.bedPart = part;
	}

	@Override
	public boolean isOccupied() {
		return occupied;
	}

	@Override
	public EnumDyeColor getDyeColor() {
		return dyeColor;
	}

	@Override
	public void setDyeColor(EnumDyeColor dyeColor) {
		Validate.notNull(dyeColor, "Dye color cannot be null!");
		this.dyeColor = dyeColor;
	}

	@Override
	public Spigot13BlockTypeBed clone() {
		return new Spigot13BlockTypeBed(getFacing(), getFaces(), bedPart, occupied, dyeColor);
	}

	@Override
	public BlockData toBlockData() {
		BlockData blockData = super.toBlockData();
		if (blockData instanceof Bed) {
			((Bed) blockData).setPart(Bed.Part.valueOf(bedPart.name()));
		}
		return blockData;
	}

	@Override
	public Spigot13BlockTypeBed readBlockData(BlockData blockData) {
		super.readBlockData(blockData);
		if (blockData instanceof Bed) {
			bedPart = EnumBlockTypeBedPart.valueOf(((Bed) blockData).getPart().name());
			occupied = ((Bed) blockData).isOccupied();
		}
		return this;
	}


	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeBed that = (Spigot13BlockTypeBed) o;
		return occupied == that.occupied &&
				bedPart == that.bedPart &&
				dyeColor == that.dyeColor;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), bedPart, occupied, dyeColor);
	}
}

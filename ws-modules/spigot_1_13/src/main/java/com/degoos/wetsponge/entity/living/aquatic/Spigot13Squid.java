package com.degoos.wetsponge.entity.living.aquatic;

import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.entity.living.Spigot13Creature;
import com.degoos.wetsponge.entity.living.WSLivingEntity;
import com.degoos.wetsponge.parser.entity.Spigot13EntityParser;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Squid;

import java.util.Optional;

public class Spigot13Squid extends Spigot13Creature implements WSSquid {


	public Spigot13Squid(Squid entity) {
		super(entity);
	}


	@Override
	public Squid getHandled() {
		return (Squid) super.getHandled();
	}

	@Override
	public void setAI(boolean ai) {
		getHandled().setAI(ai);
	}

	@Override
	public boolean hasAI() {
		return getHandled().hasAI();
	}

	@Override
	public Optional<WSEntity> getTarget() {
		return Optional.ofNullable(getHandled().getTarget()).map(Spigot13EntityParser::getWSEntity);
	}

	@Override
	public void setTarget(WSEntity entity) {
		getHandled().setTarget(!(entity instanceof WSLivingEntity) ? null : (LivingEntity) entity);
	}
}

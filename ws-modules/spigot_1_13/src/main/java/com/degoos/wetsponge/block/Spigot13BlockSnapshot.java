package com.degoos.wetsponge.block;

import com.degoos.wetsponge.material.WSBlockTypes;
import com.degoos.wetsponge.material.block.Spigot13BlockType;
import org.bukkit.block.Block;

public class Spigot13BlockSnapshot extends WSBlockSnapshot {


	public Spigot13BlockSnapshot(Block block) {
		super(((Spigot13BlockType) WSBlockTypes.getById(block.getType().getKey().toString())
				.orElse(new Spigot13BlockType(-1, block.getType().getKey().toString(), block.getType().getKey().toString(),
						block.getType().getMaxStackSize()))).readBlockData(block.getBlockData()));
	}
}

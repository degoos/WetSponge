package com.degoos.wetsponge.text.action.click;

import net.md_5.bungee.api.chat.ClickEvent;

import java.net.MalformedURLException;
import java.net.URL;

public class Spigot13OpenURLAction extends Spigot13ClickAction implements WSOpenURLAction {

    public Spigot13OpenURLAction(ClickEvent event) {
        super(event);
    }

    public Spigot13OpenURLAction(URL url) {
        super(new ClickEvent(ClickEvent.Action.OPEN_URL, url.toString()));
    }


    @Override
    public URL getURL() {
        try {
            return new URL(getHandled().getValue());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return null;
    }
}

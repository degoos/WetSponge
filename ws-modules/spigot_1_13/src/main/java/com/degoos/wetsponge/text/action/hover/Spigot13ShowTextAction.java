package com.degoos.wetsponge.text.action.hover;

import com.degoos.wetsponge.text.Spigot13Text;
import com.degoos.wetsponge.text.WSText;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;

public class Spigot13ShowTextAction extends Spigot13HoverAction implements WSShowTextAction {

    public Spigot13ShowTextAction(HoverEvent event) {
        super(event);
    }

    public Spigot13ShowTextAction(WSText text) {
        super(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(text.toFormattingText()).create()));
    }


    @Override
    public WSText getText() {
        return Spigot13Text.of(getHandled().getValue()[0]);
    }
}

package com.degoos.wetsponge.material.block;

import com.degoos.wetsponge.enums.block.EnumAxis;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.Axis;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.Orientable;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class Spigot13BlockTypeOrientable extends Spigot13BlockType implements WSBlockTypeOrientable {

	private EnumAxis axis;
	private Set<EnumAxis> axes;

	public Spigot13BlockTypeOrientable(int numericalId, String oldStringId, String newStringId, int maxStackSize, EnumAxis axis, Set<EnumAxis> axes) {
		super(numericalId, oldStringId, newStringId, maxStackSize);
		Validate.notNull(axis, "Axis cannot be null!");
		this.axis = axis;
		this.axes = axes;
	}

	@Override
	public EnumAxis getAxis() {
		return axis;
	}

	@Override
	public void setAxis(EnumAxis axis) {
		Validate.notNull(axis, "Axis cannot be null!");
		this.axis = axis;
	}

	@Override
	public Set<EnumAxis> getAxes() {
		return new HashSet<>(axes);
	}

	@Override
	public Spigot13BlockTypeOrientable clone() {
		return new Spigot13BlockTypeOrientable(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), axis, axes);
	}

	@Override
	public BlockData toBlockData() {
		BlockData blockData = super.toBlockData();
		if (blockData instanceof Orientable) {
			((Orientable) blockData).setAxis(Axis.valueOf(axes.toString()));
		}
		return blockData;
	}

	@Override
	public Spigot13BlockTypeOrientable readBlockData(BlockData blockData) {
		super.readBlockData(blockData);
		if (blockData instanceof Orientable) {
			axis = EnumAxis.valueOf(((Orientable) blockData).getAxis().name());
			axes = ((Orientable) blockData).getAxes().stream().map(target -> EnumAxis.valueOf(target.name())).collect(Collectors.toSet());
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeOrientable that = (Spigot13BlockTypeOrientable) o;
		return axis == that.axis;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), axis);
	}
}

package com.degoos.wetsponge.parser.plugin;

import com.degoos.wetsponge.plugin.Spigot13BasePlugin;
import com.degoos.wetsponge.plugin.WSBasePlugin;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

public class Spigot13PluginParser {

	public static Set<WSBasePlugin> getBasePlugins() {
		return Arrays.stream(org.bukkit.Bukkit.getPluginManager().getPlugins()).map(Spigot13BasePlugin::new).collect(Collectors.toSet());
	}

	public static boolean isBasePluginEnabled(String name) {
		return org.bukkit.Bukkit.getPluginManager().isPluginEnabled(name);
	}

}

package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.Spigot13BlockTypeDirectional;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.WallSign;

import java.util.Objects;
import java.util.Set;

public class Spigot13BlockTypeWallSign extends Spigot13BlockTypeDirectional implements WSBlockTypeWallSign {

	private boolean waterlogged;

	public Spigot13BlockTypeWallSign(EnumBlockFace facing, Set<EnumBlockFace> faces, boolean waterlogged) {
		super(68, "minecraft:wall_sign", "minecraft:wall_sign", 64, facing, faces);
		this.waterlogged = waterlogged;
	}

	@Override
	public boolean isWaterlogged() {
		return waterlogged;
	}

	@Override
	public void setWaterlogged(boolean waterlogged) {
		this.waterlogged = waterlogged;
	}

	@Override
	public Spigot13BlockTypeWallSign clone() {
		return new Spigot13BlockTypeWallSign(getFacing(), getFaces(), waterlogged);
	}

	@Override
	public BlockData toBlockData() {
		BlockData blockData = super.toBlockData();
		if (blockData instanceof WallSign) {
			((WallSign) blockData).setWaterlogged(waterlogged);
		}
		return blockData;
	}

	@Override
	public Spigot13BlockTypeWallSign readBlockData(BlockData blockData) {
		super.readBlockData(blockData);
		if (blockData instanceof WallSign) {
			waterlogged = ((WallSign) blockData).isWaterlogged();
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeWallSign that = (Spigot13BlockTypeWallSign) o;
		return waterlogged == that.waterlogged;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), waterlogged);
	}
}

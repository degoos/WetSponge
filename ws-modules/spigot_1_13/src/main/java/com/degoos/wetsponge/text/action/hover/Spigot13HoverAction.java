package com.degoos.wetsponge.text.action.hover;

import com.degoos.wetsponge.text.action.Spigot13TextAction;
import net.md_5.bungee.api.chat.HoverEvent;

public class Spigot13HoverAction extends Spigot13TextAction implements WSHoverAction {

    public static Spigot13HoverAction of(HoverEvent event) {
        switch (event.getAction()) {
            case SHOW_ACHIEVEMENT:
            case SHOW_ENTITY:
            case SHOW_ITEM:
            case SHOW_TEXT:
                new Spigot13ShowTextAction(event);
            default:
                return new Spigot13HoverAction(event);
        }
    }


    private HoverEvent hoverEvent;


    public Spigot13HoverAction(HoverEvent hoverEvent) {
        this.hoverEvent = hoverEvent;
    }

    @Override
    public HoverEvent getHandled() {
        return hoverEvent;
    }
}

package com.degoos.wetsponge.listener.spigot;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.block.Spigot13BlockSnapshot;
import com.degoos.wetsponge.block.WSBlockSnapshot;
import com.degoos.wetsponge.data.WSTransaction;
import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.enums.EnumEquipType;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.enums.EnumTristate;
import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.event.block.WSBlockModifyEvent;
import com.degoos.wetsponge.event.entity.player.interact.WSPlayerInteractBlockEvent;
import com.degoos.wetsponge.event.entity.player.interact.WSPlayerInteractEntityEvent;
import com.degoos.wetsponge.event.entity.player.interact.WSPlayerInteractItemEvent;
import com.degoos.wetsponge.item.WSItemStack;
import com.degoos.wetsponge.material.WSBlockTypes;
import com.degoos.wetsponge.parser.entity.Spigot13EntityParser;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.SpigotEventUtils;
import com.degoos.wetsponge.world.Spigot13Location;
import com.degoos.wetsponge.world.WSLocation;
import com.flowpowered.math.vector.Vector3d;
import org.bukkit.block.Block;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.util.Vector;

import java.util.Optional;

public class Spigot13PlayerInteractListener implements Listener {

	@EventHandler(priority = EventPriority.LOWEST)
	public void onBlockInteraction(PlayerInteractEvent event) {
		if (!SpigotEventUtils.shouldBeExecuted()) return;
		try {
			WSPlayerInteractItemEvent itemEvent;
			WSPlayerInteractBlockEvent blockEvent;
			WSPlayer player = WetSponge.getServer().getPlayer(event.getPlayer().getUniqueId()).orElse(null);

			if (event.getAction() == Action.PHYSICAL) {
				WSBlockModifyEvent wetSpongeEvent = new WSBlockModifyEvent(new WSTransaction<>(new Spigot13BlockSnapshot(event
						.getClickedBlock()), new Spigot13BlockSnapshot(event.getClickedBlock())), new Spigot13Location(event.getClickedBlock().getLocation()), Optional
						.of(player));
				WetSponge.getEventManager().callEvent(wetSpongeEvent);
				event.setCancelled(wetSpongeEvent.isCancelled());
				return;
			}

			boolean mainHand = WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_9) || event.getHand() == EquipmentSlot.HAND;
			boolean primary = event.getAction() == Action.LEFT_CLICK_AIR || event.getAction() == Action.LEFT_CLICK_BLOCK;
			boolean clickBlock = event.getAction() == Action.LEFT_CLICK_BLOCK || event.getAction() == Action.RIGHT_CLICK_BLOCK;
			WSItemStack itemStack = player.getEquippedItem(mainHand ? EnumEquipType.MAIN_HAND : EnumEquipType.OFF_HAND).orElse(WSItemStack.of(WSBlockTypes.AIR));

			if (primary) {
				if (mainHand) itemEvent = new WSPlayerInteractItemEvent.Primary.MainHand(player, itemStack);
				else itemEvent = new WSPlayerInteractItemEvent.Primary.OffHand(player, itemStack);
			} else {
				if (mainHand) itemEvent = new WSPlayerInteractItemEvent.Secondary.MainHand(player, itemStack);
				else itemEvent = new WSPlayerInteractItemEvent.Secondary.OffHand(player, itemStack);
			}

			WetSponge.getEventManager().callEvent(itemEvent);
			if (itemEvent.isCancelled()) {
				event.setCancelled(true);
				return;
			}

			WSBlockSnapshot block = clickBlock ? new Spigot13BlockSnapshot(event.getClickedBlock()) : new WSBlockSnapshot(WSBlockTypes.AIR.getDefaultState());
			Optional<Vector3d> position = Optional.empty();
			Optional<WSLocation> blockLocation =
					clickBlock ? Optional.ofNullable(event.getClickedBlock()).map(Block::getLocation).map(Spigot13Location::new) : Optional.empty();
			EnumBlockFace direction = EnumBlockFace.valueOf(event.getBlockFace().name());

			if (primary) {
				if (mainHand) blockEvent = new WSPlayerInteractBlockEvent.Primary.MainHand(player, block, direction, position, blockLocation);
				else blockEvent = new WSPlayerInteractBlockEvent.Primary.OffHand(player, block, direction, position, blockLocation);
			} else {
				if (mainHand) blockEvent = new WSPlayerInteractBlockEvent.Secondary.MainHand(player, block, direction, position, blockLocation, transform(event
						.useInteractedBlock()), transform(event.useItemInHand()), transform(event.useInteractedBlock()), transform(event.useItemInHand()));
				else blockEvent = new WSPlayerInteractBlockEvent.Secondary.OffHand(player, block, direction, position, blockLocation, transform(event
						.useInteractedBlock()), transform(event.useItemInHand()), transform(event.useInteractedBlock()), transform(event.useItemInHand()));
			}

			WetSponge.getEventManager().callEvent(blockEvent);
			event.setCancelled(blockEvent.isCancelled());

		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Spigot-PlayerInteractEvent!");
		}
	}


	@EventHandler(priority = EventPriority.LOWEST)
	public void onEntityInteract(PlayerInteractAtEntityEvent event) {
		if (!SpigotEventUtils.shouldBeExecuted()) return;
		try {
			event.setCancelled(executeEvent(WetSponge.getServer().getPlayer(event.getPlayer().getUniqueId()).orElse(null), Spigot13EntityParser
							.getWSEntity(event.getRightClicked()),
					WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_9) || event.getHand() == EquipmentSlot.HAND, Optional
							.ofNullable(transform(event.getClickedPosition()))));
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Spigot-PlayerInteractAtEntityEvent!");
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onEntityInteract(PlayerInteractEntityEvent event) {
		if (!SpigotEventUtils.shouldBeExecuted()) return;
		try {
			event.setCancelled(executeEvent(WetSponge.getServer().getPlayer(event.getPlayer().getUniqueId()).orElse(null), Spigot13EntityParser
							.getWSEntity(event.getRightClicked()),
					WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_9) || event.getHand() == EquipmentSlot.HAND, Optional.empty()));
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Spigot-PlayerInteractEntityEvent!");
		}
	}


	public boolean executeEvent(WSPlayer player, WSEntity entity, boolean isMainHand, Optional<Vector3d> position) {
		WSPlayerInteractEntityEvent event;
		event = isMainHand ? new WSPlayerInteractEntityEvent.Secondary.MainHand(player, entity, position)
				: new WSPlayerInteractEntityEvent.Secondary.OffHand(player, entity, position);
		WetSponge.getEventManager().callEvent(event);
		return event.isCancelled();
	}


	private Vector3d transform(Vector vector) {
		return new Vector3d(vector.getX(), vector.getY(), vector.getZ());
	}

	private Event.Result transform(EnumTristate result) {
		switch (result) {
			case FALSE:
				return Event.Result.DENY;
			case TRUE:
				return Event.Result.ALLOW;
			case UNDEFINED:
			default:
				return Event.Result.DEFAULT;
		}
	}

	private EnumTristate transform(Event.Result result) {
		switch (result) {
			case ALLOW:
				return EnumTristate.TRUE;
			case DENY:
				return EnumTristate.FALSE;
			case DEFAULT:
			default:
				return EnumTristate.UNDEFINED;
		}
	}

}

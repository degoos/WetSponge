package com.degoos.wetsponge.listener.spigot;

import com.degoos.wetsponge.SpigotWetSponge;
import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.entity.living.WSLivingEntity;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.event.entity.player.movement.WSPlayerChangeWorldEvent;
import com.degoos.wetsponge.event.entity.player.movement.WSPlayerMoveEvent;
import com.degoos.wetsponge.event.entity.player.movement.WSPlayerTeleportEvent;
import com.degoos.wetsponge.packet.play.server.WSSPacketEntityLookMove;
import com.degoos.wetsponge.parser.player.PlayerParser;
import com.degoos.wetsponge.parser.world.WorldParser;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.SpigotEventUtils;
import com.degoos.wetsponge.world.Spigot13Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.scheduler.BukkitRunnable;

public class Spigot13MovementListener implements Listener {

	@EventHandler(priority = EventPriority.LOWEST)
	public void onMove(PlayerMoveEvent event) {
		if (!SpigotEventUtils.shouldBeExecuted()) return;
		try {
			WSPlayerMoveEvent wetSpongeEvent = new WSPlayerMoveEvent(PlayerParser.getPlayer(event.getPlayer().getUniqueId()).orElse(null), new Spigot13Location(event
					.getFrom()), new Spigot13Location(event.getTo()));
			WetSponge.getEventManager().callEvent(wetSpongeEvent);
			event.setCancelled(wetSpongeEvent.isCancelled());
			event.setTo(((Spigot13Location) wetSpongeEvent.getTo()).getLocation());

			wetSpongeEvent.getPlayer().getFakeBlocks().forEach((location, type) -> {
				if (location.distance(wetSpongeEvent.getFrom()) > 100 && location.distance(wetSpongeEvent.getTo()) <= 100)
					wetSpongeEvent.getPlayer().refreshFakeBlock(location);
			});
			if (wetSpongeEvent.getPlayer().hasDisguise()) {
				WSPlayer player = wetSpongeEvent.getPlayer();
				WSLivingEntity entity = wetSpongeEvent.getPlayer().getDisguise().get();
				wetSpongeEvent.getPlayer().sendPacket(WSSPacketEntityLookMove
						.of(entity.getEntityId(), player.getLocation().toVector3i(), player.getRotation().toVector2().toInt(), player.isOnGround()));
			}
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Spigot-PlayerMoveEvent!");
		}
	}

	@EventHandler
	public void onTeleport(PlayerTeleportEvent event) {
		if (!SpigotEventUtils.shouldBeExecuted()) return;
		try {
			WSPlayerTeleportEvent wetSpongeEvent = new WSPlayerTeleportEvent(PlayerParser.getPlayer(event.getPlayer().getUniqueId())
					.orElse(null), new Spigot13Location(event.getFrom()), new Spigot13Location(event.getTo()));
			WetSponge.getEventManager().callEvent(wetSpongeEvent);
			event.setCancelled(wetSpongeEvent.isCancelled());
			event.setTo(((Spigot13Location) wetSpongeEvent.getTo()).getLocation());
			new BukkitRunnable() {
				@Override
				public void run() {
					wetSpongeEvent.getPlayer().getFakeBlocks().forEach((location, type) -> {
						if (location.distance(wetSpongeEvent.getFrom()) > 100 && location.distance(wetSpongeEvent.getTo()) <= 100)
							wetSpongeEvent.getPlayer().refreshFakeBlock(location);
					});
				}
			}.runTaskLater(SpigotWetSponge.getInstance(), 5);
			if (wetSpongeEvent.getPlayer() != null && wetSpongeEvent.getPlayer().hasDisguise()) {
				WSPlayer player = wetSpongeEvent.getPlayer();
				WSLivingEntity entity = wetSpongeEvent.getPlayer().getDisguise().get();
				wetSpongeEvent.getPlayer().sendPacket(WSSPacketEntityLookMove
						.of(entity.getEntityId(), player.getLocation().toVector3i(), player.getRotation().toVector2().toInt(), player.isOnGround()));
			}
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Spigot-PlayerTeleportEvent!");
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onChangeWorld(PlayerChangedWorldEvent event) {
		if (!SpigotEventUtils.shouldBeExecuted()) return;
		try {
			WSPlayer player = PlayerParser.getPlayer(event.getPlayer().getUniqueId()).orElse(null);
			WetSponge.getEventManager()
					.callEvent(new WSPlayerChangeWorldEvent(player, WorldParser.getOrCreateWorld(event.getFrom().getName(), event.getFrom()), WorldParser
							.getOrCreateWorld(event.getPlayer().getWorld().getName(), event.getPlayer().getWorld())));
			PlayerParser.resetPlayer(event.getPlayer(), event.getPlayer().getUniqueId());
			new BukkitRunnable() {
				@Override
				public void run() {
					player.getFakeBlocks().forEach((location, type) -> {
						if (location.distance(player.getLocation()) <= 100) player.refreshFakeBlock(location);
					});
				}
			}.runTaskLater(SpigotWetSponge.getInstance(), 5);
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Spigot-PlayerChangedWorldEvent!");
		}
	}

}

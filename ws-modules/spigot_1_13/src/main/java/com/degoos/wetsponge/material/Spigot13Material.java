package com.degoos.wetsponge.material;

import com.degoos.wetsponge.nbt.WSNBTTagCompound;
import org.bukkit.inventory.meta.ItemMeta;

public interface Spigot13Material extends WSMaterial {

	void writeItemMeta(ItemMeta itemMeta);

	void readItemMeta(ItemMeta itemMeta);

	void writeNBTTag(WSNBTTagCompound compound);

	void readNBTTag(WSNBTTagCompound compound);

}

package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.Spigot13BlockTypeMultipleFacing;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.Fence;

import java.util.Objects;
import java.util.Set;

public class Spigot13BlockTypeFence extends Spigot13BlockTypeMultipleFacing implements WSBlockTypeFence {

	private boolean waterlogged;

	public Spigot13BlockTypeFence(int numericalId, String oldStringId, String newStringId, int maxStackSize, Set<EnumBlockFace> faces, Set<EnumBlockFace> allowedFaces, boolean waterlogged) {
		super(numericalId, oldStringId, newStringId, maxStackSize, faces, allowedFaces);
		this.waterlogged = waterlogged;
	}

	@Override
	public boolean isWaterlogged() {
		return waterlogged;
	}

	@Override
	public void setWaterlogged(boolean waterlogged) {
		this.waterlogged = waterlogged;
	}

	@Override
	public Spigot13BlockTypeFence clone() {
		return new Spigot13BlockTypeFence(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), getFaces(), getAllowedFaces(), waterlogged);
	}

	@Override
	public BlockData toBlockData() {
		BlockData blockData = super.toBlockData();
		if(blockData instanceof Fence) {
			((Fence) blockData).setWaterlogged(waterlogged);
		}
		return blockData;
	}

	@Override
	public Spigot13BlockTypeFence readBlockData(BlockData blockData) {
		super.readBlockData(blockData);
		if(blockData instanceof Fence) {
			waterlogged = ((Fence) blockData).isWaterlogged();
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeFence that = (Spigot13BlockTypeFence) o;
		return waterlogged == that.waterlogged;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), waterlogged);
	}
}

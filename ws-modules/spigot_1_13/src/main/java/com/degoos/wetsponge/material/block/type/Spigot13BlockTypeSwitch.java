package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.enums.block.EnumBlockTypeSwitchFace;
import com.degoos.wetsponge.material.block.Spigot13BlockTypeDirectional;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.Switch;

import java.util.Objects;
import java.util.Set;

public class Spigot13BlockTypeSwitch extends Spigot13BlockTypeDirectional implements WSBlockTypeSwitch {

	private EnumBlockTypeSwitchFace switchFace;
	private boolean powered;

	public Spigot13BlockTypeSwitch(int numericalId, String oldStringId, String newStringId, int maxStackSize,
								   EnumBlockFace facing, Set<EnumBlockFace> faces, EnumBlockTypeSwitchFace switchFace, boolean powered) {
		super(numericalId, oldStringId, newStringId, maxStackSize, facing, faces);
		Validate.notNull(switchFace, "Switch face cannot be null!");
		this.switchFace = switchFace;
		this.powered = powered;
	}

	@Override
	public EnumBlockTypeSwitchFace getSwitchFace() {
		return switchFace;
	}

	@Override
	public void setSwitchFace(EnumBlockTypeSwitchFace face) {
		Validate.notNull(face, "Switch face cannot be null!");
		this.switchFace = face;
	}

	@Override
	public boolean isPowered() {
		return powered;
	}

	@Override
	public void setPowered(boolean powered) {
		this.powered = powered;
	}

	@Override
	public Spigot13BlockTypeSwitch clone() {
		return new Spigot13BlockTypeSwitch(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), getFacing(), getFaces(), switchFace, powered);
	}

	@Override
	public BlockData toBlockData() {
		BlockData blockData = super.toBlockData();
		if (blockData instanceof Switch) {
			((Switch) blockData).setFace(Switch.Face.valueOf(switchFace.name()));
			((Switch) blockData).setPowered(powered);
		}
		return blockData;
	}

	@Override
	public Spigot13BlockTypeSwitch readBlockData(BlockData blockData) {
		super.readBlockData(blockData);
		if (blockData instanceof Switch) {
			switchFace = EnumBlockTypeSwitchFace.valueOf(((Switch) blockData).getFace().name());
			powered = ((Switch) blockData).isPowered();
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeSwitch that = (Spigot13BlockTypeSwitch) o;
		return powered == that.powered &&
				switchFace == that.switchFace;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), switchFace, powered);
	}
}

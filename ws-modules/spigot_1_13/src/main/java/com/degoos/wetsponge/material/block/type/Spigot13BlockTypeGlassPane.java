package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.Spigot13BlockTypeMultipleFacing;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.GlassPane;

import java.util.Objects;
import java.util.Set;

public class Spigot13BlockTypeGlassPane extends Spigot13BlockTypeMultipleFacing implements WSBlockTypeGlassPane {

	private boolean waterlogged;

	public Spigot13BlockTypeGlassPane(int numericalId, String oldStringId, String newStringId, int maxStackSize, Set<EnumBlockFace> faces, Set<EnumBlockFace> allowedFaces, boolean waterlogged) {
		super(numericalId, oldStringId, newStringId, maxStackSize, faces, allowedFaces);
		this.waterlogged = waterlogged;
	}

	@Override
	public boolean isWaterlogged() {
		return waterlogged;
	}

	@Override
	public void setWaterlogged(boolean waterlogged) {
		this.waterlogged = waterlogged;
	}

	@Override
	public Spigot13BlockTypeGlassPane clone() {
		return new Spigot13BlockTypeGlassPane(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), getFaces(), getAllowedFaces(), waterlogged);
	}

	@Override
	public BlockData toBlockData() {
		BlockData blockData = super.toBlockData();
		if (blockData instanceof GlassPane) {
			((GlassPane) blockData).setWaterlogged(waterlogged);
		}
		return blockData;
	}

	@Override
	public Spigot13BlockTypeGlassPane readBlockData(BlockData blockData) {
		super.readBlockData(blockData);
		if (blockData instanceof GlassPane) {
			waterlogged = ((GlassPane) blockData).isWaterlogged();
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeGlassPane that = (Spigot13BlockTypeGlassPane) o;
		return waterlogged == that.waterlogged;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), waterlogged);
	}
}

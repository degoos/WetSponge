package com.degoos.wetsponge.world.generation;

import com.degoos.wetsponge.listener.spigot.Spigot13WorldListener;
import com.degoos.wetsponge.parser.world.WorldParser;
import com.degoos.wetsponge.world.generation.populator.WSGenerationPopulator;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.generator.BlockPopulator;
import org.bukkit.generator.ChunkGenerator;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Spigot13WorldGenerator extends ChunkGenerator implements WSWorldGenerator {

	private WSGenerationPopulator populator;

	public Spigot13WorldGenerator() {
	}

	public Spigot13WorldGenerator(WSGenerationPopulator populator) {
		this.populator = populator;
	}

	@Override
	public void setBaseGenerationPopulator(WSGenerationPopulator populator) {
		this.populator = populator;
		Bukkit.getWorlds().stream().filter(world -> equals(world.getGenerator()))
				.forEach(world -> Spigot13WorldListener.refreshGenerator(world, world.getGenerator(), this.populator == null));
	}

	@Override
	public boolean canSpawn(World world, int x, int z) {
		return true;
	}

	public byte[] generate(World world, Random random, int x, int z) {
		return new byte[(int) Math.pow(2, 10)];
	}

	@Override
	public List<BlockPopulator> getDefaultPopulators(World world) {
		return Arrays.asList(new BlockPopulator[0]);
	}

	@Override
	public ChunkData generateChunkData(World world, Random random, int x, int z, BiomeGrid biome) {
		ChunkData data = Bukkit.getServer().createChunkData(world);
		if (populator != null) populator.populate(WorldParser.getOrCreateWorld(world.getName(), world),
				new Spigot13BlockVolume(data, x, z, random, biome));
		return data;
	}

	@Override
	public ChunkGenerator getHandler() {
		return this;
	}

}

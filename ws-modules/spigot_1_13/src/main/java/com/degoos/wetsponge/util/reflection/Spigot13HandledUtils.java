package com.degoos.wetsponge.util.reflection;

import com.degoos.wetsponge.material.WSBlockTypes;
import com.degoos.wetsponge.material.block.Spigot13BlockType;
import com.degoos.wetsponge.material.block.WSBlockType;
import com.flowpowered.math.vector.Vector3d;
import com.flowpowered.math.vector.Vector3i;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.BlockState;
import org.bukkit.block.data.BlockData;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import java.util.Optional;

public class Spigot13HandledUtils {

	public static Object getWorldHandle(World w) {
		try {
			return w.getClass().getMethod("getHandle").invoke(w);
		} catch (Throwable e) {
			e.printStackTrace();
			return null;
		}
	}


	public static Object getPlayerHandle(Player pl) {
		return getEntityHandle(pl);
	}

	public static Object getEntityHandle(Entity entity) {
		try {
			return entity.getClass().getMethod("getHandle").invoke(entity);
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return null;
	}

	public static Object getTileEntity(BlockState state) {
		try {
			Class<?> clazz = NMSUtils.getNMSClass("block.CraftBlockEntityState");
			return clazz.getDeclaredMethod("getTileEntity").invoke(state);
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return null;
	}

	public static Object getServerHandle(Object object) {
		try {
			return object.getClass().getMethod("getServer").invoke(object);
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return null;
	}

	public static Object getHandle(Object object) {
		try {
			return object.getClass().getMethod("getHandle").invoke(object);
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return null;
	}

	public static Object getBlockPosition(Location loc) {
		return getBlockPosition(loc.getX(), loc.getY(), loc.getZ());
	}

	public static Object getBlockPosition(Vector3d loc) {
		return getBlockPosition(loc.getX(), loc.getY(), loc.getZ());
	}

	public static Object getBlockPosition(Vector3i loc) {
		return getBlockPosition(loc.getX(), loc.getY(), loc.getZ());
	}


	public static Object getBlockPosition(double x, double y, double z) {
		try {
			return NMSUtils.getNMSClass("BlockPosition").getConstructor(double.class, double.class, double.class).newInstance(x, y, z);
		} catch (Throwable ex) {
			return null;
		}
	}

	public static Object getBlockPosition(int x, int y, int z) {
		try {
			return NMSUtils.getNMSClass("BlockPosition").getConstructor(int.class, int.class, int.class).newInstance(x, y, z);
		} catch (Throwable ex) {
			return null;
		}
	}

	public static Vector3i getBlockPositionVector(Object position) {
		try {
			return new Vector3i((int) position.getClass().getMethod("getX").invoke(position), (int) position.getClass().getMethod("getY").invoke(position), (int)
					position
							.getClass().getMethod("getZ").invoke(position));
		} catch (Throwable ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public static Object getBlockState(WSBlockType material) {
		try {
			BlockData data = ((Spigot13BlockType) material).toBlockData();
			Class<?> blockDataClass = NMSUtils.getOBCClass("block.data.CraftBlockData");
			return blockDataClass.getMethod("getState").invoke(data);
		} catch (Throwable ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public static WSBlockType getMaterial(Object blockState) {
		try {
			Class<?> blockDataClass = NMSUtils.getOBCClass("block.data.CraftBlockData");
			Class<?> iBlockDataClass = NMSUtils.getNMSClass("IBlockData");

			BlockData blockData = (BlockData) blockDataClass.getMethod("fromData", iBlockDataClass).invoke(null, blockState);
			String id = blockData.getMaterial().getKey().toString();
			Optional<WSBlockType> optional = WSBlockTypes.getById(id);
			if (optional.isPresent()) return ((Spigot13BlockType) optional.get()).readBlockData(blockData);
			return new Spigot13BlockType(-1, id, id, blockData.getMaterial().getMaxStackSize());
		} catch (Throwable ex) {
			ex.printStackTrace();
			return null;
		}
	}


}

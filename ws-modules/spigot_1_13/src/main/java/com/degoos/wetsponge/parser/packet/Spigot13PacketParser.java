package com.degoos.wetsponge.parser.packet;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.packet.Spigot13Packet;
import com.degoos.wetsponge.packet.WSPacket;
import com.degoos.wetsponge.packet.play.client.*;
import com.degoos.wetsponge.packet.play.server.*;
import com.degoos.wetsponge.util.reflection.NMSUtils;

public class Spigot13PacketParser {

	public static WSPacket parse(Object packet) {
		//SERVER
		if (NMSUtils.getNMSClass("PacketPlayOutAnimation").isInstance(packet)) return new Spigot13SPacketAnimation(packet);
		if (NMSUtils.getNMSClass("PacketPlayOutBlockChange").isInstance(packet)) return new Spigot13SPacketBlockChange(packet);
		if (NMSUtils.getNMSClass("PacketPlayOutMultiBlockChange").isInstance(packet)) return new Spigot13SPacketMultiBlockChange(packet);
		if (NMSUtils.getNMSClass("PacketPlayOutSpawnEntityWeather").isInstance(packet)) return new Spigot13SPacketSpawnGlobalEntity(packet);
		if (NMSUtils.getNMSClass("PacketPlayOutSpawnEntityExperienceOrb").isInstance(packet)) return new Spigot13SPacketSpawnExperienceOrb(packet);
		if (NMSUtils.getNMSClass("PacketPlayOutSpawnEntityLiving").isInstance(packet)) return new Spigot13SPacketSpawnMob(packet);
		if (NMSUtils.getNMSClass("PacketPlayOutSpawnEntity").isInstance(packet)) return new Spigot13SPacketSpawnObject(packet);
		if (NMSUtils.getNMSClass("PacketPlayOutNamedEntitySpawn").isInstance(packet)) return new Spigot13SPacketSpawnPlayer(packet);
		if (NMSUtils.getNMSClass("PacketPlayOutEntityDestroy").isInstance(packet)) return new Spigot13SPacketDestroyEntities(packet);
		if (NMSUtils.getNMSClass("PacketPlayOutCloseWindow").isInstance(packet)) return new Spigot13SPacketCloseWindows(packet);
		if (NMSUtils.getNMSClass("PacketPlayOutOpenSignEditor").isInstance(packet)) return new Spigot13SPacketSignEditorOpen(packet);
		if (NMSUtils.getNMSClass("PacketPlayOutEntity$PacketPlayOutRelEntityMoveLook").isInstance(packet)) return new Spigot13SPacketEntityLookMove(packet);
		if (NMSUtils.getNMSClass("PacketPlayOutEntity$PacketPlayOutEntityLook").isInstance(packet)) return new Spigot13SPacketEntityLook(packet);
		if (NMSUtils.getNMSClass("PacketPlayOutEntity$PacketPlayOutRelEntityMove").isInstance(packet)) return new Spigot13SPacketEntityRelMove(packet);
		if (NMSUtils.getNMSClass("PacketPlayOutEntity").isInstance(packet)) return new Spigot13SPacketEntityLookMove(packet);
		if (NMSUtils.getNMSClass("PacketPlayOutHeldItemSlot").isInstance(packet)) return new Spigot13SPacketHeldItemChange(packet);
		if (NMSUtils.getNMSClass("PacketPlayOutEntityMetadata").isInstance(packet)) return new Spigot13SPacketEntityMetadata(packet);
		if (NMSUtils.getNMSClass("PacketPlayOutEntityTeleport").isInstance(packet)) return new Spigot13SPacketEntityTeleport(packet);
		if (NMSUtils.getNMSClass("PacketPlayOutEntityHeadRotation").isInstance(packet)) return new Spigot13SPacketEntityHeadLook(packet);
		if (NMSUtils.getNMSClass("PacketPlayOutUpdateAttributes").isInstance(packet)) return new Spigot13SPacketEntityProperties(packet);
		if (NMSUtils.getNMSClass("PacketPlayOutMap").isInstance(packet)) return new Spigot13SPacketMaps(packet);
		if (NMSUtils.getNMSClass("PacketStatusOutServerInfo").isInstance(packet)) return new Spigot13SPacketServerInfo(packet);
		if (NMSUtils.getNMSClass("PacketPlayOutWindowItems").isInstance(packet)) return new Spigot13SPacketWindowItems(packet);
		if (NMSUtils.getNMSClass("PacketPlayOutSetSlot").isInstance(packet)) return new Spigot13SPacketSetSlot(packet);
		if (NMSUtils.getNMSClass("PacketPlayOutOpenWindow").isInstance(packet)) return new Spigot13SPacketOpenWindow(packet);
		if (NMSUtils.getNMSClass("PacketPlayOutPlayerInfo").isInstance(packet)) return new Spigot13SPacketPlayerListItem(packet);
		if (NMSUtils.getNMSClass("PacketPlayOutEntityEquipment").isInstance(packet)) return new Spigot13SPacketEntityEquipment(packet);
		if (NMSUtils.getNMSClass("PacketPlayOutBed").isInstance(packet)) return new Spigot13SPacketUseBed(packet);

		//CLIENT
		if (NMSUtils.getNMSClass("PacketPlayInCloseWindow").isInstance(packet)) return new Spigot13CPacketCloseWindows(packet);
		if (NMSUtils.getNMSClass("PacketPlayInUpdateSign").isInstance(packet)) return new Spigot13CPacketUpdateSign(packet);
		if (NMSUtils.getNMSClass("PacketPlayInUseEntity").isInstance(packet)) return new Spigot13CPacketUseEntity(packet);
		if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD) && NMSUtils.getNMSClass("PacketPlayInUseItem").isInstance(packet))
			return new Spigot13CPacketPlayerTryUseItemOnBlock(packet);
		if (NMSUtils.getNMSClass("PacketPlayInEntityAction").isInstance(packet)) return new Spigot13CPacketEntityAction(packet);

		return new Spigot13Packet(packet) {
			@Override
			public void update() {
			}

			@Override
			public void refresh() {
			}

			@Override
			public boolean hasChanged() {
				return false;
			}
		};
	}

}

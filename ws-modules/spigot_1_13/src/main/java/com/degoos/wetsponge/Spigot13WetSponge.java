package com.degoos.wetsponge;


import com.degoos.wetsponge.command.wetspongecommand.Spigot13WetspongeCommand;
import com.degoos.wetsponge.enums.EnumEntityType;
import com.degoos.wetsponge.enums.EnumServerType;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.enums.EnumTextColor;
import com.degoos.wetsponge.loader.Spigot13ListenerLoader;
import com.degoos.wetsponge.loader.SpigotWetSpongeLoader;
import com.degoos.wetsponge.mixin.spigot.Spigot13Injector;
import com.degoos.wetsponge.parser.entity.Spigot13EntityParser;
import com.degoos.wetsponge.resource.spigot.Spigot13BungeeCord;
import com.degoos.wetsponge.server.Spigot13Server;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.reflection.Spigot13ServerUtils;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Arrays;

public class Spigot13WetSponge implements SpigotWetSpongeLoader {


	public void onEnable(JavaPlugin instance, EnumServerVersion version) {
		new BukkitRunnable() {
			@Override
			public void run() {
				try {
					long millis = System.currentTimeMillis();
					EnumServerType type = EnumServerType.SPIGOT;
					try {
						Class.forName("com.destroystokyo.paper.PaperConfig");
						type = EnumServerType.PAPER_SPIGOT;
					} catch (ClassNotFoundException ignore) {
					}

					WetSponge.load(instance.getDescription().getVersion(), type, Spigot13WetSponge.this, new Spigot13Server(Bukkit.getServer()), version,
							new Spigot13BungeeCord(instance), Spigot13ServerUtils.getMainThread());

					InternalLogger.sendInfo("Loading WetSponge " + instance.getDescription().getVersion() + "...");

					InternalLogger
							.sendInfo(WSText.builder("Using version ").append(WSText.builder("SPIGOT " + version.name()).color(EnumTextColor.GREEN).build()).build());

					InternalLogger.sendInfo("Loading entities.");
					Spigot13EntityParser.load();
					Arrays.stream(EnumEntityType.values()).forEach(EnumEntityType::load);
					InternalLogger.sendInfo("Loading Spigot listeners.");
					Spigot13ListenerLoader.load();

					InternalLogger.sendInfo("Loading injectors.");
					Spigot13Injector.inject(instance);

					InternalLogger.sendInfo("Loading common.");
					WetSponge.loadCommon();

					instance.getCommand("wetSpongeCmd").setExecutor(new Spigot13WetspongeCommand());

					double secs = (System.currentTimeMillis() - millis) / 1000D;
					InternalLogger.sendDone(WSText.builder("WetSponge has been loaded in ").append(WSText.builder(String.valueOf(secs)).color(EnumTextColor.RED).build())
							.append(WSText.builder(" seconds!").color(EnumTextColor.GREEN).build()).build());
				} catch (Throwable ex) {
					InternalLogger.printException(ex, "An error has occurred while WetSponge was loading!");
				}
			}
		}.runTaskLater(instance, 1);
	}

	@Override
	public void onDisable() {
		try {
			WetSponge.unloadCommon();
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was unloading!");
		}
	}
}
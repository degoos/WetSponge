package com.degoos.wetsponge.entity.living.animal;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.AbstractHorse;
import org.bukkit.entity.AnimalTamer;
import org.bukkit.inventory.ItemStack;

import java.util.Optional;
import java.util.UUID;

public class Spigot13AbstractHorse extends Spigot13Animal implements WSAbstractHorse {

	public Spigot13AbstractHorse(AbstractHorse entity) {
		super(entity);
	}

	@Override
	public int getDomestication() {
		return getHandled().getDomestication();
	}

	@Override
	public void setDomestication(int domestication) {
		getHandled().setDomestication(domestication);
	}

	@Override
	public int getMaxDomestication() {
		return getHandled().getMaxDomestication();
	}

	@Override
	public void setMaxDomestication(int maxDomestication) {
		getHandled().setMaxDomestication(maxDomestication);
	}

	@Override
	public double getJumpStrength() {
		return getHandled().getJumpStrength();
	}

	@Override
	public void setJumpStrength(double jumpStrength) {
		getHandled().setJumpStrength(jumpStrength);
	}

	@Override
	public boolean hasSaddle() {
		return getHandled().getInventory().getSaddle() != null && getHandled().getInventory().getSaddle().getType() == Material.SADDLE;
	}

	@Override
	public void setSaddle(boolean saddle) {
		if (saddle) getHandled().getInventory().setSaddle(new ItemStack(Material.SADDLE));
		else getHandled().getInventory().setSaddle(null);
	}

	@Override
	public boolean isTamed() {
		return getHandled().isTamed();
	}

	@Override
	public Optional<UUID> getTamer() {
		AnimalTamer tamer = getHandled().getOwner();
		if (tamer == null) return Optional.empty();
		return Optional.of(tamer.getUniqueId());
	}

	@Override
	public void setTamer(UUID tamer) {
		getHandled().setOwner(tamer == null ? null : Bukkit.getOfflinePlayer(tamer));
	}


	@Override
	public AbstractHorse getHandled() {
		return (AbstractHorse) super.getHandled();
	}
}

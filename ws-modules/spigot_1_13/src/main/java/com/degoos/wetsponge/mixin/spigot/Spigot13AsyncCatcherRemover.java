package com.degoos.wetsponge.mixin.spigot;

import com.degoos.wetsponge.util.InternalLogger;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.spigotmc.AsyncCatcher;

public class Spigot13AsyncCatcherRemover {


	public static void load(JavaPlugin javaPlugin) {
		try {
			AsyncCatcher.enabled = false;
			new BukkitRunnable() {
				@Override
				public void run() {
					if (AsyncCatcher.enabled) AsyncCatcher.enabled = false;
				}
			}.runTaskTimer(javaPlugin, 20, 20);
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An exception has occurred while WetSponge was injecting code!");
		}
	}

}

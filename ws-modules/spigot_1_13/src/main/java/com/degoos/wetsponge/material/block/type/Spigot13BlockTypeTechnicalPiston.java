package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.enums.block.EnumBlockTypePistonType;
import com.degoos.wetsponge.material.block.Spigot13BlockTypeDirectional;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.TechnicalPiston;

import java.util.Objects;
import java.util.Set;

public class Spigot13BlockTypeTechnicalPiston extends Spigot13BlockTypeDirectional implements WSBlockTypeTechnicalPiston {

	private EnumBlockTypePistonType pistonType;

	public Spigot13BlockTypeTechnicalPiston(int numericalId, String oldStringId, String newStringId, int maxStackSize, EnumBlockFace facing, Set<EnumBlockFace> faces, EnumBlockTypePistonType pistonType) {
		super(numericalId, oldStringId, newStringId, maxStackSize, facing, faces);
		this.pistonType = pistonType;
	}

	@Override
	public EnumBlockTypePistonType getType() {
		return pistonType;
	}

	@Override
	public void setType(EnumBlockTypePistonType type) {
		this.pistonType = type;
	}

	@Override
	public Spigot13BlockTypeTechnicalPiston clone() {
		return new Spigot13BlockTypeTechnicalPiston(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), getFacing(), getFaces(), pistonType);
	}

	@Override
	public BlockData toBlockData() {
		BlockData blockData = super.toBlockData();
		if (blockData instanceof TechnicalPiston) {
			((TechnicalPiston) blockData).setType(TechnicalPiston.Type.valueOf(pistonType.name()));
		}
		return blockData;
	}

	@Override
	public Spigot13BlockTypeTechnicalPiston readBlockData(BlockData blockData) {
		super.readBlockData(blockData);
		if (blockData instanceof TechnicalPiston) {
			pistonType = EnumBlockTypePistonType.valueOf(((TechnicalPiston) blockData).getType().name());
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeTechnicalPiston that = (Spigot13BlockTypeTechnicalPiston) o;
		return pistonType == that.pistonType;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), pistonType);
	}
}

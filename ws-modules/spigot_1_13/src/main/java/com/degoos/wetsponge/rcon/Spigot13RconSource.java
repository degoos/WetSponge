package com.degoos.wetsponge.rcon;


import com.degoos.wetsponge.command.Spigot13CommandSource;
import org.bukkit.command.RemoteConsoleCommandSender;

public class Spigot13RconSource extends Spigot13CommandSource implements WSRconSource {

	public Spigot13RconSource(RemoteConsoleCommandSender source) {
		super(source);
	}


	@Override
	public String getName () {
		return getHandled().getName();
	}


	@Override
	public RemoteConsoleCommandSender getHandled () {
		return (RemoteConsoleCommandSender) super.getHandled();
	}
}

package com.degoos.wetsponge.util.reflection;

import java.net.Proxy;

public class Spigot13ServerUtils {


	public static Thread getMainThread() {
		try {
			Class<?> clazz = NMSUtils.getNMSClass("MinecraftServer");
			Object server = clazz.getMethod("getServer").invoke(null);
			return (Thread) clazz.getField("primaryThread").get(server);
		} catch (Exception ex) {
			ex.printStackTrace();
			return Thread.currentThread();
		}
	}

	public static Proxy getProxy() {
		try {
			Class<?> clazz = NMSUtils.getNMSClass("MinecraftServer");
			Object server = clazz.getMethod("getServer").invoke(null);
			return ReflectionUtils.getFirstObject(clazz, Proxy.class, server);
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}


}

package com.degoos.wetsponge.world.generation;


import com.degoos.wetsponge.material.WSBlockTypes;
import com.degoos.wetsponge.material.block.Spigot13BlockType;
import com.degoos.wetsponge.material.block.WSBlockType;
import com.flowpowered.math.vector.Vector3i;
import org.bukkit.Material;
import org.bukkit.block.data.BlockData;
import org.bukkit.generator.ChunkGenerator;

import java.util.Optional;
import java.util.Random;

public class Spigot13BlockVolume implements WSBlockVolume {

	private ChunkGenerator.ChunkData data;
	private int x, z;
	private Random random;
	private ChunkGenerator.BiomeGrid biome;

	public Spigot13BlockVolume(ChunkGenerator.ChunkData data, int x, int z, Random random, ChunkGenerator.BiomeGrid biome) {
		this.data = data;
		this.x = x;
		this.z = z;
		this.random = random;
		this.biome = biome;
	}

	@Override
	public Vector3i getBlockMin() {
		return new Vector3i(x * 16, 0, z * 16);
	}

	@Override
	public Vector3i getBlockMax() {
		return new Vector3i(x * 16 + 15, data.getMaxHeight(), z * 16 + 15);
	}

	@Override
	public Vector3i getBlockSize() {
		return getBlockMax().sub(getBlockMin()).add(1, 1, 1);
	}

	@Override
	public boolean containsBlock(int x, int y, int z) {
		return data.getType(x, y, z) == Material.AIR;
	}

	@Override
	public WSBlockType getBlock(int x, int y, int z) {
		BlockData blockData = data.getBlockData(x % 16, y, z % 16);
		if (blockData == null) return WSBlockTypes.AIR.getDefaultState();
		String id = blockData.getMaterial().getKey().toString();
		Optional<WSBlockType> optional = WSBlockTypes.getById(id);
		if (optional.isPresent()) return ((Spigot13BlockType) optional.get()).readBlockData(blockData);
		else return new Spigot13BlockType(-1, id, id, blockData.getMaterial().getMaxStackSize());
	}

	@Override
	public boolean setBlock(int x, int y, int z, WSBlockType blockType) {
		data.setBlock(x % 16, y, z % 16, blockType == null ? null : ((Spigot13BlockType) blockType).toBlockData());
		return true;
	}

	public int getX() {
		return x;
	}

	public int getZ() {
		return z;
	}

	public Random getRandom() {
		return random;
	}

	public ChunkGenerator.BiomeGrid getBiome() {
		return biome;
	}

	@Override
	public ChunkGenerator.ChunkData getHandled() {
		return data;
	}
}

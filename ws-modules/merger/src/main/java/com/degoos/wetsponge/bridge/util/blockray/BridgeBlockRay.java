package com.degoos.wetsponge.bridge.util.blockray;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.util.blockray.Spigot13BlockRay;
import com.degoos.wetsponge.util.blockray.SpigotBlockRay;
import com.degoos.wetsponge.util.blockray.SpongeBlockRay;
import com.degoos.wetsponge.util.blockray.WSBlockRay;
import com.degoos.wetsponge.world.WSWorld;

public class BridgeBlockRay {

	public static WSBlockRay.Builder builder(WSWorld world) {
		switch (WetSponge.getServerType()) {
			case SPONGE:
				return new SpongeBlockRay.Builder(world);
			case PAPER_SPIGOT:
			case SPIGOT:
				if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
					return new SpigotBlockRay.Builder(world);
				return new Spigot13BlockRay.Builder(world);
			default:
				return null;
		}
	}

}

package com.degoos.wetsponge;

import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.loader.SpigotWetSpongeLoader;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class SpigotWetSponge extends JavaPlugin {

	private SpigotWetSpongeLoader loader;
	private static JavaPlugin instance;

	@Override
	public void onEnable() {
		instance = this;
		String serverVersion = Bukkit.getServer().getClass().getPackage().getName();
		EnumServerVersion version = EnumServerVersion.getBySpigotVersionName(serverVersion.substring(serverVersion.lastIndexOf(".") + 1));
		if (version.isOlderThan(EnumServerVersion.MINECRAFT_1_13)) {
			loader = new SpigotOldWetSponge();
			((SpigotOldWetSponge) loader).onEnable(this, version);
		} else {
			loader = new Spigot13WetSponge();
			((Spigot13WetSponge) loader).onEnable(this, version);
		}
	}


	@Override
	public void onDisable() {
		loader.onDisable();
	}

	public static JavaPlugin getInstance() {
		return instance;
	}
}

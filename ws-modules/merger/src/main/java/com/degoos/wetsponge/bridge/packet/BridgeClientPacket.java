package com.degoos.wetsponge.bridge.packet;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumEntityAction;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.packet.play.client.*;
import com.flowpowered.math.vector.Vector3d;

public class BridgeClientPacket {

	public static WSCPacketCloseWindows newWSCPacketCloseWindows(int windowsId) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
						return new SpigotCPacketCloseWindows(windowsId);
					return new Spigot13CPacketCloseWindows(windowsId);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeCPacketCloseWindows(windowsId);
			default:
				return null;
		}
	}

	public static WSCPacketEntityAction newWSCPacketEntityAction(int entityId, EnumEntityAction entityAction, int jumpBoost) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
						return new SpigotCPacketEntityAction(entityId, entityAction, jumpBoost);
					return new Spigot13CPacketEntityAction(entityId, entityAction, jumpBoost);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeCPacketEntityAction(entityId, entityAction, jumpBoost);
			default:
				return null;
		}
	}

	public static WSCPacketUpdateSign newWSCPacketUpdateSign(Vector3d position, String[] lines) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
						return new SpigotCPacketUpdateSign(position, lines);
					return new Spigot13CPacketUpdateSign(position, lines);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeCPacketUpdateSign(position, lines);
			default:
				return null;
		}
	}
}

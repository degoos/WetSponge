package com.degoos.wetsponge.bridge.material;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.text.translation.Spigot13Translation;
import com.degoos.wetsponge.text.translation.WSTranslation;
import com.degoos.wetsponge.util.SpongeTranslationUtils;
import com.degoos.wetsponge.util.reflection.NMSUtils;

public class BridgeMaterial {

	public static WSTranslation getTranslation(int id, String stringId) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					Class<?> itemClazz = NMSUtils.getNMSClass("Item");
					return new Spigot13Translation(itemClazz.getMethod("getName").invoke(itemClazz.getMethod("getById", int.class).invoke(null, id)) + ".name");
				} catch (Exception e) {
					return new Spigot13Translation("");
				}
			case SPONGE:
				return SpongeTranslationUtils.getMaterialTranslation(stringId);
			default:
				return null;
		}
	}

}

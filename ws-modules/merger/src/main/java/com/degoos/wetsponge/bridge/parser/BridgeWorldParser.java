package com.degoos.wetsponge.bridge.parser;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.world.Spigot13World;
import com.degoos.wetsponge.world.SpigotWorld;
import com.degoos.wetsponge.world.SpongeWorld;
import com.degoos.wetsponge.world.WSWorld;

public class BridgeWorldParser {

	public static WSWorld getWorld(Object world) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
					return new SpigotWorld(world);
				return new Spigot13World(world);
			case SPONGE:
				return new SpongeWorld(world);
			default:
				return null;
		}
	}

}

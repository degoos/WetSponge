package com.degoos.wetsponge.bridge.sound;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.enums.EnumSoundCategory;
import com.degoos.wetsponge.sound.Spigot13SoundHandler;
import com.degoos.wetsponge.sound.SpigotSoundHandler;
import com.degoos.wetsponge.sound.SpongeSoundHandler;
import com.degoos.wetsponge.sound.WSSound;
import com.flowpowered.math.vector.Vector3d;

public class BridgeSound {

	public static void playSound(WSSound sound, WSPlayer player, float volume, float pitch, EnumSoundCategory category, Vector3d position) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
					SpigotSoundHandler.playSound(sound, category, player, position, volume, pitch);
				else
					Spigot13SoundHandler.playSound(sound, category, player, position, volume, pitch);
				break;
			case SPONGE:
				SpongeSoundHandler.playSound(sound, category, player, position, volume, pitch);
				break;
			default:
				break;
		}
	}

	public static void playSound(String sound, float volume, float pitch, EnumSoundCategory category, Vector3d position, WSPlayer player) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
					SpigotSoundHandler.playSound(sound, category, player, position, volume, pitch);
				else
					Spigot13SoundHandler.playSound(sound, category, player, position, volume, pitch);
				break;
			case SPONGE:
				SpongeSoundHandler.playSound(sound, category, player, position, volume, pitch);
				break;
		}
	}

}

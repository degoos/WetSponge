package com.degoos.wetsponge.bridge.item.enchantment;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.item.enchantment.Spigot13Enchantment;
import com.degoos.wetsponge.item.enchantment.SpigotEnchantment;
import com.degoos.wetsponge.item.enchantment.SpongeEnchantment;
import com.degoos.wetsponge.item.enchantment.WSEnchantment;

public class BridgeEnchantment {

	public static WSEnchantment of(String spongeId, int spigotId, String minecraftKey) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
					return new SpigotEnchantment(spigotId);
				else return new Spigot13Enchantment(minecraftKey);
			case SPONGE:
				return new SpongeEnchantment(spongeId);
			default:
				return null;
		}
	}

}

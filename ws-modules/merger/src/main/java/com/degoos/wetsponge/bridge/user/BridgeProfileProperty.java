package com.degoos.wetsponge.bridge.user;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.user.Spigot13ProfileProperty;
import com.degoos.wetsponge.user.SpigotProfileProperty;
import com.degoos.wetsponge.user.SpongeProfileProperty;
import com.degoos.wetsponge.user.WSProfileProperty;

public class BridgeProfileProperty {

	public static WSProfileProperty of(String name, String value, String signature) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
					return SpigotProfileProperty.of(name, value, signature);
				return Spigot13ProfileProperty.of(name, value, signature);
			case SPONGE:
				return SpongeProfileProperty.of(name, value, signature);
			default:
				return null;
		}
	}

}

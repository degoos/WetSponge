package com.degoos.wetsponge.entity.weather;

import org.bukkit.entity.LightningStrike;

public class SpigotLightning extends SpigotWeatherEffect implements WSLightning {

	public SpigotLightning(LightningStrike entity) {
		super(entity);
	}

	@Override
	public LightningStrike getHandled() {
		return (LightningStrike) super.getHandled();
	}
}

package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.material.block.SpigotBlockTypeLevelled;
import org.bukkit.material.MaterialData;

public class SpigotBlockTypeLava extends SpigotBlockTypeLevelled implements WSBlockTypeLava {

	public SpigotBlockTypeLava(int level, int maximumLevel) {
		super(9, "minecraft:lava", "minecraft:lava", 64, level, maximumLevel);
	}

	@Override
	public int getNumericalId() {
		return getLevel() == 0 ? 11 : 10;
	}

	@Override
	public String getOldStringId() {
		return getLevel() == 0 ? "minecraft:lava" : "minecraft:flowing_lava";
	}

	@Override
	public SpigotBlockTypeLava clone() {
		return new SpigotBlockTypeLava(getLevel(), getMaximumLevel());
	}

	@Override
	public MaterialData toMaterialData() {
		return super.toMaterialData();
	}

	@Override
	public SpigotBlockTypeLava readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		return this;
	}
}

package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockTypeSlabPosition;
import com.degoos.wetsponge.enums.block.EnumBlockTypeSlabType;
import com.degoos.wetsponge.material.block.SpigotBlockTypeWaterlogged;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.material.MaterialData;

import java.util.Objects;

public class SpigotBlockTypeSlab extends SpigotBlockTypeWaterlogged implements WSBlockTypeSlab {

	private EnumBlockTypeSlabType type;
	private EnumBlockTypeSlabPosition position;

	public SpigotBlockTypeSlab(boolean waterLogged, EnumBlockTypeSlabType type, EnumBlockTypeSlabPosition position) {
		super(43, "minecraft:double_stone_slab", "minecraft:stone_slab", 64, waterLogged);
		Validate.notNull(type, "Type cannot be null!");
		Validate.notNull(position, "Position cannot be null!");
		this.type = type;
		this.position = position;
	}


	@Override
	public int getNumericalId() {
		switch (type) {
			case OAK:
			case SPRUCE:
			case BIRCH:
			case JUNGLE:
			case ACACIA:
			case DARK_OAK:
				return position == EnumBlockTypeSlabPosition.DOUBLE ? 125 : 126;
			case STONE:
			case RED_SANDSTONE:
				return position == EnumBlockTypeSlabPosition.DOUBLE ? 181 : 182;
			case PURPUR:
				return position == EnumBlockTypeSlabPosition.DOUBLE ? 204 : 205;
			default:
				return position == EnumBlockTypeSlabPosition.DOUBLE ? 43 : 44;
		}
	}

	@Override
	public String getOldStringId() {
		switch (type) {
			case OAK:
			case SPRUCE:
			case BIRCH:
			case JUNGLE:
			case ACACIA:
			case DARK_OAK:
				return position == EnumBlockTypeSlabPosition.DOUBLE ? "minecraft:double_wooden_slab" : "minecraft:wooden_slab";
			case STONE:
			case RED_SANDSTONE:
				return position == EnumBlockTypeSlabPosition.DOUBLE ? "minecraft:double_stone_slab" : "minecraft:stone_slab";
			case PURPUR:
				return position == EnumBlockTypeSlabPosition.DOUBLE ? "minecraft:double_purpur_slab" : "minecraft:purpur_slab";
			default:
				return position == EnumBlockTypeSlabPosition.DOUBLE ? "minecraft:double_stone_slab2" : "minecraft:stone_slab2";
		}
	}

	@Override
	public String getNewStringId() {
		return type.name().toLowerCase() + "_slab";
	}


	@Override
	public EnumBlockTypeSlabType getType() {
		return type;
	}

	@Override
	public void setType(EnumBlockTypeSlabType type) {
		Validate.notNull(type, "Type cannot be null!");
		this.type = type;
	}

	@Override
	public EnumBlockTypeSlabPosition getPosition() {
		return position;
	}

	@Override
	public void setPosition(EnumBlockTypeSlabPosition position) {
		Validate.notNull(position, "Position cannot be null!");
		this.position = position;
	}

	@Override
	public SpigotBlockTypeSlab clone() {
		return new SpigotBlockTypeSlab(isWaterlogged(), type, position);
	}

	@Override
	public MaterialData toMaterialData() {
		MaterialData data = super.toMaterialData();
		data.setData(getDataValue());
		return data;
	}


	private byte getDataValue() {
		switch (type) {
			case RED_SANDSTONE:
				return (byte) (position == EnumBlockTypeSlabPosition.TOP ? 8 : 0);
			case PURPUR:
				return (byte) (position == EnumBlockTypeSlabPosition.TOP ? 8 : 0);
			case OAK:
				return (byte) (position == EnumBlockTypeSlabPosition.TOP ? 8 : 0);
			case SPRUCE:
				return (byte) (position == EnumBlockTypeSlabPosition.TOP ? 9 : 1);
			case BIRCH:
				return (byte) (position == EnumBlockTypeSlabPosition.TOP ? 10 : 2);
			case JUNGLE:
				return (byte) (position == EnumBlockTypeSlabPosition.TOP ? 11 : 3);
			case ACACIA:
				return (byte) (position == EnumBlockTypeSlabPosition.TOP ? 12 : 4);
			case DARK_OAK:
				return (byte) (position == EnumBlockTypeSlabPosition.TOP ? 13 : 5);
			case STONE:
				return (byte) (position == EnumBlockTypeSlabPosition.TOP ? 8 : 0);
			case SANDSTONE:
				return (byte) (position == EnumBlockTypeSlabPosition.TOP ? 9 : 1);
			case PETRIFIED_OAK:
				return (byte) (position == EnumBlockTypeSlabPosition.TOP ? 10 : 2);
			case COBBLESTONE:
				return (byte) (position == EnumBlockTypeSlabPosition.TOP ? 11 : 3);
			case BRICK:
				return (byte) (position == EnumBlockTypeSlabPosition.TOP ? 12 : 4);
			case STONE_BRICK:
				return (byte) (position == EnumBlockTypeSlabPosition.TOP ? 13 : 5);
			case NETHER_BRICK:
				return (byte) (position == EnumBlockTypeSlabPosition.TOP ? 14 : 6);
			case QUARTZ:
				return (byte) (position == EnumBlockTypeSlabPosition.TOP ? 15 : 7);
			default:
				return 0;
		}
	}

	@Override
	public SpigotBlockTypeSlab readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		int data = materialData.getData();
		int id = getNumericalId();
		int newData = data > 7 ? data - 8 : data;
		switch (id) {
			case 43:
			case 44:
				switch (newData) {
					case 1:
						type = EnumBlockTypeSlabType.SANDSTONE;
						break;
					case 2:
						type = EnumBlockTypeSlabType.PETRIFIED_OAK;
						break;
					case 3:
						type = EnumBlockTypeSlabType.COBBLESTONE;
						break;
					case 5:
						type = EnumBlockTypeSlabType.BRICK;
						break;
					case 6:
						type = EnumBlockTypeSlabType.STONE_BRICK;
						break;
					case 7:
						type = EnumBlockTypeSlabType.QUARTZ;
						break;
					default:
						type = EnumBlockTypeSlabType.STONE;
						break;
				}
				break;
			case 125:
			case 126:
				switch (newData) {
					case 1:
						type = EnumBlockTypeSlabType.SPRUCE;
						break;
					case 2:
						type = EnumBlockTypeSlabType.BIRCH;
						break;
					case 3:
						type = EnumBlockTypeSlabType.JUNGLE;
						break;
					case 4:
						type = EnumBlockTypeSlabType.ACACIA;
						break;
					case 5:
						type = EnumBlockTypeSlabType.DARK_OAK;
						break;
					default:
						type = EnumBlockTypeSlabType.OAK;
						break;
				}
				break;
			default:
				type = EnumBlockTypeSlabType.STONE;
				break;
		}
		if (id == 43 || id == 125 || id == 181 || id == 204) position = EnumBlockTypeSlabPosition.DOUBLE;
		else position = data > 7 ? EnumBlockTypeSlabPosition.TOP : EnumBlockTypeSlabPosition.BOTTOM;
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeSlab that = (SpigotBlockTypeSlab) o;
		return type == that.type &&
				position == that.position;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), type, position);
	}
}

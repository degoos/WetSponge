package com.degoos.wetsponge.parser.entity;


import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.entity.SpigotEntity;
import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.entity.living.SpigotCreature;
import com.degoos.wetsponge.entity.living.SpigotLivingEntity;
import com.degoos.wetsponge.entity.living.player.SpigotHuman;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.enums.EnumEntityType;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.parser.player.PlayerParser;
import com.degoos.wetsponge.util.Validate;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;
import org.bukkit.entity.*;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class SpigotEntityParser {

	private static Set<SpigotEntityData> types;
	private static Map<Entity, WSEntity> entities;

	public static void load() {
		types = new HashSet<>();
		entities = new ConcurrentHashMap<>();
		if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD)) {
			types.add(new SpigotEntityData(EntityType.AREA_EFFECT_CLOUD, EnumEntityType.AREA_EFFECT_CLOUD));
			types.add(new SpigotEntityData(EntityType.DRAGON_FIREBALL, EnumEntityType.DRAGON_FIREBALL));
			types.add(new SpigotEntityData(EntityType.SPLASH_POTION, EnumEntityType.LINGERING_POTION));
			types.add(new SpigotEntityData(EntityType.SHULKER, EnumEntityType.SHULKER));
			types.add(new SpigotEntityData(EntityType.SHULKER_BULLET, EnumEntityType.SHULKER_BULLET));
			types.add(new SpigotEntityData(EntityType.TIPPED_ARROW, EnumEntityType.TIPPED_ARROW));
			types.add(new SpigotEntityData(EntityType.SPECTRAL_ARROW, EnumEntityType.SPECTRAL_ARROW));
		}
		if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_1_9_2)) types.add(new SpigotEntityData(EntityType.POLAR_BEAR, EnumEntityType.POLAR_BEAR));
		if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_1_10_2)) {
			types.add(new SpigotEntityData(EntityType.ELDER_GUARDIAN, EnumEntityType.ELDER_GUARDIAN));
			types.add(new SpigotEntityData(EntityType.EVOKER, EnumEntityType.EVOCATION_ILLAGER));
			types.add(new SpigotEntityData(EntityType.EVOKER_FANGS, EnumEntityType.EVOCATION_FANGS));
			types.add(new SpigotEntityData(EntityType.HUSK, EnumEntityType.HUSK));
			types.add(new SpigotEntityData(EntityType.LLAMA, EnumEntityType.LLAMA));
			types.add(new SpigotEntityData(EntityType.LLAMA_SPIT, EnumEntityType.LLAMA_SPIT));
			types.add(new SpigotEntityData(EntityType.MULE, EnumEntityType.MULE));
			types.add(new SpigotEntityData(EntityType.SKELETON_HORSE, EnumEntityType.SKELETON_HORSE));
			types.add(new SpigotEntityData(EntityType.STRAY, EnumEntityType.STRAY));
			types.add(new SpigotEntityData(EntityType.VEX, EnumEntityType.VEX));
			types.add(new SpigotEntityData(EntityType.VINDICATOR, EnumEntityType.VINDICATION_ILLAGER));
			types.add(new SpigotEntityData(EntityType.WITHER_SKELETON, EnumEntityType.WITHER_SKELETON));
			types.add(new SpigotEntityData(EntityType.ZOMBIE_HORSE, EnumEntityType.ZOMBIE_HORSE));
			types.add(new SpigotEntityData(EntityType.ZOMBIE_VILLAGER, EnumEntityType.ZOMBIE_VILLAGER));
			types.add(new SpigotEntityData(EntityType.DONKEY, EnumEntityType.DONKEY));
		}
		if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_1_11_2)) {
			types.add(new SpigotEntityData(EntityType.PARROT, EnumEntityType.PARROT));
		}
		types.add(new SpigotEntityData(EntityType.ARMOR_STAND, EnumEntityType.ARMOR_STAND));
		types.add(new SpigotEntityData(EntityType.ARROW, EnumEntityType.ARROW, Arrow.class)); //ARROW???
		types.add(new SpigotEntityData(EntityType.BAT, EnumEntityType.BAT));
		types.add(new SpigotEntityData(EntityType.BLAZE, EnumEntityType.BLAZE));
		types.add(new SpigotEntityData(EntityType.BOAT, EnumEntityType.BOAT));
		types.add(new SpigotEntityData(EntityType.CAVE_SPIDER, EnumEntityType.CAVE_SPIDER));
		types.add(new SpigotEntityData(EntityType.CHICKEN, EnumEntityType.CHICKEN));
		types.add(new SpigotEntityData(EntityType.MINECART_COMMAND, EnumEntityType.COMMAND_BLOCK_MINECART));
		types.add(new SpigotEntityData(EntityType.COMPLEX_PART, EnumEntityType.COMPLEX_PART));
		types.add(new SpigotEntityData(EntityType.COW, EnumEntityType.COW));
		types.add(new SpigotEntityData(EntityType.CREEPER, EnumEntityType.CREEPER));
		types.add(new SpigotEntityData(EntityType.EGG, EnumEntityType.EGG));
		types.add(new SpigotEntityData(EntityType.ENDER_CRYSTAL, EnumEntityType.ENDER_CRYSTAL));
		types.add(new SpigotEntityData(EntityType.ENDER_DRAGON, EnumEntityType.ENDER_DRAGON));
		types.add(new SpigotEntityData(EntityType.ENDERMAN, EnumEntityType.ENDERMAN));
		types.add(new SpigotEntityData(EntityType.ENDERMITE, EnumEntityType.ENDERMITE));
		types.add(new SpigotEntityData(EntityType.ENDER_PEARL, EnumEntityType.ENDER_PEARL));
		types.add(new SpigotEntityData(EntityType.ENDER_SIGNAL, EnumEntityType.EYE_OF_ENDER));
		types.add(new SpigotEntityData(EntityType.EXPERIENCE_ORB, EnumEntityType.EXPERIENCE_ORB));
		types.add(new SpigotEntityData(EntityType.MINECART_TNT, EnumEntityType.TNT_MINECART));
		types.add(new SpigotEntityData(EntityType.FALLING_BLOCK, EnumEntityType.FALLING_BLOCK));
		types.add(new SpigotEntityData(EntityType.FIREBALL, EnumEntityType.FIREBALL));
		types.add(new SpigotEntityData(EntityType.FIREWORK, EnumEntityType.FIREWORK));
		types.add(new SpigotEntityData(EntityType.FISHING_HOOK, EnumEntityType.FISHING_HOOK));
		types.add(new SpigotEntityData(EntityType.GHAST, EnumEntityType.GHAST));
		types.add(new SpigotEntityData(EntityType.GIANT, EnumEntityType.GIANT));
		types.add(new SpigotEntityData(EntityType.GUARDIAN, EnumEntityType.GUARDIAN));
		types.add(new SpigotEntityData(EntityType.MINECART_HOPPER, EnumEntityType.HOPPER_MINECART));
		types.add(new SpigotEntityData(EntityType.HORSE, EnumEntityType.HORSE));
		types.add(new SpigotEntityData(EntityType.IRON_GOLEM, EnumEntityType.IRON_GOLEM));
		types.add(new SpigotEntityData(EntityType.DROPPED_ITEM, EnumEntityType.ITEM));
		types.add(new SpigotEntityData(EntityType.ITEM_FRAME, EnumEntityType.ITEM_FRAME));
		types.add(new SpigotEntityData(EntityType.LEASH_HITCH, EnumEntityType.LEASH_HITCH));
		types.add(new SpigotEntityData(EntityType.LIGHTNING, EnumEntityType.LIGHTNING));
		types.add(new SpigotEntityData(EntityType.MAGMA_CUBE, EnumEntityType.MAGMA_CUBE));
		types.add(new SpigotEntityData(EntityType.MINECART, EnumEntityType.RIDEABLE_MINECART));
		types.add(new SpigotEntityData(EntityType.MUSHROOM_COW, EnumEntityType.MUSHROOM_COW));
		types.add(new SpigotEntityData(EntityType.OCELOT, EnumEntityType.OCELOT));
		types.add(new SpigotEntityData(EntityType.PAINTING, EnumEntityType.PAINTING));
		types.add(new SpigotEntityData(EntityType.PIG, EnumEntityType.PIG));
		types.add(new SpigotEntityData(EntityType.PIG_ZOMBIE, EnumEntityType.PIG_ZOMBIE));
		types.add(new SpigotEntityData(EntityType.PLAYER, EnumEntityType.PLAYER));
		types.add(new SpigotEntityData(EntityType.MINECART_FURNACE, EnumEntityType.FURNACE_MINECART));
		types.add(new SpigotEntityData(EntityType.RABBIT, EnumEntityType.RABBIT));
		types.add(new SpigotEntityData(EntityType.SHEEP, EnumEntityType.SHEEP));
		types.add(new SpigotEntityData(EntityType.SILVERFISH, EnumEntityType.SILVERFISH));
		types.add(new SpigotEntityData(EntityType.SKELETON, EnumEntityType.SKELETON));
		types.add(new SpigotEntityData(EntityType.SLIME, EnumEntityType.SLIME));
		types.add(new SpigotEntityData(EntityType.SMALL_FIREBALL, EnumEntityType.SMALL_FIREBALL));
		types.add(new SpigotEntityData(EntityType.SNOWBALL, EnumEntityType.SNOWBALL));
		types.add(new SpigotEntityData(EntityType.SNOWMAN, EnumEntityType.SNOWMAN));
		types.add(new SpigotEntityData(EntityType.MINECART_MOB_SPAWNER, EnumEntityType.MOB_SPAWNER_MINECART));
		types.add(new SpigotEntityData(EntityType.SPIDER, EnumEntityType.SPIDER));
		types.add(new SpigotEntityData(EntityType.SPLASH_POTION, EnumEntityType.SPLASH_POTION));
		types.add(new SpigotEntityData(EntityType.SQUID, EnumEntityType.SQUID));
		types.add(new SpigotEntityData(EntityType.MINECART_CHEST, EnumEntityType.CHESTED_MINECART));
		types.add(new SpigotEntityData(EntityType.THROWN_EXP_BOTTLE, EnumEntityType.THROWN_EXP_BOTTLE));
		types.add(new SpigotEntityData(EntityType.PRIMED_TNT, EnumEntityType.PRIMED_TNT));
		types.add(new SpigotEntityData(EntityType.VILLAGER, EnumEntityType.VILLAGER));
		types.add(new SpigotEntityData(EntityType.WEATHER, EnumEntityType.WEATHER));
		types.add(new SpigotEntityData(EntityType.WITCH, EnumEntityType.WITCH));
		types.add(new SpigotEntityData(EntityType.WITHER, EnumEntityType.WITHER));
		types.add(new SpigotEntityData(EntityType.WITHER_SKULL, EnumEntityType.WITHER_SKULL));
		types.add(new SpigotEntityData(EntityType.WOLF, EnumEntityType.WOLF));
		types.add(new SpigotEntityData(EntityType.ZOMBIE, EnumEntityType.ZOMBIE));
	}

	public static EnumEntityType getEntityType(Entity entity) {
		return types.stream().filter(data -> data.getEntityClass().isInstance(entity)).findAny().map(SpigotEntityData::getEntityType).orElse(EnumEntityType.UNKNOWN);
	}

	public static SpigotEntityData getEntityData(Entity entity) {
		return types.stream().filter(data -> data.getEntityClass().isInstance(entity)).findAny().orElse(null);
	}

	public static EntityType getSuperEntityType(EnumEntityType type) {
		return types.stream().filter(data -> data.getEntityType() == type).findAny().map(SpigotEntityData::getSpongeEntityType).orElse(EntityType.UNKNOWN);
	}

	public static SpigotEntityData getEntityData(EnumEntityType type) {
		return types.stream().filter(data -> data.getEntityType() == type).findAny().orElse(null);
	}


	public static EnumEntityType getEntityType(EntityType type) {
		return types.stream().filter(data -> data.getSpongeEntityType().equals(type)).findAny().map(SpigotEntityData::getEntityType).orElse(EnumEntityType.UNKNOWN);
	}

	public static SpigotEntityData getEntityData(EntityType type) {
		return types.stream().filter(data -> data.getSpongeEntityType().equals(type)).findAny().orElse(null);
	}


	public static WSEntity getWSEntity(Entity entity) {
		Validate.notNull(entity, "Entity cannot be null!");
		if (entities.containsKey(entity)) return entities.get(entity);
		else return addEntity(entity);
	}

	private static WSEntity createHandlerEntity(Entity entity) {
		Validate.notNull(entity, "Entity cannot be null!");
		if (entity instanceof Player) return PlayerParser.getOrCreatePlayer(entity, entity.getUniqueId());
		if (entity instanceof HumanEntity) return new SpigotHuman((HumanEntity) entity);

		SpigotEntityData data = getEntityData(entity);
		if (data != null && data.getEntityType().getServerClass() != null) {
			try {
				return (WSEntity) ReflectionUtils.getConstructor(data.getEntityType().getServerClass(), data.getEntityClass()).newInstance(entity);
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}

		if (entity instanceof Creature) return new SpigotCreature((Creature) entity);
		if (entity instanceof LivingEntity) return new SpigotLivingEntity((LivingEntity) entity);

		return new SpigotEntity(entity);
	}


	public static WSEntity addEntity(Entity entity) {
		Validate.notNull(entity, "Entity cannot be null!");
		WSEntity wsEntity = createHandlerEntity(entity);
		entities.put(entity, wsEntity);
		return wsEntity;
	}

	public static void removeEntity(Entity entity) {
		Validate.notNull(entity, "Entity cannot be null!");
		entities.remove(entity);
	}

	public static Optional<WSEntity> getWSEntity(UUID uuid) {
		Optional<WSPlayer> player = WetSponge.getServer().getPlayer(uuid);
		if (player.isPresent()) return Optional.ofNullable(player.get());
		for (WSEntity entity : Collections.unmodifiableCollection(entities.values()))
			if (entity != null && entity.getUniqueId() != null && entity.getUniqueId().equals(uuid)) return Optional.of(entity);
		return Optional.empty();
	}

	public static Optional<WSEntity> getWSEntity(int id) {
		return entities.values().stream().filter(target -> target != null && target.getEntityId() == id).findAny();
	}

	public static boolean containsValue(EnumEntityType type) {
		return types.stream().anyMatch(data -> data.getEntityType() == type);
	}

}

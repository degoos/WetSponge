package com.degoos.wetsponge.text;

import com.degoos.wetsponge.text.translation.SpigotTranslation;
import com.degoos.wetsponge.text.translation.WSTranslation;
import net.md_5.bungee.api.chat.TranslatableComponent;

public class SpigotTranslatableText extends SpigotText implements WSTranslatableText {

	private WSTranslation translation;

	public SpigotTranslatableText(TranslatableComponent text) {
		super(text);
		translation = new SpigotTranslation(text.getTranslate());
	}

	public SpigotTranslatableText(WSTranslation translation, Object... objects) {
		super(new TranslatableComponent(translation.getId(), objects));
		this.translation = translation;
	}

	@Override
	public WSTranslation getTranslation() {
		return translation;
	}
}

package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.SpigotBlockTypeDirectional;
import org.bukkit.material.MaterialData;

import java.util.Objects;
import java.util.Set;

public class SpigotBlockTypeDispenser extends SpigotBlockTypeDirectional implements WSBlockTypeDispenser {

	private boolean triggered;

	public SpigotBlockTypeDispenser(int numericalId, String oldStringId, String newStringId, int maxStackSize, EnumBlockFace facing, Set<EnumBlockFace> faces, boolean triggered) {
		super(numericalId, oldStringId, newStringId, maxStackSize, facing, faces);
		this.triggered = triggered;
	}

	@Override
	public boolean isTriggered() {
		return true;
	}

	@Override
	public void setTriggered(boolean triggered) {
		this.triggered = triggered;
	}

	@Override
	public SpigotBlockTypeDispenser clone() {
		return new SpigotBlockTypeDispenser(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), getFacing(), getFaces(), triggered);
	}

	@Override
	public MaterialData toMaterialData() {
		return super.toMaterialData();
	}

	@Override
	public SpigotBlockTypeDispenser readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeDispenser that = (SpigotBlockTypeDispenser) o;
		return triggered == that.triggered;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), triggered);
	}
}

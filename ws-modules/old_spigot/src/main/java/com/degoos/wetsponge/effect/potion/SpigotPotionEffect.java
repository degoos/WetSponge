package com.degoos.wetsponge.effect.potion;

import com.degoos.wetsponge.enums.EnumPotionEffectType;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class SpigotPotionEffect implements WSPotionEffect {

    private PotionEffect         potionEffect;
    private EnumPotionEffectType type;

    public SpigotPotionEffect(EnumPotionEffectType type, int duration, int amplifier, boolean ambient, boolean showParticles) {
        Validate.notNull(type, "Type cannot be null!");
        potionEffect = new PotionEffect(PotionEffectType.getById(type.getValue()), duration, amplifier, ambient, showParticles);
        this.type = type;
    }

    public SpigotPotionEffect(PotionEffect potionEffect) {
        Validate.notNull(potionEffect, "Potion effect cannot be null!");
        this.potionEffect = potionEffect;
        this.type = EnumPotionEffectType.getByValue(potionEffect.getType().getId()).orElse(EnumPotionEffectType.SPEED);
    }

    @Override
    public EnumPotionEffectType getType() {
        return type;
    }

    @Override
    public int getDuration() {
        return potionEffect.getDuration();
    }

    @Override
    public int getAmplifier() {
        return potionEffect.getAmplifier();
    }

    @Override
    public boolean isAmbient() {
        return potionEffect.isAmbient();
    }

    @Override
    public boolean getShowParticles() {
        return potionEffect.hasParticles();
    }

    @Override
    public PotionEffect getHandled() {
        return potionEffect;
    }


    public static class Builder implements WSPotionEffect.Builder {

        private EnumPotionEffectType type;
        private int duration, amplifier;
        private boolean ambient, showParticles;

        @Override
        public EnumPotionEffectType type() {
            return type;
        }

        @Override
        public WSPotionEffect.Builder type(EnumPotionEffectType type) {
            this.type = type;
            return this;
        }

        @Override
        public int duration() {
            return duration;
        }

        @Override
        public WSPotionEffect.Builder duration(int duration) {
            this.duration = duration;
            return this;
        }

        @Override
        public int amplifier() {
            return amplifier;
        }

        @Override
        public WSPotionEffect.Builder amplifier(int amplifier) {
            this.amplifier = amplifier;
            return this;
        }

        @Override
        public boolean ambient() {
            return ambient;
        }

        @Override
        public WSPotionEffect.Builder ambient(boolean ambient) {
            this.ambient = ambient;
            return this;
        }

        @Override
        public boolean showParticles() {
            return showParticles;
        }

        @Override
        public WSPotionEffect.Builder showParticles(boolean showParticles) {
            this.showParticles = showParticles;
            return this;
        }

        @Override
        public WSPotionEffect build() {
            return new SpigotPotionEffect(type, duration, amplifier, ambient, showParticles);
        }
    }
}

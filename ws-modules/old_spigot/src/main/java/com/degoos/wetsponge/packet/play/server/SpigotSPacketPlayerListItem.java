package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumGameMode;
import com.degoos.wetsponge.enums.EnumPlayerListItemAction;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.packet.SpigotPacket;
import com.degoos.wetsponge.packet.play.server.extra.WSPlayerListItemData;
import com.degoos.wetsponge.text.SpigotText;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.user.SpigotGameProfile;
import com.degoos.wetsponge.util.ListUtils;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import com.degoos.wetsponge.util.reflection.SpigotTextUtils;
import com.mojang.authlib.GameProfile;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.chat.ComponentSerializer;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class SpigotSPacketPlayerListItem extends SpigotPacket implements WSSPacketPlayerListItem {

	private static final Class<?> packetClass = NMSUtils.getNMSClass("PacketPlayOutPlayerInfo");

	private boolean changed;
	private EnumPlayerListItemAction action;
	private List<WSPlayerListItemData> data;

	public SpigotSPacketPlayerListItem(EnumPlayerListItemAction action, Collection<WSPlayerListItemData> itemData) throws IllegalAccessException, InstantiationException {
		super(packetClass.newInstance());
		changed = false;
		this.action = action;
		this.data = new ArrayList<>(itemData);
		update();
	}

	public SpigotSPacketPlayerListItem(EnumPlayerListItemAction action, WSPlayerListItemData... itemData) throws IllegalAccessException, InstantiationException {
		super(packetClass.newInstance());
		changed = false;
		this.action = action;
		this.data = ListUtils.toList(itemData);
		update();
	}

	public SpigotSPacketPlayerListItem(Object packet) {
		super(packet);
		changed = false;
		this.data = new ArrayList<>();
		refresh();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void update() {
		try {
			Field[] fields = packetClass.getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));

			Class<? extends Enum> enumClass = (Class<? extends Enum>) NMSUtils.getNMSClass("PacketPlayOutPlayerInfo$EnumPlayerInfoAction");
			Class<? extends Enum> gameModeClass = (Class<? extends Enum>) NMSUtils.getNMSClass(
					WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD) ? "EnumGamemode" : "WorldSettings$EnumGamemode");
			Class<?> iChatBaseClass = NMSUtils.getNMSClass("IChatBaseComponent");
			Class<?> dataClass = NMSUtils.getNMSClass("PacketPlayOutPlayerInfo$PlayerInfoData");
			Constructor dataConstructor = dataClass.getConstructor(packetClass, GameProfile.class, int.class, gameModeClass, iChatBaseClass);

			fields[0].set(getHandler(), Enum.valueOf(enumClass, action.name()));


			List list = (List<?>) fields[1].get(getHandler());
			list.clear();

			for (WSPlayerListItemData itemData : data) {
				list.add(dataConstructor.newInstance(getHandler(), itemData.getGameProfile().getHandled(), itemData.getPing(),
						Enum.valueOf(gameModeClass, itemData.getGameMode() == null ? "NOT_SET" : itemData.getGameMode().name()),
						itemData.getDisplayName() == null ? null : SpigotTextUtils.toIChatBaseComponentFromFormattedText(itemData.getDisplayName().toFormattingText())));
			}

		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void refresh() {
		try {
			data.clear();

			Field[] fields = packetClass.getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));

			action = EnumPlayerListItemAction.valueOf(((Enum) fields[0].get(getHandler())).name());

			List list = (List<?>) fields[1].get(getHandler());

			for (Object itemData : list) {
				GameProfile gameProfile = (GameProfile) itemData.getClass().getMethod("a").invoke(itemData);
				int ping = (int) itemData.getClass().getMethod("b").invoke(itemData);
				Enum en = (Enum) itemData.getClass().getMethod("c").invoke(itemData);
				EnumGameMode gameMode = en == null || en.name().equals("NOT_SET") ? null : EnumGameMode.valueOf(en.name());
				Object d = itemData.getClass().getMethod("d").invoke(itemData);
				WSText displayName = d == null ? null : SpigotText.of(new TextComponent(
						ComponentSerializer.parse(SpigotTextUtils.toJSON(d))));
				data.add(new WSPlayerListItemData(ping, gameMode, new SpigotGameProfile(gameProfile), displayName));
			}

		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public boolean hasChanged() {
		return changed;
	}

	@Override
	public EnumPlayerListItemAction getAction() {
		return action;
	}

	@Override
	public void setAction(EnumPlayerListItemAction action) {
		this.action = action;
		changed = true;
	}

	@Override
	public List<WSPlayerListItemData> getData() {
		changed = true;
		return data;
	}
}

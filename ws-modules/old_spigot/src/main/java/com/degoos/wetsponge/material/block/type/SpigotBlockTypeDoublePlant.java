package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockTypeBisectedHalf;
import com.degoos.wetsponge.enums.block.EnumBlockTypeDoublePlantType;
import com.degoos.wetsponge.material.block.SpigotBlockTypeBisected;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.material.MaterialData;

import java.util.Objects;

public class SpigotBlockTypeDoublePlant extends SpigotBlockTypeBisected implements WSBlockTypeDoublePlant {

	private EnumBlockTypeDoublePlantType doublePlantType;

	public SpigotBlockTypeDoublePlant(EnumBlockTypeBisectedHalf half, EnumBlockTypeDoublePlantType doublePlantType) {
		super(175, "minecraft:double_plant", "minecraft:sunflower", 64, half);
		Validate.notNull(doublePlantType, "Double plant type cannot be null!");
		this.doublePlantType = doublePlantType;
	}

	@Override
	public String getNewStringId() {
		switch (doublePlantType) {
			case LILAC:
				return "minecraft:lilac";
			case DOUBLE_TALLGRASS:
				return "minecraft:tall_grass";
			case LARGE_FERN:
				return "minecraft:large_fern";
			case ROSE_BUSH:
				return "minecraft:rose_bush";
			case PEONY:
				return "minecraft:peony";
			case SUNFLOWER:
			default:
				return "minecraft:sunflower";
		}
	}

	@Override
	public EnumBlockTypeDoublePlantType getDoublePlantType() {
		return doublePlantType;
	}

	@Override
	public void setDoublePlantType(EnumBlockTypeDoublePlantType doublePlantType) {
		Validate.notNull(doublePlantType, "Double plant type cannot be null!");
		this.doublePlantType = doublePlantType;
	}

	@Override
	public SpigotBlockTypeDoublePlant clone() {
		return new SpigotBlockTypeDoublePlant(getHalf(), doublePlantType);
	}

	@Override
	public MaterialData toMaterialData() {
		MaterialData data = super.toMaterialData();
		data.setData((byte) doublePlantType.getValue());
		return data;
	}

	@Override
	public SpigotBlockTypeDoublePlant readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		doublePlantType = EnumBlockTypeDoublePlantType.getByValue(materialData.getData()).orElse(EnumBlockTypeDoublePlantType.SUNFLOWER);
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeDoublePlant that = (SpigotBlockTypeDoublePlant) o;
		return doublePlantType == that.doublePlantType;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), doublePlantType);
	}
}

package com.degoos.wetsponge.material.item.type;

import com.degoos.wetsponge.firework.SpigotFireworkEffect;
import com.degoos.wetsponge.firework.WSFireworkEffect;
import com.degoos.wetsponge.material.item.SpigotItemType;
import org.bukkit.FireworkEffect;
import org.bukkit.inventory.meta.FireworkEffectMeta;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Objects;
import java.util.Optional;

public class SpigotItemTypeFireworkCharge extends SpigotItemType implements WSItemTypeFireworkCharge {

	private WSFireworkEffect effect;

	public SpigotItemTypeFireworkCharge(WSFireworkEffect effect) {
		super(402, "minecraft:firework_charge", "minecraft:firework_star", 64);
		this.effect = effect;
	}

	@Override
	public Optional<WSFireworkEffect> getEffect() {
		return Optional.ofNullable(effect);
	}

	@Override
	public void setEffect(WSFireworkEffect effect) {
		this.effect = effect;
	}

	@Override
	public SpigotItemTypeFireworkCharge clone() {
		return new SpigotItemTypeFireworkCharge(effect == null ? null : effect.clone());
	}

	@Override
	public void writeItemMeta(ItemMeta itemMeta) {
		super.writeItemMeta(itemMeta);
		if (itemMeta instanceof FireworkEffectMeta) {
			((FireworkEffectMeta) itemMeta).setEffect(effect == null ? null : (FireworkEffect) effect.getHandled());
		}
	}

	@Override
	public void readItemMeta(ItemMeta itemMeta) {
		super.readItemMeta(itemMeta);
		if (itemMeta instanceof FireworkEffectMeta) {
			effect = Optional.ofNullable(((FireworkEffectMeta) itemMeta).getEffect()).map(SpigotFireworkEffect::new).orElse(null);
		}
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotItemTypeFireworkCharge that = (SpigotItemTypeFireworkCharge) o;
		return Objects.equals(effect, that.effect);
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), effect);
	}
}

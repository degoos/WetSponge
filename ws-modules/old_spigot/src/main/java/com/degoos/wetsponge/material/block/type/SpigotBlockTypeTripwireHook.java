package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.SpigotBlockTypeDirectional;
import org.bukkit.material.MaterialData;
import org.bukkit.material.TripwireHook;

import java.util.Objects;
import java.util.Set;

public class SpigotBlockTypeTripwireHook extends SpigotBlockTypeDirectional implements WSBlockTypeTripwireHook {

	private boolean attached, powered;

	public SpigotBlockTypeTripwireHook(EnumBlockFace facing, Set<EnumBlockFace> faces, boolean attached, boolean powered) {
		super(131, "minecraft:tripwire_hook", "minecraft:tripwire_hook", 64, facing, faces);
		this.attached = attached;
		this.powered = powered;
	}

	@Override
	public boolean isAttached() {
		return attached;
	}

	@Override
	public void setAttached(boolean attached) {
		this.attached = attached;
	}

	@Override
	public boolean isPowered() {
		return powered;
	}

	@Override
	public void setPowered(boolean powered) {
		this.powered = powered;
	}

	@Override
	public SpigotBlockTypeTripwireHook clone() {
		return new SpigotBlockTypeTripwireHook(getFacing(), getFaces(), attached, powered);
	}

	@Override
	public MaterialData toMaterialData() {
		MaterialData data = super.toMaterialData();
		if (data instanceof TripwireHook) {
			((TripwireHook) data).setActivated(powered);
			((TripwireHook) data).setConnected(attached);
		}
		return data;
	}

	@Override
	public SpigotBlockTypeTripwireHook readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		if (materialData instanceof TripwireHook) {
			powered = ((TripwireHook) materialData).isActivated();
			attached = ((TripwireHook) materialData).isConnected();
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeTripwireHook that = (SpigotBlockTypeTripwireHook) o;
		return attached == that.attached &&
				powered == that.powered;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), attached, powered);
	}
}

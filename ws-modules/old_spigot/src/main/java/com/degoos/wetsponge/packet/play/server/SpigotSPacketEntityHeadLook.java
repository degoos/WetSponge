package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.entity.living.WSLivingEntity;
import com.degoos.wetsponge.packet.SpigotPacket;
import com.degoos.wetsponge.util.reflection.NMSUtils;

import java.lang.reflect.Field;
import java.util.Arrays;

public class SpigotSPacketEntityHeadLook extends SpigotPacket implements WSSPacketEntityHeadLook {

	private int entityId;
	private int yaw;
	private boolean changed;

	public SpigotSPacketEntityHeadLook(int entityId, int yaw) throws IllegalAccessException, InstantiationException {
		super(NMSUtils.getNMSClass("PacketPlayOutEntityHeadRotation").newInstance());
		this.entityId = entityId;
		this.yaw = yaw;
		this.changed = false;
		update();
	}

	public SpigotSPacketEntityHeadLook(WSLivingEntity entity) throws InstantiationException, IllegalAccessException {
		this(entity.getEntityId(), entity.getHeadRotation().toInt().getY());
	}

	public SpigotSPacketEntityHeadLook(Object packet) {
		super(packet);
		refresh();
		this.changed = false;
	}

	@Override
	public int getEntityId() {
		return entityId;
	}

	@Override
	public void setEntityId(int entityId) {
		this.entityId = entityId;
		changed = true;
	}

	@Override
	public int getYaw() {
		return yaw;
	}

	@Override
	public void setYaw(int yaw) {
		this.yaw = yaw;
	}

	@Override
	public void update() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));
			fields[0].setInt(getHandler(), entityId);
			fields[1].setByte(getHandler(), (byte) (yaw * 256D / 360D));
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void refresh() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));
			entityId = fields[0].getInt(getHandler());
			yaw = (int) (fields[1].getByte(getHandler()) * 360D / 256D);
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public boolean hasChanged() {
		return changed;
	}
}

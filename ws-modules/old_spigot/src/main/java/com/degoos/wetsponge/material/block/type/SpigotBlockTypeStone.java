package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockTypeStoneType;
import com.degoos.wetsponge.material.block.SpigotBlockType;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.material.MaterialData;

import java.util.Objects;

public class SpigotBlockTypeStone extends SpigotBlockType implements WSBlockTypeStone {

	private EnumBlockTypeStoneType stoneType;

	public SpigotBlockTypeStone(EnumBlockTypeStoneType stoneType) {
		super(1, "minecraft:stone", "minecraft:stone", 64);
		Validate.notNull(stoneType, "Stone type cannot be null!");
		this.stoneType = stoneType;
	}

	@Override
	public String getNewStringId() {
		switch (stoneType) {
			case DIORITE:
				return "minecraft:diorite";
			case GRANITE:
				return "minecraft:granite";
			case ANDESITE:
				return "minecraft:andesite";
			case SMOOTH_DIORITE:
				return "minecraft:polished_diorite";
			case SMOOTH_GRANITE:
				return "minecraft:polished_granite";
			case SMOOTH_ANDESITE:
				return "minecraft:polished_andesite";
			case STONE:
			default:
				return "minecraft:stone";
		}
	}

	@Override
	public EnumBlockTypeStoneType getStoneType() {
		return stoneType;
	}

	@Override
	public void setStoneType(EnumBlockTypeStoneType stoneType) {
		Validate.notNull(stoneType, "Stone type cannot be null!");
		this.stoneType = stoneType;
	}

	@Override
	public SpigotBlockTypeStone clone() {
		return new SpigotBlockTypeStone(stoneType);
	}

	@Override
	public MaterialData toMaterialData() {
		MaterialData data = super.toMaterialData();
		data.setData((byte) stoneType.getValue());
		return data;
	}

	@Override
	public SpigotBlockTypeStone readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		stoneType = EnumBlockTypeStoneType.getByValue(materialData.getData()).orElse(EnumBlockTypeStoneType.STONE);
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeStone that = (SpigotBlockTypeStone) o;
		return stoneType == that.stoneType;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), stoneType);
	}
}

package com.degoos.wetsponge.mixin.spigot;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import javassist.ClassClassPath;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtNewMethod;

public class SpigotWSEntityHumanAssist {

	public static Class<?> ENTITY_HUMAN_CLASS;

	public static void load() {
		try {
			ClassPool pool = ClassPool.getDefault();
			CtClass cWSEntityHuman = pool.makeClass("WSEntityHuman");

			Class<?> entityHumanClass = NMSUtils.getNMSClass("EntityHuman");
			pool.insertClassPath(new ClassClassPath(entityHumanClass));
			CtClass cEntityHuman = pool.get(entityHumanClass.getName());
			cWSEntityHuman.setSuperclass(cEntityHuman);

			cWSEntityHuman.addMethod(CtNewMethod
					.make(CtClass.booleanType, "isSpectator", new CtClass[]{},
							new CtClass[]{}, "{return false;}", cWSEntityHuman));

			if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD))
				cWSEntityHuman.addMethod(CtNewMethod
						.make(CtClass.booleanType, "z", new CtClass[]{},
								new CtClass[]{}, "{return false;}", cWSEntityHuman));
			ENTITY_HUMAN_CLASS = cWSEntityHuman.toClass();
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An exception has occurred while WetSponge was injecting code!");
		}
	}
}

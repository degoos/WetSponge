package com.degoos.wetsponge.mixin.spigot;

import com.degoos.wetsponge.util.reflection.NMSUtils;
import java.lang.reflect.Field;
import java.util.List;

public class SpigotMixinChunkProviderServer {

	public boolean hasChunk(int x, int z, List list) {
		try {
			Field locXField = NMSUtils.getNMSClass("Chunk").getField("locX");
			Field locZField = NMSUtils.getNMSClass("Chunk").getField("locZ");
			locXField.setAccessible(true);
			locZField.setAccessible(true);
			for (Object o : list) {
				int locX = locXField.getInt(o);
				int locZ = locZField.getInt(o);
				if (x == locX && z == locZ) {
					locXField.setAccessible(false);
					locZField.setAccessible(false);
					return true;
				}
			}
			locXField.setAccessible(false);
			locZField.setAccessible(false);
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
		return false;
	}

	public boolean hasChunkLong(long l, List list) {
		try {
			Field locXField = NMSUtils.getNMSClass("Chunk").getField("locX");
			Field locZField = NMSUtils.getNMSClass("Chunk").getField("locZ");
			locXField.setAccessible(true);
			locZField.setAccessible(true);
			for (Object o : list) {
				int  locX      = locXField.getInt(o);
				int  locZ      = locZField.getInt(o);
				long chunkLong = (long) locX & 4294967295L | ((long) locZ & 4294967295L) << 32;
				if (chunkLong == l) {
					locXField.setAccessible(false);
					locZField.setAccessible(false);
					return true;
				}
			}
			locXField.setAccessible(false);
			locZField.setAccessible(false);
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
		return false;
	}

	public static boolean hasChunkLongStatic(long l, List list) {
		try {
			Field locXField = NMSUtils.getNMSClass("Chunk").getField("locX");
			Field locZField = NMSUtils.getNMSClass("Chunk").getField("locZ");
			locXField.setAccessible(true);
			locZField.setAccessible(true);
			for (Object o : list) {
				int  locX      = locXField.getInt(o);
				int  locZ      = locZField.getInt(o);
				long chunkLong = (long) locX & 4294967295L | ((long) locZ & 4294967295L) << 32;
				if (chunkLong == l) {
					locXField.setAccessible(false);
					locZField.setAccessible(false);
					return true;
				}
			}
			locXField.setAccessible(false);
			locZField.setAccessible(false);
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
		return false;
	}
}

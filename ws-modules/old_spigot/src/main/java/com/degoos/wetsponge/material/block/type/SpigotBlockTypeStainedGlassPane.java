package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.EnumDyeColor;
import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.DyeColor;
import org.bukkit.material.Colorable;
import org.bukkit.material.MaterialData;

import java.util.Objects;
import java.util.Set;

public class SpigotBlockTypeStainedGlassPane extends SpigotBlockTypeGlassPane implements WSBlockTypeStainedGlassPane {

	private EnumDyeColor dyeColor;

	public SpigotBlockTypeStainedGlassPane(Set<EnumBlockFace> faces, Set<EnumBlockFace> allowedFaces, boolean waterlogged, EnumDyeColor dyeColor) {
		super(160, "minecraft:stained_glass_pane", "stained_glass_pane", 64, faces, allowedFaces, waterlogged);
		Validate.notNull(dyeColor, "Dye color cannot be null!");
		this.dyeColor = dyeColor;
	}

	@Override
	public String getNewStringId() {
		return "minecraft:" + dyeColor.getMinecraftName().toLowerCase() + "_" + super.getNewStringId();
	}

	@Override
	public EnumDyeColor getDyeColor() {
		return dyeColor;
	}

	@Override
	public void setDyeColor(EnumDyeColor dyeColor) {
		Validate.notNull(dyeColor, "Dye color cannot be null!");
		this.dyeColor = dyeColor;
	}

	@Override
	public SpigotBlockTypeStainedGlassPane clone() {
		return new SpigotBlockTypeStainedGlassPane(getFaces(), getAllowedFaces(), isWaterlogged(), dyeColor);
	}

	@Override
	public MaterialData toMaterialData() {
		MaterialData data = super.toMaterialData();
		if (data instanceof Colorable) {
			((Colorable) data).setColor(DyeColor.getByDyeData(dyeColor.getDyeData()));
		}
		return data;
	}

	@Override
	public SpigotBlockTypeStainedGlassPane readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		if (materialData instanceof Colorable) {
			dyeColor = EnumDyeColor.getByDyeData(((Colorable) materialData).getColor().getDyeData()).orElse(EnumDyeColor.WHITE);
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeStainedGlassPane that = (SpigotBlockTypeStainedGlassPane) o;
		return dyeColor == that.dyeColor;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), dyeColor);
	}
}

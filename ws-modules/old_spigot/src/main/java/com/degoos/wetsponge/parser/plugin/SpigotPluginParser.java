package com.degoos.wetsponge.parser.plugin;

import com.degoos.wetsponge.plugin.SpigotBasePlugin;
import com.degoos.wetsponge.plugin.WSBasePlugin;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

public class SpigotPluginParser {

	public static Set<WSBasePlugin> getBasePlugins() {
		return Arrays.stream(org.bukkit.Bukkit.getPluginManager().getPlugins()).map(SpigotBasePlugin::new).collect(Collectors.toSet());
	}

	public static boolean isBasePluginEnabled(String name) {
		return org.bukkit.Bukkit.getPluginManager().isPluginEnabled(name);
	}

}

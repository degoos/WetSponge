package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.entity.living.player.WSHuman;
import com.degoos.wetsponge.packet.SpigotPacket;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import com.degoos.wetsponge.util.reflection.SpigotHandledUtils;
import com.flowpowered.math.vector.Vector3i;

import java.lang.reflect.Field;
import java.util.Arrays;

public class SpigotSPacketUseBed extends SpigotPacket implements WSSPacketUseBed {

	private int entityId;
	private Vector3i position;
	private boolean changed;

	public SpigotSPacketUseBed(int entityId, Vector3i position) throws IllegalAccessException, InstantiationException {
		super(NMSUtils.getNMSClass("PacketPlayOutBed").newInstance());
		this.position = position;
		this.entityId = entityId;
		update();
	}

	public SpigotSPacketUseBed(WSHuman human, Vector3i position) throws IllegalAccessException, InstantiationException {
		super(NMSUtils.getNMSClass("PacketPlayOutBed").newInstance());
		this.position = position;
		this.entityId = human.getEntityId();
		update();
	}

	public SpigotSPacketUseBed(Object packet) {
		super(packet);
		refresh();
	}

	public void update() {
		try {

			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));

			fields[0].setInt(getHandler(), entityId);
			fields[1].set(getHandler(), SpigotHandledUtils.getBlockPosition(position));
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was updating a packet!");
		}
	}

	public void refresh() {
		try {

			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));
			entityId = fields[0].getInt(getHandler());
			position = SpigotHandledUtils.getBlockPositionVector(fields[1].get(getHandler()));
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was refreshing a packet!");
			entityId = 0;
			position = new Vector3i(0, 0, 0);
		}
	}


	@Override
	public boolean hasChanged() {
		return changed;
	}

	@Override
	public int getEntityId() {
		return entityId;
	}

	@Override
	public void setEntityId(int entityId) {
		this.entityId = entityId;
	}

	@Override
	public Vector3i getPosition() {
		return position;
	}

	@Override
	public void setPosition(Vector3i position) {
		this.position = position;
		changed = true;
	}
}

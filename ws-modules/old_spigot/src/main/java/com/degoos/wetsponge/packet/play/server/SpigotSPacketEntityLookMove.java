package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import com.flowpowered.math.vector.Vector2i;
import com.flowpowered.math.vector.Vector3i;

public class SpigotSPacketEntityLookMove extends SpigotSPacketEntity implements WSSPacketEntityLookMove {

    public SpigotSPacketEntityLookMove(WSEntity entity, Vector3i position, Vector2i rotation, boolean onGround) {
        this(entity.getEntityId(), position, rotation, onGround);
    }

    public SpigotSPacketEntityLookMove(int entity, Vector3i position, Vector2i rotation, boolean onGround) {
        super(getPacket(entity, position, rotation, onGround));
    }

    public SpigotSPacketEntityLookMove(Object packet) {
        super(packet);
    }

    public static Object getPacket(int entity, Vector3i position, Vector2i rotation, boolean onGround) {
        try {
            Class<?> clazz = NMSUtils.getNMSClass("PacketPlayOutEntity$PacketPlayOutRelEntityMoveLook");
            if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD))
                return clazz.getConstructor(int.class, long.class, long.class, long.class, byte.class, byte.class, boolean.class)
                        .newInstance(entity, (long) position.getX(), (long) position.getY(), (long) position.getZ(), (byte) rotation.getY(), (byte) rotation.getX(),
                                onGround);
            else return clazz.getConstructor(int.class, byte.class, byte.class, byte.class, byte.class, byte.class, boolean.class)
                    .newInstance(entity, (byte) position.getX(), (byte) position.getY(), (byte) position.getZ(), (byte) rotation.getY(), (byte) rotation.getX(),
                            onGround);
        } catch (Throwable ex) {
            ex.printStackTrace();
            return null;
        }
    }
}

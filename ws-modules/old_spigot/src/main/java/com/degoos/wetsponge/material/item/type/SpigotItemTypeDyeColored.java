package com.degoos.wetsponge.material.item.type;

import com.degoos.wetsponge.enums.EnumDyeColor;
import com.degoos.wetsponge.material.item.SpigotItemType;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.material.MaterialData;

import java.util.Objects;

public class SpigotItemTypeDyeColored extends SpigotItemType implements WSItemTypeDyeColored {

	private EnumDyeColor dyeColor;

	public SpigotItemTypeDyeColored(int numericalId, String oldStringId, String newStringId, int maxStackSize, EnumDyeColor dyeColor) {
		super(numericalId, oldStringId, newStringId.startsWith("minecraft:") ? newStringId.substring(10) : newStringId, maxStackSize);
		Validate.notNull(dyeColor, "Dye color cannot be null!");
		this.dyeColor = dyeColor;
	}

	@Override
	public String getNewStringId() {
		return dyeColor.getMinecraftName().toLowerCase() + "_" + super.getNewStringId();
	}

	@Override
	public EnumDyeColor getDyeColor() {
		return dyeColor;
	}

	@Override
	public void setDyeColor(EnumDyeColor dyeColor) {
		Validate.notNull(dyeColor, "Dye color cannot be null!");
		this.dyeColor = dyeColor;
	}

	@Override
	public SpigotItemTypeDyeColored clone() {
		return new SpigotItemTypeDyeColored(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), dyeColor);
	}

	@Override
	public MaterialData toMaterialData() {
		MaterialData data = super.toMaterialData();
		data.setData(dyeColor.getWoolData());
		return data;
	}

	@Override
	public SpigotItemTypeDyeColored readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		dyeColor = EnumDyeColor.getByWoolData(materialData.getData()).orElse(EnumDyeColor.WHITE);
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotItemTypeDyeColored that = (SpigotItemTypeDyeColored) o;
		return dyeColor == that.dyeColor;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), dyeColor);
	}
}

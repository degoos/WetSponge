package com.degoos.wetsponge.nbt;


import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;
import java.lang.reflect.InvocationTargetException;

public class SpigotNBTTagIntArray extends SpigotNBTBase implements WSNBTTagIntArray {

	private static final Class<?> clazz = NMSUtils.getNMSClass("NBTTagIntArray");

	public SpigotNBTTagIntArray(int[] ints) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
		this(clazz.getConstructor(int[].class).newInstance((Object) ints));
	}

	public SpigotNBTTagIntArray(Object nbtTagByteArray) {
		super(nbtTagByteArray);
	}

	@Override
	public int[] getIntArray() {
		try {
			return ReflectionUtils.getFirstObject(clazz, int[].class, getHandled());
		} catch (Exception e) {
			InternalLogger.printException(e, "An error has occurred while WetSponge was getting the value of a NBTTag!");
			return new int[0];
		}
	}


	@Override
	public WSNBTTagIntArray copy() {
		try {
			return new SpigotNBTTagIntArray(ReflectionUtils.invokeMethod(getHandled(), clazz, "clone"));
		} catch (Exception e) {
			InternalLogger.printException(e, "An exception has occurred while WetSponge was cloning a NBTTag!");
			return null;
		}
	}

	@Override
	public Object getHandled() {
		return super.getHandled();
	}
}

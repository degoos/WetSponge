package com.degoos.wetsponge.material.block;

import org.bukkit.material.MaterialData;

import java.util.Objects;

public class SpigotBlockTypeLevelled extends SpigotBlockType implements WSBlockTypeLevelled {

	private int level, maximumLevel;

	public SpigotBlockTypeLevelled(int numericalId, String oldStringId, String newStringId, int maxStackSize, int level, int maximumLevel) {
		super(numericalId, oldStringId, newStringId, maxStackSize);
		this.level = level;
		this.maximumLevel = maximumLevel;
	}

	@Override
	public int getLevel() {
		return level;
	}

	@Override
	public void setLevel(int level) {
		this.level = Math.min(maximumLevel, Math.max(0, level));
	}

	@Override
	public int getMaximumLevel() {
		return maximumLevel;
	}

	@Override
	public SpigotBlockTypeLevelled clone() {
		return new SpigotBlockTypeLevelled(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), level, maximumLevel);
	}

	@Override
	public MaterialData toMaterialData() {
		MaterialData data = super.toMaterialData();
		data.setData((byte) level);
		return data;
	}

	@Override
	public SpigotBlockTypeLevelled readMaterialData(MaterialData materialData) {
		level = materialData.getData();
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeLevelled that = (SpigotBlockTypeLevelled) o;
		return level == that.level &&
				maximumLevel == that.maximumLevel;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), level, maximumLevel);
	}
}

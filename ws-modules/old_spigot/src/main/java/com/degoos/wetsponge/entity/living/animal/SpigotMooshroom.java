package com.degoos.wetsponge.entity.living.animal;


import org.bukkit.entity.MushroomCow;

public class SpigotMooshroom extends SpigotCow implements WSMooshroom {


    public SpigotMooshroom(MushroomCow entity) {
        super(entity);
    }

    @Override
    public MushroomCow getHandled() {
        return (MushroomCow) super.getHandled();
    }
}

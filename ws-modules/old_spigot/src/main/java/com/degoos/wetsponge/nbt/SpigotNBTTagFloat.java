package com.degoos.wetsponge.nbt;


import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.MathHelper;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;
import java.lang.reflect.InvocationTargetException;

public class SpigotNBTTagFloat extends SpigotNBTBase implements WSNBTTagFloat {

	private static final Class<?> clazz = NMSUtils.getNMSClass("NBTTagFloat");

	public SpigotNBTTagFloat(float f) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
		this(clazz.getConstructor(float.class).newInstance(f));
	}

	public SpigotNBTTagFloat(Object object) {
		super(object);
	}

	private float getValue() {
		try {
			return ReflectionUtils.getFirstObject(clazz, float.class, getHandled());
		} catch (Exception e) {
			InternalLogger.printException(e, "An error has occurred while WetSponge was getting the value of a NBTTag!");
			return 0;
		}
	}

	@Override
	public long getLong() {
		return (long) getValue();
	}

	@Override
	public int getInt() {
		return MathHelper.floor(getValue());
	}

	@Override
	public short getShort() {
		return (short) MathHelper.floor(getValue());
	}

	@Override
	public byte getByte() {
		return (byte) (MathHelper.floor(getValue()) & 255);
	}

	@Override
	public double getDouble() {
		return getValue();
	}

	@Override
	public float getFloat() {
		return getValue();
	}

	@Override
	public WSNBTTagFloat copy() {
		try {
			return new SpigotNBTTagFloat(getValue());
		} catch (Exception e) {
			InternalLogger.printException(e, "An error has occurred while WetSponge was copying a NBTTag!");
			return null;
		}
	}

	@Override
	public Object getHandled() {
		return super.getHandled();
	}
}

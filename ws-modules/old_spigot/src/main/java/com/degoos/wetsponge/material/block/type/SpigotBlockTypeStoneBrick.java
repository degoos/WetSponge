package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockTypeStoneBrickType;
import com.degoos.wetsponge.material.block.SpigotBlockType;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.material.MaterialData;

import java.util.Objects;

public class SpigotBlockTypeStoneBrick extends SpigotBlockType implements WSBlockTypeStoneBrick {

	private EnumBlockTypeStoneBrickType stoneBrickType;

	public SpigotBlockTypeStoneBrick(EnumBlockTypeStoneBrickType stoneBrickType) {
		super(98, "minecraft:stonebrick", "minecraft:stone_bricks", 64);
		Validate.notNull(stoneBrickType, "Stone brick type cannot be null!");
		this.stoneBrickType = stoneBrickType;
	}

	@Override
	public String getNewStringId() {
		switch (stoneBrickType) {
			case MOSSY:
				return "minecraft:mossy_stone_bricks";
			case CRACKED:
				return "minecraft:cracked_stone_bricks";
			case CHISELED:
				return "minecraft:chiseled_stone_bricks";
			case DEFAULT:
			default:
				return "minecraft:stone_bricks";
		}
	}

	@Override
	public EnumBlockTypeStoneBrickType getStoneBrickType() {
		return stoneBrickType;
	}

	@Override
	public void setStoneBrickType(EnumBlockTypeStoneBrickType stoneBrickType) {
		Validate.notNull(stoneBrickType, "Stone brick type cannot be null!");
		this.stoneBrickType = stoneBrickType;
	}

	@Override
	public SpigotBlockTypeStoneBrick clone() {
		return new SpigotBlockTypeStoneBrick(stoneBrickType);
	}

	@Override
	public MaterialData toMaterialData() {
		MaterialData data = super.toMaterialData();
		data.setData((byte) stoneBrickType.getValue());
		return data;
	}

	@Override
	public SpigotBlockTypeStoneBrick readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		stoneBrickType = EnumBlockTypeStoneBrickType.getByValue(materialData.getData()).orElse(EnumBlockTypeStoneBrickType.DEFAULT);
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeStoneBrick that = (SpigotBlockTypeStoneBrick) o;
		return stoneBrickType == that.stoneBrickType;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), stoneBrickType);
	}
}

package com.degoos.wetsponge.hook.vault;

import java.util.Optional;
import net.milkbowl.vault.economy.EconomyResponse;

public class SpigotVaultEconomyResponse implements WSVaultEconomyResponse {

	private EconomyResponse response;

	public SpigotVaultEconomyResponse(EconomyResponse response) {
		this.response = response;
	}

	@Override
	public double getAmount() {
		return response.amount;
	}

	@Override
	public double getBalance() {
		return response.balance;
	}

	@Override
	public WSVaultResponseType getResponseType() {
		return WSVaultResponseType.getByName(response.type.name()).orElse(WSVaultResponseType.NOT_IMPLEMENTED);
	}

	@Override
	public Optional<String> getErrorMessage() {
		return Optional.ofNullable(response.errorMessage).filter(error -> !error.isEmpty());
	}
}

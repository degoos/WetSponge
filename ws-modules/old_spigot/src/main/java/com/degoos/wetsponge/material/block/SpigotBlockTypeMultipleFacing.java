package com.degoos.wetsponge.material.block;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import org.bukkit.material.MaterialData;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class SpigotBlockTypeMultipleFacing extends SpigotBlockType implements WSBlockTypeMultipleFacing {

	private Set<EnumBlockFace> faces, allowedFaces;

	public SpigotBlockTypeMultipleFacing(int numericalId, String oldStringId, String newStringId, int maxStackSize, Set<EnumBlockFace> faces, Set<EnumBlockFace> allowedFaces) {
		super(numericalId, oldStringId, newStringId, maxStackSize);
		this.faces = faces;
		this.allowedFaces = allowedFaces;
	}

	@Override
	public boolean hasFace(EnumBlockFace face) {
		return faces.contains(face);
	}

	@Override
	public void setFace(EnumBlockFace face, boolean enabled) {
		if (enabled) faces.add(face);
		else faces.remove(face);
	}

	@Override
	public Set<EnumBlockFace> getFaces() {
		return new HashSet<>(faces);
	}

	@Override
	public Set<EnumBlockFace> getAllowedFaces() {
		return new HashSet<>(allowedFaces);
	}

	@Override
	public SpigotBlockTypeMultipleFacing clone() {
		return new SpigotBlockTypeMultipleFacing(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), new HashSet<>(faces), new HashSet<>(allowedFaces));
	}

	@Override
	public MaterialData toMaterialData() {
		return super.toMaterialData();
	}

	@Override
	public SpigotBlockTypeMultipleFacing readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeMultipleFacing that = (SpigotBlockTypeMultipleFacing) o;
		return Objects.equals(faces, that.faces);
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), faces);
	}
}

package com.degoos.wetsponge.resource.spigot;

import com.degoos.wetsponge.enums.EnumTextColor;
import com.degoos.wetsponge.enums.EnumTextStyle;
import com.degoos.wetsponge.text.WSText;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SpigotTextBuilder {


    public static Map<String, EnumTextColor> colors;
    public static Map<String, EnumTextStyle> formats;

    static {
        colors = new HashMap<>();

        colors.put("0", EnumTextColor.BLACK);
        colors.put("1", EnumTextColor.DARK_BLUE);
        colors.put("2", EnumTextColor.DARK_GREEN);
        colors.put("3", EnumTextColor.DARK_AQUA);
        colors.put("4", EnumTextColor.DARK_RED);
        colors.put("5", EnumTextColor.DARK_PURPLE);
        colors.put("6", EnumTextColor.GOLD);
        colors.put("7", EnumTextColor.GRAY);
        colors.put("8", EnumTextColor.DARK_GRAY);
        colors.put("9", EnumTextColor.BLUE);
        colors.put("a", EnumTextColor.GREEN);
        colors.put("b", EnumTextColor.AQUA);
        colors.put("c", EnumTextColor.RED);
        colors.put("d", EnumTextColor.LIGHT_PURPLE);
        colors.put("e", EnumTextColor.YELLOW);
        colors.put("f", EnumTextColor.WHITE);

        formats = new HashMap<>();

        formats.put("l", EnumTextStyle.BOLD);
        formats.put("r", EnumTextStyle.RESET);
        formats.put("k", EnumTextStyle.OBFUSCATED);
        formats.put("m", EnumTextStyle.STRIKETHROUGH);
        formats.put("n", EnumTextStyle.UNDERLINE);
        formats.put("o", EnumTextStyle.ITALIC);
    }

    public static WSText sendToFactory(String s) {
        s = s + "&f";
        List<WSText> list = new ArrayList<>();
        int l = 0;
        EnumTextColor lastC = null;
        EnumTextStyle lastS = null;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (c == '\u00A7') {
                if (colors.containsKey(String.valueOf(s.charAt(i + 1)))) {
                    if (i > 0) list.add(addStyle(addColor(WSText.builder(s.substring(l + 1, i)), lastC), lastS).build());
                    lastC = colors.get(String.valueOf(s.charAt(i + 1)));
                    lastS = null;
                    i++;
                    l = i;
                } else if (formats.containsKey(String.valueOf(s.charAt(i + 1)))) {
                    if (i > 0) list.add(WSText.builder(s.substring(l + 1, i)).color(lastC).style(lastS).build());
                    lastS = formats.get(String.valueOf(s.charAt(i + 1)));
                    if (lastS.equals(EnumTextStyle.RESET))
                        lastC = null;
                    i++;
                    l = i;
                }
            }
        }
        if (!s.isEmpty())
            list.add(addColor(WSText.builder(s.substring(l, s.length() - 1)), lastC).build());
        return WSText.builder().append(list).build();
    }

    public static WSText.Builder addColor(WSText.Builder builder, EnumTextColor color) {
        if (color != null) builder.color(color);
        return builder;
    }

    public static WSText.Builder addStyle(WSText.Builder builder, EnumTextStyle style) {
        if (style != null) builder.style(style);
        return builder;
    }

}

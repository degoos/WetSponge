package com.degoos.wetsponge.text;


import com.degoos.wetsponge.enums.EnumTextColor;
import com.degoos.wetsponge.enums.EnumTextStyle;
import com.degoos.wetsponge.resource.centeredmessages.CenteredMessageSender;
import com.degoos.wetsponge.text.action.click.SpigotClickAction;
import com.degoos.wetsponge.text.action.click.WSClickAction;
import com.degoos.wetsponge.text.action.hover.SpigotHoverAction;
import com.degoos.wetsponge.text.action.hover.WSHoverAction;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.reflection.SpigotTextUtils;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.chat.TranslatableComponent;

import java.util.*;
import java.util.stream.Collectors;

public class SpigotText implements WSText {

	private BaseComponent text;

	public static SpigotText of(BaseComponent component) {
		if (component instanceof TranslatableComponent) return new SpigotTranslatableText((TranslatableComponent) component);
		return new SpigotText(component);
	}

	protected SpigotText(BaseComponent text) {
		this.text = text;
	}


	public SpigotText(String string) {
		this.text = new TextComponent(string);
	}


	public static SpigotText getByFormattingText(String text) {
		try {
			return SpigotText.of(concentrate(SpigotTextUtils.fromLegacyText(text)));
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was creating a text by a formatted text! (Text: " + text + ")");
			return new SpigotText("");
		}
	}

	@Override
	public String getText() {
		if (text instanceof TranslatableComponent) return ((TranslatableComponent) text).getTranslate();
		if (text instanceof TextComponent) return ((TextComponent) text).getText();
		return text.toString();
	}

	private static BaseComponent concentrate(BaseComponent[] texts) {
		if (texts.length == 1) return texts[0];
		return new TextComponent(texts);
	}

	@Override
	public Optional<EnumTextColor> getColor() {
		return text.getColorRaw() == null ? Optional.empty() : EnumTextColor.getByName(text.getColorRaw().getName());
	}


	@Override
	public Collection<EnumTextStyle> getStyles() {
		List<EnumTextStyle> styles = new ArrayList<>();
		if (text.isBold()) styles.add(EnumTextStyle.BOLD);
		if (text.isItalic()) styles.add(EnumTextStyle.ITALIC);
		if (text.isObfuscated()) styles.add(EnumTextStyle.OBFUSCATED);
		if (text.isStrikethrough()) styles.add(EnumTextStyle.STRIKETHROUGH);
		if (text.isUnderlined()) styles.add(EnumTextStyle.UNDERLINE);
		return styles;
	}


	@Override
	public Collection<? extends WSText> getChildren() {
		if (text.getExtra() == null) return new ArrayList<>();
		return text.getExtra().stream().map(SpigotText::of).collect(Collectors.toList());
	}


	@Override
	public Optional<WSClickAction> getClickAction() {
		return Optional.ofNullable(getHandled().getClickEvent()).map(SpigotClickAction::of);
	}


	@Override
	public Optional<WSHoverAction> getHoverAction() {
		return Optional.ofNullable(getHandled().getHoverEvent()).map(SpigotHoverAction::of);
	}


	@Override
	public boolean contains(String sequence) {
		if (getText().contains(sequence)) return true;
		for (WSText text : getChildren()) if (text.contains(sequence)) return true;
		return false;
	}


	@Override
	public List<WSText> split(String regex) {
		List<WSText> list = new ArrayList<>();

		boolean endsWithRegex = false;
		WSText clone = moveChildrenToOneList();
		WSText parent = clone.cloneSingle();
		if (parent.getText().contains(regex)) {
			endsWithRegex = parent.getText().endsWith(regex);
			String[] strings = parent.getText().split(regex);
			parent = clone.cloneSingle(strings.length > 0 ? strings[0] : "");
			if (strings.length > 1) {
				for (int i = 1; i < strings.length; i++) {
					list.add(parent);
					parent = clone.cloneSingle(strings[i]);
				}
			}
		}

		if (endsWithRegex) list.add(parent);
		WSText.Builder lastBuilder = endsWithRegex ? null : parent.toBuilder();

		Collection<? extends WSText> children = clone.getChildren();

		for (WSText child : children) {
			if (child.getText().contains(regex)) {
				String[] strings = child.getText().split(regex);
				if (strings.length <= 1) {
					WSText.Builder childBuilder = applyParent(child, parent, strings.length == 0 ? "" : strings[0]);
					if (endsWithRegex) lastBuilder = childBuilder;
					else lastBuilder.append(childBuilder.build());
					endsWithRegex = child.getText().endsWith(regex);
					if (endsWithRegex) list.add(lastBuilder.build());
					continue;
				}
				if (endsWithRegex) lastBuilder = child.cloneSingle(strings[0]).toBuilder();
				else lastBuilder.append(child.cloneSingle(strings[0]));

				endsWithRegex = child.getText().endsWith(regex);

				for (int i = 1; i < strings.length; i++) {
					list.add(lastBuilder.build());
					WSText.Builder childBuilder = applyParent(child, parent, strings[1]);
					if (!child.getColor().isPresent()) {
						Collection<EnumTextStyle> styles = child.getStyles();
						styles.addAll(parent.getStyles());
						childBuilder.style(styles);
					}
					if (!child.getColor().isPresent() && parent.getColor().isPresent()) childBuilder.color(parent.getColor().get());
					if (!child.getHoverAction().isPresent() && parent.getHoverAction().isPresent()) childBuilder.hoverAction(parent.getHoverAction().get());
					if (!child.getClickAction().isPresent() && parent.getClickAction().isPresent()) childBuilder.clickAction(parent.getClickAction().get());
					lastBuilder = childBuilder;
				}
				if (endsWithRegex) list.add(lastBuilder.build());

			} else {
				WSText.Builder childBuilder = applyParent(child, parent, child.getText());
				if (endsWithRegex) lastBuilder = childBuilder;
				else lastBuilder.append(childBuilder.build());
				endsWithRegex = false;
			}
		}
		if (!endsWithRegex) list.add(lastBuilder.build());
		return list;
	}


	private WSText.Builder applyParent(WSText child, WSText parent, String name) {
		WSText.Builder childBuilder = child.cloneSingle(name).toBuilder();
		if (!child.getColor().isPresent()) {
			Collection<EnumTextStyle> styles = child.getStyles();
			styles.addAll(parent.getStyles());
			childBuilder.style(styles);
		}
		if (!child.getColor().isPresent() && parent.getColor().isPresent()) childBuilder.color(parent.getColor().get());
		if (!child.getHoverAction().isPresent() && parent.getHoverAction().isPresent()) childBuilder.hoverAction(parent.getHoverAction().get());
		if (!child.getClickAction().isPresent() && parent.getClickAction().isPresent()) childBuilder.clickAction(parent.getClickAction().get());
		return childBuilder;
	}


	@Override
	public WSText moveChildrenToOneList() {
		WSText.Builder builder = cloneSingle().toBuilder();
		getChildren().forEach(child -> {
			builder.append(child.cloneSingle());
			child.getChildren().forEach(childChild -> append(builder, childChild, child));
		});
		return builder.build();
	}


	private void append(WSText.Builder builder, WSText child, WSText parent) {
		WSText.Builder childBuilder = child.cloneSingle().toBuilder();
		if (!child.getColor().isPresent()) {
			Collection<EnumTextStyle> styles = child.getStyles();
			styles.addAll(parent.getStyles());
			childBuilder.style(styles);
		}
		if (!child.getColor().isPresent() && parent.getColor().isPresent()) childBuilder.color(parent.getColor().get());
		if (!child.getHoverAction().isPresent() && parent.getHoverAction().isPresent()) childBuilder.hoverAction(parent.getHoverAction().get());
		if (!child.getClickAction().isPresent() && parent.getClickAction().isPresent()) childBuilder.clickAction(parent.getClickAction().get());
		WSText newChild = childBuilder.build();
		builder.append(newChild);
		child.getChildren().forEach(childChild -> append(builder, childChild, newChild));
	}


	@Override
	public WSText replace(String toReplace, String replacement) {
		WSText.Builder clone = cloneSingle(getText().replace(toReplace, replacement)).toBuilder();
		getChildren().forEach(child -> clone.append(child.replace(toReplace, replacement)));
		return clone.build();
	}


	@Override
	public WSText replace(String toReplace, WSText replacement) {
		if (!contains(toReplace)) return this;
		boolean endsWithToReplace = toPlain().endsWith(toReplace);
		List<WSText> split = split(toReplace);
		WSText.Builder firstBuilder = split.get(0).toBuilder();
		firstBuilder.append(replacement);
		for (int i = 1; i < split.size() - (endsWithToReplace ? 0 : 1); i++) {
			WSText text = split.get(i);
			WSText.Builder builder = text.toBuilder();
			builder.append(applyParent(replacement, text, replacement.getText()).build());
			firstBuilder.append(builder.build());
		}
		return firstBuilder.build();
	}


	@Override
	public String toFormattingText() {
		StringBuilder builder = new StringBuilder();
		if (text.getColorRaw() != null) builder.append(text.getColorRaw().toString());
		getStyles().forEach(style -> builder.append(style.toString()));
		if (text instanceof TranslatableComponent) builder.append(((TranslatableComponent) text).getTranslate());
		if (text instanceof TextComponent) builder.append(((TextComponent) text).getText());
		getChildren().forEach(child -> builder.append(child.toFormattingText()));
		return builder.toString();
	}

	@Override
	public String toPlain() {
		return text.toPlainText();
	}

	@Override
	public WSText clone() {
		return clone(getText());
	}

	@Override
	public WSText clone(String string) {
		WSText.Builder text = WSText.builder(string);
		getColor().ifPresent(text::color);
		text.style(getStyles());
		getHoverAction().ifPresent(text::hoverAction);
		getClickAction().ifPresent(text::clickAction);
		getChildren().forEach(child -> text.append(child.clone()));
		return text.build();
	}

	@Override
	public WSText cloneSingle() {
		return cloneSingle(getText());
	}

	@Override
	public WSText cloneSingle(String string) {
		WSText.Builder text = WSText.builder(string);
		getColor().ifPresent(text::color);
		text.style(getStyles());
		getHoverAction().ifPresent(text::hoverAction);
		getClickAction().ifPresent(text::clickAction);
		return text.build();
	}

	@Override
	public BaseComponent getHandled() {
		return text;
	}


	public String toString() {
		return text.toString();
	}


	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		SpigotText that = (SpigotText) o;

		return that.toFormattingText().equals(toFormattingText());
	}


	@Override
	public int hashCode() {
		return toFormattingText().hashCode();
	}


	public static class Builder implements WSText.Builder {

		private BaseComponent baseComponent;


		public Builder() {
			baseComponent = new TextComponent();
		}


		public Builder(String string) {
			baseComponent = new TextComponent(string);
		}


		public Builder(WSText text) {
			baseComponent = ((SpigotText) text).getHandled().duplicate();
		}


		@Override
		public WSText build() {
			return SpigotText.of(baseComponent);
		}


		@Override
		public WSText.Builder color(EnumTextColor color) {
			baseComponent.setColor(ChatColor.getByChar(color.getId()));
			return this;
		}


		@Override
		public WSText.Builder style(EnumTextStyle style) {
			return style(Collections.singleton(style));
		}


		@Override
		public WSText.Builder style(Collection<EnumTextStyle> styles) {
			for (EnumTextStyle textStyle : styles) {
				switch (textStyle) {
					case OBFUSCATED:
						baseComponent.setObfuscated(true);
						break;
					case BOLD:
						baseComponent.setBold(true);
						break;
					case STRIKETHROUGH:
						baseComponent.setStrikethrough(true);
						break;
					case UNDERLINE:
						baseComponent.setUnderlined(true);
						break;
					case ITALIC:
						baseComponent.setItalic(true);
						break;
					case RESET:
						baseComponent.setObfuscated(false);
						baseComponent.setBold(false);
						baseComponent.setStrikethrough(false);
						baseComponent.setUnderlined(false);
						baseComponent.setItalic(false);
						break;
				}
			}
			return this;
		}


		@Override
		public WSText.Builder clickAction(WSClickAction action) {
			if (action == null) baseComponent.setClickEvent(null);
			baseComponent.setClickEvent(((SpigotClickAction) action).getHandled());
			return this;
		}


		@Override
		public WSText.Builder hoverAction(WSHoverAction action) {
			if (action == null) baseComponent.setHoverEvent(null);
			else baseComponent.setHoverEvent(((SpigotHoverAction) action).getHandled());
			return this;
		}


		@Override
		public WSText.Builder append(WSText... children) {
			for (WSText text : children)
				baseComponent.addExtra(((SpigotText) text).getHandled());
			return this;
		}


		@Override
		public WSText.Builder append(Collection<? extends WSText> children) {
			children.forEach(text -> baseComponent.addExtra(((SpigotText) text).getHandled()));
			return this;
		}


		@Override
		public WSText.Builder newLine() {
			baseComponent.addExtra("\n");
			return this;
		}


		@Override
		public WSText.Builder center() {
			baseComponent = new TextComponent(CenteredMessageSender.getCenteredMessage(SpigotText.of(baseComponent).toFormattingText()));
			return this;
		}

		@Override
		public WSText.Builder stripColors() {
			stripColor(baseComponent);
			return this;
		}

		private void stripColor(BaseComponent baseComponent) {
			baseComponent.setColor(null);
			baseComponent.getExtra().forEach(this::stripColor);
		}


		@Override
		public WSText.Builder translateColors() {
			baseComponent = new TextComponent(TextComponent.fromLegacyText(SpigotText.of(baseComponent).toFormattingText().replace('&', '\u00A7')));
			return this;
		}
	}
}

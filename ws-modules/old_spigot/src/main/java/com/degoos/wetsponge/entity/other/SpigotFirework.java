package com.degoos.wetsponge.entity.other;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.entity.SpigotEntity;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.firework.SpigotFireworkEffect;
import com.degoos.wetsponge.firework.WSFireworkEffect;
import com.degoos.wetsponge.util.ListUtils;
import com.degoos.wetsponge.util.reflection.SpigotHandledUtils;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.bukkit.entity.Firework;
import org.bukkit.inventory.meta.FireworkMeta;

public class SpigotFirework extends SpigotEntity implements WSFirework {

	private static final Class<?> FIREWORK_CLASS = NMSUtils.getNMSClass("EntityFireworks");

	public SpigotFirework(Firework entity) {
		super(entity);
	}

	@Override
	public List<WSFireworkEffect> getFireworkEffects() {
		return getHandled().getFireworkMeta().getEffects().stream().map(SpigotFireworkEffect::new).collect(Collectors.toList());
	}

	@Override
	public void setFireworkEffects(Collection<WSFireworkEffect> effects) {
		FireworkMeta meta = getHandled().getFireworkMeta();
		meta.clearEffects();
		meta.addEffects(effects.stream().map(effect -> ((SpigotFireworkEffect) effect).getHandled()).collect(Collectors.toList()));
		getHandled().setFireworkMeta(meta);
	}

	@Override
	public boolean addFireworkEffect(WSFireworkEffect effect) {
		FireworkMeta meta = getHandled().getFireworkMeta();
		meta.addEffect(((SpigotFireworkEffect) effect).getHandled());
		getHandled().setFireworkMeta(meta);
		return true;
	}

	@Override
	public boolean addFireworkEffects(WSFireworkEffect... effects) {
		FireworkMeta meta = getHandled().getFireworkMeta();
		meta.addEffects(Arrays.stream(effects).map(effect -> ((SpigotFireworkEffect) effect).getHandled()).collect(Collectors.toList()));
		getHandled().setFireworkMeta(meta);
		return true;
	}

	@Override
	public boolean addFireworkEffects(List<WSFireworkEffect> effects) {
		FireworkMeta meta = getHandled().getFireworkMeta();
		meta.addEffects(effects.stream().map(effect -> ((SpigotFireworkEffect) effect).getHandled()).collect(Collectors.toList()));
		getHandled().setFireworkMeta(meta);
		return true;
	}

	@Override
	public boolean removeFireworkEffect(WSFireworkEffect effect) {
		List<WSFireworkEffect> effects = getFireworkEffects();
		boolean removed = effects.remove(effect);
		setFireworkEffects(effects);
		return removed;
	}

	@Override
	public boolean removeFireworkEffects(WSFireworkEffect... effects) {
		List<WSFireworkEffect> list = getFireworkEffects();
		boolean removed = list.removeAll(ListUtils.toList(effects));
		setFireworkEffects(list);
		return removed;
	}

	@Override
	public boolean removeFireworkEffects(List<WSFireworkEffect> effects) {
		List<WSFireworkEffect> list = getFireworkEffects();
		boolean removed = list.removeAll(effects);
		setFireworkEffects(list);
		return removed;
	}

	@Override
	public void clearFireworkEffects() {
		FireworkMeta meta = getHandled().getFireworkMeta();
		meta.clearEffects();
		getHandled().setFireworkMeta(meta);
	}

	@Override
	public boolean hasEffects() {
		return getHandled().getFireworkMeta().hasEffects();
	}

	@Override
	public int getEffectSize() {
		return getHandled().getFireworkMeta().getEffectsSize();
	}

	@Override
	public int getLifeTime() {
		try {
			int i = WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD) ? 3 : 1;
			return ReflectionUtils.setAccessible(FIREWORK_CLASS.getDeclaredFields()[i]).getInt(getNMSHandler());
		} catch (Throwable ex) {
			ex.printStackTrace();
			return 0;
		}
	}

	@Override
	public void setLifeTime(int lifeTime) {
		try {
			int i = WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD) ? 3 : 1;
			ReflectionUtils.setAccessible(FIREWORK_CLASS.getDeclaredFields()[i]).setInt(getNMSHandler(), lifeTime);
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public int getFireworkAge() {
		try {
			int i = WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD) ? 2 : 0;
			return ReflectionUtils.setAccessible(FIREWORK_CLASS.getDeclaredFields()[i]).getInt(getNMSHandler());
		} catch (Throwable ex) {
			ex.printStackTrace();
			return 0;
		}
	}

	@Override
	public void setFireworkAge(int fireworkAge) {
		try {
			int i = WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD) ? 2 : 0;
			ReflectionUtils.setAccessible(FIREWORK_CLASS.getDeclaredFields()[i]).setInt(getNMSHandler(), fireworkAge);
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}


	@Override
	public void detonate() {
		getHandled().detonate();
	}

	@Override
	public Firework getHandled() {
		return (Firework) super.getHandled();
	}

	public Object getNMSHandler() {
		return SpigotHandledUtils.getEntityHandle(getHandled());
	}
}

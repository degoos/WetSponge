package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.enums.block.EnumBlockTypeComparatorMode;
import com.degoos.wetsponge.material.block.SpigotBlockTypeDirectional;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.material.Comparator;
import org.bukkit.material.MaterialData;

import java.util.Objects;
import java.util.Set;

public class SpigotBlockTypeComparator extends SpigotBlockTypeDirectional implements WSBlockTypeComparator {

	private EnumBlockTypeComparatorMode mode;
	private boolean powered;

	public SpigotBlockTypeComparator(EnumBlockFace facing, Set<EnumBlockFace> faces, EnumBlockTypeComparatorMode mode, boolean powered) {
		super(149, "minecraft:unpowered_comparator", "minecraft:comparator", 64, facing, faces);
		Validate.notNull(mode, "Mode cannot be null!");
		this.mode = mode;
		this.powered = powered;
	}

	@Override
	public int getNumericalId() {
		return isPowered() ? 150 : 149;
	}

	@Override
	public String getOldStringId() {
		return isPowered() ? "minecraft:powered_comparator" : "minecraft:unpowered_comparator";
	}

	@Override
	public EnumBlockTypeComparatorMode getMode() {
		return mode;
	}

	@Override
	public void setMode(EnumBlockTypeComparatorMode mode) {
		Validate.notNull(mode, "Mode cannot be null!");
		this.mode = mode;
	}

	@Override
	public boolean isPowered() {
		return powered;
	}

	@Override
	public void setPowered(boolean powered) {
		this.powered = powered;
	}

	@Override
	public SpigotBlockTypeComparator clone() {
		return new SpigotBlockTypeComparator(getFacing(), getFaces(), mode, powered);
	}

	@Override
	public MaterialData toMaterialData() {
		MaterialData data = super.toMaterialData();
		if (data instanceof Comparator) {
			((Comparator) data).setSubtractionMode(mode == EnumBlockTypeComparatorMode.SUBTRACT);
		}
		return data;
	}

	@Override
	public SpigotBlockTypeDirectional readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		if (materialData instanceof Comparator) {
			mode = ((Comparator) materialData).isSubtractionMode() ? EnumBlockTypeComparatorMode.SUBTRACT : EnumBlockTypeComparatorMode.COMPARE;
			powered = ((Comparator) materialData).isPowered();
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeComparator that = (SpigotBlockTypeComparator) o;
		return powered == that.powered &&
				mode == that.mode;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), mode, powered);
	}
}

package com.degoos.wetsponge.entity.vehicle;

import com.degoos.wetsponge.entity.SpigotEntity;
import org.bukkit.entity.Boat;

public class SpigotBoat extends SpigotEntity implements WSBoat {

    public SpigotBoat(Boat entity) {
        super(entity);
    }

    @Override
    public boolean isInWater() {
        return false;
    }

    @Override
    public double getMaxSpeed() {
        return getHandled().getMaxSpeed();
    }

    @Override
    public void setMaxSpeed(double maxSpeed) {
        getHandled().setMaxSpeed(maxSpeed);
    }

    @Override
    public boolean canMoveOnLand() {
        return getHandled().getWorkOnLand();
    }

    @Override
    public void setMoveOnLand(boolean moveOnLand) {
        getHandled().setWorkOnLand(moveOnLand);
    }

    @Override
    public double getOccupiedDeceleration() {
        return getHandled().getOccupiedDeceleration();
    }

    @Override
    public void setOccupiedDeceleration(double occupiedDeceleration) {
        getHandled().setOccupiedDeceleration(occupiedDeceleration);
    }

    @Override
    public double getUnoccupiedDeceleration() {
        return getHandled().getUnoccupiedDeceleration();
    }

    @Override
    public void setUnoccupiedDeceleration(double unoccupiedDeceleration) {
        getHandled().setUnoccupiedDeceleration(unoccupiedDeceleration);
    }


    @Override
    public Boat getHandled() {
        return (Boat) super.getHandled();
    }

}

package com.degoos.wetsponge.entity.living;


import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.parser.entity.SpigotEntityParser;
import com.degoos.wetsponge.util.reflection.SpigotEntityUtils;
import java.util.Optional;
import org.bukkit.entity.Creature;

public class SpigotCreature extends SpigotLivingEntity implements WSCreature {


	public SpigotCreature(Creature entity) {
		super(entity);
	}


	@Override
	public void setAI(boolean ai) {
		if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD)) getHandled().setAI(ai);
		else SpigotEntityUtils.setAI(getHandled(), ai);
	}


	@Override
	public boolean hasAI() {
		if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD)) return getHandled().hasAI();
		else return SpigotEntityUtils.hasAI(getHandled());
	}


	@Override
	public Optional<WSEntity> getTarget() {
		return Optional.ofNullable(getHandled().getTarget()).map(SpigotEntityParser::getWSEntity);
	}


	@Override
	public void setTarget(WSEntity entity) {
		if (entity == null) getHandled().setTarget(null);
		else if (entity instanceof SpigotLivingEntity) getHandled().setTarget(((SpigotLivingEntity) entity).getHandled());
	}


	@Override
	public Creature getHandled() {
		return (Creature) super.getHandled();
	}
}

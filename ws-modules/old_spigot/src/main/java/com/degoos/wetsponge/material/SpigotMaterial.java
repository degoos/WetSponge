package com.degoos.wetsponge.material;

import com.degoos.wetsponge.nbt.WSNBTTagCompound;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.MaterialData;

public interface SpigotMaterial extends WSMaterial {

	void writeItemMeta(ItemMeta itemMeta);

	void readItemMeta(ItemMeta itemMeta);

	void writeNBTTag(WSNBTTagCompound compound);

	void readNBTTag(WSNBTTagCompound compound);

	MaterialData toMaterialData();

	SpigotMaterial readMaterialData(MaterialData materialData);

}

package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockTypeTallGrassType;
import com.degoos.wetsponge.material.block.SpigotBlockType;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.material.MaterialData;

import java.util.Objects;

public class SpigotBlockTypeTallGrassType extends SpigotBlockType implements WSBlockTypeTallGrass {

	private EnumBlockTypeTallGrassType tallGrassType;

	public SpigotBlockTypeTallGrassType(EnumBlockTypeTallGrassType tallGrassType) {
		super(31, "minecraft:tallgrass", "minecraft:grass", 64);
		Validate.notNull(tallGrassType, "Tall grass type cannot be null!");
		this.tallGrassType = tallGrassType;
	}

	@Override
	public String getNewStringId() {
		switch (tallGrassType) {
			case FERN:
				return "minecraft:fern";
			case DEAD_BUSH:
				return "minecraft:dead_bush";
			case TALL_GRASS:
			default:
				return "minecraft:grass";
		}
	}

	@Override
	public EnumBlockTypeTallGrassType getTallGrassType() {
		return tallGrassType;
	}

	@Override
	public void setTallGrassType(EnumBlockTypeTallGrassType tallGrassType) {
		Validate.notNull(tallGrassType, "Tall grass type cannot be null!");
		this.tallGrassType = tallGrassType;
	}

	@Override
	public SpigotBlockTypeTallGrassType clone() {
		return new SpigotBlockTypeTallGrassType(tallGrassType);
	}


	@Override
	public MaterialData toMaterialData() {
		MaterialData data = super.toMaterialData();
		data.setData((byte) tallGrassType.getValue());
		return data;
	}

	@Override
	public SpigotBlockTypeTallGrassType readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		tallGrassType = EnumBlockTypeTallGrassType.getByValue(materialData.getData()).orElse(EnumBlockTypeTallGrassType.DEAD_BUSH);
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeTallGrassType that = (SpigotBlockTypeTallGrassType) o;
		return tallGrassType == that.tallGrassType;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), tallGrassType);
	}
}

package com.degoos.wetsponge.util.reflection;

import org.bukkit.entity.Entity;

public class SpigotEntityUtils {

    public static void setAI(Entity entity, boolean ai) {
        try {
            Object nmsEntity = entity.getClass().getDeclaredMethod("getHandle").invoke(entity);
            Object tag = null;
            if (tag == null) {
                tag = NMSUtils.getNMSClass("NBTTagCompound").getConstructor().newInstance();
            }
            nmsEntity.getClass().getMethod("c", tag.getClass()).invoke(nmsEntity, tag);
            tag.getClass().getMethod("setInt", String.class, int.class).invoke(tag, "NoAI", ai ? 0 : 1);
            nmsEntity.getClass().getMethod("f", tag.getClass()).invoke(nmsEntity, tag);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public static boolean hasAI(Entity entity) {
        try {
            Object nmsEntity = entity.getClass().getDeclaredMethod("getHandle").invoke(entity);
            Object tag = null;
            if (tag == null) {
                tag = NMSUtils.getNMSClass("NBTTagCompound").getConstructor().newInstance();
            }
            nmsEntity.getClass().getMethod("c", tag.getClass()).invoke(nmsEntity, tag);
            return ((int) tag.getClass().getMethod("getInt", String.class).invoke(tag, "NoAI")) == 0;
        } catch (Throwable e) {
            e.printStackTrace();
            return false;
        }
    }
}

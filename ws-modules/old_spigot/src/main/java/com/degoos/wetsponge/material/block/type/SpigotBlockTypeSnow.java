package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.material.block.SpigotBlockType;
import org.bukkit.material.MaterialData;

import java.util.Objects;

public class SpigotBlockTypeSnow extends SpigotBlockType implements WSBlockTypeSnow {

	private int layers, minimumLayers, maximumLayers;

	public SpigotBlockTypeSnow(int layers, int minimumLayers, int maximumLayers) {
		super(78, "minecraft:snow_layer", "minecraft:snow", 64);
		this.layers = layers;
		this.minimumLayers = minimumLayers;
		this.maximumLayers = maximumLayers;
	}

	@Override
	public int getLayers() {
		return layers;
	}

	@Override
	public void setLayers(int layers) {
		layers = Math.max(minimumLayers, Math.min(maximumLayers, layers));
	}

	@Override
	public int getMinimumLayers() {
		return minimumLayers;
	}

	@Override
	public int getMaximumLayers() {
		return maximumLayers;
	}

	@Override
	public SpigotBlockTypeSnow clone() {
		return new SpigotBlockTypeSnow(layers, minimumLayers, maximumLayers);
	}

	@Override
	public MaterialData toMaterialData() {
		MaterialData data = super.toMaterialData();
		data.setData((byte) Math.max(0, layers - 1));
		return data;
	}

	@Override
	public SpigotBlockTypeSnow readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		layers = materialData.getData() + 1;
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeSnow that = (SpigotBlockTypeSnow) o;
		return layers == that.layers &&
				minimumLayers == that.minimumLayers &&
				maximumLayers == that.maximumLayers;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), layers, minimumLayers, maximumLayers);
	}
}

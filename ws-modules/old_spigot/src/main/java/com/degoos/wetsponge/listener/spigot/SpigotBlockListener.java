package com.degoos.wetsponge.listener.spigot;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.block.SpigotBlock;
import com.degoos.wetsponge.block.SpigotBlockSnapshot;
import com.degoos.wetsponge.block.WSBlock;
import com.degoos.wetsponge.block.WSBlockSnapshot;
import com.degoos.wetsponge.data.WSTransaction;
import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.event.block.*;
import com.degoos.wetsponge.material.WSBlockTypes;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.SpigotEventUtils;
import com.degoos.wetsponge.world.SpigotLocation;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.*;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class SpigotBlockListener implements Listener {

	@EventHandler(priority = EventPriority.LOWEST)
	public void onBlockBreak(BlockBreakEvent event) {
		if (!SpigotEventUtils.shouldBeExecuted()) return;
		try {
			WSTransaction<WSBlockSnapshot> transaction = new WSTransaction<>(new SpigotBlockSnapshot(event.getBlock()), new WSBlockSnapshot(WSBlockTypes.AIR
					.getDefaultState()));

			WSBlockBreakEvent wetSpongeEvent = new WSBlockBreakEvent(transaction, new SpigotLocation(event.getBlock().getLocation()), Optional
					.ofNullable(event.getPlayer())
					.map(player -> WetSponge.getServer().getPlayer(player.getName()).<NullPointerException>orElseThrow(NullPointerException::new)));
			WetSponge.getEventManager().callEvent(wetSpongeEvent);

			event.setCancelled(wetSpongeEvent.isCancelled());

			if (wetSpongeEvent.getTransaction().getNewData().getMaterial().getNumericalId() != 0) {
				wetSpongeEvent.setCancelled(true);
				wetSpongeEvent.getTransaction().getNewData().setToBlock(new SpigotBlock(event.getBlock()));
			}
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Spigot-BlockBreakEvent!");
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onBlockPlaced(BlockPlaceEvent event) {
		if (!SpigotEventUtils.shouldBeExecuted()) return;
		try {
			WSTransaction<WSBlockSnapshot> transaction = new WSTransaction<>(new SpigotBlockSnapshot(event.getBlockAgainst()), new SpigotBlockSnapshot(event
					.getBlockPlaced()));

			WSBlockPlaceEvent wetSpongeEvent = new WSBlockPlaceEvent(transaction, new SpigotLocation(event.getBlock().getLocation()), Optional
					.ofNullable(event.getPlayer())
					.map(player -> WetSponge.getServer().getPlayer(player.getName()).<NullPointerException>orElseThrow(NullPointerException::new)));
			WetSponge.getEventManager().callEvent(wetSpongeEvent);

			event.setCancelled(wetSpongeEvent.isCancelled());
			if (!wetSpongeEvent.isCancelled() && wetSpongeEvent.getTransaction().getNewData().hasChanged())
				wetSpongeEvent.getTransaction().getNewData().setToBlock(new SpigotBlock(event.getBlockPlaced()));
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Spigot-BlockPlaceEvent!");
		}
	}

	@EventHandler
	public void onBlockFromTo(BlockFromToEvent event) {
		if (!SpigotEventUtils.shouldBeExecuted()) return;
		try {
			WSTransaction<WSBlockSnapshot> transaction = new WSTransaction<>(new SpigotBlockSnapshot(event.getBlock()), new SpigotBlockSnapshot(event.getToBlock()));

			WSBlockChangeEvent wetSpongeEvent;
			if (transaction.getNewData().getMaterial().getNumericalId() == transaction.getOldData().getMaterial().getNumericalId())
				wetSpongeEvent = new WSBlockModifyEvent(transaction, new SpigotLocation(event.getBlock().getLocation()), Optional.empty());
			else
				wetSpongeEvent = new WSBlockChangeEvent(transaction, new SpigotLocation(event.getBlock().getLocation()));
			WetSponge.getEventManager().callEvent(wetSpongeEvent);
			event.setCancelled(wetSpongeEvent.isCancelled());

			if (!wetSpongeEvent.isCancelled() && wetSpongeEvent.getTransaction().getNewData().hasChanged())
				wetSpongeEvent.getTransaction().getNewData().setToBlock(new SpigotBlock(event.getToBlock()));
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Spigot-BlockFromToEvent!");
		}
	}

	@EventHandler
	public void onPistonExtendEvent(BlockPistonExtendEvent event) {
		Set<WSBlock> blocks = event.getBlocks().stream().map(SpigotBlock::new)
				.collect(Collectors.toSet());
		EnumBlockFace direction = EnumBlockFace.valueOf(event.getDirection().name());
		WSPistonExtendEvent wetSpongeEvent = new WSPistonExtendEvent(new SpigotBlock(event.getBlock()), blocks, direction);
		WetSponge.getEventManager().callEvent(wetSpongeEvent);
		event.setCancelled(wetSpongeEvent.isCancelled());
	}

	@EventHandler
	public void onPistonRetractEvent(BlockPistonRetractEvent event) {
		Set<WSBlock> blocks = event.getBlocks().stream().map(SpigotBlock::new)
				.collect(Collectors.toSet());
		EnumBlockFace direction = EnumBlockFace.valueOf(event.getDirection().name());
		WSPistonRetractEvent wetSpongeEvent = new WSPistonRetractEvent(new SpigotBlock(event.getBlock()), blocks, direction);
		WetSponge.getEventManager().callEvent(wetSpongeEvent);
		event.setCancelled(wetSpongeEvent.isCancelled());
	}

}

package com.degoos.wetsponge.block.tileentity;

import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.block.SpigotBlock;
import org.bukkit.Nameable;
import org.bukkit.inventory.InventoryHolder;

public class SpigotTileEntityNameableInventory extends SpigotTileEntityInventory implements WSTileEntityNameableInventory {


    public SpigotTileEntityNameableInventory(SpigotBlock block) {
        super(block);
    }

    @Override
    public WSText getCustomName() {
        return WSText.getByFormattingText(((Nameable) getHandled()).getCustomName());
    }

    @Override
    public void setCustomName(WSText customName) {
        ((Nameable) getHandled()).setCustomName(customName.toFormattingText());
        update();
    }

    @Override
    public InventoryHolder getHandled() {
        return super.getHandled();
    }

}

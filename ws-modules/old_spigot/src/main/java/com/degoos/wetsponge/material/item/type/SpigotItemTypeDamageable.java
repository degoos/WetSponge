package com.degoos.wetsponge.material.item.type;

import com.degoos.wetsponge.material.item.SpigotItemType;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

import java.util.Objects;

public class SpigotItemTypeDamageable extends SpigotItemType implements WSItemTypeDamageable {

	private int damage, maxUses;

	public SpigotItemTypeDamageable(int numericalId, String oldStringId, String newStringId, int damage, int maxUses) {
		super(numericalId, oldStringId, newStringId, 1);
		this.damage = damage;
		this.maxUses = maxUses;
	}

	@Override
	public int getDamage() {
		return damage;
	}

	@Override
	public void setDamage(int damage) {
		this.damage = damage;
	}

	@Override
	public int getMaxUses() {
		return maxUses;
	}

	@Override
	public SpigotItemTypeDamageable clone() {
		return new SpigotItemTypeDamageable(getNumericalId(), getOldStringId(), getNewStringId(), damage, maxUses);
	}

	@Override
	public void writeNBTTag(WSNBTTagCompound compound) {
		super.writeNBTTag(compound);
		compound.setInteger("Damage", damage);
	}

	@Override
	public void readNBTTag(WSNBTTagCompound compound) {
		super.readNBTTag(compound);
		damage = compound.getInteger("Damage");
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotItemTypeDamageable that = (SpigotItemTypeDamageable) o;
		return damage == that.damage &&
				maxUses == that.maxUses;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), damage, maxUses);
	}
}

package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockTypeSandType;
import com.degoos.wetsponge.material.block.SpigotBlockType;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.material.MaterialData;

import java.util.Objects;

public class SpigotBlockTypeSand extends SpigotBlockType implements WSBlockTypeSand {

	private EnumBlockTypeSandType sandType;

	public SpigotBlockTypeSand(EnumBlockTypeSandType sandType) {
		super(12, "minecraft:sand", "minecraft:sand", 64);
		Validate.notNull(sandType, "Sand type cannot be null!");
		this.sandType = sandType;
	}

	@Override
	public String getNewStringId() {
		return sandType == EnumBlockTypeSandType.NORMAL ? "minecraft:sand" : "minecraft:red_sand";
	}

	@Override
	public EnumBlockTypeSandType getSandType() {
		return sandType;
	}

	@Override
	public void setSandType(EnumBlockTypeSandType sandType) {
		Validate.notNull(sandType, "Sand type cannot be null!");
		this.sandType = sandType;
	}

	@Override
	public SpigotBlockTypeSand clone() {
		return new SpigotBlockTypeSand(sandType);
	}

	@Override
	public MaterialData toMaterialData() {
		MaterialData materialData = super.toMaterialData();
		materialData.setData((byte) sandType.getValue());
		return materialData;
	}

	@Override
	public SpigotBlockTypeSand readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		sandType = EnumBlockTypeSandType.getByValue(materialData.getData()).orElse(EnumBlockTypeSandType.NORMAL);
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeSand that = (SpigotBlockTypeSand) o;
		return sandType == that.sandType;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), sandType);
	}
}

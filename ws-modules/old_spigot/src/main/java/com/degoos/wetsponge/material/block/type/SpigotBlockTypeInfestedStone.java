package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockTypeDisguiseType;
import com.degoos.wetsponge.material.block.SpigotBlockType;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.material.MaterialData;

import java.util.Objects;

public class SpigotBlockTypeInfestedStone extends SpigotBlockType implements WSBlockTypeInfestedStone {

	private EnumBlockTypeDisguiseType disguiseType;

	public SpigotBlockTypeInfestedStone(EnumBlockTypeDisguiseType disguiseType) {
		super(97, "minecraft:monster_egg", "minecraft:infested_stone", 64);
		Validate.notNull(disguiseType, "Disguise type cannot be null!");
		this.disguiseType = disguiseType;
	}

	@Override
	public String getNewStringId() {
		switch (disguiseType) {
			case STONE_BRICK:
				return "minecraft:infested_stone_bricks";
			case COBBLESTONE:
				return "minecraft:infested_cobblestone";
			case MOSSY_STONE_BRICK:
				return "minecraft:infested_mossy_stone_bricks";
			case CRACKED_STONE_BRICK:
				return "minecraft:infested_cracked_stone_bricks";
			case CHISELED_STONE_BRICK:
				return "minecraft:infested_chiseled_stone_bricks";
			case STONE:
			default:
				return "minecraft:infested_stone";
		}
	}

	@Override
	public EnumBlockTypeDisguiseType getDisguiseType() {
		return disguiseType;
	}

	@Override
	public void setDisguiseType(EnumBlockTypeDisguiseType disguiseType) {
		Validate.notNull(disguiseType, "Disguise type cannot be null!");
		this.disguiseType = disguiseType;
	}

	@Override
	public SpigotBlockTypeInfestedStone clone() {
		return new SpigotBlockTypeInfestedStone(disguiseType);
	}

	@Override
	public MaterialData toMaterialData() {
		MaterialData data = super.toMaterialData();
		data.setData((byte) disguiseType.getValue());
		return data;
	}

	@Override
	public SpigotBlockTypeInfestedStone readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		disguiseType = EnumBlockTypeDisguiseType.getByValue(materialData.getData()).orElse(EnumBlockTypeDisguiseType.STONE);
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeInfestedStone that = (SpigotBlockTypeInfestedStone) o;
		return disguiseType == that.disguiseType;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), disguiseType);
	}
}

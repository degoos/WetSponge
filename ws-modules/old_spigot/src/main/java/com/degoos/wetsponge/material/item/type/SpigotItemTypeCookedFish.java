package com.degoos.wetsponge.material.item.type;

import com.degoos.wetsponge.enums.item.EnumItemTypeCookedFishType;
import com.degoos.wetsponge.material.item.SpigotItemType;
import com.degoos.wetsponge.util.Validate;

import java.util.Objects;

public class SpigotItemTypeCookedFish extends SpigotItemType implements WSItemTypeCookedFish {

	private EnumItemTypeCookedFishType cookedFishType;

	public SpigotItemTypeCookedFish(EnumItemTypeCookedFishType cookedFishType) {
		super(263, "minecraft:cooked_fish", "minecraft:cooked_cod", 64);
		Validate.notNull(cookedFishType, "Cooked fish type cannot be null!");
		this.cookedFishType = cookedFishType;
	}

	@Override
	public String getNewStringId() {
		switch (cookedFishType) {
			case SALMON:
				return "minecraft:cooked_salmon";
			case COD:
			default:
				return "minecraft:cooked_cod";
		}
	}

	@Override
	public EnumItemTypeCookedFishType getCookedFishType() {
		return cookedFishType;
	}

	public void setCookedFishType(EnumItemTypeCookedFishType cookedFishType) {
		Validate.notNull(cookedFishType, "Cooked fish type cannot be null!");
		this.cookedFishType = cookedFishType;
	}

	@Override
	public SpigotItemTypeCookedFish clone() {
		return new SpigotItemTypeCookedFish(cookedFishType);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotItemTypeCookedFish that = (SpigotItemTypeCookedFish) o;
		return cookedFishType == that.cookedFishType;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), cookedFishType);
	}
}

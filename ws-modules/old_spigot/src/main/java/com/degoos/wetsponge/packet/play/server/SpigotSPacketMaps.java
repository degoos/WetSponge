package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumMapDecorationType;
import com.degoos.wetsponge.enums.EnumMapScale;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.map.WSMapDecoration;
import com.degoos.wetsponge.map.WSMapView;
import com.degoos.wetsponge.packet.SpigotPacket;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.Validate;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import com.flowpowered.math.vector.Vector2i;

import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class SpigotSPacketMaps extends SpigotPacket implements WSSPacketMaps {

	private int mapId;
	private Vector2i origin, size;
	private WSMapView mapView;
	private boolean changed;

	public SpigotSPacketMaps(int mapId, Vector2i origin, Vector2i size, WSMapView mapView) throws IllegalAccessException, InstantiationException {
		super(NMSUtils.getNMSClass("PacketPlayOutMap").newInstance());
		Validate.notNull(origin, "Origin cannot be null!");
		Validate.notNull(size, "Size cannot be null!");
		Validate.notNull(mapView, "MapView cannot be null!");
		this.mapId = mapId;
		this.origin = origin;
		this.size = size;
		this.mapView = mapView;
		update();
	}

	public SpigotSPacketMaps(Object packet) {
		super(packet);
		refresh();
	}

	@Override
	public void update() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));

			fields[0].setInt(getHandler(), mapId);
			fields[1].setByte(getHandler(), mapView.getMapScale().getId());

			if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD)) {

				fields[2].setBoolean(getHandler(), mapView.isTrackingPositions());
				fields[3].set(getHandler(), toMapDecoration(mapView.getAllDecorations()));

				fields[4].setInt(getHandler(), origin.getX());
				fields[5].setInt(getHandler(), origin.getY());
				fields[6].setInt(getHandler(), size.getX());
				fields[7].setInt(getHandler(), size.getY());
				fields[8].set(getHandler(), mapView.getColors());

			} else {
				fields[7].set(getHandler(), mapView.getColors());
				fields[2].set(getHandler(), toMapDecoration(mapView.getAllDecorations()));

				fields[3].setInt(getHandler(), origin.getX());
				fields[4].setInt(getHandler(), origin.getY());
				fields[5].setInt(getHandler(), size.getX());
				fields[6].setInt(getHandler(), size.getY());
			}
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was updating the packet SpigotSPacketMaps!");
		}
	}

	@Override
	public void refresh() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));
			mapId = fields[0].getInt(getHandler());

			if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD)) {
				origin = new Vector2i(fields[4].getInt(getHandler()), fields[5].getInt(getHandler()));
				size = new Vector2i(fields[6].getInt(getHandler()), fields[7].getInt(getHandler()));

				byte[] colors = (byte[]) fields[8].get(getHandler());
				if (colors.length == 16384)
					mapView = new WSMapView(colors, toWSMapDecoration(fields[3].get(getHandler())), fields[2].getBoolean(getHandler()), EnumMapScale
							.getById(fields[1].getByte(getHandler())).orElse(EnumMapScale.CLOSEST));
			} else {
				origin = new Vector2i(fields[3].getInt(getHandler()), fields[4].getInt(getHandler()));
				size = new Vector2i(fields[5].getInt(getHandler()), fields[6].getInt(getHandler()));

				byte[] colors = (byte[]) fields[7].get(getHandler());
				if (colors.length == 16384)
					mapView = new WSMapView(colors, toWSMapDecoration(fields[2].get(getHandler())), false, EnumMapScale.getById(fields[1].getByte(getHandler()))
							.orElse(EnumMapScale.CLOSEST));
			}
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was refreshing the packet SpigotSPacketMaps!");
		}
	}

	private Object toMapDecoration(Collection<WSMapDecoration> decorations) {
		Class<?> clazz = NMSUtils.getNMSClass("MapIcon");
		Object array = Array.newInstance(clazz, decorations.size());
		try {
			if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD)) {
				Class<? extends Enum> type = (Class<? extends Enum>) NMSUtils.getNMSClass("MapIcon$Type");
				Constructor constructor = clazz.getConstructor(type, byte.class, byte.class, byte.class);
				int i = 0;
				for (WSMapDecoration decoration : decorations) {
					Array.set(array, i, constructor
							.newInstance(Enum.valueOf(type, decoration.getType().name()), (byte) decoration.getPosition().getX(), (byte) decoration.getPosition()
									.getY(), (byte) decoration.getRotation()));
					i++;
				}
			} else {
				Constructor constructor = clazz.getConstructor(byte.class, byte.class, byte.class, byte.class);
				int i = 0;
				for (WSMapDecoration decoration : decorations) {
					Array.set(array, i, constructor
							.newInstance(decoration.getType().getId(), (byte) decoration.getPosition().getX(), (byte) decoration.getPosition().getY(), (byte) decoration
									.getRotation()));
					i++;
				}
			}
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was updating the packet SpigotSPacketMaps!");
		}
		return array;
	}

	private Set<WSMapDecoration> toWSMapDecoration(Object object) {
		Set<WSMapDecoration> decorations = new HashSet<>();
		Field[] fields = NMSUtils.getNMSClass("MapIcon").getDeclaredFields();
		Arrays.stream(fields).forEach(field -> field.setAccessible(true));
		try {
			int length = Array.getLength(object);

			if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD)) {
				Class<? extends Enum> type = (Class<? extends Enum>) NMSUtils.getNMSClass("MapIcon$Type");
				for (int i = 0; i < length; i++) {
					Object decoration = Array.get(object, i);
					Object t = fields[0].get(decoration);
					decorations.add(new WSMapDecoration(new Vector2i(fields[1].getByte(decoration), fields[2].getByte(decoration)), EnumMapDecorationType
							.getById((byte) type.getMethod("a").invoke(t)).orElse(EnumMapDecorationType.PLAYER), fields[3].getByte(decoration)));
				}
			} else {
				for (int i = 0; i < length; i++) {
					Object decoration = Array.get(object, i);
					decorations.add(new WSMapDecoration(new Vector2i(fields[1].getByte(decoration), fields[2].getByte(decoration)), EnumMapDecorationType
							.getById(fields[0].getByte(decoration)).orElse(EnumMapDecorationType.PLAYER), fields[3].getByte(decoration)));
				}
			}
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was refreshing the packet SpigotSPacketMaps!");
		}
		return decorations;
	}

	@Override
	public boolean hasChanged() {
		return changed;
	}

	@Override
	public int getMapId() {
		return mapId;
	}

	@Override
	public void setMapId(int id) {
		this.mapId = id;
		changed = true;
	}

	@Override
	public Vector2i getOrigin() {
		return origin;
	}

	@Override
	public void setOrigin(Vector2i origin) {
		this.origin = origin;
		changed = true;
	}

	@Override
	public Vector2i setSize() {
		return size;
	}

	@Override
	public void setSize(Vector2i size) {
		this.size = size;
		changed = true;
	}

	@Override
	public WSMapView getMapView() {
		changed = true;
		return mapView;
	}

	@Override
	public void setMapView(WSMapView mapView) {
		this.mapView = mapView;
		changed = true;
	}
}

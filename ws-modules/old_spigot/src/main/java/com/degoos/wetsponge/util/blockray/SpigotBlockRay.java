package com.degoos.wetsponge.util.blockray;

import com.degoos.wetsponge.block.SpigotBlock;
import com.degoos.wetsponge.block.WSBlock;
import com.degoos.wetsponge.material.WSBlockTypes;
import com.degoos.wetsponge.material.block.SpigotBlockType;
import com.degoos.wetsponge.material.block.WSBlockType;
import com.degoos.wetsponge.util.NumericUtils;
import com.degoos.wetsponge.util.Validate;
import com.degoos.wetsponge.world.WSWorld;
import com.flowpowered.math.vector.Vector3d;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.util.BlockIterator;
import org.bukkit.util.Vector;

import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.function.Predicate;

public class SpigotBlockRay implements WSBlockRay {

	private BlockIterator iterator;
	private Predicate<WSBlockType> skipFilter;
	private WSBlock hit;
	private boolean ahead;

	public SpigotBlockRay(BlockIterator iterator, Predicate<WSBlockType> skipFilter) {
		this.iterator = iterator;
		this.skipFilter = skipFilter == null ? target -> false : skipFilter;
		this.hit = null;
		this.ahead = false;
	}

	@Override
	public boolean hasNext() {
		if (ahead) return true;
		else try {
			advance();
			ahead = true;
			return true;
		} catch (NoSuchElementException ex) {
			return false;
		}
	}

	@Override
	public WSBlock next() {
		if (ahead) ahead = false;
		else advance();
		return hit;
	}

	@Override
	public Optional<WSBlock> end() {
		WSBlock last = null;
		while (hasNext()) last = next();
		return Optional.ofNullable(last);
	}

	private void advance() {
		while (!advanceOneBlock()) ;
	}

	private boolean advanceOneBlock() {
		Block next = iterator.next();
		WSBlockType type = WSBlockTypes.getById(next.getTypeId()).map(target -> ((SpigotBlockType) target).readMaterialData(next.getType().getNewData(next.getData()))).orElse(null);
		if (type == null || skipFilter.test(type)) return false;
		else {
			hit = new SpigotBlock(next);
			return true;
		}
	}

	public static class Builder implements WSBlockRay.Builder {

		private WSWorld world;
		private Vector3d origin;
		private Vector3d direction;
		private double yOffset;
		private double maxDistance;
		private Predicate<WSBlockType> skipFilter;

		public Builder(WSWorld world) {
			Validate.notNull(world, "World cannot be null!");
			this.world = world;
			this.skipFilter = target -> true;
		}

		@Override
		public WSBlockRay.Builder origin(Vector3d origin) {
			this.origin = origin;
			return this;
		}

		@Override
		public WSBlockRay.Builder direction(Vector3d direction) {
			this.direction = direction;
			return this;
		}

		@Override
		public WSBlockRay.Builder yOffset(double yOffset) {
			this.yOffset = yOffset;
			return this;
		}

		@Override
		public WSBlockRay.Builder maxDistance(double maxDistance) {
			this.maxDistance = maxDistance;
			return this;
		}

		@Override
		public WSBlockRay.Builder skipFilter(Predicate<WSBlockType> skipFilter) {
			this.skipFilter = skipFilter;
			return this;
		}

		@Override
		public WSBlockRay build() {
			Validate.notNull(origin, "Origin cannot be null!");
			Validate.notNull(direction, "Direction cannot be null!");
			Validate.isTrue(maxDistance > 0, "MaxDistance must be bigger than 0!");
			SpigotBlockRay ray = new SpigotBlockRay(new BlockIterator((World) world.getHandled(), toVector(origin),
					toVector(direction), yOffset, NumericUtils.floor(maxDistance)), skipFilter);
			ray.advanceOneBlock();
			return ray;
		}

		private Vector toVector(Vector3d vector3d) {
			return new Vector(vector3d.getX(), vector3d.getY(), vector3d.getZ());
		}
	}
}

package com.degoos.wetsponge.merchant;

import com.degoos.wetsponge.item.SpigotItemStack;
import com.degoos.wetsponge.item.WSItemStack;
import com.degoos.wetsponge.util.Validate;
import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;
import org.bukkit.Material;
import org.bukkit.inventory.MerchantRecipe;

public class SpigotTrade implements WSTrade {

	private MerchantRecipe trade;

	public SpigotTrade(MerchantRecipe trade) {
		this.trade = trade;
	}

	@Override
	public int getUses() {
		return trade.getUses();
	}

	@Override
	public int getMaxUses() {
		return trade.getMaxUses();
	}

	@Override
	public WSItemStack getResult() {
		return new SpigotItemStack(trade.getResult()).clone();
	}

	@Override
	public WSItemStack getFirstItem() {
		return new SpigotItemStack(trade.getIngredients().get(0)).clone();
	}

	@Override
	public Optional<WSItemStack> getSecondItem() {
		return trade.getIngredients().size() == 2 && trade.getIngredients().get(1).getType() != Material.AIR ? Optional
			.of(new SpigotItemStack(trade.getIngredients().get(1)).clone()) : Optional.empty();
	}

	@Override
	public boolean doesGrantExperience() {
		return trade.hasExperienceReward();
	}

	@Override
	public MerchantRecipe getHandled() {
		return trade;
	}

	public static class Builder implements WSTrade.Builder {

		private int uses, maxUses;
		private WSItemStack result, firstItem, secondItem;
		private boolean grantExperience;


		@Override
		public WSTrade.Builder uses(int uses) {
			this.uses = uses;
			return this;
		}

		@Override
		public WSTrade.Builder maxUses(int maxUses) {
			this.maxUses = maxUses;
			return this;
		}

		@Override
		public WSTrade.Builder result(WSItemStack result) {
			Validate.notNull(result, "Result cannot be null!");
			this.result = result;
			return this;
		}

		@Override
		public WSTrade.Builder firstItem(WSItemStack firstItem) {
			Validate.notNull(firstItem, "First item cannot be null!");
			this.firstItem = firstItem;
			return this;
		}

		@Override
		public WSTrade.Builder secondItem(WSItemStack secondItem) {
			this.secondItem = secondItem;
			return this;
		}

		@Override
		public WSTrade.Builder canGrantExperience(boolean grantExperience) {
			this.grantExperience = grantExperience;
			return this;
		}

		@Override
		public WSTrade build() {
			Validate.notNull(result, "Result cannot be null!");
			Validate.notNull(firstItem, "First item cannot be null!");
			MerchantRecipe merchantRecipe = new MerchantRecipe(((SpigotItemStack) result).getHandled(), maxUses);
			merchantRecipe.setUses(uses);
			merchantRecipe.setExperienceReward(grantExperience);
			merchantRecipe.setIngredients(secondItem == null ? Collections.singletonList(((SpigotItemStack) firstItem).getHandled())
			                                                 : Arrays.asList(((SpigotItemStack) firstItem).getHandled(), ((SpigotItemStack) secondItem).getHandled()));
			return new SpigotTrade(merchantRecipe);
		}
	}
}

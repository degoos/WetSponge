package com.degoos.wetsponge.util.reflection;


import com.degoos.wetsponge.util.InternalLogger;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SpigotTextUtils {

	private static final Pattern url = Pattern.compile("^(?:(https?)://)?([-\\w_\\.]{2,}\\.[a-z]{2,4})(/\\S*)?$");

	private static Class<?> clazz = NMSUtils.getNMSClass("IChatBaseComponent");

	public static String toJSON(Object iChatBaseComponent) {
		try {
			return (String) NMSUtils.getNMSClass("IChatBaseComponent").getDeclaredClasses()[0].getMethod("a", new Class[]{clazz})
					.invoke(null, iChatBaseComponent);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}


	public static Object toIChatBaseComponentFromJSON(String json) {
		try {
			return NMSUtils.getNMSClass("IChatBaseComponent").getDeclaredClasses()[0].getMethod("a", new Class[]{String.class})
					.invoke(null, json);
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was creating a IChatBaseComponent!");
			return null;
		}
	}

	public static Object toIChatBaseComponentFromFormattedText(String text) {
		try {
			return NMSUtils.getNMSClass("IChatBaseComponent").getDeclaredClasses()[0].getMethod("a", new Class[]{String.class})
					.invoke(null, "{\"text\":\"" + text + "\"}");
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was creating a IChatBaseComponent!");
			return null;
		}
	}

	public static BaseComponent[] fromLegacyText(String message) {
		ArrayList<BaseComponent> components = new ArrayList();
		StringBuilder builder = new StringBuilder();
		TextComponent component = new TextComponent();
		Matcher matcher = url.matcher(message);

		for(int i = 0; i < message.length(); ++i) {
			char c = message.charAt(i);
			TextComponent old;
			if (c == 167) {
				++i;
				if (i >= message.length()) {
					break;
				}

				c = message.charAt(i);
				if (c >= 'A' && c <= 'Z') {
					c = (char)(c + 32);
				}

				ChatColor format = ChatColor.getByChar(c);
				if (format != null) {
					if (builder.length() > 0) {
						old = component;
						component = new TextComponent(component);
						old.setText(builder.toString());
						builder = new StringBuilder();
						components.add(old);
					}

					switch(format) {
						case BOLD:
							component.setBold(true);
							break;
						case ITALIC:
							component.setItalic(true);
							break;
						case UNDERLINE:
							component.setUnderlined(true);
							break;
						case STRIKETHROUGH:
							component.setStrikethrough(true);
							break;
						case MAGIC:
							component.setObfuscated(true);
							break;
						case RESET:
							format = ChatColor.WHITE;
						default:
							component = new TextComponent();
							component.setColor(format);
					}
				}
			} else {
				int pos = message.indexOf(32, i);
				if (pos == -1) {
					pos = message.length();
				}

				if (matcher.region(i, pos).find()) {
					if (builder.length() > 0) {
						old = component;
						component = new TextComponent(component);
						old.setText(builder.toString());
						builder = new StringBuilder();
						components.add(old);
					}

					old = component;
					component = new TextComponent(component);
					String urlString = message.substring(i, pos);
					component.setText(urlString);
					component.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, urlString.startsWith("http") ? urlString : "http://" + urlString));
					components.add(component);
					i += pos - i - 1;
					component = old;
				} else {
					builder.append(c);
				}
			}
		}

		if (builder.length() > 0) {
			component.setText(builder.toString());
			components.add(component);
		}

		if (components.isEmpty()) {
			components.add(new TextComponent(""));
		}

		return (BaseComponent[])components.toArray(new BaseComponent[components.size()]);
	}

}

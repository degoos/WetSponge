package com.degoos.wetsponge.material.block;

import org.bukkit.material.MaterialData;

import java.util.Objects;

public class SpigotBlockTypeWaterlogged extends SpigotBlockType implements WSBlockTypeWaterlogged {

	private boolean waterLogged;

	public SpigotBlockTypeWaterlogged(int numericalId, String oldStringId, String newStringId, int maxStackSize, boolean waterLogged) {
		super(numericalId, oldStringId, newStringId, maxStackSize);
		this.waterLogged = waterLogged;
	}

	@Override
	public boolean isWaterlogged() {
		return waterLogged;
	}

	@Override
	public void setWaterlogged(boolean waterlogged) {
		this.waterLogged = waterlogged;
	}

	@Override
	public SpigotBlockTypeWaterlogged clone() {
		return new SpigotBlockTypeWaterlogged(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), waterLogged);
	}

	@Override
	public MaterialData toMaterialData() {
		return super.toMaterialData();
	}

	@Override
	public SpigotBlockTypeWaterlogged readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeWaterlogged that = (SpigotBlockTypeWaterlogged) o;
		return waterLogged == that.waterLogged;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), waterLogged);
	}
}

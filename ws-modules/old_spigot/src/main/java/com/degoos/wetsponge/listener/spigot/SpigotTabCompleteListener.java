package com.degoos.wetsponge.listener.spigot;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.command.SpigotCommandSource;
import com.degoos.wetsponge.command.WSCommandManager;
import com.degoos.wetsponge.event.command.WSTabCompleteChatEvent;
import com.degoos.wetsponge.event.command.WSTabCompleteCommandEvent;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.SpigotEventUtils;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.server.TabCompleteEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SpigotTabCompleteListener implements Listener {

	@EventHandler(priority = EventPriority.LOWEST)
	public void onTabComplete(TabCompleteEvent event) {
		if (!SpigotEventUtils.shouldBeExecuted()) return;
		try {
			String message = event.getBuffer();
			if ((message.startsWith("/") && event.getSender() instanceof Player) || !(event.getSender() instanceof Player)) {
				String[] sl = message.split(" ");
				String command = sl[0];

				boolean hasLine = command.startsWith("/");
				if (hasLine) command = command.replaceFirst("/", "");

				String[] arguments;
				if (event.getBuffer().split(" ").length != 1 || event.getBuffer().endsWith(" ")) {
					String rawArguments = event.getBuffer().replace((hasLine ? "/" : "") + command + " ", "").replace(" ", "<WETSPONGEREMOVE>" + " ");
					arguments = rawArguments.split("<WETSPONGEREMOVE>");
					for (int i = 0; i < arguments.length; i++)
						arguments[i] = arguments[i].replace(" ", "");
				} else arguments = new String[]{""};
				WSTabCompleteCommandEvent wetSpongeEvent = new WSTabCompleteCommandEvent(new SpigotCommandSource(event.getSender()), command, arguments, event
						.getCompletions());
				WetSponge.getEventManager().callEvent(wetSpongeEvent);
				if (wetSpongeEvent.isCancelled()) event.setCancelled(true);
				else {
					try {
						if (event.getBuffer().split(" ").length == 1 && !event.getBuffer().endsWith(" ")) {
							List<String> list = new ArrayList<>(event.getCompletions());
							list.addAll(WSCommandManager.getInstance().getCommandsAndAliases().stream()
									.filter(arg -> arg.toLowerCase().startsWith(wetSpongeEvent.getCommand().toLowerCase())).map(arg -> (hasLine ? "/" : "") + arg)
									.collect(Collectors.toList()));
							event.setCompletions(list);
						} else {
							List<String> list = new ArrayList<>(event.getCompletions());

							WSCommandManager.getInstance().getCommand(wetSpongeEvent.getCommand()).ifPresent(wsCommand -> list
									.addAll(wsCommand.sendTab(wetSpongeEvent.getSource(), wetSpongeEvent.getCommand(), wetSpongeEvent.getArguments())));
							event.setCompletions(list);
						}
					} catch (Exception ignore) {
						InternalLogger.sendWarning("Cannot add completions for the sender " + event.getSender().getName());
					}
				}

			} else {
				WSTabCompleteChatEvent wetSpongeEvent = new WSTabCompleteChatEvent(new SpigotCommandSource(event.getSender()), message, event.getCompletions());
				WetSponge.getEventManager().callEvent(wetSpongeEvent);
				if (wetSpongeEvent.isCancelled()) event.setCancelled(true);
			}
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Spigot-TabCompleteEvent!");
		}
	}


}

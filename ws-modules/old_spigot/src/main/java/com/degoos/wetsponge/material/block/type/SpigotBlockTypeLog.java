package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumAxis;
import com.degoos.wetsponge.enums.block.EnumWoodType;
import com.degoos.wetsponge.material.block.SpigotBlockTypeOrientable;
import org.bukkit.material.MaterialData;

import java.util.Objects;
import java.util.Set;

public class SpigotBlockTypeLog extends SpigotBlockTypeOrientable implements WSBlockTypeLog {

	private EnumWoodType woodType;
	private boolean stripped, allFacesCovered;

	public SpigotBlockTypeLog(EnumAxis axis, Set<EnumAxis> axes, EnumWoodType woodType, boolean stripped, boolean allFacesCovered) {
		super(17, "minecraft:log", "minecraft:log", 64, axis, axes);
		this.woodType = woodType;
		this.stripped = stripped;
		this.allFacesCovered = allFacesCovered;
	}

	@Override
	public int getNumericalId() {
		return getWoodType() == EnumWoodType.ACACIA || getWoodType() == EnumWoodType.DARK_OAK ? 162 : 17;
	}

	@Override
	public String getOldStringId() {
		return getWoodType() == EnumWoodType.ACACIA || getWoodType() == EnumWoodType.DARK_OAK ? "minecraft:log2" : "minecraft:log";
	}

	@Override
	public String getNewStringId() {
		return "minecraft:" + (stripped ? "stripped_" : "") + getWoodType().name().toLowerCase() + (allFacesCovered ? "_wood" : "_log");
	}

	@Override
	public EnumWoodType getWoodType() {
		return woodType;
	}

	@Override
	public void setWoodType(EnumWoodType woodType) {
		this.woodType = woodType;
	}

	@Override
	public boolean isStripped() {
		return stripped;
	}

	@Override
	public void setStripped(boolean stripped) {
		this.stripped = stripped;
	}

	@Override
	public boolean hasAllFacesCovered() {
		return allFacesCovered;
	}

	@Override
	public void setAllFacesCovered(boolean allFacesCovered) {
		this.allFacesCovered = allFacesCovered;
	}

	@Override
	public SpigotBlockTypeLog clone() {
		return new SpigotBlockTypeLog(getAxis(), getAxes(), woodType, stripped, allFacesCovered);
	}

	@Override
	public MaterialData toMaterialData() {
		MaterialData data = super.toMaterialData();
		int sum;
		if (allFacesCovered) sum = 12;
		else {
			switch (getAxis()) {
				case X:
					sum = 4;
					break;
				case Z:
					sum = 8;
					break;
				default:
				case Y:
					sum = 0;
					break;
			}
		}
		data.setData((byte) (sum + (woodType.getValue() % 4)));
		return data;
	}

	@Override
	public SpigotBlockTypeLog readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		int data = materialData.getData();
		switch (data / 4) {
			case 1:
				setAxis(EnumAxis.X);
				allFacesCovered = false;
				break;
			case 2:
				setAxis(EnumAxis.Z);
				allFacesCovered = false;
				break;
			case 3:
				setAxis(EnumAxis.Y);
				allFacesCovered = true;
				break;
			default:
			case 0:
				setAxis(EnumAxis.Y);
				allFacesCovered = false;
				break;
		}
		if (getNumericalId() == 17)
			woodType = EnumWoodType.getByValue(data % 4).orElse(EnumWoodType.OAK);
		else woodType = EnumWoodType.getByValue((data % 4) + 4).orElse(EnumWoodType.OAK);
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeLog that = (SpigotBlockTypeLog) o;
		return stripped == that.stripped &&
				allFacesCovered == that.allFacesCovered &&
				woodType == that.woodType;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), woodType, stripped, allFacesCovered);
	}
}

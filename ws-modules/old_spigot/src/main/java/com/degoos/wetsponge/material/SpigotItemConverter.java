package com.degoos.wetsponge.material;

import com.degoos.wetsponge.block.tileentity.extra.WSBannerPattern;
import com.degoos.wetsponge.color.WSColor;
import com.degoos.wetsponge.enums.EnumDyeColor;
import com.degoos.wetsponge.enums.EnumEntityType;
import com.degoos.wetsponge.enums.block.EnumBlockTypeSkullType;
import com.degoos.wetsponge.enums.item.*;
import com.degoos.wetsponge.firework.WSFireworkEffect;
import com.degoos.wetsponge.material.item.SpigotItemType;
import com.degoos.wetsponge.material.item.WSItemType;
import com.degoos.wetsponge.material.item.type.*;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.user.WSGameProfile;

import java.util.List;

public class SpigotItemConverter {

	@SuppressWarnings("unchecked")
	public static WSItemType createWSItemType(int numericalId, String oldStringId, String newStringId, int maxStackSize,
											  Class<? extends WSItemType> materialClass, Object[] extra) {

		if (materialClass.equals(WSItemType.class))
			return new SpigotItemType(numericalId, oldStringId, newStringId, maxStackSize);

		if (materialClass.equals(WSItemTypeBanner.class))
			return new SpigotItemTypeBanner((EnumDyeColor) extra[0], (List<WSBannerPattern>) extra[1]);
		if (materialClass.equals(WSItemTypeCoal.class))
			return new SpigotItemTypeCoal((EnumItemTypeCoalType) extra[0]);
		if (materialClass.equals(WSItemTypeCookedFish.class))
			return new SpigotItemTypeCookedFish((EnumItemTypeCookedFishType) extra[0]);
		if (materialClass.equals(WSItemTypeDamageable.class))
			return new SpigotItemTypeDamageable(numericalId, oldStringId, newStringId, (int) extra[0], (int) extra[1]);
		if (materialClass.equals(WSItemTypeDye.class))
			return new SpigotItemTypeDye((EnumDyeColor) extra[0]);
		if (materialClass.equals(WSItemTypeDyeColored.class))
			return new SpigotItemTypeDyeColored(numericalId, oldStringId, newStringId, maxStackSize, (EnumDyeColor) extra[0]);
		if (materialClass.equals(WSItemTypeFirework.class))
			return new SpigotItemTypeFirework((int) extra[0], (List<WSFireworkEffect>) extra[1]);
		if (materialClass.equals(WSItemTypeFireworkCharge.class))
			return new SpigotItemTypeFireworkCharge((WSFireworkEffect) extra[0]);
		if (materialClass.equals(WSItemTypeFish.class))
			return new SpigotItemTypeFish((EnumItemTypeFishType) extra[0]);
		if (materialClass.equals(WSItemTypeFishBucket.class))
			return new SpigotItemTypeFishBucket((EnumItemTypeFishType) extra[0]);
		if (materialClass.equals(WSItemTypeGoldenApple.class))
			return new SpigotItemTypeGoldenApple((EnumItemTypeGoldenAppleType) extra[0]);
		if (materialClass.equals(WSItemTypeLeatherArmor.class))
			return new SpigotItemTypeLeatherArmor(numericalId, oldStringId, newStringId, (int) extra[0], (int) extra[1], (WSColor) extra[2]);
		if (materialClass.equals(WSItemTypeMap.class))
			return new SpigotItemTypeMap((int) extra[0], (WSColor) extra[1]);
		if (materialClass.equals(WSItemTypeSkull.class))
			return new SpigotItemTypeSkull((WSGameProfile) extra[0], (EnumBlockTypeSkullType) extra[1]);
		if (materialClass.equals(WSItemTypeSpawnEgg.class))
			return new SpigotItemTypeSpawnEgg((EnumEntityType) extra[0]);
		if (materialClass.equals(WSItemTypeWrittenBook.class))
			return new SpigotItemTypeWrittenBook(numericalId, oldStringId, newStringId, (WSText) extra[0], (WSText) extra[1],
					(List<WSText>) extra[2], (EnumItemTypeBookGeneration) extra[3]);

		return new SpigotItemType(numericalId, oldStringId, newStringId, maxStackSize);
	}

}

package com.degoos.wetsponge.nbt;

import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import java.lang.reflect.Constructor;

public class SpigotNBTTagEnd extends SpigotNBTBase implements WSNBTTagEnd {

	private static final Class<?> clazz = NMSUtils.getNMSClass("NBTTagEnd");

	public SpigotNBTTagEnd(Object nbtTagEnd) {
		super(nbtTagEnd);
	}

	public SpigotNBTTagEnd() {
		this(newInstance());
	}

	@Override
	public WSNBTTagEnd copy() {
		return new SpigotNBTTagEnd(newInstance());
	}

	private static Object newInstance() {
		try {
			Constructor constructor = clazz.getDeclaredConstructor();
			constructor.setAccessible(true);
			return constructor.newInstance();
		} catch (Exception e) {
			InternalLogger.printException(e, "An error has occurred while WetSponge was creating a new instance of a NBTTagEnd!");
			return null;
		}
	}
}

package com.degoos.wetsponge.util.reflection;

import com.degoos.wetsponge.resource.spigot.SpigotMerchantUtils;
import org.bukkit.inventory.ItemStack;

public class SpigotItemStackUtils {

	public static String getDisplayName(ItemStack itemStack) {
		try {
			Object is = SpigotMerchantUtils.asNMSCopy(itemStack);
			Object nbt = ReflectionUtils.invokeMethod(is, "d", "display");
			if (nbt != null) {
				if ((boolean) ReflectionUtils.invokeMethod(nbt, "hasKeyOfType", "Name", 8)) return (String) ReflectionUtils.invokeMethod(nbt, "getString", "Name");
				if ((boolean) ReflectionUtils.invokeMethod(nbt, "hasKeyOfType", "LocName", 8)) return (String) ReflectionUtils.invokeMethod(nbt, "getString", "LocName");
			}

			return ReflectionUtils.invokeMethod(ReflectionUtils.invokeMethod(is, "getItem"), "getName") + ".name";
		} catch (Exception ex) {
			return "";
		}
	}

}

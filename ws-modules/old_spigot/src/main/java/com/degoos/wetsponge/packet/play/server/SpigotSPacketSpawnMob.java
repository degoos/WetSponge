package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.entity.living.SpigotLivingEntity;
import com.degoos.wetsponge.entity.living.WSLivingEntity;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.packet.SpigotPacket;
import com.degoos.wetsponge.util.Validate;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;
import com.degoos.wetsponge.util.reflection.SpigotHandledUtils;
import com.flowpowered.math.vector.Vector2d;
import com.flowpowered.math.vector.Vector3d;
import org.bukkit.entity.LivingEntity;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

public class SpigotSPacketSpawnMob extends SpigotPacket implements WSSPacketSpawnMob {

	private Optional<WSLivingEntity> entity;
	private int entityId;
	private UUID uniqueId;
	private int type;
	private Vector3d position, velocity;
	private Vector2d rotation;
	private double headPitch;
	private boolean changed;
	private WSLivingEntity disguise;

	public SpigotSPacketSpawnMob(WSLivingEntity entity) throws InstantiationException, IllegalAccessException {
		this(entity, entity.getLocation().toVector3d(), entity.getVelocity(), entity.getRotation().toVector2(), entity.getHeadRotation().getY());
	}

	public SpigotSPacketSpawnMob(WSLivingEntity entity, Vector3d position, Vector3d velocity, Vector2d rotation, double headPitch)
			throws IllegalAccessException, InstantiationException {
		super(NMSUtils.getNMSClass("PacketPlayOutSpawnEntityLiving").newInstance());
		updateEntity(entity);
		this.position = position;
		this.velocity = velocity;
		this.rotation = rotation;
		this.headPitch = headPitch;
		this.disguise = null;
		update();
	}

	public SpigotSPacketSpawnMob(Object packet) {
		super(packet);
		this.entity = Optional.empty();
		this.disguise = null;
		refresh();
	}

	public Optional<WSLivingEntity> getEntity() {
		return entity;
	}

	public void setEntity(WSLivingEntity entity) {
		Validate.notNull(entity, "Entity cannot be null!");
		updateEntity(entity);
	}

	@Override
	public Optional<WSLivingEntity> getDisguise() {
		return Optional.ofNullable(disguise);
	}

	public void setDisguise(WSLivingEntity disguise) {
		Validate.notNull(disguise, "Disguise cannot be null!");
		changed = true;
		this.disguise = disguise;
	}

	public int getEntityId() {
		return entityId;
	}

	public void setEntityId(int entityId) {
		changed = true;
		this.entityId = entityId;
	}

	public UUID getUniqueId() {
		return uniqueId;
	}

	public void setUniqueId(UUID uniqueId) {
		changed = true;
		this.uniqueId = uniqueId;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		changed = true;
		this.type = type;
	}

	public Vector3d getPosition() {
		return position;
	}

	public void setPosition(Vector3d position) {
		changed = true;
		this.position = position;
	}

	public Vector3d getVelocity() {
		return velocity;
	}

	public void setVelocity(Vector3d velocity) {
		changed = true;
		this.velocity = velocity;
	}

	public Vector2d getRotation() {
		return rotation;
	}

	public void setRotation(Vector2d rotation) {
		changed = true;
		this.rotation = rotation;
	}

	public double getHeadPitch() {
		return headPitch;
	}

	public void setHeadPitch(double headPitch) {
		changed = true;
		this.headPitch = headPitch;
	}

	private void updateEntity(WSLivingEntity entity) {
		Class<?> entityTypesClass = NMSUtils.getNMSClass("EntityTypes");
		LivingEntity spigotEntity = ((SpigotLivingEntity) entity).getHandled();
		Object livingBase = SpigotHandledUtils.getEntityHandle(spigotEntity);
		this.entity = Optional.ofNullable(entity);
		this.entityId = entity.getEntityId();
		this.uniqueId = spigotEntity.getUniqueId();

		if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD)) try {
			Object registry = entityTypesClass.getField("b").get(null);
			this.type = (int) ReflectionUtils.getMethod(registry.getClass(), "a", NMSUtils.getNMSClass("Entity").getClass())
					.invoke(registry, livingBase.getClass());
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
		else {
			try {
				Field field = entityTypesClass.getDeclaredField("f");
				field.setAccessible(true);
				type = (int) ReflectionUtils.getMethodByName(Map.class, "get").invoke(field.get(null), livingBase.getClass());
			} catch (Throwable ex) {
				ex.printStackTrace();
			}
		}
	}

	@Override
	public void update() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));

			if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD)) {
				fields[0].setInt(getHandler(), entityId);
				fields[1].set(getHandler(), uniqueId);
				fields[2].setInt(getHandler(), disguise == null ? type : disguise.getEntityType().getTypeId());

				fields[3].setDouble(getHandler(), position.getX());
				fields[4].setDouble(getHandler(), position.getY());
				fields[5].setDouble(getHandler(), position.getZ());

				fields[6].setInt(getHandler(), (int) (velocity.getX() * 8000.0D));
				fields[7].setInt(getHandler(), (int) (velocity.getY() * 8000.0D));
				fields[8].setInt(getHandler(), (int) (velocity.getZ() * 8000.0D));

				fields[9].setByte(getHandler(), (byte) ((int) (rotation.getY() * 256.0F / 360.0F)));
				fields[10].setByte(getHandler(), (byte) ((int) (rotation.getX() * 256.0F / 360.0F)));
				fields[11].setByte(getHandler(), (byte) ((int) (headPitch * 256.0F / 360.0F)));

				if (disguise != null) {
					LivingEntity spigotEntity = ((SpigotLivingEntity) disguise).getHandled();
					Object livingBase = SpigotHandledUtils.getEntityHandle(spigotEntity);
					fields[12].set(getHandler(), ReflectionUtils.getMethod(livingBase.getClass(), "getDataWatcher").invoke(livingBase));
				} else if (entity.isPresent()) {
					LivingEntity spigotEntity = ((SpigotLivingEntity) entity.get()).getHandled();
					Object livingBase = SpigotHandledUtils.getEntityHandle(spigotEntity);
					fields[12].set(getHandler(), ReflectionUtils.getMethod(livingBase.getClass(), "getDataWatcher").invoke(livingBase));
				}

			} else {
				fields[0].setInt(getHandler(), entityId);
				fields[1].setInt(getHandler(), type);
				fields[2].setInt(getHandler(), floor(position.getX() * 32.0D));
				fields[3].setInt(getHandler(), floor(position.getY() * 32.0D));
				fields[4].setInt(getHandler(), floor(position.getZ() * 32.0D));

				double max = 3.9D;
				double x = velocity.getX();
				double y = velocity.getY();
				double z = velocity.getZ();
				if (x < -max) {
					x = -max;
				}
				if (y < -max) {
					y = -max;
				}
				if (z < -max) {
					z = -max;
				}
				if (x > max) {
					x = max;
				}
				if (y > max) {
					y = max;
				}
				if (z > max) {
					z = max;
				}
				fields[5].setInt(getHandler(), (int) (x * 8000.0D));
				fields[6].setInt(getHandler(), (int) (y * 8000.0D));
				fields[7].setInt(getHandler(), (int) (z * 8000.0D));

				fields[8].setByte(getHandler(), (byte) ((int) (rotation.getX() * 256.0F / 360.0F)));
				fields[9].setByte(getHandler(), (byte) ((int) (rotation.getY() * 256.0F / 360.0F)));
				fields[10].setByte(getHandler(), (byte) ((int) (headPitch * 256.0F / 360.0F)));

				if (disguise != null) {
					LivingEntity spigotEntity = ((SpigotLivingEntity) disguise).getHandled();
					Object livingBase = SpigotHandledUtils.getEntityHandle(spigotEntity);
					fields[11].set(getHandler(), ReflectionUtils.getMethod(livingBase.getClass(), "getDataWatcher").invoke(livingBase));
				} else if (entity.isPresent()) {
					LivingEntity spigotEntity = ((SpigotLivingEntity) entity.get()).getHandled();
					Object livingBase = SpigotHandledUtils.getEntityHandle(spigotEntity);
					fields[11].set(getHandler(), ReflectionUtils.getMethod(livingBase.getClass(), "getDataWatcher").invoke(livingBase));
				}

			}
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void refresh() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));
			if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD)) {
				entityId = fields[0].getInt(getHandler());
				uniqueId = (UUID) fields[1].get(getHandler());
				type = fields[2].getInt(getHandler());
				position = new Vector3d(fields[3].getDouble(getHandler()), fields[4].getDouble(getHandler()), fields[5].getDouble(getHandler()));
				velocity = new Vector3d(fields[6].getInt(getHandler()) / 8000.0D, fields[7].getInt(getHandler()) / 8000.0D, fields[8].getInt(getHandler()) / 8000.0D);
				rotation = new Vector2d(fields[10].getByte(getHandler()) * 360.0D / 256.0D, fields[9].getByte(getHandler()) * 360.0D / 256.0D);
				headPitch = fields[11].getByte(getHandler()) * 360.0D / 256.0D;
			} else {
				entityId = fields[0].getInt(getHandler());
				uniqueId = null;
				type = fields[1].getInt(getHandler());
				position = new Vector3d(
						(double) fields[2].getInt(getHandler()) / 32D, (double) fields[3].getInt(getHandler()) / 32D, (double) fields[4].getInt(getHandler()) / 32D);
				velocity = new Vector3d(fields[5].getInt(getHandler()) / 8000.0D, fields[6].getInt(getHandler()) / 8000.0D, fields[7].getInt(getHandler()) / 8000.0D);
				rotation = new Vector2d(fields[9].getByte(getHandler()) * 360.0D / 256.0D, fields[8].getByte(getHandler()) * 360.0D / 256.0D);
				headPitch = fields[10].getByte(getHandler()) * 360.0D / 256.0D;
			}
			disguise = null;
			entity = Optional.empty();
		} catch (Throwable ex) {
			ex.printStackTrace();
		}

	}

	@Override
	public boolean hasChanged() {
		return changed;
	}

	private int floor(double var0) {
		int var2 = (int) var0;
		return var0 < (double) var2 ? var2 - 1 : var2;
	}

}

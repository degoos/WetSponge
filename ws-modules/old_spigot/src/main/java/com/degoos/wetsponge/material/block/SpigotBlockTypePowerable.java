package com.degoos.wetsponge.material.block;

import org.bukkit.material.MaterialData;

import java.util.Objects;

public class SpigotBlockTypePowerable extends SpigotBlockType implements WSBlockTypePowerable {

	private boolean powered;

	public SpigotBlockTypePowerable(int numericalId, String oldStringId, String newStringId, int maxStackSize, boolean powered) {
		super(numericalId, oldStringId, newStringId, maxStackSize);
		this.powered = powered;
	}

	@Override
	public boolean isPowered() {
		return powered;
	}

	@Override
	public void setPowered(boolean powered) {
		this.powered = powered;
	}

	@Override
	public SpigotBlockTypePowerable clone() {
		return new SpigotBlockTypePowerable(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), powered);
	}

	@Override
	public MaterialData toMaterialData() {
		MaterialData data = super.toMaterialData();
		data.setData(powered ? (byte) 1 : 0);
		return data;
	}

	@Override
	public SpigotBlockTypePowerable readMaterialData(MaterialData materialData) {
		powered = materialData.getData() > 0;
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypePowerable that = (SpigotBlockTypePowerable) o;
		return powered == that.powered;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), powered);
	}
}

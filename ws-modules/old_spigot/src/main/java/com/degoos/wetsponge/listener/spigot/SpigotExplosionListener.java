package com.degoos.wetsponge.listener.spigot;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.entity.explosive.WSExplosive;
import com.degoos.wetsponge.event.world.WSExplosionEvent.Detonate;
import com.degoos.wetsponge.event.world.WSExplosionEvent.Pre;
import com.degoos.wetsponge.parser.entity.SpigotEntityParser;
import com.degoos.wetsponge.util.SpigotEventUtils;
import com.degoos.wetsponge.world.SpigotExplosion;
import com.degoos.wetsponge.world.SpigotLocation;
import com.degoos.wetsponge.world.WSExplosion;
import com.degoos.wetsponge.world.WSLocation;
import com.degoos.wetsponge.world.WSWorld;
import java.util.stream.Collectors;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityExplodeEvent;

public class SpigotExplosionListener implements Listener {

	@EventHandler(priority = EventPriority.LOWEST)
	public void onExplosion(EntityExplodeEvent event) {
		if(!SpigotEventUtils.shouldBeExecuted()) return;
		WSLocation location = new SpigotLocation(event.getLocation());
		WSWorld world = location.getWorld();
		WSEntity wsEntity = event.getEntity() == null ? null : SpigotEntityParser.getWSEntity(event.getEntity());
		WSExplosive explosive = wsEntity instanceof WSExplosive ? (WSExplosive) wsEntity : null;
		Pre pre = new Pre(world, new SpigotExplosion(explosive, location, event.getYield(), false, true, event.blockList().isEmpty(), true));
		WetSponge.getEventManager().callEvent(pre);

		WSExplosion explosion = pre.getExplosion();

		event.setYield(explosion.getRadius());
		if (!explosion.shouldBreakBlocks()) event.blockList().clear();

		if (pre.isCancelled()) {
			event.setCancelled(true);
			return;
		}

		Detonate detonate = new Detonate(world, explosion, event.blockList().stream().map(b -> new SpigotLocation(b.getLocation())).collect(Collectors.toList()));
		WetSponge.getEventManager().callEvent(detonate);
		event.blockList().clear();
		event.blockList().addAll(detonate.getAffectedLocations().stream().map(l -> ((SpigotLocation) l).getLocation().getBlock()).collect(Collectors.toList()));
	}

}

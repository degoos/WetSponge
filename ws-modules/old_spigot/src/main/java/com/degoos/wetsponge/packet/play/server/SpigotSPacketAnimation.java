package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.packet.SpigotPacket;
import com.degoos.wetsponge.util.reflection.NMSUtils;

import java.lang.reflect.Field;

public class SpigotSPacketAnimation extends SpigotPacket implements WSSPacketAnimation {

    private int entityId;
    private int animationType;
    private boolean changed;

    public SpigotSPacketAnimation(WSEntity entity, int animationType) throws IllegalAccessException, InstantiationException {
        super(NMSUtils.getNMSClass("PacketPlayOutAnimation").newInstance());
        this.entityId = entity.getEntityId();
        this.animationType = animationType;
        update();
    }

    public SpigotSPacketAnimation(int entity, int animationType) throws IllegalAccessException, InstantiationException {
        super(NMSUtils.getNMSClass("PacketPlayOutAnimation").newInstance());
        this.entityId = entity;
        this.animationType = animationType;
        update();
    }

    public SpigotSPacketAnimation(Object packet) {
        super(packet);
        refresh();
    }

    public int getEntityId() {
        refresh();
        return entityId;
    }

    public void setEntityId(int entityId) {
        this.entityId = entityId;
        changed = true;
    }

    public int getAnimationType() {
        refresh();
        return animationType;
    }

    public void setAnimationType(int animationType) {
        this.animationType = animationType;
        changed = true;
    }

    public void update() {
        try {
            Field entityId = getHandler().getClass().getDeclaredField("a");
            Field type = getHandler().getClass().getDeclaredField("b");
            entityId.setAccessible(true);
            type.setAccessible(true);
            entityId.set(getHandler(), this.entityId);
            type.set(getHandler(), animationType);
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
    }

    public void refresh() {
        try {
            Field entityId = getHandler().getClass().getDeclaredField("a");
            Field type = getHandler().getClass().getDeclaredField("b");
            entityId.setAccessible(true);
            type.setAccessible(true);
            this.entityId = entityId.getInt(getHandler());
            animationType = type.getInt(getHandler());
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public boolean hasChanged() {
        return changed;
    }
}

package com.degoos.wetsponge.packet.play.server;


import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.packet.SpigotPacket;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import com.flowpowered.math.vector.Vector2i;
import com.flowpowered.math.vector.Vector3i;

import java.lang.reflect.Field;
import java.util.Arrays;

public class SpigotSPacketEntity extends SpigotPacket implements WSSPacketEntity {

	private int      entityId;
	private Vector3i position;
	private Vector2i rotation;
	private boolean  onGround, rotating, changed;


	public SpigotSPacketEntity (Object packet) {
		super(packet);
		refresh();
	}


	@Override
	public int getEntityId () {
		return entityId;
	}


	@Override
	public void setEntityId (int entityId) {
		this.entityId = entityId;
		changed = true;
	}


	public Vector3i getPosition () {
		return position;
	}


	public void setPosition (Vector3i position) {
		this.position = position;
		changed = true;
	}


	public Vector2i getRotation () {
		return rotation;
	}


	public void setRotation (Vector2i rotation) {
		this.rotation = rotation;
		changed = true;
	}


	public boolean isOnGround () {
		return onGround;
	}


	public void setOnGround (boolean onGround) {
		this.onGround = onGround;
		changed = true;
	}


	public boolean isRotating () {
		return rotating;
	}


	public void setRotating (boolean rotating) {
		this.rotating = rotating;
		changed = true;
	}


	@Override
	public void update () {
		try {
			Field[] fields = NMSUtils.getNMSClass("PacketPlayOutEntity").getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));
			fields[0].setInt(getHandler(), entityId);
			if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD)) {
				fields[1].setInt(getHandler(), position.getX());
				fields[2].setInt(getHandler(), position.getY());
				fields[3].setInt(getHandler(), position.getZ());
			}
			else {
				fields[1].setByte(getHandler(), (byte) position.getX());
				fields[2].setByte(getHandler(), (byte) position.getY());
				fields[3].setByte(getHandler(), (byte) position.getZ());
			}
			fields[4].setByte(getHandler(), (byte) rotation.getX());
			fields[5].setByte(getHandler(), (byte) rotation.getY());
			fields[6].setBoolean(getHandler(), onGround);
			fields[7].setBoolean(getHandler(), rotating);
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}


	@Override
	public void refresh () {
		try {
			Field[] fields = NMSUtils.getNMSClass("PacketPlayOutEntity").getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));
			entityId = fields[0].getInt(getHandler());
			if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD))
				position = new Vector3i(fields[1].getInt(getHandler()), fields[2].getInt(getHandler()), fields[3].getInt(getHandler()));
			else position = new Vector3i(fields[1].getByte(getHandler()), fields[2].getByte(getHandler()), fields[3].getByte(getHandler()));
			rotation = new Vector2i(fields[4].getByte(getHandler()), fields[5].getByte(getHandler()));
			onGround = fields[6].getBoolean(getHandler());
			rotating = fields[7].getBoolean(getHandler());
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}


	@Override
	public boolean hasChanged () {
		return changed;
	}
}

package com.degoos.wetsponge.entity.living.merchant;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.entity.living.SpigotAgeable;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.enums.EnumVillagerProfession;
import com.degoos.wetsponge.merchant.SpigotTrade;
import com.degoos.wetsponge.merchant.WSTrade;
import com.degoos.wetsponge.parser.player.PlayerParser;
import com.degoos.wetsponge.resource.spigot.SpigotMerchantUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import org.bukkit.entity.Villager;

public class SpigotVillager extends SpigotAgeable implements WSVillager {


	public SpigotVillager(Villager entity) {
		super(entity);
	}

	@Override
	public EnumVillagerProfession getProfession() {
		return EnumVillagerProfession.getBySpigotName(getHandled().getProfession().name()).orElseThrow(NullPointerException::new);
	}

	@Override
	public void setProfession(EnumVillagerProfession profession) {
		getHandled().setProfession(Villager.Profession.valueOf(profession.getSpigotName()));
	}

	@Override
	public Optional<WSPlayer> getCustomer() {
		return WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD) ? Optional.ofNullable(getHandled().getTrader())
			.map(target -> PlayerParser.getPlayer(target.getUniqueId()).orElse(null)).filter(Objects::nonNull) : Optional.empty();
	}

	@Override
	public void setCustomer(WSPlayer player) {
		if (getCustomer().isPresent()) return;
		SpigotMerchantUtils.openGUI(player, this);
	}

	@Override
	public List<WSTrade> getTrades() {
		if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD))
			return getHandled().getRecipes().stream().map(SpigotTrade::new).collect(Collectors.toList());
		else return SpigotMerchantUtils.getTrades(this);
	}

	@Override
	public void setTrades(List<WSTrade> trades) {
		if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD))
			getHandled().setRecipes(trades.stream().map(trade -> ((SpigotTrade) trade).getHandled()).collect(Collectors.toList()));
		else SpigotMerchantUtils.setTrades(this, trades);
	}

	@Override
	public void addTrade(WSTrade trade) {
		if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD)) {
			List<org.bukkit.inventory.MerchantRecipe> recipes = new ArrayList<>(getHandled().getRecipes());
			recipes.add(((SpigotTrade) trade).getHandled());
			getHandled().setRecipes(recipes);
		} else {
			List<WSTrade> trades = SpigotMerchantUtils.getTrades(this);
			trades.add(trade);
			SpigotMerchantUtils.setTrades(this, trades);
		}
	}

	@Override
	public void removeTrade(WSTrade trade) {
		if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD))
			getHandled().setRecipes(getHandled().getRecipes().stream().filter(target -> target.equals(((SpigotTrade) trade).getHandled())).collect(Collectors.toList()));
		else {
			List<WSTrade> trades = SpigotMerchantUtils.getTrades(this);
			trades.remove(trade);
			SpigotMerchantUtils.setTrades(this, trades);
		}
	}

	@Override
	public void clearTrades() {
		if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD)) getHandled().setRecipes(new ArrayList<>());
		else SpigotMerchantUtils.setTrades(this, new ArrayList<>());
	}

	@Override
	public Villager getHandled() {
		return (Villager) super.getHandled();
	}

}

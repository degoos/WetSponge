package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.SpigotBlockTypeDirectional;
import org.bukkit.material.MaterialData;

import java.util.Objects;
import java.util.Set;

public class SpigotBlockTypeWallSign extends SpigotBlockTypeDirectional implements WSBlockTypeWallSign {

	private boolean waterlogged;

	public SpigotBlockTypeWallSign(EnumBlockFace facing, Set<EnumBlockFace> faces, boolean waterlogged) {
		super(68, "minecraft:wall_sign", "minecraft:wall_sign", 64, facing, faces);
		this.waterlogged = waterlogged;
	}

	@Override
	public boolean isWaterlogged() {
		return waterlogged;
	}

	@Override
	public void setWaterlogged(boolean waterlogged) {
		this.waterlogged = waterlogged;
	}

	@Override
	public SpigotBlockTypeWallSign clone() {
		return new SpigotBlockTypeWallSign(getFacing(), getFaces(), waterlogged);
	}

	@Override
	public MaterialData toMaterialData() {
		return super.toMaterialData();
	}

	@Override
	public SpigotBlockTypeWallSign readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeWallSign that = (SpigotBlockTypeWallSign) o;
		return waterlogged == that.waterlogged;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), waterlogged);
	}
}

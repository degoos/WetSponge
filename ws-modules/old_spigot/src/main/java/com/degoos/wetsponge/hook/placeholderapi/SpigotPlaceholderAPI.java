package com.degoos.wetsponge.hook.placeholderapi;

import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.enums.EnumServerType;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.util.Validate;
import me.clip.placeholderapi.PlaceholderAPI;
import me.clip.placeholderapi.PlaceholderHook;
import me.clip.placeholderapi.expansion.PlaceholderExpansion;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class SpigotPlaceholderAPI extends WSPlaceholderAPI {

	private static final Pattern PLACEHOLDER_PATTERN = Pattern.compile("[%]([^%]+)[%]");
	private static final Pattern BRACKET_PLACEHOLDER_PATTERN = Pattern.compile("[{]([^{}]+)[}]");
	private static final Pattern RELATIONAL_PLACEHOLDER_PATTERN = Pattern.compile("[%](rel_)([^%]+)[%]");

	public SpigotPlaceholderAPI() {
		super(EnumServerType.SPIGOT, EnumServerType.PAPER_SPIGOT);
	}

	@Override
	public WSText parse(WSPlayer player, WSText text) {
		Validate.notNull(text, "Text cannot be null!");
		Validate.notNull(player, "Player cannot be null!");
		Map<String, PlaceholderHook> hooks = PlaceholderAPI.getPlaceholders();
		if (text != null && !hooks.isEmpty()) {
			Matcher m = PLACEHOLDER_PATTERN.matcher(text.toFormattingText());

			while (m.find()) {
				String format = m.group(1);
				int index = format.indexOf("_");
				if (index > 0 && index < format.length()) {
					String pl = format.substring(0, index).toLowerCase();
					String identifier = format.substring(index + 1);
					if (hooks.containsKey(pl)) {
						String value = hooks.get(pl).onPlaceholderRequest((Player) player.getHandled(), identifier);
						if (value != null) {
							text = text.replace("%" + format + "%", Matcher.quoteReplacement(value).replace('&', '\u00A7'));
						}
					}
				}
			}
			return text;
		} else {
			return text;
		}
	}

	@Override
	public List<WSText> parse(WSPlayer player, List<WSText> texts) {
		Validate.notNull(texts, "Texts cannot be null!");
		return texts.stream().map(text -> parse(player, text)).collect(Collectors.toList());
	}

	@Override
	public String parse(WSPlayer player, String string) {
		Validate.notNull(string, "String cannot be null!");
		return parse(player, WSText.of(string)).toFormattingText();
	}

	@Override
	public void registerExpansion(WSPlaceholderAPIExpansion expansion) {
		Validate.notNull(expansion, "Expansion cannot be null!");
		new SpigotPlaceholderAPIExpansionTranslator(expansion).register();
	}

	@Override
	public void unregisterExpansion(String identifier) {
		Validate.notNull(identifier, "Identifier cannot be null!");
		PlaceholderAPI.unregisterPlaceholderHook(identifier);
	}

	@Override
	public Collection<WSPlaceholderAPIHook> getHooks() {
		List<WSPlaceholderAPIHook> hooks = new ArrayList<>();
		PlaceholderAPI.getPlaceholders().forEach((s, placeholderHook) -> {
			if (placeholderHook instanceof PlaceholderExpansion) hooks.add(new SpigotPlaceholderAPIExpansion((PlaceholderExpansion) placeholderHook));
			else hooks.add(new SpigotPlaceholderAPIHook(placeholderHook, s));
		});
		return hooks;
	}

	@Override
	public void unregisterAllExpansions() {
		PlaceholderAPI.unregisterAllExpansions();
	}

	@Override
	public void onUnload() {
	}
}

package com.degoos.wetsponge.material.item.type;

import com.degoos.wetsponge.block.tileentity.extra.WSBannerPattern;
import com.degoos.wetsponge.enums.EnumDyeColor;
import com.degoos.wetsponge.enums.block.EnumBannerPatternShape;
import org.bukkit.DyeColor;
import org.bukkit.block.banner.Pattern;
import org.bukkit.block.banner.PatternType;
import org.bukkit.inventory.meta.BannerMeta;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class SpigotItemTypeBanner extends SpigotItemTypeDyeColored implements WSItemTypeBanner {

	private List<WSBannerPattern> patterns;

	public SpigotItemTypeBanner(EnumDyeColor dyeColor, List<WSBannerPattern> patterns) {
		super(425, "minecraft:banner", "banner", 64, dyeColor);
		this.patterns = patterns == null ? new ArrayList<>() : patterns.stream().map(WSBannerPattern::clone).collect(Collectors.toList());
	}

	@Override
	public List<WSBannerPattern> getPatterns() {
		return patterns;
	}

	@Override
	public void setPatterns(List<WSBannerPattern> patterns) {
		this.patterns = patterns == null ? new ArrayList<>() : patterns;
	}

	@Override
	public SpigotItemTypeBanner clone() {
		return new SpigotItemTypeBanner(getDyeColor(), patterns);
	}

	@Override
	public void writeItemMeta(ItemMeta itemMeta) {
		super.writeItemMeta(itemMeta);
		if (itemMeta instanceof BannerMeta) {
			while (((BannerMeta) itemMeta).numberOfPatterns() > 0) ((BannerMeta) itemMeta).removePattern(0);
			((BannerMeta) itemMeta).setPatterns(patterns.stream().map(target -> new Pattern(DyeColor.getByDyeData(target.getColor().getDyeData()),
					PatternType.valueOf(target.getShape().name()))).collect(Collectors.toList()));
		}
	}

	@Override
	public void readItemMeta(ItemMeta itemMeta) {
		super.readItemMeta(itemMeta);
		if (itemMeta instanceof BannerMeta) {
			patterns = ((BannerMeta) itemMeta).getPatterns().stream().map(target -> new WSBannerPattern(EnumBannerPatternShape.
					getByName(target.getPattern().name()).orElse(EnumBannerPatternShape.BASE),
					EnumDyeColor.getByDyeData(target.getColor().getDyeData()).orElse(EnumDyeColor.WHITE))).collect(Collectors.toList());
		}
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotItemTypeBanner that = (SpigotItemTypeBanner) o;
		return Objects.equals(patterns, that.patterns);
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), patterns);
	}
}

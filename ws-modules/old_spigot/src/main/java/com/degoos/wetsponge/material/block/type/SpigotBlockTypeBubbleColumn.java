package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.material.block.SpigotBlockType;
import org.bukkit.material.MaterialData;

import java.util.Objects;

public class SpigotBlockTypeBubbleColumn extends SpigotBlockType implements WSBlockTypeBubbleColumn {

	private boolean drag;

	public SpigotBlockTypeBubbleColumn(boolean drag) {
		super(-1, null, "minecraft:bubble_column", 64);
		this.drag = drag;
	}

	@Override
	public boolean isDrag() {
		return drag;
	}

	@Override
	public void setDrag(boolean drag) {
		this.drag = drag;
	}

	@Override
	public SpigotBlockTypeBubbleColumn clone() {
		return new SpigotBlockTypeBubbleColumn(drag);
	}

	@Override
	public MaterialData toMaterialData() {
		return super.toMaterialData();
	}

	@Override
	public SpigotBlockTypeBubbleColumn readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeBubbleColumn that = (SpigotBlockTypeBubbleColumn) o;
		return drag == that.drag;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), drag);
	}
}

package com.degoos.wetsponge.material.item.type;

import com.degoos.wetsponge.enums.item.EnumItemTypeGoldenAppleType;
import com.degoos.wetsponge.material.item.SpigotItemType;
import com.degoos.wetsponge.util.Validate;

import java.util.Objects;

public class SpigotItemTypeGoldenApple extends SpigotItemType implements WSItemTypeGoldenApple {

	private EnumItemTypeGoldenAppleType goldenAppleType;

	public SpigotItemTypeGoldenApple(EnumItemTypeGoldenAppleType goldenAppleType) {
		super(322, "minecraft:golden_apple", "minecraft:golden_apple", 64);
		Validate.notNull(goldenAppleType, "Golden apple type cannot be null!");
		this.goldenAppleType = goldenAppleType;
	}

	@Override
	public EnumItemTypeGoldenAppleType getGoldenAppleType() {
		return goldenAppleType;
	}

	@Override
	public void setGoldenAppleType(EnumItemTypeGoldenAppleType goldenAppleType) {
		Validate.notNull(goldenAppleType, "Golden apple type cannot be null!");
		this.goldenAppleType = goldenAppleType;
	}

	@Override
	public SpigotItemTypeGoldenApple clone() {
		return new SpigotItemTypeGoldenApple(goldenAppleType);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotItemTypeGoldenApple that = (SpigotItemTypeGoldenApple) o;
		return goldenAppleType == that.goldenAppleType;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), goldenAppleType);
	}
}

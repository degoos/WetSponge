package com.degoos.wetsponge.entity.living.complex;

import org.bukkit.entity.EnderDragon;

public class SpigotEnderDragon extends SpigotComplexLivingEntity implements WSEnderDragon {


	public SpigotEnderDragon(EnderDragon entity) {
		super(entity);
	}

	@Override
	public EnderDragon getHandled() {
		return (EnderDragon) super.getHandled();
	}
}

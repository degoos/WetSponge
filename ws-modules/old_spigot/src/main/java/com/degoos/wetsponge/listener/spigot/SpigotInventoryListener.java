package com.degoos.wetsponge.listener.spigot;


import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.data.WSTransaction;
import com.degoos.wetsponge.event.entity.player.WSPlayerChangeHotbarSlotEvent;
import com.degoos.wetsponge.event.inventory.WSInventoryClickEvent;
import com.degoos.wetsponge.event.inventory.WSInventoryCloseEvent;
import com.degoos.wetsponge.event.inventory.WSInventoryOpenEvent;
import com.degoos.wetsponge.inventory.SpigotInventory;
import com.degoos.wetsponge.inventory.WSSlotPos;
import com.degoos.wetsponge.inventory.multiinventory.MultiInventoryListener;
import com.degoos.wetsponge.item.SpigotItemStack;
import com.degoos.wetsponge.parser.player.PlayerParser;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.SpigotEventUtils;
import java.util.Optional;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;

public class SpigotInventoryListener implements Listener {

	@EventHandler(priority = EventPriority.LOWEST)
	public void onOpen(InventoryOpenEvent event) {
		if(!SpigotEventUtils.shouldBeExecuted()) return;
		try {
			WSInventoryOpenEvent wetSpongeEvent = new WSInventoryOpenEvent(new SpigotInventory(event.getInventory()), PlayerParser
				.getPlayer(event.getPlayer().getUniqueId()).orElse(null));
			WetSponge.getEventManager().callEvent(wetSpongeEvent);
			event.setCancelled(wetSpongeEvent.isCancelled());
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Spigot-InventoryOpenEvent!");
		}
	}


	@EventHandler(priority = EventPriority.LOWEST)
	public void onClose(InventoryCloseEvent event) {
		if(!SpigotEventUtils.shouldBeExecuted()) return;
		try {
			WSInventoryCloseEvent wetSpongeEvent = new WSInventoryCloseEvent(new SpigotInventory(event.getInventory()), PlayerParser
				.getPlayer(event.getPlayer().getUniqueId()).orElse(null));
			WetSponge.getEventManager().callEvent(wetSpongeEvent);
			if (!wetSpongeEvent.isCancelled()) MultiInventoryListener.closeInv(wetSpongeEvent.getPlayer());
			if (wetSpongeEvent.isCancelled()) event.getPlayer().openInventory(event.getInventory());
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Spigot-InventoryCloseEvent!");
		}
	}


	@EventHandler(priority = EventPriority.LOWEST)
	public void onClick(InventoryClickEvent event) {
		if(!SpigotEventUtils.shouldBeExecuted()) return;
		try {
			WSInventoryClickEvent wetSpongeEvent = new WSInventoryClickEvent(
				event.getClickedInventory() == null ? new WSSlotPos(-1, -1) : WSSlotPos.getBySlot(9, event.getSlot()), new SpigotInventory(
				event.getClickedInventory() == null ? event.getInventory() : event.getClickedInventory()), PlayerParser.getPlayer(event.getWhoClicked().getUniqueId())
				.orElse(null), new WSTransaction<>(Optional.ofNullable(event.getCurrentItem()).map(SpigotItemStack::new), Optional.ofNullable(event.getCursor())
				.map(SpigotItemStack::new)));
			WetSponge.getEventManager().callEvent(wetSpongeEvent);
			if (wetSpongeEvent.getTransaction().hasChanged()) {
				if (wetSpongeEvent.getTransaction().getNewData().isPresent())
					event.setCursor(((SpigotItemStack) wetSpongeEvent.getTransaction().getNewData().get()).getHandled());
				else event.setCursor(null);
			}
			if (!wetSpongeEvent.isCancelled()) MultiInventoryListener.clickInv(wetSpongeEvent);
			event.setCancelled(wetSpongeEvent.isCancelled());
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Spigot-InventoryClickEvent!");
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onChangeHotbarSlot(PlayerItemHeldEvent event) {
		if(!SpigotEventUtils.shouldBeExecuted()) return;
		WSPlayerChangeHotbarSlotEvent wetSpongeEvent = new WSPlayerChangeHotbarSlotEvent(PlayerParser.getPlayer(event.getPlayer().getUniqueId()).orElse(null), event
			.getPreviousSlot(), event.getNewSlot());
		WetSponge.getEventManager().callEvent(wetSpongeEvent);
		event.setCancelled(wetSpongeEvent.isCancelled());
	}
}

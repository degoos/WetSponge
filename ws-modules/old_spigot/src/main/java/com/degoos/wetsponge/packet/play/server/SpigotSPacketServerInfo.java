package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.packet.SpigotPacket;
import com.degoos.wetsponge.server.response.WSServerStatusResponse;
import com.degoos.wetsponge.server.response.WSStatusPlayers;
import com.degoos.wetsponge.server.response.WSStatusVersion;
import com.degoos.wetsponge.text.SpigotText;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.user.SpigotGameProfile;
import com.degoos.wetsponge.user.WSGameProfile;
import com.degoos.wetsponge.util.ListUtils;
import com.degoos.wetsponge.util.Validate;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;
import com.degoos.wetsponge.util.reflection.SpigotTextUtils;
import com.mojang.authlib.GameProfile;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.chat.ComponentSerializer;

public class SpigotSPacketServerInfo extends SpigotPacket implements WSSPacketServerInfo {

	private WSServerStatusResponse response;
	private boolean changed;

	public SpigotSPacketServerInfo(WSServerStatusResponse response) {
		super(NMSUtils.getNMSClass("PacketStatusOutServerInfo"));
		Validate.notNull(response, "Response cannot be null!");
		this.response = response;
		this.changed = false;
		update();
	}

	public SpigotSPacketServerInfo(Object packet) {
		super(packet);
		this.changed = false;
		refresh();
	}

	@Override
	public void update() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));

			Class<?> responseClass = NMSUtils.getNMSClass("ServerPing");
			Class<?> versionClass = NMSUtils.getNMSClass("ServerPing$ServerData");
			Class<?> playersClass = NMSUtils.getNMSClass("ServerPing$ServerPingPlayerSample");

			Object nmsResponse = ReflectionUtils.instantiateObject(responseClass);
			Object version = ReflectionUtils.instantiateObject(versionClass, response.getVersion().getName(), response.getVersion().getProtocol());
			Object players = ReflectionUtils.instantiateObject(playersClass, response.getPlayers().getMaxPlayers(), response.getPlayers().getPlayerCount());

			ReflectionUtils.invokeMethod(nmsResponse, "setFavicon", response.getFaviconString());
			ReflectionUtils.invokeMethod(nmsResponse, "setMOTD", SpigotTextUtils.toJSON(ComponentSerializer.toString((BaseComponent) response.getMOTD().getHandled())));

			GameProfile[] profiles = ListUtils
				.toArray(com.mojang.authlib.GameProfile.class, response.getPlayers().getProfiles().stream().map(profile -> (GameProfile) profile.getHandled())
					.collect(Collectors.toList()));

			ReflectionUtils.invokeMethod(players, "a", (Object) profiles);

			ReflectionUtils.invokeMethod(nmsResponse, "setPlayerSample", players);
			ReflectionUtils.invokeMethod(nmsResponse, "setServerInfo", version);

			fields[1].set(getHandler(), nmsResponse);
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void refresh() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));

			Class<?> responseClass = NMSUtils.getNMSClass("ServerPing");
			Class<?> versionClass = NMSUtils.getNMSClass("ServerPing$ServerData");
			Class<?> playersClass = NMSUtils.getNMSClass("ServerPing$ServerPingPlayerSample");

			Field[] responseFields = responseClass.getClass().getDeclaredFields();
			Arrays.stream(responseFields).forEach(field -> field.setAccessible(true));

			Object nmsResponse = fields[1].get(getHandler());
			Object nmsMotd = responseFields[0].get(nmsResponse);
			Object nmsPlayers = responseFields[1].get(nmsResponse);
			Object nmsVersion = responseFields[2].get(nmsResponse);
			String favicon = (String) responseFields[3].get(nmsResponse);

			WSText motd = SpigotText.of(new TextComponent(ComponentSerializer.parse(SpigotTextUtils.toJSON(nmsMotd))));

			Field[] playerFields = playersClass.getClass().getDeclaredFields();
			Arrays.stream(playerFields).forEach(field -> field.setAccessible(true));
			Field[] versionFields = versionClass.getClass().getDeclaredFields();
			Arrays.stream(versionFields).forEach(field -> field.setAccessible(true));

			Set<WSGameProfile> profiles = ListUtils.toSet((GameProfile[]) playerFields[2].get(nmsPlayers)).stream().map(SpigotGameProfile::new)
				.collect(Collectors.toSet());

			response = new WSServerStatusResponse(motd, favicon, new WSStatusPlayers(playerFields[0].getInt(nmsPlayers), playerFields[1]
				.getInt(nmsPlayers), profiles), new WSStatusVersion((String) versionFields[0].get(nmsVersion), versionFields[1].getInt(nmsVersion)));
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public boolean hasChanged() {
		return changed || response.hasChanged();
	}

	@Override
	public WSServerStatusResponse getResponse() {
		return response;
	}

	@Override
	public void setResponse(WSServerStatusResponse response) {
		this.response = response;
		this.changed = true;
	}
}

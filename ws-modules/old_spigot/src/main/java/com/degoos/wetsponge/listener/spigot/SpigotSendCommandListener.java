package com.degoos.wetsponge.listener.spigot;


import com.degoos.wetsponge.command.SpigotCommandSource;
import com.degoos.wetsponge.command.WSCommandManager;
import com.degoos.wetsponge.command.WSCommandSource;
import com.degoos.wetsponge.console.SpigotConsoleSource;
import com.degoos.wetsponge.event.WSEventManager;
import com.degoos.wetsponge.event.command.WSSendCommandEvent;
import com.degoos.wetsponge.parser.player.PlayerParser;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.SpigotEventUtils;
import com.degoos.wetsponge.util.StringUtils;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.server.ServerCommandEvent;

public class SpigotSendCommandListener implements Listener {

	@EventHandler(priority = EventPriority.LOWEST)
	public void onCommand(PlayerCommandPreprocessEvent event) {
		if (!SpigotEventUtils.shouldBeExecuted()) return;
		try {
			for (StackTraceElement traceElement : Thread.currentThread().getStackTrace())
				if (traceElement.toString().contains("worldedit")) return;
			boolean hasLine = event.getMessage().startsWith("/");
			String command = event.getMessage().split(" ")[0];
			if (hasLine) command = StringUtils.replaceFirstChar(command, '/', null);
			WSCommandSource source = getCommandSource(event.getPlayer());
			WSSendCommandEvent wetSpongeEvent = new WSSendCommandEvent(command, StringUtils.replaceFirst(event.getMessage(),
					(hasLine ? "/" : "") + command + " ", null).split(" "), source);
			WSEventManager.getInstance().callEvent(wetSpongeEvent);
			event.setMessage((hasLine ? "/" : "") + wetSpongeEvent.getCommand() + getArgumentsString(wetSpongeEvent.getArguments()));

			if (wetSpongeEvent.isCancelled()) {
				event.setCancelled(true);
				return;
			}
			WSCommandManager.getInstance().getCommand(wetSpongeEvent.getCommand()).ifPresent(wsCommand -> {
				event.setCancelled(true);
				//wsCommand.timings.startTiming();
				wsCommand.executeCommand(source, wetSpongeEvent.getCommand(), wetSpongeEvent.getArguments());
				//wsCommand.timings.stopTiming();
			});
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Spigot-PlayerCommandPreprocessEvent!");
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onCommand(ServerCommandEvent event) {
		if (!SpigotEventUtils.shouldBeExecuted()) return;
		try {
			for (StackTraceElement traceElement : Thread.currentThread().getStackTrace())
				if (traceElement.toString().contains("worldedit")) return;
			boolean hasLine = event.getCommand().startsWith("/");
			String command = event.getCommand().split(" ")[0];
			if (hasLine) command = StringUtils.replaceFirstChar(command, '/', null);
			WSCommandSource source = getCommandSource(event.getSender());
			WSSendCommandEvent wetSpongeEvent = new WSSendCommandEvent(command,
					StringUtils.replaceFirst(event.getCommand(), (hasLine ? "/" : "") + command + " ", null)
							.split(" "), source);
			WSEventManager.getInstance().callEvent(wetSpongeEvent);
			event.setCommand((hasLine ? "/" : "") + wetSpongeEvent.getCommand() + getArgumentsString(wetSpongeEvent.getArguments()));
			if (wetSpongeEvent.isCancelled()) {
				event.setCancelled(true);
				return;
			}
			WSCommandManager.getInstance().getCommand(wetSpongeEvent.getCommand()).ifPresent(wsCommand -> {
				event.setCancelled(true);
				//wsCommand.timings.startTiming();
				wsCommand.executeCommand(source, wetSpongeEvent.getCommand(), wetSpongeEvent.getArguments());
				//wsCommand.timings.stopTiming();
			});
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Spigot-ServerCommandEvent!");
		}
	}


	private WSCommandSource getCommandSource(CommandSender source) {
		if (source instanceof Player) return PlayerParser.getPlayer(((Player) source).getUniqueId()).orElse(null);
		if (source instanceof ConsoleCommandSender) return new SpigotConsoleSource((ConsoleCommandSender) source);
		else return new SpigotCommandSource(source);
	}


	private String getArgumentsString(String[] arguments) {
		StringBuilder builder = new StringBuilder();
		for (String s : arguments)
			builder.append(" ").append(s);
		return builder.toString();
	}


}

package com.degoos.wetsponge.world;


import com.degoos.wetsponge.block.SpigotBlock;
import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.enums.EnumBiomeType;
import com.degoos.wetsponge.listener.spigot.SpigotWorldListener;
import com.degoos.wetsponge.parser.entity.SpigotEntityParser;
import com.degoos.wetsponge.parser.world.WorldParser;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.NumericUtils;
import com.flowpowered.math.vector.Vector2f;
import com.flowpowered.math.vector.Vector3d;
import com.flowpowered.math.vector.Vector3i;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.util.NumberConversions;

import java.util.*;
import java.util.stream.Collectors;

public class SpigotLocation implements WSLocation {

	public static Map<String, List<SpigotLocation>> locations = new HashMap<>();

	private String worldName;
	private double x, y, z;
	private float yaw, pitch;
	private Location bukkitLocation;
	private World bukkitWorld;


	public SpigotLocation(String worldName, double x, double y, double z, float yaw, float pitch) {
		this.worldName = worldName;
		this.x = x;
		this.y = y;
		this.z = z;
		this.yaw = yaw;
		this.pitch = pitch;
		this.bukkitWorld = SpigotWorldListener.getWorld(worldName.toLowerCase()).orElse(null);
		putInMap();

	}


	public SpigotLocation(World world, double x, double y, double z, float yaw, float pitch) {
		this.worldName = world.getName();
		this.x = x;
		this.y = y;
		this.z = z;
		this.yaw = yaw;
		this.pitch = pitch;
		this.bukkitWorld = SpigotWorldListener.getWorld(worldName.toLowerCase()).orElse(null);
		putInMap();
	}


	public SpigotLocation(Location location) {
		this.worldName = location.getWorld().getName();
		this.x = location.getX();
		this.y = location.getY();
		this.z = location.getZ();
		this.yaw = location.getYaw();
		this.pitch = location.getPitch();
		this.bukkitWorld = location.getWorld();
		putInMap();
	}


	public SpigotLocation(WSLocation location) {
		this.worldName = location.getWorldName();
		this.x = location.getX();
		this.y = location.getY();
		this.z = location.getZ();
		this.yaw = location.getYaw();
		this.pitch = location.getPitch();
		this.bukkitWorld = SpigotWorldListener.getWorld(worldName.toLowerCase()).orElse(null);
		putInMap();
	}


	public SpigotLocation(String string) {
		String[] sl = string.split(";");
		if (NumericUtils.isDouble(sl[0])) {
			this.worldName = sl[5];
			this.x = Double.parseDouble(sl[0]);
			this.y = Double.parseDouble(sl[1]);
			this.z = Double.parseDouble(sl[2]);
			this.pitch = Float.parseFloat(sl[3]);
			this.yaw = Float.parseFloat(sl[4]);
		} else {
			this.worldName = sl[0];
			this.x = Double.parseDouble(sl[1]);
			this.y = Double.parseDouble(sl[2]);
			this.z = Double.parseDouble(sl[3]);
			this.yaw = Float.parseFloat(sl[4]);
			this.pitch = Float.parseFloat(sl[5]);
		}
		this.bukkitWorld = SpigotWorldListener.getWorld(worldName.toLowerCase()).orElse(null);
		putInMap();
	}


	public SpigotLocation(Player player) {
		Location location = player.getLocation();
		this.worldName = location.getWorld().getName();
		this.x = location.getX();
		this.y = location.getY();
		this.z = location.getZ();
		this.yaw = location.getYaw();
		this.pitch = location.getPitch();
		this.bukkitWorld = location.getWorld();
		putInMap();
	}


	private void putInMap() {
		if (locations.containsKey(worldName)) locations.get(worldName).add(this);
		else {
			List<SpigotLocation> list = new ArrayList<>();
			list.add(this);
			locations.put(worldName, list);
		}
		if (bukkitWorld != null) bukkitLocation = new Location(bukkitWorld, x, y, z, yaw, pitch);

	}


	public String toString() {
		return worldName + ";" + x + ";" + y + ";" + z + ";" + yaw + ";" + pitch;
	}


	public Location getLocation() {
		bukkitWorld = SpigotWorldListener.getWorld(worldName.toLowerCase()).orElse(null);
		if (bukkitWorld != null && bukkitLocation == null) bukkitLocation = new Location(bukkitWorld, x, y, z, yaw, pitch);
		if (bukkitWorld == null) {
			InternalLogger.sendError("ERROR: world " + worldName + " doesn't exist!");
			return new Location(Bukkit.getWorlds().get(0), x, y, z, yaw, pitch);
		}
		return bukkitLocation.clone();
	}


	public SpigotBlock getBlock() {
		return new SpigotBlock(getLocation().getBlock());
	}


	public String getWorldName() {
		return worldName;
	}


	public SpigotLocation setWorldName(String worldName) {
		locations.get(this.worldName).remove(this);
		this.worldName = worldName;
		bukkitWorld = SpigotWorldListener.getWorld(worldName.toLowerCase()).orElse(null);
		putInMap();
		return this;
	}


	public SpigotWorld getWorld() {
		bukkitWorld = SpigotWorldListener.getWorld(worldName.toLowerCase()).orElse(null);
		return (SpigotWorld) WorldParser.getOrCreateWorld(bukkitWorld.getName(), bukkitWorld);
	}


	@Override
	public WSLocation setWorld(WSWorld world) {
		setWorldName(world.getName());
		return this;
	}


	public WSLocation updateWorld() {
		this.bukkitWorld = SpigotWorldListener.getWorld(worldName.toLowerCase()).orElse(null);
		if (bukkitLocation != null) bukkitLocation.setWorld(bukkitWorld);
		else bukkitLocation = new Location(bukkitWorld, x, y, z, yaw, pitch);
		return this;
	}


	public double getX() {
		return x;
	}


	public SpigotLocation setX(double x) {
		this.x = x;
		if (bukkitLocation != null) bukkitLocation.setX(x);
		return this;
	}


	public double getY() {
		return y;
	}


	public SpigotLocation setY(double y) {
		this.y = y;
		if (bukkitLocation != null) bukkitLocation.setY(y);
		return this;
	}


	public double getZ() {
		return z;
	}


	public SpigotLocation setZ(double z) {
		this.z = z;
		if (bukkitLocation != null) bukkitLocation.setZ(z);
		return this;
	}


	public float getYaw() {
		return yaw;
	}


	public SpigotLocation setYaw(float yaw) {
		this.yaw = yaw;
		if (bukkitLocation != null) bukkitLocation.setYaw(yaw);
		return this;
	}


	public float getPitch() {
		return pitch;
	}


	public SpigotLocation setPitch(float pitch) {
		this.pitch = pitch;
		if (bukkitLocation != null) bukkitLocation.setPitch(pitch);
		return this;
	}

	@Override
	public Vector2f getRotation() {
		return new Vector2f(pitch, yaw);
	}

	@Override
	public WSLocation setRotation(Vector2f rotation) {
		this.pitch = rotation.getX();
		this.yaw = rotation.getY();
		return this;
	}


	public int getBlockX() {
		return NumberConversions.floor(x);
	}


	public int getBlockY() {
		return NumberConversions.floor(y);
	}


	public int getBlockZ() {
		return NumberConversions.floor(z);
	}


	public double distance(WSLocation location) {
		return Math.sqrt(Math.pow(location.getX() - x, 2) + Math.pow(location.getY() - y, 2) + Math.pow(location.getZ() - z, 2));
	}


	public double distance(Location location) {
		return Math.sqrt(Math.pow(location.getX() - x, 2) + Math.pow(location.getY() - y, 2) + Math.pow(location.getZ() - z, 2));
	}


	public double distance(double x, double y, double z) {
		return Math.sqrt(Math.pow(x - this.x, 2) + Math.pow(y - this.y, 2) + Math.pow(z - this.z, 2));
	}


	@Override
	public double distance(Vector3d vector3d) {
		return distance(vector3d.getX(), vector3d.getY(), vector3d.getZ());
	}


	public SpigotLocation add(double x, double y, double z) {
		this.x += x;
		this.y += y;
		this.z += z;
		if (bukkitLocation != null) bukkitLocation.add(x, y, z);
		return this;
	}

	@Override
	public WSLocation add(Vector3d vector3d) {
		return add(vector3d.getX(), vector3d.getY(), vector3d.getZ());
	}

	public Set<Entity> getEntitiesOnBlock() {
		if (bukkitWorld == null) return new HashSet<>();
		return bukkitWorld.getEntities().stream().filter(entity -> new SpigotLocation(entity.getLocation()).equalsBlock(this)).collect(Collectors.toSet());
	}


	public <T extends Entity> Set<T> getEntitiesOnBlock(Class<T> entityType) {
		if (bukkitWorld == null) return new HashSet<>();
		Set<T> set = new HashSet<>();
		bukkitWorld.getEntities().stream().filter(entity -> entityType.isInstance(entity) && new SpigotLocation(entity.getLocation()).equalsBlock(this))
				.forEach(entity -> set.add((T) entity));
		return set;
	}


	public SpigotLocation clone() {
		return new SpigotLocation(this);
	}


	public SpigotLocation getBlockLocation() {
		return new SpigotLocation(worldName, getBlockX(), getBlockY(), getBlockZ(), 0, 0);
	}

	@Override
	public EnumBiomeType getBiome() {
		return getWorld().getBiome(getBlockX(), getBlockY(), getBlockZ());
	}

	@Override
	public void setBiome(EnumBiomeType biome) {
		getWorld().setBiome(getBlockX(), getBlockY(), getBlockZ(), biome);
	}


	@Override
	public Collection<WSEntity> getNearbyEntities(Vector3d radius) {
		return bukkitWorld.getNearbyEntities(bukkitLocation, radius.getX(), radius.getY(), radius.getZ()).stream().map(SpigotEntityParser::getWSEntity)
				.collect(Collectors.toList());
	}


	@Override
	public Set<WSEntity> getNearbyEntities(double radius) {
		if (bukkitWorld == null) return new HashSet<>();
		return bukkitWorld.getEntities().stream().filter(entity -> distance(entity.getLocation()) <= radius).map(SpigotEntityParser::getWSEntity)
				.collect(Collectors.toSet());
	}


	@Override
	public <T extends WSEntity> Collection<T> getNearbyEntities(Vector3d radius, Class<T> entityType) {
		if (bukkitWorld == null) return new HashSet<>();
		Set<T> set = new HashSet<>();
		bukkitWorld.getEntities().stream().map(SpigotEntityParser::getWSEntity).filter(entity -> {
			WSLocation location = entity.getLocation();
			return entityType.isInstance(entity) && Math.abs(location.getX() - x) < radius.getX() && Math.abs(location.getY() - y) < radius.getY() &&
					Math.abs(location.getZ() - z) < radius.getZ();
		}).forEach(entity -> set.add((T) entity));
		return set;
	}


	@Override
	public <T extends WSEntity> Collection<T> getNearbyEntities(double radius, Class<T> entityType) {
		if (bukkitWorld == null) return new HashSet<>();
		Set<T> set = new HashSet<>();
		bukkitWorld.getEntities().stream().map(SpigotEntityParser::getWSEntity)
				.filter(entity -> entityType.isInstance(entity) && distance(entity.getLocation()) <= radius).forEach(entity -> set.add((T) entity));
		return set;
	}


	@Override
	public Collection<WSPlayer> getNearbyPlayers(Vector3d radius) {
		return getNearbyEntities(radius).stream().filter(entity -> entity instanceof Player).map(entity -> (WSPlayer) entity).collect(Collectors.toList());
	}


	@Override
	public Collection<WSPlayer> getNearbyPlayers(double radius) {
		return getNearbyEntities(radius).stream().filter(entity -> entity instanceof Player).map(entity -> (WSPlayer) entity).collect(Collectors.toList());
	}

	@Override
	public int getHighestY() {
		return bukkitWorld.getHighestBlockYAt(getBlockX(), getBlockZ());
	}


	public WSLocation updateSign(String[] lines) {
		if (lines.length != 4) return this;
		if (bukkitWorld == null) getLocation();
		if (bukkitWorld == null) return this;
		Block block = bukkitWorld.getBlockAt((int) x, (int) y, (int) z);
		if (block == null || !(block.getState() instanceof Sign)) return this;
		Sign sign = (Sign) block.getState();
		for (int i = 0; i < 4; i++) sign.setLine(i, lines[i]);
		sign.update(true);
		return this;
	}


	public boolean equalsBlock(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		SpigotLocation that = (SpigotLocation) o;

		return getBlockX() == that.getBlockX() && getBlockY() == that.getBlockY() && getBlockZ() == that.getBlockZ();
	}

	@Override
	public Vector3d getFacingDirection() {
		double xz = Math.cos(Math.toRadians(pitch));
		return new Vector3d(-xz * Math.sin(Math.toRadians(yaw)), -Math.sin(Math.toRadians(pitch)), xz * Math.cos(Math.toRadians(yaw)));
	}

	@Override
	public Vector3d toVector3d() {
		return new Vector3d(x, y, z);
	}


	@Override
	public Vector3i toVector3i() {
		return new Vector3i(getBlockX(), getBlockY(), getBlockZ());
	}

	@Override
	public Object getHandledWorld() {
		return bukkitWorld;
	}


	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		SpigotLocation that = (SpigotLocation) o;

		return Double.compare(that.x, x) == 0 && Double.compare(that.y, y) == 0 && Double.compare(that.z, z) == 0 && Float.compare(that.yaw, yaw) == 0 &&
				Float.compare(that.pitch, pitch) == 0 && (worldName != null ? worldName.equals(that.worldName) : that.worldName == null);
	}


	@Override
	public int hashCode() {
		int result;
		long temp;
		result = worldName != null ? worldName.hashCode() : 0;
		temp = Double.doubleToLongBits(x);
		result = 31 * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(y);
		result = 31 * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(z);
		result = 31 * result + (int) (temp ^ (temp >>> 32));
		result = 31 * result + (yaw != +0.0f ? Float.floatToIntBits(yaw) : 0);
		result = 31 * result + (pitch != +0.0f ? Float.floatToIntBits(pitch) : 0);
		return result;
	}
}

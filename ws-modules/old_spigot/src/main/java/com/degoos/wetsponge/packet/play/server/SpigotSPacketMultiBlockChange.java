package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.material.block.WSBlockType;
import com.degoos.wetsponge.packet.SpigotPacket;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;
import com.degoos.wetsponge.util.reflection.SpigotHandledUtils;
import com.flowpowered.math.vector.Vector2i;
import com.flowpowered.math.vector.Vector3i;

import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;

public class SpigotSPacketMultiBlockChange extends SpigotPacket implements WSSPacketMultiBlockChange {

	private Vector2i chunkPosition;
	private Map<Vector3i, WSBlockType> materials;
	private int index;
	private boolean changed;

	public SpigotSPacketMultiBlockChange(Vector2i chunkPosition, Map<Vector3i, WSBlockType> materials) throws IllegalAccessException, InstantiationException {
		super(NMSUtils.getNMSClass("PacketPlayOutMultiBlockChange").newInstance());
		this.chunkPosition = chunkPosition;
		this.materials = materials;
		update();
	}

	public SpigotSPacketMultiBlockChange(Object packet) {
		super(packet);
		refresh();
	}

	public Vector2i getChunkPosition() {
		return chunkPosition;
	}

	public void setChunkPosition(Vector2i chunkPosition) {
		changed = true;
		this.chunkPosition = chunkPosition;
	}

	public Map<Vector3i, WSBlockType> getMaterials() {
		changed = true;
		return materials;
	}

	public void setMaterials(Map<Vector3i, WSBlockType> materials) {
		changed = true;
		this.materials = materials;
	}

	public void removeMaterial(Vector3i position) {
		changed = true;
		materials.remove(position);
	}

	public void addMaterial(Vector3i position, WSBlockType type) {
		changed = true;
		materials.put(position, type);
	}

	@Override
	public void update() {
		index = 0;
		try {

			Class<?> chunkPosClass = NMSUtils.getNMSClass("ChunkCoordIntPair");
			Class<?> packetClass = NMSUtils.getNMSClass("PacketPlayOutMultiBlockChange");
			Class<?> blockUpdateDataClass = packetClass.getDeclaredClasses()[0];
			Class<?> blockStateClass = NMSUtils.getNMSClass("IBlockData");

			ReflectionUtils.setFirstObject(packetClass, chunkPosClass, getHandler(), chunkPosClass.getConstructor(int.class, int.class)
					.newInstance(chunkPosition.getX(), chunkPosition.getY()));

			Object data = Array.newInstance(blockUpdateDataClass, materials.size());

			Constructor constructor = blockUpdateDataClass.getConstructor(packetClass, short.class, blockStateClass);
			constructor.setAccessible(true);
			materials.forEach((vector3i, type) -> {
				try {
					short offset = (short) (((vector3i.getX() << 12) & 61440) | (vector3i.getY() & 255) | ((vector3i.getZ() << 8) & 3840));
					Object state = SpigotHandledUtils.getBlockState(type);
					Array.set(data, index, constructor.newInstance(getHandler(), offset, state));
				} catch (Throwable ex) {
					ex.printStackTrace();
				}
				index++;
			});
			ReflectionUtils.setFirstObject(packetClass, data.getClass(), getHandler(), data);
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void refresh() {
		try {

			Class<?> chunkPosClass = NMSUtils.getNMSClass("ChunkCoordIntPair");
			// Class<?> blockUpdateDataClass = NMSUtils.getNMSClass("PacketPlayOutMultiBlockChange.MultiBlockChangeInfo");
			Class<?> packetClass = NMSUtils.getNMSClass("PacketPlayOutMultiBlockChange");
			Class<?> blockUpdateDataClass = packetClass.getDeclaredClasses()[0];

			Object blocks = ReflectionUtils.getFirstObject(packetClass, Class.forName("[L" + blockUpdateDataClass.getName() + ";"), getHandler());

			Object pos = ReflectionUtils.getFirstObject(packetClass, chunkPosClass, getHandler());
			chunkPosition = new Vector2i(pos.getClass().getField("x").getInt(pos), pos.getClass().getField("z").getInt(pos));
			materials = new HashMap<>();

			int length = Array.getLength(blocks);
			for (int i = 0; i < length; i++) {
				Object data = Array.get(blocks, i);
				Vector3i blockPos = SpigotHandledUtils.getBlockPositionVector(data.getClass().getMethod("a").invoke(data))
						.sub(chunkPosition.getX() * 16, 0, chunkPosition.getY() * 16);
				materials.put(blockPos, SpigotHandledUtils.getMaterial(data.getClass().getMethod("c").invoke(data)));
			}
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public boolean hasChanged() {
		return changed;
	}
}

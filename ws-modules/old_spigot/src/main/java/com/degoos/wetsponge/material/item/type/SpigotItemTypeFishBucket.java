package com.degoos.wetsponge.material.item.type;

import com.degoos.wetsponge.enums.item.EnumItemTypeFishType;
import com.degoos.wetsponge.material.item.SpigotItemType;
import com.degoos.wetsponge.util.Validate;

import java.util.Objects;

public class SpigotItemTypeFishBucket extends SpigotItemType implements WSItemTypeFishBucket {

	private EnumItemTypeFishType fishType;

	public SpigotItemTypeFishBucket(EnumItemTypeFishType fishType) {
		super(-1, "minecraft:cod_bucket", "minecraft:cod_bucket", 1);
		Validate.notNull(fishType, "Fish type cannot be null!");
		this.fishType = fishType;
	}

	@Override
	public String getNewStringId() {
		switch (fishType) {
			case SALMON:
				return "minecraft:salmon_bucket";
			case PUFFERFISH:
				return "minecraft:pufferfish_bucket";
			case TROPICAL_FISH:
				return "minecraft:tropical_fish_bucket";
			case COD:
			default:
				return "minecraft:cod_bucket";
		}
	}

	@Override
	public EnumItemTypeFishType getFishType() {
		return fishType;
	}

	@Override
	public void setFishType(EnumItemTypeFishType fishType) {
		Validate.notNull(fishType, "Fish type cannot be null!");
		this.fishType = fishType;
	}

	@Override
	public SpigotItemTypeFishBucket clone() {
		return new SpigotItemTypeFishBucket(fishType);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotItemTypeFishBucket that = (SpigotItemTypeFishBucket) o;
		return fishType == that.fishType;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), fishType);
	}
}

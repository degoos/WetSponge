package com.degoos.wetsponge.material.block;

import com.degoos.wetsponge.enums.EnumDyeColor;
import com.degoos.wetsponge.material.block.type.WSBlockTypeDyeColored;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.DyeColor;
import org.bukkit.material.Colorable;
import org.bukkit.material.MaterialData;

import java.util.Objects;

public class SpigotBlockTypeDyeColored extends SpigotBlockType implements WSBlockTypeDyeColored {

	private EnumDyeColor dyeColor;

	public SpigotBlockTypeDyeColored(int numericalId, String oldStringId, String newStringId, int maxStackSize, EnumDyeColor dyeColor) {
		super(numericalId, oldStringId, newStringId.startsWith("minecraft:") ? newStringId.substring(10) : newStringId, maxStackSize);
		Validate.notNull(dyeColor, "Dye color cannot be null!");
		this.dyeColor = dyeColor;
	}

	@Override
	public String getNewStringId() {
		return "minecraft:" + dyeColor.getMinecraftName().toLowerCase() + "_" + super.getNewStringId();
	}

	@Override
	public EnumDyeColor getDyeColor() {
		return dyeColor;
	}

	@Override
	public void setDyeColor(EnumDyeColor dyeColor) {
		Validate.notNull(dyeColor, "Dye color cannot be null!");
		this.dyeColor = dyeColor;
	}

	@Override
	public SpigotBlockTypeDyeColored clone() {
		return new SpigotBlockTypeDyeColored(getNumericalId(), getOldStringId(), super.getNewStringId(), getMaxStackSize(), dyeColor);
	}

	@Override
	public MaterialData toMaterialData() {
		MaterialData data = super.toMaterialData();
		if (data instanceof Colorable) {
			((Colorable) data).setColor(DyeColor.getByDyeData(dyeColor.getDyeData()));
		} else data.setData(dyeColor.getWoolData());
		return data;
	}

	@Override
	public SpigotBlockTypeDyeColored readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		if (materialData instanceof Colorable) {
			dyeColor = EnumDyeColor.getByDyeData(((Colorable) materialData).getColor().getDyeData()).orElse(EnumDyeColor.WHITE);
		} else dyeColor = EnumDyeColor.getByWoolData(materialData.getData()).orElse(EnumDyeColor.WHITE);
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeDyeColored that = (SpigotBlockTypeDyeColored) o;
		return dyeColor == that.dyeColor;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), dyeColor);
	}
}

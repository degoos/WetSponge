package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.SpigotBlockTypeMultipleFacing;
import org.bukkit.material.MaterialData;
import org.bukkit.material.Tripwire;

import java.util.Objects;
import java.util.Set;

public class SpigotBlockTypeTripwire extends SpigotBlockTypeMultipleFacing implements WSBlockTypeTripwire {

	private boolean disarmed, attached, powered;

	public SpigotBlockTypeTripwire(Set<EnumBlockFace> faces, Set<EnumBlockFace> allowedFaces, boolean disarmed, boolean attached, boolean powered) {
		super(132, "minecraft:tripwire", "minecraft:tripwire", 64, faces, allowedFaces);
		this.disarmed = disarmed;
		this.attached = attached;
		this.powered = powered;
	}

	@Override
	public boolean isDisarmed() {
		return disarmed;
	}

	@Override
	public void setDisarmed(boolean disarmed) {
		this.disarmed = disarmed;
	}

	@Override
	public boolean isAttached() {
		return attached;
	}

	@Override
	public void setAttached(boolean attached) {
		this.attached = attached;
	}

	@Override
	public boolean isPowered() {
		return powered;
	}

	@Override
	public void setPowered(boolean powered) {
		this.powered = powered;
	}

	@Override
	public SpigotBlockTypeTripwire clone() {
		return new SpigotBlockTypeTripwire(getFaces(), getAllowedFaces(), disarmed, attached, powered);
	}

	@Override
	public MaterialData toMaterialData() {
		MaterialData data = super.toMaterialData();
		if (data instanceof Tripwire) {
			((Tripwire) data).setActivated(attached);
			((Tripwire) data).setObjectTriggering(powered);
		}
		return data;
	}

	@Override
	public SpigotBlockTypeTripwire readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		if (materialData instanceof Tripwire) {
			attached = ((Tripwire) materialData).isActivated();
			powered = ((Tripwire) materialData).isObjectTriggering();
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeTripwire that = (SpigotBlockTypeTripwire) o;
		return disarmed == that.disarmed &&
				attached == that.attached &&
				powered == that.powered;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), disarmed, attached, powered);
	}
}

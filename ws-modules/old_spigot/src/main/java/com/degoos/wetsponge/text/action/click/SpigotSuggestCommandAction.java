package com.degoos.wetsponge.text.action.click;

import net.md_5.bungee.api.chat.ClickEvent;

public class SpigotSuggestCommandAction extends SpigotClickAction implements WSSuggestCommandAction {

    public SpigotSuggestCommandAction(ClickEvent event) {
        super(event);
    }

    public SpigotSuggestCommandAction(String command) {
        super(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, command));
    }

    @Override
    public String getCommand() {
        return getHandled().getValue();
    }
}

package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumWoodType;
import com.degoos.wetsponge.material.block.SpigotBlockType;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.material.MaterialData;

import java.util.Objects;

public class SpigotBlockTypeWoodPlanks extends SpigotBlockType implements WSBlockTypeWoodPlanks {

	private EnumWoodType woodType;

	public SpigotBlockTypeWoodPlanks(EnumWoodType woodType) {
		super(5, "minecraft:planks", "minecraft:planks", 64);
		Validate.notNull(woodType, "Wood type cannot be null!");
		this.woodType = woodType;
	}

	@Override
	public String getNewStringId() {
		switch (getWoodType()) {
			case SPRUCE:
				return "minecraft:spruce_planks";
			case BIRCH:
				return "minecraft:birch_planks";
			case JUNGLE:
				return "minecraft:jungle_planks";
			case ACACIA:
				return "minecraft:acacia_planks";
			case DARK_OAK:
				return "minecraft:dark_oak_planks";
			case OAK:
			default:
				return "minecraft:oak_planks";
		}
	}

	@Override
	public EnumWoodType getWoodType() {
		return woodType;
	}

	@Override
	public void setWoodType(EnumWoodType woodType) {
		Validate.notNull(woodType, "Wood type cannot be null");
		this.woodType = woodType;
	}

	@Override
	public SpigotBlockTypeWoodPlanks clone() {
		return new SpigotBlockTypeWoodPlanks(woodType);
	}

	@Override
	public MaterialData toMaterialData() {
		MaterialData data = super.toMaterialData();
		data.setData((byte) woodType.getValue());
		return data;
	}

	@Override
	public SpigotBlockTypeWoodPlanks readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		woodType = EnumWoodType.getByValue(materialData.getData()).orElse(EnumWoodType.OAK);
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeWoodPlanks that = (SpigotBlockTypeWoodPlanks) o;
		return woodType == that.woodType;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), woodType);
	}
}

package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockTypePottedPlant;
import com.degoos.wetsponge.material.block.SpigotBlockType;
import org.bukkit.material.MaterialData;

import java.util.Objects;

public class SpigotBlockTypeFlowerPot extends SpigotBlockType implements WSBlockTypeFlowerPot {

	private EnumBlockTypePottedPlant pottedPlant;

	public SpigotBlockTypeFlowerPot(EnumBlockTypePottedPlant pottedPlant) {
		super(140, "minecraft:flower_pot", "minecraft:flower_pot", 64);
		this.pottedPlant = pottedPlant == null ? EnumBlockTypePottedPlant.NONE : pottedPlant;
	}

	@Override
	public String getNewStringId() {
		return pottedPlant == EnumBlockTypePottedPlant.NONE ? "minecraft:flower_pot" : "minecraft:potted_" + pottedPlant.name().toLowerCase();
	}

	@Override
	public EnumBlockTypePottedPlant getPottedPlant() {
		return pottedPlant;
	}

	@Override
	public void setPottedPlant(EnumBlockTypePottedPlant pottedPlant) {
		this.pottedPlant = pottedPlant == null ? EnumBlockTypePottedPlant.NONE : pottedPlant;
	}

	@Override
	public SpigotBlockTypeFlowerPot clone() {
		return new SpigotBlockTypeFlowerPot(pottedPlant);
	}

	@Override
	public MaterialData toMaterialData() {
		MaterialData data = super.toMaterialData();
		data.setData((byte) Math.max(0, pottedPlant.getValue()));
		return data;
	}

	@Override
	public SpigotBlockTypeFlowerPot readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		pottedPlant = EnumBlockTypePottedPlant.getByValue(materialData.getData()).orElse(EnumBlockTypePottedPlant.NONE);
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeFlowerPot that = (SpigotBlockTypeFlowerPot) o;
		return pottedPlant == that.pottedPlant;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), pottedPlant);
	}
}

package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockTypePrismarineType;
import com.degoos.wetsponge.material.block.SpigotBlockType;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.material.MaterialData;

import java.util.Objects;

public class SpigotBlockTypePrismarine extends SpigotBlockType implements WSBlockTypePrismarine {

	private EnumBlockTypePrismarineType prismarineType;

	public SpigotBlockTypePrismarine(EnumBlockTypePrismarineType prismarineType) {
		super(168, "minecraft:prismarine", "minecraft:prismarine", 64);
		Validate.notNull(prismarineType, "Prismarine type cannot be null!");
		this.prismarineType = prismarineType;
	}

	@Override
	public String getNewStringId() {
		switch (prismarineType) {
			case DARK:
				return "minecraft:dark_prismarine";
			case BRICKS:
				return "minecraft:prismarine_bricks";
			case ROUGH:
			default:
				return "minecraft:prismarine";
		}
	}

	@Override
	public EnumBlockTypePrismarineType getPrismarineType() {
		return prismarineType;
	}

	@Override
	public void setPrismarineType(EnumBlockTypePrismarineType prismarineType) {
		Validate.notNull(prismarineType, "Prismarine type cannot be null!");
		this.prismarineType = prismarineType;
	}

	@Override
	public SpigotBlockTypePrismarine clone() {
		return new SpigotBlockTypePrismarine(prismarineType);
	}

	@Override
	public MaterialData toMaterialData() {
		MaterialData data = super.toMaterialData();
		data.setData((byte) prismarineType.getValue());
		return data;
	}

	@Override
	public SpigotBlockTypePrismarine readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		prismarineType = EnumBlockTypePrismarineType.getByValue(materialData.getData()).orElse(EnumBlockTypePrismarineType.ROUGH);
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypePrismarine that = (SpigotBlockTypePrismarine) o;
		return prismarineType == that.prismarineType;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), prismarineType);
	}
}

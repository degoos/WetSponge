package com.degoos.wetsponge.nbt;


import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;
import java.lang.reflect.InvocationTargetException;

public class SpigotNBTTagInt extends SpigotNBTBase implements WSNBTTagInt {

	private static final Class<?> clazz = NMSUtils.getNMSClass("NBTTagInt");

	public SpigotNBTTagInt(int i) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
		this(clazz.getConstructor(int.class).newInstance(i));
	}

	public SpigotNBTTagInt(Object object) {
		super(object);
	}

	private int getValue() {
		try {
			return ReflectionUtils.getFirstObject(clazz, int.class, getHandled());
		} catch (Exception e) {
			InternalLogger.printException(e, "An error has occurred while WetSponge was getting the value of a NBTTag!");
			return 0;
		}
	}

	@Override
	public long getLong() {
		return getValue();
	}

	@Override
	public int getInt() {
		return getValue();
	}

	@Override
	public short getShort() {
		return (short) getValue();
	}

	@Override
	public byte getByte() {
		return (byte) (getValue() & 255);
	}

	@Override
	public double getDouble() {
		return getValue();
	}

	@Override
	public float getFloat() {
		return getValue();
	}

	@Override
	public WSNBTTagInt copy() {
		try {
			return new SpigotNBTTagInt(getValue());
		} catch (Exception e) {
			InternalLogger.printException(e, "An error has occurred while WetSponge was copying a NBTTag!");
			return null;
		}
	}

	@Override
	public Object getHandled() {
		return super.getHandled();
	}
}

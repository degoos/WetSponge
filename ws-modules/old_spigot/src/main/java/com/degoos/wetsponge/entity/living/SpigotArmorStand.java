package com.degoos.wetsponge.entity.living;

import com.degoos.wetsponge.enums.EnumEquipType;
import com.degoos.wetsponge.item.SpigotItemStack;
import com.degoos.wetsponge.item.WSItemStack;
import com.flowpowered.math.vector.Vector3d;
import java.util.Optional;
import org.bukkit.entity.ArmorStand;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.EulerAngle;

public class SpigotArmorStand extends SpigotLivingEntity implements WSArmorStand {


	public SpigotArmorStand(ArmorStand entity) {
		super(entity);
	}

	@Override
	public Vector3d getHeadDirection() {
		EulerAngle angle = getHandled().getHeadPose();
		return new Vector3d(angle.getX(), angle.getY(), angle.getZ()).mul(180).div(Math.PI);
	}

	@Override
	public void setHeadDirection(Vector3d headDirection) {
		headDirection = headDirection.mul(Math.PI).div(180);
		getHandled().setHeadPose(new EulerAngle(headDirection.getX(), headDirection.getY(), headDirection.getZ()));
	}

	@Override
	public Vector3d getBodyRotation() {
		EulerAngle angle = getHandled().getBodyPose();
		return new Vector3d(angle.getX(), angle.getY(), angle.getZ()).mul(180).div(Math.PI);
	}

	@Override
	public void setBodyRotation(Vector3d bodyRotation) {
		bodyRotation = bodyRotation.mul(Math.PI).div(180);
		getHandled().setBodyPose(new EulerAngle(bodyRotation.getX(), bodyRotation.getY(), bodyRotation.getZ()));
	}

	@Override
	public Vector3d getLeftArmDirection() {
		EulerAngle angle = getHandled().getLeftArmPose();
		return new Vector3d(angle.getX(), angle.getY(), angle.getZ()).mul(180).div(Math.PI);
	}

	@Override
	public void setLeftArmDirection(Vector3d leftArmDirection) {
		leftArmDirection = leftArmDirection.mul(Math.PI).div(180);
		getHandled().setLeftArmPose(new EulerAngle(leftArmDirection.getX(), leftArmDirection.getY(), leftArmDirection.getZ()));
	}

	@Override
	public Vector3d getRightArmDirection() {
		EulerAngle angle = getHandled().getRightArmPose();
		return new Vector3d(angle.getX(), angle.getY(), angle.getZ()).mul(180).div(Math.PI);
	}

	@Override
	public void setRightArmDirection(Vector3d rightArmDirection) {
		rightArmDirection = rightArmDirection.mul(Math.PI).div(180);
		getHandled().setRightArmPose(new EulerAngle(rightArmDirection.getX(), rightArmDirection.getY(), rightArmDirection.getZ()));
	}

	@Override
	public Vector3d getLeftLegDirection() {
		EulerAngle angle = getHandled().getLeftLegPose();
		return new Vector3d(angle.getX(), angle.getY(), angle.getZ()).mul(180).div(Math.PI);
	}

	@Override
	public void setLeftLegDirection(Vector3d leftLegDirection) {
		leftLegDirection = leftLegDirection.mul(Math.PI).div(180);
		getHandled().setLeftLegPose(new EulerAngle(leftLegDirection.getX(), leftLegDirection.getY(), leftLegDirection.getZ()));
	}

	@Override
	public Vector3d getRightLegDirection() {
		EulerAngle angle = getHandled().getRightLegPose();
		return new Vector3d(angle.getX(), angle.getY(), angle.getZ()).mul(180).div(Math.PI);
	}

	@Override
	public void setRightLegDirection(Vector3d rightLegDirection) {
		rightLegDirection = rightLegDirection.mul(Math.PI).div(180);
		getHandled().setRightLegPose(new EulerAngle(rightLegDirection.getX(), rightLegDirection.getY(), rightLegDirection.getZ()));
	}

	@Override
	public boolean isMarker() {
		return getHandled().isMarker();
	}

	@Override
	public void setMarker(boolean marker) {
		getHandled().setMarker(marker);
	}

	@Override
	public boolean isSmall() {
		return getHandled().isSmall();
	}

	@Override
	public void setSmall(boolean small) {
		getHandled().setSmall(small);
	}

	@Override
	public boolean hasBasePlate() {
		return getHandled().hasBasePlate();
	}

	@Override
	public void setBasePlate(boolean basePlate) {
		getHandled().setBasePlate(basePlate);
	}

	@Override
	public boolean hasArms() {
		return getHandled().hasArms();
	}

	@Override
	public void setArms(boolean arms) {
		getHandled().setArms(arms);
	}

	@Override
	public boolean hasGravity() {
		return getHandled().hasGravity();
	}

	@Override
	public void setGravity(boolean gravity) {
		getHandled().setGravity(gravity);
	}

	@Override
	public Optional<WSItemStack> getEquippedItem(EnumEquipType type) {
		ItemStack itemStack = null;
		switch (type) {
			case HELMET:
				itemStack = getHandled().getEquipment().getHelmet();
				break;
			case CHESTPLATE:
				itemStack = getHandled().getEquipment().getChestplate();
				break;
			case LEGGINGS:
				itemStack = getHandled().getEquipment().getLeggings();
				break;
			case BOOTS:
				itemStack = getHandled().getEquipment().getBoots();
				break;
			case MAIN_HAND:
				itemStack = getHandled().getEquipment().getItemInMainHand();
				break;
			case OFF_HAND:
				itemStack = getHandled().getEquipment().getItemInOffHand();
				break;
		}
		return Optional.ofNullable(itemStack).map(SpigotItemStack::new);
	}


	@Override
	public void setEquippedItem(EnumEquipType type, WSItemStack itemStack) {
		switch (type) {
			case HELMET:
				getHandled().getEquipment().setHelmet(itemStack == null ? null : ((SpigotItemStack) itemStack).getHandled().clone());
				break;
			case CHESTPLATE:
				getHandled().getEquipment().setChestplate(itemStack == null ? null : ((SpigotItemStack) itemStack).getHandled().clone());
				break;
			case LEGGINGS:
				getHandled().getEquipment().setLeggings(itemStack == null ? null : ((SpigotItemStack) itemStack).getHandled().clone());
				break;
			case BOOTS:
				getHandled().getEquipment().setBoots(itemStack == null ? null : ((SpigotItemStack) itemStack).getHandled().clone());
				break;
			case MAIN_HAND:
				getHandled().getEquipment().setItemInMainHand(itemStack == null ? null : ((SpigotItemStack) itemStack).getHandled().clone());
				break;
			case OFF_HAND:
				getHandled().getEquipment().setItemInOffHand(itemStack == null ? null : ((SpigotItemStack) itemStack).getHandled().clone());
				break;
		}
	}


	@Override
	public ArmorStand getHandled() {
		return (ArmorStand) super.getHandled();
	}
}

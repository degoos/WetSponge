package com.degoos.wetsponge.merchant;

import com.degoos.wetsponge.item.WSItemStack;
import com.degoos.wetsponge.util.Validate;

import java.util.Optional;

public class OldSpigotTrade implements WSTrade {

	private int uses, maxUses;
	private WSItemStack result, firstItem, secondItem;
	private boolean grantExperience;

	public OldSpigotTrade(int uses, int maxUses, WSItemStack result, WSItemStack firstItem, WSItemStack secondItem, boolean grantExperience) {
		Validate.notNull(result, "Result cannot be null!");
		Validate.notNull(firstItem, "First item cannot be null!");
		this.uses = uses;
		this.maxUses = maxUses;
		this.result = result;
		this.firstItem = firstItem;
		this.secondItem = secondItem;
		this.grantExperience = grantExperience;
	}

	@Override
	public int getUses() {
		return uses;
	}

	@Override
	public int getMaxUses() {
		return maxUses;
	}

	@Override
	public WSItemStack getResult() {
		return result.clone();
	}

	@Override
	public WSItemStack getFirstItem() {
		return firstItem.clone();
	}

	@Override
	public Optional<WSItemStack> getSecondItem() {
		return Optional.ofNullable(secondItem).map(WSItemStack::clone);
	}

	@Override
	public boolean doesGrantExperience() {
		return grantExperience;
	}

	@Override
	public Object getHandled() {
		return null;
	}

	public static class Builder implements WSTrade.Builder {

		private int uses, maxUses;
		private WSItemStack result, firstItem, secondItem;
		boolean grantExperience;


		@Override
		public WSTrade.Builder uses(int uses) {
			this.uses = uses;
			return this;
		}

		@Override
		public WSTrade.Builder maxUses(int maxUses) {
			this.maxUses = maxUses;
			return this;
		}

		@Override
		public WSTrade.Builder result(WSItemStack result) {
			Validate.notNull(result, "Result cannot be null!");
			this.result = result;
			return this;
		}

		@Override
		public WSTrade.Builder firstItem(WSItemStack firstItem) {
			Validate.notNull(firstItem, "First item cannot be null!");
			this.firstItem = firstItem;
			return this;
		}

		@Override
		public WSTrade.Builder secondItem(WSItemStack secondItem) {
			this.secondItem = secondItem == null || secondItem.getMaterial().getNumericalId() == 0 ? null : secondItem;
			return this;
		}

		@Override
		public WSTrade.Builder canGrantExperience(boolean grantExperience) {
			this.grantExperience = grantExperience;
			return this;
		}

		@Override
		public WSTrade build() {
			Validate.notNull(result, "Result cannot be null!");
			Validate.notNull(firstItem, "First item cannot be null!");
			return new OldSpigotTrade(uses, maxUses, result, firstItem, secondItem, grantExperience);
		}
	}
}

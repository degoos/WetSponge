package com.degoos.wetsponge.listener.spigot;


import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.entity.living.WSLivingEntity;
import com.degoos.wetsponge.entity.living.player.SpigotPlayer;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.event.entity.player.connection.WSPlayerReceivePacketEvent;
import com.degoos.wetsponge.event.entity.player.connection.WSPlayerSendPacketEvent;
import com.degoos.wetsponge.material.block.WSBlockType;
import com.degoos.wetsponge.packet.WSPacket;
import com.degoos.wetsponge.packet.play.server.*;
import com.degoos.wetsponge.parser.entity.SpigotEntityParser;
import com.degoos.wetsponge.parser.packet.SpigotPacketParser;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;
import com.degoos.wetsponge.world.WSLocation;
import com.flowpowered.math.vector.Vector2i;
import io.netty.channel.Channel;
import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;

public class SpigotPacketListener extends ChannelDuplexHandler {

	private WSPlayer player;


	public SpigotPacketListener(WSPlayer player) {
		this.player = player;
	}


	@Override
	public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
		try {
			WSPacket finalPacket = SpigotPacketParser.parse(msg);
			WSPacket packet = finalPacket;

			if (packet instanceof WSSPacketBlockChange) {
				player.getFakeBlock(WSLocation.of(player.getWorld(), ((WSSPacketBlockChange) packet).getBlockPosition()))
						.ifPresent(((WSSPacketBlockChange) packet)::setMaterial);
			}
			if (packet instanceof WSSPacketMultiBlockChange) {
				Vector2i chunk = ((WSSPacketMultiBlockChange) packet).getChunkPosition();
				Map<WSLocation, WSBlockType> fakeBlocks = player.getFakeBlocks();
				if (!fakeBlocks.isEmpty() && fakeBlocks.keySet().stream()
						.anyMatch(target -> chunk.getX() == target.getBlockX() >> 4 && chunk.getY() == target.getBlockZ() >> 4))
					new HashMap<>(((WSSPacketMultiBlockChange) packet).getMaterials()).forEach((vector3i, type) -> {
						WSLocation location = WSLocation.of(player.getWorld(), vector3i.add(chunk.getX() * 16, 0, chunk.getY() * 16));
						player.getFakeBlock(location).ifPresent(blockType -> ((WSSPacketMultiBlockChange) finalPacket).addMaterial(location.toVector3i(), blockType));
					});
			}
			if (packet instanceof WSSPacketSpawnMob) {
				WSLivingEntity entity = ((WSSPacketSpawnMob) packet).getEntity().orElse(null);
				if (entity == null) {
					WSEntity target = SpigotEntityParser.getWSEntity(((WSSPacketSpawnMob) packet).getUniqueId()).orElse(null);
					if (target != null && target instanceof WSLivingEntity) entity = (WSLivingEntity) target;
				}
				if (entity != null && !(entity instanceof WSPlayer && player.getName().equals(((WSPlayer) entity).getName())) && entity.hasDisguise())
					((WSSPacketSpawnMob) packet).setDisguise(entity.getDisguise().get());
			}
			if (packet instanceof WSSPacketSpawnPlayer) {
				WSPlayer player = WetSponge.getServer().getPlayer(((WSSPacketSpawnPlayer) packet).getUniqueId()).orElse(null);
				if (player != null && player.hasDisguise() && !this.player.getName().equals(player.getName())) {
					packet = WSSPacketSpawnMob.of(player.getDisguise().get(), ((WSSPacketSpawnPlayer) packet).getPosition(), player.getVelocity(), player.getRotation()
							.toVector2(), (float) player.getHeadRotation().getY());
					((WSSPacketSpawnMob) packet).setEntityId(player.getEntityId());
					((WSSPacketSpawnMob) packet).setUniqueId(player.getUniqueId());
				}
			}
			if (packet instanceof WSSPacketEntityMetadata) {
				WSLivingEntity entity = null;
				WSEntity target = SpigotEntityParser.getWSEntity(((WSSPacketEntityMetadata) packet).getEntityId()).orElse(null);
				if (target != null && target instanceof WSLivingEntity) entity = (WSLivingEntity) target;
				if (entity != null && !(entity instanceof WSPlayer && player.getName().equals(((WSPlayer) entity).getName())) && entity.hasDisguise())
					((WSSPacketEntityMetadata) packet).setMetadataOf(entity.getDisguise().get());
			}
			if (packet instanceof WSSPacketEntityProperties) {
				WSLivingEntity entity = null;
				WSEntity target = SpigotEntityParser.getWSEntity(((WSSPacketEntityProperties) packet).getEntityId()).orElse(null);
				if (target != null && target instanceof WSLivingEntity) entity = (WSLivingEntity) target;
				if (entity != null && !(entity instanceof WSPlayer && player.getName().equals(((WSPlayer) entity).getName())) && entity.hasDisguise())
					((WSSPacketEntityProperties) packet).setPropertiesOf(entity.getDisguise().get());
			}
			if (packet instanceof WSSPacketMaps) {
				player.getWorld().getMapView(((WSSPacketMaps) packet).getMapId()).ifPresent(((WSSPacketMaps) packet)::setMapView);
			}

			WSPlayerSendPacketEvent event = new WSPlayerSendPacketEvent(player, packet);
			WetSponge.getEventManager().callEvent(event);
			if (event.isCancelled()) return;
			if (event.getPacket().hasChanged()) event.getPacket().update();

			super.write(ctx, event.getPacket().getHandler(), promise);
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was writing a Spigot packet!");
		}
	}


	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
		try {
			WSPacket packet = SpigotPacketParser.parse(msg);
			WSPlayerReceivePacketEvent event = new WSPlayerReceivePacketEvent(player, packet);
			WetSponge.getEventManager().callEvent(event);
			if (event.isCancelled()) return;
			if (event.getPacket().hasChanged()) event.getPacket().update();

			super.channelRead(ctx, event.getPacket().getHandler());
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was reading a Spigot packet!");
		}
	}


	public static void inject(WSPlayer player) {
		new Thread(() -> {
			Player p = ((SpigotPlayer) player).getHandled();
			try {
				Object craftHandle = ReflectionUtils.invokeMethod(p, p.getClass(), "getHandle");
				Object playerCon = ReflectionUtils.getObject(craftHandle, "playerConnection");
				Object manager = ReflectionUtils.getObject(playerCon, "networkManager");
				Channel channel = ReflectionUtils.getFirstObject(manager.getClass(), Channel.class, manager);

				if (channel.pipeline().context("WetSponge") != null) channel.pipeline().remove("WetSponge");

				channel.pipeline().addBefore("packet_handler", "WetSponge", new SpigotPacketListener(player));
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}).start();
	}


	public static void uninject(WSPlayer player) {
		new Thread(() -> {
			Player p = ((SpigotPlayer) player).getHandled();
			try {
				Object craftHandle = ReflectionUtils.invokeMethod(p, p.getClass(), "getHandle");
				Object playerCon = ReflectionUtils.getObject(craftHandle, "playerConnection");
				Object manager = ReflectionUtils.getObject(playerCon, "networkManager");
				Channel channel = (Channel) ReflectionUtils.getFirstObject(manager.getClass(), Channel.class, manager);

				if (channel.pipeline().context("WetSponge") != null) channel.pipeline().remove("WetSponge");
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}).start();
	}

}

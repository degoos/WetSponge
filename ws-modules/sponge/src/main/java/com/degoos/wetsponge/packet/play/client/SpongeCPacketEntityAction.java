package com.degoos.wetsponge.packet.play.client;

import com.degoos.wetsponge.enums.EnumEntityAction;
import com.degoos.wetsponge.packet.SpongePacket;
import java.lang.reflect.Field;
import java.util.Arrays;
import net.minecraft.network.Packet;
import net.minecraft.network.play.client.CPacketEntityAction;

public class SpongeCPacketEntityAction extends SpongePacket implements WSCPacketEntityAction {

	private boolean changed;
	private int entityId, jumpBoost;
	private EnumEntityAction entityAction;

	public SpongeCPacketEntityAction(int entityId, EnumEntityAction entityAction, int jumpBoost) {
		super(new CPacketEntityAction());
		this.entityId = entityId;
		this.entityAction = entityAction;
		this.jumpBoost = jumpBoost;
		update();
	}

	public SpongeCPacketEntityAction(Packet<?> packet) {
		super(packet);
		refresh();
	}

	@Override
	public void update() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));
			fields[0].setInt(getHandler(), entityId);
			fields[2].setInt(getHandler(), jumpBoost);
			if (entityAction != null) fields[1].set(getHandler(), CPacketEntityAction.Action.valueOf(entityAction.name()));
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void refresh() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));
			entityId = fields[0].getInt(getHandler());
			entityAction = EnumEntityAction.getById(((CPacketEntityAction.Action) fields[1].get(getHandler())).ordinal()).orElse(null);
			jumpBoost = fields[2].getInt(getHandler());
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public boolean hasChanged() {
		return changed;
	}

	@Override
	public int getEntityId() {
		return entityId;
	}

	@Override
	public void setEntityId(int entityId) {
		this.entityId = entityId;
		changed = true;
	}

	@Override
	public EnumEntityAction getAction() {
		return entityAction;
	}

	@Override
	public void setAction(EnumEntityAction entityAction) {
		this.entityAction = entityAction;
		changed = true;
	}

	@Override
	public int getJumpBoost() {
		return jumpBoost;
	}

	@Override
	public void setJumpBoost(int jumpBoost) {
		this.jumpBoost = jumpBoost;
		changed = true;
	}
}

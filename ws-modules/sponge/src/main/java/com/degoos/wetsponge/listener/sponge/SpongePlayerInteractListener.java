package com.degoos.wetsponge.listener.sponge;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.block.SpongeBlockSnapshot;
import com.degoos.wetsponge.block.WSBlockSnapshot;
import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.enums.EnumTristate;
import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.event.entity.player.interact.WSPlayerInteractBlockEvent;
import com.degoos.wetsponge.event.entity.player.interact.WSPlayerInteractEntityEvent;
import com.degoos.wetsponge.event.entity.player.interact.WSPlayerInteractEvent;
import com.degoos.wetsponge.event.entity.player.interact.WSPlayerInteractItemEvent;
import com.degoos.wetsponge.item.SpongeItemStack;
import com.degoos.wetsponge.item.WSItemStack;
import com.degoos.wetsponge.parser.entity.SpongeEntityParser;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.world.SpongeLocation;
import com.degoos.wetsponge.world.WSLocation;
import com.flowpowered.math.vector.Vector3d;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.Order;
import org.spongepowered.api.event.action.InteractEvent;
import org.spongepowered.api.event.block.InteractBlockEvent;
import org.spongepowered.api.event.entity.InteractEntityEvent;
import org.spongepowered.api.event.filter.cause.First;
import org.spongepowered.api.event.item.inventory.InteractItemEvent;
import org.spongepowered.api.util.Tristate;

import java.util.Optional;

public class SpongePlayerInteractListener {

	@Listener(order = Order.FIRST)
	public void onBlockInteraction(InteractEvent event, @First Player spongePlayer) {
		try {
			WSPlayerInteractEvent wetSpongeEvent;
			WSPlayer player = WetSponge.getServer().getPlayer(spongePlayer.getUniqueId()).orElse(null);
			Optional<Vector3d> position = event.getInteractionPoint();
			if (event instanceof InteractBlockEvent) {
				Optional<WSLocation> blockLocation = ((InteractBlockEvent) event).getTargetBlock().getLocation().map(SpongeLocation::new);
				WSBlockSnapshot block = new SpongeBlockSnapshot(((InteractBlockEvent) event).getTargetBlock());
				EnumBlockFace direction = EnumBlockFace.getBySpongeName(((InteractBlockEvent) event).getTargetSide().name()).orElse(EnumBlockFace.SELF);
				if (event instanceof InteractBlockEvent.Primary) {
					if (event instanceof InteractBlockEvent.Primary.MainHand)
						wetSpongeEvent = new WSPlayerInteractBlockEvent.Primary.MainHand(player, block, direction, position, blockLocation);
					else wetSpongeEvent = new WSPlayerInteractBlockEvent.Primary.OffHand(player, block, direction, position, blockLocation);
				} else {
					if (event instanceof InteractBlockEvent.Secondary.MainHand)
						wetSpongeEvent = new WSPlayerInteractBlockEvent.Secondary.MainHand(player, block, direction, position, blockLocation, transform((
								(InteractBlockEvent.Secondary) event)
								.getUseBlockResult()), transform(((InteractBlockEvent.Secondary) event).getUseItemResult()), transform(((InteractBlockEvent.Secondary) event)
								.getOriginalUseBlockResult()), transform(((InteractBlockEvent.Secondary) event).getOriginalUseItemResult()));
					else
						wetSpongeEvent = new WSPlayerInteractBlockEvent.Secondary.OffHand(player, block, direction, position, blockLocation, transform((
								(InteractBlockEvent.Secondary) event)
								.getUseBlockResult()), transform(((InteractBlockEvent.Secondary) event).getUseItemResult()), transform(((InteractBlockEvent.Secondary) event)
								.getOriginalUseBlockResult()), transform(((InteractBlockEvent.Secondary) event).getOriginalUseItemResult()));
				}
				WetSponge.getEventManager().callEvent(wetSpongeEvent);
				if (wetSpongeEvent instanceof WSPlayerInteractBlockEvent.Secondary) {
					((InteractBlockEvent.Secondary) event).setUseBlockResult(transform(((WSPlayerInteractBlockEvent.Secondary) wetSpongeEvent).getUseBlockResult()));
					((InteractBlockEvent.Secondary) event).setUseItemResult(transform(((WSPlayerInteractBlockEvent.Secondary) wetSpongeEvent).getUseItemResult()));
				}
			} else if (event instanceof InteractEntityEvent) {
				WSEntity entity = SpongeEntityParser.getWSEntity(((InteractEntityEvent) event).getTargetEntity());
				if (event instanceof InteractEntityEvent.Primary) {
					if (event instanceof InteractEntityEvent.Primary.MainHand)
						wetSpongeEvent = new WSPlayerInteractEntityEvent.Primary.MainHand(player, entity, position);
					else wetSpongeEvent = new WSPlayerInteractEntityEvent.Primary.OffHand(player, entity, position);
				} else {
					if (event instanceof InteractEntityEvent.Secondary.MainHand)
						wetSpongeEvent = new WSPlayerInteractEntityEvent.Secondary.MainHand(player, entity, position);
					else wetSpongeEvent = new WSPlayerInteractEntityEvent.Secondary.OffHand(player, entity, position);
				}
				WetSponge.getEventManager().callEvent(wetSpongeEvent);
			} else if (event instanceof InteractItemEvent) {
				WSItemStack itemStack = new SpongeItemStack(((InteractItemEvent) event).getItemStack().createStack());
				if (event instanceof InteractItemEvent.Primary) {
					if (event instanceof InteractItemEvent.Primary.MainHand) wetSpongeEvent = new WSPlayerInteractItemEvent.Primary.MainHand(player, itemStack);
					else wetSpongeEvent = new WSPlayerInteractItemEvent.Primary.OffHand(player, itemStack);
				} else {
					if (event instanceof InteractItemEvent.Secondary.MainHand) wetSpongeEvent = new WSPlayerInteractItemEvent.Secondary.MainHand(player, itemStack);
					else wetSpongeEvent = new WSPlayerInteractItemEvent.Secondary.OffHand(player, itemStack);
				}
				WetSponge.getEventManager().callEvent(wetSpongeEvent);
			} else {
				wetSpongeEvent = new WSPlayerInteractEvent(player);
				WetSponge.getEventManager().callEvent(wetSpongeEvent);
			}
			event.setCancelled(wetSpongeEvent.isCancelled());
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Sponge-InteractEvent!");
		}
	}

	private Tristate transform(EnumTristate result) {
		switch (result) {
			case FALSE:
				return Tristate.FALSE;
			case TRUE:
				return Tristate.TRUE;
			case UNDEFINED:
			default:
				return Tristate.UNDEFINED;
		}
	}

	private EnumTristate transform(Tristate result) {
		switch (result) {
			case TRUE:
				return EnumTristate.TRUE;
			case FALSE:
				return EnumTristate.FALSE;
			case UNDEFINED:
			default:
				return EnumTristate.UNDEFINED;
		}
	}

}

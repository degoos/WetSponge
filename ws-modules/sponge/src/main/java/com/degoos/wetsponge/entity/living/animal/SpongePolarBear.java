package com.degoos.wetsponge.entity.living.animal;


import org.spongepowered.api.entity.living.animal.PolarBear;

public class SpongePolarBear extends SpongeAnimal implements WSPolarBear {


	public SpongePolarBear (PolarBear entity) {
		super(entity);
	}


	@Override
	public PolarBear getHandled () {
		return (PolarBear) super.getHandled();
	}

}

package com.degoos.wetsponge.entity.living.aquatic;

import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.entity.living.SpongeCreature;
import com.degoos.wetsponge.entity.living.SpongeLivingEntity;
import org.spongepowered.api.entity.living.Squid;

import java.util.Optional;

public class SpongeSquid extends SpongeCreature implements WSSquid {


    public SpongeSquid(Squid entity) {
        super(entity);
    }


    @Override
    public Squid getHandled() {
        return (Squid) super.getHandled();
    }

}

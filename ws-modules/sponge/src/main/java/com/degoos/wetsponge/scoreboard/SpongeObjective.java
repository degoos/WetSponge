package com.degoos.wetsponge.scoreboard;

import com.degoos.wetsponge.enums.EnumCriteria;
import com.degoos.wetsponge.text.SpongeText;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.Validate;
import org.spongepowered.api.scoreboard.objective.Objective;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class SpongeObjective implements WSObjective {

    private Objective objective;

    public SpongeObjective(Objective objective) {
        Validate.notNull(objective, "Objective cannot be null!");
        this.objective = objective;
    }

    @Override
    public String getName() {
        return objective.getName();
    }

    @Override
    public WSText getDisplayName() {
        return SpongeText.of(objective.getDisplayName());
    }

    @Override
    public void setDisplayName(WSText text) {
        objective.setDisplayName(((SpongeText) text).getHandled());
    }

    @Override
    public EnumCriteria getCriteria() {
        return EnumCriteria.getBySpongeName(objective.getCriterion().getName()).orElse(EnumCriteria.DUMMY);
    }

    @Override
    public Map<WSText, WSScore> getScores() {
        Map<WSText, WSScore> map = new HashMap<>();
        objective.getScores().forEach((spongeName, spongeScore) -> {
            WSText name = SpongeText.of(spongeName);
            map.put(name, new SpongeScore(spongeScore));
        });
        return map;
    }

    @Override
    public boolean hasScore(WSText name) {
        return objective.hasScore(((SpongeText) name).getHandled());
    }

    @Override
    public WSScore getOrCreateScore(WSText name) {
        return new SpongeScore(objective.getOrCreateScore(((SpongeText) name).getHandled()));
    }

    @Override
    public boolean removeScore(WSText name) {
        return objective.removeScore(((SpongeText) name).getHandled());
    }

    @Override
    public boolean removeScore(WSScore score) {
        return objective.removeScore(((SpongeScore) score).getHandled());
    }

    @Override
    public void removeAllScoresWithScore(int score) {
        try {
            getScores().values().stream().filter(target -> target.getScore() == score).forEach(this::removeScore);
        } catch (Exception ex) {
            InternalLogger.printException(ex, "An error has occurred while WetSponge was removing all objectives with the score "+score+"!");
        }
    }

    @Override
    public Optional<WSScoreboard> getScoreboard() {
        return objective.getScoreboards().size() == 0 ? Optional.empty() : objective.getScoreboards().stream().findFirst().map(SpongeScoreboard::new);
    }

    @Override
    public Objective getHandled() {
        return objective;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SpongeObjective that = (SpongeObjective) o;

        return objective.equals(that.objective);
    }

    @Override
    public int hashCode() {
        return objective.hashCode();
    }
}

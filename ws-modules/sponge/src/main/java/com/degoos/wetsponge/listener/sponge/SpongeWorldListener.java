package com.degoos.wetsponge.listener.sponge;


import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.event.world.WSChunkLoadEvent;
import com.degoos.wetsponge.event.world.WSChunkUnloadEvent;
import com.degoos.wetsponge.parser.world.WorldParser;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.world.SpongeChunk;
import com.degoos.wetsponge.world.SpongeLocation;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.Order;
import org.spongepowered.api.event.world.LoadWorldEvent;
import org.spongepowered.api.event.world.UnloadWorldEvent;
import org.spongepowered.api.event.world.chunk.LoadChunkEvent;
import org.spongepowered.api.event.world.chunk.UnloadChunkEvent;
import org.spongepowered.api.world.World;

public class SpongeWorldListener {

	private static Map<String, World> worlds = new HashMap<>();


	public SpongeWorldListener() {
		Sponge.getServer().getWorlds().forEach(world -> worlds.put(world.getName().toLowerCase(), world));
	}


	public static Optional<World> getWorld(String name) {
		return worlds.keySet().stream().filter(worldName -> worldName.equalsIgnoreCase(name)).findAny().map(s -> worlds.get(s));
	}


	@Listener(order = Order.FIRST)
	public void load(LoadWorldEvent event) {
		try {
			worlds.put(event.getTargetWorld().getName().toLowerCase(), event.getTargetWorld());
			if (SpongeLocation.locations.containsKey(event.getTargetWorld().getName()))
				SpongeLocation.locations.get(event.getTargetWorld().getName()).forEach(SpongeLocation::updateWorld);
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Sponge-LoadWorldEvent!");
		}
	}


	@Listener(order = Order.FIRST)
	public void unload(UnloadWorldEvent e) {
		try {
			worlds.remove(e.getTargetWorld().getName().toLowerCase());
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Sponge-UnloadWorldEvent!");
		}
	}


	@Listener(order = Order.FIRST)
	public void onLoadChunk(LoadChunkEvent event) {
		WetSponge.getEventManager().callEvent(new WSChunkLoadEvent(WorldParser
			.getOrCreateWorld(event.getTargetChunk().getWorld().getName(), event.getTargetChunk().getWorld()), new SpongeChunk(event.getTargetChunk())));
	}

	//UNLOAD IN MIXIN.
}

package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockTypeTallGrassType;
import com.degoos.wetsponge.material.block.SpongeBlockType;
import com.degoos.wetsponge.util.Validate;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.ShrubType;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.Objects;

public class SpongeBlockTypeTallGrassType extends SpongeBlockType implements WSBlockTypeTallGrass {

    private EnumBlockTypeTallGrassType tallGrassType;

    public SpongeBlockTypeTallGrassType(EnumBlockTypeTallGrassType tallGrassType) {
        super(31, "minecraft:tallgrass", "minecraft:grass", 64);
        Validate.notNull(tallGrassType, "Tall grass type cannot be null!");
        this.tallGrassType = tallGrassType;
    }

    @Override
    public String getNewStringId() {
        switch (tallGrassType) {
            case FERN:
                return "minecraft:fern";
            case DEAD_BUSH:
                return "minecraft:dead_bush";
            case TALL_GRASS:
            default:
                return "minecraft:grass";
        }
    }

    @Override
    public EnumBlockTypeTallGrassType getTallGrassType() {
        return tallGrassType;
    }

    @Override
    public void setTallGrassType(EnumBlockTypeTallGrassType tallGrassType) {
        Validate.notNull(tallGrassType, "Tall grass type cannot be null!");
        this.tallGrassType = tallGrassType;
    }

    @Override
    public SpongeBlockTypeTallGrassType clone() {
        return new SpongeBlockTypeTallGrassType(tallGrassType);
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        super.writeItemStack(itemStack);
        itemStack.offer(Keys.SHRUB_TYPE, Sponge.getRegistry().getType(ShrubType.class, tallGrassType.name()).orElseThrow(NullPointerException::new));
        return itemStack;
    }

    @Override
    public BlockState writeBlockState(BlockState blockState) {
        blockState = super.writeBlockState(blockState);
        return blockState.with(Keys.SHRUB_TYPE, Sponge.getRegistry().getType(ShrubType.class, tallGrassType.name()).orElseThrow(NullPointerException::new)).orElse(blockState);
    }

    @Override
    public SpongeBlockTypeTallGrassType readContainer(ValueContainer<?> valueContainer) {
        super.readContainer(valueContainer);
        tallGrassType = EnumBlockTypeTallGrassType.getByName(valueContainer.get(Keys.SHRUB_TYPE).get().getName()).orElse(EnumBlockTypeTallGrassType.DEAD_BUSH);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeTallGrassType that = (SpongeBlockTypeTallGrassType) o;
        return tallGrassType == that.tallGrassType;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), tallGrassType);
    }
}

package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockTypeStoneBrickType;
import com.degoos.wetsponge.material.block.SpongeBlockType;
import com.degoos.wetsponge.util.Validate;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.BrickType;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.Objects;

public class SpongeBlockTypeStoneBrick extends SpongeBlockType implements WSBlockTypeStoneBrick {

    private EnumBlockTypeStoneBrickType stoneBrickType;

    public SpongeBlockTypeStoneBrick(EnumBlockTypeStoneBrickType stoneBrickType) {
        super(98, "minecraft:stonebrick", "minecraft:stone_bricks", 64);
        Validate.notNull(stoneBrickType, "Stone brick type cannot be null!");
        this.stoneBrickType = stoneBrickType;
    }

    @Override
    public String getNewStringId() {
        switch (stoneBrickType) {
            case MOSSY:
                return "minecraft:mossy_stone_bricks";
            case CRACKED:
                return "minecraft:cracked_stone_bricks";
            case CHISELED:
                return "minecraft:chiseled_stone_bricks";
            case DEFAULT:
            default:
                return "minecraft:stone_bricks";
        }
    }

    @Override
    public EnumBlockTypeStoneBrickType getStoneBrickType() {
        return stoneBrickType;
    }

    @Override
    public void setStoneBrickType(EnumBlockTypeStoneBrickType stoneBrickType) {
        Validate.notNull(stoneBrickType, "Stone brick type cannot be null!");
        this.stoneBrickType = stoneBrickType;
    }

    @Override
    public SpongeBlockTypeStoneBrick clone() {
        return new SpongeBlockTypeStoneBrick(stoneBrickType);
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        super.writeItemStack(itemStack);
        itemStack.offer(Keys.BRICK_TYPE, Sponge.getRegistry().getType(BrickType.class, stoneBrickType.name()).orElseThrow(NullPointerException::new));
        return itemStack;
    }

    @Override
    public BlockState writeBlockState(BlockState blockState) {
        blockState = super.writeBlockState(blockState);
        return blockState.with(Keys.BRICK_TYPE, Sponge.getRegistry().getType(BrickType.class, stoneBrickType.name())
                .orElseThrow(NullPointerException::new)).orElse(blockState);
    }

    @Override
    public SpongeBlockTypeStoneBrick readContainer(ValueContainer<?> valueContainer) {
        super.readContainer(valueContainer);
        stoneBrickType = EnumBlockTypeStoneBrickType.getByName(valueContainer.get(Keys.BRICK_TYPE).get().getName()).orElseThrow(NullPointerException::new);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeStoneBrick that = (SpongeBlockTypeStoneBrick) o;
        return stoneBrickType == that.stoneBrickType;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), stoneBrickType);
    }
}

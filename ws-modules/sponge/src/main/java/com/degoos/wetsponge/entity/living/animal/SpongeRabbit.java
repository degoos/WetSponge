package com.degoos.wetsponge.entity.living.animal;


import com.degoos.wetsponge.enums.EnumRabbitType;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.RabbitType;
import org.spongepowered.api.data.type.RabbitTypes;
import org.spongepowered.api.entity.living.animal.Rabbit;

import java.util.Optional;

public class SpongeRabbit extends SpongeAnimal implements WSRabbit {


	public SpongeRabbit (Rabbit entity) {
		super(entity);
	}


	@Override
	public Optional<EnumRabbitType> getRabbitType () {
		return EnumRabbitType.getRabbitType(getHandled().variant().get().getName());
	}


	@Override
	public void setRabbitType (EnumRabbitType rabbitType) {
		getHandled().offer(Keys.RABBIT_TYPE, getRabbitType(rabbitType));
	}


	@Override
	public Rabbit getHandled () {
		return (Rabbit) super.getHandled();
	}


	private RabbitType getRabbitType (EnumRabbitType rabbitType) {
		switch (rabbitType) {
			case BROWN:
				return RabbitTypes.BROWN;
			case WHITE:
				return RabbitTypes.WHITE;
			case BLACK:
				return RabbitTypes.BLACK;
			case BLACK_AND_WHITE:
				return RabbitTypes.BLACK_AND_WHITE;
			case GOLD:
				return RabbitTypes.GOLD;
			case SALT_AND_PEPPER:
				return RabbitTypes.SALT_AND_PEPPER;
			case THE_KILLER_BUNNY:
				return RabbitTypes.KILLER;
			default:
				return RabbitTypes.BROWN;
		}
	}
}

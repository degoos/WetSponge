package com.degoos.wetsponge.material;

import com.degoos.wetsponge.enums.EnumDyeColor;
import com.degoos.wetsponge.enums.EnumInstrument;
import com.degoos.wetsponge.enums.block.*;
import com.degoos.wetsponge.material.block.*;
import com.degoos.wetsponge.material.block.type.*;

import java.util.Map;
import java.util.Set;

public class SpongeBlockConverter {

    @SuppressWarnings("unchecked")
    public static WSBlockType createWSBlockType(int numericalId, String oldStringId, String newStringId, int maxStackSize,
                                                Class<? extends WSBlockType> materialClass, Object[] extra) {

        if (materialClass.equals(WSBlockType.class))
            return new SpongeBlockType(numericalId, oldStringId, newStringId, maxStackSize);
        if (materialClass.equals(WSBlockTypeAgeable.class))
            return new SpongeBlockTypeAgeable(numericalId, oldStringId, newStringId, maxStackSize, (int) extra[0], (int) extra[1]);
        if (materialClass.equals(WSBlockTypeAnaloguePowerable.class))
            return new SpongeBlockTypeAnaloguePowerable(numericalId, oldStringId, newStringId, maxStackSize, (int) extra[0], (int) extra[1]);
        if (materialClass.equals(WSBlockTypeAttachable.class))
            return new SpongeBlockTypeAttachable(numericalId, oldStringId, newStringId, maxStackSize, (boolean) extra[0]);
        if (materialClass.equals(WSBlockTypeBisected.class))
            return new SpongeBlockTypeBisected(numericalId, oldStringId, newStringId, maxStackSize, (EnumBlockTypeBisectedHalf) extra[0]);
        if (materialClass.equals(WSBlockTypeDirectional.class))
            return new SpongeBlockTypeDirectional(numericalId, oldStringId, newStringId, maxStackSize, (EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1]);
        if (materialClass.equals(WSBlockTypeDyeColored.class))
            return new SpongeBlockTypeDyeColored(numericalId, oldStringId, newStringId, maxStackSize, (EnumDyeColor) extra[0]);
        if (materialClass.equals(WSBlockTypeLevelled.class))
            return new SpongeBlockTypeLevelled(numericalId, oldStringId, newStringId, maxStackSize, (int) extra[0], (int) extra[1]);
        if (materialClass.equals(WSBlockTypeLightable.class))
            return new SpongeBlockTypeLightable(numericalId, oldStringId, newStringId, maxStackSize, (boolean) extra[0]);
        if (materialClass.equals(WSBlockTypeMultipleFacing.class))
            return new SpongeBlockTypeMultipleFacing(numericalId, oldStringId, newStringId, maxStackSize, (Set<EnumBlockFace>) extra[0], (Set<EnumBlockFace>) extra[1]);
        if (materialClass.equals(WSBlockTypeOpenable.class))
            return new SpongeBlockTypeOpenable(numericalId, oldStringId, newStringId, maxStackSize, (boolean) extra[0]);
        if (materialClass.equals(WSBlockTypeOrientable.class))
            return new SpongeBlockTypeOrientable(numericalId, oldStringId, newStringId, maxStackSize, (EnumAxis) extra[0], (Set<EnumAxis>) extra[1]);
        if (materialClass.equals(WSBlockTypePowerable.class))
            return new SpongeBlockTypePowerable(numericalId, oldStringId, newStringId, maxStackSize, (boolean) extra[0]);
        if (materialClass.equals(WSBlockTypeRail.class))
            return new SpongeBlockTypeRail(numericalId, oldStringId, newStringId, maxStackSize, (EnumBlockTypeRailShape) extra[0], (Set<EnumBlockTypeRailShape>) extra[1]);
        if (materialClass.equals(WSBlockTypeRotatable.class))
            return new SpongeBlockTypeRotatable(numericalId, oldStringId, newStringId, maxStackSize, (EnumBlockFace) extra[0]);
        if (materialClass.equals(WSBlockTypeSnowable.class))
            return new SpongeBlockTypeSnowable(numericalId, oldStringId, newStringId, maxStackSize, (boolean) extra[0]);
        if (materialClass.equals(WSBlockTypeWaterlogged.class))
            return new SpongeBlockTypeWaterlogged(numericalId, oldStringId, newStringId, maxStackSize, (boolean) extra[0]);

        if (materialClass.equals(WSBlockTypeAnvil.class))
            return new SpongeBlockTypeAnvil((EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (EnumBlockTypeAnvilDamage) extra[2]);
        if (materialClass.equals(WSBlockTypeBed.class))
            return new SpongeBlockTypeBed((EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (EnumBlockTypeBedPart) extra[2], (boolean) extra[3], (EnumDyeColor) extra[4]);
        if (materialClass.equals(WSBlockTypeBrewingStand.class))
            return new SpongeBlockTypeBrewingStand((Set<Integer>) extra[0], (int) extra[1]);
        if (materialClass.equals(WSBlockTypeBubbleColumn.class))
            return new SpongeBlockTypeBubbleColumn((boolean) extra[0]);
        if (materialClass.equals(WSBlockTypeCake.class))
            return new SpongeBlockTypeCake((int) extra[0], (int) extra[1]);
        if (materialClass.equals(WSBlockTypeChest.class))
            return new SpongeBlockTypeChest(numericalId, oldStringId, newStringId, maxStackSize, (EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (EnumBlockTypeChestType) extra[2], (boolean) extra[3]);
        if (materialClass.equals(WSBlockTypeCobblestoneWall.class))
            return new SpongeBlockTypeCobblestoneWall((Set<EnumBlockFace>) extra[0], (Set<EnumBlockFace>) extra[1], (boolean) extra[2], (boolean) extra[3]);
        if (materialClass.equals(WSBlockTypeCocoa.class))
            return new SpongeBlockTypeCocoa((EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (int) extra[2], (int) extra[3]);
        if (materialClass.equals(WSBlockTypeCommandBlock.class))
            return new SpongeBlockTypeCommandBlock(numericalId, oldStringId, newStringId, maxStackSize, (EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (boolean) extra[2]);
        if (materialClass.equals(WSBlockTypeComparator.class))
            return new SpongeBlockTypeComparator((EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (EnumBlockTypeComparatorMode) extra[2], (boolean) extra[3]);
        if (materialClass.equals(WSBlockTypeCoralWallFan.class))
            return new SpongeBlockTypeCoralWallFan(numericalId, oldStringId, newStringId, maxStackSize, (EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (boolean) extra[2]);
        if (materialClass.equals(WSBlockTypeDaylightDetector.class))
            return new SpongeBlockTypeDaylightDetector((int) extra[0], (int) extra[1], (boolean) extra[2]);
        if (materialClass.equals(WSBlockTypeDirt.class))
            return new SpongeBlockTypeDirt((EnumBlockTypeDirtType) extra[0]);
        if (materialClass.equals(WSBlockTypeDispenser.class))
            return new SpongeBlockTypeDispenser(numericalId, oldStringId, newStringId, maxStackSize, (EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (boolean) extra[2]);
        if (materialClass.equals(WSBlockTypeDoor.class))
            return new SpongeBlockTypeDoor(numericalId, oldStringId, newStringId, maxStackSize, (EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (EnumBlockTypeDoorHinge) extra[2], (EnumBlockTypeBisectedHalf) extra[3], (boolean) extra[4], (boolean) extra[5]);
        if (materialClass.equals(WSBlockTypeDoublePlant.class))
            return new SpongeBlockTypeDoublePlant((EnumBlockTypeBisectedHalf) extra[0], (EnumBlockTypeDoublePlantType) extra[1]);
        if (materialClass.equals(WSBlockTypeEnderchest.class))
            return new SpongeBlockTypeEnderchest((EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (boolean) extra[2]);
        if (materialClass.equals(WSBlockTypeEndPortalFrame.class))
            return new SpongeBlockTypeEndPortalFrame((EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (boolean) extra[2]);
        if (materialClass.equals(WSBlockTypeFarmland.class))
            return new SpongeBlockTypeFarmland((int) extra[0], (int) extra[1]);
        if (materialClass.equals(WSBlockTypeFence.class))
            return new SpongeBlockTypeFence(numericalId, oldStringId, newStringId, maxStackSize, (Set<EnumBlockFace>) extra[0], (Set<EnumBlockFace>) extra[1], (boolean) extra[2]);
        if (materialClass.equals(WSBlockTypeFire.class))
            return new SpongeBlockTypeFire((Set<EnumBlockFace>) extra[0], (Set<EnumBlockFace>) extra[1], (int) extra[2], (int) extra[3]);
        if (materialClass.equals(WSBlockTypeFlower.class))
            return new SpongeBlockTypeFlower((EnumBlockTypeFlowerType) extra[0]);
        if (materialClass.equals(WSBlockTypeFlowerPot.class))
            return new SpongeBlockTypeFlowerPot((EnumBlockTypePottedPlant) extra[0]);
        if (materialClass.equals(WSBlockTypeFurnace.class))
            return new SpongeBlockTypeFurnace((EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (boolean) extra[2]);
        if (materialClass.equals(WSBlockTypeGate.class))
            return new SpongeBlockTypeGate(numericalId, oldStringId, newStringId, maxStackSize, (EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (boolean) extra[2], (boolean) extra[3], (boolean) extra[4]);
        if (materialClass.equals(WSBlockTypeGlassPane.class))
            return new SpongeBlockTypeGlassPane(numericalId, oldStringId, newStringId, maxStackSize, (Set<EnumBlockFace>) extra[0], (Set<EnumBlockFace>) extra[1], (boolean) extra[2]);
        if (materialClass.equals(WSBlockTypeHopper.class))
            return new SpongeBlockTypeHopper((EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (boolean) extra[2]);
        if (materialClass.equals(WSBlockTypeInfestedStone.class))
            return new SpongeBlockTypeInfestedStone((EnumBlockTypeDisguiseType) extra[0]);
        if (materialClass.equals(WSBlockTypeJukebox.class))
            return new SpongeBlockTypeJukebox((boolean) extra[0]);
        if (materialClass.equals(WSBlockTypeLadder.class))
            return new SpongeBlockTypeLadder((EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (boolean) extra[2]);
        if (materialClass.equals(WSBlockTypeLava.class))
            return new SpongeBlockTypeLava((int) extra[0], (int) extra[1]);
        if (materialClass.equals(WSBlockTypeLeaves.class))
            return new SpongeBlockTypeLeaves((boolean) extra[0], (int) extra[1], (EnumWoodType) extra[2]);
        if (materialClass.equals(WSBlockTypeLog.class))
            return new SpongeBlockTypeLog((EnumAxis) extra[0], (Set<EnumAxis>) extra[1], (EnumWoodType) extra[2], (boolean) extra[3], (boolean) extra[4]);
        if (materialClass.equals(WSBlockTypeNoteBlock.class))
            return new SpongeBlockTypeNoteBlock((boolean) extra[0], (EnumInstrument) extra[1], (int) extra[2]);
        if (materialClass.equals(WSBlockTypeObserver.class))
            return new SpongeBlockTypeObserver((EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (boolean) extra[2]);
        if (materialClass.equals(WSBlockTypePiston.class))
            return new SpongeBlockTypePiston(numericalId, oldStringId, newStringId, maxStackSize, (EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (boolean) extra[2]);
        if (materialClass.equals(WSBlockTypePistonHead.class))
            return new SpongeBlockTypePistonHead((EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (EnumBlockTypePistonType) extra[2], (boolean) extra[3]);
        if (materialClass.equals(WSBlockTypePrismarine.class))
            return new SpongeBlockTypePrismarine((EnumBlockTypePrismarineType) extra[0]);
        if (materialClass.equals(WSBlockTypeQuartz.class))
            return new SpongeBlockTypeQuartz((EnumAxis) extra[0], (Set<EnumAxis>) extra[1], (EnumBlockTypeQuartzType) extra[2]);
        if (materialClass.equals(WSBlockTypeRedstoneLamp.class))
            return new SpongeBlockTypeRedstoneLamp((boolean) extra[0]);
        if (materialClass.equals(WSBlockTypeRedstoneOre.class))
            return new SpongeBlockTypeRedstoneOre((boolean) extra[0]);
        if (materialClass.equals(WSBlockTypeRedstoneRail.class))
            return new SpongeBlockTypeRedstoneRail(numericalId, oldStringId, newStringId, maxStackSize, (EnumBlockTypeRailShape) extra[0], (Set<EnumBlockTypeRailShape>) extra[1], (boolean) extra[2]);
        if (materialClass.equals(WSBlockTypeRedstoneTorch.class))
            return new SpongeBlockTypeRedstoneTorch((EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (boolean) extra[2]);
        if (materialClass.equals(WSBlockTypeRedstoneWire.class))
            return new SpongeBlockTypeRedstoneWire((int) extra[0], (int) extra[1], (Map<EnumBlockFace, EnumBlockTypeRedstoneWireConnection>) extra[2], (Set<EnumBlockFace>) extra[3]);
        if (materialClass.equals(WSBlockTypeRepeater.class))
            return new SpongeBlockTypeRepeater((EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (int) extra[2], (int) extra[3], (int) extra[4], (boolean) extra[5], (boolean) extra[6]);
        if (materialClass.equals(WSBlockTypeSand.class))
            return new SpongeBlockTypeSand((EnumBlockTypeSandType) extra[0]);
        if (materialClass.equals(WSBlockTypeSandstone.class))
            return new SpongeBlockTypeSandstone((EnumBlockTypeSandType) extra[0], (EnumBlockTypeSandstoneType) extra[1]);
        if (materialClass.equals(WSBlockTypeSapling.class))
            return new SpongeBlockTypeSapling((EnumWoodType) extra[0], (int) extra[1], (int) extra[2]);
        if (materialClass.equals(WSBlockTypeSeaPickle.class))
            return new SpongeBlockTypeSeaPickle((boolean) extra[0], (int) extra[1], (int) extra[2], (int) extra[3]);
        if (materialClass.equals(WSBlockTypeSign.class))
            return new SpongeBlockTypeSign((EnumBlockFace) extra[0], (boolean) extra[1]);
        if (materialClass.equals(WSBlockTypeSkull.class))
            return new SpongeBlockTypeSkull((EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (EnumBlockFace) extra[2], (EnumBlockTypeSkullType) extra[3]);
        if (materialClass.equals(WSBlockTypeSlab.class))
            return new SpongeBlockTypeSlab((boolean) extra[0], (EnumBlockTypeSlabType) extra[1], (EnumBlockTypeSlabPosition) extra[2]);
        if (materialClass.equals(WSBlockTypeSnow.class))
            return new SpongeBlockTypeSnow((int) extra[0], (int) extra[1], (int) extra[2]);
        if (materialClass.equals(WSBlockTypeSponge.class))
            return new SpongeBlockTypeSponge((boolean) extra[0]);
        if (materialClass.equals(WSBlockTypeStainedGlassPane.class))
            return new SpongeBlockTypeStainedGlassPane((Set<EnumBlockFace>) extra[0], (Set<EnumBlockFace>) extra[1], (boolean) extra[2], (EnumDyeColor) extra[3]);
        if (materialClass.equals(WSBlockTypeStairs.class))
            return new SpongeBlockTypeStairs(numericalId, oldStringId, newStringId, maxStackSize, (EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (EnumBlockTypeStairShape) extra[2], (EnumBlockTypeBisectedHalf) extra[3], (boolean) extra[4]);
        if (materialClass.equals(WSBlockTypeStone.class))
            return new SpongeBlockTypeStone((EnumBlockTypeStoneType) extra[0]);
        if (materialClass.equals(WSBlockTypeStoneBrick.class))
            return new SpongeBlockTypeStoneBrick((EnumBlockTypeStoneBrickType) extra[0]);
        if (materialClass.equals(WSBlockTypeStructureBlock.class))
            return new SpongeBlockTypeStructureBlock((EnumBlockTypeStructureBlockMode) extra[0]);
        if (materialClass.equals(WSBlockTypeSwitch.class))
            return new SpongeBlockTypeSwitch(numericalId, oldStringId, newStringId, maxStackSize, (EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (EnumBlockTypeSwitchFace) extra[2], (boolean) extra[3]);
        if (materialClass.equals(WSBlockTypeTallGrass.class))
            return new SpongeBlockTypeTallGrassType((EnumBlockTypeTallGrassType) extra[0]);
        if (materialClass.equals(WSBlockTypeTechnicalPiston.class))
            return new SpongeBlockTypeTechnicalPiston(numericalId, oldStringId, newStringId, maxStackSize, (EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (EnumBlockTypePistonType) extra[2]);
        if (materialClass.equals(WSBlockTypeTorch.class))
            return new SpongeBlockTypeTorch(numericalId, oldStringId, newStringId, maxStackSize, (String) extra[0], (EnumBlockFace) extra[1], (Set<EnumBlockFace>) extra[2]);
        if (materialClass.equals(WSBlockTypeTrapDoor.class))
            return new SpongeBlockTypeTrapDoor(numericalId, oldStringId, newStringId, maxStackSize, (EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (EnumBlockTypeBisectedHalf) extra[2], (boolean) extra[3], (boolean) extra[4], (boolean) extra[5]);
        if (materialClass.equals(WSBlockTypeTripwire.class))
            return new SpongeBlockTypeTripwire((Set<EnumBlockFace>) extra[0], (Set<EnumBlockFace>) extra[1], (boolean) extra[2], (boolean) extra[3], (boolean) extra[4]);
        if (materialClass.equals(WSBlockTypeTripwireHook.class))
            return new SpongeBlockTypeTripwireHook((EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (boolean) extra[2], (boolean) extra[3]);
        if (materialClass.equals(WSBlockTypeTurtleEgg.class))
            return new SpongeBlockTypeTurtleEgg((int) extra[0], (int) extra[1], (int) extra[2], (int) extra[3], (int) extra[4]);
        if (materialClass.equals(WSBlockTypeWallSign.class))
            return new SpongeBlockTypeWallSign((EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (boolean) extra[2]);
        if (materialClass.equals(WSBlockTypeWater.class))
            return new SpongeBlockTypeWater((int) extra[0], (int) extra[1]);
        if (materialClass.equals(WSBlockTypeWoodPlanks.class))
            return new SpongeBlockTypeWoodPlanks((EnumWoodType) extra[0]);

        return new SpongeBlockType(numericalId, oldStringId, newStringId, maxStackSize);
    }

}

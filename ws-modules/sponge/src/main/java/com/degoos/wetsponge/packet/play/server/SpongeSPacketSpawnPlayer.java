package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.entity.SpongeEntity;
import com.degoos.wetsponge.entity.living.WSLivingEntity;
import com.degoos.wetsponge.entity.living.player.SpongeHuman;
import com.degoos.wetsponge.entity.living.player.SpongePlayer;
import com.degoos.wetsponge.entity.living.player.WSHuman;
import com.degoos.wetsponge.packet.SpongePacket;
import com.degoos.wetsponge.util.Validate;
import com.flowpowered.math.vector.Vector2d;
import com.flowpowered.math.vector.Vector3d;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.SPacketSpawnPlayer;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Optional;
import java.util.UUID;

public class SpongeSPacketSpawnPlayer extends SpongePacket implements WSSPacketSpawnPlayer {

	private Optional<WSLivingEntity> entity;
	private int entityId;
	private UUID uniqueId;
	private Vector3d position;
	private Vector2d rotation;
	private boolean changed;

	public SpongeSPacketSpawnPlayer(WSHuman entity) {
		super(new SPacketSpawnPlayer((EntityPlayer) ((SpongePlayer) entity).getHandled()));
		this.entity = Optional.ofNullable(entity);
		refresh();
	}

	public SpongeSPacketSpawnPlayer(WSHuman entity, Vector3d position, Vector2d rotation) {
		super(new SPacketSpawnPlayer());
		updateEntity(entity);
		this.position = position;
		this.rotation = rotation;
		update();
	}

	public SpongeSPacketSpawnPlayer(Packet<?> packet) {
		super(packet);
		this.entity = Optional.empty();
		refresh();
	}

	public Optional<WSLivingEntity> getEntity() {
		return entity;
	}

	public void setEntity(WSHuman entity) {
		Validate.notNull(entity, "Entity cannot be null!");
		updateEntity(entity);
	}

	public int getEntityId() {
		return entityId;
	}

	public void setEntityId(int entityId) {
		changed = true;
		this.entityId = entityId;
	}

	public UUID getUniqueId() {
		return uniqueId;
	}

	public void setUniqueId(UUID uniqueId) {
		changed = true;
		this.uniqueId = uniqueId;
	}

	public Vector3d getPosition() {
		return position;
	}

	public void setPosition(Vector3d position) {
		changed = true;
		this.position = position;
	}

	public Vector2d getRotation() {
		return rotation;
	}

	public void setRotation(Vector2d rotation) {
		changed = true;
		this.rotation = rotation;
	}

	private void updateEntity(WSHuman entity) {
		EntityPlayer livingBase = (EntityPlayer) ((SpongeHuman) entity).getHandled();
		this.entity = Optional.ofNullable(entity);
		this.entityId = livingBase.getEntityId();
		this.uniqueId = livingBase.getUniqueID();
	}

	@Override
	public void update() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));
			fields[0].setInt(getHandler(), entityId);
			fields[1].set(getHandler(), uniqueId);

			fields[2].setDouble(getHandler(), position.getX());
			fields[3].setDouble(getHandler(), position.getY());
			fields[4].setDouble(getHandler(), position.getZ());

			fields[5].setByte(getHandler(), (byte) ((int) (rotation.getX() * 256.0F / 360.0F)));
			fields[6].setByte(getHandler(), (byte) ((int) (rotation.getY() * 256.0F / 360.0F)));

			if (entity.isPresent())
				fields[7].set(getHandler(), ((EntityLivingBase) ((SpongeEntity) entity.get()).getHandled()).getDataManager());

		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void refresh() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));
			entityId = fields[0].getInt(getHandler());
			uniqueId = (UUID) fields[1].get(getHandler());
			position = new Vector3d(fields[2].getDouble(getHandler()), fields[3].getDouble(getHandler()), fields[4].getDouble(getHandler()));
			rotation = new Vector2d(fields[5].getByte(getHandler()) * 360.0D / 256.0D, fields[6].getByte(getHandler()) * 360.0D / 256.0D);
			entity = Optional.empty();
		} catch (Throwable ex) {
			ex.printStackTrace();
		}

	}

	@Override
	public boolean hasChanged() {
		return changed;
	}

}

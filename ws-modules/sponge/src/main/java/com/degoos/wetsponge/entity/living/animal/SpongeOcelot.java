package com.degoos.wetsponge.entity.living.animal;

import com.degoos.wetsponge.enums.EnumOcelotType;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.OcelotType;
import org.spongepowered.api.data.type.OcelotTypes;
import org.spongepowered.api.entity.living.animal.Ocelot;

import java.util.Optional;
import java.util.UUID;

public class SpongeOcelot extends SpongeAnimal implements WSOcelot {


    public SpongeOcelot(Ocelot entity) {
        super(entity);
    }

    @Override
    public EnumOcelotType getOcelotType() {
        return EnumOcelotType.getByName(getHandled().variant().get().getName()).orElse(EnumOcelotType.WILD_OCELOT);
    }

    @Override
    public void setOcelotType(EnumOcelotType ocelotType) {
        getHandled().offer(Keys.OCELOT_TYPE, Sponge.getRegistry().getType(OcelotType.class, ocelotType.name()).orElse(OcelotTypes.WILD_OCELOT));
    }

    @Override
    public boolean isSitting() {
        return getHandled().get(Keys.IS_SITTING).orElse(false);
    }

    @Override
    public void setSitting(boolean sitting) {
        getHandled().offer(Keys.IS_SITTING, sitting);
    }

    @Override
    public boolean isTamed() {
        return getHandled().get(Keys.TAMED_OWNER).orElse(Optional.empty()).isPresent();
    }

    @Override
    public Optional<UUID> getTamer() {
        return getHandled().get(Keys.TAMED_OWNER).orElse(Optional.empty());
    }

    @Override
    public void setTamer(UUID tamer) {
        getHandled().offer(Keys.TAMED_OWNER, Optional.ofNullable(tamer));
    }

    @Override
    public Ocelot getHandled() {
        return (Ocelot) super.getHandled();
    }
}

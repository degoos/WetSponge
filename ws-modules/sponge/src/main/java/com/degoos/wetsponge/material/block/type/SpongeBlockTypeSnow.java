package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.material.block.SpongeBlockType;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.Objects;

public class SpongeBlockTypeSnow extends SpongeBlockType implements WSBlockTypeSnow {

    private int layers, minimumLayers, maximumLayers;

    public SpongeBlockTypeSnow(int layers, int minimumLayers, int maximumLayers) {
        super(78, "minecraft:snow_layer", "minecraft:snow", 64);
        this.layers = layers;
        this.minimumLayers = minimumLayers;
        this.maximumLayers = maximumLayers;
    }

    @Override
    public int getLayers() {
        return layers;
    }

    @Override
    public void setLayers(int layers) {
        layers = Math.max(minimumLayers, Math.min(maximumLayers, layers));
    }

    @Override
    public int getMinimumLayers() {
        return minimumLayers;
    }

    @Override
    public int getMaximumLayers() {
        return maximumLayers;
    }

    @Override
    public SpongeBlockTypeSnow clone() {
        return new SpongeBlockTypeSnow(layers, minimumLayers, maximumLayers);
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        super.writeItemStack(itemStack);
        itemStack.offer(Keys.LAYER, layers);
        return itemStack;
    }

    @Override
    public BlockState writeBlockState(BlockState blockState) {
        blockState = super.writeBlockState(blockState);
        return blockState.with(Keys.LAYER, layers).orElse(blockState);
    }

    @Override
    public SpongeBlockTypeSnow readContainer(ValueContainer<?> valueContainer) {
        super.readContainer(valueContainer);
        layers = valueContainer.get(Keys.LAYER).orElse(1);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeSnow that = (SpongeBlockTypeSnow) o;
        return layers == that.layers &&
                minimumLayers == that.minimumLayers &&
                maximumLayers == that.maximumLayers;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), layers, minimumLayers, maximumLayers);
    }
}

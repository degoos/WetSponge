package com.degoos.wetsponge.entity.living.golem;

import com.degoos.wetsponge.entity.living.SpongeCreature;
import com.degoos.wetsponge.enums.EnumDyeColor;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;
import org.spongepowered.api.entity.living.golem.Shulker;

public class SpongeShulker extends SpongeCreature implements WSShulker {

	public SpongeShulker(Shulker entity) {
		super(entity);
	}

	@Override
	public EnumDyeColor getDyeColor() {
		return EnumDyeColor.getByDyeData(writeToNBTTagCompound(WSNBTTagCompound.of()).getByte("Color")).orElse(EnumDyeColor.WHITE);
	}

	@Override
	public void setDyeColor(EnumDyeColor color) {
		WSNBTTagCompound compound = writeToNBTTagCompound(WSNBTTagCompound.of());
		compound.setByte("Color", color.getWoolData());
		readFromNBTTagCompound(compound);
	}

	@Override
	public Shulker getHandled() {
		return (Shulker) super.getHandled();
	}
}

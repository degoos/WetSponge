package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.enums.EnumGameMode;
import com.degoos.wetsponge.enums.EnumPlayerListItemAction;
import com.degoos.wetsponge.packet.SpongePacket;
import com.degoos.wetsponge.packet.play.server.extra.WSPlayerListItemData;
import com.degoos.wetsponge.text.SpongeText;
import com.degoos.wetsponge.user.SpongeGameProfile;
import com.degoos.wetsponge.util.ListUtils;
import com.mojang.authlib.GameProfile;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.SPacketPlayerListItem;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.world.GameType;
import org.spongepowered.api.text.Text;
import org.spongepowered.common.text.SpongeTexts;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class SpongeSPacketPlayerListItem extends SpongePacket implements WSSPacketPlayerListItem {

	private boolean changed;
	private EnumPlayerListItemAction action;
	private List<WSPlayerListItemData> data;
	private Class<?> nestedClass = SPacketPlayerListItem.class.getDeclaredClasses()[0];

	public SpongeSPacketPlayerListItem(EnumPlayerListItemAction action, Collection<WSPlayerListItemData> itemData) {
		super(new SPacketPlayerListItem());
		changed = false;
		this.action = action;
		this.data = new ArrayList<>(itemData);
		update();
	}

	public SpongeSPacketPlayerListItem(EnumPlayerListItemAction action, WSPlayerListItemData... itemData) {
		super(new SPacketPlayerListItem());
		changed = false;
		this.action = action;
		this.data = ListUtils.toList(itemData);
		update();
	}

	public SpongeSPacketPlayerListItem(Packet<?> packet) {
		super(packet);
		changed = false;
		refresh();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void update() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));

			fields[0].set(getHandler(), SPacketPlayerListItem.Action.valueOf(action.name()));


			List list = (List) fields[1].get(getHandler());
			list.clear();

			Constructor constructor = nestedClass.getConstructor(SPacketPlayerListItem.class, GameProfile.class, int.class, GameType.class, ITextComponent.class);

			for (WSPlayerListItemData itemData : data) {

				list.add(constructor.newInstance((SPacketPlayerListItem) getHandler(), (GameProfile) itemData.getGameProfile().getHandled(), itemData.getPing(),
						GameType.valueOf(itemData.getGameMode() == null ? "NOT_SET" : itemData.getGameMode().name()),
						(ITextComponent) (itemData.getDisplayName() == null ? null : SpongeTexts.toComponent((Text) itemData.getDisplayName().getHandled()))));

			}
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void refresh() {
		try {
			if (data != null)
				data.clear();
			else data = new ArrayList<>();

			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));

			action = EnumPlayerListItemAction.valueOf(((Enum) fields[0].get(getHandler())).name());

			List list = (List) fields[1].get(getHandler());

			for (Object o : list) {

				Field[] nestedFields = o.getClass().getDeclaredFields();
				Arrays.stream(nestedFields).forEach(field -> field.setAccessible(true));

				int ping = nestedFields[0].getInt(o);
				GameType gameType = (GameType) nestedFields[1].get(o);
				GameProfile profile = (GameProfile) nestedFields[2].get(o);
				ITextComponent displayName = (ITextComponent) nestedFields[3].get(o);
				data.add(new WSPlayerListItemData(ping,
						gameType == GameType.NOT_SET ? null : EnumGameMode.valueOf(gameType.name()),
						new SpongeGameProfile((org.spongepowered.api.profile.GameProfile) profile),
						displayName == null ? null : SpongeText.of(SpongeTexts.toText(displayName))));
			}
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public boolean hasChanged() {
		return changed;
	}

	@Override
	public EnumPlayerListItemAction getAction() {
		return action;
	}

	@Override
	public void setAction(EnumPlayerListItemAction action) {
		this.action = action;
		changed = true;
	}

	@Override
	public List<WSPlayerListItemData> getData() {
		changed = true;
		return data;
	}
}

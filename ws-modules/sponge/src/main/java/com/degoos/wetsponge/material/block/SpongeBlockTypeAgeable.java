package com.degoos.wetsponge.material.block;

import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.Objects;

public class SpongeBlockTypeAgeable extends SpongeBlockType implements WSBlockTypeAgeable {

    private int age, maximumAge;

    public SpongeBlockTypeAgeable(int numericalId, String oldStringId, String newStringId, int maxStackSize, int age, int maximumAge) {
        super(numericalId, oldStringId, newStringId, maxStackSize);
        this.age = age;
        this.maximumAge = maximumAge;
    }

    @Override
    public int getAge() {
        return age;
    }

    @Override

    public void setAge(int age) {
        this.age = Math.min(maximumAge, Math.max(0, age));
    }

    @Override
    public int getMaximumAge() {
        return maximumAge;
    }

    @Override
    public SpongeBlockTypeAgeable clone() {
        return new SpongeBlockTypeAgeable(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), age, maximumAge);
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        super.writeItemStack(itemStack);
        itemStack.offer(Keys.GROWTH_STAGE, age);
        return itemStack;
    }

    @Override
    public BlockState writeBlockState(BlockState blockState) {
        blockState = super.writeBlockState(blockState);
        return blockState.with(Keys.GROWTH_STAGE, age).orElse(blockState);
    }

    @Override
    public SpongeBlockTypeAgeable readContainer(ValueContainer<?> valueContainer) {
        super.readContainer(valueContainer);
        age = valueContainer.get(Keys.GROWTH_STAGE).orElse(0);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeAgeable that = (SpongeBlockTypeAgeable) o;
        return age == that.age &&
                maximumAge == that.maximumAge;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), age, maximumAge);
    }
}

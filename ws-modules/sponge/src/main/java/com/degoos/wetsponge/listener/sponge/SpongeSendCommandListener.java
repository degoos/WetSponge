package com.degoos.wetsponge.listener.sponge;


import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.command.SpongeCommandSource;
import com.degoos.wetsponge.command.WSCommand;
import com.degoos.wetsponge.command.WSCommandManager;
import com.degoos.wetsponge.command.WSCommandSource;
import com.degoos.wetsponge.console.SpongeConsoleSource;
import com.degoos.wetsponge.event.WSEventManager;
import com.degoos.wetsponge.event.command.WSSendCommandEvent;
import com.degoos.wetsponge.event.command.WSTabCompleteChatEvent;
import com.degoos.wetsponge.event.command.WSTabCompleteCommandEvent;
import com.degoos.wetsponge.parser.player.PlayerParser;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.StringUtils;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.source.ConsoleSource;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.Order;
import org.spongepowered.api.event.command.SendCommandEvent;
import org.spongepowered.api.event.command.TabCompleteEvent;
import org.spongepowered.api.event.filter.cause.First;

import java.util.Optional;
import java.util.stream.Collectors;

public class SpongeSendCommandListener {

	@Listener(order = Order.FIRST)
	public void onCommand(SendCommandEvent event, @First CommandSource commandSource) {
		try {
			WSCommandSource source = getCommandSource(commandSource);
			WSSendCommandEvent sendCommandEvent = new WSSendCommandEvent(event.getCommand(), event.getArguments().split(" "), source);
			WSEventManager.getInstance().callEvent(sendCommandEvent);
			if (sendCommandEvent.isCancelled()) {
				event.setCancelled(true);
				return;
			}
			Optional<WSCommand> command = WSCommandManager.getInstance().getCommand(sendCommandEvent.getCommand());
			command.ifPresent(wsCommand -> {
				event.setCancelled(true);
				//wsCommand.timings.startTiming();
				wsCommand.executeCommand(source, sendCommandEvent.getCommand(), sendCommandEvent.getArguments());
				//wsCommand.timings.stopTiming();
			});
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Sponge-SendCommandEvent!");
		}
	}


	@Listener(order = Order.FIRST)
	public void onTabRequest(TabCompleteEvent event, @First CommandSource commandSource) {
		try {
			if (event instanceof TabCompleteEvent.Chat) {
				WSTabCompleteChatEvent wetSpongeEvent = new WSTabCompleteChatEvent(new SpongeCommandSource(commandSource), event.getRawMessage(), event
						.getTabCompletions());
				WetSponge.getEventManager().callEvent(wetSpongeEvent);
				if (wetSpongeEvent.isCancelled()) event.setCancelled(true);

			} else if (event instanceof TabCompleteEvent.Command) {
				String[] arguments;
				if (event.getRawMessage().split(" ").length != 1 || event.getRawMessage().endsWith(" ")) {
					String rawArguments = StringUtils.replace(((TabCompleteEvent.Command) event).getArguments(), " ", "<WETSPONGEREMOVE>" + " ");
					arguments = rawArguments.split("<WETSPONGEREMOVE>");
					for (int i = 0; i < arguments.length; i++)
						arguments[i] = StringUtils.replace(arguments[i], " ", "");
				} else arguments = new String[]{""};
				WSTabCompleteCommandEvent wetSpongeEvent = new WSTabCompleteCommandEvent(new SpongeCommandSource(commandSource), ((TabCompleteEvent.Command) event)
						.getCommand(), arguments, event.getTabCompletions());
				WetSponge.getEventManager().callEvent(wetSpongeEvent);
				if (wetSpongeEvent.isCancelled()) event.setCancelled(true);
				else {
					if (event.getRawMessage().split(" ").length == 1 && !event.getRawMessage().endsWith(" ")) event.getTabCompletions()
							.addAll(WSCommandManager.getInstance().getCommandsAndAliases().stream()
									.filter(command -> command.toLowerCase().startsWith(wetSpongeEvent.getCommand().toLowerCase())).collect(Collectors.toList()));
					else WSCommandManager.getInstance().getCommand(wetSpongeEvent.getCommand()).ifPresent(wsCommand -> event.getTabCompletions()
							.addAll(wsCommand.sendTab(wetSpongeEvent.getSource(), wetSpongeEvent.getCommand(), wetSpongeEvent.getArguments())));
				}
			}
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Sponge-TabCompleteEvent!");
		}
	}


	private WSCommandSource getCommandSource(CommandSource source) {
		if (source instanceof Player) return PlayerParser.getPlayer(((Player) source).getUniqueId()).orElse(null);
		if (source instanceof ConsoleSource) return new SpongeConsoleSource((ConsoleSource) source);
		else return new SpongeCommandSource(source);
	}

}

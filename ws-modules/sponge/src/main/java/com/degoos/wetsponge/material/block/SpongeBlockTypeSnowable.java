package com.degoos.wetsponge.material.block;

import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.Objects;

public class SpongeBlockTypeSnowable extends SpongeBlockType implements WSBlockTypeSnowable {

    public boolean snowy;

    public SpongeBlockTypeSnowable(int numericalId, String oldStringId, String newStringId, int maxStackSize, boolean snowy) {
        super(numericalId, oldStringId, newStringId, maxStackSize);
        this.snowy = snowy;
    }

    @Override
    public boolean isSnowy() {
        return snowy;
    }

    @Override
    public void setSnowy(boolean snowy) {
        this.snowy = snowy;
    }

    @Override
    public SpongeBlockTypeSnowable clone() {
        return new SpongeBlockTypeSnowable(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), snowy);
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        super.writeItemStack(itemStack);
        itemStack.offer(Keys.SNOWED, snowy);
        return itemStack;
    }

    @Override
    public BlockState writeBlockState(BlockState blockState) {
        blockState = super.writeBlockState(blockState);
        return blockState.with(Keys.SNOWED, snowy).orElse(blockState);
    }

    @Override
    public SpongeBlockTypeSnowable readContainer(ValueContainer<?> valueContainer) {
        super.readContainer(valueContainer);
        snowy = valueContainer.get(Keys.SNOWED).orElse(false);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeSnowable that = (SpongeBlockTypeSnowable) o;
        return snowy == that.snowy;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), snowy);
    }
}

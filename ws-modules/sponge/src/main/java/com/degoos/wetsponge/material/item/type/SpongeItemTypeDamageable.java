package com.degoos.wetsponge.material.item.type;

import com.degoos.wetsponge.material.item.SpongeItemType;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.Objects;

public class SpongeItemTypeDamageable extends SpongeItemType implements WSItemTypeDamageable {

	private int damage, maxUses;

	public SpongeItemTypeDamageable(int numericalId, String oldStringId, String newStringId, int damage, int maxUses) {
		super(numericalId, oldStringId, newStringId, 1);
		this.damage = damage;
		this.maxUses = maxUses;
	}

	@Override
	public int getDamage() {
		return damage;
	}

	@Override
	public void setDamage(int damage) {
		this.damage = damage;
	}

	@Override
	public int getMaxUses() {
		return maxUses;
	}

	@Override
	public SpongeItemTypeDamageable clone() {
		return new SpongeItemTypeDamageable(getNumericalId(), getOldStringId(), getNewStringId(), damage, maxUses);
	}

	@Override
	public ItemStack writeItemStack(ItemStack itemStack) {
		super.writeItemStack(itemStack);
		itemStack.offer(Keys.ITEM_DURABILITY, maxUses - damage);
		return itemStack;
	}

	@Override
	public SpongeItemTypeDamageable readContainer(ValueContainer<?> valueContainer) {
		super.readContainer(valueContainer);
		damage = maxUses - valueContainer.get(Keys.ITEM_DURABILITY).orElse(0);
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpongeItemTypeDamageable that = (SpongeItemTypeDamageable) o;
		return damage == that.damage &&
				maxUses == that.maxUses;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), damage, maxUses);
	}
}

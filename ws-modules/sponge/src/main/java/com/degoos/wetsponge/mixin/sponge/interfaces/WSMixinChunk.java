package com.degoos.wetsponge.mixin.sponge.interfaces;

public interface WSMixinChunk {

	boolean canBeUnloaded();

	void setCanBeUnloaded(boolean canBeUnloaded);

	boolean canBeSaved();

	void setCanBeSaved(boolean canBeSaved);

	boolean isPreparedToCancelUnload();

	void setPreparedToCancelUnload(boolean preparedToCancelUnload);

}

package com.degoos.wetsponge.entity.living.complex;


import org.spongepowered.api.entity.living.complex.EnderDragon;

public class SpongeEnderDragon extends SpongeComplexLivingEntity implements WSEnderDragon {


	public SpongeEnderDragon(EnderDragon entity) {
		super(entity);
	}

	@Override
	public EnderDragon getHandled() {
		return (EnderDragon) super.getHandled();
	}
}

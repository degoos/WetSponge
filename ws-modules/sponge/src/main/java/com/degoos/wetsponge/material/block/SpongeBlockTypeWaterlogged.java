package com.degoos.wetsponge.material.block;

public class SpongeBlockTypeWaterlogged extends SpongeBlockType implements WSBlockTypeWaterlogged {

    private boolean waterLogged;

    public SpongeBlockTypeWaterlogged(int numericalId, String oldStringId, String newStringId, int maxStackSize, boolean waterLogged) {
        super(numericalId, oldStringId, newStringId, maxStackSize);
        this.waterLogged = waterLogged;
    }

    @Override
    public boolean isWaterlogged() {
        return waterLogged;
    }

    @Override
    public void setWaterlogged(boolean waterlogged) {
        this.waterLogged = waterlogged;
    }

    @Override
    public SpongeBlockTypeWaterlogged clone() {
        return new SpongeBlockTypeWaterlogged(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), waterLogged);
    }

    public boolean isWaterLogged() {
        return waterLogged;
    }

    public void setWaterLogged(boolean waterLogged) {
        this.waterLogged = waterLogged;
    }
}

package com.degoos.wetsponge.item;


import com.degoos.wetsponge.item.enchantment.SpongeEnchantment;
import com.degoos.wetsponge.item.enchantment.WSEnchantment;
import com.degoos.wetsponge.material.SpongeMaterial;
import com.degoos.wetsponge.material.WSBlockTypes;
import com.degoos.wetsponge.material.WSMaterial;
import com.degoos.wetsponge.nbt.SpongeNBTTagCompound;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;
import com.degoos.wetsponge.resource.sponge.SpongeSkullBuilder;
import com.degoos.wetsponge.text.SpongeText;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.text.translation.SpongeTranslation;
import com.degoos.wetsponge.text.translation.WSTranslation;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.Validate;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;
import net.minecraft.item.Item;
import net.minecraft.nbt.NBTTagCompound;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.item.enchantment.Enchantment;
import org.spongepowered.api.item.inventory.ItemStack;

import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;

public class SpongeItemStack implements WSItemStack {

	private ItemStack itemStack;
	private SpongeText displayName;
	private List<WSText> lore;
	private WSMaterial material;
	private int quantity;
	private boolean unbreakable, hideEnchantments, hideAttributes, hideUnbreakable, hideCanDestroy, hideCanBePlacedOn, hidePotionEffects;
	private WSTranslation translation;

	public static SpongeItemStack fromFormat(String format) {
		return new SpongeItemStack(SpongeSkullBuilder.createItemStackByUnknownFormat(format));
	}

	public SpongeItemStack(ItemStack itemStack) {
		Validate.notNull(itemStack, "ItemStack cannot be null!");
		this.itemStack = itemStack;
		refresh();
	}


	public SpongeItemStack(WSMaterial material) {
		Validate.notNull(material, "Material cannot be null!");
		this.material = material;
		this.displayName = null;
		this.lore = new ArrayList<>();
		this.quantity = 1;
		try {
			this.itemStack = ItemStack.of(Sponge.getRegistry()
					.getType(ItemType.class, material.getStringId())
					.get(), 1);
		} catch (NoSuchElementException ex) {
			InternalLogger.printException(ex, material.getStringId());
			Sponge.getRegistry().getAllOf(ItemType.class).forEach(target -> InternalLogger.sendWarning(target.getId()));
		}
		update();
	}

	public SpongeItemStack(String nbt) throws Exception {
		this(WSNBTTagCompound.of(nbt));
	}

	public SpongeItemStack(WSNBTTagCompound nbt) {
		Validate.notNull(nbt, "NBT cannot be null!");
		if (!nbt.hasKey("id")) throw new NullPointerException("NBTTagCompound doesn't contain key id!");
		WSMaterial.getById(nbt.getString("id")).ifPresent(material -> nbt.setString("id", material.getStringId()));
		itemStack = (ItemStack) (Object) new net.minecraft.item.ItemStack((NBTTagCompound) nbt.getHandled());
		refresh();
	}


	public static WSItemStack of(WSMaterial material) {
		return new SpongeItemStack(material);
	}


	@Override
	public WSText getDisplayName() {
		return displayName;
	}


	@Override
	public SpongeItemStack setDisplayName(WSText displayName) {
		this.displayName = (SpongeText) displayName;
		return this;
	}


	@Override
	public List<WSText> getLore() {
		return lore;
	}


	@Override
	public SpongeItemStack setLore(List<WSText> lore) {
		this.lore = lore == null ? new ArrayList<>() : new ArrayList<>(lore);
		return this;
	}

	@Override
	public WSItemStack addLoreLine(WSText line) {
		lore.add(line == null ? WSText.empty() : line);
		return this;
	}

	@Override
	public WSItemStack clearLore() {
		lore.clear();
		return this;
	}

	@Override
	public Map<WSEnchantment, Integer> getEnchantments() {
		Map<WSEnchantment, Integer> map = new HashMap<>();
		getHandled().get(Keys.ITEM_ENCHANTMENTS).orElse(new ArrayList<>())
				.forEach(itemEnchantment -> map.put(new SpongeEnchantment(itemEnchantment.getType()), itemEnchantment.getLevel()));
		return map;
	}

	@Override
	public Optional<Integer> getEnchantmentLevel(WSEnchantment enchantment) {
		Map<WSEnchantment, Integer> enchantments = getEnchantments();
		return Optional.ofNullable(enchantments.get(enchantment));
	}

	@Override
	public boolean containsEnchantment(WSEnchantment enchantment) {
		return getEnchantments().containsKey(enchantment);
	}

	@Override
	public WSItemStack addEnchantment(WSEnchantment enchantment, int level) {
		List<Enchantment> list = getHandled().get(Keys.ITEM_ENCHANTMENTS).orElse(new ArrayList<>());
		list.add(Enchantment.of(((SpongeEnchantment) enchantment).getHandled(), level));
		getHandled().offer(Keys.ITEM_ENCHANTMENTS, list);
		return this;
	}

	@Override
	public WSItemStack removeEnchantment(WSEnchantment enchantment) {
		List<Enchantment> list = getHandled().get(Keys.ITEM_ENCHANTMENTS).orElse(new ArrayList<>());
		list.stream().filter(target -> target.getType().equals(((SpongeEnchantment) enchantment).getHandled())).forEach(list::remove);
		getHandled().offer(Keys.ITEM_ENCHANTMENTS, list);
		return this;
	}

	@Override
	public WSItemStack clearEnchantments() {
		getHandled().offer(Keys.ITEM_ENCHANTMENTS, new ArrayList<>());
		return this;
	}


	public WSMaterial getMaterial() {
		return material;
	}

	@Override
	public WSItemStack setMaterial(WSMaterial material) {
		this.material = material;
		return this;
	}


	@Override
	public int getQuantity() {
		return quantity;
	}


	@Override
	public SpongeItemStack setQuantity(int quantity) {
		this.quantity = quantity;
		return this;
	}

	@Override
	public boolean isUnbreakable() {
		return unbreakable;
	}

	@Override
	public WSItemStack setUnbreakable(boolean unbreakable) {
		this.unbreakable = unbreakable;
		return this;
	}

	@Override
	public boolean isHidingEnchantments() {
		return hideEnchantments;
	}

	@Override
	public WSItemStack hideEnchantments(boolean hideEnchantments) {
		this.hideEnchantments = hideEnchantments;
		return this;
	}

	@Override
	public boolean isHidingAttributes() {
		return hideAttributes;
	}

	@Override
	public WSItemStack hideAttributes(boolean hideAttributes) {
		this.hideAttributes = hideAttributes;
		return this;
	}

	@Override
	public boolean isHidingUnbreakable() {
		return hideUnbreakable;
	}

	@Override
	public WSItemStack hideUnbreakable(boolean hideUnbreakable) {
		this.hideUnbreakable = hideUnbreakable;
		return this;
	}

	@Override
	public boolean isHidingCanDestroy() {
		return hideCanDestroy;
	}

	@Override
	public WSItemStack hideCanDestroy(boolean hideCanDestroy) {
		this.hideCanDestroy = hideCanDestroy;
		return this;
	}

	@Override
	public boolean isHidingCanBePlacedOn() {
		return hideCanBePlacedOn;
	}

	@Override
	public WSItemStack hideCanBePlacedOn(boolean hideCanBePlacedOn) {
		this.hideCanBePlacedOn = hideCanBePlacedOn;
		return this;
	}

	@Override
	public boolean isHidingPotionEffects() {
		return hidePotionEffects;
	}

	@Override
	public WSItemStack hidePotionEffects(boolean hidePotionEffects) {
		this.hidePotionEffects = hidePotionEffects;
		return this;
	}

	@Override
	public WSNBTTagCompound toNBTTag() {
		NBTTagCompound nbtTagCompound = new NBTTagCompound();
		return new SpongeNBTTagCompound(((net.minecraft.item.ItemStack) (Object) itemStack).writeToNBT(nbtTagCompound));
	}

	@Override
	public String toSerializedNBTTag() {
		NBTTagCompound nbtTagCompound = new NBTTagCompound();
		((net.minecraft.item.ItemStack) (Object) itemStack).writeToNBT(nbtTagCompound);
		return nbtTagCompound.toString();
	}


	@Override
	public SpongeItemStack update() {

		try {
			Field field = ReflectionUtils.getFirstField(net.minecraft.item.ItemStack.class, Item.class);
			ReflectionUtils.setAccessible(field);
			field.set(itemStack, net.minecraft.item.Item.getByNameOrId(material.getStringId()));
			ReflectionUtils.setAccessible(field, false);
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was setting an ItemStack id!");
			return this;
		}

		((SpongeMaterial) material).writeItemStack(itemStack);
		if (displayName != null) itemStack.offer(Keys.DISPLAY_NAME, displayName.getHandled());
		else itemStack.remove(Keys.DISPLAY_NAME);
		if (!lore.isEmpty()) itemStack.offer(Keys.ITEM_LORE, lore.stream().map(text -> ((SpongeText) text).getHandled()).collect(Collectors.toList()));
		else itemStack.remove(Keys.ITEM_LORE);

		itemStack.offer(Keys.UNBREAKABLE, unbreakable);

		itemStack.offer(Keys.HIDE_ENCHANTMENTS, hideEnchantments);
		itemStack.offer(Keys.HIDE_ATTRIBUTES, hideAttributes);
		itemStack.offer(Keys.HIDE_UNBREAKABLE, hideUnbreakable);
		itemStack.offer(Keys.HIDE_CAN_DESTROY, hideCanDestroy);
		itemStack.offer(Keys.HIDE_CAN_PLACE, hideCanBePlacedOn);
		itemStack.offer(Keys.HIDE_MISCELLANEOUS, hidePotionEffects);

		itemStack.setQuantity(quantity);
		translation = new SpongeTranslation(itemStack.getTranslation());
		return this;
	}


	@Override
	public SpongeItemStack refresh() {
		this.displayName = itemStack.get(Keys.DISPLAY_NAME).map(SpongeText::of).orElse(null);
		this.lore = itemStack.get(Keys.ITEM_LORE).orElse(new ArrayList<>()).stream().map(SpongeText::of).collect(Collectors.toList());
		Optional<? extends WSMaterial> optional = WSMaterial.getById(itemStack.getItem().getId());
		this.material = optional.isPresent() ? optional.get() : WSBlockTypes.AIR.getDefaultState();

		((SpongeMaterial) material).readContainer(itemStack);

		unbreakable = itemStack.get(Keys.UNBREAKABLE).orElse(false);

		hideEnchantments = itemStack.get(Keys.HIDE_ENCHANTMENTS).orElse(false);
		hideAttributes = itemStack.get(Keys.HIDE_ATTRIBUTES).orElse(false);
		hideUnbreakable = itemStack.get(Keys.HIDE_UNBREAKABLE).orElse(false);
		hideCanDestroy = itemStack.get(Keys.HIDE_CAN_DESTROY).orElse(false);
		hideCanBePlacedOn = itemStack.get(Keys.HIDE_CAN_PLACE).orElse(false);
		hidePotionEffects = itemStack.get(Keys.HIDE_MISCELLANEOUS).orElse(false);

		this.quantity = itemStack.getQuantity();
		translation = new SpongeTranslation(itemStack.getTranslation());
		return this;
	}

	@Override
	public WSTranslation getTranslation() {
		return translation;
	}


	@Override
	public WSItemStack clone() {
		return new SpongeItemStack(itemStack.copy());
	}

	@Override
	public boolean isSimilar(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		SpongeItemStack that = (SpongeItemStack) o;

		if (unbreakable != that.unbreakable) return false;
		if (hideEnchantments != that.hideEnchantments) return false;
		if (hideAttributes != that.hideAttributes) return false;
		if (hideUnbreakable != that.hideUnbreakable) return false;
		if (hideCanDestroy != that.hideCanDestroy) return false;
		if (hideCanBePlacedOn != that.hideCanBePlacedOn) return false;
		if (hidePotionEffects != that.hidePotionEffects) return false;
		if (displayName != null ? !displayName.equals(that.displayName) : that.displayName != null) return false;
		if (lore != null ? !lore.equals(that.lore) : that.lore != null) return false;
		return material != null ? material.equals(that.material) : that.material == null;
	}


	@Override
	public ItemStack getHandled() {
		return itemStack;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		SpongeItemStack that = (SpongeItemStack) o;

		if (quantity != that.quantity) return false;
		if (unbreakable != that.unbreakable) return false;
		if (hideEnchantments != that.hideEnchantments) return false;
		if (hideAttributes != that.hideAttributes) return false;
		if (hideUnbreakable != that.hideUnbreakable) return false;
		if (hideCanDestroy != that.hideCanDestroy) return false;
		if (hideCanBePlacedOn != that.hideCanBePlacedOn) return false;
		if (hidePotionEffects != that.hidePotionEffects) return false;
		if (displayName != null ? !displayName.equals(that.displayName) : that.displayName != null) return false;
		if (lore != null ? !lore.equals(that.lore) : that.lore != null) return false;
		return material != null ? material.equals(that.material) : that.material == null;
	}

	@Override
	public int hashCode() {
		int result = displayName != null ? displayName.hashCode() : 0;
		result = 31 * result + (lore != null ? lore.hashCode() : 0);
		result = 31 * result + (material != null ? material.hashCode() : 0);
		result = 31 * result + quantity;
		result = 31 * result + (unbreakable ? 1 : 0);
		result = 31 * result + (hideEnchantments ? 1 : 0);
		result = 31 * result + (hideAttributes ? 1 : 0);
		result = 31 * result + (hideUnbreakable ? 1 : 0);
		result = 31 * result + (hideCanDestroy ? 1 : 0);
		result = 31 * result + (hideCanBePlacedOn ? 1 : 0);
		result = 31 * result + (hidePotionEffects ? 1 : 0);
		return result;
	}
}

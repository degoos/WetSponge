package com.degoos.wetsponge;


import com.degoos.wetsponge.enums.EnumEntityType;
import com.degoos.wetsponge.enums.EnumServerType;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.enums.EnumTextColor;
import com.degoos.wetsponge.loader.SpongeListenerLoader;
import com.degoos.wetsponge.loader.WetSpongeLoader;
import com.degoos.wetsponge.parser.entity.SpongeEntityParser;
import com.degoos.wetsponge.resource.sponge.SpongeBungeeCord;
import com.degoos.wetsponge.server.SpongeServer;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.util.InternalLogger;
import net.minecraft.server.dedicated.DedicatedServer;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.game.state.GameStartedServerEvent;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.scheduler.Task;

import java.util.Arrays;

@Plugin(id = "wetsponge", name = "WetSponge", version = "Sponge", description = "WetSponge Sponge", authors = {"gaeqs", "IhToN"})
public class SpongeWetSponge implements WetSpongeLoader {

	private static SpongeWetSponge instance;


	public static SpongeWetSponge getInstance() {
		return instance;
	}


	@Listener
	public void onServerStart(GameStartedServerEvent event) {
		instance = this;
		Task.builder().delayTicks(1).execute(() -> {
			try {
				long millis = System.currentTimeMillis();

				EnumServerVersion version = EnumServerVersion.getByVersionName(Sponge.getGame().getPlatform().getMinecraftVersion().getName());

				String wetSpongeVersion = getClass().getAnnotation(Plugin.class).version();

				DedicatedServer minecraftServer = (DedicatedServer) Sponge.getServer();

				WetSponge.load(wetSpongeVersion, EnumServerType.SPONGE, this, new SpongeServer(Sponge.getServer()), version, new SpongeBungeeCord(), minecraftServer
						.getServerThread());

				InternalLogger.sendInfo("Loading WetSponge  " + wetSpongeVersion + "...");

				InternalLogger.sendInfo(WSText.builder("Using version ").append(WSText.builder("SPONGE " + version.name()).color(EnumTextColor.GREEN).build()).build());

				InternalLogger.sendInfo("Loading entities.");
				SpongeEntityParser.load();
				Arrays.stream(EnumEntityType.values()).forEach(EnumEntityType::load);
				InternalLogger.sendInfo("Loading Sponge listeners.");
				SpongeListenerLoader.load();
				InternalLogger.sendInfo("Loading common.");
				WetSponge.loadCommon();
				double secs = (System.currentTimeMillis() - millis) / 1000D;
				InternalLogger.sendDone(WSText.builder("WetSponge has been loaded in ").append(WSText.builder(String.valueOf(secs)).color(EnumTextColor.RED).build())
						.append(WSText.builder(" seconds!").color(EnumTextColor.GREEN).build()).build());
			} catch (Throwable ex) {
				ex.printStackTrace();
				InternalLogger.printException(ex, "An error has occurred while WetSponge was loading!");
			}
		}).submit(this);
	}

	public void unload() {
		try {
			WetSponge.unloadCommon();
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was unloading!");
		}
	}
}

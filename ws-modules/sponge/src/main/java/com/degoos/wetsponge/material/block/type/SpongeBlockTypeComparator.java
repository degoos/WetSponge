package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.enums.block.EnumBlockTypeComparatorMode;
import com.degoos.wetsponge.material.block.SpongeBlockTypeDirectional;
import com.degoos.wetsponge.util.Validate;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.ComparatorTypes;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.Objects;
import java.util.Set;

public class SpongeBlockTypeComparator extends SpongeBlockTypeDirectional implements WSBlockTypeComparator {

    private EnumBlockTypeComparatorMode mode;
    private boolean powered;

    public SpongeBlockTypeComparator(EnumBlockFace facing, Set<EnumBlockFace> faces, EnumBlockTypeComparatorMode mode, boolean powered) {
        super(149, "minecraft:unpowered_comparator", "minecraft:comparator", 64, facing, faces);
        Validate.notNull(mode, "Mode cannot be null!");
        this.mode = mode;
        this.powered = powered;
    }

    @Override
    public int getNumericalId() {
        return isPowered() ? 150 : 149;
    }

    @Override
    public String getOldStringId() {
        return isPowered() ? "minecraft:powered_comparator" : "minecraft:unpowered_comparator";
    }

    @Override
    public EnumBlockTypeComparatorMode getMode() {
        return mode;
    }

    @Override
    public void setMode(EnumBlockTypeComparatorMode mode) {
        Validate.notNull(mode, "Mode cannot be null!");
        this.mode = mode;
    }

    @Override
    public boolean isPowered() {
        return powered;
    }

    @Override
    public void setPowered(boolean powered) {
        this.powered = powered;
    }

    @Override
    public SpongeBlockTypeComparator clone() {
        return new SpongeBlockTypeComparator(getFacing(), getFaces(), mode, powered);
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        super.writeItemStack(itemStack);
        itemStack.offer(Keys.COMPARATOR_TYPE, mode == EnumBlockTypeComparatorMode.SUBTRACT ? ComparatorTypes.SUBTRACT : ComparatorTypes.COMPARE);
        itemStack.offer(Keys.POWERED, powered);
        return itemStack;
    }

    @Override
    public BlockState writeBlockState(BlockState blockState) {
        blockState = super.writeBlockState(blockState);
        blockState = blockState.with(Keys.POWERED, powered).orElse(blockState);
        return blockState.with(Keys.COMPARATOR_TYPE, mode == EnumBlockTypeComparatorMode.SUBTRACT ? ComparatorTypes.SUBTRACT : ComparatorTypes.COMPARE).orElse(blockState);
    }

    @Override
    public SpongeBlockTypeComparator readContainer(ValueContainer<?> valueContainer) {
        super.readContainer(valueContainer);
        mode = valueContainer.get(Keys.COMPARATOR_TYPE).orElse(ComparatorTypes.COMPARE).equals(ComparatorTypes.COMPARE) ?
                EnumBlockTypeComparatorMode.COMPARE : EnumBlockTypeComparatorMode.SUBTRACT;
        powered = valueContainer.get(Keys.POWERED).orElse(false);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeComparator that = (SpongeBlockTypeComparator) o;
        return powered == that.powered &&
                mode == that.mode;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), mode, powered);
    }
}

package com.degoos.wetsponge.material;

import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;

public interface SpongeMaterial extends WSMaterial {

    SpongeMaterial readContainer(ValueContainer<?> valueContainer);

    ItemStack writeItemStack(ItemStack itemStack);
}

package com.degoos.wetsponge.world;


import com.degoos.wetsponge.block.SpongeBlock;
import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.enums.EnumBiomeType;
import com.degoos.wetsponge.listener.sponge.SpongeWorldListener;
import com.degoos.wetsponge.parser.entity.SpongeEntityParser;
import com.degoos.wetsponge.parser.world.WorldParser;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.NumericUtils;
import com.flowpowered.math.vector.Vector2f;
import com.flowpowered.math.vector.Vector3d;
import com.flowpowered.math.vector.Vector3i;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.tileentity.Sign;
import org.spongepowered.api.block.tileentity.TileEntity;
import org.spongepowered.api.block.tileentity.TileEntityTypes;
import org.spongepowered.api.data.manipulator.mutable.tileentity.SignData;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.Transform;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.*;
import java.util.stream.Collectors;

public class SpongeLocation implements WSLocation {

	public static Map<String, List<SpongeLocation>> locations = new HashMap<>();

	private String worldName;
	private double x, y, z;
	private float yaw, pitch;
	private Transform<World> spongeLocation;
	private World spongeWorld;


	public SpongeLocation(String worldName, double x, double y, double z, float yaw, float pitch) {
		this.worldName = worldName;
		this.x = x;
		this.y = y;
		this.z = z;
		this.yaw = yaw;
		this.pitch = pitch;
		this.spongeWorld = SpongeWorldListener.getWorld(worldName).orElse(null);
		putInMap();
	}


	public SpongeLocation(World world, double x, double y, double z, float yaw, float pitch) {
		this.worldName = world.getName();
		this.x = x;
		this.y = y;
		this.z = z;
		this.yaw = yaw;
		this.pitch = pitch;
		this.spongeWorld = SpongeWorldListener.getWorld(worldName).orElse(null);
		putInMap();
	}


	public SpongeLocation(Location<World> location) {
		this.worldName = location.getExtent().getName();
		this.x = location.getX();
		this.y = location.getY();
		this.z = location.getZ();
		this.yaw = 0;
		this.pitch = 0;
		this.spongeWorld = location.getExtent();
		putInMap();
	}

	public SpongeLocation(Location<World> location, Vector3d rotation) {
		this.worldName = location.getExtent().getName();
		this.x = location.getX();
		this.y = location.getY();
		this.z = location.getZ();
		this.yaw = (float) rotation.getY();
		this.pitch = (float) rotation.getX();
		this.spongeWorld = location.getExtent();
		putInMap();
	}

	public SpongeLocation(Transform<World> transform) {
		this.worldName = transform.getExtent().getName();
		this.x = transform.getLocation().getX();
		this.y = transform.getLocation().getY();
		this.z = transform.getLocation().getZ();
		this.yaw = (float) transform.getRotation().getY();
		this.pitch = (float) transform.getRotation().getX();
		this.spongeWorld = transform.getExtent();
		putInMap();
	}


	public SpongeLocation(WSLocation location) {
		this.worldName = location.getWorldName();
		this.x = location.getX();
		this.y = location.getY();
		this.z = location.getZ();
		this.yaw = location.getYaw();
		this.pitch = location.getPitch();
		this.spongeWorld = SpongeWorldListener.getWorld(worldName).orElse(null);
		putInMap();
	}


	public SpongeLocation(String string) {
		String[] sl = string.split(";");
		if (NumericUtils.isDouble(sl[0])) {
			this.worldName = sl[5];
			this.x = Double.parseDouble(sl[0]);
			this.y = Double.parseDouble(sl[1]);
			this.z = Double.parseDouble(sl[2]);
			this.pitch = Float.parseFloat(sl[3]);
			this.yaw = Float.parseFloat(sl[4]);
		} else {
			this.worldName = sl[0];
			this.x = Double.parseDouble(sl[1]);
			this.y = Double.parseDouble(sl[2]);
			this.z = Double.parseDouble(sl[3]);
			this.yaw = Float.parseFloat(sl[4]);
			this.pitch = Float.parseFloat(sl[5]);
		}
		this.spongeWorld = SpongeWorldListener.getWorld(worldName).orElse(null);
		putInMap();
	}


	public SpongeLocation(Player player) {
		Location<World> playerLocation = player.getLocation();
		this.worldName = playerLocation.getExtent().getName();
		this.x = playerLocation.getX();
		this.y = playerLocation.getY();
		this.z = playerLocation.getZ();
		this.yaw = (float) player.getRotation().getX();
		this.pitch = (float) player.getRotation().getZ();
		this.spongeWorld = playerLocation.getExtent();
		putInMap();
	}


	private void putInMap() {
		if (locations.containsKey(worldName)) locations.get(worldName).add(this);
		else {
			List<SpongeLocation> list = new ArrayList<>();
			list.add(this);
			locations.put(worldName, list);
		}
		if (spongeWorld != null) spongeLocation = new Transform<>(spongeWorld, new Vector3d(x, y, z), getRotation().toVector3().toDouble());

	}


	public String toString() {
		return worldName + ";" + x + ";" + y + ";" + z + ";" + yaw + ";" + pitch;
	}


	public Transform<World> getLocation() {
		spongeWorld = SpongeWorldListener.getWorld(worldName).orElse(null);
		if (spongeWorld != null && spongeLocation == null) spongeLocation = new Transform<>(spongeWorld, new Vector3d(x, y, z), new Vector3d(pitch, yaw, 0));
		if (spongeWorld == null) {
			InternalLogger.sendError("ERROR: world " + worldName + " doesn't exist!");
			Optional<World> optional = Sponge.getServer().getWorlds().stream().findFirst();
			return optional.map(world -> new Transform<>(spongeWorld, new Vector3d(x, y, z), getRotation().toVector3().toDouble())).orElse(null);
		}
		return spongeLocation;
	}


	public SpongeBlock getBlock() {
		if (spongeWorld != null && spongeLocation == null) spongeLocation = new Transform<>(spongeWorld, new Vector3d(x, y, z), new Vector3d(pitch, yaw, 0));
		if (spongeLocation == null) return null;
		return new SpongeBlock(spongeLocation.getLocation());
	}


	public String getWorldName() {
		return worldName;
	}


	public SpongeLocation setWorldName(String worldName) {
		locations.get(this.worldName).remove(this);
		this.worldName = worldName;
		spongeWorld = SpongeWorldListener.getWorld(worldName).orElse(null);
		putInMap();
		return this;
	}


	public SpongeWorld getWorld() {
		spongeWorld = SpongeWorldListener.getWorld(worldName).orElse(null);
		return (SpongeWorld) WorldParser.getOrCreateWorld(spongeWorld.getName(), spongeWorld);
	}


	@Override
	public WSLocation setWorld(WSWorld world) {
		setWorldName(world.getName());
		return this;
	}


	public WSLocation updateWorld() {
		this.spongeWorld = SpongeWorldListener.getWorld(worldName).orElse(null);
		if (spongeLocation != null) spongeLocation = spongeLocation.setExtent(spongeWorld);
		else spongeLocation = new Transform<>(spongeWorld, new Vector3d(x, y, z), getRotation().toVector3().toDouble());
		return this;
	}


	public double getX() {
		return x;
	}


	public SpongeLocation setX(double x) {
		this.x = x;
		if (getLocation() != null) spongeLocation = spongeLocation.setPosition(new Vector3d(x, spongeLocation.getPosition().getY(), spongeLocation.getPosition().getZ()));
		return this;
	}


	public double getY() {
		return y;
	}


	public SpongeLocation setY(double y) {
		this.y = y;
		if (getLocation() != null) spongeLocation = spongeLocation.setPosition(new Vector3d(spongeLocation.getPosition().getX(), y, spongeLocation.getPosition().getZ
				()));
		return this;
	}


	public double getZ() {
		return z;
	}


	public SpongeLocation setZ(double z) {
		this.z = z;
		if (getLocation() != null) spongeLocation = spongeLocation.setPosition(new Vector3d(spongeLocation.getPosition().getX(), spongeLocation.getPosition().getY(),
				z));
		return this;
	}


	public float getYaw() {
		return yaw;
	}


	public SpongeLocation setYaw(float yaw) {
		setRotation(new Vector2f(pitch, yaw));
		return this;
	}


	public float getPitch() {
		return pitch;
	}


	public SpongeLocation setPitch(float pitch) {
		setRotation(new Vector2f(pitch, yaw));
		return this;
	}

	@Override
	public Vector2f getRotation() {
		return new Vector2f(pitch, yaw);
	}

	@Override
	public WSLocation setRotation(Vector2f rotation) {
		this.pitch = rotation.getX();
		this.yaw = rotation.getY();
		spongeLocation.setRotation(new Vector3d(pitch, yaw, 0));
		return this;
	}


	public int getBlockX() {
		return NumericUtils.floor(x);
	}


	public int getBlockY() {
		return NumericUtils.floor(y);
	}


	public int getBlockZ() {
		return NumericUtils.floor(z);
	}


	public double distance(WSLocation location) {
		return Math.sqrt(Math.pow(location.getX() - x, 2) + Math.pow(location.getY() - y, 2) + Math.pow(location.getZ() - z, 2));
	}


	public double distance(Location location) {
		return Math.sqrt(Math.pow(location.getX() - x, 2) + Math.pow(location.getY() - y, 2) + Math.pow(location.getZ() - z, 2));
	}


	public double distance(double x, double y, double z) {
		return Math.sqrt(Math.pow(x - this.x, 2) + Math.pow(y - this.y, 2) + Math.pow(z - this.z, 2));
	}


	@Override
	public double distance(Vector3d vector3d) {
		return distance(vector3d.getX(), vector3d.getY(), vector3d.getZ());
	}


	public SpongeLocation add(double x, double y, double z) {
		this.x += x;
		this.y += y;
		this.z += z;
		if (getLocation() != null) spongeLocation = spongeLocation.setLocation(spongeLocation.getLocation().add(x, y, z));
		return this;
	}

	@Override
	public WSLocation add(Vector3d vector3d) {
		return add(vector3d.getX(), vector3d.getY(), vector3d.getZ());
	}


	public Set<Entity> getEntitiesOnBlock() {
		if (getLocation() == null) return new HashSet<>();
		return spongeWorld.getEntities().stream().filter(entity -> new SpongeLocation(entity.getLocation()).equalsBlock(this)).collect(Collectors.toSet());
	}


	public <T extends Entity> Set<T> getEntitiesOnBlock(Class<T> entityType) {
		if (getLocation() == null) return new HashSet<>();
		Set<T> set = new HashSet<>();
		spongeWorld.getEntities().stream().filter(entity -> entityType.isInstance(entity) && new SpongeLocation(entity.getLocation()).equalsBlock(this))
				.forEach(entity -> set.add((T) entity));
		return set;
	}


	public SpongeLocation clone() {
		return new SpongeLocation(this);
	}


	public SpongeLocation getBlockLocation() {
		return new SpongeLocation(worldName, getBlockX(), getBlockY(), getBlockZ(), 0, 0);
	}

	@Override
	public EnumBiomeType getBiome() {
		return getWorld().getBiome(getBlockX(), getBlockY(), getBlockZ());
	}

	@Override
	public void setBiome(EnumBiomeType biome) {
		getWorld().setBiome(getBlockX(), getBlockY(), getBlockZ(), biome);
	}


	@Override
	public Collection<WSEntity> getNearbyEntities(Vector3d radius) {
		if (getLocation() == null) return new HashSet<>();
		return spongeWorld.getEntities().stream().filter(entity -> {
			Location<World> location = entity.getLocation();
			return Math.abs(location.getX() - x) < radius.getX() && Math.abs(location.getY() - y) < radius.getY() && Math.abs(location.getZ() - z) < radius.getZ();
		}).map(SpongeEntityParser::getWSEntity).collect(Collectors.toSet());
	}


	@Override
	public Set<WSEntity> getNearbyEntities(double radius) {
		if (getLocation() == null) return new HashSet<>();
		return spongeWorld.getEntities().stream().filter(entity -> distance(entity.getLocation()) <= radius).map(SpongeEntityParser::getWSEntity)
				.collect(Collectors.toSet());
	}


	@Override
	public <T extends WSEntity> Collection<T> getNearbyEntities(Vector3d radius, Class<T> entityType) {
		if (spongeWorld == null) return new HashSet<>();
		Set<T> set = new HashSet<>();
		try {
			spongeWorld.getEntities().stream().map(SpongeEntityParser::getWSEntity).filter(entity -> {
				WSLocation location = entity.getLocation();
				return entityType.isInstance(entity) && Math.abs(location.getX() - x) < radius.getX() && Math.abs(location.getY() - y) < radius.getY() &&
						Math.abs(location.getZ() - z) < radius.getZ();
			}).forEach(entity -> set.add((T) entity));
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return set;
	}


	@Override
	public <T extends WSEntity> Collection<T> getNearbyEntities(double radius, Class<T> entityType) {
		if (spongeWorld == null) return new HashSet<>();
		Set<T> set = new HashSet<>();
		try {
			spongeWorld.getEntities().stream().map(SpongeEntityParser::getWSEntity)
					.filter(entity -> entityType.isInstance(entity) && distance(entity.getLocation()) <= radius).forEach(entity -> set.add((T) entity));
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return set;
	}


	@Override
	public Collection<WSPlayer> getNearbyPlayers(Vector3d radius) {
		return getNearbyEntities(radius, WSPlayer.class);
	}

	@Override
	public Collection<WSPlayer> getNearbyPlayers(double radius) {
		return getNearbyEntities(radius, WSPlayer.class);
	}

	@Override
	public int getHighestY() {
		return spongeLocation.getExtent().getHighestYAt(getBlockX(), getBlockZ());
	}


	public WSLocation updateSign(String[] lines) {
		if (lines.length != 4) return this;
		if (spongeWorld == null) getLocation();
		if (spongeWorld == null) return this;
		Optional<TileEntity> optional = spongeLocation.getLocation().getTileEntity();
		if (!optional.isPresent() || !optional.get().getType().equals(TileEntityTypes.SIGN)) return this;
		Sign sign = (Sign) optional.get();
		SignData data = sign.getSignData();
		for (int i = 0; i < 4; i++) data.set(data.lines().set(i, Text.of(lines[i])));
		sign.offer(data);
		return this;
	}


	public boolean equalsBlock(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		SpongeLocation that = (SpongeLocation) o;

		return getBlockX() == that.getBlockX() && getBlockY() == that.getBlockY() && getBlockZ() == that.getBlockZ();
	}

	@Override
	public Vector3d getFacingDirection() {
		double xz = Math.cos(Math.toRadians(pitch));
		return new Vector3d(-xz * Math.sin(Math.toRadians(yaw)), -Math.sin(Math.toRadians(pitch)), xz * Math.cos(Math.toRadians(yaw)));
	}


	public Vector3d toVector3d() {
		return new Vector3d(x, y, z);
	}


	@Override
	public Vector3i toVector3i() {
		return new Vector3i(getBlockX(), getBlockY(), getBlockZ());
	}

	@Override
	public World getHandledWorld() {
		return spongeWorld;
	}


	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		SpongeLocation that = (SpongeLocation) o;

		return Double.compare(that.x, x) == 0 && Double.compare(that.y, y) == 0 && Double.compare(that.z, z) == 0 && Float.compare(that.yaw, yaw) == 0 &&
				Float.compare(that.pitch, pitch) == 0 && (worldName != null ? worldName.equals(that.worldName) : that.worldName == null);
	}


	@Override
	public int hashCode() {
		int result;
		long temp;
		result = worldName != null ? worldName.hashCode() : 0;
		temp = Double.doubleToLongBits(x);
		result = 31 * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(y);
		result = 31 * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(z);
		result = 31 * result + (int) (temp ^ (temp >>> 32));
		result = 31 * result + (yaw != +0.0f ? Float.floatToIntBits(yaw) : 0);
		result = 31 * result + (pitch != +0.0f ? Float.floatToIntBits(pitch) : 0);
		return result;
	}
}

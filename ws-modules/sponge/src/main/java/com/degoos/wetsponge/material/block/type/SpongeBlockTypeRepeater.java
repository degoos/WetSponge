package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.SpongeBlockTypeDirectional;
import net.minecraft.block.BlockRedstoneRepeater;
import net.minecraft.block.state.IBlockState;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.Objects;
import java.util.Set;

public class SpongeBlockTypeRepeater extends SpongeBlockTypeDirectional implements WSBlockTypeRepeater {

    private int delay, minimumDelay, maximumDelay;
    private boolean locked, powered;

    public SpongeBlockTypeRepeater(EnumBlockFace facing, Set<EnumBlockFace> faces, int delay, int minimumDelay, int maximumDelay, boolean locked, boolean powered) {
        super(93, "minecraft:unpowered_repeater", "minecraft:repeater", 64, facing, faces);
        this.delay = delay;
        this.minimumDelay = minimumDelay;
        this.maximumDelay = maximumDelay;
        this.locked = locked;
        this.powered = powered;
    }

    @Override
    public int getNumericalId() {
        return powered ? 94 : 93;
    }

    @Override
    public String getOldStringId() {
        return powered ? "minecraft:powered_repeater" : "minecraft:unpowered_repeater";
    }

    @Override

    public int getDelay() {
        return delay;
    }

    @Override
    public void setDelay(int delay) {
        this.delay = Math.max(minimumDelay, Math.min(minimumDelay, delay));
    }

    @Override
    public int getMinimumDelay() {
        return minimumDelay;
    }

    @Override
    public int getMaximumDelay() {
        return maximumDelay;
    }

    @Override
    public boolean isLocked() {
        return locked;
    }

    @Override
    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    @Override
    public boolean isPowered() {
        return powered;
    }

    @Override
    public void setPowered(boolean powered) {
        this.powered = powered;
    }

    @Override
    public SpongeBlockTypeRepeater clone() {
        return new SpongeBlockTypeRepeater(getFacing(), getFaces(), delay, minimumDelay, maximumDelay, locked, powered);
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        super.writeItemStack(itemStack);
        itemStack.offer(Keys.DELAY, delay);
        return itemStack;
    }

    @Override
    public BlockState writeBlockState(BlockState blockState) {
        blockState = super.writeBlockState(blockState);
        blockState = (BlockState) ((IBlockState) blockState).withProperty(BlockRedstoneRepeater.LOCKED, locked);
        return blockState.with(Keys.DELAY, delay).orElse(blockState);
    }

    @Override
    public SpongeBlockTypeRepeater readContainer(ValueContainer<?> valueContainer) {
        super.readContainer(valueContainer);
        delay = valueContainer.get(Keys.DELAY).orElse(0);
        if (valueContainer instanceof IBlockState)
            locked = ((IBlockState) valueContainer).getValue(BlockRedstoneRepeater.LOCKED);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeRepeater that = (SpongeBlockTypeRepeater) o;
        return delay == that.delay &&
                minimumDelay == that.minimumDelay &&
                maximumDelay == that.maximumDelay &&
                locked == that.locked &&
                powered == that.powered;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), delay, minimumDelay, maximumDelay, locked, powered);
    }
}

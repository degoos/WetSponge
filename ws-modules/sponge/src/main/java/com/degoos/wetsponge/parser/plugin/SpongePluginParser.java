package com.degoos.wetsponge.parser.plugin;

import com.degoos.wetsponge.plugin.SpongeBasePlugin;
import com.degoos.wetsponge.plugin.WSBasePlugin;

import java.util.Set;
import java.util.stream.Collectors;

public class SpongePluginParser {

	public static Set<WSBasePlugin> getBasePlugins() {
		return org.spongepowered.api.Sponge.getPluginManager().getPlugins().stream().map(SpongeBasePlugin::new).collect(Collectors.toSet());
	}

	public static boolean isBasePluginEnabled(String name) {
		return org.spongepowered.api.Sponge.getPluginManager().isLoaded(name);
	}

}

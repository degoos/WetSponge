package com.degoos.wetsponge.world.generation;

import com.degoos.wetsponge.parser.world.WorldParser;
import com.degoos.wetsponge.world.SpongeWorld;
import com.degoos.wetsponge.world.generation.populator.WSGenerationPopulator;
import org.spongepowered.api.world.gen.WorldGenerator;

public class SpongeWorldGenerator implements WSWorldGenerator {

    private WorldGenerator worldGenerator;

    public SpongeWorldGenerator(WorldGenerator worldGenerator) {
        this.worldGenerator = worldGenerator;
    }

    @Override
    public void setBaseGenerationPopulator(WSGenerationPopulator populator) {
        worldGenerator.setBaseGenerationPopulator((world, buffer, biomes) ->
                populator.populate(WorldParser.getOrCreateWorld(world.getName(), world), new SpongeBlockVolume(buffer)));
        worldGenerator.setBiomeGenerator(buffer -> {

        });
    }

    @Override
    public WorldGenerator getHandler() {
        return worldGenerator;
    }
}

package com.degoos.wetsponge.user;

import com.degoos.wetsponge.util.Validate;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.Multimap;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.profile.GameProfile;

import javax.annotation.Nullable;
import java.util.Optional;
import java.util.UUID;

public class SpongeGameProfile implements WSGameProfile {

	public static WSGameProfile of(UUID uniqueId, @Nullable String name) {
		return new SpongeGameProfile(Sponge.getServer().getGameProfileManager().createProfile(uniqueId, name));
	}

	private GameProfile gameProfile;


	public SpongeGameProfile(GameProfile gameProfile) {
		this.gameProfile = gameProfile;
	}

	@Override
	public UUID getId() {
		return gameProfile.getUniqueId();
	}

	@Override
	public Optional<String> getName() {
		return gameProfile.getName();
	}

	@Override
	public Multimap<String, WSProfileProperty> getPropertyMap() {
		Multimap<String, WSProfileProperty> map = LinkedHashMultimap.create();
		gameProfile.getPropertyMap().asMap().forEach((name, collection) -> collection.forEach(prop -> map.put(name, new SpongeProfileProperty(prop))));
		return map;
	}

	@Override
	public WSGameProfile addProperty(String name, WSProfileProperty property) {
		Validate.notNull(name, "Name cannot be null!");
		Validate.notNull(property, "Property cannot be null!");
		gameProfile.addProperty(name, ((SpongeProfileProperty) property).getHandled());
		return this;
	}

	@Override
	public boolean isFilled() {
		return gameProfile.isFilled();
	}

	@Override
	public GameProfile getHandled() {
		return gameProfile;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		SpongeGameProfile that = (SpongeGameProfile) o;

		return gameProfile.equals(that.gameProfile);
	}

	@Override
	public int hashCode() {
		return gameProfile.hashCode();
	}
}

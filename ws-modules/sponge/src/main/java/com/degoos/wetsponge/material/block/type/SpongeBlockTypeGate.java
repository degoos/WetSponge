package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.SpongeBlockTypeDirectional;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.Objects;
import java.util.Set;

public class SpongeBlockTypeGate extends SpongeBlockTypeDirectional implements WSBlockTypeGate {

    private boolean inWall, open, powered;

    public SpongeBlockTypeGate(int numericalId, String oldStringId, String newStringId, int maxStackSize, EnumBlockFace facing, Set<EnumBlockFace> faces,
                               boolean inWall, boolean open, boolean powered) {
        super(numericalId, oldStringId, newStringId, maxStackSize, facing, faces);
        this.inWall = inWall;
        this.open = open;
        this.powered = powered;
    }

    @Override
    public boolean isInWall() {
        return inWall;
    }

    @Override
    public void setInWall(boolean inWall) {
        this.inWall = inWall;
    }

    @Override
    public boolean isOpen() {
        return open;
    }

    @Override
    public void setOpen(boolean open) {
        this.open = open;
    }

    @Override
    public boolean isPowered() {
        return powered;
    }

    @Override
    public void setPowered(boolean powered) {
        this.powered = powered;
    }

    @Override
    public SpongeBlockTypeGate clone() {
        return new SpongeBlockTypeGate(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), getFacing(), getFaces(), inWall, open, powered);
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        super.writeItemStack(itemStack);
        itemStack.offer(Keys.IN_WALL, inWall);
        itemStack.offer(Keys.OPEN, open);
        itemStack.offer(Keys.POWERED, powered);
        return itemStack;
    }

    @Override
    public BlockState writeBlockState(BlockState blockState) {
        blockState = super.writeBlockState(blockState);
        blockState = blockState.with(Keys.IN_WALL, inWall).orElse(blockState);
        blockState = blockState.with(Keys.OPEN, open).orElse(blockState);
        return blockState.with(Keys.POWERED, powered).orElse(blockState);
    }

    @Override
    public SpongeBlockTypeGate readContainer(ValueContainer<?> valueContainer) {
        super.readContainer(valueContainer);
        inWall = valueContainer.get(Keys.IN_WALL).orElse(false);
        open = valueContainer.get(Keys.OPEN).orElse(false);
        powered = valueContainer.get(Keys.POWERED).orElse(false);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeGate that = (SpongeBlockTypeGate) o;
        return inWall == that.inWall &&
                open == that.open &&
                powered == that.powered;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), inWall, open, powered);
    }
}

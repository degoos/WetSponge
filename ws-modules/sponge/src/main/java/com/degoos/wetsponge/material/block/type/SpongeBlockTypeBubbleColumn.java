package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.material.block.SpongeBlockType;

import java.util.Objects;

public class SpongeBlockTypeBubbleColumn extends SpongeBlockType implements WSBlockTypeBubbleColumn {

    private boolean drag;

    public SpongeBlockTypeBubbleColumn(boolean drag) {
        super(-1, null, "minecraft:bubble_column", 64);
        this.drag = drag;
    }

    @Override
    public boolean isDrag() {
        return drag;
    }

    @Override
    public void setDrag(boolean drag) {
        this.drag = drag;
    }

    @Override
    public SpongeBlockTypeBubbleColumn clone() {
        return new SpongeBlockTypeBubbleColumn(drag);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeBubbleColumn that = (SpongeBlockTypeBubbleColumn) o;
        return drag == that.drag;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), drag);
    }
}

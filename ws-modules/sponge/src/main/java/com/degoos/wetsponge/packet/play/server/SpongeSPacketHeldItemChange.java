package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.packet.SpongePacket;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.SPacketHeldItemChange;

import java.lang.reflect.Field;
import java.util.Arrays;

public class SpongeSPacketHeldItemChange extends SpongePacket implements WSSPacketHeldItemChange {

    private int slot;
    private boolean changed;

    public SpongeSPacketHeldItemChange(int slot) {
        super(new SPacketHeldItemChange());
        this.slot = slot;
        update();
    }

    public SpongeSPacketHeldItemChange(Packet<?> packet) {
        super(packet);
        refresh();
    }

    @Override
    public void update() {
        try {
            Field[] fields = getHandler().getClass().getDeclaredFields();
            Arrays.stream(fields).forEach(field -> field.setAccessible(true));
            fields[0].setInt(getHandler(), slot);
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void refresh() {
        try {
            Field[] fields = getHandler().getClass().getDeclaredFields();
            Arrays.stream(fields).forEach(field -> field.setAccessible(true));
            slot = fields[0].getInt(getHandler());
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public boolean hasChanged() {
        return changed;
    }

    @Override
    public int getSlot() {
        return slot;
    }

    @Override
    public void setSlot(int slot) {
        this.slot = slot;
        changed = true;
    }
}

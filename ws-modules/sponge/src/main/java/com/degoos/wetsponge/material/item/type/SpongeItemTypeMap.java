package com.degoos.wetsponge.material.item.type;

import com.degoos.wetsponge.color.WSColor;
import com.degoos.wetsponge.material.item.SpongeItemType;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.Objects;

public class SpongeItemTypeMap extends SpongeItemType implements WSItemTypeMap {

    private int mapId;
    private WSColor color;

    public SpongeItemTypeMap(int mapId, WSColor color) {
        super(358, "minecraft:filled_map", "minecraft:filled_map", 1);
        this.mapId = mapId;
        this.color = color;
    }

    @Override
    public int getMapId() {
        return mapId;
    }

    @Override
    public void setMapId(int mapId) {
        this.mapId = Math.max(0, mapId);
    }

    @Override
    public WSColor getMapColor() {
        return color;
    }

    @Override
    public void setMapColor(WSColor mapColor) {
        this.color = mapColor;
    }

    @Override
    public SpongeItemTypeMap clone() {
        return new SpongeItemTypeMap(mapId, color);
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        super.writeItemStack(itemStack);
        ((net.minecraft.item.ItemStack) (Object) itemStack).setItemDamage(mapId);
        return itemStack;
    }

    @Override
    public SpongeItemTypeMap readContainer(ValueContainer<?> valueContainer) {
        super.readContainer(valueContainer);
        if (valueContainer instanceof ItemStack) {
            mapId = ((net.minecraft.item.ItemStack) (Object) valueContainer).getItemDamage();
        }
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeItemTypeMap that = (SpongeItemTypeMap) o;
        return mapId == that.mapId &&
                Objects.equals(color, that.color);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), mapId, color);
    }
}

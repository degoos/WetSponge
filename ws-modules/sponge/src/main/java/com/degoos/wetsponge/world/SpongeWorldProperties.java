package com.degoos.wetsponge.world;

import com.degoos.wetsponge.enums.EnumDifficulty;
import com.degoos.wetsponge.enums.EnumGameMode;
import com.flowpowered.math.vector.Vector3i;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.entity.living.player.gamemode.GameMode;
import org.spongepowered.api.entity.living.player.gamemode.GameModes;
import org.spongepowered.api.world.difficulty.Difficulties;
import org.spongepowered.api.world.difficulty.Difficulty;
import org.spongepowered.api.world.storage.WorldProperties;

public class SpongeWorldProperties implements WSWorldProperties {

    private WorldProperties worldProperties;

    public SpongeWorldProperties(WorldProperties worldProperties) {
        this.worldProperties = worldProperties;
    }

    @Override
    public boolean isInitialized() {
        return worldProperties.isInitialized();
    }

    @Override
    public String getWorldName() {
        return worldProperties.getWorldName();
    }

    @Override
    public UUID getUniqueId() {
        return worldProperties.getUniqueId();
    }

    @Override
    public boolean isEnabled() {
        return worldProperties.isEnabled();
    }

    @Override
    public void setEnabled(boolean enabled) {
        worldProperties.setEnabled(enabled);
    }

    @Override
    public boolean loadOnStartup() {
        return worldProperties.loadOnStartup();
    }

    @Override
    public void setLoadOnStartup(boolean loadOnStartup) {
        worldProperties.setLoadOnStartup(loadOnStartup);
    }

    @Override
    public boolean doesKeepSpawnLoaded() {
        return worldProperties.doesKeepSpawnLoaded();
    }

    @Override
    public void setKeepSpawnLoaded(boolean keepSpawnLoaded) {
        worldProperties.setKeepSpawnLoaded(keepSpawnLoaded);
    }

    @Override
    public boolean doesGenerateSpawnOnLoad() {
        return worldProperties.doesGenerateSpawnOnLoad();
    }

    @Override
    public void setGenerateSpawnOnLoad(boolean generateSpawnOnLoad) {
        worldProperties.setGenerateSpawnOnLoad(generateSpawnOnLoad);
    }

    @Override
    public Vector3i getSpawnPosition() {
        return worldProperties.getSpawnPosition();
    }

    @Override
    public void setSpawnPosition(Vector3i spawnPosition) {
        worldProperties.setSpawnPosition(spawnPosition);
    }

    @Override
    public long getSeed() {
        return worldProperties.getSeed();
    }

    @Override
    public void setSeed(long seed) {
        worldProperties.setSeed(seed);
    }

    @Override
    public long getTotalTime() {
        return worldProperties.getTotalTime();
    }

    @Override
    public long getWorldTime() {
        return worldProperties.getWorldTime();
    }

    @Override
    public void setWorldTime(long worldTime) {
        worldProperties.setWorldTime(worldTime);
    }

    @Override
    public boolean isPVPEnabled() {
        return worldProperties.isPVPEnabled();
    }

    @Override
    public void setPVPEnabled(boolean pvpEnabled) {
        worldProperties.setPVPEnabled(pvpEnabled);
    }

    @Override
    public boolean isRaining() {
        return worldProperties.isRaining();
    }

    @Override
    public void setRaining(boolean raining) {
        worldProperties.setRaining(raining);
    }

    @Override
    public int getRainTime() {
        return worldProperties.getRainTime();
    }

    @Override
    public void setRainTime(int rainTime) {
        worldProperties.setRainTime(rainTime);
    }

    @Override
    public boolean isThundering() {
        return worldProperties.isThundering();
    }

    @Override
    public void setThundering(boolean thundering) {
        worldProperties.setThundering(thundering);
    }

    @Override
    public int getThunderTime() {
        return worldProperties.getThunderTime();
    }

    @Override
    public void setThunderTime(int thunderTime) {
        worldProperties.setThunderTime(thunderTime);
    }

    @Override
    public EnumGameMode getGameMode() {
        return EnumGameMode.valueOf(worldProperties.getGameMode().getName());
    }

    @Override
    public void setGameMode(EnumGameMode gameMode) {
        worldProperties.setGameMode(Sponge.getRegistry().getType(GameMode.class, gameMode.name()).orElse(GameModes.SURVIVAL));
    }

    @Override
    public boolean usesMapFeatures() {
        return worldProperties.usesMapFeatures();
    }

    @Override
    public void setMapFeaturesEnabled(boolean mapFeaturesEnabled) {
        worldProperties.setMapFeaturesEnabled(mapFeaturesEnabled);
    }

    @Override
    public boolean isHardcore() {
        return worldProperties.isHardcore();
    }

    @Override
    public void setHardcore(boolean hardcore) {
        worldProperties.setHardcore(hardcore);
    }

    @Override
    public boolean areCommandsAllowed() {
        return worldProperties.areCommandsAllowed();
    }

    @Override
    public void setCommandsAllowed(boolean commandsAllowed) {
        worldProperties.setCommandsAllowed(commandsAllowed);
    }

    @Override
    public EnumDifficulty getDifficulty() {
        return EnumDifficulty.valueOf(worldProperties.getDifficulty().getName());
    }

    @Override
    public void setDifficulty(EnumDifficulty difficulty) {
        worldProperties.setDifficulty(Sponge.getRegistry().getType(Difficulty.class, difficulty.name()).orElse(Difficulties.NORMAL));
    }

    @Override
    public boolean doesGenerateBonusChest() {
        return worldProperties.doesGenerateBonusChest();
    }

    @Override
    public Optional<String> getGameRule(String name) {
        return worldProperties.getGameRule(name);
    }

    @Override
    public Map<String, String> getGameRules() {
        return worldProperties.getGameRules();
    }

    @Override
    public void setGameRule(String name, String value) {
        worldProperties.setGameRule(name, value);
    }

    @Override
    public boolean removeGameRule(String name) {
        return worldProperties.removeGameRule(name);
    }

    @Override
    public int getMaxHeight() {
        return 255;
    }
}

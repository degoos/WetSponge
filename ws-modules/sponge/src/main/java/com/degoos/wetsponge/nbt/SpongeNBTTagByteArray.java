package com.degoos.wetsponge.nbt;

import net.minecraft.nbt.NBTTagByteArray;

public class SpongeNBTTagByteArray extends SpongeNBTBase implements WSNBTTagByteArray {

	public SpongeNBTTagByteArray(NBTTagByteArray nbtTagByteArray) {
		super(nbtTagByteArray);
	}

	public SpongeNBTTagByteArray(byte[] array) {
		this(new NBTTagByteArray(array));
	}

	@Override
	public byte[] getByteArray() {
		return getHandled().getByteArray();
	}


	@Override
	public WSNBTTagByteArray copy() {
		return new SpongeNBTTagByteArray((NBTTagByteArray) getHandled().copy());
	}

	@Override
	public NBTTagByteArray getHandled() {
		return (NBTTagByteArray) super.getHandled();
	}
}

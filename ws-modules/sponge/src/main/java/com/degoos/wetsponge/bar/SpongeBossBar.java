package com.degoos.wetsponge.bar;

import com.degoos.wetsponge.entity.living.player.SpongePlayer;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.enums.EnumBossBarColor;
import com.degoos.wetsponge.enums.EnumBossBarOverlay;
import com.degoos.wetsponge.parser.player.PlayerParser;
import com.degoos.wetsponge.text.SpongeText;
import com.degoos.wetsponge.text.WSText;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.boss.*;

import java.util.Set;
import java.util.stream.Collectors;

public class SpongeBossBar implements WSBossBar {

	protected ServerBossBar bossBar;

	public SpongeBossBar(ServerBossBar bossBar) {
		this.bossBar = bossBar;
	}

	@Override
	public SpongeBossBar addPlayer(WSPlayer player) {
		bossBar.addPlayer(((SpongePlayer) player).getHandled());
		return this;
	}

	@Override
	public SpongeBossBar removePlayer(WSPlayer player) {
		bossBar.removePlayer(((SpongePlayer) player).getHandled());
		return this;
	}

	@Override
	public SpongeBossBar clearPlayers() {
		bossBar.removePlayers(bossBar.getPlayers());
		return this;
	}

	@Override
	public Set<WSPlayer> getPlayers() {
		return bossBar.getPlayers().stream().map(player -> PlayerParser.getOrCreatePlayer(player, player.getUniqueId())).collect(Collectors.toSet());
	}

	@Override
	public EnumBossBarColor getColor() {
		return EnumBossBarColor.getByName(bossBar.getColor().getName()).orElse(EnumBossBarColor.PURPLE);
	}

	@Override
	public SpongeBossBar setColor(EnumBossBarColor color) {
		bossBar.setColor(Sponge.getRegistry().getType(BossBarColor.class, color.name()).orElse(BossBarColors.PURPLE));
		return this;
	}

	@Override
	public EnumBossBarOverlay getOverlay() {
		return EnumBossBarOverlay.getBySpongeName(bossBar.getOverlay().getName()).orElse(EnumBossBarOverlay.PROGRESS);
	}

	@Override
	public SpongeBossBar setOverlay(EnumBossBarOverlay overlay) {
		bossBar.setOverlay(Sponge.getRegistry().getType(BossBarOverlay.class, overlay.getSpongeName()).orElse(BossBarOverlays.PROGRESS));
		return this;
	}

	@Override
	public float getPercent() {
		return bossBar.getPercent();
	}

	@Override
	public SpongeBossBar setPercent(float percent) {
		bossBar.setPercent(percent);
		return this;
	}

	@Override
	public boolean shouldCreateFog() {
		return bossBar.shouldCreateFog();
	}

	@Override
	public SpongeBossBar setCreateFog(boolean createFog) {
		bossBar.setCreateFog(createFog);
		return this;
	}

	@Override
	public boolean shouldDarkenSky(boolean darkenSky) {
		return bossBar.shouldDarkenSky();
	}

	@Override
	public SpongeBossBar setDarkenSky(boolean darkenSky) {
		bossBar.setDarkenSky(darkenSky);
		return this;
	}

	@Override
	public boolean shouldPlayEndBossMusic() {
		return bossBar.shouldPlayEndBossMusic();
	}

	@Override
	public SpongeBossBar setPlayEndBossMusic(boolean playEndBossMusic) {
		bossBar.setPlayEndBossMusic(playEndBossMusic);
		return this;
	}

	@Override
	public boolean isVisible() {
		return bossBar.isVisible();
	}

	@Override
	public SpongeBossBar setVisible(boolean visible) {
		bossBar.setVisible(visible);
		return this;
	}

	@Override
	public WSText getName() {
		return SpongeText.of(bossBar.getName());
	}

	@Override
	public SpongeBossBar setName(WSText name) {
		bossBar.setName(((SpongeText) name).getHandled());
		return this;
	}

	@Override
	public ServerBossBar getHandled() {
		return bossBar;
	}

	public static class Builder implements WSBossBar.Builder {

		private ServerBossBar.Builder builder;

		public Builder() {
			builder = ServerBossBar.builder();
		}

		@Override
		public Builder color(EnumBossBarColor color) {
			builder.color(Sponge.getRegistry().getType(BossBarColor.class, color.name()).orElse(BossBarColors.PURPLE));
			return this;
		}

		@Override
		public Builder overlay(EnumBossBarOverlay overlay) {
			builder.overlay(Sponge.getRegistry().getType(BossBarOverlay.class, overlay.getSpongeName()).orElse(BossBarOverlays.PROGRESS));
			return this;
		}

		@Override
		public Builder percent(float percent) {
			builder.percent(percent);
			return this;
		}

		@Override
		public Builder createFog(boolean createFog) {
			builder.createFog(createFog);
			return this;
		}

		@Override
		public Builder darkenSky(boolean darkenSky) {
			builder.darkenSky(darkenSky);
			return this;
		}

		@Override
		public Builder playEndBossMusic(boolean endBossMusic) {
			builder.playEndBossMusic(endBossMusic);
			return this;
		}

		@Override
		public Builder visible(boolean visible) {
			builder.visible(visible);
			return this;
		}

		@Override
		public Builder name(WSText name) {
			builder.name(((SpongeText) name).getHandled());
			return this;
		}

		@Override
		public WSBossBar build() {
			return new SpongeBossBar(builder.build());
		}
	}

}

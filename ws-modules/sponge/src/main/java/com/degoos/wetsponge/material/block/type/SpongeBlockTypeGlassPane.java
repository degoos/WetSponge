package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.SpongeBlockTypeMultipleFacing;

import java.util.Objects;
import java.util.Set;

public class SpongeBlockTypeGlassPane extends SpongeBlockTypeMultipleFacing implements WSBlockTypeGlassPane {

    private boolean waterlogged;

    public SpongeBlockTypeGlassPane(int numericalId, String oldStringId, String newStringId, int maxStackSize, Set<EnumBlockFace> faces, Set<EnumBlockFace> allowedFaces, boolean waterlogged) {
        super(numericalId, oldStringId, newStringId, maxStackSize, faces, allowedFaces);
        this.waterlogged = waterlogged;
    }

    @Override
    public boolean isWaterlogged() {
        return waterlogged;
    }

    @Override
    public void setWaterlogged(boolean waterlogged) {
        this.waterlogged = waterlogged;
    }

    @Override
    public SpongeBlockTypeGlassPane clone() {
        return new SpongeBlockTypeGlassPane(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), getFaces(), getAllowedFaces(), waterlogged);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeGlassPane that = (SpongeBlockTypeGlassPane) o;
        return waterlogged == that.waterlogged;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), waterlogged);
    }
}

package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumAxis;
import com.degoos.wetsponge.enums.block.EnumBlockTypePrismarineType;
import com.degoos.wetsponge.enums.block.EnumBlockTypeQuartzType;
import com.degoos.wetsponge.material.block.SpongeBlockTypeOrientable;
import com.degoos.wetsponge.util.Validate;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.QuartzType;
import org.spongepowered.api.data.type.QuartzTypes;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.Objects;
import java.util.Set;

public class SpongeBlockTypeQuartz extends SpongeBlockTypeOrientable implements WSBlockTypeQuartz {

    private EnumBlockTypeQuartzType quartzType;

    public SpongeBlockTypeQuartz(EnumAxis axis, Set<EnumAxis> axes, EnumBlockTypeQuartzType quartzType) {
        super(155, "minecraft:quartz_block", "minecraft:quartz_block", 64, axis, axes);
        Validate.notNull(quartzType, "Quartz type cannot be null!");
        this.quartzType = quartzType;
    }

    @Override
    public String getNewStringId() {
        switch (quartzType) {
            case CHISELED:
                return "minecraft:chiseled_quartz_block";
            case PILLAR:
                return "minecraft:quartz_pillar";
            case NORMAL:
            default:
                return "minecraft:quartz_block";
        }
    }

    @Override
    public EnumBlockTypeQuartzType getQuartzType() {
        return quartzType;
    }

    @Override
    public void setQuartzType(EnumBlockTypeQuartzType quartzType) {
        Validate.notNull(quartzType, "Quartz type cannot be null!");
        this.quartzType = quartzType;
    }

    @Override
    public SpongeBlockTypeQuartz clone() {
        return new SpongeBlockTypeQuartz(getAxis(), getAxes(), quartzType);
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        itemStack.offer(Keys.QUARTZ_TYPE, toQuartzType(quartzType));
        return itemStack;
    }

    @Override
    public BlockState writeBlockState(BlockState blockState) {
        return blockState.with(Keys.QUARTZ_TYPE, toQuartzType(quartzType)).orElse(blockState);
    }

    @Override
    public SpongeBlockTypeQuartz readContainer(ValueContainer<?> valueContainer) {
        QuartzType type = valueContainer.get(Keys.QUARTZ_TYPE).orElse(QuartzTypes.DEFAULT);
        if (type.equals(QuartzTypes.DEFAULT)) {
            setAxis(EnumAxis.Y);
            quartzType = EnumBlockTypeQuartzType.NORMAL;
        } else if (type.equals(QuartzTypes.CHISELED)) {
            setAxis(EnumAxis.Y);
            quartzType = EnumBlockTypeQuartzType.CHISELED;
        } else if (type.equals(QuartzTypes.LINES_X)) {
            setAxis(EnumAxis.X);
            quartzType = EnumBlockTypeQuartzType.PILLAR;
        } else if (type.equals(QuartzTypes.LINES_Y)) {
            setAxis(EnumAxis.Y);
            quartzType = EnumBlockTypeQuartzType.PILLAR;
        } else {
            setAxis(EnumAxis.Z);
            quartzType = EnumBlockTypeQuartzType.PILLAR;
        }
        return this;
    }

    private QuartzType toQuartzType(EnumBlockTypeQuartzType type) {
        switch (type) {
            case CHISELED:
                return QuartzTypes.CHISELED;
            case PILLAR:
                switch (getAxis()) {
                    case X:
                        return QuartzTypes.LINES_X;
                    case Z:
                        return QuartzTypes.LINES_Z;
                    case Y:
                    default:
                        return QuartzTypes.LINES_Y;
                }
            case NORMAL:
            default:
                return QuartzTypes.DEFAULT;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeQuartz that = (SpongeBlockTypeQuartz) o;
        return quartzType == that.quartzType;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), quartzType);
    }
}

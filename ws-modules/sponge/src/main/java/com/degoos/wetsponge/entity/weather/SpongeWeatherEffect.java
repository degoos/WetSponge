package com.degoos.wetsponge.entity.weather;

import com.degoos.wetsponge.entity.SpongeEntity;
import org.spongepowered.api.entity.weather.WeatherEffect;

public class SpongeWeatherEffect extends SpongeEntity implements WSWeatherEffect {

	public SpongeWeatherEffect(WeatherEffect entity) {
		super(entity);
	}

	@Override
	public WeatherEffect getHandled() {
		return (WeatherEffect) super.getHandled();
	}
}

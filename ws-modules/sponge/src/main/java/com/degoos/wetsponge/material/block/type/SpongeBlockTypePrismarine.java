package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockTypePrismarineType;
import com.degoos.wetsponge.material.block.SpongeBlockType;
import com.degoos.wetsponge.util.Validate;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.PrismarineType;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.Objects;

public class SpongeBlockTypePrismarine extends SpongeBlockType implements WSBlockTypePrismarine {

    private EnumBlockTypePrismarineType prismarineType;

    public SpongeBlockTypePrismarine(EnumBlockTypePrismarineType prismarineType) {
        super(168, "minecraft:prismarine", "minecraft:prismarine", 64);
        Validate.notNull(prismarineType, "Prismarine type cannot be null!");
        this.prismarineType = prismarineType;
    }

    @Override
    public String getNewStringId() {
        switch (prismarineType) {
            case DARK:
                return "minecraft:dark_prismarine";
            case BRICKS:
                return "minecraft:prismarine_bricks";
            case ROUGH:
            default:
                return "minecraft:prismarine";
        }
    }

    @Override
    public EnumBlockTypePrismarineType getPrismarineType() {
        return prismarineType;
    }

    @Override
    public void setPrismarineType(EnumBlockTypePrismarineType prismarineType) {
        Validate.notNull(prismarineType, "Prismarine type cannot be null!");
        this.prismarineType = prismarineType;
    }

    @Override
    public SpongeBlockTypePrismarine clone() {
        return new SpongeBlockTypePrismarine(prismarineType);
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        super.writeItemStack(itemStack);
        itemStack.offer(Keys.PRISMARINE_TYPE, Sponge.getRegistry().getType(PrismarineType.class, prismarineType.name()).orElseThrow(NullPointerException::new));
        return itemStack;
    }

    @Override
    public BlockState writeBlockState(BlockState blockState) {
        blockState = super.writeBlockState(blockState);
        return blockState.with(Keys.PRISMARINE_TYPE, Sponge.getRegistry().getType(PrismarineType.class, prismarineType.name()).orElseThrow(NullPointerException::new)).orElse(blockState);
    }

    @Override
    public SpongeBlockTypePrismarine readContainer(ValueContainer<?> valueContainer) {
        super.readContainer(valueContainer);
        prismarineType = EnumBlockTypePrismarineType.getByName(valueContainer.get(Keys.PRISMARINE_TYPE).get().getName()).orElse(EnumBlockTypePrismarineType.ROUGH);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypePrismarine that = (SpongeBlockTypePrismarine) o;
        return prismarineType == that.prismarineType;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), prismarineType);
    }
}

package com.degoos.wetsponge.block.tileentity;

import com.degoos.wetsponge.block.SpongeBlock;
import com.degoos.wetsponge.item.WSItemStack;
import org.spongepowered.api.block.tileentity.Jukebox;
import org.spongepowered.api.item.inventory.ItemStack;

public class SpongeTileEntityJukebox extends SpongeTileEntity implements WSTileEntityJukebox {


    public SpongeTileEntityJukebox(SpongeBlock block) {
        super(block);
    }

    @Override
    public void playRecord() {
        getHandled().playRecord();
    }

    @Override
    public void stopRecord() {
        getHandled().stopRecord();
    }

    @Override
    public void ejectRecord() {
        getHandled().ejectRecord();
    }

    @Override
    public void insertRecord(WSItemStack record) {
        getHandled().insertRecord((ItemStack) record.getHandled());
    }

    @Override
    public Jukebox getHandled() {
        return (Jukebox) super.getHandled();
    }
}

package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.entity.SpongeEntity;
import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.packet.SpongePacket;
import com.flowpowered.math.vector.Vector2i;
import com.flowpowered.math.vector.Vector3d;
import java.lang.reflect.Field;
import java.util.Arrays;
import net.minecraft.entity.Entity;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.SPacketEntityTeleport;

public class SpongeSPacketEntityTeleport extends SpongePacket implements WSSPacketEntityTeleport {

	private int entityId;
	private Vector3d position;
	private Vector2i rotation;
	private boolean onGround, changed;

	public SpongeSPacketEntityTeleport(int entityId, Vector3d position, Vector2i rotation, boolean onGround) {
		super(new SPacketEntityTeleport());
		this.entityId = entityId;
		this.position = position;
		this.rotation = rotation;
		this.onGround = onGround;
		update();
	}

	public SpongeSPacketEntityTeleport(WSEntity entity) {
		super(new SPacketEntityTeleport((Entity) ((SpongeEntity) entity).getHandled()));
		refresh();
	}

	public SpongeSPacketEntityTeleport(Packet<?> packet) {
		super(packet);
		refresh();
	}

	@Override
	public int getEntityId() {
		return entityId;
	}

	@Override
	public void setEntityId(int entityId) {
		this.entityId = entityId;
		changed = true;
	}

	public Vector3d getPosition() {
		return position;
	}

	public void setPosition(Vector3d position) {
		this.position = position;
		changed = true;
	}

	public Vector2i getRotation() {
		return rotation;
	}

	public void setRotation(Vector2i rotation) {
		this.rotation = rotation;
		changed = true;
	}

	public boolean isOnGround() {
		return onGround;
	}

	public void setOnGround(boolean onGround) {
		this.onGround = onGround;
		changed = true;
	}

	@Override
	public void update() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));
			fields[0].setInt(getHandler(), entityId);
			fields[1].setDouble(getHandler(), position.getX());
			fields[2].setDouble(getHandler(), position.getY());
			fields[3].setDouble(getHandler(), position.getZ());
			fields[4].setByte(getHandler(), (byte) (rotation.getX() * 256F / 360F));
			fields[5].setByte(getHandler(), (byte) (rotation.getY() * 256F / 360F));
			fields[6].setBoolean(getHandler(), onGround);
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void refresh() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));
			entityId = fields[0].getInt(getHandler());
			position = new Vector3d(fields[1].getDouble(getHandler()), fields[2].getDouble(getHandler()), fields[3].getDouble(getHandler()));
			rotation = new Vector2i(((int) fields[4].getByte(getHandler())) * 360 / 256, ((int) fields[5].getByte(getHandler())) * 360 / 256);
			onGround = fields[6].getBoolean(getHandler());
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public boolean hasChanged() {
		return changed;
	}
}

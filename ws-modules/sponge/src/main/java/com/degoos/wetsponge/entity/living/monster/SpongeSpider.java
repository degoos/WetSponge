package com.degoos.wetsponge.entity.living.monster;


import net.minecraft.entity.monster.EntitySpider;
import org.spongepowered.api.entity.living.monster.Spider;

public class SpongeSpider extends SpongeMonster implements WSSpider {


	public SpongeSpider(Spider entity) {
		super(entity);
	}


	@Override
	public Spider getHandled() {
		return (Spider) super.getHandled();
	}

	@Override
	public boolean isClimbing() {
		return getHandled().isClimbing();
	}

	@Override
	public void setClimbing(boolean climbing) {
		((EntitySpider) getHandled()).setBesideClimbableBlock(climbing);
	}
}

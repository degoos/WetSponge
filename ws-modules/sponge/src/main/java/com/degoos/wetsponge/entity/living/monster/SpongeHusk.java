package com.degoos.wetsponge.entity.living.monster;

import org.spongepowered.api.entity.living.monster.Husk;

public class SpongeHusk extends SpongeZombie implements WSHusk {

	public SpongeHusk(Husk entity) {
		super(entity);
	}

	@Override
	public Husk getHandled() {
		return (Husk) super.getHandled();
	}
}

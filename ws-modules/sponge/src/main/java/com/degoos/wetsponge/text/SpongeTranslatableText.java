package com.degoos.wetsponge.text;

import com.degoos.wetsponge.text.translation.SpongeTranslation;
import com.degoos.wetsponge.text.translation.WSTranslation;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.TranslatableText;

public class SpongeTranslatableText extends SpongeText implements WSTranslatableText {

	private WSTranslation translation;

	public SpongeTranslatableText(TranslatableText text) {
		super(text);
		translation = new SpongeTranslation(text.getTranslation());
	}

	public SpongeTranslatableText(WSTranslation translation, Object... objects) {
		super(Text.of(((SpongeTranslation) translation).getHandled(), objects));
		this.translation = translation;
	}

	@Override
	public WSTranslation getTranslation() {
		return translation;
	}
}

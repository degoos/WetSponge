package com.degoos.wetsponge.block.tileentity;

import com.degoos.wetsponge.block.SpongeBlock;
import org.spongepowered.api.block.tileentity.carrier.BrewingStand;
import org.spongepowered.api.data.key.Keys;

public class SpongeTileEntityBrewingStand extends SpongeTileEntityNameableInventory implements WSTileEntityBrewingStand {


    public SpongeTileEntityBrewingStand(SpongeBlock block) {
        super(block);
    }

    @Override
    public int getFuelLevel() {
        return getHandled().get(Keys.MAX_BURN_TIME).orElse(0) - getHandled().get(Keys.PASSED_BURN_TIME).orElse(0);
    }

    @Override
    public void setFuelLevel(int fuelLevel) {
        getHandled().offer(Keys.PASSED_BURN_TIME, getHandled().get(Keys.MAX_BURN_TIME).orElse(0) - fuelLevel);
    }

    @Override
    public int getBrewingTime() {
        return getHandled().get(Keys.REMAINING_BREW_TIME).orElse(0);
    }

    @Override
    public void setBrewingTime(int brewingTime) {
        getHandled().offer(Keys.REMAINING_BREW_TIME, brewingTime);
    }


    @Override
    public BrewingStand getHandled() {
        return (BrewingStand) super.getHandled();
    }
}

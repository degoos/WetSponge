package com.degoos.wetsponge.material.item.type;

import com.degoos.wetsponge.enums.EnumDyeColor;
import com.degoos.wetsponge.material.item.SpongeItemType;
import com.degoos.wetsponge.util.Validate;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.DyeColor;
import org.spongepowered.api.data.type.DyeColors;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.Objects;

public class SpongeItemTypeDyeColored extends SpongeItemType implements WSItemTypeDyeColored {

    private EnumDyeColor dyeColor;

    public SpongeItemTypeDyeColored(int numericalId, String oldStringId, String newStringId, int maxStackSize, EnumDyeColor dyeColor) {
        super(numericalId, oldStringId, newStringId.startsWith("minecraft:") ? newStringId.substring(10) : newStringId, maxStackSize);
        Validate.notNull(dyeColor, "Dye color cannot be null!");
        this.dyeColor = dyeColor;
    }

    @Override
    public String getNewStringId() {
        return dyeColor.getMinecraftName().toLowerCase() + "_" + super.getNewStringId();
    }

    @Override
    public EnumDyeColor getDyeColor() {
        return dyeColor;
    }

    @Override
    public void setDyeColor(EnumDyeColor dyeColor) {
        Validate.notNull(dyeColor, "Dye color cannot be null!");
        this.dyeColor = dyeColor;
    }

    @Override
    public SpongeItemTypeDyeColored clone() {
        return new SpongeItemTypeDyeColored(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), dyeColor);
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        super.writeItemStack(itemStack);
        itemStack.offer(Keys.DYE_COLOR, Sponge.getRegistry().getType(DyeColor.class, dyeColor.getName()).orElse(DyeColors.WHITE));
        return itemStack;
    }

    @Override
    public SpongeItemTypeDyeColored readContainer(ValueContainer<?> valueContainer) {
        dyeColor = EnumDyeColor.getByName(valueContainer.get(Keys.DYE_COLOR).orElse(DyeColors.WHITE).getId()).orElse(EnumDyeColor.WHITE);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeItemTypeDyeColored that = (SpongeItemTypeDyeColored) o;
        return dyeColor == that.dyeColor;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), dyeColor);
    }
}

package com.degoos.wetsponge.material.block;

import com.degoos.wetsponge.enums.block.EnumBlockTypeRailShape;
import com.degoos.wetsponge.util.Validate;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.RailDirection;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class SpongeBlockTypeRail extends SpongeBlockType implements WSBlockTypeRail {

    private EnumBlockTypeRailShape shape;
    private Set<EnumBlockTypeRailShape> allowedShapes;

    public SpongeBlockTypeRail(int numericalId, String oldStringId, String newStringId, int maxStackSize, EnumBlockTypeRailShape shape, Set<EnumBlockTypeRailShape> allowedShapes) {
        super(numericalId, oldStringId, newStringId, maxStackSize);
        Validate.notNull(shape, "Shape cannot be null!");
        this.shape = shape;
        this.allowedShapes = allowedShapes;
    }

    @Override
    public EnumBlockTypeRailShape getShape() {
        return shape;
    }

    @Override
    public void setShape(EnumBlockTypeRailShape shape) {
        Validate.notNull(shape, "Shape cannot be null!");
        this.shape = shape;
    }

    @Override

    public Set<EnumBlockTypeRailShape> allowedShapes() {
        return new HashSet<>(allowedShapes);
    }

    @Override
    public SpongeBlockTypeRail clone() {
        return new SpongeBlockTypeRail(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), shape, new HashSet<>(allowedShapes));
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        return super.writeItemStack(itemStack);
    }

    @Override
    public BlockState writeBlockState(BlockState blockState) {
        blockState = super.writeBlockState(blockState);
        return blockState.with(Keys.RAIL_DIRECTION, Sponge.getRegistry().getType(RailDirection.class, shape.name())
                .orElseThrow(NullPointerException::new)).orElse(blockState);
    }

    @Override
    public SpongeBlockTypeRail readContainer(ValueContainer<?> valueContainer) {
        super.readContainer(valueContainer);
        shape = valueContainer.get(Keys.RAIL_DIRECTION).map(target -> EnumBlockTypeRailShape.valueOf(target.getName())).orElse(EnumBlockTypeRailShape.NORTH_SOUTH);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeRail that = (SpongeBlockTypeRail) o;
        return shape == that.shape &&
                Objects.equals(allowedShapes, that.allowedShapes);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), shape, allowedShapes);
    }
}

package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.enums.block.EnumBlockTypePistonType;
import com.degoos.wetsponge.material.block.SpongeBlockTypeDirectional;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.PistonTypes;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.Objects;
import java.util.Set;

public class SpongeBlockTypeTechnicalPiston extends SpongeBlockTypeDirectional implements WSBlockTypeTechnicalPiston {

    private EnumBlockTypePistonType pistonType;

    public SpongeBlockTypeTechnicalPiston(int numericalId, String oldStringId, String newStringId, int maxStackSize, EnumBlockFace facing, Set<EnumBlockFace> faces, EnumBlockTypePistonType pistonType) {
        super(numericalId, oldStringId, newStringId, maxStackSize, facing, faces);
        this.pistonType = pistonType;
    }

    @Override
    public EnumBlockTypePistonType getType() {
        return pistonType;
    }

    @Override
    public void setType(EnumBlockTypePistonType type) {
        this.pistonType = type;
    }

    @Override
    public SpongeBlockTypeTechnicalPiston clone() {
        return new SpongeBlockTypeTechnicalPiston(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), getFacing(), getFaces(), pistonType);
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        super.writeItemStack(itemStack);
        itemStack.offer(Keys.PISTON_TYPE, pistonType == EnumBlockTypePistonType.STICKY ? PistonTypes.STICKY : PistonTypes.NORMAL);
        return itemStack;
    }

    @Override
    public BlockState writeBlockState(BlockState blockState) {
        blockState = super.writeBlockState(blockState);
        return blockState.with(Keys.PISTON_TYPE, pistonType == EnumBlockTypePistonType.STICKY ? PistonTypes.STICKY : PistonTypes.NORMAL).orElse(blockState);
    }

    @Override
    public SpongeBlockTypeTechnicalPiston readContainer(ValueContainer<?> valueContainer) {
        super.readContainer(valueContainer);
        pistonType = valueContainer.get(Keys.PISTON_TYPE).orElse(PistonTypes.NORMAL).equals(PistonTypes.NORMAL)
                ? EnumBlockTypePistonType.NORMAL : EnumBlockTypePistonType.STICKY;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeTechnicalPiston that = (SpongeBlockTypeTechnicalPiston) o;
        return pistonType == that.pistonType;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), pistonType);
    }
}

package com.degoos.wetsponge.listener.sponge;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.event.server.WSServerListPingEvent;
import com.degoos.wetsponge.server.SpongeFavicon;
import com.degoos.wetsponge.server.WSFavicon;
import com.degoos.wetsponge.text.SpongeText;
import java.util.Optional;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.Order;
import org.spongepowered.api.event.server.ClientPingServerEvent;
import org.spongepowered.api.event.server.ClientPingServerEvent.Response;
import org.spongepowered.api.network.status.StatusResponse.Players;

public class SpongeServerListener {

	@Listener(order = Order.FIRST)
	public void onServerListPing(ClientPingServerEvent event) {
		Response response = event.getResponse();
		Optional<WSFavicon> favicon = response.getFavicon().map(SpongeFavicon::new);
		WSServerListPingEvent wetSpongeEvent = new WSServerListPingEvent(SpongeText.of(response.getDescription()), favicon, response.getPlayers().map(Players::getMax)
			.orElse(0), event.getClient().getAddress().getAddress());
		WetSponge.getEventManager().callEvent(wetSpongeEvent);
		if (wetSpongeEvent.getFavicon().isPresent()) response.setFavicon(((SpongeFavicon) wetSpongeEvent.getFavicon().get()).getHandled());
		else response.setFavicon(null);
		response.getPlayers().ifPresent(players -> players.setMax(wetSpongeEvent.getMaxPlayers()));
		response.setDescription(((SpongeText) wetSpongeEvent.getDescription()).getHandled());
	}

}

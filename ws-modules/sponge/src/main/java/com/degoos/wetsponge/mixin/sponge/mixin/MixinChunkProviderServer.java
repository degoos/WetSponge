package com.degoos.wetsponge.mixin.sponge.mixin;

import com.degoos.wetsponge.mixin.sponge.interfaces.WSMixinChunkProviderServer;
import it.unimi.dsi.fastutil.longs.Long2ObjectMap;
import net.minecraft.crash.CrashReport;
import net.minecraft.crash.CrashReportCategory;
import net.minecraft.util.ReportedException;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.ChunkProviderServer;
import net.minecraft.world.gen.IChunkGenerator;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(value = ChunkProviderServer.class, priority = Integer.MAX_VALUE)
public abstract class MixinChunkProviderServer implements WSMixinChunkProviderServer, IChunkProvider {

	@Shadow
	private IChunkGenerator chunkGenerator;

	@Shadow
	private Long2ObjectMap<Chunk> loadedChunks;

	@Shadow
	public abstract Chunk loadChunk(int x, int z);


	@Override
	public Chunk vanillaProvideChunk(int p_provideChunk_1_, int p_provideChunk_2_) {
		Chunk lvt_3_1_ = this.loadChunk(p_provideChunk_1_, p_provideChunk_2_);
		if (lvt_3_1_ == null) {
			long lvt_4_1_ = ChunkPos.asLong(p_provideChunk_1_, p_provideChunk_2_);

			try {
				lvt_3_1_ = this.chunkGenerator.generateChunk(p_provideChunk_1_, p_provideChunk_2_);
			} catch (Throwable var9) {
				CrashReport lvt_7_1_ = CrashReport.makeCrashReport(var9, "Exception generating new chunk");
				CrashReportCategory lvt_8_1_ = lvt_7_1_.makeCategory("Chunk to be generated");
				lvt_8_1_.addCrashSection("Location", String.format("%d,%d", p_provideChunk_1_, p_provideChunk_2_));
				lvt_8_1_.addCrashSection("Position hash", lvt_4_1_);
				lvt_8_1_.addCrashSection("Generator", this.chunkGenerator);
				throw new ReportedException(lvt_7_1_);
			}

			this.loadedChunks.put(lvt_4_1_, lvt_3_1_);
			lvt_3_1_.onLoad();
			lvt_3_1_.populate(this, this.chunkGenerator);
		}

		return lvt_3_1_;
	}

}

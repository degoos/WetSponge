package com.degoos.wetsponge.listener.sponge;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.block.SpongeBlock;
import com.degoos.wetsponge.event.block.WSSignChangeEvent;
import com.degoos.wetsponge.text.SpongeText;
import com.degoos.wetsponge.util.InternalLogger;
import java.util.stream.Collectors;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.Order;
import org.spongepowered.api.event.block.tileentity.ChangeSignEvent;
import org.spongepowered.api.event.filter.cause.First;

public class SpongeSignListener {

	@Listener(order = Order.FIRST)
	public void onSignChange(ChangeSignEvent event, @First Player player) {
		try {
			WSSignChangeEvent wetSpongeEvent = new WSSignChangeEvent(new SpongeBlock(event.getTargetTile().getLocation()), WetSponge.getServer()
				.getPlayer(player.getName()).orElse(null), event.getText().lines().getAll().stream().map(SpongeText::of).collect(Collectors.toList()));
			WetSponge.getEventManager().callEvent(wetSpongeEvent);
			event.getText().set(Keys.SIGN_LINES, wetSpongeEvent.getLines().stream().map(line -> ((SpongeText) line).getHandled()).collect(Collectors.toList()));
			event.setCancelled(wetSpongeEvent.isCancelled());
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Sponge-SignChangeEvent!");
		}
	}

}

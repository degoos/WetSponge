package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.enums.block.EnumBlockTypeBisectedHalf;
import com.degoos.wetsponge.enums.block.EnumBlockTypeDoorHinge;
import com.degoos.wetsponge.material.block.SpongeBlockTypeDirectional;
import com.degoos.wetsponge.util.Validate;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.Hinges;
import org.spongepowered.api.data.type.PortionTypes;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.Objects;
import java.util.Set;

public class SpongeBlockTypeDoor extends SpongeBlockTypeDirectional implements WSBlockTypeDoor {

    private EnumBlockTypeDoorHinge hinge;
    private EnumBlockTypeBisectedHalf half;
    private boolean open, powered;

    public SpongeBlockTypeDoor(int numericalId, String oldStringId, String newStringId, int maxStackSize, EnumBlockFace facing, Set<EnumBlockFace> faces,
                               EnumBlockTypeDoorHinge hinge, EnumBlockTypeBisectedHalf half, boolean open, boolean powered) {
        super(numericalId, oldStringId, newStringId, maxStackSize, facing, faces);
        Validate.notNull(hinge, "Hinge cannot be null!");
        Validate.notNull(half, "Half cannot be null!");
        this.hinge = hinge;
        this.half = half;
        this.open = open;
        this.powered = powered;
    }

    @Override
    public EnumBlockTypeDoorHinge getHinge() {
        return hinge;
    }

    @Override
    public void setHinge(EnumBlockTypeDoorHinge hinge) {
        Validate.notNull(hinge, "Hinge cannot be null!");
        this.hinge = hinge;
    }

    @Override
    public EnumBlockTypeBisectedHalf getHalf() {
        return half;
    }

    @Override
    public void setHalf(EnumBlockTypeBisectedHalf half) {
        Validate.notNull(half, "Half cannot be null!");
        this.half = half;
    }

    @Override
    public boolean isOpen() {
        return open;
    }

    @Override
    public void setOpen(boolean open) {
        this.open = open;
    }

    @Override
    public boolean isPowered() {
        return powered;
    }

    @Override
    public void setPowered(boolean powered) {
        this.powered = powered;
    }

    @Override
    public SpongeBlockTypeDoor clone() {
        return new SpongeBlockTypeDoor(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), getFacing(), getFaces(), hinge, half, open, powered);
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        super.writeItemStack(itemStack);
        itemStack.offer(Keys.HINGE_POSITION, hinge == EnumBlockTypeDoorHinge.LEFT ? Hinges.LEFT : Hinges.RIGHT);
        itemStack.offer(Keys.OPEN, open);
        itemStack.offer(Keys.PORTION_TYPE, half == EnumBlockTypeBisectedHalf.TOP ? PortionTypes.TOP : PortionTypes.BOTTOM);
        itemStack.offer(Keys.POWERED, powered);
        return itemStack;
    }

    @Override
    public BlockState writeBlockState(BlockState blockState) {
        blockState = super.writeBlockState(blockState);
        blockState = blockState.with(Keys.HINGE_POSITION, hinge == EnumBlockTypeDoorHinge.LEFT ? Hinges.LEFT : Hinges.RIGHT).orElse(blockState);
        blockState = blockState.with(Keys.OPEN, open).orElse(blockState);
        blockState = blockState.with(Keys.PORTION_TYPE, half == EnumBlockTypeBisectedHalf.TOP ? PortionTypes.TOP : PortionTypes.BOTTOM).orElse(blockState);
        return blockState.with(Keys.POWERED, powered).orElse(blockState);
    }

    @Override
    public SpongeBlockTypeDoor readContainer(ValueContainer<?> valueContainer) {
        super.readContainer(valueContainer);
        hinge = valueContainer.get(Keys.HINGE_POSITION).orElse(Hinges.LEFT).equals(Hinges.LEFT) ? EnumBlockTypeDoorHinge.LEFT : EnumBlockTypeDoorHinge.RIGHT;
        open = valueContainer.get(Keys.OPEN).orElse(false);
        half = valueContainer.get(Keys.PORTION_TYPE).orElse(PortionTypes.BOTTOM).equals(PortionTypes.BOTTOM) ? EnumBlockTypeBisectedHalf.BOTTOM : EnumBlockTypeBisectedHalf.TOP;
        powered = valueContainer.get(Keys.POWERED).orElse(false);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeDoor that = (SpongeBlockTypeDoor) o;
        return open == that.open &&
                powered == that.powered &&
                hinge == that.hinge &&
                half == that.half;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), hinge, half, open, powered);
    }
}

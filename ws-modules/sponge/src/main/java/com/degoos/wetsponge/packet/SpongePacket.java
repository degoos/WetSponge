package com.degoos.wetsponge.packet;

import net.minecraft.network.Packet;

public abstract class SpongePacket implements WSPacket {

    private Packet<?> packet;

    public SpongePacket(Packet<?> packet) {
        this.packet = packet;
    }

    public Packet<?> getHandler() {
        return packet;
    }
}

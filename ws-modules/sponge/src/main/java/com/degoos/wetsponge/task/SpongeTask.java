package com.degoos.wetsponge.task;


import com.degoos.wetsponge.SpongeWetSponge;
import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumTextColor;
import com.degoos.wetsponge.plugin.WSPlugin;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.util.InternalLogger;
import org.spongepowered.api.scheduler.Task;

import java.util.UUID;
import java.util.function.Consumer;

public class SpongeTask implements WSTask {

	private Consumer<WSTask> consumer;
	private Task task;
	private long times, timesExecuted;
	private WSPlugin plugin;
	private StackTraceElement caller;
	private String taskDescription;
	private boolean timer, instantaneous, later;
	private long delay, interval;

	public SpongeTask(Runnable runnable) {
		this.times = -1;
		this.timesExecuted = 0;
		caller = Thread.currentThread().getStackTrace()[3];
		consumer = (wsTask -> {
			try {
				if (times > -1) {
					if (times <= timesExecuted) {
						cancel();
						return;
					}
				}

				WSPlugin caller = WetSponge.getTimings().getAssignedPlugin();

				WetSponge.getTimings().assignPluginToThread(plugin);
				WetSponge.getTimings().startTiming(taskDescription + " (From " + SpongeTask.this.caller + ")");

				runnable.run();

				WetSponge.getTimings().stopTiming();
				WetSponge.getTimings().assignPluginToThread(caller);

				this.timesExecuted++;
			} catch (Throwable ex) {
				InternalLogger.printException(ex, WSText.builder("An error has occurred while WetSponge was executing the task ")
						.append(WSText.of(task.getUniqueId().toString(), EnumTextColor.YELLOW)).append(WSText.of("!")).build());
			}
		});
	}


	public SpongeTask(Consumer<WSTask> consumer) {
		this.times = -1;
		caller = Thread.currentThread().getStackTrace()[3];
		this.consumer = (wsTask -> {
			try {
				if (times > -1) {
					if (times <= timesExecuted) {
						cancel();
						return;
					}
				}

				WSPlugin caller = WetSponge.getTimings().getAssignedPlugin();

				WetSponge.getTimings().assignPluginToThread(plugin);
				WetSponge.getTimings().startTiming(taskDescription + " (UUID: " + getUniqueId() + ") (From " + SpongeTask.this.caller + ")");

				consumer.accept(SpongeTask.this);

				WetSponge.getTimings().stopTiming();
				WetSponge.getTimings().assignPluginToThread(caller);


				this.timesExecuted++;
			} catch (Throwable ex) {
				InternalLogger.printException(ex, WSText.builder("An error has occurred while WetSponge was executing the task ")
						.append(WSText.of(task.getUniqueId().toString(), EnumTextColor.YELLOW)).append(WSText.of("!")).build());
			}
		});
	}


	@Override
	public void run(WSPlugin plugin) {
		this.plugin = plugin;
		this.instantaneous = true;
		this.later = false;
		this.timer = false;
		taskDescription = plugin.getId() + " task. (Run)";
		task = Task.builder().execute((task1 -> consumer.accept(SpongeTask.this))).submit(SpongeWetSponge.getInstance());
	}


	@Override
	public void runAsynchronously(WSPlugin plugin) {
		this.plugin = plugin;
		this.instantaneous = true;
		this.later = false;
		this.timer = false;
		taskDescription = plugin.getId() + " task. (Run) (Async)";
		task = Task.builder().async().execute((task1 -> consumer.accept(SpongeTask.this))).submit(SpongeWetSponge.getInstance());
	}


	@Override
	public void runTaskLater(long delay, WSPlugin plugin) {
		if (!WSTaskPool.addTask(plugin, this)) return;
		this.plugin = plugin;
		this.instantaneous = false;
		this.later = true;
		this.timer = false;
		this.delay = delay;
		taskDescription = plugin.getId() + " task. (Later: " + delay + ")";
		task = Task.builder().delayTicks(delay).execute((task1 -> consumer.accept(SpongeTask.this))).submit(SpongeWetSponge.getInstance());
	}


	@Override
	public void runTaskLaterAsynchronously(long delay, WSPlugin plugin) {
		if (!WSTaskPool.addTask(plugin, this)) return;
		this.plugin = plugin;
		this.instantaneous = false;
		this.later = true;
		this.timer = false;
		this.delay = delay;
		taskDescription = plugin.getId() + " task. (Later: " + delay + ") (Async)";
		task = Task.builder().delayTicks(delay).async().execute((task1 -> consumer.accept(SpongeTask.this))).submit(SpongeWetSponge.getInstance());
	}


	@Override
	public void runTaskTimer(long delay, long interval, WSPlugin plugin) {
		if (!WSTaskPool.addTask(plugin, this)) return;
		this.plugin = plugin;
		this.instantaneous = false;
		this.later = false;
		this.timer = true;
		this.delay = delay;
		this.interval = interval;
		taskDescription = plugin.getId() + " task. (Timer: " + delay + ", " + interval + ")";
		task = Task.builder().delayTicks(delay).intervalTicks(interval).execute((task1 -> consumer.accept(SpongeTask.this))).submit(SpongeWetSponge.getInstance());
	}

	@Override
	public void runTaskTimer(long delay, long interval, long times, WSPlugin plugin) {
		this.times = times;
		this.instantaneous = false;
		this.later = false;
		this.timer = true;
		this.delay = delay;
		this.interval = interval;
		taskDescription = plugin.getId() + " task. (Timer: " + delay + ", " + interval + ", " + times + ")";
		runTaskTimer(delay, interval, plugin);
	}


	@Override
	public void runTaskTimerAsynchronously(long delay, long interval, WSPlugin plugin) {
		if (!WSTaskPool.addTask(plugin, this)) return;
		this.plugin = plugin;
		this.instantaneous = false;
		this.later = false;
		this.timer = true;
		this.delay = delay;
		this.interval = interval;
		taskDescription = plugin.getId() + " task. (Timer: " + delay + ", " + interval + ") (Async)";
		task = Task.builder().delayTicks(delay).intervalTicks(interval).async().execute((task1 -> consumer.accept(SpongeTask.this)))
				.submit(SpongeWetSponge.getInstance());
	}

	@Override
	public void runTaskTimerAsynchronously(long delay, long interval, long times, WSPlugin plugin) {
		this.times = times;
		this.instantaneous = false;
		this.later = false;
		this.timer = true;
		this.delay = delay;
		this.interval = interval;
		taskDescription = plugin.getId() + " task. (Timer: " + delay + ", " + interval + ", " + times + ") (Async)";
		runTaskTimerAsynchronously(delay, interval, plugin);
	}

	@Override
	public UUID getUniqueId() {
		return task == null ? null : task.getUniqueId();
	}

	@Override
	public StackTraceElement getCallerStackTraceElement() {
		return caller;
	}

	@Override
	public long getTimesExecuted() {
		return timesExecuted;
	}

	@Override
	public boolean isAsynchronous() {
		return task != null && task.isAsynchronous();
	}

	@Override
	public boolean isTimer() {
		return timer;
	}

	@Override
	public boolean isInstantaneous() {
		return instantaneous;
	}

	@Override
	public boolean isLater() {
		return later;
	}

	@Override
	public long getDelay() {
		return delay;
	}

	@Override
	public long getInterval() {
		return interval;
	}

	@Override
	public long getTimesToExecute() {
		return times;
	}

	@Override
	public WSPlugin getPlugin() {
		return plugin;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		SpongeTask that = (SpongeTask) o;

		return task != null ? task.equals(that.task) : that.task == null;
	}

	@Override
	public int hashCode() {
		return task != null ? task.hashCode() : 0;
	}

	@Override
	public void cancel() {
		if (task != null) task.cancel();
	}

	/*public Timing getTimingsHandler() {
		if (this.taskTimer == null) {
			this.taskTimer = WetSpongeTimings.getPluginTaskTimings(this, times);
		}
		return this.taskTimer;
	}*/
}

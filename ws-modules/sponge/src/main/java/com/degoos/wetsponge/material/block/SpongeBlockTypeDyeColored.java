package com.degoos.wetsponge.material.block;

import com.degoos.wetsponge.enums.EnumDyeColor;
import com.degoos.wetsponge.material.block.type.WSBlockTypeDyeColored;
import com.degoos.wetsponge.util.Validate;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.DyeColor;
import org.spongepowered.api.data.type.DyeColors;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.Objects;

public class SpongeBlockTypeDyeColored extends SpongeBlockType implements WSBlockTypeDyeColored {

    private EnumDyeColor dyeColor;

    public SpongeBlockTypeDyeColored(int numericalId, String oldStringId, String newStringId, int maxStackSize, EnumDyeColor dyeColor) {
        super(numericalId, oldStringId, newStringId.startsWith("minecraft:") ? newStringId.substring(10) : newStringId, maxStackSize);
        Validate.notNull(dyeColor, "Dye color cannot be null!");
        this.dyeColor = dyeColor;
    }

    @Override
    public String getNewStringId() {
        return "minecraft:" + dyeColor.getMinecraftName().toLowerCase() + "_" + super.getNewStringId();
    }

    @Override
    public EnumDyeColor getDyeColor() {
        return dyeColor;
    }

    @Override
    public void setDyeColor(EnumDyeColor dyeColor) {
        Validate.notNull(dyeColor, "Dye color cannot be null!");
        this.dyeColor = dyeColor;
    }

    @Override
    public SpongeBlockTypeDyeColored clone() {
        return new SpongeBlockTypeDyeColored(getNumericalId(), getOldStringId(), super.getNewStringId(), getMaxStackSize(), dyeColor);
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        super.writeItemStack(itemStack);
        itemStack.offer(Keys.DYE_COLOR, Sponge.getRegistry().getType(DyeColor.class, dyeColor.getName()).orElse(DyeColors.WHITE));
        return itemStack;
    }

    @Override
    public BlockState writeBlockState(BlockState blockState) {
        blockState = super.writeBlockState(blockState);
        return blockState.with(Keys.DYE_COLOR, Sponge.getRegistry().getType(DyeColor.class, dyeColor.getName()).orElse(DyeColors.WHITE)).orElse(blockState);
    }

    @Override
    public SpongeBlockTypeDyeColored readContainer(ValueContainer<?> valueContainer) {
        super.readContainer(valueContainer);
        dyeColor = EnumDyeColor.getByName(valueContainer.get(Keys.DYE_COLOR).orElse(DyeColors.WHITE).getId()).orElse(EnumDyeColor.WHITE);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeDyeColored that = (SpongeBlockTypeDyeColored) o;
        return dyeColor == that.dyeColor;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), dyeColor);
    }
}

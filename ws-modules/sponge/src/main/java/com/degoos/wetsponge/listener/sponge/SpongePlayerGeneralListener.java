package com.degoos.wetsponge.listener.sponge;

import com.degoos.wetsponge.SpongeWetSponge;
import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.block.SpongeBlock;
import com.degoos.wetsponge.data.WSTransaction;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.entity.other.WSItem;
import com.degoos.wetsponge.enums.EnumResourcePackStatus;
import com.degoos.wetsponge.event.entity.WSEntityDismountEvent;
import com.degoos.wetsponge.event.entity.WSEntityMountEvent;
import com.degoos.wetsponge.event.entity.WSEntityRideEvent;
import com.degoos.wetsponge.event.entity.player.WSPlayerDropItemEvent;
import com.degoos.wetsponge.event.entity.player.WSPlayerPickupItemEvent;
import com.degoos.wetsponge.event.entity.player.WSPlayerResourcePackStatusEvent;
import com.degoos.wetsponge.event.entity.player.WSPlayerRespawnEvent;
import com.degoos.wetsponge.event.entity.player.bed.WSPlayerEnterBedEvent;
import com.degoos.wetsponge.event.entity.player.bed.WSPlayerLeaveBedEvent;
import com.degoos.wetsponge.parser.entity.SpongeEntityParser;
import com.degoos.wetsponge.parser.player.PlayerParser;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.world.SpongeLocation;
import com.degoos.wetsponge.world.WSLocation;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.Item;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.Order;
import org.spongepowered.api.event.action.SleepingEvent;
import org.spongepowered.api.event.entity.CollideEntityEvent;
import org.spongepowered.api.event.entity.RideEntityEvent;
import org.spongepowered.api.event.entity.living.humanoid.player.ResourcePackStatusEvent;
import org.spongepowered.api.event.entity.living.humanoid.player.RespawnPlayerEvent;
import org.spongepowered.api.event.filter.cause.First;
import org.spongepowered.api.event.item.inventory.DropItemEvent;
import org.spongepowered.api.scheduler.Task;

public class SpongePlayerGeneralListener {

	@Listener(order = Order.FIRST)
	public void onBedEnter(SleepingEvent.Post event) {
		try {
			if (!(event.getTargetEntity() instanceof Player) || !event.getBed().getLocation().isPresent()) return;
			WSPlayerEnterBedEvent wetSpongeEvent = new WSPlayerEnterBedEvent(PlayerParser.getPlayer(event.getTargetEntity().getUniqueId())
				.orElse(null), new SpongeBlock(event.getBed().getLocation().get()));
			WetSponge.getEventManager().callEvent(wetSpongeEvent);
			event.setCancelled(wetSpongeEvent.isCancelled());
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Sponge-SleepingEvent.Post!");
		}
	}

	@Listener(order = Order.FIRST)
	public void onBedLeave(SleepingEvent.Finish event) {
		try {
			if (!(event.getTargetEntity() instanceof Player) || !event.getBed().getLocation().isPresent()) return;
			WSPlayerLeaveBedEvent wetSpongeEvent = new WSPlayerLeaveBedEvent(PlayerParser.getPlayer(event.getTargetEntity().getUniqueId())
				.orElse(null), new SpongeBlock(event.getBed().getLocation().get()));
			WetSponge.getEventManager().callEvent(wetSpongeEvent);
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Sponge-SleepingEvent.Finish!");
		}
	}


	@Listener(order = Order.FIRST)
	public void onResourcePackStatus(ResourcePackStatusEvent event) {
		try {
			WetSponge.getEventManager()
				.callEvent(new WSPlayerResourcePackStatusEvent(WetSponge.getServer().getPlayer(event.getPlayer().getUniqueId()).orElse(null), EnumResourcePackStatus
					.getBySpongeName(event.getStatus().name()).orElseThrow(NullPointerException::new)));
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Sponge-ResourcePackStatusEvent!");
		}
	}

	@Listener(order = Order.FIRST)
	public void onRespawn(RespawnPlayerEvent event) {
		try {
			PlayerParser.resetPlayer(event.getTargetEntity(), event.getTargetEntity().getUniqueId());
			WSPlayer player = WetSponge.getServer().getPlayer(event.getTargetEntity().getUniqueId()).orElse(null);
			WSTransaction<WSLocation> transaction = new WSTransaction<>(new SpongeLocation(event.getFromTransform()), new SpongeLocation(event.getToTransform()));
			WSPlayerRespawnEvent wetSpongeEvent = new WSPlayerRespawnEvent(player, transaction, event.isBedSpawn());
			WetSponge.getEventManager().callEvent(wetSpongeEvent);
			event.setToTransform(((SpongeLocation) transaction.getNewData()).getLocation());

			Task.builder().delayTicks(10).execute(() -> player.getFakeBlocks().forEach((location, type) -> {
				if (location.distance(player.getLocation()) <= 100) player.refreshFakeBlock(location);
			})).submit(SpongeWetSponge.getInstance());
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Sponge-RespawnPlayerEvent!");
		}
	}

	@Listener(order = Order.FIRST)
	public void onDrop(DropItemEvent.Dispense event, @First Player player) {
		try {
			WSPlayer wetSpongePlayer = PlayerParser.getPlayer(player.getUniqueId()).orElseThrow(NullPointerException::new);
			event.getEntities().forEach(entity -> {
				WSPlayerDropItemEvent wetSpongeEvent = new WSPlayerDropItemEvent(wetSpongePlayer, (WSItem) SpongeEntityParser.getWSEntity(entity));
				WetSponge.getEventManager().callEvent(wetSpongeEvent);
				event.setCancelled(wetSpongeEvent.isCancelled());
			});
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Sponge-DropItemEvent.Dispense!");
		}
	}

	@Listener(order = Order.FIRST)
	public void onPickup(CollideEntityEvent event, @First Player player) {
		try {
			WSPlayer wetSpongePlayer = PlayerParser.getPlayer(player.getUniqueId()).orElse(null);
			if (wetSpongePlayer == null) return;
			event.getEntities().stream().filter(entity -> entity instanceof Item && entity.get(Keys.PICKUP_DELAY).orElse(0) <= 0).forEach(item -> {
				if (event.isCancelled()) return;
				WSItem wsItem = (WSItem) SpongeEntityParser.getWSEntity(item);
				if (wsItem.getProperty("WS_PickedUp").isPresent()) return;
				WSPlayerPickupItemEvent wetSpongeEvent = new WSPlayerPickupItemEvent(wetSpongePlayer, wsItem);
				WetSponge.getEventManager().callEvent(wetSpongeEvent);
				event.setCancelled(wetSpongeEvent.isCancelled());
				wsItem.addProperty("WS_PickedUp", true);
			});
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Sponge-CollideEntityEvent!");
		}
	}

	@Listener(order = Order.FIRST)
	public void onRide(RideEntityEvent event, @First Entity entity) {
		try {
			WSEntityRideEvent wetSpongeEvent;
			if (event instanceof RideEntityEvent.Mount)
				wetSpongeEvent = new WSEntityMountEvent(SpongeEntityParser.getWSEntity(entity), SpongeEntityParser.getWSEntity(event.getTargetEntity()));
			else if (event instanceof RideEntityEvent.Dismount)
				wetSpongeEvent = new WSEntityDismountEvent(SpongeEntityParser.getWSEntity(entity), SpongeEntityParser.getWSEntity(event.getTargetEntity()));
			else wetSpongeEvent = new WSEntityRideEvent(SpongeEntityParser.getWSEntity(entity), SpongeEntityParser.getWSEntity(event.getTargetEntity()));
			WetSponge.getEventManager().callEvent(wetSpongeEvent);
			event.setCancelled(wetSpongeEvent.isCancelled());
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Sponge-EntityRideEvent!");
		}
	}
}

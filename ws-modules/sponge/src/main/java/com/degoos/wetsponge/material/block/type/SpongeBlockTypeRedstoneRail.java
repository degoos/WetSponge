package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockTypeRailShape;
import com.degoos.wetsponge.material.block.SpongeBlockTypeRail;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.Objects;
import java.util.Set;

public class SpongeBlockTypeRedstoneRail extends SpongeBlockTypeRail implements WSBlockTypeRedstoneRail {

    private boolean powered;

    public SpongeBlockTypeRedstoneRail(int numericalId, String oldStringId, String newStringId, int maxStackSize,
                                       EnumBlockTypeRailShape shape, Set<EnumBlockTypeRailShape> allowedShapes, boolean powered) {
        super(numericalId, oldStringId, newStringId, maxStackSize, shape, allowedShapes);
        this.powered = powered;
    }

    @Override
    public boolean isPowered() {
        return powered;
    }

    @Override
    public void setPowered(boolean powered) {
        this.powered = powered;
    }

    @Override
    public SpongeBlockTypeRedstoneRail clone() {
        return new SpongeBlockTypeRedstoneRail(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), getShape(), allowedShapes(), powered);
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        super.writeItemStack(itemStack);
        itemStack.offer(Keys.POWERED, powered);
        return itemStack;
    }

    @Override
    public BlockState writeBlockState(BlockState blockState) {
        blockState = super.writeBlockState(blockState);
        return blockState.with(Keys.POWERED, powered).orElse(blockState);
    }

    @Override
    public SpongeBlockTypeRedstoneRail readContainer(ValueContainer<?> valueContainer) {
        super.readContainer(valueContainer);
        powered = valueContainer.get(Keys.POWERED).orElse(false);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeRedstoneRail that = (SpongeBlockTypeRedstoneRail) o;
        return powered == that.powered;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), powered);
    }
}

package com.degoos.wetsponge.server;

import com.degoos.wetsponge.resource.B64;
import com.degoos.wetsponge.text.SpongeText;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.util.InternalLogger;
import com.google.common.io.Files;
import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Optional;
import javax.imageio.ImageIO;
import org.spongepowered.api.Server;

public class SpongeServerInfo implements WSServerInfo {

	private Server server;

	public SpongeServerInfo(Server server) {
		this.server = server;
	}

	@Override
	public int getOnlinePlayers() {
		return server.getOnlinePlayers().size();
	}

	@Override
	public int getMaxPlayers() {
		return server.getMaxPlayers();
	}

	@Override
	public int getIdleTimeout() {
		return server.getPlayerIdleTimeout();
	}

	@Override
	public void setIdleTimeout(int idleTimeout) {
		server.setPlayerIdleTimeout(idleTimeout);
	}

	@Override
	public boolean isOnlineMode() {
		return server.getOnlineMode();
	}

	@Override
	public boolean isFull() {
		return getMaxPlayers() <= getOnlinePlayers();
	}

	@Override
	public boolean hasWhiteList() {
		return server.hasWhitelist();
	}

	@Override
	public String getServerName() {
		return server.getMotd().toString();
	}

	@Override
	public WSText getMotd() {
		return SpongeText.of(server.getMotd());
	}

	@Override
	public String getBase64ServerIcon() {
		String icon = "";

		File file = new File("server-icon.png");
		if (file.isFile()) {
			try {
				icon = "data:image/png;base64," + B64.encode(Files.toByteArray(file));
			} catch (IOException ex) {
				InternalLogger.printException(ex, "An error has occurred while WetSponge was trying to get the server icon!");
			}
		}

		return icon;
	}

	@Override
	public Optional<WSFavicon> getServerIcon() {
		try {
			File file = new File("server-icon.png");
			if (file.isFile()) return Optional.of(new SpongeFavicon(org.spongepowered.common.network.status.SpongeFavicon.load(ImageIO.read(file))));
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was trying to get the server icon!");
			return Optional.empty();
		}
		return Optional.empty();
	}

	@Override
	public Optional<InetSocketAddress> getBoundAddress() {
		return server.getBoundAddress();
	}
}

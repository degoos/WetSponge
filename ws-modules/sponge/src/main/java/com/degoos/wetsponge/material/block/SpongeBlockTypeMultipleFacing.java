package com.degoos.wetsponge.material.block;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.util.Direction;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class SpongeBlockTypeMultipleFacing extends SpongeBlockType implements WSBlockTypeMultipleFacing {

    private Set<EnumBlockFace> faces, allowedFaces;

    public SpongeBlockTypeMultipleFacing(int numericalId, String oldStringId, String newStringId, int maxStackSize, Set<EnumBlockFace> faces, Set<EnumBlockFace> allowedFaces) {
        super(numericalId, oldStringId, newStringId, maxStackSize);
        this.faces = faces;
        this.allowedFaces = allowedFaces;
    }

    @Override
    public boolean hasFace(EnumBlockFace face) {
        return faces.contains(face);
    }

    @Override
    public void setFace(EnumBlockFace face, boolean enabled) {
        if (enabled) faces.add(face);
        else faces.remove(face);
    }

    @Override
    public Set<EnumBlockFace> getFaces() {
        return new HashSet<>(faces);
    }

    @Override
    public Set<EnumBlockFace> getAllowedFaces() {
        return new HashSet<>(allowedFaces);
    }

    @Override
    public SpongeBlockTypeMultipleFacing clone() {
        return new SpongeBlockTypeMultipleFacing(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), new HashSet<>(faces), new HashSet<>(allowedFaces));
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        super.writeItemStack(itemStack);

        itemStack.offer(Keys.CONNECTED_DIRECTIONS, faces.stream().map(target -> Direction.valueOf(target.getSpongeName())).collect(Collectors.toSet()));

        return itemStack;
    }

    @Override
    public BlockState writeBlockState(BlockState blockState) {
        blockState = super.writeBlockState(blockState);
        return blockState.with(Keys.CONNECTED_DIRECTIONS, faces.stream().map(target ->
                Direction.valueOf(target.getSpongeName())).collect(Collectors.toSet())).orElse(blockState);
    }

    @Override
    public SpongeBlockTypeMultipleFacing readContainer(ValueContainer<?> valueContainer) {
        super.readContainer(valueContainer);

        faces = valueContainer.get(Keys.CONNECTED_DIRECTIONS).orElse(new HashSet<>()).stream().map(target ->
                EnumBlockFace.getBySpongeName(target.name()).orElseThrow(NullPointerException::new)).collect(Collectors.toSet());

        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeMultipleFacing that = (SpongeBlockTypeMultipleFacing) o;
        return Objects.equals(faces, that.faces) &&
                Objects.equals(allowedFaces, that.allowedFaces);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), faces, allowedFaces);
    }
}

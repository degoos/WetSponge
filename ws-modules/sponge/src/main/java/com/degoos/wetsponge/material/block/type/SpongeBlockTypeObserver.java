package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.SpongeBlockTypeDirectional;

import java.util.Objects;
import java.util.Set;

public class SpongeBlockTypeObserver extends SpongeBlockTypeDirectional implements WSBlockTypeObserver {

    private boolean powered;

    public SpongeBlockTypeObserver(EnumBlockFace facing, Set<EnumBlockFace> faces, boolean powered) {
        super(218, "minecraft:observer", "minecraft:observer", 64, facing, faces);
        this.powered = powered;
    }

    @Override
    public boolean isPowered() {
        return powered;
    }

    @Override
    public void setPowered(boolean powered) {
        this.powered = powered;
    }

    @Override
    public SpongeBlockTypeObserver clone() {
        return new SpongeBlockTypeObserver(getFacing(), getFaces(), powered);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeObserver that = (SpongeBlockTypeObserver) o;
        return powered == that.powered;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), powered);
    }
}

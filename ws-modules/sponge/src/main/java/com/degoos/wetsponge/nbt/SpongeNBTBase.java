package com.degoos.wetsponge.nbt;

import net.minecraft.nbt.NBTBase;

public abstract class SpongeNBTBase implements WSNBTBase {

	private NBTBase nbtBase;


	public SpongeNBTBase(NBTBase nbtBase) {
		this.nbtBase = nbtBase;
	}

	@Override
	public byte getId() {
		return nbtBase.getId();
	}

	@Override
	public abstract WSNBTBase copy();

	@Override
	public boolean hasNoTags() {
		return nbtBase.isEmpty();
	}

	@Override
	public String toString() {
		return nbtBase == null ? super.toString() : nbtBase.toString();
	}

	@Override
	public NBTBase getHandled() {
		return nbtBase;
	}
}

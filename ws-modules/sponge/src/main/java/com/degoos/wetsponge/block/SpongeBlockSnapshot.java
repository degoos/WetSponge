package com.degoos.wetsponge.block;

import com.degoos.wetsponge.material.SpongeMaterial;
import com.degoos.wetsponge.material.WSBlockTypes;
import com.degoos.wetsponge.material.block.SpongeBlockType;
import org.spongepowered.api.block.BlockSnapshot;

public class SpongeBlockSnapshot extends WSBlockSnapshot {

	private BlockSnapshot snapshot;

	public SpongeBlockSnapshot(BlockSnapshot snapshot) {
		super(WSBlockTypes.getById(snapshot.getState().getType().getId())
				.orElse(new SpongeBlockType(0, snapshot.getState().getType().getId(), snapshot.getState().getType().getId(), 64)));
		this.snapshot = snapshot;
		((SpongeMaterial) original).readContainer(snapshot.getState());
	}
}

package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.enums.block.EnumBlockTypeSwitchFace;
import com.degoos.wetsponge.material.block.SpongeBlockTypeDirectional;
import com.degoos.wetsponge.util.Validate;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.Objects;
import java.util.Set;

public class SpongeBlockTypeSwitch extends SpongeBlockTypeDirectional implements WSBlockTypeSwitch {

    private EnumBlockTypeSwitchFace switchFace;
    private boolean powered;

    public SpongeBlockTypeSwitch(int numericalId, String oldStringId, String newStringId, int maxStackSize,
                                 EnumBlockFace facing, Set<EnumBlockFace> faces, EnumBlockTypeSwitchFace switchFace, boolean powered) {
        super(numericalId, oldStringId, newStringId, maxStackSize, facing, faces);
        Validate.notNull(switchFace, "Switch face cannot be null!");
        this.switchFace = switchFace;
        this.powered = powered;
    }

    @Override
    public EnumBlockTypeSwitchFace getSwitchFace() {
        return switchFace;
    }

    @Override
    public void setSwitchFace(EnumBlockTypeSwitchFace face) {
        Validate.notNull(face, "Switch face cannot be null!");
        this.switchFace = face;
    }

    @Override
    public boolean isPowered() {
        return powered;
    }

    @Override
    public void setPowered(boolean powered) {
        this.powered = powered;
    }

    @Override
    public SpongeBlockTypeSwitch clone() {
        return new SpongeBlockTypeSwitch(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), getFacing(), getFaces(), switchFace, powered);
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        super.writeItemStack(itemStack);
        itemStack.offer(Keys.POWERED, powered);
        return itemStack;
    }

    @Override
    public BlockState writeBlockState(BlockState blockState) {
        blockState = super.writeBlockState(blockState);
        return blockState.with(Keys.POWERED, powered).orElse(blockState);
    }

    @Override
    public SpongeBlockTypeSwitch readContainer(ValueContainer<?> valueContainer) {
        super.readContainer(valueContainer);
        powered = valueContainer.get(Keys.POWERED).orElse(false);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeSwitch that = (SpongeBlockTypeSwitch) o;
        return powered == that.powered &&
                switchFace == that.switchFace;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), switchFace, powered);
    }
}

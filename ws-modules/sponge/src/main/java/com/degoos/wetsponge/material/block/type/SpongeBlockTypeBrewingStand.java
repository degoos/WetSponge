package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.material.block.SpongeBlockType;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class SpongeBlockTypeBrewingStand extends SpongeBlockType implements WSBlockTypeBrewingStand {

    private Set<Integer> bottles;
    private int maximumBottles;

    public SpongeBlockTypeBrewingStand(Set<Integer> bottles, int maximumBottles) {
        super(117, "minecraft:brewing_stand", "minecraft:brewing_stand", 64);
        this.bottles = bottles == null ? new HashSet<>() : bottles;
        this.maximumBottles = maximumBottles;
    }

    @Override
    public boolean hasBottle(int index) {
        return bottles.contains(index);
    }

    @Override
    public void setBottle(int index, boolean bottle) {
        if (bottle) bottles.add(index);
        else bottles.remove(index);
    }

    @Override
    public Set<Integer> getBottles() {
        return new HashSet<>(bottles);
    }

    @Override
    public int getMaximumBottles() {
        return maximumBottles;
    }

    @Override
    public SpongeBlockTypeBrewingStand clone() {
        return new SpongeBlockTypeBrewingStand(new HashSet<>(bottles), maximumBottles);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeBrewingStand that = (SpongeBlockTypeBrewingStand) o;
        return maximumBottles == that.maximumBottles &&
                Objects.equals(bottles, that.bottles);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), bottles, maximumBottles);
    }

}

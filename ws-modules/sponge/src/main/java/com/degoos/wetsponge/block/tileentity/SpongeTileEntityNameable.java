package com.degoos.wetsponge.block.tileentity;

import com.degoos.wetsponge.block.SpongeBlock;
import com.degoos.wetsponge.text.SpongeText;
import com.degoos.wetsponge.text.WSText;
import org.spongepowered.api.block.tileentity.TileEntity;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.text.Text;

public class SpongeTileEntityNameable extends SpongeTileEntity implements WSTileEntityNameable {


    public SpongeTileEntityNameable(SpongeBlock block) {
        super(block);
    }

    @Override
    public WSText getCustomName() {
        return SpongeText.of(getHandled().get(Keys.DISPLAY_NAME).orElse(Text.EMPTY));
    }

    @Override
    public void setCustomName(WSText customName) {
        getHandled().offer(Keys.DISPLAY_NAME, ((SpongeText) customName).getHandled());
    }

    @Override
    public TileEntity getHandled() {
        return (TileEntity) super.getHandled();
    }

}

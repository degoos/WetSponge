package com.degoos.wetsponge.user;


import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.parser.player.PlayerParser;
import com.degoos.wetsponge.util.Validate;
import java.util.Optional;
import java.util.UUID;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.data.manipulator.mutable.entity.JoinData;
import org.spongepowered.api.entity.living.player.User;
import org.spongepowered.api.profile.GameProfile;
import org.spongepowered.api.service.ban.BanService;
import org.spongepowered.api.service.user.UserStorageService;
import org.spongepowered.api.service.whitelist.WhitelistService;

public class SpongeUser implements WSUser {

	public static SpongeUser of(UUID uuid) {
		return new SpongeUser(Sponge.getServiceManager().provideUnchecked(UserStorageService.class).getOrCreate(GameProfile.of(uuid, null)));
	}


	public static SpongeUser of(UUID uuid, String name) {
		return new SpongeUser(Sponge.getServiceManager().provideUnchecked(UserStorageService.class).getOrCreate(GameProfile.of(uuid, name)));
	}

	private User user;


	public SpongeUser(User user) {
		Validate.notNull(user, "User cannot be null!");
		this.user = user;
	}


	@Override
	public boolean isOnline() {
		return user.isOnline();
	}


	@Override
	public String getName() {
		return user.getName();
	}


	@Override
	public UUID getUniqueId() {
		return user.getUniqueId();
	}


	@Override
	public boolean isBanned() {
		return Sponge.getGame().getServiceManager().provideUnchecked(BanService.class).isBanned(user.getProfile());
	}


	@Override
	public boolean isWhitelisted() {
		return Sponge.getGame().getServiceManager().provideUnchecked(WhitelistService.class).isWhitelisted(user.getProfile());
	}


	@Override
	public void setWhitelisted(boolean whitelisted) {
		if (whitelisted) Sponge.getGame().getServiceManager().provideUnchecked(WhitelistService.class).addProfile(user.getProfile());
		else Sponge.getGame().getServiceManager().provideUnchecked(WhitelistService.class).removeProfile(user.getProfile());
	}


	@Override
	public Optional<WSPlayer> getPlayer() {
		return user.getPlayer().map(player -> PlayerParser.getOrCreatePlayer(player, player.getUniqueId()));
	}


	@Override
	public long getFirstPlayed() {
		Optional<JoinData> optional = user.get(JoinData.class);
		return optional.map(joinData -> joinData.firstPlayed().get().toEpochMilli()).orElse((long) 0);
	}


	@Override
	public long getLastPlayed() {
		Optional<JoinData> optional = user.get(JoinData.class);
		return optional.map(joinData -> joinData.lastPlayed().get().toEpochMilli()).orElse((long) 0);
	}


	@Override
	public boolean hasPlayedBefore() {
		return user.get(JoinData.class).isPresent();
	}


	@Override
	public User getHandled() {
		return user;
	}
}

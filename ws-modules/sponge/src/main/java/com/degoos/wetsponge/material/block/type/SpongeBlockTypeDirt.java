package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockTypeDirtType;
import com.degoos.wetsponge.material.block.SpongeBlockType;
import com.degoos.wetsponge.util.Validate;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.DirtType;
import org.spongepowered.api.data.type.DirtTypes;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.Objects;

public class SpongeBlockTypeDirt extends SpongeBlockType implements WSBlockTypeDirt {

    private EnumBlockTypeDirtType dirtType;

    public SpongeBlockTypeDirt(EnumBlockTypeDirtType dirtType) {
        super(3, "minecraft:dirt", "minecraft:dirt", 64);
        Validate.notNull(dirtType, "Dirt type cannot be null!");
        this.dirtType = dirtType;
    }

    @Override
    public String getNewStringId() {
        switch (dirtType) {
            case PODZOL:
                return "minecraft:podzol";
            case COARSE_DIRT:
                return "minecraft:coarse_dirt";
            case DIRT:
            default:
                return "minecraft:dirt";
        }

    }

    @Override
    public EnumBlockTypeDirtType getDirtType() {
        return dirtType;
    }

    @Override
    public void setDirtType(EnumBlockTypeDirtType dirtType) {
        Validate.notNull(dirtType, "Dirt type cannot be null!");
        this.dirtType = dirtType;
    }

    @Override
    public SpongeBlockTypeDirt clone() {
        return new SpongeBlockTypeDirt(dirtType);
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        super.writeItemStack(itemStack);
        itemStack.offer(Keys.DIRT_TYPE, Sponge.getRegistry().getType(DirtType.class, dirtType.name()).orElse(DirtTypes.DIRT));
        return itemStack;
    }

    @Override
    public BlockState writeBlockState(BlockState blockState) {
        blockState = super.writeBlockState(blockState);
        return blockState.with(Keys.DIRT_TYPE, Sponge.getRegistry().getType(DirtType.class, dirtType.name()).orElse(DirtTypes.DIRT)).orElse(blockState);
    }

    @Override
    public SpongeBlockTypeDirt readContainer(ValueContainer<?> valueContainer) {
        super.readContainer(valueContainer);
        dirtType = EnumBlockTypeDirtType.getByName(valueContainer.get(Keys.DIRT_TYPE).get().getName()).orElse(EnumBlockTypeDirtType.DIRT);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeDirt that = (SpongeBlockTypeDirt) o;
        return dirtType == that.dirtType;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), dirtType);
    }
}

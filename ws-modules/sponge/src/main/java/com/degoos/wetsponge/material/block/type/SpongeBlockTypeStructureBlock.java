package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockTypeStructureBlockMode;
import com.degoos.wetsponge.material.block.SpongeBlockType;
import com.degoos.wetsponge.util.Validate;
import net.minecraft.block.BlockStructure;
import net.minecraft.block.state.IBlockState;
import net.minecraft.tileentity.TileEntityStructure;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.Objects;

public class SpongeBlockTypeStructureBlock extends SpongeBlockType implements WSBlockTypeStructureBlock {

    private EnumBlockTypeStructureBlockMode mode;

    public SpongeBlockTypeStructureBlock(EnumBlockTypeStructureBlockMode mode) {
        super(255, "minecraft:structure_block", "minecraft:structure_block", 64);
        Validate.notNull(mode, "Mode cannot be null!");
        this.mode = mode;
    }

    @Override
    public EnumBlockTypeStructureBlockMode getMode() {
        return mode;
    }

    @Override
    public void setMode(EnumBlockTypeStructureBlockMode mode) {
        Validate.notNull(mode, "Mode cannot be null!");
        this.mode = mode;
    }

    @Override
    public SpongeBlockTypeStructureBlock clone() {
        return new SpongeBlockTypeStructureBlock(mode);
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        return super.writeItemStack(itemStack);
    }

    @Override
    public BlockState writeBlockState(BlockState blockState) {
        blockState = super.writeBlockState(blockState);
        return (BlockState) ((IBlockState) blockState).withProperty(BlockStructure.MODE, TileEntityStructure.Mode.valueOf(mode.name()));
    }

    @Override
    public SpongeBlockTypeStructureBlock readContainer(ValueContainer<?> valueContainer) {
        super.readContainer(valueContainer);
        if (valueContainer instanceof IBlockState)
            mode = EnumBlockTypeStructureBlockMode.valueOf(((IBlockState) valueContainer).getValue(BlockStructure.MODE).name());
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeStructureBlock that = (SpongeBlockTypeStructureBlock) o;
        return mode == that.mode;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), mode);
    }
}

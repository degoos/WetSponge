package com.degoos.wetsponge.nbt;

import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;

public class SpongeNBTTagList extends SpongeNBTBase implements WSNBTTagList {

	public SpongeNBTTagList(NBTTagList nbtTagList) {
		super(nbtTagList);
	}

	public SpongeNBTTagList() {
		this(new NBTTagList());
	}

	@Override
	public void appendTag(WSNBTBase wsnbt) {
		getHandled().appendTag((NBTBase) wsnbt.getHandled());
	}

	@Override
	public void set(int index, WSNBTBase wsnbt) {
		getHandled().set(index, (NBTBase) wsnbt.getHandled());
	}

	@Override
	public void removeTag(int index) {
		getHandled().removeTag(index);
	}

	@Override
	public WSNBTTagCompound getCompoundTagAt(int index) {
		NBTTagCompound compound = getHandled().getCompoundTagAt(index);
		return compound == null ? null : new SpongeNBTTagCompound(compound);
	}

	@Override
	public int getIntAt(int index) {
		return getHandled().getIntAt(index);
	}

	@Override
	public int[] getIntArrayAt(int index) {
		return getHandled().getIntArrayAt(index);
	}

	@Override
	public double getDoubleAt(int index) {
		return getHandled().getDoubleAt(index);
	}

	@Override
	public float getFloatAt(int index) {
		return getHandled().getFloatAt(index);
	}

	@Override
	public String getStringAt(int index) {
		return getHandled().getStringTagAt(index);
	}

	@Override
	public WSNBTBase get(int index) {
		return SpongeNBTTagParser.parse(getHandled().get(index));
	}

	@Override
	public int tagCount() {
		return getHandled().tagCount();
	}

	@Override
	public int getTagType() {
		return getHandled().getTagType();
	}

	@Override
	public WSNBTTagList copy() {
		return new SpongeNBTTagList(getHandled().copy());
	}

	@Override
	public NBTTagList getHandled() {
		return (NBTTagList) super.getHandled();
	}
}

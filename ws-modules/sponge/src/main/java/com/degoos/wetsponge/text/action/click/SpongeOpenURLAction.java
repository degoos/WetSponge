package com.degoos.wetsponge.text.action.click;

import org.spongepowered.api.text.action.ClickAction;
import org.spongepowered.api.text.action.TextActions;

import java.net.URL;

public class SpongeOpenURLAction extends SpongeClickAction implements WSOpenURLAction {

    public SpongeOpenURLAction(URL url) {
        super(TextActions.openUrl(url));
    }

    public SpongeOpenURLAction(ClickAction.OpenUrl action) {
        super(action);
    }

    @Override
    public URL getURL() {
        return getHandled().getResult();
    }

    @Override
    public ClickAction.OpenUrl getHandled() {
        return (ClickAction.OpenUrl) super.getHandled();
    }
}

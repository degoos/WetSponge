package com.degoos.wetsponge.entity.living.monster;


import org.spongepowered.api.entity.living.monster.CaveSpider;

public class SpongeCaveSpider extends SpongeSpider implements WSCaveSpider {


    public SpongeCaveSpider(CaveSpider entity) {
        super(entity);
    }

    @Override
    public CaveSpider getHandled() {
        return (CaveSpider) super.getHandled();
    }
}

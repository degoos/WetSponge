package com.degoos.wetsponge.material.item.type;

import com.degoos.wetsponge.firework.SpongeFireworkEffect;
import com.degoos.wetsponge.firework.WSFireworkEffect;
import com.degoos.wetsponge.material.item.SpongeItemType;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.FireworkEffect;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.*;

public class SpongeItemTypeFireworkCharge extends SpongeItemType implements WSItemTypeFireworkCharge {

    private WSFireworkEffect effect;

    public SpongeItemTypeFireworkCharge(WSFireworkEffect effect) {
        super(402, "minecraft:firework_charge", "minecraft:firework_star", 64);
        this.effect = effect;
    }

    @Override
    public Optional<WSFireworkEffect> getEffect() {
        return Optional.ofNullable(effect);
    }

    @Override
    public void setEffect(WSFireworkEffect effect) {
        this.effect = effect;
    }

    @Override
    public SpongeItemTypeFireworkCharge clone() {
        return new SpongeItemTypeFireworkCharge(effect == null ? null : effect.clone());
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        super.writeItemStack(itemStack);
        itemStack.offer(Keys.FIREWORK_EFFECTS, effect == null ? new ArrayList<>() : Collections.singletonList((FireworkEffect) effect.getHandled()));
        return itemStack;
    }

    @Override
    public SpongeItemTypeFireworkCharge readContainer(ValueContainer<?> valueContainer) {
        super.readContainer(valueContainer);
        List<FireworkEffect> effects = valueContainer.get(Keys.FIREWORK_EFFECTS).orElseThrow(NullPointerException::new);
        effect = effects.isEmpty() ? null : new SpongeFireworkEffect(effects.get(0));
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeItemTypeFireworkCharge that = (SpongeItemTypeFireworkCharge) o;
        return Objects.equals(effect, that.effect);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), effect);
    }
}

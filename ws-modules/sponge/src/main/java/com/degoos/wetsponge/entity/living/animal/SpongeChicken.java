package com.degoos.wetsponge.entity.living.animal;


import net.minecraft.entity.passive.EntityChicken;
import org.spongepowered.api.entity.living.animal.Chicken;

public class SpongeChicken extends SpongeAnimal implements WSChicken {


	public SpongeChicken(Chicken entity) {
		super(entity);
	}


	@Override
	public Chicken getHandled() {
		return (Chicken) super.getHandled();
	}

	@Override
	public int getTimeUntilNextEgg() {
		return ((EntityChicken) getHandled()).timeUntilNextEgg;
	}

	@Override
	public void setTimeUntilNextEgg(int timeUntilNextEgg) {
		((EntityChicken) getHandled()).timeUntilNextEgg = timeUntilNextEgg;
	}
}

package com.degoos.wetsponge.world;


import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.entity.SpongeEntity;
import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.entity.living.player.WSHuman;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.enums.EnumBiomeType;
import com.degoos.wetsponge.enums.EnumEntityType;
import com.degoos.wetsponge.enums.EnumMapBaseColor;
import com.degoos.wetsponge.map.WSMapView;
import com.degoos.wetsponge.material.block.WSBlockType;
import com.degoos.wetsponge.mixin.sponge.interfaces.WSMixinChunk;
import com.degoos.wetsponge.parser.entity.SpongeEntityParser;
import com.degoos.wetsponge.parser.packet.SpongePacketParser;
import com.degoos.wetsponge.parser.player.PlayerParser;
import com.degoos.wetsponge.resource.WSObjectCollection;
import com.degoos.wetsponge.resource.WSObjectIterator;
import com.degoos.wetsponge.user.WSGameProfile;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.Validate;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;
import com.degoos.wetsponge.world.generation.SpongeWorldGenerator;
import com.degoos.wetsponge.world.generation.WSWorldGenerator;
import com.flowpowered.math.vector.Vector2i;
import com.flowpowered.math.vector.Vector3d;
import com.flowpowered.math.vector.Vector3i;
import com.mojang.authlib.GameProfile;
import it.unimi.dsi.fastutil.longs.Long2ObjectMap;
import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectCollection;
import it.unimi.dsi.fastutil.objects.ObjectIterator;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.WorldServer;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.chunk.storage.IChunkLoader;
import net.minecraft.world.gen.ChunkProviderServer;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.living.Human;
import org.spongepowered.api.world.World;
import org.spongepowered.api.world.biome.BiomeType;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;

public class SpongeWorld implements WSWorld {

	private World world;
	private WSWorldProperties worldProperties;
	private WSWorldGenerator worldGenerator;
	private WSChunkLoaderSavable chunkLoader;
	private ChunkProviderServer chunkProvider;
	private Long2ObjectMap long2ObjectMap;
	private WSWorldBorder worldBorder;
	private Map<Integer, WSMapView> mapViews;

	public SpongeWorld(Object world) {
		this((World) world);
	}

	@SuppressWarnings("unchecked")
	public SpongeWorld(World world) {
		Validate.notNull(world, "World cannot be null!");
		this.world = world;
		this.worldBorder = new SpongeWorldBorder(world.getWorldBorder());
		mapViews = new HashMap<>();
		this.worldProperties = new SpongeWorldProperties(world.getProperties());
		this.worldGenerator = new SpongeWorldGenerator(world.getWorldGenerator());
		try {
			chunkProvider = (ChunkProviderServer) ReflectionUtils.getFirstObject(net.minecraft.world.World.class, IChunkProvider.class, world);
			chunkLoader = (WSChunkLoaderSavable) ReflectionUtils.getFirstObject(ChunkProviderServer.class, IChunkLoader.class, chunkProvider);
			Field chunkMapField = ReflectionUtils.getFirstField(chunkProvider.getClass(), Long2ObjectMap.class);
			ReflectionUtils.setAccessible(chunkMapField);
			long2ObjectMap = new Long2ObjectOpenHashMap<Chunk>((Long2ObjectMap<Chunk>) chunkMapField.get(chunkProvider)) {
				@Override
				public Chunk remove(long l) {
					Chunk chunk = get(l);
					if (chunk == null) return null;
					if (((WSMixinChunk) chunk).isPreparedToCancelUnload()) {
						((WSMixinChunk) chunk).setPreparedToCancelUnload(false);
						return null;
					}
					return super.remove(l);
				}

				@Override
				public ObjectCollection<Chunk> values() {
					return new WSObjectCollection<Chunk>(super.values()) {

						@Override
						public ObjectIterator<Chunk> iterator() {
							return new WSObjectIterator<Chunk>(super.iterator()) {

								@Override
								public void remove() {
									Chunk current = current();
									if (((WSMixinChunk) current).isPreparedToCancelUnload()) ((WSMixinChunk) current).setPreparedToCancelUnload(false);
									else super.remove();
								}
							};
						}
					};
				}
			};
			chunkMapField.set(chunkProvider, long2ObjectMap);
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}


	@Override
	public String getName() {
		return world.getName();
	}


	@Override
	public Set<WSEntity> getEntities() {
		return world.getEntities().stream().map(SpongeEntityParser::getWSEntity).collect(Collectors.toSet());
	}

	@Override
	public Set<WSPlayer> getPlayers() {
		return getHandled().getPlayers().stream().map(player -> PlayerParser.getOrCreatePlayer(player, player.getUniqueId())).collect(Collectors.toSet());
	}


	@Override
	public UUID getUniqueId() {
		return world.getUniqueId();
	}

	@Override
	public <T extends WSEntity> Optional<T> spawnEntity(Class<T> entityClass, Vector3d location) {
		WetSponge.getTimings().startTiming("Spawn entity");
		try {
			Optional<EnumEntityType> optional = EnumEntityType.getByClass(entityClass);
			if (!optional.isPresent()) {
				WetSponge.getTimings().stopTiming();
				return Optional.empty();
			}
			Optional<T> entity = (Optional<T>) spawnEntity(optional.get(), location);
			WetSponge.getTimings().stopTiming();
			return entity;
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was spawning an entity!");
			WetSponge.getTimings().stopTiming();
			return Optional.empty();
		}
	}

	@Override
	public Optional<WSEntity> spawnEntity(EnumEntityType type, Vector3d location) {
		WetSponge.getTimings().startTiming("Spawn entity");
		try {
			EntityType spongeType = SpongeEntityParser.getSuperEntityType(type);
			if (spongeType.equals(EntityTypes.UNKNOWN)) {
				WetSponge.getTimings().stopTiming();
				return Optional.empty();
			}
			Entity spongeEntity = world.createEntity(spongeType, location);
			if (!world.spawnEntity(spongeEntity)) {
				WetSponge.getTimings().stopTiming();
				return Optional.empty();
			}
			Optional<WSEntity> optional = Optional.ofNullable(SpongeEntityParser.getWSEntity(spongeEntity));
			WetSponge.getTimings().stopTiming();
			return optional;
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was spawning an entity!");
			WetSponge.getTimings().stopTiming();
			return Optional.empty();
		}
	}

	@Override
	public boolean spawnEntity(WSEntity entity) {
		WetSponge.getTimings().startTiming("Spawn entity");
		try {
			boolean b = world.spawnEntity(((SpongeEntity) entity).getHandled());
			WetSponge.getTimings().stopTiming();
			return b;
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was spawning an entity!");
			WetSponge.getTimings().stopTiming();
			return false;
		}
	}

	@Override
	public <T extends WSEntity> Optional<T> createEntity(Class<T> entityClass, Vector3d location) {
		WetSponge.getTimings().startTiming("Parse entity type");
		try {
			Optional<EnumEntityType> optional = EnumEntityType.getByClass(entityClass);
			if (!optional.isPresent()) {
				WetSponge.getTimings().stopTiming();
				return Optional.empty();
			}
			WetSponge.getTimings().stopTiming();
			return (Optional<T>) createEntity(optional.get(), location);
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was creating an entity!");
			WetSponge.getTimings().stopTiming();
			return Optional.empty();
		}
	}

	@Override
	public Optional<WSEntity> createEntity(EnumEntityType type, Vector3d location) {
		WetSponge.getTimings().startTiming("Create entity type");
		try {
			EntityType spongeType = SpongeEntityParser.getSuperEntityType(type);
			if (spongeType.equals(EntityTypes.UNKNOWN)) {
				WetSponge.getTimings().stopTiming();
				return Optional.empty();
			}
			Optional<WSEntity> entity = Optional.ofNullable(SpongeEntityParser.getWSEntity(world.createEntity(spongeType, location)));
			WetSponge.getTimings().stopTiming();
			return entity;
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was creating an entity!");
			WetSponge.getTimings().stopTiming();
			return Optional.empty();
		}
	}

	@Override
	public Optional<WSHuman> createNPC(WSGameProfile gameProfile, Vector3d vector3d) {
		WetSponge.getTimings().startTiming("Create NPC");
		try {
			Human player = (Human) (Object) new EntityPlayer((net.minecraft.world.World) world, (GameProfile) gameProfile.getHandled()) {
				@Override
				public boolean isSpectator() {
					return false;
				}

				@Override
				public boolean isCreative() {
					return false;
				}
			};
			WSEntity entity = SpongeEntityParser.getWSEntity(player);
			if (!(entity instanceof WSHuman)) return Optional.empty();
			WetSponge.getTimings().stopTiming();
			return Optional.ofNullable((WSHuman) entity);
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was creating a NPC!");
			WetSponge.getTimings().stopTiming();
			return Optional.empty();
		}
	}

	@Override
	public WSWorldProperties getProperties() {
		return worldProperties;
	}

	@Override
	public WSWorldGenerator getGenerator() {
		return worldGenerator;
	}

	@Override
	public Optional<WSChunk> getChunk(int x, int z) {
		return world.getChunk(x, 0, z).map(SpongeChunk::new);
	}

	@Override
	public Optional<WSChunk> getChunkAtLocation(int x, int z) {
		return world.getChunkAtBlock(x, 0, z).map(SpongeChunk::new);
	}

	@Override
	public Optional<WSChunk> loadChunk(int x, int z, boolean generate) {
		try {
			WetSponge.getTimings().startTiming("Load chunk");
			Optional<WSChunk> chunk = world.loadChunk(x, 0, z, generate).map(SpongeChunk::new);
			WetSponge.getTimings().stopTiming();
			return chunk;
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was loading a chunk!");
			WetSponge.getTimings().stopTiming();
			return Optional.empty();
		}
	}

	@Override
	public Optional<WSChunk> loadChunkAtLocation(int x, int z, boolean shouldGenerate) {
		return loadChunk(x >> 4, z >> 4, shouldGenerate);
	}

	@Override
	public Set<WSChunk> getLoadedChunks() {
		Set<WSChunk> chunks = new HashSet<>();
		world.getLoadedChunks().forEach(chunk -> chunks.add(new SpongeChunk(chunk)));
		return chunks;
	}

	@Override
	public EnumBiomeType getBiome(int x, int y, int z) {
		return EnumBiomeType.getBySpongeName(world.getBiome(x, y, z).getName()).orElseThrow(NullPointerException::new);
	}

	@Override
	public void setBiome(int x, int y, int z, EnumBiomeType biome) {
		world.setBiome(x, y, z, Sponge.getRegistry().getType(BiomeType.class, biome.getSpongeName()).orElseThrow(NullPointerException::new));
	}

	@Override
	public boolean isAutoSave() {
		return !((WorldServer) world).disableLevelSaving;
	}

	@Override
	public void setAutoSave(boolean autoSave) {
		((WorldServer) world).disableLevelSaving = !autoSave;
	}

	@Override
	public void save() {
		try {
			world.save();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	@Override
	public void flushUnloadedChunksQueue() {
		chunkProvider.tick();
	}

	@Override
	public void shutdownAllChunks() {
		long2ObjectMap.clear();
	}

	@Override
	public boolean canBeSaved() {
		return chunkLoader.canSaveChunks(this);
	}

	@Override
	public void setCanBeSaved(boolean canSave) {
		chunkLoader.setSaveChunks(canSave, this);
	}

	@Override
	public void saveAllChunks() {
		chunkLoader.saveAllChunks(this);
	}

	@Override
	public WSBlockType getBlockType(Vector3i position) {
		return getBlockType(position.getX(), position.getY(), position.getZ());
	}

	@Override
	public WSBlockType getBlockType(int x, int y, int z) {
		return SpongePacketParser.getMaterial(((net.minecraft.world.World) world).getChunk(x >> 4, z >> 4).getBlockState(new BlockPos(x, y, z)));
	}

	@Override
	public Map<Vector3i, WSBlockType> getBlockTypesInArea(Vector3i pos1, Vector3i pos2) {
		return getBlockTypesInArea(pos1, pos2, false);
	}

	@Override
	public Map<Vector3i, WSBlockType> getBlockTypesInArea(Vector3i pos1, Vector3i pos2, boolean getAir) {
		WetSponge.getTimings().startTiming("Get block types in area");
		Vector3i min = new Vector3i(Math.min(pos1.getX(), pos2.getX()), Math.min(pos1.getY(), pos2.getY()), Math.min(pos1.getZ(), pos2.getZ()));
		Vector3i max = new Vector3i(Math.max(pos1.getX(), pos2.getX()), Math.max(pos1.getY(), pos2.getY()), Math.max(pos1.getZ(), pos2.getZ()));

		Vector2i minChunk = new Vector2i(min.getX() >> 4, min.getZ() >> 4);
		Vector2i maxChunk = new Vector2i(max.getX() >> 4, max.getZ() >> 4);
		Map<Vector3i, WSBlockType> blockTypes = new HashMap<>();
		try {
			net.minecraft.world.World nmsWorld = (net.minecraft.world.World) world;
			for (int cx = minChunk.getX(); cx <= maxChunk.getX(); cx++) {
				for (int cz = minChunk.getY(); cz <= maxChunk.getY(); cz++) {
					Chunk chunk = chunkLoader.loadVanillaChunk(nmsWorld, cx, cz);
					if (chunk == null) continue;
					int minX = 0;
					int minZ = 0;
					int maxX = 15;
					int maxZ = 15;
					if (cx == minChunk.getX()) minX = min.getX() - (cx << 4);
					if (cz == minChunk.getY()) minZ = min.getZ() - (cz << 4);
					if (cx == maxChunk.getX()) maxX = max.getX() - (cx << 4);
					if (cz == maxChunk.getY()) maxZ = max.getZ() - (cz << 4);
					for (int x = minX; x <= maxX; x++) {
						for (int z = minZ; z <= maxZ; z++) {
							for (int y = min.getY(); y <= max.getY(); y++) {
								int fx = (cx << 4) + x;
								int fz = (cz << 4) + z;
								WSBlockType blockType = SpongePacketParser.getMaterial(chunk.getBlockState(fx, y, fz));
								if (getAir || blockType.getNumericalId() != 0) blockTypes.put(new Vector3i(fx, y, fz), blockType);
							}
						}
					}
				}
			}
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was getting the block types of an area!");
		}
		WetSponge.getTimings().stopTiming();
		return blockTypes;
	}

	/*@Override
	public WSSchematic createSchematic(Vector3i center, Vector3i pos1, Vector3i pos2, boolean copyAir) {
		Vector3i min = new Vector3i(Math.min(pos1.getX(), pos2.getX()), Math.min(pos1.getY(), pos2.getY()), Math.min(pos1.getZ(), pos2.getZ()));
		Vector3i max = new Vector3i(Math.max(pos1.getX(), pos2.getX()), Math.max(pos1.getY(), pos2.getY()), Math.max(pos1.getZ(), pos2.getZ()));

		Vector2i minChunk = new Vector2i(min.getX() >> 4, min.getZ() >> 4);
		Vector2i maxChunk = new Vector2i(max.getX() >> 4, max.getZ() >> 4);

		Map<Vector3i, WSBlockType> blockTypes = new HashMap<>();
		Map<Vector3i, String> tileEntities = new HashMap<>();

		try {
			net.minecraft.world.World nmsWorld = (net.minecraft.world.World) world;
			for (int cx = minChunk.getX(); cx <= maxChunk.getX(); cx++) {
				for (int cz = minChunk.getY(); cz <= maxChunk.getY(); cz++) {
					Chunk chunk = chunkLoader.loadVanillaChunk(nmsWorld, cx, cz);
					if (chunk == null) continue;
					int minX = 0;
					int minZ = 0;
					int maxX = 15;
					int maxZ = 15;
					if (cx == minChunk.getX()) minX = min.getX() - (cx << 4);
					if (cz == minChunk.getY()) minZ = min.getZ() - (cz << 4);
					if (cx == maxChunk.getX()) maxX = max.getX() - (cx << 4);
					if (cz == maxChunk.getY()) maxZ = max.getZ() - (cz << 4);
					for (int x = minX; x <= maxX; x++) {
						for (int z = minZ; z <= maxZ; z++) {
							for (int y = min.getY(); y <= max.getY(); y++) {
								int fx = (cx << 4) + x;
								int fz = (cz << 4) + z;
								WSBlockType blockType = SpongePacketParser.getMaterial(chunk.getBlockState(fx, y, fz));
								boolean air;
								if ((air = blockType.getId() != 0) || copyAir) {
									Vector3i position = new Vector3i(fx, y, fz).sub(center);
									blockTypes.put(position, blockType);
									if (!air) {
										TileEntity tileEntity = chunk.getTileEntity(new BlockPos(fx, y, fz), EnumCreateEntityType.CHECK);
										if (tileEntity != null) {
											NBTTagCompound compound = new NBTTagCompound();
											tileEntity.writeToNBT(compound);
											tileEntities.put(position, compound.toString());
										}
									}
								}
							}
						}
					}
				}
			}
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was getting the block types of an area!");
		}
		return new WSSchematic(blockTypes, tileEntities);
	}*/

	@Override
	public Map<Vector2i, EnumMapBaseColor> getMapColors(Vector3i pos1, Vector3i pos2) {
		WetSponge.getTimings().startTiming("Get map colors");
		Vector3i min = new Vector3i(Math.min(pos1.getX(), pos2.getX()), Math.min(pos1.getY(), pos2.getY()), Math.min(pos1.getZ(), pos2.getZ()));
		Vector3i max = new Vector3i(Math.max(pos1.getX(), pos2.getX()), Math.max(pos1.getY(), pos2.getY()), Math.max(pos1.getZ(), pos2.getZ()));

		Vector2i minChunk = new Vector2i(min.getX() >> 4, min.getZ() >> 4);
		Vector2i maxChunk = new Vector2i(max.getX() >> 4, max.getZ() >> 4);
		Map<Vector2i, EnumMapBaseColor> colors = new HashMap<>();
		net.minecraft.world.World nmsWorld = (net.minecraft.world.World) world;
		try {
			for (int cx = minChunk.getX(); cx <= maxChunk.getX(); cx++) {
				for (int cz = minChunk.getY(); cz <= maxChunk.getY(); cz++) {
					Chunk chunk = chunkLoader.loadVanillaChunk(nmsWorld, cx, cz);
					if (chunk == null) continue;
					int minX = 0;
					int minZ = 0;
					int maxX = 15;
					int maxZ = 15;
					if (cx == minChunk.getX()) minX = min.getX() - (cx << 4);
					if (cz == minChunk.getY()) minZ = min.getZ() - (cz << 4);
					if (cx == maxChunk.getX()) maxX = max.getX() - (cx << 4);
					if (cz == maxChunk.getY()) maxZ = max.getZ() - (cz << 4);
					for (int x = minX; x <= maxX; x++) {
						for (int z = minZ; z <= maxZ; z++) {
							for (int y = max.getY(); y >= min.getY(); y--) {
								int fx = (cx << 4) + x;
								int fz = (cz << 4) + z;
								IBlockState state = chunk.getBlockState(fx, y, fz);
								EnumMapBaseColor mapColor = EnumMapBaseColor.getById(state.getMapColor(nmsWorld, new BlockPos(fx, y, fz)).colorIndex)
										.orElse(EnumMapBaseColor.AIR);
								if (mapColor.equals(EnumMapBaseColor.AIR)) continue;
								colors.put(new Vector2i(fx, fz), mapColor);
								break;
							}
						}
					}
				}
			}
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was getting the block types of an area!");
		}
		WetSponge.getTimings().stopTiming();
		return colors;
	}

	@Override
	public void triggerExplosion(WSExplosion explosion) {
		world.triggerExplosion(((SpongeExplosion) explosion).getHandler());
	}

	@Override
	public WSWorldBorder getWorldBorder() {
		return worldBorder;
	}

	@Override
	public Map<Integer, WSMapView> getMapViews() {
		return new HashMap<>(mapViews);
	}

	@Override
	public Optional<WSMapView> getMapView(int mapId) {
		return Optional.ofNullable(mapViews.getOrDefault(mapId, null));
	}

	@Override
	public void putMapView(int mapId, WSMapView mapView) {
		Validate.notNull(mapView, "Map view cannot be null!");
		mapViews.put(mapId, mapView);
	}

	@Override
	public void removeMapView(int mapId) {
		mapViews.remove(mapId);
	}

	@Override
	public void clearMapViews() {
		mapViews.clear();
	}

	@Override
	public File getWorldFolder() {
		return ((net.minecraft.world.World) world).getSaveHandler().getWorldDirectory();
	}

	@Override
	public ChunkProviderServer getChunkProvider() {
		return chunkProvider;
	}


	public WSChunkLoaderSavable getChunkLoader() {
		return chunkLoader;
	}

	@Override
	public World getHandled() {
		return world;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		SpongeWorld that = (SpongeWorld) o;

		return world != null ? world.equals(that.world) : that.world == null;
	}


	@Override
	public int hashCode() {
		return world != null ? world.hashCode() : 0;
	}
}

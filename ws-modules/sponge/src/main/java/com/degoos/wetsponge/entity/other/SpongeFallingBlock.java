package com.degoos.wetsponge.entity.other;

import com.degoos.wetsponge.entity.SpongeEntity;
import com.degoos.wetsponge.material.SpongeMaterial;
import com.degoos.wetsponge.material.WSBlockTypes;
import com.degoos.wetsponge.material.block.SpongeBlockType;
import com.degoos.wetsponge.material.block.WSBlockType;
import com.degoos.wetsponge.util.Validate;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.FallingBlock;

public class SpongeFallingBlock extends SpongeEntity implements WSFallingBlock {


	public SpongeFallingBlock(FallingBlock entity) {
		super(entity);
	}

	@Override
	public WSBlockType getBlockType() {
		WSBlockType blockType = WSBlockTypes.getById(getHandled().blockState().get().getId()).orElse(WSBlockTypes.AIR.getDefaultState());
		((SpongeMaterial) blockType).readContainer(getHandled().blockState().get());
		return blockType;
	}

	@Override
	public void setBlockType(WSBlockType blockType) {
		Validate.notNull(blockType, "Block typecannot be null!");
		getHandled().offer(Keys.FALLING_BLOCK_STATE, ((SpongeBlockType) blockType).writeBlockState(Sponge.getRegistry()
				.getType(BlockType.class, blockType.getStringId()).get().getDefaultState()));
	}

	@Override
	public double getFallDamagePerBlock() {
		return getHandled().fallDamagePerBlock().get();
	}

	@Override
	public void setFallDamagerPerBlock(double fallDamagerPerBlock) {
		getHandled().offer(Keys.FALL_DAMAGE_PER_BLOCK, fallDamagerPerBlock);
	}

	@Override
	public double getMaxFallDamage() {
		return getHandled().maxFallDamage().get();
	}

	@Override
	public void setMaxFallDamage(double maxFallDamage) {
		getHandled().offer(Keys.MAX_FALL_DAMAGE, maxFallDamage);
	}

	@Override
	public boolean canPlaceAsBlock() {
		return getHandled().canPlaceAsBlock().get();
	}

	@Override
	public void setCanPlaceAsBlock(boolean canPlaceAsBlock) {
		getHandled().offer(Keys.CAN_PLACE_AS_BLOCK, canPlaceAsBlock);
	}

	@Override
	public boolean canDropAsItem() {
		return getHandled().canDropAsItem().get();
	}

	@Override
	public void setCanDropAsItem(boolean canDropAsItem) {
		getHandled().offer(Keys.CAN_DROP_AS_ITEM, canDropAsItem);
	}

	@Override
	public int getFallTime() {
		return getHandled().fallTime().get();
	}

	@Override
	public void setFallTime(int fallTime) {
		getHandled().offer(Keys.FALL_TIME, fallTime);
	}

	@Override
	public boolean canHurtEntities() {
		return getHandled().canHurtEntities().get();
	}

	@Override
	public void setCanHurtEntities(boolean canHurtEntities) {
		getHandled().offer(Keys.FALLING_BLOCK_CAN_HURT_ENTITIES, canHurtEntities);
	}

	@Override
	public FallingBlock getHandled() {
		return (FallingBlock) super.getHandled();
	}
}

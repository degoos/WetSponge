package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.material.WSBlockTypes;
import com.degoos.wetsponge.material.block.WSBlockType;
import com.degoos.wetsponge.packet.SpongePacket;
import com.degoos.wetsponge.parser.packet.SpongePacketParser;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;
import com.flowpowered.math.vector.Vector3i;
import net.minecraft.block.state.IBlockState;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.SPacketBlockChange;
import net.minecraft.util.math.BlockPos;

public class SpongeSPacketBlockChange extends SpongePacket implements WSSPacketBlockChange {

	private WSBlockType material;
	private Vector3i position;
	private boolean changed;

	public SpongeSPacketBlockChange(WSBlockType type, Vector3i position) {
		super(new SPacketBlockChange());
		this.position = position;
		this.material = type.clone();
		update();
	}

	public SpongeSPacketBlockChange(Packet<?> packet) {
		super(packet);
		refresh();
	}

	public void update() {
		try {
			IBlockState state = SpongePacketParser.getBlockState(material);
			ReflectionUtils.setFirstObject(getHandler().getClass(), BlockPos.class, getHandler(),
					new BlockPos(position.getX(), position.getY(), position.getZ()));
			ReflectionUtils.setFirstObject(getHandler().getClass(), IBlockState.class, getHandler(), state);
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	public void refresh() {
		try {
			BlockPos pos = (BlockPos) ReflectionUtils.getFirstObject(SPacketBlockChange.class, BlockPos.class, getHandler());
			material = SpongePacketParser.getMaterial((IBlockState) ReflectionUtils.getFirstObject(SPacketBlockChange.class, IBlockState.class, getHandler()));
			position = new Vector3i(pos.getX(), pos.getY(), pos.getZ());
		} catch (Throwable ex) {
			ex.printStackTrace();
			material = WSBlockTypes.AIR.getDefaultState();
			position = new Vector3i(0, 0, 0);
		}
	}

	@Override
	public Vector3i getBlockPosition() {
		return position;
	}

	@Override
	public void setBlockPosition(Vector3i position) {
		changed = true;
		this.position = position;
	}

	@Override
	public WSBlockType getMaterial() {
		changed = true;
		return material;
	}

	@Override
	public void setMaterial(WSBlockType material) {
		changed = true;
		this.material = material;
	}

	@Override
	public boolean hasChanged() {
		return changed;
	}
}

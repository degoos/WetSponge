package com.degoos.wetsponge.material.block;

import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.Objects;

public class SpongeBlockTypeAttachable extends SpongeBlockType implements WSBlockTypeAttachable {

    private boolean attached;

    public SpongeBlockTypeAttachable(int numericalId, String oldStringId, String newStringId, int maxStackSize, boolean attached) {
        super(numericalId, oldStringId, newStringId, maxStackSize);
        this.attached = attached;
    }

    @Override
    public boolean isAttached() {
        return attached;
    }

    @Override
    public void setAttached(boolean attached) {
        this.attached = attached;
    }

    @Override
    public SpongeBlockTypeAttachable clone() {
        return new SpongeBlockTypeAttachable(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), attached);
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        super.writeItemStack(itemStack);
        itemStack.offer(Keys.ATTACHED, attached);
        return itemStack;
    }

    @Override
    public BlockState writeBlockState(BlockState blockState) {
        blockState = super.writeBlockState(blockState);
        return blockState.with(Keys.ATTACHED, attached).orElse(blockState);
    }

    @Override
    public SpongeBlockTypeAttachable readContainer(ValueContainer<?> valueContainer) {
        super.readContainer(valueContainer);
        attached = valueContainer.get(Keys.ATTACHED).orElse(false);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeAttachable that = (SpongeBlockTypeAttachable) o;
        return attached == that.attached;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), attached);
    }
}

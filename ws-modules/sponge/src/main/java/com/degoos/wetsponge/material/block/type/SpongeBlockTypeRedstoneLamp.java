package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.material.block.SpongeBlockTypeLightable;

public class SpongeBlockTypeRedstoneLamp extends SpongeBlockTypeLightable implements WSBlockTypeRedstoneLamp {


    public SpongeBlockTypeRedstoneLamp(boolean lit) {
        super(123, "minecraft:redstone_lamp", "minecraft:redstone_lamp", 64, lit);
    }

    @Override
    public int getNumericalId() {
        return isLit() ? 124 : 123;
    }

    @Override
    public String getOldStringId() {
        return isLit() ? "minecraft:redstone_lamp" : "minecraft:lit_redstone_lamp";
    }

    @Override
    public SpongeBlockTypeRedstoneLamp clone() {
        return new SpongeBlockTypeRedstoneLamp(isLit());
    }
}

package com.degoos.wetsponge.block.tileentity;


import com.degoos.wetsponge.SpongeWetSponge;
import com.degoos.wetsponge.block.SpongeBlock;
import com.degoos.wetsponge.entity.living.player.SpongePlayer;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.text.SpongeText;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.tileentity.TileEntitySign;
import net.minecraft.util.math.BlockPos;
import org.spongepowered.api.block.tileentity.Sign;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.manipulator.mutable.tileentity.SignData;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.scheduler.Task;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class SpongeTileEntitySign extends SpongeTileEntity implements WSTileEntitySign {

    public SpongeTileEntitySign(SpongeBlock block) {
        super(block);
    }


    @Override
    public void setLine(int line, WSText value) {
        if (line < 0 || line > 3) return;
        Sign sign = getHandled();
        List<WSText> lines = getLines();
        lines.set(line, value);
        SignData data = sign.getSignData();
        data.set(Keys.SIGN_LINES, lines.stream().map(target -> ((SpongeText) target).getHandled()).collect(Collectors.toList()));
        sign.offer(data);
    }

    @Override
    public Optional<WSText> getLine(int line) {
        return getHandled().getSignData().get(line).map(SpongeText::of);
    }

    @Override
    public List<WSText> getLines() {
        return getHandled().getSignData().get(Keys.SIGN_LINES).get().stream().map(SpongeText::of).collect(Collectors.toList());
    }

    @Override
    public void setLines(WSText[] lines) {
        if (lines.length < 4) return;
        Sign sign = getHandled();
        SignData data = sign.getSignData();
        data.set(Keys.SIGN_LINES, Arrays.stream(lines).map(target -> ((SpongeText) target).getHandled()).collect(Collectors.toList()));
        sign.offer(data);
    }

    @Override
    public void editSign(WSPlayer wsPlayer) {
        Task.builder().execute(() -> {
            try {
                Player player = ((SpongePlayer) wsPlayer).getHandled();
                Location<World> location = getHandled().getLocation();
                BlockPos pos = new BlockPos(location.getX(), location.getY(), location.getZ());
                TileEntitySign sign = (TileEntitySign) ((net.minecraft.world.World) player.getWorld()).getTileEntity(pos);
                ReflectionUtils.setFirstObject(sign.getClass(), boolean.class, sign, true);
                ((EntityPlayerMP) player).openEditSign(sign);
            } catch (Throwable ex) {
                ex.printStackTrace();
            }
        }).delayTicks(2).submit(SpongeWetSponge.getInstance());
    }

    @Override
    public Sign getHandled() {
        return (Sign) super.getHandled();
    }
}

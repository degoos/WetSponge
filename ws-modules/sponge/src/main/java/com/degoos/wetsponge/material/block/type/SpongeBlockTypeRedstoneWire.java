package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.enums.block.EnumBlockTypeRedstoneWireConnection;
import com.degoos.wetsponge.material.block.SpongeBlockTypeAnaloguePowerable;
import com.degoos.wetsponge.util.Validate;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.WireAttachmentType;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.util.Direction;

import java.util.*;

public class SpongeBlockTypeRedstoneWire extends SpongeBlockTypeAnaloguePowerable implements WSBlockTypeRedstoneWire {

    private Map<EnumBlockFace, EnumBlockTypeRedstoneWireConnection> connections;
    private Set<EnumBlockFace> allowedFaces;

    public SpongeBlockTypeRedstoneWire(int power, int maximumPower, Map<EnumBlockFace, EnumBlockTypeRedstoneWireConnection> connections, Set<EnumBlockFace> allowedFaces) {
        super(55, "minecraft:redstone_wire", "minecraft:redstone_wire", 64, power, maximumPower);
        this.connections = connections == null ? new HashMap<>() : new HashMap<>(connections);
        this.allowedFaces = allowedFaces == null ? new HashSet<>() : allowedFaces;
    }

    @Override
    public EnumBlockTypeRedstoneWireConnection getFaceConnection(EnumBlockFace face) {
        Validate.notNull(face, "Face cannot be null!");
        return connections.getOrDefault(face, EnumBlockTypeRedstoneWireConnection.NONE);
    }

    @Override
    public void setFaceConnection(EnumBlockFace face, EnumBlockTypeRedstoneWireConnection connection) {
        Validate.notNull(face, "Face cannot be null!");
        connections.put(face, connection == null ? EnumBlockTypeRedstoneWireConnection.NONE : connection);
    }

    @Override
    public Set<EnumBlockFace> getAllowedFaces() {
        return allowedFaces;
    }

    @Override
    public SpongeBlockTypeRedstoneWire clone() {
        return new SpongeBlockTypeRedstoneWire(getPower(), gerMaximumPower(), connections, allowedFaces);
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        super.writeItemStack(itemStack);
        itemStack.offer(Keys.WIRE_ATTACHMENTS, toMap());
        return itemStack;
    }

    @Override
    public BlockState writeBlockState(BlockState blockState) {
        blockState = super.writeBlockState(blockState);
        return blockState.with(Keys.WIRE_ATTACHMENTS, toMap()).orElse(blockState);
    }

    @Override
    public SpongeBlockTypeAnaloguePowerable readContainer(ValueContainer<?> valueContainer) {
        super.readContainer(valueContainer);
        connections.clear();
        valueContainer.get(Keys.WIRE_ATTACHMENTS).orElse(new HashMap<>()).forEach((direction, wireAttachmentType) ->
                connections.put(EnumBlockFace.getBySpongeName(direction.name()).orElseThrow(NullPointerException::new),
                        EnumBlockTypeRedstoneWireConnection.valueOf(wireAttachmentType.getName())));
        return this;
    }

    private Map<Direction, WireAttachmentType> toMap() {
        Map<Direction, WireAttachmentType> map = new HashMap<>();
        connections.forEach((face, connection) -> map.put(Direction.valueOf(face.getSpongeName()),
                Sponge.getRegistry().getType(WireAttachmentType.class, connection.name()).orElseThrow(NullPointerException::new)));
        return map;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeRedstoneWire that = (SpongeBlockTypeRedstoneWire) o;
        return Objects.equals(connections, that.connections) &&
                Objects.equals(allowedFaces, that.allowedFaces);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), connections, allowedFaces);
    }
}

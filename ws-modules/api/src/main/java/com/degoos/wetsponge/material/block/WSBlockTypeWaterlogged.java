package com.degoos.wetsponge.material.block;

import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSBlockTypeWaterlogged extends WSBlockType {

	boolean isWaterlogged();

	void setWaterlogged(boolean waterlogged);

	@Override
	WSBlockTypeWaterlogged clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setBoolean("waterlogged", isWaterlogged());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setWaterlogged(compound.getBoolean("waterlogged"));
		return compound;
	}
}

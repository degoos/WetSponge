package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockTypeStoneBrickType;
import com.degoos.wetsponge.material.block.WSBlockType;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSBlockTypeStoneBrick extends WSBlockType {

	EnumBlockTypeStoneBrickType getStoneBrickType();

	void setStoneBrickType(EnumBlockTypeStoneBrickType stoneBrickType);

	@Override
	WSBlockTypeStoneBrick clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setString("stoneBrickType", getStoneBrickType().name());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setStoneBrickType(EnumBlockTypeStoneBrickType.valueOf(compound.getString("stoneBrickType")));
		return compound;
	}
}

package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.bridge.packet.BridgeServerPacket;
import com.degoos.wetsponge.enums.EnumPlayerListItemAction;
import com.degoos.wetsponge.packet.WSPacket;
import com.degoos.wetsponge.packet.play.server.extra.WSPlayerListItemData;

import java.util.Collection;
import java.util.List;

public interface WSSPacketPlayerListItem extends WSPacket {

	public static WSSPacketPlayerListItem of(EnumPlayerListItemAction action, Collection<WSPlayerListItemData> collection) {
		return BridgeServerPacket.newWSSPacketPlayerListItem(action, collection);
	}

	public static WSSPacketPlayerListItem of(EnumPlayerListItemAction action, WSPlayerListItemData... itemData) {
		return BridgeServerPacket.newWSSPacketPlayerListItem(action, itemData);
	}

	EnumPlayerListItemAction getAction();

	void setAction(EnumPlayerListItemAction action);

	List<WSPlayerListItemData> getData();

}

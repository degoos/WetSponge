package com.degoos.wetsponge.entity.living.golem;

/**
 * Represents an Iron Golem.
 */
public interface WSIronGolem extends WSGolem {

	/**
	 * @return true if this iron golem is created by a player.
	 */
	boolean isPlayerCreated();

	/**
	 * Sets if this golem is created by a player.
	 *
	 * @param playerCreated the boolean.
	 */
	void setPlayerCreated(boolean playerCreated);

}

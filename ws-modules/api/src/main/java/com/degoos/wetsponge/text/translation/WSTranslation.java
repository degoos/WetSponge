package com.degoos.wetsponge.text.translation;


import java.util.Locale;

/**
 * Represents a translation.
 */
public interface WSTranslation {

	/**
	 * Returns the default translation.
	 *
	 * @return the default translation.
	 */
	String get();

	/**
	 * Returns the translation of the locale.
	 *
	 * @param locale the locale.
	 *
	 * @return the translation.
	 */
	String get(Locale locale);

	/**
	 * Returns the translation of the locale using some arguments.
	 *
	 * @param locale the location.
	 * @param args the arguments.
	 *
	 * @return the translation.
	 */
	String get(Locale locale, Object... args);

	/**
	 * Returns the default translation using some arguments.
	 *
	 * @param args the arguments.
	 *
	 * @return the translation.
	 */
	String get(Object... args);

	/**
	 * Returns the id of the translation. It usually has no spaces.
	 *
	 * @return the id.
	 */
	String getId();

}

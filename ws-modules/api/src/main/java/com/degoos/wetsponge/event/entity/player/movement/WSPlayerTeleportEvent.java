package com.degoos.wetsponge.event.entity.player.movement;

import com.degoos.wetsponge.event.WSCancellable;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.event.WSEvent;
import com.degoos.wetsponge.world.WSLocation;

/**
 * This event will be called after all teleport events.
 */
public class WSPlayerTeleportEvent extends WSEvent implements WSCancellable {

    private WSPlayer   player;
    private WSLocation from, to;
    private boolean cancelled;

    public WSPlayerTeleportEvent(WSPlayer player, WSLocation from, WSLocation to) {
        this.player = player;
        this.from = from;
        this.to = to;
        this.cancelled = false;
    }

    public WSPlayer getPlayer() {
        return player;
    }


    public WSLocation getFrom() {
        return from;
    }

    public WSLocation getTo() {
        return to;
    }

    public void setTo(WSLocation to) {
        this.to = to;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }
}

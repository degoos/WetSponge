package com.degoos.wetsponge.entity.living.aquatic;

import com.degoos.wetsponge.entity.living.WSCreature;

/**
 * Represents an aquatic entity.
 */
public interface WSAquatic extends WSCreature {
}

package com.degoos.wetsponge.particle;


import com.degoos.wetsponge.bridge.particle.BridgeParticles;
import com.degoos.wetsponge.exception.particle.WSInvalidParticleException;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class WSParticles {

	private static Map<String, WSParticle> particleEffects = new HashMap<>();
	public static final WSParticle ANGRY_VILLAGER = of("VILLAGER_ANGRY", "angryVillager", "minecraft:angry_villager");
	public static final WSParticle BARRIER = of("BARRIER", "barrier", "minecraft:barrier");
	public static final WSParticle BLOCK_CRACK = of("BLOCK_CRACK", "blockcrack", "minecraft:block_crack");
	public static final WSParticle BLOCK_DUST = of("BLOCK_DUST", "blockdust", "minecraft:block_dust");
	public static final WSParticle BUBBLE = of("WATER_BUBBLE", "bubble", "minecraft:water_bubble");
	public static final WSParticle CLOUD = of("CLOUD", "cloud", "minecraft:cloud");
	public static final WSParticle CRITIC = of("CRIT", "crit", "minecraft:critical_hit");
	public static final WSParticle DAMAGE_INDICATOR = of("DAMAGE_INDICATOR", "damageIndicator", "minecraft:damage_indicator");
	public static final WSParticle DEPTH_SUSPEND = of("SUSPENDED_DEPTH", "depthsuspend", "minecraft:suspended_depth");
	public static final WSParticle DRAGON_BREATH = of("DRAGON_BREATH", "dragonbreath", "minecraft:dragon_breath");
	public static final WSParticle DRIP_LAVA = of("DRIP_LAVA", "dripLava", "minecraft:drip_lava");
	public static final WSParticle DRIP_WATER = of("DRIP_WATER", "dripWater", "minecraft:drip_water");
	public static final WSParticle DROPLET = of("WATER_DROP", "droplet", "minecraft:water_drop");
	public static final WSParticle ENCHANTMENT_TABLE = of("ENCHANTMENT_TABLE", "enchantmenttable", "minecraft:enchanting_glyphs");
	public static final WSParticle END_ROD = of("END_ROD", "endRod", "minecraft:end_rod");
	public static final WSParticle EXPLODE = of("EXPLOSION_NORMAL", "explode", "minecraft:explosion");
	public static final WSParticle FALLING_DUST = of("FALLING_DUST", "fallingdust", "minecraft:falling_dust");
	public static final WSParticle FIREWORK_SPARK = of("FIREWORKS_SPARK", "fireworkSpark", "minecraft:fireworks_spark");
	public static final WSParticle FLAME = of("FLAME", "flame", "minecraft:flame");
	public static final WSParticle FOOTSTEP = of("FOOTSTEP", "footstep", "minecraft:footstep");
	public static final WSParticle HAPPY_VILLAGER = of("VILLAGER_HAPPY", "happyVillager", "minecraft:happy_villager");
	public static final WSParticle HEART = of("HEART", "heart", "minecraft:heart");
	public static final WSParticle HUGE_EXPLOSION = of("EXPLOSION_HUGE", "hugeexplosion", "minecraft:huge_explosion");
	public static final WSParticle ITEM_CRACK = of("ITEM_CRACK", "iconcrack", "minecraft:item_crack");
	public static final WSParticle INSTANT_SPELL = of("SPELL_INSTANT", "instantSpell", "minecraft:instant_spell");
	public static final WSParticle LARGE_EXPLODE = of("EXPLOSION_LARGE", "largeexplode", "minecraft:large_explosion");
	public static final WSParticle LARGE_SMOKE = of("SMOKE_LARGE", "largesmoke", "minecraft:large_smoke");
	public static final WSParticle LAVA = of("LAVA", "lava", "minecraft:lava");
	public static final WSParticle MAGIC_CRITIC = of("CRIT_MAGIC", "magicCrit", "minecraft:magic_critical_hit");
	public static final WSParticle MOB_SPELL = of("SPELL_MOB", "mobSpell", "minecraft:mob_spell");
	public static final WSParticle MOB_SPELL_AMBIENT = of("SPELL_MOB_AMBIENT", "mobSpellAmbient", "minecraft:ambient_mob_spell");
	public static final WSParticle GUARDIAN_APPEARANCE = of("MOB_APPEARANCE", "mobappearance", "minecraft:guardian_appearance");
	public static final WSParticle NOTE = of("NOTE", "note", "minecraft:note");
	public static final WSParticle PORTAL = of("PORTAL", "portal", "minecraft:portal");
	public static final WSParticle RED_DUST = of("REDSTONE", "reddust", "minecraft:redstone_dust");
	public static final WSParticle SLIME = of("SLIME", "slime", "minecraft:slime");
	public static final WSParticle SMOKE = of("SMOKE_NORMAL", "smoke", "minecraft:smoke");
	public static final WSParticle SNOWBALL_POOF = of("SNOWBALL", "snowballpoof", "minecraft:snowball");
	public static final WSParticle SNOW_SHOVEL = of("SNOW_SHOVEL", "snowshovel", "minecraft:snow_shovel");
	public static final WSParticle SPELL = of("SPELL", "spell", "minecraft:spell");
	public static final WSParticle SPIT = of("", "spit", "minecraft:split");
	public static final WSParticle SPLASH_POTION = of("SPIT", "splash", "minecraft:water_splash");
	public static final WSParticle SUSPENDED = of("SUSPENDED", "suspended", "minecraft:suspended");
	public static final WSParticle SWEEP_ATTACK = of("SWEEP_ATTACK", "sweepAttack", "minecraft:sweep_attack");
	public static final WSParticle TOTEM = of("TOTEM", "totem", "minecraft:totem");
	public static final WSParticle TOWN_AURA = of("TOWN_AURA", "townaura", "minecraft:town_aura");
	public static final WSParticle WAKE = of("WATER_WAKE", "wake", "minecraft:water_wake");
	public static final WSParticle NULL = of("NULL", "null", "minecraft:null");


	private static WSParticle of(String particleName, String stringId, String minecraftId) {
		try {
			WSParticle particle = BridgeParticles.of(particleName, stringId, minecraftId);
			particleEffects.put(particleName, particle);
			return particle;
		} catch (WSInvalidParticleException ex) {
			//InternalLogger.sendError("Particle " + stringId + " not found.");
			return NULL;
		}
	}

	public static Optional<WSParticle> getByParticleName(String name) {
		return particleEffects.keySet().stream().filter(wsParticle -> wsParticle.equalsIgnoreCase(name)).findAny().map(particleEffects::get);
	}

	public static Optional<WSParticle> getByStringId(String stringId) {
		return particleEffects.values().stream().filter(wsParticle -> wsParticle.getOldMinecraftId().equalsIgnoreCase(stringId)).findAny();
	}

	public static Optional<WSParticle> getByMinecraftId(String minecraftId) {
		return particleEffects.values().stream().filter(wsParticle -> wsParticle.getMinecraftId().equalsIgnoreCase(minecraftId)).findAny();
	}
}

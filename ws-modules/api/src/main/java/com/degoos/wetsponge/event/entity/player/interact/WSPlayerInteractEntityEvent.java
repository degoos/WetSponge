package com.degoos.wetsponge.event.entity.player.interact;

import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.util.Validate;
import com.flowpowered.math.vector.Vector3d;

import java.util.Optional;

public class WSPlayerInteractEntityEvent extends WSPlayerInteractEvent {

    private WSEntity entity;
    private Optional<Vector3d> position;

    public WSPlayerInteractEntityEvent(WSPlayer player, WSEntity entity, Optional<Vector3d> position) {
        super(player);
        Validate.notNull(entity, "Entity cannot be null!");
        Validate.notNull(position, "Position cannot be null!");
        this.entity = entity;
        this.position = position;
    }

    @Override
    public WSEntity getEntity() {
        return entity;
    }

    public Optional<Vector3d> getPosition() {
        return position;
    }

    public static class Primary extends WSPlayerInteractEntityEvent {

        public Primary(WSPlayer player, WSEntity entity, Optional<Vector3d> position) {
            super(player, entity, position);
        }

        public static class MainHand extends Primary {

            public MainHand(WSPlayer player, WSEntity entity, Optional<Vector3d> position) {
                super(player, entity, position);
            }
        }

        public static class OffHand extends Primary {

            public OffHand(WSPlayer player, WSEntity entity, Optional<Vector3d> position) {
                super(player, entity, position);
            }
        }
    }

    public static class Secondary extends WSPlayerInteractEntityEvent {


        public Secondary(WSPlayer player, WSEntity entity, Optional<Vector3d> position) {
            super(player, entity, position);
        }

        public static class MainHand extends Secondary {

            public MainHand(WSPlayer player, WSEntity entity, Optional<Vector3d> position) {
                super(player, entity, position);
            }
        }

        public static class OffHand extends Secondary {

            public OffHand(WSPlayer player, WSEntity entity, Optional<Vector3d> position) {
                super(player, entity, position);
            }
        }
    }
}

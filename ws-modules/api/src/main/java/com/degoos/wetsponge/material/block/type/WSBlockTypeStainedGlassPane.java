package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.EnumDyeColor;
import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;
import com.degoos.wetsponge.nbt.WSNBTTagList;
import com.degoos.wetsponge.nbt.WSNBTTagString;

public interface WSBlockTypeStainedGlassPane extends WSBlockTypeGlassPane, WSBlockTypeDyeColored {

	@Override
	WSBlockTypeStainedGlassPane clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setBoolean("waterlogged", isWaterlogged());
		WSNBTTagList list = WSNBTTagList.of();
		getFaces().forEach(target -> list.appendTag(WSNBTTagString.of(target.name())));
		compound.setTag("faces", list);
		compound.setString("color", getDyeColor().name());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setWaterlogged(compound.getBoolean("waterlogged"));
		getFaces().forEach(target -> setFace(target, false));
		WSNBTTagList list = compound.getTagList("faces", 8);
		for (int i = 0; i < list.tagCount(); i++)
			setFace(EnumBlockFace.valueOf(list.getStringAt(i)), true);
		setDyeColor(EnumDyeColor.valueOf(compound.getString("color")));
		return compound;
	}
}

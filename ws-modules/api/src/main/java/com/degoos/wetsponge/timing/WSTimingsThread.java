package com.degoos.wetsponge.timing;

import com.degoos.wetsponge.plugin.WSPlugin;
import com.degoos.wetsponge.task.WSTask;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class WSTimingsThread {

	private WSTimingRecorder wetSpongeTiming;
	private final long threadId;
	private final String threadName;
	private final Map<WSPlugin, WSTimingRecorder> pluginTimings;
	private WSPlugin assignedPlugin;
	private WSTimingRecorder assignedPluginRecorder;

	public WSTimingsThread(long threadId) {
		this.threadId = threadId;
		this.threadName = null;
		this.wetSpongeTiming = null;
		this.pluginTimings = new ConcurrentHashMap<>();
		this.assignedPlugin = null;
	}

	public WSTimingsThread(long threadId, String threadName) {
		this.threadId = threadId;
		this.threadName = threadName;
		this.wetSpongeTiming = null;
		this.pluginTimings = new ConcurrentHashMap<>();
		this.assignedPlugin = null;
	}

	public WSTimingsThread(Thread thread) {
		this.threadId = thread.getId();
		this.threadName = thread.getName();
		this.wetSpongeTiming = null;
		this.pluginTimings = new ConcurrentHashMap<>();
		this.assignedPlugin = null;
	}

	public void startTiming(String name, StackTraceElement element) {
		if (assignedPlugin == null) {
			if (wetSpongeTiming == null) wetSpongeTiming = new WSTimingRecorder(null);
			wetSpongeTiming.startTiming(name, element);
		} else assignedPluginRecorder.startTiming(name, element);
	}

	public void startTiming(WSTask task) {
		if (assignedPlugin == null) {
			if (wetSpongeTiming == null) wetSpongeTiming = new WSTimingRecorder(null);
			wetSpongeTiming.startTiming(task);
		} else assignedPluginRecorder.startTiming(task);
	}

	public void stopTiming() {
		if (assignedPlugin == null) {
			if (wetSpongeTiming != null) wetSpongeTiming.stopTiming();
		} else assignedPluginRecorder.stopTiming();
	}

	public void assignPlugin(WSPlugin plugin) {
		if (plugin == null) {
			assignedPlugin = null;
			assignedPluginRecorder = null;
		} else {
			assignedPlugin = plugin;
			assignedPluginRecorder = getPluginTimings(plugin);
		}
	}

	public WSTimingRecorder getPluginTimings(WSPlugin plugin) {
		if (pluginTimings.containsKey(plugin)) return pluginTimings.get(plugin);

		WSTimingRecorder recorder = new WSTimingRecorder(plugin);
		pluginTimings.put(plugin, recorder);
		return recorder;
	}

	public long getThreadId() {
		return threadId;
	}

	public WSPlugin getAssignedPlugin() {
		return assignedPlugin;
	}

	public WSTimingRecorder getWetSpongeTiming() {
		return wetSpongeTiming;
	}

	public Map<WSPlugin, WSTimingRecorder> getAllPluginTimings() {
		return pluginTimings;
	}

	public String getThreadName() {
		return threadName == null ? String.valueOf(threadId) : threadName;
	}

	public void merge(WSTimingsThread thread) {
		thread.pluginTimings.forEach((plugin, recorder) -> {
			if (!pluginTimings.containsKey(plugin)) pluginTimings.put(plugin, recorder);
			else pluginTimings.get(plugin).merge(recorder);
		});
	}
}

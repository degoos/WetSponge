package com.degoos.wetsponge.enums;

import com.degoos.wetsponge.item.WSItemStack;
import com.degoos.wetsponge.material.WSBlockTypes;
import com.degoos.wetsponge.material.WSItemTypes;
import com.degoos.wetsponge.material.WSMaterial;

import java.util.Arrays;
import java.util.Optional;

public enum EnumHorseArmorType {

	NONE(WSBlockTypes.AIR.getDefaultState()),
	IRON(WSItemTypes.IRON_HORSE_ARMOR.getDefaultState()),
	GOLD(WSItemTypes.GOLD_HORSE_ARMOR.getDefaultState()),
	DIAMOND(WSItemTypes.DIAMOND_HORSE_ARMOR.getDefaultState());

	private WSMaterial itemType;

	EnumHorseArmorType(WSMaterial itemType) {
		this.itemType = itemType;
	}

	public WSItemStack getArmorItemStack() {
		return WSItemStack.of(itemType);
	}

	public static EnumHorseArmorType getByMaterial(int id) {
		switch (id) {
			case 417:
				return IRON;
			case 418:
				return GOLD;
			case 419:
				return DIAMOND;
			default:
				return NONE;
		}
	}

	public static Optional<EnumHorseArmorType> getByName(String name) {
		return Arrays.stream(values()).filter(target -> target.name().equalsIgnoreCase(name)).findAny();
	}
}

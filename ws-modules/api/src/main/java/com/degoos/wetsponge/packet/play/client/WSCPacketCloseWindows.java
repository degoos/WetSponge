package com.degoos.wetsponge.packet.play.client;

import com.degoos.wetsponge.bridge.packet.BridgeClientPacket;
import com.degoos.wetsponge.packet.WSPacket;

public interface WSCPacketCloseWindows extends WSPacket {

	public static WSCPacketCloseWindows of(int windowsId) {
		return BridgeClientPacket.newWSCPacketCloseWindows(windowsId);
	}

	int getWindowsId();

	void setWindowsId(int windowsId);
}

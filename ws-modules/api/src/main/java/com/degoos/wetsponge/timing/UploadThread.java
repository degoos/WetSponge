package com.degoos.wetsponge.timing;

import com.degoos.wetsponge.command.WSCommandSource;
import com.degoos.wetsponge.config.WetSpongeMessages;
import com.degoos.wetsponge.util.HastebinUtils;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.Validate;

import java.util.Queue;
import java.util.concurrent.LinkedBlockingDeque;

public class UploadThread extends Thread {

	private Queue<UploadRequest> requests = new LinkedBlockingDeque<>();

	public UploadThread() {
		setName("WetSponge timings upload thread");
		setPriority(3);
	}

	@Override
	public void run() {

		while (true) {
			UploadRequest request;
			while ((request = requests.poll()) != null) {
				WetSpongeMessages.getMessage("command.timings.uploading").ifPresent(request.source::sendMessage);
				String rawLink = HastebinUtils.paste(request.json, false);
				if (rawLink == null) {
					WetSpongeMessages.getMessage("command.timings.uploadError").ifPresent(request.source::sendMessage);
					return;
				}
				String link = "https://timings.degoos.com/#/paste/" + rawLink;
				WetSpongeMessages.getMessage("command.timings.uploaded", "<LINK>", link).ifPresent(request.source::sendMessage);
			}
			try {
				synchronized (this) {
					wait();
				}
			} catch (InterruptedException e) {
				InternalLogger.printException(e, "An error has occurred while WetSponge was waiting an upload request!");
			}
		}
	}

	public void addRequest(UploadRequest request) {
		synchronized (this) {
			Validate.notNull(request, "Request cannot be null!");
			requests.add(request);
			notify();
		}
	}


	protected static class UploadRequest {

		private WSCommandSource source;
		private String json;

		public UploadRequest(WSCommandSource source, String json) {
			this.source = source;
			this.json = json;
		}
	}
}

package com.degoos.wetsponge.material.block;

import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSBlockTypeSnowable extends WSBlockType {

	boolean isSnowy();

	void setSnowy(boolean snowy);

	@Override
	WSBlockTypeSnowable clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setBoolean("snowy", isSnowy());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setSnowy(compound.getBoolean("snowy"));
		return compound;
	}
}

package com.degoos.wetsponge.item.enchantment;

import com.degoos.wetsponge.bridge.item.enchantment.BridgeEnchantment;

public class WSEnchantments {

	public static final WSEnchantment AQUA_AFFINITY = of("AQUA_AFFINITY", 6, "minecraft:aqua_affinity");
	public static final WSEnchantment BANE_OF_ARTHROPOD = of("BANE_OF_ARTHROPODS", 18, "minecraft:bane_of_arthropods");
	public static final WSEnchantment BINDING_CURSE = of("BINDING_CURSE", 10, "minecraft:binding_curse");
	public static final WSEnchantment BLAST_PROTECTION = of("BLAST_PROTECTION", 3, "minecraft:blast_protection");
	/**
	 * Minecraft 1.13
	 */
	public static final WSEnchantment CHANNELING = of("CHANNELING", -1, "minecraft:channeling");
	public static final WSEnchantment DEPTH_STRIDER = of("DEPTH_STRIDER", 8, "minecraft:depth_strider");
	public static final WSEnchantment EFFICIENCY = of("EFFICIENCY", 32, "minecraft:efficiency");
	public static final WSEnchantment FEATHER_FALLING = of("FEATHER_FALLING", 2, "minecraft:feather_falling");
	public static final WSEnchantment FIRE_ASPECT = of("FIRE_ASPECT", 20, "minecraft:fire_aspect");
	public static final WSEnchantment FIRE_PROTECTION = of("FIRE_PROTECTION", 1, "minecraft:fire_protection");
	public static final WSEnchantment FLAME = of("FLAME", 50, "minecraft:flame");
	public static final WSEnchantment FORTUNE = of("FORTUNE", 35, "minecraft:fortune");
	public static final WSEnchantment FROST_WALKER = of("FROST_WALKER", 9, "minecraft:frost_walker");
	/**
	 * Minecraft 1.13
	 */
	public static final WSEnchantment IMPALING = of("IMPALING", -1, "minecraft:impaling");
	public static final WSEnchantment INFINITY = of("INFINITY", 51, "minecraft:infinity");
	public static final WSEnchantment KNOCKBACK = of("KNOCKBACK", 19, "minecraft:knockback");
	public static final WSEnchantment LOOTING = of("LOOTING", 21, "minecraft:looting");
	/**
	 * Minecraft 1.13
	 */
	public static final WSEnchantment LOYALTY = of("LOYALTY", -1, "minecraft:loyalty");
	public static final WSEnchantment LUCK_OF_THE_SEA = of("LUCK_OF_THE_SEA", 61, "minecraft:luck_of_the_sea");
	public static final WSEnchantment LURE = of("LURE", 62, "minecraft:lure");
	public static final WSEnchantment MENDING = of("MENDING", 70, "minecraft:mending");
	public static final WSEnchantment POWER = of("POWER", 48, "minecraft:power");
	public static final WSEnchantment PROJECTILE_PROTECTION = of("PROJECTILE_PROTECTION", 4, "minecraft:projectile_protection");
	public static final WSEnchantment PROTECTION = of("PROTECTION", 0, "minecraft:protection");
	public static final WSEnchantment PUNCH = of("PUNCH", 49, "minecraft:punch");
	public static final WSEnchantment RESPIRATION = of("RESPIRATION", 5, "minecraft:respiration");
	/**
	 * Minecraft 1.13
	 */
	public static final WSEnchantment RIPTIDE = of("RIPTIDE", -1, "minecraft:riptide");
	public static final WSEnchantment SHARPNESS = of("SHARPNESS", 16, "minecraft:sharpness");
	public static final WSEnchantment SILK_TOUCH = of("SILK_TOUCH", 33, "minecraft:silk_touch");
	public static final WSEnchantment SMITE = of("SMITE", 17, "minecraft:smite");
	/**
	 * Not implemented by Spigot 1.12.
	 */
	public static final WSEnchantment SWEEPING = of("SWEEPING", 10000, "minecraft:sweeping");
	public static final WSEnchantment THORNS = of("THORNS", 7, "minecraft:thorns");
	public static final WSEnchantment UNBREAKING = of("UNBREAKING", 34, "minecraft:unbreaking");
	public static final WSEnchantment VANISHING_CURSE = of("VANISHING_CURSE", 71, "minecraft:vanishing_curse");


	private static WSEnchantment of(String spongeId, int spigotId, String minecraftKey) {
		return BridgeEnchantment.of(spongeId, spigotId, minecraftKey);
	}


}

package com.degoos.wetsponge.merchant;

import com.degoos.wetsponge.entity.living.merchant.WSVillager;
import com.degoos.wetsponge.entity.living.player.WSPlayer;

import java.util.List;
import java.util.Optional;

/**
 * This class represents a Merchant. A merchant is an object that has some offer traders.
 * Players can interact to a merchant and open the trade GUI.
 * The only available entity which extends this class is {@link WSVillager}.
 */
public interface WSMerchant {

    /**
     * Returns the {@link WSPlayer player} who is trading with the {@link WSMerchant merchant}, if present.
     * On {@link com.degoos.wetsponge.enums.EnumServerType#SPIGOT Spigot} {@link com.degoos.wetsponge.enums.EnumServerVersion#MINECRAFT_OLD OLD}
     * this method will always return an empty Optional.
     *
     * @return the {@link WSPlayer player}, if present.
     */
    Optional<WSPlayer> getCustomer();

    /**
     * Sets the {@link WSPlayer player} who is trading with the {@link WSMerchant merchant}. This method will open a GUI
     * to the {@link WSPlayer player}.
     *
     * @param player the {@link WSPlayer player}.
     */
    void setCustomer(WSPlayer player);

    /**
     * Returns a list with all trades of the {@link WSMerchant merchant}.
     *
     * @return the list.
     */
    List<WSTrade> getTrades();

    /**
     * Sets the list of all trades of the {@link WSMerchant merchant}.
     *
     * @param trades the list.
     */
    void setTrades(List<WSTrade> trades);

    /**
     * Adds a trade to the {@link WSMerchant merchant}.
     *
     * @param trade the trade.
     */
    void addTrade(WSTrade trade);

    /**
     * Removes a trade from the {@link WSMerchant merchant}.
     *
     * @param trade the trade.
     */
    void removeTrade(WSTrade trade);

    /**
     * Clears all trades of the {@link WSMerchant merchant}.
     */
    void clearTrades();

}

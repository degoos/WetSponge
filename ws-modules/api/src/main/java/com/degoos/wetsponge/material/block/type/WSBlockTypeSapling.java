package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumWoodType;
import com.degoos.wetsponge.material.block.WSBlockType;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSBlockTypeSapling extends WSBlockType {

	EnumWoodType getWoodType();

	void setWoodType(EnumWoodType woodType);

	int getStage();

	void setStage(int stage);

	int getMaximumStage();

	@Override
	WSBlockTypeSapling clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setString("woodType", getWoodType().name());
		compound.setInteger("stage", getStage());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setWoodType(EnumWoodType.valueOf(compound.getString("woodType")));
		setStage(compound.getInteger("stage"));
		return compound;
	}
}

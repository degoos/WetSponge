package com.degoos.wetsponge.packet;

public interface WSPacket {

    void update();

    void refresh();

    boolean hasChanged();

    Object getHandler();

}

package com.degoos.wetsponge.block;


import com.degoos.wetsponge.block.tileentity.WSTileEntity;
import com.degoos.wetsponge.enums.EnumMapBaseColor;
import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.WSBlockType;
import com.degoos.wetsponge.world.WSLocatable;

public interface WSBlock extends WSLocatable {

	/**
	 * This method returns the {@link Integer} id of the material
	 * of this block. This ID is deprecated, but Spigot uses it as
	 * a base ID.
	 *
	 * @return the material id of the block.
	 */
	int getNumericalId();

	/**
	 * This method returns the {@link String} id of the material.
	 *
	 * @return the material id of the block.
	 */
	String getStringId();

	/**
	 * This method returns the {@link String} id of the material of this block for Minecraft 1.13 or newer.
	 *
	 * @return the material id of the block.
	 */
	String getNewStringId();

	/**
	 * This method returns the {@link String} id of the material of this block for Minecraft 1.12 or older.
	 *
	 * @return the material id of the block.
	 */
	String getOldStringId();

	/**
	 * This method creates a new {@link WSBlockState} from this {@link WSBlock}.
	 * With a {@link WSBlockState} you can edit everything you want of a block.
	 *
	 * @return the {@link WSBlockState}.
	 */
	WSBlockState createState();

	/**
	 * This method returns the tile entity of this block. Tile entities updates automatically,
	 * so you needn't use any update method.
	 *
	 * @return the tile entity of this block.
	 */
	WSTileEntity getTileEntity();

	/**
	 * Returns the {@link WSBlockType block item} of the {@link WSBlock}.
	 * Any changes to the {@link WSBlockType block item} won't affect to the {@link WSBlock}.
	 * For a mutable {@link WSBlockType block item}, use {@link WSBlock#createState()}.
	 *
	 * @return the {@link WSBlockType block item}.
	 */
	WSBlockType getBlockType();

	/**
	 * Gets the relative {@link WSBlock block} by the given {@link EnumBlockFace block direction}.
	 *
	 * @param direction the {@link EnumBlockFace block direction}.
	 * @return the relative {@link WSBlock block}.
	 */
	WSBlock getRelative(EnumBlockFace direction);

	/**
	 * Returns the {@link EnumMapBaseColor map base color} associated to the {@link WSBlock block}.
	 *
	 * @return the {@link EnumMapBaseColor map base color}.
	 */
	EnumMapBaseColor getMapBaseColor();

	/**
	 * @return the handled object.
	 */
	Object getHandled();
}

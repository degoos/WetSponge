package com.degoos.wetsponge.block.tileentity;

/**
 *  WetSponge on 22/04/2017 by gaelito.
 */
public interface WSTileEntityFurnace extends WSTileEntityInventory {

    int getBurnTime();

    void setBurnTime(int burnTime);

    int getCookTime();

    void setCookTime(int cookTime);
}

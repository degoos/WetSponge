package com.degoos.wetsponge.resource;


import java.util.Base64;

public class B64 {

	/**
	 * @param s
	 * 		String to encode to base64
	 *
	 * @return the encoded base64String
	 */
	public static String encode (String s) {
		return encode(s.getBytes());
	}


	/**
	 * @param s
	 * 		String to encode to base64
	 *
	 * @return the encoded base64String
	 */
	public static String encode (byte[] s) {
		return Base64.getEncoder().encodeToString(s);
	}


	/**
	 * @param s
	 * 		Base64 String to decode
	 *
	 * @return the decoded base64String
	 */
	public static String decode (String s) {
		return new String(decode(s.getBytes()));
	}


	/**
	 * @param s
	 * 		Base64 String to decode
	 *
	 * @return the encoded base64byte
	 */
	public static byte[] decode (byte[] s) {
		return Base64.getDecoder().decode(s);
	}
}

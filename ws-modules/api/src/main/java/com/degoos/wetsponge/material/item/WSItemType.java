package com.degoos.wetsponge.material.item;

import com.degoos.wetsponge.material.WSMaterial;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSItemType extends WSMaterial {

	WSItemType clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		return compound;
	}
}

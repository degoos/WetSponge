package com.degoos.wetsponge.enums;


import java.util.Arrays;
import java.util.Optional;

public enum EnumServerVersion {

	MINECRAFT_1_13("1.13", 0),
	MINECRAFT_1_12_2("1.12.2", 1),
	MINECRAFT_1_12_1("1.12.1", 2),
	MINECRAFT_1_12("1.12", 3),
	MINECRAFT_1_11_2("1.11.2", 4),
	MINECRAFT_1_11("1.11", 5),
	MINECRAFT_1_10_2("1.10.2", 6),
	MINECRAFT_1_10("1.10", 7),
	MINECRAFT_1_9_2("1.9.2", 8),
	MINECRAFT_1_9("1.9", 9),
	MINECRAFT_OLD(10);

	private String version;
	private int id;


	EnumServerVersion(int id) {
		this.version = "OLD";
		this.id = id;
	}


	EnumServerVersion(String version, int id) {
		this.version = version;
		this.id = id;
	}


	public static EnumServerVersion getByVersionName(String version) {
		Optional<EnumServerVersion> optional = Arrays.stream(values()).filter(target -> target.getVersion().equals(version)).findAny();
		return optional.orElse(MINECRAFT_OLD);
	}


	public static EnumServerVersion getBySpigotVersionName(String version) {
		if (version.toLowerCase().startsWith("v1_13")) return MINECRAFT_1_13;
		if (version.toLowerCase().startsWith("v1_12_1")) return MINECRAFT_1_12_1;
		if (version.toLowerCase().startsWith("v1_12")) return MINECRAFT_1_12;
		if (version.toLowerCase().startsWith("v1_11_2")) return MINECRAFT_1_11_2;
		if (version.toLowerCase().startsWith("v1_11")) return MINECRAFT_1_11;
		if (version.toLowerCase().startsWith("v1_10_2")) return MINECRAFT_1_10_2;
		if (version.toLowerCase().startsWith("v1_10")) return MINECRAFT_1_10;
		if (version.toLowerCase().startsWith("v1_9_2")) return MINECRAFT_1_9_2;
		if (version.toLowerCase().startsWith("v1_9")) return MINECRAFT_1_9_2;
		return MINECRAFT_OLD;
	}


	public String getVersion() {
		return version;
	}


	public int getId() {
		return id;
	}


	public boolean isNewerThan(EnumServerVersion version) {
		return id < version.id;
	}


	public boolean isOlderThan(EnumServerVersion version) {
		return id > version.id;
	}
}

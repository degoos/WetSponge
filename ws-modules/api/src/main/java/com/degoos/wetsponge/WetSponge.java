package com.degoos.wetsponge;


import com.degoos.wetsponge.command.WSCommandManager;
import com.degoos.wetsponge.command.wetspongecommand.WetSpongeCommand;
import com.degoos.wetsponge.config.WetSpongeConfig;
import com.degoos.wetsponge.config.WetSpongeMessages;
import com.degoos.wetsponge.enums.EnumServerType;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.event.WSEventManager;
import com.degoos.wetsponge.hook.WSHookManager;
import com.degoos.wetsponge.inventory.multiinventory.MultiInventoryListener;
import com.degoos.wetsponge.loader.WetSpongeLoader;
import com.degoos.wetsponge.plugin.WSPluginManager;
import com.degoos.wetsponge.resource.SQLDriverUtils;
import com.degoos.wetsponge.resource.WSBungeeCord;
import com.degoos.wetsponge.server.WSServer;
import com.degoos.wetsponge.timing.WSTimings;
import com.degoos.wetsponge.util.InternalLogger;

import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

/**
 * The WetSponge class is the main class of WetSponge.
 * This is the core of WetSponge, where you can find all managers.
 */
public class WetSponge {

	static {
		LogManager.getLogManager().reset();
	}

	private static EnumServerType serverType;
	private static WetSpongeLoader loader;
	private static WSServer server;
	private static EnumServerVersion version;
	private static WSBungeeCord bungeeCord;
	private static Thread mainThread;
	private static String wetSpongeVersion;
	private static WSTimings timings;


	protected static void load(String wetSpongeVersion, EnumServerType serverType, WetSpongeLoader loader, WSServer server, EnumServerVersion version,
							   WSBungeeCord bungeeCord, Thread mainThread) {
		Logger.getLogger("org.jooq.Constants").setLevel(Level.OFF);

		WetSponge.wetSpongeVersion = wetSpongeVersion;
		WetSponge.serverType = serverType;
		WetSponge.loader = loader;
		WetSponge.server = server;
		WetSponge.version = version;
		WetSponge.bungeeCord = bungeeCord;
		WetSponge.mainThread = mainThread;
	}


	protected static void loadCommon() {
		InternalLogger.sendInfo("Loading config.");
		WetSpongeConfig.load();

		InternalLogger.sendInfo("Loading timings.");
		timings = new WSTimings();

		timings.startTiming("WetSponge load");

		InternalLogger.sendInfo("Loading database drivers.");
		InternalLogger.sendInfo("WetSponge implements JOOQ, so developers can use it to handle connections to databases.");
		SQLDriverUtils.loadDrivers();


		InternalLogger.sendDebug("Debug mode activated.");

		InternalLogger.sendInfo("Loading messages.");
		WetSpongeMessages.load();

		InternalLogger.sendInfo("Loading MultiInventory API.");
		MultiInventoryListener.load();

		InternalLogger.sendInfo("Loading hooks.");
		WSHookManager.getInstance();

		InternalLogger.sendInfo("Loading WetSponge commands.");
		getCommandManager().addCommand(new WetSpongeCommand());

		timings.stopTiming();

		InternalLogger.sendInfo("Loading plugins.");
		getPluginManager().loadPlugins();
		//Timings.setTimingsEnabled(false);
	}


	protected static void unloadCommon() {
		getPluginManager().getPlugins().forEach(plugin -> getPluginManager().unloadPlugin(plugin));
	}


	/**
	 * Check if you are under a {@link EnumServerType} instance.
	 *
	 * @param serverType the {@link EnumServerType} to check
	 * @return whether the current server is under the selected server type or not,
	 */
	public static boolean is(EnumServerType serverType) {
		return WetSponge.serverType == serverType;
	}

	/**
	 * Check if you are under an {@link EnumServerType#SPIGOT Spigot} instance.
	 *
	 * @return whether the current server is under {@link EnumServerType#SPIGOT Spigot}
	 * @deprecated
	 */
	@Deprecated
	public static boolean isSpigot() {
		return serverType == EnumServerType.SPIGOT;
	}


	/**
	 * Checks if you are under an {@link EnumServerType#SPONGE Sponge} instance.
	 *
	 * @return whether the current server is under {@link EnumServerType#SPONGE Sponge}.
	 * @deprecated
	 */
	@Deprecated
	public static boolean isSponge() {
		return serverType == EnumServerType.SPONGE;
	}


	/**
	 * @return the {@link EnumServerType server type}.
	 */
	public static EnumServerType getServerType() {
		return serverType;
	}

	/**
	 * Returns the Wetsponge version.
	 *
	 * @return the Wetsponge version.
	 */
	public static String getWetSpongeVersion() {
		return wetSpongeVersion;
	}

	/**
	 * Returns the {@link WetSpongeLoader WetSponge loader}.
	 *
	 * @return the {@link WetSpongeLoader WetSponge loader}.
	 */
	public static WetSpongeLoader getLoader() {
		return loader;
	}


	/**
	 * Returns the current {@link EnumServerVersion minecraft version} of the {@link WSServer server} instance.
	 *
	 * @return the {@link EnumServerVersion minecraft version} of the {@link WSServer server} instance.
	 */
	public static EnumServerVersion getVersion() {
		return version;
	}


	/**
	 * @return the instance of the current {@link WSServer server}.
	 */
	public static WSServer getServer() {
		return server;
	}


	/**
	 * Returns the {@link WSBungeeCord BungeeCord manager}.
	 * <p>
	 * BungeeCord is an API which allows to connect several
	 * {@link WSServer server}s into a network.
	 * </p>
	 * <p>For more information see: https://www.spigotmc.org/wiki/bungeecord/</p>
	 *
	 * @return the {@link WSBungeeCord BungeeCord manager}.
	 */
	public static WSBungeeCord getBungeeCord() {
		return bungeeCord;
	}


	/**
	 * Returns the {@link WSEventManager event manager}.
	 * <p>
	 * With the {@link WSEventManager event manager} you can register
	 * {@link com.degoos.wetsponge.event.WSListener listener}s and
	 * call {@link com.degoos.wetsponge.event.WSEvent events}.
	 *
	 * @return the {@link WSEventManager event manager}.
	 */
	public static WSEventManager getEventManager() {
		return WSEventManager.getInstance();
	}


	/**
	 * Returns the {@link WSPluginManager plugin manager}.
	 * <p>
	 * With the {@link WSPluginManager plugin manager} you can get
	 * the {@link com.degoos.wetsponge.plugin.WSPlugin WetSponge plugin}s
	 * and the base plugins.
	 *
	 * @return the {@link WSEventManager plugin manager}.
	 */
	public static WSPluginManager getPluginManager() {
		return WSPluginManager.getInstance();
	}


	/**
	 * Returns the {@link WSCommandManager command manager}.
	 * <p>
	 * With the {@link WSCommandManager command manager} you can
	 * register, unregister or get {@link com.degoos.wetsponge.command.WSCommand WetSponge command}s.
	 *
	 * @return the {@link WSCommandManager command manager}.
	 */
	public static WSCommandManager getCommandManager() {
		return WSCommandManager.getInstance();
	}

	/**
	 * Returns the {@link WSHookManager hook manager}.
	 * <p>
	 * With the {@link WSHookManager hook manager} you can some features of the base plugins
	 * WetSponge supports. You can help us creating new hooks!
	 *
	 * @return the {@link WSHookManager hook manager}.
	 */
	public static WSHookManager getHookManager() {
		return WSHookManager.getInstance();
	}

	/**
	 * Returns the {@link WSTimings timings manager}.
	 * <p>
	 * Timings can be used to debug lag on your plugins easily.
	 *
	 * @return the {@link WSTimings timings manager}.
	 */
	public static WSTimings getTimings() {
		return timings;
	}

	/**
	 * Returns the main {@link Thread thread} of Minecraft.
	 *
	 * @return the main {@link Thread thread}.
	 */
	public static Thread getMainThread() {
		return mainThread;
	}
}

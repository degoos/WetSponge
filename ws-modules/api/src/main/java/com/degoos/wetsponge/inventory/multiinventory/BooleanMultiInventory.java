package com.degoos.wetsponge.inventory.multiinventory;

import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.inventory.WSInventory;
import com.degoos.wetsponge.item.WSItemStack;
import com.degoos.wetsponge.material.WSBlockTypes;
import com.degoos.wetsponge.text.WSText;

import java.util.*;

public abstract class BooleanMultiInventory implements MultiInventory {

	private String id;
	private WSText name;
	private InventoryRows rows;
	protected Map<Integer, WSInventory> inventories;
	private WSPlayer player;
	private boolean hotBar;


	public BooleanMultiInventory(WSText name, WSPlayer player, String id) {
		this.player = player;
		this.name = name;
		this.id = id;
		this.rows = InventoryRows.THREE;
		inventories = new HashMap<>();
		this.hotBar = false;
		inventories.put(0, createInv());
		setItem(11, WSItemStack.of(WSBlockTypes.GREEN_STAINED_HARDENED_CLAY));
		setItem(15, WSItemStack.of(WSBlockTypes.RED_STAINED_HARDENED_CLAY));
	}

	public BooleanMultiInventory(WSText name, WSPlayer player, String id, WSText noName, WSText yesName) {
		this.player = player;
		this.name = name;
		this.id = id;
		this.rows = InventoryRows.THREE;
		inventories = new HashMap<>();
		this.hotBar = false;
		inventories.put(0, createInv());

		setItem(11, WSItemStack.of(WSBlockTypes.GREEN_STAINED_HARDENED_CLAY).setDisplayName(yesName)
				.update());
		setItem(15, WSItemStack.of(WSBlockTypes.RED_STAINED_HARDENED_CLAY).setDisplayName(noName).update());
	}


	public BooleanMultiInventory(WSText name, WSPlayer player, WSItemStack noItem, WSItemStack yesItem, String id) {
		this.player = player;
		this.name = name;
		this.id = id;
		this.rows = InventoryRows.THREE;
		inventories = new HashMap<>();
		this.hotBar = false;
		inventories.put(0, createInv());
		setItem(11, yesItem);
		setItem(15, noItem);
	}


	public void setItem(int slot, WSItemStack item) {
		if (inventories.size() == 1) {
			getInventory(0).ifPresent(inventory -> inventory.setItem(item, slot));
			return;
		}
		int inventory = Math.floorDiv(slot, rows.getSlots());
		int invSlot = slot - inventory * rows.getSlots();
		setItem(inventory, invSlot, item);
	}


	public void setItem(int inventory, int invSlot, WSItemStack item) {
		if (inventories.size() == 1) {
			getInventory(0).ifPresent(inv -> inv.setItem(item, invSlot));
			return;
		}
		if (!inventories.containsKey(inventory) || invSlot >= rows.getSlots()) return;
		inventories.get(inventory).setItem(item, invSlot);
	}


	@Override
	public void setItemOnHotbar(int slot, WSItemStack itemStack) {
		if (inventories.size() > 1 && (slot == 2 || slot == 3 || slot == 5 || slot == 6)) return;
		if (inventories.size() == 1 && !hotBar) return;
		if (slot < 0 || slot > 8) return;
		int realSlot = rows.getSlots() + slot;
		inventories.values().forEach(inventory -> inventory.setItem(itemStack, realSlot));
	}


	public Set<WSItemStack> getContents() {
		Set<WSItemStack> items = new HashSet<>();
		for (int i = 0; i < size(); i++) getItem(i).ifPresent(items::add);
		return items;
	}


	public Map<Integer, WSItemStack> getContentsMap() {
		Map<Integer, WSItemStack> map = new HashMap<>();
		for (int i = 0; i < size(); i++) {
			Optional<WSItemStack> itemStack = getItem(i);
			if (itemStack.isPresent()) map.put(i, itemStack.get());
		}
		return map;
	}


	public Map<Integer, WSInventory> getInventories() {
		return new HashMap<>(inventories);
	}


	public Optional<WSInventory> getInventory(int i) {
		return Optional.ofNullable(inventories.get(i));
	}


	public void open(int i) {
		if (!inventories.containsKey(i)) return;

		MultiInventoryListener.change.add(player);
		player.openInventory(inventories.get(i));
		MultiInventoryListener.players.put(player, new MultiInvEntry(this, i));
		MultiInventoryListener.change.remove(player);
	}


	public void openFirst() {
		open(0);
	}


	public void openLast() {
		int i = inventories.size() - 1;
		open(i);
	}


	public void openNext() {
		if (!MultiInventoryListener.players.containsKey(player) || !MultiInventoryListener.players.get(player).getMultiInventory().equals(this)) return;
		int i = MultiInventoryListener.players.get(player).getInventory();
		if (i + 1 >= inventories.size()) return;
		open(i + 1);
	}


	public void openPrevious() {
		if (!MultiInventoryListener.players.containsKey(player) || !MultiInventoryListener.players.get(player).getMultiInventory().equals(this)) return;
		int i = MultiInventoryListener.players.get(player).getInventory();
		if (i - 1 < 0) return;
		open(i - 1);
	}


	private WSInventory createInv() {
		return WSInventory.of(rows.getSlots(), name);
	}


	public WSPlayer getPlayer() {
		return player;
	}


	public InventoryRows getRows() {
		return rows;
	}


	public WSText getName() {
		return name;
	}


	public String getId() {
		return id;
	}


	public boolean hasHotBar() {
		return inventories.size() > 1 || hotBar;
	}


	@Override
	public boolean hasHotbarIfUnique() {
		return hotBar;
	}


	@Override
	public Optional<WSItemStack> getItem(int slot) {
		if (inventories.size() == 1) return inventories.get(0).getItem(slot);
		int invNumber = Math.floorDiv(slot, rows.getSlots());
		if (inventories.size() <= invNumber) return Optional.empty();
		return inventories.get(invNumber).getItem(slot % rows.getSlots());
	}


	public int size() {
		return inventories.size() == 1 && hotBar ? rows.getRealSlots() : inventories.size() * rows.getSlots();
	}

	@Override
	public void onClick(MultiInventoryClickEvent event) {

	}

	@Override
	public void onPlayerInventoryClick(MultiInventoryClickPlayerInvEvent event) {

	}

	@Override
	public void onHotbarClick(MultiInventoryClickHotbarEvent event) {

	}

	@Override
	public void onClose(MultiInventoryCloseEvent event) {

	}

	public abstract void onNo();

	public abstract void onYes();
}

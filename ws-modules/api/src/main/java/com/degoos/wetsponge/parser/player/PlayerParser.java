package com.degoos.wetsponge.parser.player;

import com.degoos.wetsponge.entity.living.player.WSPlayer;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

public class PlayerParser {

	private static Map<UUID, WSPlayer> players = new HashMap<>();

	public static WSPlayer addPlayer(WSPlayer player) {
		players.putIfAbsent(player.getUniqueId(), player);
		return players.get(player.getUniqueId());
	}

	public static void removePlayer(WSPlayer player) {
		players.remove(player.getUniqueId());
	}

	public static void resetPlayer(Object handled, UUID uuid) {
		/*if (handled instanceof WSPlayer) handled = ((WSPlayer) handled).getHandled();
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				((SpigotPlayer) getPlayer(uuid).orElseThrow(NullPointerException::new)).setHandled(handled);
				break;
			case SPONGE:
				((SpongePlayer) getPlayer(uuid).orElseThrow(NullPointerException::new)).setHandled(handled);
				break;
		}*/
	}

	public static Optional<WSPlayer> getPlayer(UUID uuid) {
		/*WSPlayer player = players.get(uuid);
		if (player != null) return Optional.of(player);
		switch (WetSponge.getServerType()) {
			case SPONGE:
				return SpongePlayerParser.checkPlayer(uuid);
			case SPIGOT:
			case PAPER_SPIGOT:
				return SpigotPlayerParser.checkPlayer(uuid);
			default:
				return Optional.empty();
		}*/
		return null;
	}

	public static WSPlayer getOrCreatePlayer(Object entity, UUID uuid) {
		/*Optional<WSPlayer> optional = getPlayer(uuid);
		if (optional.isPresent()) return optional.get();

		switch (WetSponge.getServerType()) {
			case SPONGE:
				return addPlayer(SpongePlayerParser.newInstance(entity));
			case SPIGOT:
			case PAPER_SPIGOT:
				return addPlayer(SpigotPlayerParser.newInstance(entity));
			default:
				return null;
		}*/
		return null;
	}

	public static Optional<WSPlayer> getPlayer(String name) {
		/*Optional<WSPlayer> optional = players.values().stream().filter(player -> player.getName().equals(name)).findAny();
		if (optional.isPresent()) return optional;
		switch (WetSponge.getServerType()) {
			case SPONGE:
				return SpongePlayerParser.checkPlayer(name);
			case SPIGOT:
			case PAPER_SPIGOT:
				return SpigotPlayerParser.checkPlayer(name);
			default:
				return Optional.empty();
		}*/
		return null;
	}

}

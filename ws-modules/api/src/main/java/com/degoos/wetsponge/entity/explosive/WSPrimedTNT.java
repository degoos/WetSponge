package com.degoos.wetsponge.entity.explosive;

import com.degoos.wetsponge.entity.living.WSLivingEntity;
import java.util.Optional;

public interface WSPrimedTNT extends WSFusedExplosive {

	Optional<WSLivingEntity> getDetonator();

	void setDetonator(WSLivingEntity entity);

}

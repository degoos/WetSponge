package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockTypeSandType;
import com.degoos.wetsponge.material.block.WSBlockType;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSBlockTypeSand extends WSBlockType {

	EnumBlockTypeSandType getSandType();

	void setSandType(EnumBlockTypeSandType sandType);

	@Override
	WSBlockTypeSand clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setString("sandType", getSandType().name());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setSandType(EnumBlockTypeSandType.valueOf(compound.getString("sandType")));
		return compound;
	}
}

package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.material.block.WSBlockTypeLightable;

public interface WSBlockTypeRedstoneLamp extends WSBlockTypeLightable {

	@Override
	WSBlockTypeRedstoneLamp clone();
}

package com.degoos.wetsponge.block.tileentity;

import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.text.WSText;

import java.util.List;
import java.util.Optional;

public interface WSTileEntitySign extends WSTileEntity {

    void setLine(int line, WSText value);

    Optional<WSText> getLine(int line);

    List<WSText> getLines();

    void setLines(WSText[] lines);

    void editSign (WSPlayer player);

}

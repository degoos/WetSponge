package com.degoos.wetsponge.util.blockray;

import com.degoos.wetsponge.block.WSBlock;
import com.degoos.wetsponge.bridge.util.blockray.BridgeBlockRay;
import com.degoos.wetsponge.entity.living.WSLivingEntity;
import com.degoos.wetsponge.material.WSBlockTypes;
import com.degoos.wetsponge.material.block.WSBlockType;
import com.degoos.wetsponge.world.WSLocation;
import com.degoos.wetsponge.world.WSWorld;
import com.flowpowered.math.vector.Vector3d;

import java.util.Iterator;
import java.util.Optional;
import java.util.function.Predicate;

/**
 * Represents a block ray which traces a line and returns all block boundaries intersected in order.
 * The block the start location is inside is never detected.
 * <p>
 * WSBlockRay extends the {@link Iterator} interface. Using the method {@link Iterator#remove()} will throw an exception.
 * <p>
 * The block ray will ignore blocks whose block types return true when they're tested on the skip filter.
 * The block ray stops when the distance limit is reached.
 * <p>
 * To get a block ray for an entities' line of sight, use the method {@link WSBlockRay#of(WSLivingEntity, double, Predicate)}.
 */
public interface WSBlockRay extends Iterator<WSBlock> {

	/**
	 * A filter that returns true always.
	 */
	Predicate<WSBlockType> ALL_BLOCKS = wsBlockType -> true;

	/**
	 * A filter that returns true only if the block is air.
	 */
	Predicate<WSBlockType> ONLY_AIR = wsBlockType -> wsBlockType.getStringId().equalsIgnoreCase(WSBlockTypes.AIR.getStringId());

	/**
	 * Returns a filter that returns true only if the block item of the inspected block and the given one is the same.
	 *
	 * @param type the given {@link WSBlockType block item}.
	 * @return the filter.
	 */
	static Predicate<WSBlockType> blockTypeFilter(WSBlockType type) {
		return wsBlockType -> wsBlockType.equals(type);
	}

	/**
	 * Creates a WSBlockRay builder.
	 *
	 * @param world the world where the block ray will work.
	 * @return the builder.
	 */
	static Builder builder(WSWorld world) {
		return BridgeBlockRay.builder(world);
	}

	/**
	 * Creates a WSBlockRay using a {@link WSLivingEntity living entity}'s line of sight, a max distance and a {@link Predicate skip filter}.
	 *
	 * @param entity      the {@link WSLivingEntity living entity}.
	 * @param maxDistance the max distance.
	 * @param skipFilter  the {@link Predicate skip filter}.
	 * @return the WSBlockRay.
	 */
	static WSBlockRay of(WSLivingEntity entity, double maxDistance, Predicate<WSBlockType> skipFilter) {
		WSLocation location = entity.getLocation();
		return builder(entity.getWorld()).origin(location.toVector3d()).direction(location.getFacingDirection()).maxDistance(maxDistance)
				.yOffset(entity.getEyeHeight()).skipFilter(skipFilter).build();
	}


	/**
	 * Returns the last detected {@link WSBlock block} within the area of the {@link WSBlockRay}.
	 *
	 * @return the last detected {@link WSBlock block}.
	 */
	Optional<WSBlock> end();

	/**
	 * Represents the builder for {@link WSBlockRay}s.
	 */
	interface Builder {

		/**
		 * Sets the origin position of the {@link WSBlockRay}.
		 *
		 * @param origin the origin position.
		 * @return the builder.
		 */
		Builder origin(Vector3d origin);

		/**
		 * Sets the direction of the {@link WSBlockRay}.
		 *
		 * @param direction the direction.
		 * @return the builder.
		 */
		Builder direction(Vector3d direction);

		/**
		 * Sets the yOffset of the {@link WSBlockRay}.
		 * This value will be added to the yCord of the origin.
		 *
		 * @param yOffset the yOffset.
		 * @return the builder.
		 */
		Builder yOffset(double yOffset);

		/**
		 * Sets the max range where the {@link WSBlockRay} will search blocks.
		 *
		 * @param maxDistance the max distance.
		 * @return the builder.
		 */
		Builder maxDistance(double maxDistance);

		/**
		 * Sets the skip filter of the {@link WSBlockRay}.
		 * Every block whose block item is accepted by the filter will be ignored.
		 *
		 * @param skipFilter the skip filter.
		 * @return the builder.
		 */
		Builder skipFilter(Predicate<WSBlockType> skipFilter);

		/**
		 * Builds a new {@link WSBlockRay}.
		 *
		 * @return the {@link WSBlockRay}.
		 * @throws NullPointerException whether the origin or the direction are {@code null} or whether the max distance is lower or equal to 0.
		 */
		WSBlockRay build();

	}
}

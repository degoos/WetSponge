package com.degoos.wetsponge.event.entity.player.interact;

import com.degoos.wetsponge.block.WSBlockSnapshot;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.enums.EnumTristate;
import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.util.Validate;
import com.degoos.wetsponge.world.WSLocation;
import com.flowpowered.math.vector.Vector3d;

import java.util.Optional;

public class WSPlayerInteractBlockEvent extends WSPlayerInteractEvent {

	private WSBlockSnapshot targetBlock;
	private EnumBlockFace targetSide;
	private Optional<Vector3d> clickedPosition;
	private Optional<WSLocation> blockLocation;

	public WSPlayerInteractBlockEvent(WSPlayer player, WSBlockSnapshot targetBlock, EnumBlockFace targetSide,
									  Optional<Vector3d> clickedPosition, Optional<WSLocation> blockLocation) {
		super(player);
		Validate.notNull(targetBlock, "Target block cannot be null!");
		Validate.notNull(targetSide, "Target side cannot be null!");
		Validate.notNull(clickedPosition, "Position cannot be null!");
		this.targetBlock = targetBlock;
		this.targetSide = targetSide;
		this.clickedPosition = clickedPosition;
		this.blockLocation = blockLocation;
	}

	public WSBlockSnapshot getTargetBlock() {
		return targetBlock;
	}

	public EnumBlockFace getTargetSide() {
		return targetSide;
	}

	/**
	 * Returns the clicked position of the block.
	 * <p>WARNING! This is not the block position! To get the block position use {@link #getTargetBlock()}.</p>
	 * <p>This method only works on {@link com.degoos.wetsponge.enums.EnumServerType#SPONGE Sponge}.</p>
	 *
	 * @return the clicked position of the block.
	 */
	public Optional<Vector3d> getClickedPosition() {
		return clickedPosition;
	}

	/**
	 * Returns the block position.
	 *
	 * @return the block position.
	 */
	public Optional<WSLocation> getBlockLocation() {
		return blockLocation;
	}

	public static class Primary extends WSPlayerInteractBlockEvent {

		public Primary(WSPlayer player, WSBlockSnapshot targetBlock,
					   EnumBlockFace targetSide, Optional<Vector3d> clickedPosition, Optional<WSLocation> blockLocation) {
			super(player, targetBlock, targetSide, clickedPosition, blockLocation);
		}

		public static class MainHand extends Primary {

			public MainHand(WSPlayer player, WSBlockSnapshot targetBlock,
							EnumBlockFace targetSide, Optional<Vector3d> clickedPosition, Optional<WSLocation> blockLocation) {
				super(player, targetBlock, targetSide, clickedPosition, blockLocation);
			}
		}

		public static class OffHand extends Primary {

			public OffHand(WSPlayer player, WSBlockSnapshot targetBlock,
						   EnumBlockFace targetSide, Optional<Vector3d> clickedPosition, Optional<WSLocation> blockLocation) {
				super(player, targetBlock, targetSide, clickedPosition, blockLocation);
			}
		}
	}

	public static class Secondary extends WSPlayerInteractBlockEvent {

		EnumTristate useBlockResult, useItemResult, originalUseBlockResult, originalUseItemResult;

		public Secondary(WSPlayer player, WSBlockSnapshot targetBlock, EnumBlockFace targetSide, Optional<Vector3d> clickedPosition,
						 Optional<WSLocation> blockLocation, EnumTristate useBlockResult, EnumTristate useItemResult,
						 EnumTristate originalUseBlockResult, EnumTristate originalUseItemResult) {
			super(player, targetBlock, targetSide, clickedPosition, blockLocation);
			this.useBlockResult = useBlockResult;
			this.useItemResult = useItemResult;
			this.originalUseBlockResult = originalUseBlockResult;
			this.originalUseItemResult = originalUseItemResult;
		}

		public EnumTristate getOriginalUseBlockResult() {
			return originalUseBlockResult;
		}

		public EnumTristate getOriginalUseItemResult() {
			return originalUseItemResult;
		}

		public EnumTristate getUseBlockResult() {
			return useBlockResult;
		}

		public void setUseBlockResult(EnumTristate useBlockResult) {
			this.useBlockResult = useBlockResult;
		}

		public EnumTristate getUseItemResult() {
			return useItemResult;
		}

		public void setUseItemResult(EnumTristate useItemResult) {
			this.useItemResult = useItemResult;
		}

		public static class MainHand extends Secondary {

			public MainHand(WSPlayer player, WSBlockSnapshot targetBlock, EnumBlockFace targetSide,
							Optional<Vector3d> clickedPosition, Optional<WSLocation> blockLocation, EnumTristate useBlockResult,
							EnumTristate useItemResult, EnumTristate originalUseBlockResult, EnumTristate originalUseItemResult) {
				super(player, targetBlock, targetSide, clickedPosition, blockLocation, useBlockResult, useItemResult, originalUseBlockResult, originalUseItemResult);
			}
		}

		public static class OffHand extends Secondary {

			public OffHand(WSPlayer player, WSBlockSnapshot targetBlock, EnumBlockFace targetSide, Optional<Vector3d> clickedPosition,
						   Optional<WSLocation> blockLocation, EnumTristate useBlockResult, EnumTristate useItemResult,
						   EnumTristate originalUseBlockResult, EnumTristate originalUseItemResult) {
				super(player, targetBlock, targetSide, clickedPosition, blockLocation, useBlockResult, useItemResult, originalUseBlockResult, originalUseItemResult);
			}
		}
	}
}

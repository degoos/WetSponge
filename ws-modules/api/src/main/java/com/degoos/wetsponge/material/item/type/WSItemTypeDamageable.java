package com.degoos.wetsponge.material.item.type;

import com.degoos.wetsponge.material.item.WSItemType;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSItemTypeDamageable extends WSItemType {

	int getDamage();

	void setDamage(int damage);

	int getMaxUses();

	@Override
	WSItemTypeDamageable clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setInteger("damage", getDamage());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setDamage(compound.getInteger("damage"));
		return compound;
	}
}

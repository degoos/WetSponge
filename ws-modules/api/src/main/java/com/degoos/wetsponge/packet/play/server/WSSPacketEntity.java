package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.packet.WSPacket;
import com.flowpowered.math.vector.Vector2i;
import com.flowpowered.math.vector.Vector3i;

public interface WSSPacketEntity extends WSPacket {

    int getEntityId();

    void setEntityId(int entityId);

    Vector3i getPosition();

    void setPosition(Vector3i position);

    Vector2i getRotation();

    void setRotation(Vector2i rotation);

    boolean isOnGround();

    void setOnGround(boolean onGround);
}

package com.degoos.wetsponge.firework;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.bridge.firework.BridgeFireworkEffect;
import com.degoos.wetsponge.color.WSColor;
import com.degoos.wetsponge.enums.EnumFireworkShape;
import java.util.Collection;
import java.util.List;

public interface WSFireworkEffect {

	public static Builder builder() {
		return BridgeFireworkEffect.builder();
	}

	/**
	 * Gets whether the {@link WSFireworkEffect firework effect}} will flicker when
	 * detonated.
	 *
	 * @return Whether the effect will flicker
	 */
	boolean flickers();

	/**
	 * Gets whether the {@link WSFireworkEffect firework effect}} will have a trail
	 * when detonated.
	 *
	 * @return Whether the effect will have a trail
	 */
	boolean hasTrail();

	/**
	 * Gets the ordered list of {@link WSColor color}s.
	 * <p>In some implementations, the order of {@link WSColor color}s defines the colors
	 * showing from edge to center of the firework explosion.</p>
	 *
	 * @return The list of {@link WSColor color}s
	 */
	List<WSColor> getColors();

	/**
	 * Gets the ordered list of {@link WSColor color}s.
	 * <p>Normally in vanilla, the order of {@link WSColor color}s defines the colors
	 * showing from edge to center of the firework explosion.</p>
	 *
	 * @return The list of {@link WSColor color}s
	 */
	List<WSColor> getFadeColors();

	/**
	 * Gets the explosion shape.
	 *
	 * @return The explosion shape
	 */
	EnumFireworkShape getShape();

	/**
	 * Transforms the {@link WSFireworkEffect firework effect} into a {@link Builder builder}.
	 * The new {@link Builder builder} won't do any change to the {@link WSFireworkEffect firework effect}.
	 *
	 * @return the {@link Builder builder}
	 */
	Builder toBuilder();

	/**
	 * Returns a new copy of the {@link WSFireworkEffect firework effect}.
	 *
	 * @return a new copy of the {@link WSFireworkEffect firework effect}.
	 */
	WSFireworkEffect clone();

	/**
	 * Returns the handled object of the {@link WSFireworkEffect firework effect}.
	 *
	 * @return the handled object.
	 */
	Object getHandled();

	interface Builder {

		/**
		 * Sets whether the {@link WSFireworkEffect firework effect}} is going to have a trail
		 * or not.
		 *
		 * @param trail Whether the firework will have a trail
		 *
		 * @return the builder, for chaining
		 */
		Builder trail(boolean trail);

		/**
		 * Sets whether the {@link WSFireworkEffect firework effect}} is going to flicker
		 * on explosion.
		 *
		 * @param flicker Whether the explosion will flicker
		 *
		 * @return the builder, for chaining
		 */
		Builder flicker(boolean flicker);

		/**
		 * Adds the given {@link WSColor color} to the initial explosion colors.
		 * <p>Colors can be mixed and matched in the order they are added
		 * in.</p>
		 *
		 * @param color The color to add to the explosion
		 *
		 * @return the builder, for chaining
		 */
		Builder color(WSColor color);

		/**
		 * Adds the given {@link WSColor color}s to the initial explosion colors.
		 * <p>Colors can be mixed and matched in the order they are added
		 * in.</p>
		 *
		 * @param colors The colors to add to the explosion
		 *
		 * @return the builder, for chaining
		 */
		Builder colors(WSColor... colors);

		/**
		 * Adds the given {@link WSColor color}s to the initial explosion colors.
		 * <p>Colors can be mixed and matched in the order they are added
		 * in.</p>
		 *
		 * @param colors The colors to add to the explosion
		 *
		 * @return the builder, for chaining
		 */
		Builder colors(Collection<WSColor> colors);

		/**
		 * Adds the given {@link WSColor color} to the fade colors.
		 * <p>Colors can be mixed and matched in the order they are added
		 * in.</p>
		 *
		 * @param color The colors to add to the fade
		 *
		 * @return the builder, for chaining
		 */
		Builder fade(WSColor color);

		/**
		 * Adds the given {@link WSColor color}s to the fade colors.
		 * <p>Colors can be mixed and matched in the order they are added
		 * in.</p>
		 *
		 * @param colors The colors to add to the fade
		 *
		 * @return the builder, for chaining
		 */
		Builder fades(WSColor... colors);

		/**
		 * Adds the given {@link WSColor color}s to the fade colors.
		 * <p>Colors can be mixed and matched in the order they are added
		 * in.</p>
		 *
		 * @param colors The colors to add to the fade
		 *
		 * @return the builder, for chaining
		 */
		Builder fades(Collection<WSColor> colors);

		/**
		 * Sets the shape of the {@link EnumFireworkShape firework shape} explosion.
		 *
		 * @param shape The shape of the explosion
		 *
		 * @return the builder, for chaining
		 */
		Builder shape(EnumFireworkShape shape);

		/**
		 * Builds a {@link WSFireworkEffect firework effect}} based on the current state of the
		 * builder.
		 *
		 * @return A newly created firework effect
		 */
		WSFireworkEffect build();
	}
}

package com.degoos.wetsponge.enums.block;


import java.util.Arrays;
import java.util.Optional;

public enum EnumBlockTypePrismarineType {

    ROUGH(0), BRICKS(1), DARK(2);

    private int value;


    EnumBlockTypePrismarineType(int value) {
        this.value = value;
    }


    public static Optional<EnumBlockTypePrismarineType> getByValue(int value) {
        return Arrays.stream(values()).filter(target -> target.getValue() == value).findAny();
    }


    public static Optional<EnumBlockTypePrismarineType> getByName(String name) {
        return Arrays.stream(values()).filter(target -> target.name().equalsIgnoreCase(name)).findAny();
    }


    public int getValue() {
        return value;
    }
}

package com.degoos.wetsponge.entity.projectile;

import com.degoos.wetsponge.entity.WSEntity;

public interface WSProjectile extends WSEntity {

    WSProjectileSource getShooter();

    void setShooter(WSProjectileSource source);

}

package com.degoos.wetsponge.material.item.type;

import com.degoos.wetsponge.enums.EnumEntityType;
import com.degoos.wetsponge.material.item.WSItemType;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSItemTypeSpawnEgg extends WSItemType {

	EnumEntityType getEntityType();

	void setEntityType(EnumEntityType entityType);

	@Override
	WSItemTypeSpawnEgg clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setString("entity", getEntityType().name());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setEntityType(EnumEntityType.valueOf(compound.getString("entity")));
		return compound;
	}
}

package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockTypeTallGrassType;
import com.degoos.wetsponge.material.block.WSBlockType;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSBlockTypeTallGrass extends WSBlockType {

	EnumBlockTypeTallGrassType getTallGrassType();

	void setTallGrassType(EnumBlockTypeTallGrassType tallGrassType);

	@Override
	WSBlockTypeTallGrass clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setString("tallGrassType", getTallGrassType().name());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setTallGrassType(EnumBlockTypeTallGrassType.valueOf(compound.getString("tallGrassType")));
		return compound;
	}
}

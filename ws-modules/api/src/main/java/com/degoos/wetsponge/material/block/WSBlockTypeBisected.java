package com.degoos.wetsponge.material.block;

import com.degoos.wetsponge.enums.block.EnumBlockTypeBisectedHalf;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSBlockTypeBisected extends WSBlockType {

	EnumBlockTypeBisectedHalf getHalf();

	void setHalf(EnumBlockTypeBisectedHalf half);

	@Override
	WSBlockTypeBisected clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setString("half", getHalf().name());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setHalf(EnumBlockTypeBisectedHalf.valueOf(compound.getString("half")));
		return compound;
	}
}

package com.degoos.wetsponge.material.block;

import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSBlockTypeAttachable extends WSBlockType {

	boolean isAttached();

	void setAttached(boolean attached);

	@Override
	WSBlockTypeAttachable clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setBoolean("attached", isAttached());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setAttached(compound.getBoolean("attached"));
		return compound;
	}
}

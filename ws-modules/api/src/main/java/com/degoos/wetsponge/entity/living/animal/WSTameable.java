package com.degoos.wetsponge.entity.living.animal;

import java.util.Optional;
import java.util.UUID;

/**
 * Represents a tameable {@link com.degoos.wetsponge.entity.WSEntity entity}.
 */
public interface WSTameable {

	/**
	 * Returns whether the {@link WSTameable tameable entity} is tamed.
	 *
	 * @return whether the {@link WSTameable tameable entity} is tamed.
	 */
	boolean isTamed();

	/**
	 * Returns the tamer of the {@link WSTameable tameable entity}, if present.
	 *
	 * @return the tamer of the {@link WSTameable tameable entity}.
	 */
	Optional<UUID> getTamer();

	/**
	 * Sets the tamer of the {@link WSTameable tameable entity}, or null.
	 *
	 * @param tamer the tamer of the {@link WSTameable tameable entity}, or null.
	 */
	void setTamer(UUID tamer);

}

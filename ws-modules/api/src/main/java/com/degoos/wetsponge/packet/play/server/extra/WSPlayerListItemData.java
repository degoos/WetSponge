package com.degoos.wetsponge.packet.play.server.extra;

import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.enums.EnumGameMode;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.user.WSGameProfile;
import com.degoos.wetsponge.util.Validate;

import java.util.Objects;

public class WSPlayerListItemData {

	private int ping;
	private EnumGameMode gameMode;
	private WSGameProfile gameProfile;
	private WSText displayName;

	public WSPlayerListItemData(int ping, EnumGameMode gameMode, WSGameProfile gameProfile, WSText displayName) {
		Validate.notNull(gameProfile, "Game profile cannot be null!");
		Validate.isTrue(gameProfile.getName().isPresent(), "Game profile must have a name!");
		this.ping = ping;
		this.gameMode = gameMode;
		this.gameProfile = gameProfile;
		this.displayName = displayName;
	}

	public WSPlayerListItemData(WSPlayer player) {
		this.ping = player.getPing();
		this.gameMode = player.getGameMode();
		this.gameProfile = player.getProfile();
		this.displayName = player.getDisplayedName() == null ? WSText.of(player.getName()) : player.getDisplayedName();
	}

	public int getPing() {
		return ping;
	}

	public void setPing(int ping) {
		this.ping = ping;
	}

	public EnumGameMode getGameMode() {
		return gameMode;
	}

	public void setGameMode(EnumGameMode gameMode) {
		this.gameMode = gameMode;
	}

	public WSGameProfile getGameProfile() {
		return gameProfile;
	}

	public void setGameProfile(WSGameProfile gameProfile) {
		Validate.notNull(gameProfile, "Game profile cannot be null!");
		Validate.isTrue(gameProfile.getName().isPresent(), "Game profile must have a name!");
		this.gameProfile = gameProfile;
	}

	public WSText getDisplayName() {
		return displayName;
	}

	public void setDisplayName(WSText displayName) {
		this.displayName = displayName;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		WSPlayerListItemData that = (WSPlayerListItemData) o;
		return Objects.equals(gameProfile, that.gameProfile);
	}

	@Override
	public int hashCode() {

		return Objects.hash(gameProfile);
	}
}

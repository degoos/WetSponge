package com.degoos.wetsponge.packet.play.client;

import com.degoos.wetsponge.bridge.packet.BridgeClientPacket;
import com.degoos.wetsponge.enums.EnumEntityAction;
import com.degoos.wetsponge.packet.WSPacket;

public interface WSCPacketEntityAction extends WSPacket {

	public static WSCPacketEntityAction of(int entityId, EnumEntityAction entityAction, int jumpBoost) {
		return BridgeClientPacket.newWSCPacketEntityAction(entityId, entityAction, jumpBoost);
	}

	int getEntityId();

	void setEntityId(int entityId);

	EnumEntityAction getAction();

	void setAction(EnumEntityAction entityAction);

	int getJumpBoost();

	void setJumpBoost(int jumpBoost);
}

package com.degoos.wetsponge.map;

import com.degoos.wetsponge.enums.EnumMapDecorationType;
import com.degoos.wetsponge.util.Validate;
import com.flowpowered.math.vector.Vector2i;

public class WSMapDecoration {

	private Vector2i position;
	private EnumMapDecorationType type;
	private int rotation;

	public WSMapDecoration(Vector2i position, EnumMapDecorationType type, int rotation) {
		Validate.notNull(position, "Position cannot be null!");
		Validate.notNull(type, "Type cannot be null!");
		this.position = position;
		this.type = type;
		this.rotation = rotation;
	}

	public Vector2i getPosition() {
		return position;
	}

	public void setPosition(Vector2i position) {
		Validate.notNull(position, "Position cannot be null!");
		this.position = position;
	}

	void setPosition(int x, int z) {
		this.position = new Vector2i(x, z);
	}

	public EnumMapDecorationType getType() {
		return type;
	}

	public void setType(EnumMapDecorationType type) {
		Validate.notNull(type, "Type cannot be null!");
		this.type = type;
	}

	public int getRotation() {
		return rotation;
	}

	public void setRotation(int rotation) {
		this.rotation = rotation;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		WSMapDecoration that = (WSMapDecoration) o;

		if (rotation != that.rotation) return false;
		if (!position.equals(that.position)) return false;
		return type == that.type;
	}

	@Override
	public int hashCode() {
		int result = position.hashCode();
		result = 31 * result + type.hashCode();
		result = 31 * result + rotation;
		return result;
	}
}

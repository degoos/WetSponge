package com.degoos.wetsponge.enums.block;

import com.flowpowered.math.vector.Vector3d;

import java.util.Arrays;
import java.util.Optional;

public enum EnumBlockFace {

	NORTH(0, 0, -1, "NORTH", 8),
	EAST(1, 0, 0, "EAST", 12),
	SOUTH(0, 0, 1, "SOUTH", 0),
	WEST(-1, 0, 0, "WEST", 4),
	UP(0, 1, 0, "UP", -1),
	DOWN(0, -1, 0, "DOWN", -1),
	NORTH_EAST(NORTH, EAST, "NORTHEAST", 10),
	NORTH_WEST(NORTH, WEST, "NORTHWEST", 6),
	SOUTH_EAST(SOUTH, EAST, "SOUTHEAST", 14),
	SOUTH_WEST(SOUTH, WEST, "SOUTHWEST", 2),
	WEST_NORTH_WEST(WEST, NORTH_WEST, "WEST_NORTHWEST", 5),
	NORTH_NORTH_WEST(NORTH, NORTH_WEST, "NORTH_NORTHWEST", 7),
	NORTH_NORTH_EAST(NORTH, NORTH_EAST, "NORTH_NORTHEAST", 9),
	EAST_NORTH_EAST(EAST, NORTH_EAST, "EAST_NORTHEAST", 11),
	EAST_SOUTH_EAST(EAST, SOUTH_EAST, "EAST_SOUTHEAST", 13),
	SOUTH_SOUTH_EAST(SOUTH, SOUTH_EAST, "SOUTH_SOUTHEAST", 15),
	SOUTH_SOUTH_WEST(SOUTH, SOUTH_WEST, "SOUTH_SOUTHWEST", 1),
	WEST_SOUTH_WEST(WEST, SOUTH_WEST, "WEST_SOUTHWEST", 3),
	SELF(0, 0, 0, "NONE", -1);

	private final int modX;
	private final int modY;
	private final int modZ;
	private final String spongeName;
	private final int rotationValue;

	private EnumBlockFace(int modX, int modY, int modZ, String spongeName, int rotationValue) {
		this.modX = modX;
		this.modY = modY;
		this.modZ = modZ;
		this.spongeName = spongeName;
		this.rotationValue = rotationValue;
	}

	private EnumBlockFace(EnumBlockFace face1, EnumBlockFace face2, String spongeName, int rotationValue) {
		this.modX = face1.getModX() + face2.getModX();
		this.modY = face1.getModY() + face2.getModY();
		this.modZ = face1.getModZ() + face2.getModZ();
		this.spongeName = spongeName;
		this.rotationValue = rotationValue;
	}

	public int getModX() {
		return this.modX;
	}

	public int getModY() {
		return this.modY;
	}

	public int getModZ() {
		return this.modZ;
	}

	public EnumBlockFace getOppositeFace() {
		switch (ordinal()) {
			case 1:
				return SOUTH;
			case 2:
				return WEST;
			case 3:
				return NORTH;
			case 4:
				return EAST;
			case 5:
				return DOWN;
			case 6:
				return UP;
			case 7:
				return SOUTH_WEST;
			case 8:
				return SOUTH_EAST;
			case 9:
				return NORTH_WEST;
			case 10:
				return NORTH_EAST;
			case 11:
				return EAST_SOUTH_EAST;
			case 12:
				return SOUTH_SOUTH_EAST;
			case 13:
				return SOUTH_SOUTH_WEST;
			case 14:
				return WEST_SOUTH_WEST;
			case 15:
				return WEST_NORTH_WEST;
			case 16:
				return NORTH_NORTH_WEST;
			case 17:
				return NORTH_NORTH_EAST;
			case 18:
				return EAST_NORTH_EAST;
			case 19:
				return SELF;
			default:
				return SELF;
		}
	}

	public Vector3d getRelative() {
		return new Vector3d(getModX(), getModY(), getModZ());
	}

	public String getSpongeName() {
		return spongeName;
	}

	public int getRotationValue() {
		return rotationValue;
	}

	public static Optional<EnumBlockFace> getBySpongeName(String spongeName) {
		return Arrays.stream(values()).filter(target -> target.spongeName.equalsIgnoreCase(spongeName)).findAny();
	}

	public static Optional<EnumBlockFace> getByRotationValue(int rotationValue) {
		return Arrays.stream(values()).filter(target -> target.rotationValue == rotationValue).findAny();
	}
}

package com.degoos.wetsponge.entity.modifier;

import com.degoos.wetsponge.enums.EnumDamageModifierType;
import com.degoos.wetsponge.util.Validate;

import java.util.function.DoubleUnaryOperator;

public class WSDamageModifier {

    private EnumDamageModifierType damageModifierType;
    private DoubleUnaryOperator function;

    public WSDamageModifier(EnumDamageModifierType damageModifierType, DoubleUnaryOperator function) {
        Validate.notNull(damageModifierType, "Damager modifier item cannot be null!");
        this.damageModifierType = damageModifierType;
        this.function = function == null ? t -> t : function;
    }

    public EnumDamageModifierType getDamageModifierType() {
        return damageModifierType;
    }

    public void setDamageModifierType(EnumDamageModifierType damageModifierType) {
        this.damageModifierType = damageModifierType;
    }

    public DoubleUnaryOperator getFunction() {
        return function;
    }

    public void setFunction(DoubleUnaryOperator function) {
        this.function = function == null ? t -> t : function;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WSDamageModifier that = (WSDamageModifier) o;

        return damageModifierType == that.damageModifierType;
    }

    @Override
    public int hashCode() {
        return damageModifierType.hashCode();
    }
}

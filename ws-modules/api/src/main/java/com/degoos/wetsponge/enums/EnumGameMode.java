package com.degoos.wetsponge.enums;

import java.util.Arrays;
import java.util.Optional;

public enum EnumGameMode {

    SURVIVAL(0), CREATIVE(1), ADVENTURE(2), SPECTATOR(3);

    private int value;

    EnumGameMode(int value) {
        this.value = value;
    }

    public static Optional<EnumGameMode> getByValue(int value) {
        return Arrays.stream(values()).filter(target -> target.getValue() == value).findAny();
    }

    public int getValue() {
        return value;
    }

}

package com.degoos.wetsponge.material.block;

import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSBlockTypePowerable extends WSBlockType {

	boolean isPowered();

	void setPowered(boolean powered);

	@Override
	WSBlockTypePowerable clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setBoolean("powered", isPowered());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setPowered(compound.getBoolean("powered"));
		return compound;
	}
}

package com.degoos.wetsponge.material.item.type;

import com.degoos.wetsponge.enums.item.EnumItemTypeGoldenAppleType;
import com.degoos.wetsponge.material.item.WSItemType;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSItemTypeGoldenApple extends WSItemType {

	EnumItemTypeGoldenAppleType getGoldenAppleType();

	void setGoldenAppleType(EnumItemTypeGoldenAppleType goldenAppleType);

	@Override
	WSItemTypeGoldenApple clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setString("goldenAppleType", getGoldenAppleType().name());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setGoldenAppleType(EnumItemTypeGoldenAppleType.valueOf(compound.getString("goldenAppleType")));
		return compound;
	}
}

package com.degoos.wetsponge.user;

import com.degoos.wetsponge.bridge.user.BridgeProfileProperty;

import java.util.Optional;

public interface WSProfileProperty {


	/**
	 * Creates a new property.
	 *
	 * @param name  The name for the property.
	 * @param value The value of the property.
	 * @return The new property.
	 */
	static WSProfileProperty of(String name, String value) {
		return of(name, value, null);
	}

	/**
	 * Creates a new signed property.
	 * <p>
	 * Depending on the property name, if the signature is provided it must
	 * originate from Mojang.
	 *
	 * @param name      The name for the property.
	 * @param value     The value of the property.
	 * @param signature The signature of the property.
	 * @return The new property
	 */
	static WSProfileProperty of(String name, String value, String signature) {
		return BridgeProfileProperty.of(name, value, signature);
	}

	/**
	 * Gets the name of this property.
	 *
	 * @return The name.
	 */
	String getName();

	/**
	 * Gets the value of this property.
	 *
	 * @return The value.
	 */
	String getValue();

	/**
	 * Gets the signature of this property.
	 * <p>
	 * Depending on the property name, if the signature is provided it must
	 * originate from Mojang.
	 *
	 * @return The signature, or {@link Optional#empty()} if not present..
	 */
	Optional<String> getSignature();

	/**
	 * Tests if this property has a signature.
	 *
	 * @return Whether this property has a signature.
	 */
	default boolean hasSignature() {
		return this.getSignature().isPresent();
	}

	/**
	 * Returns the handled object of the {@link WSProfileProperty}.
	 *
	 * @return the handled object.
	 */
	Object getHandled();

}

package com.degoos.wetsponge.event.block;

import com.degoos.wetsponge.block.WSBlock;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.event.WSCancellable;
import com.degoos.wetsponge.text.WSText;

import java.util.List;

public class WSSignChangeEvent extends WSBlockEvent implements WSCancellable {

    private WSPlayer player;
    private List<WSText> lines;
    private boolean cancelled;

    public WSSignChangeEvent(WSBlock block, WSPlayer player, List<WSText> lines) {
        super(block);
        this.player = player;
        this.lines = lines;
        cancelled = false;
    }


    public WSPlayer getPlayer() {
        return player;
    }


    public List<WSText> getLines() {
        return lines;
    }

    public void setLines(List<WSText> lines) {
        this.lines = lines;
    }

    public WSText getLine(int index) {
        return lines.get(index);
    }

    public void setLine(int index, WSText line) {
        lines.add(index, line);
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }
}

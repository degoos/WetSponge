package com.degoos.wetsponge.enums.block;

import java.util.Arrays;
import java.util.Optional;

public enum EnumBlockTypePottedPlant {

	POPPY(1, "rose"),
	DANDELION(2, "dandelion"),
	OAK_SAPLING(3, "oak_sapling"),
	SPRUCE_SAPLING(4, "spruce_sapling"),
	BIRCH_SAPLING(5, "birch_sapling"),
	JUNGLE_SAPLING(6, "jungle_sapling"),
	RED_MUSHROOM(7, "mushroom_red"),
	BROWN_MUSHROOM(8, "mushroom_brown"),
	CACTUS(9, "cactus"),
	DEAD_BUSH(10, "dead_bush"),
	FERN(11, "fern"),
	ACACIA_SAPLING(12, "acacia_sapling"),
	DARK_OAK_SAPLING(13, "dark_oak_sapling"),
	BLUE_ORCHID(-1, "blue_orchid"),
	ALLIUM(-1, "allium"),
	AZURE_BLUET(-1, "blue_orchid"),
	RED_TULIP(-1, "red_tulip"),
	ORANGE_TULIP(-1, "orange_tulip"),
	WHITE_TULIP(-1, "white_tulip"),
	PINK_TULIP(-1, "pink_tulip"),
	OXEYE_DAISY(-1, "oxeye_daisy"),
	NONE(0, "empty");

	private int value;
	private String oldMinecraftName;

	EnumBlockTypePottedPlant(int value, String oldMinecraftName) {
		this.value = value;
		this.oldMinecraftName = oldMinecraftName;
	}

	public int getValue() {
		return value;
	}

	public String getOldMinecraftName() {
		return oldMinecraftName;
	}

	public static Optional<EnumBlockTypePottedPlant> getByName(String name) {
		return Arrays.stream(values()).filter(target -> target.name().equalsIgnoreCase(name)).findAny();
	}

	public static Optional<EnumBlockTypePottedPlant> getByOldMinecraftName(String name) {
		return Arrays.stream(values()).filter(target -> target.getOldMinecraftName().equalsIgnoreCase(name)).findAny();
	}

	public static Optional<EnumBlockTypePottedPlant> getByValue(int value) {
		return Arrays.stream(values()).filter(target -> target.value == value).findAny();
	}
}

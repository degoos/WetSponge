package com.degoos.wetsponge.enums.item;

import java.util.Arrays;
import java.util.Optional;

public enum EnumItemTypeFishType {

	COD(0, "COD"), SALMON(1, "SALMON"), TROPICAL_FISH(2, "CLOWNFISH"), PUFFERFISH(3, "PUFFERFISH");

	private int value;
	private String spongeName;

	EnumItemTypeFishType(int value, String spongeName) {
		this.value = value;
		this.spongeName = spongeName;
	}

	public int getValue() {
		return value;
	}

	public String getSpongeName() {
		return spongeName;
	}

	public static Optional<EnumItemTypeFishType> getByValue(int value) {
		return Arrays.stream(values()).filter(target -> target.getValue() == value).findAny();
	}


	public static Optional<EnumItemTypeFishType> getByName(String name) {
		return Arrays.stream(values()).filter(target -> target.name().equalsIgnoreCase(name)).findAny();
	}

	public static Optional<EnumItemTypeFishType> getBySpongeName(String name) {
		return Arrays.stream(values()).filter(target -> target.getSpongeName().equalsIgnoreCase(name)).findAny();
	}
}

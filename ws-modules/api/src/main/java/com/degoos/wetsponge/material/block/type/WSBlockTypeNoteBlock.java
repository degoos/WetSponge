package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.EnumInstrument;
import com.degoos.wetsponge.material.block.WSBlockTypePowerable;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSBlockTypeNoteBlock extends WSBlockTypePowerable {

	EnumInstrument getInstrument();

	void setInstrument(EnumInstrument instrument);

	int getNote();

	void setNote(int note);

	@Override
	WSBlockTypeNoteBlock clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setBoolean("powered", isPowered());
		compound.setString("instrument", getInstrument().name());
		compound.setInteger("note", getNote());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setPowered(compound.getBoolean("powered"));
		setInstrument(EnumInstrument.valueOf(compound.getString("instrument")));
		setNote(compound.getInteger("note"));
		return compound;
	}
}

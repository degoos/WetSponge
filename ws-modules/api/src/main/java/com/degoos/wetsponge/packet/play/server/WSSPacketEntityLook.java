package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.bridge.packet.BridgeServerPacket;
import com.degoos.wetsponge.entity.WSEntity;
import com.flowpowered.math.vector.Vector2i;

public interface WSSPacketEntityLook extends WSSPacketEntity {

	public static WSSPacketEntityLook of(WSEntity entity, Vector2i rotation, boolean onGround) {
		return BridgeServerPacket.newWSSPacketEntityLook(entity, rotation, onGround);
	}

	public static WSSPacketEntityLook of(int entity, Vector2i rotation, boolean onGround) {
		return BridgeServerPacket.newWSSPacketEntityLook(entity, rotation, onGround);
	}

}

package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.WSBlockTypeDirectional;
import com.degoos.wetsponge.material.block.WSBlockTypeLightable;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSBlockTypeFurnace extends WSBlockTypeDirectional, WSBlockTypeLightable {

	@Override
	WSBlockTypeFurnace clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setString("facing", getFacing().name());
		compound.setBoolean("lit", isLit());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setFacing(EnumBlockFace.valueOf(compound.getString("facing")));
		setLit(compound.getBoolean("lit"));
		return compound;
	}
}

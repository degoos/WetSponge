package com.degoos.wetsponge.enums.block;


import java.util.Arrays;
import java.util.Optional;

public enum EnumBlockTypeFlowerType {

	DANDELION(0), POPPY(0), BLUE_ORCHID(1), ALLIUM(2), HOUSTONIA(3), RED_TULIP(4), ORANGE_TULIP(5), WHITE_TULIP(6), PINK_TULIP(7), OXEYE_DAISY(8);

	private int value;


	EnumBlockTypeFlowerType(int value) {
		this.value = value;
	}


	public static Optional<EnumBlockTypeFlowerType> getByValue(int value) {
		return Arrays.stream(values()).filter(flowerType -> flowerType.getValue() == value).findAny();
	}


	public static Optional<EnumBlockTypeFlowerType> getByName(String name) {
		return Arrays.stream(values()).filter(flowerType -> flowerType.name().equalsIgnoreCase(name)).findAny();
	}

	public int getValue() {
		return value;
	}
}

package com.degoos.wetsponge.enums;

public enum EnumParrotVariant {

	RED,
	BLUE,
	GREEN,
	CYAN,
	GRAY

}

package com.degoos.wetsponge.enums.block;

import java.util.Arrays;
import java.util.Optional;

public enum EnumBlockTypeStructureBlockMode {

	SAVE(2), LOAD(3), CORNER(4), DATA(1);

	private int value;

	EnumBlockTypeStructureBlockMode(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public static Optional<EnumBlockTypeStructureBlockMode> getByValue(int value) {
		return Arrays.stream(values()).filter(target -> target.value == value).findAny();
	}
}

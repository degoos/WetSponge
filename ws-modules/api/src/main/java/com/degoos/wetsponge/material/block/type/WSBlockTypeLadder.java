package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.WSBlockTypeDirectional;
import com.degoos.wetsponge.material.block.WSBlockTypeWaterlogged;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSBlockTypeLadder extends WSBlockTypeDirectional, WSBlockTypeWaterlogged {

	@Override
	WSBlockTypeLadder clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setString("facing", getFacing().name());
		compound.setBoolean("waterlogged", isWaterlogged());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setFacing(EnumBlockFace.valueOf(compound.getString("facing")));
		setWaterlogged(compound.getBoolean("waterlogged"));
		return compound;
	}
}

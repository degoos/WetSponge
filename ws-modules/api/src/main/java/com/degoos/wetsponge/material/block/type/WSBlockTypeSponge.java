package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.material.block.WSBlockType;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSBlockTypeSponge extends WSBlockType {

	boolean isWet();

	void setWet(boolean wet);

	@Override
	WSBlockTypeSponge clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setBoolean("wet", isWet());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setWet(compound.getBoolean("wet"));
		return compound;
	}
}

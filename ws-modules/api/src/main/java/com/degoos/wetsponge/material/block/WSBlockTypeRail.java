package com.degoos.wetsponge.material.block;

import com.degoos.wetsponge.enums.block.EnumBlockTypeRailShape;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

import java.util.Set;

public interface WSBlockTypeRail extends WSBlockType {

	EnumBlockTypeRailShape getShape();

	void setShape(EnumBlockTypeRailShape shape);

	Set<EnumBlockTypeRailShape> allowedShapes();

	@Override
	WSBlockTypeRail clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setString("shape", getShape().name());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setShape(EnumBlockTypeRailShape.valueOf(compound.getString("shape")));
		return compound;
	}
}

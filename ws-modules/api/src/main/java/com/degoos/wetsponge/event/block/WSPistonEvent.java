package com.degoos.wetsponge.event.block;

import com.degoos.wetsponge.block.WSBlock;
import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.event.WSCancellable;

import java.util.Set;

public class WSPistonEvent extends WSBlockEvent implements WSCancellable {

	private boolean cancelled;
	private Set<WSBlock> blocks;
	private EnumBlockFace direction;

	public WSPistonEvent(WSBlock block, Set<WSBlock> blocks, EnumBlockFace direction) {
		super(block);
		this.blocks = blocks;
		this.direction = direction;
		this.cancelled = false;
	}

	public Set<WSBlock> getBlocks() {
		return blocks;
	}

	public EnumBlockFace getDirection() {
		return direction;
	}

	@Override
	public boolean isCancelled() {
		return cancelled;
	}

	@Override
	public void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
	}
}

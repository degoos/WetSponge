package com.degoos.wetsponge.bridge.packet;

import com.degoos.wetsponge.enums.EnumEntityAction;
import com.degoos.wetsponge.packet.play.client.WSCPacketCloseWindows;
import com.degoos.wetsponge.packet.play.client.WSCPacketEntityAction;
import com.degoos.wetsponge.packet.play.client.WSCPacketUpdateSign;
import com.flowpowered.math.vector.Vector3d;

public class BridgeClientPacket {

	public static WSCPacketCloseWindows newWSCPacketCloseWindows(int windowsId) {
		/*switch (WetSponge.getServerType()) {
			case SPIGOT:  		case PAPER_SPIGOT:
				try {
					return new SpigotCPacketCloseWindows(windowsId);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeCPacketCloseWindows(windowsId);
			default:
				return null;
		}*/
		return null;
	}

	public static WSCPacketEntityAction newWSCPacketEntityAction(int entityId, EnumEntityAction entityAction, int jumpBoost) {
		/*switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					return new SpigotCPacketEntityAction(entityId, entityAction, jumpBoost);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeCPacketEntityAction(entityId, entityAction, jumpBoost);
			default:
				return null;
		}*/
		return null;
	}

	public static WSCPacketUpdateSign newWSCPacketUpdateSign(Vector3d position, String[] lines) {
		/*switch (WetSponge.getServerType()) {
			case SPIGOT:  		case PAPER_SPIGOT:
				try {
					return new SpigotCPacketUpdateSign(position, lines);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeCPacketUpdateSign(position, lines);
			default:
				return null;
		}*/
		return null;
	}
}

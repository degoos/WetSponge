package com.degoos.wetsponge.command.wetspongecommand.timings;

import com.degoos.wetsponge.command.WSCommandSource;
import com.degoos.wetsponge.command.ramified.WSRamifiedCommand;
import com.degoos.wetsponge.command.ramified.WSSubcommand;

import java.util.ArrayList;
import java.util.List;

public class WetSpongeSubcommandTimingsVerbOn extends WSSubcommand {


	public WetSpongeSubcommandTimingsVerbOn(WSRamifiedCommand command) {
		super("verbOn", command);
	}

	@Override
	public void executeCommand(WSCommandSource source, String command, String[] arguments, String[] remainingArguments) {
		/*if (!Timings.isTimingsEnabled()) {
			source.sendMessage(WetSpongeMessages.getMessage("command.timings.notEnabled").orElse(WSText.empty()));
			return;
		}
		Timings.setVerboseTimingsEnabled(true);
		source.sendMessage(WetSpongeMessages.getMessage("command.timings.enableVerbose").orElse(WSText.empty()));*/
	}

	@Override
	public List<String> sendTab(WSCommandSource source, String command, String[] arguments, String[] remainingArguments) {
		return new ArrayList<>();
	}
}

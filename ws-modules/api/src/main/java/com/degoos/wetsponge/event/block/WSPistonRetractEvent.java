package com.degoos.wetsponge.event.block;

import com.degoos.wetsponge.block.WSBlock;
import com.degoos.wetsponge.enums.block.EnumBlockFace;

import java.util.Set;

public class WSPistonRetractEvent extends WSPistonEvent {

	public WSPistonRetractEvent(WSBlock block, Set<WSBlock> blocks, EnumBlockFace direction) {
		super(block, blocks, direction);
	}
}

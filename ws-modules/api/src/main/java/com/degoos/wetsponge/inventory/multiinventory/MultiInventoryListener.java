package com.degoos.wetsponge.inventory.multiinventory;


import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.event.entity.player.connection.WSPlayerQuitEvent;
import com.degoos.wetsponge.event.inventory.WSInventoryClickEvent;
import com.degoos.wetsponge.inventory.WSInventory;
import com.degoos.wetsponge.sound.WSSound;
import com.degoos.wetsponge.util.InternalLogger;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

public class MultiInventoryListener {

	public static Map<WSPlayer, MultiInvEntry> players;
	public static Set<WSPlayer> change;


	public static void load() {
		players = new HashMap<>();
		change = new HashSet<>();
	}


	public static void closeInv(WSPlayer player) {
		if (change.contains(player)) {
			change.remove(player);
			return;
		}
		if (!players.containsKey(player)) return;
		MultiInventoryCloseEvent event = new MultiInventoryCloseEvent(players.get(player).getMultiInventory(), player);
		players.remove(player);
		try {
			event.getMultiInventory().onClose(event);
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was calling the event MultiInventoryCloseEvent!");
		}
		WetSponge.getEventManager().callEvent(event);
	}


	public static void leave(WSPlayerQuitEvent event) {
		WSPlayer pl = event.getPlayer();
		if (!players.containsKey(pl)) return;
		WetSponge.getEventManager().callEvent(new MultiInventoryCloseEvent(players.get(pl).getMultiInventory(), pl));
		players.remove(pl);
	}


	public static void clickInv(WSInventoryClickEvent e) {
		try {
			WSPlayer pl = e.getPlayer();

			if (!players.containsKey(pl)) return;

			MultiInvEntry ent = players.get(pl);

			if (e.getClickedInventory() == null) return;

			Optional<WSInventory> optional = ent.getMultiInventory().getInventory(ent.getInventory());
			if (!optional.isPresent()) return;

			if (e.getClickedInventory().equals(pl.getInventory())) {
				MultiInventoryClickPlayerInvEvent event = new MultiInventoryClickPlayerInvEvent(e.getClickedSlot(), pl.getInventory(), ent.getMultiInventory(), pl, e);
				try {
					ent.getMultiInventory().onPlayerInventoryClick(event);
				} catch (Throwable ex) {
					InternalLogger.printException(ex, "An error has occurred while WetSponge was calling the event MultiInventoryClickPlayerInvEvent!");
				}
				if (!event.isCancelled()) WetSponge.getEventManager().callEvent(event);
				if (event.isCancelled() || ent.getMultiInventory() instanceof BooleanMultiInventory) e.setCancelled(true);
				return;
			}

			if (ent.getMultiInventory() instanceof BooleanMultiInventory) {
				int slot = e.getClickedSlot().getSlot();
				e.setCancelled(true);
				if (slot == 11) ((BooleanMultiInventory) ent.getMultiInventory()).onYes();
				if (slot == 15) ((BooleanMultiInventory) ent.getMultiInventory()).onNo();
				return;
			}

			if (e.getClickedSlot().getSlot() >= ent.getMultiInventory().getRows().getSlots()) {
				e.setCancelled(true);
				MultiInventoryClickHotbarEvent event = new MultiInventoryClickHotbarEvent(e.getClickedSlot(), ent.getInventory(), e.getClickedInventory(), ent
					.getMultiInventory(), pl, e);
				try {
					ent.getMultiInventory().onHotbarClick(event);
				} catch (Throwable ex) {
					InternalLogger.printException(ex, "An error has occurred while WetSponge was calling the event MultiInventoryClickHotbarEvent!");
				}
				if (!event.isCancelled()) WetSponge.getEventManager().callEvent(event);
				if (event.isCancelled() || ent.getMultiInventory().getInventories().size() == 1) return;

				int slot = e.getClickedSlot().getSlot();

				if (ent.getMultiInventory() instanceof PlayerMultiInventory) {
					int i = ent.getMultiInventory().getRows().getSlots();
					if (slot == i + 2) {
						((PlayerMultiInventory) ent.getMultiInventory()).openFirst();
						pl.playSound(WSSound.BLOCK_STONE_BUTTON_CLICK_ON, 1);
					} else if (slot == i + 3) {
						((PlayerMultiInventory) ent.getMultiInventory()).openPrevious();
						pl.playSound(WSSound.BLOCK_STONE_BUTTON_CLICK_ON, 1);
					} else if (slot == i + 5) {
						((PlayerMultiInventory) ent.getMultiInventory()).openNext();
						pl.playSound(WSSound.BLOCK_STONE_BUTTON_CLICK_ON, 1);
					} else if (slot == i + 6) {
						((PlayerMultiInventory) ent.getMultiInventory()).openLast();
						pl.playSound(WSSound.BLOCK_STONE_BUTTON_CLICK_ON, 1);
					}
				}
				if (ent.getMultiInventory() instanceof GlobalMultiInventory) {
					int i = ent.getMultiInventory().getRows().getSlots();
					if (slot == i + 2) {
						((GlobalMultiInventory) ent.getMultiInventory()).openFirst(pl);
						pl.playSound(WSSound.BLOCK_STONE_BUTTON_CLICK_ON, 1);
					} else if (slot == i + 3) {
						((GlobalMultiInventory) ent.getMultiInventory()).openPrevious(pl);
						pl.playSound(WSSound.BLOCK_STONE_BUTTON_CLICK_ON, 1);
					} else if (slot == i + 5) {
						((GlobalMultiInventory) ent.getMultiInventory()).openNext(pl);
						pl.playSound(WSSound.BLOCK_STONE_BUTTON_CLICK_ON, 1);
					} else if (slot == i + 6) {
						((GlobalMultiInventory) ent.getMultiInventory()).openLast(pl);
						pl.playSound(WSSound.BLOCK_STONE_BUTTON_CLICK_ON, 1);
					}
				}
			} else {
				int slot = e.getClickedSlot().getSlot() + ent.getInventory() * ent.getMultiInventory().getRows().getSlots();
				if (ent.getMultiInventory() instanceof ModifiedMultiInventory) slot = ((ModifiedMultiInventory) ent.getMultiInventory()).getSlot(slot).orElse(-1);
				if (slot == -1) {
					e.setCancelled(true);
					return;
				}
				MultiInventoryClickEvent event = new MultiInventoryClickEvent(e.getClickedSlot(), slot, ent.getInventory(), e.getClickedInventory(), ent
					.getMultiInventory(), pl, e);
				try {
					ent.getMultiInventory().onClick(event);
				} catch (Throwable ex) {
					InternalLogger.printException(ex, "An error has occurred while WetSponge was calling the event MultiInventoryClickEvent!");
				}
				if (!event.isCancelled()) WetSponge.getEventManager().callEvent(event);
				if (event.isCancelled()) e.setCancelled(true);
			}
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was executing the MultiInventoryListener!");
		}
	}
}

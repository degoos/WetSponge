package com.degoos.wetsponge.task;


import com.degoos.wetsponge.bridge.task.BridgeTask;
import com.degoos.wetsponge.plugin.WSPlugin;

import java.util.UUID;
import java.util.function.Consumer;

/**
 * The WSTask represents a {@link com.degoos.wetsponge.enums.EnumServerType#SPIGOT} or a {@link com.degoos.wetsponge.enums.EnumServerType#SPONGE} task.
 * Tasks depends of a {@link WSPlugin plugin} to be executed. If a {@link WSPlugin plugin} is unloaded, all its {@link WSTask tasks} will be stopped.
 */
public interface WSTask {

	/**
	 * Creates a new {@link WSTask}.
	 *
	 * @param runnable the {@link Runnable runnable} to run.
	 * @return a new {@link WSTask}.
	 */
	public static WSTask of(Runnable runnable) {
		return BridgeTask.of(runnable);
	}

	/**
	 * Creates a new {@link WSTask}.
	 *
	 * @param consumer the {@link Consumer} to run.
	 * @return a new {@link WSTask}.
	 */
	public static WSTask of(Consumer<WSTask> consumer) {
		return BridgeTask.of(consumer);
	}

	/**
	 * Runs the {@link WSTask task}.
	 *
	 * @param plugin the owner {@link WSPlugin plugin}.
	 */
	void run(WSPlugin plugin);

	/**
	 * Runs the {@link WSTask task} asynchronously.
	 *
	 * @param plugin the owner {@link WSPlugin plugin}.
	 */
	void runAsynchronously(WSPlugin plugin);

	/**
	 * Runs the {@link WSTask task} after an amount of time.
	 *
	 * @param delay  the amount of time in ticks. ( 20 ticks = 1 second).
	 * @param plugin the owner {@link WSPlugin plugin}.
	 */
	void runTaskLater(long delay, WSPlugin plugin);

	/**
	 * Runs the {@link WSTask task} asynchronously after an amount of time.
	 *
	 * @param delay  the amount of time in ticks. ( 20 ticks = 1 second).
	 * @param plugin the owner {@link WSPlugin plugin}.
	 */
	void runTaskLaterAsynchronously(long delay, WSPlugin plugin);

	/**
	 * Runs the {@link WSTask task} after a delay.
	 * After this the {@link WSTask task} will be execute every interval of time.
	 *
	 * @param delay    the delay of time in ticks. ( 20 ticks = 1 second).
	 * @param interval the interval of time in ticks (20 ticks = 1 second).
	 * @param plugin   the owner {@link WSPlugin plugin}.
	 */
	void runTaskTimer(long delay, long interval, WSPlugin plugin);

	/**
	 * Runs the {@link WSTask task} after a delay.
	 * After this the {@link WSTask task} will be execute every interval of time.
	 *
	 * @param delay    the delay of time in ticks. ( 20 ticks = 1 second).
	 * @param interval the interval of time in ticks (20 ticks = 1 second).
	 * @param times    the amount of times the {@link WSTask task} will be executed
	 * @param plugin   the owner {@link WSPlugin plugin}.
	 */
	void runTaskTimer(long delay, long interval, long times, WSPlugin plugin);

	/**
	 * Runs the {@link WSTask task} asynchronously after a delay.
	 * After this the {@link WSTask task} will be execute asynchronously every interval of time.
	 *
	 * @param delay    the delay of time in ticks. ( 20 ticks = 1 second).
	 * @param interval the interval of time in ticks (20 ticks = 1 second).
	 * @param plugin   the owner {@link WSPlugin plugin}.
	 */
	void runTaskTimerAsynchronously(long delay, long interval, WSPlugin plugin);

	/**
	 * Runs the {@link WSTask task} asynchronously after a delay.
	 * After this the {@link WSTask task} will be execute asynchronously every interval of time.
	 *
	 * @param delay    the delay of time in ticks. ( 20 ticks = 1 second).
	 * @param interval the interval of time in ticks (20 ticks = 1 second).
	 * @param times    the amount of times the {@link WSTask task} will be executed
	 * @param plugin   the owner {@link WSPlugin plugin}.
	 */
	void runTaskTimerAsynchronously(long delay, long interval, long times, WSPlugin plugin);

	/**
	 * Returns the {@link UUID unique id} of the {@link WSTask task}.
	 * It can be null if the {@link WSTask task} is not being executed.
	 *
	 * @return the {@link UUID unique id}.
	 */
	UUID getUniqueId();

	/**
	 * Returns the {@link StackTraceElement stack trace element} with
	 * the information of the line that creates the {@link WSTask task}.
	 *
	 * @return the {@link StackTraceElement stack trace element}.
	 */
	StackTraceElement getCallerStackTraceElement();

	/**
	 * Returns the amount of times the {@link WSTask task} was executed.
	 *
	 * @return the amount of times the {@link WSTask task} was executed.
	 */
	long getTimesExecuted();

	/**
	 * @return whether the task is asynchronously.
	 */
	boolean isAsynchronous();

	/**
	 * @return whether the task was executed using runTaskTimer.
	 */
	boolean isTimer();

	/**
	 * @return whether the task was executed using run.
	 */
	boolean isInstantaneous();

	/**
	 * @return whether the task was executed using runTaskLater.
	 */
	boolean isLater();

	/**
	 * @return the delay of the task if it is a later or timer task.
	 */
	long getDelay();

	/**
	 * @return the interval of the task if it is a timer task.
	 */
	long getInterval();

	/**
	 * @return the amount of times the task will be executed or -1.
	 */
	long getTimesToExecute();

	/**
	 * Returns the {@link WSPlugin plugin} which owns this {@link WSTask task}.
	 *
	 * @return the {@link WSPlugin plugin}.
	 */
	WSPlugin getPlugin();

	/**
	 * Cancels the {@link WSTask task}.
	 */
	void cancel();

	/*/**
	 * Returns the timings handled.
	 *
	 * @return the timings handled.
	public Timing getTimingsHandler();*/
}

package com.degoos.wetsponge.material.item.type;

import com.degoos.wetsponge.enums.item.EnumItemTypeCoalType;
import com.degoos.wetsponge.material.item.WSItemType;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSItemTypeCoal extends WSItemType {

	EnumItemTypeCoalType getCoalType();

	void setCoalType(EnumItemTypeCoalType coalType);

	@Override
	WSItemTypeCoal clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setString("coalType", getCoalType().name());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setCoalType(EnumItemTypeCoalType.valueOf(compound.getString("coalType")));
		return compound;
	}
}

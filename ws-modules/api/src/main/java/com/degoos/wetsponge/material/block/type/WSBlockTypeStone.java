package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockTypeStoneType;
import com.degoos.wetsponge.material.block.WSBlockType;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSBlockTypeStone extends WSBlockType {

	EnumBlockTypeStoneType getStoneType();

	void setStoneType(EnumBlockTypeStoneType stoneType);

	@Override
	WSBlockTypeStone clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setString("stoneType", getStoneType().name());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setStoneType(EnumBlockTypeStoneType.valueOf(compound.getString("stoneType")));
		return compound;
	}
}

package com.degoos.wetsponge.resource;


import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.text.WSText;

import java.net.InetSocketAddress;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

public interface WSBungeeCord {

	/**
	 * Connects a WSPlayer on this server to another server.
	 *
	 * @param player The WSPlayer to connect.
	 * @param server The server to send them to.
	 * @throws IllegalStateException if the server is starting or stopping.
	 */
	void connectWSPlayer(WSPlayer player, String server);

	/**
	 * Connects a WSPlayer from any server to any other server.
	 *
	 * @param player The WSPlayer to connect.
	 * @param server The server to send them to.
	 * @throws IllegalStateException if the server is starting or stopping.
	 */
	void connectWSPlayer(String player, String server);

	/**
	 * Gets the real {@link InetSocketAddress} of a WSPlayer.
	 * <p>
	 * Do not call the returned {@link CompletableFuture} immediately, or
	 * the entire server will hang.
	 *
	 * @param player The WSPlayer to get the address of.
	 * @return A future which returns the address.
	 * @throws IllegalStateException if the server is starting or stopping.
	 */
	CompletableFuture<InetSocketAddress> getWSPlayerIP(WSPlayer player);

	/**
	 * Gets the number of WSPlayers on a given server.
	 * <p>
	 * Do not call the returned {@link CompletableFuture} immediately, or
	 * the entire server will hang.
	 *
	 * @param server The server to query.
	 * @return A future which returns the count.
	 * @throws IllegalStateException if the server is starting or stopping.
	 */
	CompletableFuture<Integer> getWSPlayerCount(String server);

	/**
	 * Gets the number of WSPlayers on all servers.
	 * <p>
	 * Do not call the returned {@link CompletableFuture} immediately, or
	 * the entire server will hang.
	 *
	 * @return A future which returns the count.
	 * @throws IllegalStateException if the server is starting or stopping.
	 */
	CompletableFuture<Integer> getGlobalWSPlayerCount();

	/**
	 * Gets all online WSPlayers on a specific server.
	 * <p>
	 * Do not call the returned {@link CompletableFuture} immediately, or
	 * the entire server will hang.
	 *
	 * @param server The server to query.
	 * @return A future which returns the WSPlayers.
	 * @throws IllegalStateException if the server is starting or stopping.
	 */
	CompletableFuture<List<String>> getOnlineWSPlayers(String server);

	/**
	 * Gets a list of every single WSPlayer online.
	 * <p>
	 * Do not call the returned {@link CompletableFuture} immediately, or
	 * the entire server will hang.
	 *
	 * @return A future which returns the WSPlayers.
	 * @throws IllegalStateException if the server is starting or stopping.
	 */
	CompletableFuture<List<String>> getAllOnlineWSPlayers();

	/**
	 * Gets a list of all servers on the network.
	 * <p>
	 * Do not call the returned {@link CompletableFuture} immediately, or
	 * the entire server will hang.
	 *
	 * @return A future which returns the servers.
	 * @throws IllegalStateException if the server is starting or stopping.
	 */
	CompletableFuture<List<String>> getServerList();

	/**
	 * Sends a message to a WSPlayer elsewhere on the network.
	 *
	 * @param player  The WSPlayer to send the message to.
	 * @param message The message to send.
	 * @throws IllegalStateException if the server is starting or stopping.
	 */
	public void sendMessage(String player, WSText message);

	/**
	 * Gets the the Bungee-defined name of this server.
	 * <p>
	 * Do not call the returned {@link CompletableFuture} immediately, or
	 * the entire server will hang.
	 *
	 * @return A future which returns the name.
	 * @throws IllegalStateException if the server is starting or stopping.
	 */
	CompletableFuture<String> getServerName();

	/**
	 * Sends a plugin message to another server.
	 *
	 * @param payload A consumer to write the data to.
	 * @param channel The channel to send on.
	 * @param server  The server to send to.
	 * @throws IllegalStateException if the server is starting or stopping.
	 */
	void sendServerPluginMessage(byte[] payload, String channel, String server);

	/**
	 * Sends a plugin message to all servers on the network.
	 *
	 * @param payload A consumer to write the data to.
	 * @param channel The channel to send on.
	 * @throws IllegalStateException if the server is starting or stopping.
	 */
	void sendGlobalPluginMessage(byte[] payload, String channel);

	/**
	 * Sends a plugin message to another WSPlayer on the network.
	 *
	 * @param payload A consumer to write the data to.
	 * @param channel The channel to send on.
	 * @param player  The WSPlayer to send to.
	 * @throws IllegalStateException if the server is starting or stopping.
	 */
	void sendWSPlayerPluginMessage(byte[] payload, String channel, String player);

	/**
	 * Gets the real {@link UUID} of a WSPlayer on this server.
	 * <p>
	 * Do not call the returned {@link CompletableFuture} immediately, or
	 * the entire server will hang.
	 *
	 * @param WSPlayer The WSPlayer to query.
	 * @return A future which returns the {@link UUID}.
	 * @throws IllegalStateException if the server is starting or stopping.
	 */
	CompletableFuture<UUID> getRealUUID(WSPlayer WSPlayer);

	/**
	 * Gets the real {@link UUID} of a WSPlayer on the network.
	 * <p>
	 * Do not call the returned {@link CompletableFuture} immediately, or
	 * the entire server will hang.
	 *
	 * @param player The WSPlayer to query
	 * @return A future which returns the {@link UUID}
	 * @throws IllegalStateException if the server is starting or stopping.
	 */
	CompletableFuture<UUID> getRealUUID(String player);

	/**
	 * Gets the {@link InetSocketAddress} of a server on the network.
	 * <p>
	 * Do not call the returned {@link CompletableFuture} immediately, or
	 * the entire server will hang.
	 *
	 * @param server The server to query.
	 * @return A future which returns the address.
	 * @throws IllegalStateException if the server is starting or stopping.
	 */
	CompletableFuture<InetSocketAddress> getServerIP(String server);

	/**
	 * Kicks a WSPlayer from whatever server they are on.
	 *
	 * @param player The WSPlayer to kick.
	 * @param reason Why they're being kicked.
	 * @throws IllegalStateException if the server is starting or stopping.
	 */
	void kickWSPlayer(String player, WSText reason);

	/**
	 * This API operates via plugin message packets, and therefore requires
	 * there to be at least one WSPlayer on the server.
	 */
	class NoWSPlayerOnlineException extends IllegalStateException {
	}

}

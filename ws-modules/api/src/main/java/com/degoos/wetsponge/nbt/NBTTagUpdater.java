package com.degoos.wetsponge.nbt;

public class NBTTagUpdater {


	public static String update(String toUpdate) {
		if (!toUpdate.contains("ench:")) return toUpdate;
		boolean done = false;
		StringBuilder newString = new StringBuilder();
		for (int i = 0; i < toUpdate.length(); i++) {
			char c = toUpdate.charAt(i);
			if (c == 'e' && !done) {
				if (toUpdate.length() <= i + 5) {
					done = true;
					continue;
				}
				if (toUpdate.charAt(i + 1) == 'n' && toUpdate.charAt(i + 2) == 'c' && toUpdate.charAt(i + 3) == 'h' && toUpdate.charAt(i + 4) == ':' &&
					toUpdate.charAt(i + 5) == '[') {
					newString.append("ench:[");
					i += 6;
					boolean ignore = true;
					while (toUpdate.charAt(i) != ']' && toUpdate.length() > i) {
						c = toUpdate.charAt(i);
						i++;
						if (c == '}') {
							ignore = true;
							newString.append(c);
							continue;
						}
						if (c == '{') ignore = false;
						if (!ignore || c == ',') newString.append(c);
					}
					newString.append(']');
					done = true;
				} else newString.append(c);
			} else newString.append(c);
		}
		return newString.toString();
	}

}

package com.degoos.wetsponge.material.item.type;

import com.degoos.wetsponge.color.WSColor;
import com.degoos.wetsponge.material.item.WSItemType;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSItemTypeMap extends WSItemType {

	int getMapId();

	void setMapId(int mapId);

	WSColor getMapColor();

	void setMapColor(WSColor mapColor);

	@Override
	WSItemTypeMap clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setInteger("mapId", getMapId());
		WSColor color = getMapColor();
		if (color == null) return compound;
		compound.setInteger("color", color.toRGB());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setMapId(compound.getInteger("mapId"));
		if (!compound.hasKey("color")) {
			setMapColor(null);
			return compound;
		}
		setMapColor(WSColor.ofRGB(compound.getInteger("color")));
		return compound;
	}
}

package com.degoos.wetsponge.map;

import java.util.HashMap;

public class WSMapFont {

	private final HashMap<Character, WSMapCharacterSprite> chars = new HashMap<>();
	private int height = 0;
	protected boolean malleable = true;

	public void setChar(char ch, WSMapCharacterSprite sprite) {
		if (!this.malleable) {
			throw new IllegalStateException("this font is not malleable");
		} else {
			this.chars.put(ch, sprite);
			if (sprite.getHeight() > this.height) {
				this.height = sprite.getHeight();
			}

		}
	}

	public WSMapCharacterSprite getChar(char ch) {
		return this.chars.get(ch);
	}

	public int getWidth(String text) {
		if (!this.isValid(text)) {
			throw new IllegalArgumentException("text contains invalid characters");
		} else if (text.length() == 0) {
			return 0;
		} else {
			int result = 0;

			for (int i = 0; i < text.length(); ++i) {
				char ch = text.charAt(i);
				if (ch != 167) {
					result += this.chars.get(ch).getWidth();
				}
			}

			result += text.length() - 1;
			return result;
		}
	}

	public int getHeight() {
		return this.height;
	}

	public boolean isValid(String text) {
		for (int i = 0; i < text.length(); ++i) {
			char ch = text.charAt(i);
			if (ch != 167 && ch != '\n' && this.chars.get(ch) == null) {
				return false;
			}
		}

		return true;
	}

}

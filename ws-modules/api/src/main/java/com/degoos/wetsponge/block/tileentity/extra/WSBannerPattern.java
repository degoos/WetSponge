package com.degoos.wetsponge.block.tileentity.extra;

import com.degoos.wetsponge.enums.EnumDyeColor;
import com.degoos.wetsponge.enums.block.EnumBannerPatternShape;

public class WSBannerPattern {

	private EnumBannerPatternShape shape;
	private EnumDyeColor color;

	public WSBannerPattern(EnumBannerPatternShape shape, EnumDyeColor color) {
		this.shape = shape;
		this.color = color;
	}

	public EnumBannerPatternShape getShape() {
		return shape;
	}

	public void setShape(EnumBannerPatternShape shape) {
		this.shape = shape;
	}

	public EnumDyeColor getColor() {
		return color;
	}

	public void setColor(EnumDyeColor color) {
		this.color = color;
	}

	public WSBannerPattern clone() {
		return new WSBannerPattern(shape, color);
	}
}

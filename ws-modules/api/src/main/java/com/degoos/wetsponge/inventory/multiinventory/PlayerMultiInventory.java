package com.degoos.wetsponge.inventory.multiinventory;

import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.enums.EnumTextColor;
import com.degoos.wetsponge.enums.EnumTextStyle;
import com.degoos.wetsponge.inventory.WSInventory;
import com.degoos.wetsponge.item.WSItemStack;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.util.HeadDatabase;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

public class PlayerMultiInventory implements MultiInventory {

	private String id;
	private WSText name;
	private InventoryRows rows;
	protected Map<Integer, WSInventory> inventories;
	private WSPlayer player;
	private boolean hotBar;


	public PlayerMultiInventory(WSText name, InventoryRows rows, int invAmount, WSPlayer player, String id) {
		this(name, rows, invAmount, player, id, false);
	}


	public PlayerMultiInventory(WSText name, InventoryRows rows, int invAmount, WSPlayer player, String id, boolean hotBarIfUnique) {
		if (invAmount < 1) throw new IllegalArgumentException("invAmount can't be less than 0!");
		this.player = player;
		this.name = name;
		this.id = id;
		this.rows = rows;
		inventories = new HashMap<>();
		this.hotBar = hotBarIfUnique;
		for (int i = 0; i < invAmount; i++)
			inventories.put(i, createInv(invAmount, hotBarIfUnique));
	}


	public PlayerMultiInventory(WSText name, int slots, InventoryRows rows, WSPlayer player, String id) {
		this(name, slots, rows, player, id, false);
	}


	public PlayerMultiInventory(WSText name, int slots, InventoryRows rows, WSPlayer player, String id, boolean hotBarIfUnique) {
		this(name, slots, rows, player, id, hotBarIfUnique, rows.getSlots());
	}

	public PlayerMultiInventory(WSText name, int slots, InventoryRows rows, WSPlayer player, String id, boolean hotBarIfUnique, int slotsPerInventory) {
		if (slots < 1) throw new IllegalArgumentException("slots can't be less than 0!");
		this.player = player;
		this.name = name;
		this.rows = rows;
		this.id = id;
		this.hotBar = hotBarIfUnique;
		inventories = new HashMap<>();
		int invAmount = (int) Math.ceil((double) slots / (double) slotsPerInventory);
		for (int i = 0; i < invAmount; i++)
			inventories.put(i, createInv(invAmount, hotBarIfUnique));
	}


	public void setItem(int slot, WSItemStack item) {
		if (inventories.size() == 1) {
			getInventory(0).ifPresent(inventory -> inventory.setItem(item, slot));
			return;
		}
		int inventory = Math.floorDiv(slot, rows.getSlots());
		int invSlot = slot - inventory * rows.getSlots();
		setItem(inventory, invSlot, item);
	}

	public void setItem(int inventory, int invSlot, WSItemStack item) {
		if (inventories.size() == 1) {
			getInventory(0).ifPresent(inv -> inv.setItem(item, invSlot));
			return;
		}
		if (!inventories.containsKey(inventory) || invSlot >= rows.getSlots()) return;
		inventories.get(inventory).setItem(item, invSlot);
	}


	@Override
	public void setItemOnHotbar(int slot, WSItemStack itemStack) {
		if (inventories.size() > 1 && (slot == 2 || slot == 3 || slot == 5 || slot == 6)) return;
		if (inventories.size() == 1 && !hotBar) return;
		if (slot < 0 || slot > 8) return;
		int realSlot = rows.getSlots() + slot;
		inventories.values().forEach(inventory -> inventory.setItem(itemStack, realSlot));
	}


	public Set<WSItemStack> getContents() {
		Set<WSItemStack> items = new HashSet<>();
		for (int i = 0; i < size(); i++) getItem(i).ifPresent(items::add);
		return items;
	}


	public Map<Integer, WSItemStack> getContentsMap() {
		Map<Integer, WSItemStack> map = new HashMap<>();
		for (int i = 0; i < size(); i++) {
			Optional<WSItemStack> itemStack = getItem(i);
			if (itemStack.isPresent()) map.put(i, itemStack.get());
		}
		return map;
	}


	public Map<Integer, WSInventory> getInventories() {
		return new HashMap<>(inventories);
	}


	public Optional<WSInventory> getInventory(int i) {
		return Optional.ofNullable(inventories.get(i));
	}


	public void open(int i) {
		if (!inventories.containsKey(i)) return;

		MultiInventoryListener.change.add(player);
		player.openInventory(inventories.get(i));
		MultiInventoryListener.players.put(player, new MultiInvEntry(this, i));
		MultiInventoryListener.change.remove(player);
	}


	public void openFirst() {
		open(0);
	}


	public void openLast() {
		int i = inventories.size() - 1;
		open(i);
	}


	public void openNext() {
		if (!MultiInventoryListener.players.containsKey(player) || !MultiInventoryListener.players.get(player).getMultiInventory().equals(this)) return;
		int i = MultiInventoryListener.players.get(player).getInventory();
		if (i + 1 >= inventories.size()) return;
		open(i + 1);
	}


	public void openPrevious() {
		if (!MultiInventoryListener.players.containsKey(player) || !MultiInventoryListener.players.get(player).getMultiInventory().equals(this)) return;
		int i = MultiInventoryListener.players.get(player).getInventory();
		if (i - 1 < 0) return;
		open(i - 1);
	}


	private WSInventory createInv(int invAmount, boolean hotBar) {
		WSInventory inv = WSInventory.of(invAmount > 1 || hotBar ? rows.getRealSlots() : rows.getSlots(), name);
		if (invAmount > 1) {
			inv.setItem(HeadDatabase.STONE_LEFT.getHead()
				.setDisplayName(WSText.of("<", EnumTextColor.RED, WSText.of("--", EnumTextColor.RED, EnumTextStyle.STRIKETHROUGH))).update(), rows.getSlots() + 2);
			inv.setItem(HeadDatabase.WOOD_LEFT.getHead()
				.setDisplayName(WSText.of("<", EnumTextColor.GREEN, WSText.of("-", EnumTextColor.GREEN, EnumTextStyle.STRIKETHROUGH))).update(), rows.getSlots() + 3);
			inv.setItem(HeadDatabase.WOOD_RIGHT.getHead()
					.setDisplayName(WSText.of("-", EnumTextColor.GREEN, WSText.of(">", EnumTextColor.GREEN, EnumTextStyle.RESET), EnumTextStyle.STRIKETHROUGH)).update(),
				rows.getSlots() + 5);
			inv.setItem(HeadDatabase.STONE_RIGHT.getHead()
					.setDisplayName(WSText.of("--", EnumTextColor.RED, WSText.of(">", EnumTextColor.RED, EnumTextStyle.RESET), EnumTextStyle.STRIKETHROUGH)).update(),
				rows.getSlots() + 6);
		}
		return inv;
	}


	public WSPlayer getPlayer() {
		return player;
	}


	public InventoryRows getRows() {
		return rows;
	}


	public WSText getName() {
		return name;
	}


	public String getId() {
		return id;
	}


	public boolean hasHotBar() {
		return inventories.size() > 1 || hotBar;
	}


	@Override
	public boolean hasHotbarIfUnique() {
		return hotBar;
	}


	@Override
	public Optional<WSItemStack> getItem(int slot) {
		if (inventories.size() == 1) return inventories.get(0).getItem(slot);
		int invNumber = Math.floorDiv(slot, rows.getSlots());
		if (inventories.size() <= invNumber) return Optional.empty();
		return inventories.get(invNumber).getItem(slot % rows.getSlots());
	}


	public int size() {
		return inventories.size() == 1 && hotBar ? rows.getRealSlots() : inventories.size() * rows.getSlots();
	}

	@Override
	public void onClick(MultiInventoryClickEvent event) {

	}

	@Override
	public void onPlayerInventoryClick(MultiInventoryClickPlayerInvEvent event) {

	}

	@Override
	public void onHotbarClick(MultiInventoryClickHotbarEvent event) {

	}

	@Override
	public void onClose(MultiInventoryCloseEvent event) {

	}
}

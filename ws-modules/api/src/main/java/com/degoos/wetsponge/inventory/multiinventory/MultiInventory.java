package com.degoos.wetsponge.inventory.multiinventory;


import com.degoos.wetsponge.inventory.WSInventory;
import com.degoos.wetsponge.item.WSItemStack;
import com.degoos.wetsponge.text.WSText;

import java.util.Map;
import java.util.Optional;
import java.util.Set;

public interface MultiInventory {

    void setItem(int slot, WSItemStack item);

    void setItem(int inventory, int invSlot, WSItemStack item);

    void setItemOnHotbar(int slot, WSItemStack WSItemStack);

    Set<WSItemStack> getContents();

    Map<Integer, WSItemStack> getContentsMap();

    Map<Integer, WSInventory> getInventories();

    Optional<WSInventory> getInventory(int i);

    InventoryRows getRows();

    WSText getName();

    String getId();

    boolean hasHotBar();

    boolean hasHotbarIfUnique();

    Optional<WSItemStack> getItem(int slot);

    int size();

    void onClick(MultiInventoryClickEvent event);

    void onPlayerInventoryClick(MultiInventoryClickPlayerInvEvent event);

    void onHotbarClick(MultiInventoryClickHotbarEvent event);

    void onClose(MultiInventoryCloseEvent event);
}

package com.degoos.wetsponge.skin;

/**
 * Represents the visibility of a {@link WSMineskin} in the website https://mineskin.org/.
 * If the skin is private it won't appear in the list of skins.
 */
public enum WSMineskinVisibility {

	PUBLIC(0),
	PRIVATE(1);

	private int value;

	WSMineskinVisibility(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

}

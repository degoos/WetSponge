package com.degoos.wetsponge.plugin;


import com.degoos.wetsponge.config.ConfigAccessor;
import com.degoos.wetsponge.exception.plugin.WSInvalidDescriptionException;
import com.google.common.collect.ImmutableList;
import java.io.InputStream;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.yaml.snakeyaml.Yaml;

public final class WSPluginDescription {

	String rawName = null;
	private String       name        = null;
	private String       main        = null;
	private List<String> depend      = ImmutableList.of();
	private List<String> softDepend  = ImmutableList.of();
	private String       version     = null;
	private String       description = null;
	private List<String> authors     = null;
	private String       website     = null;
	private ConfigAccessor configAccessor;


	public WSPluginDescription(final InputStream stream) throws WSInvalidDescriptionException {
		configAccessor = new ConfigAccessor(stream);
		loadMap(configAccessor);
	}


	public WSPluginDescription(final Reader reader) throws WSInvalidDescriptionException {
		configAccessor = new ConfigAccessor(reader);
		loadMap(configAccessor);
	}


	public WSPluginDescription(final String pluginName, final String pluginVersion, final String mainClass) {
		name = pluginName.replace(' ', '_');
		version = pluginVersion;
		main = mainClass;
	}


	private static List<String> makePluginNameList(final ConfigAccessor configAccessor, final String key) throws WSInvalidDescriptionException {
		return configAccessor.getStringList(key).stream().map(string -> string.replace(' ', '_')).collect(Collectors.toList());
	}


	public String getName() {
		return name;
	}


	public String getVersion() {
		return version;
	}


	public String getMain() {
		return main;
	}


	public String getDescription() {
		return description;
	}


	public List<String> getAuthors() {
		return authors;
	}


	public String getWebsite() {
		return website;
	}


	public List<String> getDepend() {
		return depend;
	}


	public List<String> getSoftDepend() {
		return softDepend;
	}


	public String getFullName() {
		return name + " v" + version;
	}


	public void save(Writer writer) {
		if (configAccessor == null) new Yaml().dump(saveMap(), writer);
		else configAccessor.save(writer);
	}


	private void loadMap(ConfigAccessor accessor) throws WSInvalidDescriptionException {
		name = rawName = accessor.getString("name").replace(" ", "");
		if (name == null) throw new WSInvalidDescriptionException("name is not defined");
		if (!name.matches("^[A-Za-z0-9 _.-]+$")) throw new WSInvalidDescriptionException("name '" + name + "' contains invalid characters.");
		name = name.replace(' ', '_');
		version = configAccessor.getString("version");
		if (version == null) throw new WSInvalidDescriptionException("version is not defined");
		main = configAccessor.getString("main");
		if (main == null) throw new WSInvalidDescriptionException("main is not defined");
		depend = makePluginNameList(configAccessor, "depend");
		softDepend = makePluginNameList(configAccessor, "softdepend");
		if (configAccessor.contains("website")) website = configAccessor.getString("website");
		if (configAccessor.contains("description")) description = configAccessor.getString("description");
		if (configAccessor.contains("authors")) authors = configAccessor.getStringList("authors");
		else authors = new ArrayList<>();
	}


	private Map<String, Object> saveMap() {
		Map<String, Object> map = new HashMap<>();
		map.put("name", name);
		map.put("main", main);
		map.put("version", version);
		if (depend != null) map.put("depend", depend);
		if (softDepend != null) map.put("softdepend", softDepend);
		if (website != null) map.put("website", website);
		if (description != null) map.put("description", description);
		else if (authors.size() > 1) map.put("authors", authors);
		return map;
	}


	public String getRawName() {
		return rawName;
	}
}


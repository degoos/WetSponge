package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.enums.block.EnumBlockTypeSwitchFace;
import com.degoos.wetsponge.material.block.WSBlockTypeDirectional;
import com.degoos.wetsponge.material.block.WSBlockTypePowerable;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSBlockTypeSwitch extends WSBlockTypeDirectional, WSBlockTypePowerable {

	EnumBlockTypeSwitchFace getSwitchFace();

	void setSwitchFace(EnumBlockTypeSwitchFace face);

	@Override
	WSBlockTypeSwitch clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setString("facing", getFacing().name());
		compound.setBoolean("powered", isPowered());
		compound.setString("switchFace", getSwitchFace().name());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setFacing(EnumBlockFace.valueOf(compound.getString("facing")));
		setPowered(compound.getBoolean("powered"));
		setSwitchFace(EnumBlockTypeSwitchFace.valueOf(compound.getString("switchFace")));
		return compound;
	}
}

package com.degoos.wetsponge.entity.living.animal;

/**
 * Represents a Donkey.
 */
public interface WSDonkey extends WSChestedHorse {}

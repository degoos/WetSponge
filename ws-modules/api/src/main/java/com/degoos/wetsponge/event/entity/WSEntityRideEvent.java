package com.degoos.wetsponge.event.entity;

import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.event.WSCancellable;

public class WSEntityRideEvent extends WSEntityEvent implements WSCancellable {

	private WSEntity vehicle;
	private boolean cancelled;

	public WSEntityRideEvent(WSEntity entity, WSEntity vehicle) {
		super(entity);
		this.vehicle = vehicle;
		this.cancelled = false;
	}

	public WSEntity getVehicle() {
		return vehicle;
	}

	@Override
	public boolean isCancelled() {
		return cancelled;
	}

	@Override
	public void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
	}
}

package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.WSBlockTypeDirectional;
import com.degoos.wetsponge.material.block.WSBlockTypeOpenable;
import com.degoos.wetsponge.material.block.WSBlockTypePowerable;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSBlockTypeGate extends WSBlockTypeDirectional, WSBlockTypeOpenable, WSBlockTypePowerable {

	boolean isInWall();

	void setInWall(boolean inWall);

	@Override
	WSBlockTypeGate clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setString("facing", getFacing().name());
		compound.setBoolean("open", isOpen());
		compound.setBoolean("powered", isPowered());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setFacing(EnumBlockFace.valueOf(compound.getString("facing")));
		setOpen(compound.getBoolean("open"));
		setPowered(compound.getBoolean("powered"));
		return compound;
	}
}

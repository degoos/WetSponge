package com.degoos.wetsponge.enums.item;

import java.util.Arrays;
import java.util.Optional;

public enum EnumItemTypeCoalType {

    COAL(0), CHARCOAL(1);

    private int value;

    EnumItemTypeCoalType(int value) {
        this.value = value;
    }

    public static Optional<EnumItemTypeCoalType> getByValue(int value) {
        return Arrays.stream(values()).filter(target -> target.getValue() == value).findAny();
    }


    public static Optional<EnumItemTypeCoalType> getByName(String name) {
        return Arrays.stream(values()).filter(target -> target.name().equalsIgnoreCase(name)).findAny();
    }


    public int getValue() {
        return value;
    }
}

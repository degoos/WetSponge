package com.degoos.wetsponge.entity.vehicle;


public interface WSBoat extends WSVehicle {

	/**
	 * Gets whether this boat is currently in water.
	 * Due to Spigot doesn't have implemented this method, it will return false on this server item.
	 *
	 * @return If the boat is in water
	 */
	boolean isInWater ();

	/**
	 * Gets the maximum speed that this boat is allowed to travel at.
	 *
	 * The Default value is 0.4.
	 *
	 * @return The maximum speed
	 */
	double getMaxSpeed ();

	/**
	 * Sets the maximum speed that this boat is allowed to travel at.
	 *
	 * The Default value is 0.4.
	 *
	 * @param maxSpeed
	 * 		The new max speed
	 */
	void setMaxSpeed (double maxSpeed);

	/**
	 * Gets whether or not the boat is able to move freely on land.
	 *
	 * @return If the boat can move on land
	 */
	boolean canMoveOnLand ();

	/**
	 * Gets whether or not the boat is able to move freely on land.
	 *
	 * @param moveOnLand
	 * 		If the boat can move on land
	 */
	void setMoveOnLand (boolean moveOnLand);

	/**
	 * Gets the rate at which occupied boats decelerate.
	 *
	 * @return The occupied deceleration rate
	 */
	double getOccupiedDeceleration ();

	/**
	 * Sets the rate at which occupied boats decelerate.
	 *
	 * @param occupiedDeceleration
	 * 		The new occupied deceleration rate
	 */
	void setOccupiedDeceleration (double occupiedDeceleration);

	/**
	 * Gets the rate at which unoccupied boats decelerate.
	 *
	 * @return The unoccupied deceleration rate
	 */
	double getUnoccupiedDeceleration ();

	/**
	 * Sets the rate at which unoccupied boats decelerate.
	 *
	 * @param unoccupiedDeceleration
	 * 		The new unoccupied deceleration rate
	 */
	void setUnoccupiedDeceleration (double unoccupiedDeceleration);

}

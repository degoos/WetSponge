package com.degoos.wetsponge.nbt;

public interface WSNBTPrimitive extends WSNBTBase {

	long getLong();

	int getInt();

	short getShort();

	byte getByte();

	double getDouble();

	float getFloat();

	@Override
	WSNBTPrimitive copy();

}

package com.degoos.wetsponge.skin;

import com.degoos.wetsponge.user.WSGameProfile;

/**
 * This class represents a skin stored in the website https://mineskin.org/.
 * <p>
 * You can get an instance using the class {@link WSMineskinClient}.
 */
public class WSMineskin {

	private int id;
	private String name;
	private WSSkinData skinData;
	private long timestamp;
	private boolean privateSkin;
	private int views;

	WSMineskin(JMineskin jMineskin) {
		this.id = jMineskin.id;
		this.name = jMineskin.name;
		this.skinData = new WSSkinData(jMineskin.data);
		this.timestamp = jMineskin.timestamp;
		this.privateSkin = jMineskin.prvate;
		this.views = jMineskin.views;
	}

	/**
	 * Returns the id of the skin.
	 *
	 * @return the id of the skin.
	 */
	public int getId() {
		return id;
	}

	/**
	 * Returns the name of the skin.
	 *
	 * @return the name of the skin.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Returns the {@link WSSkinData skin data}.
	 * This object stores the UUID of the official minecraft account which created the skin and the texture.
	 * The given instance is a copy of the original one, so you can't modify it.
	 *
	 * @return the {@link WSSkinData skin data}.
	 */
	public WSSkinData getSkinData() {
		return skinData.clone();
	}

	/**
	 * Returns the timestamp that represents when the skin was uploaded.
	 *
	 * @return the timestamp.
	 */
	public long getTimestamp() {
		return timestamp;
	}

	/**
	 * Returns whether this skin is private.
	 * If a skin is private it won't appear in the website.
	 *
	 * @return whether this skin is private.
	 */
	public boolean isPrivateSkin() {
		return privateSkin;
	}

	/**
	 * Return the amount of views the skin has received.
	 *
	 * @return the amount of views the skin has received.
	 */
	public int getViews() {
		return views;
	}

	/**
	 * Transforms this skin into a {@link WSGameProfile game profile}. This can be used to create skulls and other things.
	 *
	 * @return the {@link WSGameProfile game profile}.
	 */
	public WSGameProfile toGameProfile() {
		return skinData.toGameProfile();
	}
}

package com.degoos.wetsponge.plugin;


import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.util.Validate;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.logging.LogManager;
import java.util.stream.Collectors;

/**
 * Represents a plugin of WetSponge. This class must be extended on every WetSponge plugin.
 */
public class WSPlugin {

	static {
		LogManager.getLogManager().reset();
	}

	private WSPluginClassLoader classLoader;
	private WSPluginDescription pluginDescription;
	private String id;
	private boolean enabled;
	private File dataFolder;
	private Set<WSPlugin> dependencies, enabledSoftDependencies;
	private WSPluginLogger logger;
	//public TimingHandler timings;


	public WSPlugin() {

	}

	/**
	 * Initiates the plugin. This method should be used only by WetSponge.
	 *
	 * @param classLoader       the class loader.
	 * @param pluginDescription the description.
	 */
	protected void init(WSPluginClassLoader classLoader, WSPluginDescription pluginDescription) {
		LogManager.getLogManager().reset();
		Validate.notNull(classLoader, "Classloader cannot be null!");
		Validate.notNull(pluginDescription, "Plugin description cannot be null!");
		this.classLoader = classLoader;
		this.pluginDescription = pluginDescription;
		this.id = pluginDescription.getName();
		this.logger = new WSPluginLogger(id);

		this.dataFolder = new File(WetSponge.getPluginManager().getPluginFolder(), id);
		if (!dataFolder.exists()) dataFolder.mkdirs();
		else if (dataFolder.isFile()) {
			dataFolder.delete();
			dataFolder.mkdirs();
		}

		dependencies = pluginDescription.getDepend().stream().map(target -> WSPluginManager.getInstance().getPlugin(target).orElse(null)).filter(Objects::nonNull)
				.collect(Collectors.toSet());
		enabledSoftDependencies = pluginDescription.getSoftDepend().stream().map(target -> WSPluginManager.getInstance().getPlugin(target).orElse(null))
				.filter(Objects::nonNull).collect(Collectors.toSet());

		enabled = false;
	}


	/**
	 * This method can be override by the plugin. It's called on the startup.
	 */
	public void onEnable() {

	}


	/**
	 * This mehtod can be override by the plugin. It's called when WetSponge is stopped.
	 */
	public void onDisable() {

	}


	/**
	 * Returns the {@link WSPluginClassLoader class loader} of the {@link WSPlugin plugin}.
	 *
	 * @return the {@link WSPluginClassLoader class loader} of the {@link WSPlugin plugin}.
	 */
	public WSPluginClassLoader getClassLoader() {
		return classLoader;
	}


	/**
	 * Returns the {@link WSPluginDescription description} of the {@link WSPlugin plugin}.
	 *
	 * @return the {@link WSPluginDescription description} of the {@link WSPlugin plugin}.
	 */
	public WSPluginDescription getPluginDescription() {
		return pluginDescription;
	}

	/**
	 * Returns the id of the {@link WSPlugin plugin}. This is equivalent to the name.
	 *
	 * @return the id of the {@link WSPlugin plugin}.
	 */
	public String getId() {
		return id;
	}

	/**
	 * Returns the {@link File folder} used by the {@link WSPlugin plugin} to storage data.
	 *
	 * @return the {@link File folder} used by the {@link WSPlugin plugin} to storage data.
	 */
	public File getDataFolder() {
		return dataFolder;
	}

	/**
	 * Returns the logger of the {@link WSPlugin plugin}. This can be used to send log messages.
	 *
	 * @return the logger of the {@link WSPlugin plugin}.
	 */
	public WSPluginLogger getLogger() {
		return logger;
	}

	/**
	 * Returns whether the plugin is enabled.
	 *
	 * @return whether the plugin is enabled.
	 */
	public boolean isEnabled() {
		return enabled;
	}

	/**
	 * Enables or disables the plugin. This method calls the method {@link #onEnable()} or {@link #onDisable()}.
	 *
	 * @param enabled whether the plugin will be enabled or disabled.
	 */
	public void setEnabled(boolean enabled) {
		if (this.enabled == enabled) return;
		this.enabled = enabled;

		WetSponge.getTimings().assignPluginToThread(this);
		WetSponge.getTimings().startTiming(getId() + (enabled ? " enabling" : " disabling"));
		if (enabled) onEnable();
		else onDisable();
		WetSponge.getTimings().stopTiming();
		WetSponge.getTimings().assignPluginToThread(null);
	}

	/**
	 * Returns a list with all {@link WSPlugin dependencies} of the {@link WSPlugin plugin}.
	 *
	 * @return a list with all {@link WSPlugin dependencies} of the {@link WSPlugin plugin}.
	 */
	public Set<WSPlugin> getDependencies() {
		return new HashSet<>(dependencies);
	}

	/**
	 * Returns whether the given plugin is a dependency.
	 *
	 * @param plugin the plugin.
	 * @return whether the given plugin is a dependency.
	 */
	public boolean isDependency(WSPlugin plugin) {
		return dependencies.contains(plugin);
	}

	/**
	 * Returns a list with all {@link WSPlugin optional dependencies} of the {@link WSPlugin plugin}.
	 *
	 * @return a list with all {@link WSPlugin optional dependencies} of the {@link WSPlugin plugin}.
	 */
	public Set<WSPlugin> getEnabledSoftDependencies() {
		return enabledSoftDependencies;
	}

	/**
	 * Returns whether the given plugin is an optional dependency.
	 *
	 * @param plugin the plugin.
	 * @return whether the given plugin is an optional dependency.
	 */
	public boolean isSoftDependency(WSPlugin plugin) {
		return enabledSoftDependencies.contains(plugin);
	}

	/**
	 * Returns an {@link InputStream input stream} of a file stored in the jar of the {@link WSPlugin plugin}.
	 *
	 * @param filename the URL of the file.
	 * @return an {@link InputStream input stream} of a file stored in the jar of the {@link WSPlugin plugin}.
	 */
	public InputStream getResource(String filename) {
		if (filename == null) {
			throw new IllegalArgumentException("Filename cannot be null");
		} else {
			try {
				URL url = classLoader.getResource(filename);
				if (url == null) {
					return null;
				} else {
					URLConnection connection = url.openConnection();
					connection.setUseCaches(false);
					return connection.getInputStream();
				}
			} catch (IOException var4) {
				return null;
			}
		}
	}


	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		WSPlugin wsPlugin = (WSPlugin) o;

		return id.equals(wsPlugin.id);
	}


	@Override
	public int hashCode() {
		return id.hashCode();
	}
}

package com.degoos.wetsponge.text.translation;

/**
 * Represents a translatable object.
 */
public interface WSTranslatable {

	/**
	 * Gets the {@link WSTranslation} of this object.
	 *
	 * @return the translation.
	 */
	WSTranslation getTranslation();

}

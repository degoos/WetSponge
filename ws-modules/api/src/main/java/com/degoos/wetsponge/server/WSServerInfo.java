package com.degoos.wetsponge.server;


import com.degoos.wetsponge.text.WSText;
import java.net.InetSocketAddress;
import java.util.Optional;

/**
 * The WSServerInfo class storages the information about the {@link WSServer server}.
 */
public interface WSServerInfo {

	/**
	 * @return the amount of {@link com.degoos.wetsponge.entity.living.player.WSPlayer player}s the {@link WSServer server} has.
	 */
	int getOnlinePlayers();

	/**
	 * @return the amount of {@link com.degoos.wetsponge.entity.living.player.WSPlayer player}s the {@link WSServer server} can hold.
	 */
	int getMaxPlayers();

	/**
	 * @return the idle timeout.
	 */
	int getIdleTimeout();

	/**
	 * Sets the idle timeout.
	 *
	 * @param idleTimeout the idle timeout.
	 */
	void setIdleTimeout(int idleTimeout);

	/**
	 * Returns whether the {@link WSServer server} is in online mode. If the {@link WSServer server} is in offline mode, players will be able to join the {@link WSServer
	 * server} without passing the Mojang's login service.
	 *
	 * @return whether the {@link WSServer server} is in online mode.
	 */
	boolean isOnlineMode();

	/**
	 * Returns whether the {@link WSServer server} is full. If true, {@link com.degoos.wetsponge.entity.living.player.WSPlayer player}s cannot join unless a {@link
	 * com.degoos.wetsponge.plugin.WSPlugin plugin} allows it.
	 *
	 * @return whether the {@link WSServer server} is full.
	 */
	boolean isFull();

	/**
	 * Returns whether the {@link WSServer server} has the whitelist enabled. If true, {@link com.degoos.wetsponge.entity.living.player.WSPlayer player}s can only
	 * join if
	 * they're in the whitelist.
	 *
	 * @return whether the {@link WSServer server} has the whitelist enabled.
	 */
	boolean hasWhiteList();

	/**
	 * @return returns the server Name (or Motd in case of Sponge) of the {@link WSServer server}.
	 */
	String getServerName();

	/**
	 * @return returns the default Motd of the {@link WSServer server}.
	 */
	WSText getMotd();

	/**
	 * @return returns the Base64 encoded image of the {@link WSServer server}, or an empty {@link String string} if not present.
	 */
	String getBase64ServerIcon();

	/**
	 * Returns the {@link WSFavicon server icon} of the {@link WSServer server}.
	 * @return the {@link WSFavicon server icon}.
	 */
	Optional<WSFavicon> getServerIcon();

	/**
	 * @return the {@link WSServer server} {@link InetSocketAddress connection}, if present.
	 */
	Optional<InetSocketAddress> getBoundAddress();
}

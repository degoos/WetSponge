package com.degoos.wetsponge.enums.item;

import java.util.Arrays;
import java.util.Optional;

public enum EnumItemTypeCookedFishType {

	COD(0), SALMON(1);

	private int value;

	EnumItemTypeCookedFishType(int value) {
		this.value = value;
	}

	public static Optional<EnumItemTypeCookedFishType> getByValue(int value) {
		return Arrays.stream(values()).filter(target -> target.getValue() == value).findAny();
	}


	public static Optional<EnumItemTypeCookedFishType> getByName(String name) {
		return Arrays.stream(values()).filter(target -> target.name().equalsIgnoreCase(name)).findAny();
	}


	public int getValue() {
		return value;
	}
}

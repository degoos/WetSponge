package com.degoos.wetsponge.enums;


import java.util.Arrays;
import java.util.Optional;

public enum EnumTextColor {

    BLACK('0'),
    DARK_BLUE('1'),
    DARK_GREEN('2'),
    DARK_AQUA('3'),
    DARK_RED('4'),
    DARK_PURPLE('5'),
    GOLD('6'),
    GRAY('7'),
    DARK_GRAY('8'),
    BLUE('9'),
    GREEN('a'),
    AQUA('b'),
    RED('c'),
    LIGHT_PURPLE('d'),
    YELLOW('e'),
    WHITE('f');

    private char id;


    EnumTextColor(char id) {
        this.id = id;
    }


    public static Optional<EnumTextColor> getByChar(char c) {
        return Arrays.stream(values()).filter(enumTextColor -> enumTextColor.getId() == c).findAny();
    }


    public static Optional<EnumTextColor> getByName(String name) {
        return Arrays.stream(values()).filter(enumTextColor -> enumTextColor.name().equalsIgnoreCase(name)).findAny();
    }


    public char getId() {
        return id;
    }

    public EnumDyeColor toDyeColor() {
        switch (this) {
            case BLACK:
                return EnumDyeColor.BLACK;
            case DARK_BLUE:
                return EnumDyeColor.BLUE;
            case DARK_GREEN:
                return EnumDyeColor.GREEN;
            case DARK_AQUA:
                return EnumDyeColor.CYAN;
            case DARK_RED:
                return EnumDyeColor.RED;
            case DARK_PURPLE:
                return EnumDyeColor.PURPLE;
            case GOLD:
                return EnumDyeColor.ORANGE;
            case GRAY:
                return EnumDyeColor.LIGHT_GRAY;
            case DARK_GRAY:
                return EnumDyeColor.GRAY;
            case BLUE:
                return EnumDyeColor.BLUE;
            case GREEN:
                return EnumDyeColor.LIME;
            case AQUA:
                return EnumDyeColor.LIGHT_BLUE;
            case RED:
                return EnumDyeColor.BROWN;
            case LIGHT_PURPLE:
                return EnumDyeColor.PINK;
            case YELLOW:
                return EnumDyeColor.YELLOW;
            case WHITE:
                return EnumDyeColor.WHITE;
            default:
                return EnumDyeColor.WHITE;
        }
    }
}

package com.degoos.wetsponge.plugin;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.config.WetSpongeConfig;
import com.degoos.wetsponge.enums.EnumTextColor;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.util.InternalLogger;

import java.util.Objects;

/**
 * Represents the logger of a plugin. This util class allows plugins to send colored messages to the config.
 * You can create custom loggers using the public constructor.
 * <p>
 * The message format is:
 * <p>
 * {@code {yellow}[ID -> {color}TYPE{yellow}] {color}MESSAGE.}
 */
public class WSPluginLogger {

	private final String id;
	private final WSText banner;
	private static final WSText close = WSText.builder("] ").color(EnumTextColor.YELLOW).build();

	public WSPluginLogger(String id) {
		this.id = id;
		banner = WSText.builder("[" + id + " -> ").color(EnumTextColor.YELLOW).build();
	}

	/**
	 * Sends an error message. The color of the message is {@link EnumTextColor#RED}.
	 *
	 * @param string the message.
	 */
	public void sendError(String string) {
		sendError(WSText.of(string));
	}

	/**
	 * Sends a warning message. The color of the message is {@link EnumTextColor#YELLOW}.
	 *
	 * @param string the message.
	 */
	public void sendWarning(String string) {
		sendWarning(WSText.of(string));
	}

	/**
	 * Sends an info message. The color of the message is {@link EnumTextColor#AQUA}.
	 *
	 * @param string the message.
	 */
	public void sendInfo(String string) {
		sendInfo(WSText.of(string));
	}

	/**
	 * Sends a done message. The color of the message is {@link EnumTextColor#GREEN}.
	 *
	 * @param string the message.
	 */
	public void sendDone(String string) {
		sendDone(WSText.of(string));
	}

	/**
	 * Sends a debug message. The color of the message is {@link EnumTextColor#GRAY}.
	 * Debug messages will only appear if the node 'debug' is enabled in the file wetSpongeConfig.yml.
	 *
	 * @param string the message.
	 */
	public void sendDebug(String string) {
		if (WetSpongeConfig.getConfig().getBoolean("debug", false))
			send(WSText.of(string), WSText.builder("Debug").color(EnumTextColor.GRAY).build(), EnumTextColor.GRAY);
	}

	/**
	 * Sends an error message. The color of the message is {@link EnumTextColor#RED}.
	 *
	 * @param text the message.
	 */
	public void sendError(WSText text) {
		send(text, WSText.builder("Error").color(EnumTextColor.RED).build(), EnumTextColor.RED);
	}

	/**
	 * Sends a warning message. The color of the message is {@link EnumTextColor#YELLOW}.
	 *
	 * @param text the message.
	 */
	public void sendWarning(WSText text) {
		send(text, WSText.builder("Warning").color(EnumTextColor.LIGHT_PURPLE).build(), EnumTextColor.LIGHT_PURPLE);
	}

	/**
	 * Sends an info message. The color of the message is {@link EnumTextColor#AQUA}.
	 *
	 * @param text the message.
	 */
	public void sendInfo(WSText text) {
		send(text, WSText.builder("Info").color(EnumTextColor.AQUA).build(), EnumTextColor.AQUA);
	}

	/**
	 * Sends a done message. The color of the message is {@link EnumTextColor#GREEN}.
	 *
	 * @param text the message.
	 */
	public void sendDone(WSText text) {
		send(text, WSText.builder("Done").color(EnumTextColor.GREEN).build(), EnumTextColor.GREEN);
	}

	/**
	 * Sends a debug message. The color of the message is {@link EnumTextColor#GRAY}.
	 * Debug messages will only appear if the node 'debug' is enabled in the file wetSpongeConfig.yml.
	 *
	 * @param text the message.
	 */
	public void sendDebug(WSText text) {
		if (WetSpongeConfig.getConfig().getBoolean("debug", false))
			send(text, WSText.builder("Debug").color(EnumTextColor.GRAY).build(), EnumTextColor.GRAY);
	}

	private void send(WSText text, WSText type, EnumTextColor color) {
		WetSponge.getServer().getConsole().sendMessage(banner.toBuilder().append(type).append(close)
				.append(WSText.builder("").color(color).append(text).build()).build());
	}

	/**
	 * Prints an exception on the console. Using this method the stacktrace will be more visible and colorful.
	 * The error will also be saved in the folder 'WetSpongeErrors' and it will be uploaded to the Degoos's hastebin server.
	 *
	 * @param ex   the exception.
	 * @param info the error message.
	 */
	public void printException(Throwable ex, String info) {
		printException(ex, WSText.of(info));
	}

	/**
	 * Prints an exception on the console. Using this method the stacktrace will be more visible and colorful.
	 * The error will also be saved in the folder 'WetSpongeErrors' and it will be uploaded to the Degoos's hastebin server.
	 *
	 * @param ex   the exception.
	 * @param info the error message.
	 */
	public void printException(Throwable ex, WSText info) {
		printException(ex, info, false);
	}

	private void printException(Throwable ex, WSText info, boolean causedBy) {
		new Thread(() -> {
			try {
				if (!causedBy) {
					sendError("--------------------------------------------");
					sendError(info);
				}
				sendError("");
				sendError(WSText.of("Type: ", WSText.of(ex.getClass().getName(), EnumTextColor.YELLOW)));
				sendError("");
				sendError(WSText.of("Description: ", WSText.of(ex.getLocalizedMessage() == null ? "-" : ex.getLocalizedMessage(), EnumTextColor.YELLOW)));
				try {
					sendError(WSText.of("Cause: ", WSText.of((ex.getCause() == null || Objects.equals(ex.getCause().getLocalizedMessage(), "") ? "-" : ex.getCause()
							.getLocalizedMessage()), EnumTextColor.YELLOW)));
				} catch (Exception e) {
					sendError(WSText.of("Cause: -"));
				}
				sendError("");
				sendError("StackTrace:");
				for (StackTraceElement stackTraceElement : ex.getStackTrace())
					sendError(getColoredStackTrace(stackTraceElement));
				if (ex.getCause() == null) {
					sendError("");
					String[] pastedata = InternalLogger.getHasteStackTraceURL(ex, info, ex.getStackTrace(), id);
					sendError(WSText.builder(WSText.of("Error uploaded to: ", EnumTextColor.AQUA)).append(WSText.of(pastedata[0], EnumTextColor.YELLOW)).build());
					sendError(WSText.builder(WSText.of("Error saved in: ", EnumTextColor.AQUA)).append(WSText.of(pastedata[1], EnumTextColor.YELLOW)).build());
					sendError("");
					sendError("--------------------------------------------");
				} else {
					sendError("");
					sendError("Caused by:");
					printException(ex.getCause(), info, true);
				}
			} catch (Exception ex2) {
				ex.printStackTrace();
				ex2.printStackTrace();
			}
		}).start();
	}

	private static WSText getColoredStackTrace(StackTraceElement element) {
		WSText methodDataText;
		if (element.isNativeMethod()) {
			methodDataText = WSText.of(" (", EnumTextColor.GREEN, WSText.of("Native method", EnumTextColor.YELLOW, WSText.of(")", EnumTextColor.GREEN)));
		} else {
			WSText.Builder builder = WSText.builder(" (").color(EnumTextColor.GREEN);
			if (element.getFileName() != null && element.getLineNumber() >= 0) {
				builder.append(WSText.of(element.getFileName(), EnumTextColor.YELLOW));
				builder.append(WSText.of(":", EnumTextColor.GREEN));
				builder.append(WSText.of(String.valueOf(element.getLineNumber()), EnumTextColor.YELLOW));
				builder.append(WSText.of(")", EnumTextColor.GREEN));
			} else {
				builder.append(WSText.of(element.getFileName() != null ? element.getFileName() : "Unknown Source", EnumTextColor.YELLOW));
				builder.append(WSText.of(")", EnumTextColor.GREEN));
			}
			methodDataText = builder.build();
		}
		WSText classText = WSText.of(element.getMethodName(), EnumTextColor.LIGHT_PURPLE, methodDataText);
		return WSText.of("- " + element.getClassName() + ".", EnumTextColor.RED, classText);
	}

}

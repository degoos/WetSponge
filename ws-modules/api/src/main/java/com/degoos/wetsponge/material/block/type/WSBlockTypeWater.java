package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.material.block.WSBlockTypeLevelled;

public interface WSBlockTypeWater extends WSBlockTypeLevelled {

	@Override
	WSBlockTypeWater clone();
}

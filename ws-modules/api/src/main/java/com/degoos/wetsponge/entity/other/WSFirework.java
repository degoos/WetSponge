package com.degoos.wetsponge.entity.other;

import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.firework.WSFireworkEffect;

import java.util.Collection;
import java.util.List;

public interface WSFirework extends WSEntity {

    /**
     * Returns an immutable {@link List list} with all {@link WSFireworkEffect firework effect}s in the {@link WSFirework firework}.
     *
     * @return the (@link List list}.
     */
    List<WSFireworkEffect> getFireworkEffects();

    /**
     * Sets the {@link WSFireworkEffect firework effect}s of the {@link WSFirework firework}.
     * Any change after use this method in the given {@link Collection collection} won't affect to the {@link WSFirework firework}.
     *
     * @param effects the {@link Collection collection} with the {@link WSFireworkEffect firework effect}s.
     */
    void setFireworkEffects(Collection<WSFireworkEffect> effects);

    /**
     * Adds a {@link WSFireworkEffect firework effect} to the {@link WSFirework firework}.
     *
     * @param effect the {@link WSFireworkEffect firework effect}.
     * @return whether the {@link WSFireworkEffect firework effect} was added.
     */
    boolean addFireworkEffect(WSFireworkEffect effect);

    /**
     * Adds all given {@link WSFireworkEffect firework effect}s to the {@link WSFirework firework}.
     *
     * @param effects the {@link WSFireworkEffect firework effect}s.
     * @return whether the {@link WSFireworkEffect firework effect}s were added.
     */
    boolean addFireworkEffects(WSFireworkEffect... effects);

    /**
     * Adds all given {@link WSFireworkEffect firework effect}s to the {@link WSFirework firework}.
     *
     * @param effects the {@link WSFireworkEffect firework effect}s.
     * @return whether the {@link WSFireworkEffect firework effect}s were added.
     */
    boolean addFireworkEffects(List<WSFireworkEffect> effects);


    /**
     * Removes a {@link WSFireworkEffect firework effect} from the {@link WSFirework firework}.
     *
     * @param effect the {@link WSFireworkEffect firework effect}.
     * @return whether the {@link WSFireworkEffect firework effect} was removed.
     */
    boolean removeFireworkEffect(WSFireworkEffect effect);

    /**
     * Removes all given {@link WSFireworkEffect firework effect}s from the {@link WSFirework firework}.
     *
     * @param effects the {@link WSFireworkEffect firework effect}s.
     * @return whether the {@link WSFireworkEffect firework effect} were removed.
     */
    boolean removeFireworkEffects(WSFireworkEffect... effects);

    /**
     * Removes all given {@link WSFireworkEffect firework effect}s from the {@link WSFirework firework}.
     *
     * @param effects the {@link WSFireworkEffect firework effect}s.
     * @return whether the {@link WSFireworkEffect firework effect} were removed.
     */
    boolean removeFireworkEffects(List<WSFireworkEffect> effects);

    /**
     * Clears all {@link WSFireworkEffect firework effect}s from the {@link WSFirework firwork}.
     */
    void clearFireworkEffects();

    /**
     * Returns whether the {@link WSFirework firework} has any effect.
     * @return whether the {@link WSFirework firework} has any effect.
     */
    boolean hasEffects ();

    /**
     * Returns the amount of effects the {@link WSFirework firework} has.
     * @return the amount.
     */
    int getEffectSize ();

    /**
     * Return the maximum amount of ticks the {@link WSFirework firework} can live.
     * If this amount is less than {@link #getFireworkAge()}, the {@link WSFirework firework} will explode.
     *
     * @return the maximum amount of ticks the {@link WSFirework firework} can live.
     */
    int getLifeTime();

    /**
     * Sets the maximum amount of ticks the {@link WSFirework firework} can live.
     * If this amount is smaller than {@link #getFireworkAge()}, the {@link WSFirework firework} will explode.
     *
     * @param lifeTime the maximum amount of ticks the {@link WSFirework firework} can live.
     */
    void setLifeTime(int lifeTime);

    /**
     * Returns the age of the {@link WSFirework firework}.
     * If this amount is bigger than {@link #getLifeTime()} ()}, the {@link WSFirework firework} will explode.
     *
     * @return the age of the {@link WSFirework firework}.
     */
    int getFireworkAge();

    /**
     * Sets the age of the {@link WSFirework firework}.
     * If this amount is bigger than {@link #getLifeTime()} ()}, the {@link WSFirework firework} will explode.
     *
     * @param fireworkAge the age of the {@link WSFirework firework}.
     */
    void setFireworkAge(int fireworkAge);

    /**
     * Detonates the {@link WSFirework firework}.
     */
    void detonate();
}

package com.degoos.wetsponge.material.item.type;

import com.degoos.wetsponge.color.WSColor;
import com.degoos.wetsponge.firework.WSFireworkEffect;
import com.degoos.wetsponge.material.item.WSItemType;
import com.degoos.wetsponge.nbt.WSNBTBase;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;
import com.degoos.wetsponge.nbt.WSNBTTagInt;
import com.degoos.wetsponge.nbt.WSNBTTagList;

import java.util.ArrayList;
import java.util.List;

public interface WSItemTypeFirework extends WSItemType {

	int getPower();

	void setPower(int power);

	List<WSFireworkEffect> getEffects();

	void setEffects(List<WSFireworkEffect> effects);

	@Override
	WSItemTypeFirework clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setInteger("power", getPower());
		WSNBTTagList list = WSNBTTagList.of();
		getEffects().forEach(target -> {
			WSNBTTagCompound fireworkCompound = WSNBTTagCompound.of();
			fireworkCompound.setString("shape", target.getShape().name());
			fireworkCompound.setBoolean("flickers", target.flickers());
			fireworkCompound.setBoolean("trail", target.hasTrail());
			WSNBTTagList colors = WSNBTTagList.of();
			target.getColors().forEach(color -> colors.appendTag(WSNBTTagInt.of(color.toRGB())));
			fireworkCompound.setTag("colors", colors);
			WSNBTTagList fadeColors = WSNBTTagList.of();
			target.getFadeColors().forEach(color -> fadeColors.appendTag(WSNBTTagInt.of(color.toRGB())));
			fireworkCompound.setTag("fadeColors", fadeColors);
			list.appendTag(fireworkCompound);
		});
		compound.setTag("effects", list);
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		List<WSFireworkEffect> list = new ArrayList<>();
		if (!compound.hasKey("effects")) return compound;
		WSNBTBase base = compound.getTag("effects");
		if (!(base instanceof WSNBTTagList) || ((WSNBTTagList) base).getTagType() != 10) return compound;
		for (int i = 0; i < ((WSNBTTagList) base).tagCount(); i++) {
			WSNBTTagCompound firework = (WSNBTTagCompound) ((WSNBTTagList) base).get(i);
			boolean flickers = firework.getBoolean("flickers");
			boolean trail = firework.getBoolean("trail");

			List<WSColor> colors = new ArrayList<>();
			WSNBTTagList colorTag = (WSNBTTagList) firework.getTag("colors");
			for (int c = 0; c < colorTag.tagCount(); c++)
				colors.add(WSColor.ofRGB(colorTag.getIntAt(c)));

			List<WSColor> fadeColors = new ArrayList<>();
			WSNBTTagList fadeColorTag = (WSNBTTagList) firework.getTag("fadeColors");
			for (int c = 0; c < fadeColorTag.tagCount(); c++)
				fadeColors.add(WSColor.ofRGB(fadeColorTag.getIntAt(c)));

			list.add(WSFireworkEffect.builder().flicker(flickers).trail(trail).colors(colors).fades(fadeColors).build());
		}
		setEffects(list);
		return compound;
	}
}

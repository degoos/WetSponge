package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.bridge.packet.BridgeServerPacket;
import com.degoos.wetsponge.packet.WSPacket;

public interface WSSPacketCloseWindows extends WSPacket {

	public static WSSPacketCloseWindows of(int windowsId) {
		return BridgeServerPacket.newWSSPacketCloseWindows(windowsId);
	}

	int getWindowsId();

	void setWindowsId(int windowsId);

}

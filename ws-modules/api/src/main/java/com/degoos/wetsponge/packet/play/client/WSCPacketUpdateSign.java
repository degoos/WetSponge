package com.degoos.wetsponge.packet.play.client;

import com.degoos.wetsponge.bridge.packet.BridgeClientPacket;
import com.degoos.wetsponge.packet.WSPacket;
import com.flowpowered.math.vector.Vector3d;

public interface WSCPacketUpdateSign extends WSPacket {

	public static WSCPacketUpdateSign of(Vector3d position, String[] lines) {
		return BridgeClientPacket.newWSCPacketUpdateSign(position, lines);
	}

	Vector3d getPosition();

	void setPosition(Vector3d position);

	String[] getLines();

	void setLines(String[] lines);

}

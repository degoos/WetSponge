package com.degoos.wetsponge.enums.block;


import java.util.Arrays;
import java.util.Optional;

public enum EnumBlockTypeStoneType {

	STONE(0), GRANITE(1), SMOOTH_GRANITE(2), DIORITE(3), SMOOTH_DIORITE(4), ANDESITE(5), SMOOTH_ANDESITE(6);

	private int value;


	EnumBlockTypeStoneType(int value) {
		this.value = value;
	}


	public static Optional<EnumBlockTypeStoneType> getByValue (int value) {
		return Arrays.stream(values()).filter(stoneType -> stoneType.getValue() == value).findAny();
	}

	public static Optional<EnumBlockTypeStoneType> getByName (String name) {
		return Arrays.stream(values()).filter(stoneType -> stoneType.name().equalsIgnoreCase(name)).findAny();
	}


	public int getValue () {
		return value;
	}
}

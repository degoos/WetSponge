package com.degoos.wetsponge.event;


public interface WSCancellable {

	boolean isCancelled();

	void setCancelled(boolean cancelled);

}

package com.degoos.wetsponge.nbt;

import com.degoos.wetsponge.bridge.nbt.BridgeNBT;

public interface WSNBTTagInt extends WSNBTPrimitive {

	public static WSNBTTagInt of(int i) {
		return BridgeNBT.ofInt(i);
	}

	@Override
	WSNBTTagInt copy();
}

package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.WSBlockTypeDirectional;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSBlockTypeDispenser extends WSBlockTypeDirectional {

	boolean isTriggered();

	void setTriggered(boolean triggered);

	@Override
	WSBlockTypeDispenser clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setString("facing", getFacing().name());
		compound.setBoolean("triggered", isTriggered());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setFacing(EnumBlockFace.valueOf(compound.getString("facing")));
		setTriggered(compound.getBoolean("triggered"));
		return compound;
	}
}

package com.degoos.wetsponge.material.item.type;

import com.degoos.wetsponge.enums.block.EnumBlockTypeSkullType;
import com.degoos.wetsponge.material.item.WSItemType;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;
import com.degoos.wetsponge.nbt.WSNBTTagList;
import com.degoos.wetsponge.user.WSGameProfile;
import com.degoos.wetsponge.user.WSProfileProperty;

import java.net.URL;
import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

public interface WSItemTypeSkull extends WSItemType {

	Optional<WSGameProfile> getProfile();

	void setProfile(WSGameProfile profile);

	void setTexture(String texture);

	void setTexture(URL texture);

	void setTextureByPlayerName(String name);

	void findFormatAndSetTexture(String texture);

	EnumBlockTypeSkullType getSkullType();

	void setSkullType(EnumBlockTypeSkullType skullType);

	@Override
	WSItemTypeSkull clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setString("skullType", getSkullType().name());
		WSGameProfile gameProfile = getProfile().orElse(null);
		if (gameProfile == null) return compound;
		WSNBTTagCompound profileCompound = WSNBTTagCompound.of();
		profileCompound.setUniqueId("id", gameProfile.getId());
		gameProfile.getName().ifPresent(target -> profileCompound.setString("name", target));
		WSNBTTagCompound properties = WSNBTTagCompound.of();
		gameProfile.getPropertyMap().keys().forEach(target -> {
			Collection<WSProfileProperty> collection = gameProfile.getPropertyMap().get(target);
			WSNBTTagList list = WSNBTTagList.of();
			collection.forEach(property -> {
				WSNBTTagCompound propertyCompound = WSNBTTagCompound.of();
				profileCompound.setString("value", property.getValue());
				property.getSignature().ifPresent(signature -> profileCompound.setString("signature", signature));
				list.appendTag(propertyCompound);
			});
			properties.setTag(target, list);
		});

		profileCompound.setTag("properties", properties);
		compound.setTag("profile", profileCompound);
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setSkullType(EnumBlockTypeSkullType.valueOf(compound.getString("skullType")));
		if (!compound.hasKey("profile")) {
			setProfile(null);
			return compound;
		}
		WSNBTTagCompound profileCompound = (WSNBTTagCompound) compound.getTag("profile");
		UUID uuid = profileCompound.getUniqueId("id");
		String name = profileCompound.hasKey("name") ? null : profileCompound.getString("name");

		WSGameProfile profile = WSGameProfile.of(uuid, name);
		setProfile(profile);

		WSNBTTagCompound properties = (WSNBTTagCompound) profileCompound.getTag("properties");
		properties.getKeySet().forEach(target -> {
			WSNBTTagList list = properties.getTagList(target, 10);
			if (list == null) return;
			for (int i = 0; i < list.tagCount(); i++) {
				WSNBTTagCompound propertyCompound = list.getCompoundTagAt(i);
				if (propertyCompound == null) return;
				profile.addProperty(target, WSProfileProperty.of(target, propertyCompound.getString("value"),
						propertyCompound.hasKey("signature") ? null : propertyCompound.getString("signature")));
			}
		});
		return compound;
	}
}

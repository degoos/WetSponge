package com.degoos.wetsponge.server;

import com.degoos.wetsponge.text.WSText;

public interface WSServerProperties {

	/**
	 * Returns whether flying is allowed in the server.
	 *
	 * @return whether flying is allowed in the server.
	 */
	boolean isFlightAllowed();

	/**
	 * Sets whether flying is allowed in the server.
	 * This won't be saved in the file server.properties!
	 *
	 * @param flightAllowed whether flying is allowed in the server.
	 *
	 * @return the {@link WSServerProperties server properties}.
	 */
	WSServerProperties setFlightAllowed(boolean flightAllowed);

	/**
	 * Returns whether PVP is enabled in the server.
	 *
	 * @return whether PVP is enabled in the server.
	 */
	boolean isPVPEnabled();

	/**
	 * Sets whether PVP is enabled in the server.
	 * This won't be saved in the file server.properties!
	 *
	 * @param pvpEnabled whether PVP is enabled in the server.
	 *
	 * @return the {@link WSServerProperties server properties}.
	 */
	WSServerProperties setPVPEnabled(boolean pvpEnabled);

	/**
	 * Returns whether NPC spawn is enabled in the server.
	 *
	 * @return whether NPC spawn is enabled in the server.
	 */
	boolean canSpawnNPCs();

	/**
	 * Sets whether NPC spawn is enabled in the server.
	 * This won't be saved in the file server.properties!
	 *
	 * @param canSpawnNPCs whether NPC spawn is enabled in the server.
	 *
	 * @return the {@link WSServerProperties server properties}.
	 */
	WSServerProperties setCanSpawnNPCs(boolean canSpawnNPCs);

	/**
	 * Returns whether animal spawn is enabled in the server.
	 *
	 * @return whether animal spawn is enabled in the server.
	 */
	boolean canSpawnAnimals();

	/**
	 * Sets whether animal spawn is enabled in the server.
	 * This won't be saved in the file server.properties!
	 *
	 * @param canSpawnAnimals whether animal spawn is enabled in the server.
	 *
	 * @return the {@link WSServerProperties server properties}.
	 */
	WSServerProperties setCanSpawnAnimals(boolean canSpawnAnimals);

	/**
	 * Returns whether the server prevents proxy connections.
	 * This is always true in {@link com.degoos.wetsponge.enums.EnumServerType#SPIGOT Spigot}.
	 *
	 * @return whether the server prevents proxy connections.
	 */
	boolean preventProxyConnections();

	/**
	 * Sets whether the server prevents proxy connections.
	 * This won't be saved in the file server.properties!
	 * This cannot be changed in {@link com.degoos.wetsponge.enums.EnumServerType#SPIGOT} Spigot.
	 *
	 * @param preventProxyConnections whether the server prevents proxy connections.
	 *
	 * @return the {@link WSServerProperties server properties}.
	 */
	WSServerProperties setPreventProxyConnections(boolean preventProxyConnections);

	/**
	 * Returns whether the server is in online mode.
	 *
	 * @return whether the server is in online mode.
	 */
	boolean isServerInOnlineMode();

	/**
	 * Sets whether the server is in online mode.
	 * This won't be saved in the file server.properties!
	 *
	 * @param onlineMode whether the server is in online mode
	 *
	 * @return the {@link WSServerProperties server properties}.
	 */
	WSServerProperties setOnlineMode(boolean onlineMode);

	/**
	 * Returns the {@link WSText MOTD} of the server.
	 *
	 * @return the {@link WSText MOTD} of the server.
	 */
	WSText getMOTD();

	/**
	 * Sets the {@link WSText MOTD} of the server.
	 * This won't be saved in the file server.properties!
	 *
	 * @param motd the {@link WSText MOTD}.
	 *
	 * @return the {@link WSServerProperties server properties}.
	 */
	WSServerProperties setMOTD(WSText motd);


	/**
	 * Returns the build limit of the server.
	 *
	 * @return the build limit of the server.
	 */
	int getBuildLimit();

	/**
	 * Sets the build limit of the server.
	 * This won't be saved in the file server.properties!
	 *
	 * @param buildLimit the build limit.
	 *
	 * @return the {@link WSServerProperties server properties}.
	 */
	WSServerProperties setBuildLimit(int buildLimit);

}

package com.degoos.wetsponge.event;


import co.aikar.wetspongetimings.TimedEventListener;
import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.comparator.EventListenerComparator;
import com.degoos.wetsponge.exception.event.WSListenerRegistrationException;
import com.degoos.wetsponge.plugin.WSPlugin;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.Validate;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class WSEventManager {

	private static WSEventManager instance = new WSEventManager();
	private Map<WSPlugin, Set<TimedEventListener>> listeners;


	private WSEventManager() {
		listeners = new ConcurrentHashMap<>();
	}


	public static WSEventManager getInstance() {
		return instance;
	}


	public void registerListener(Object listener, WSPlugin plugin) {
		try {
			Validate.notNull(plugin, "Plugin cannot be null!");
			Validate.notNull(listener, "Listener cannot be null!");

			if (!plugin.isEnabled()) throw new WSListenerRegistrationException("Plugin " + plugin.getId() + " is not enabled");
			if (!listeners.containsKey(plugin)) listeners.put(plugin, Collections.newSetFromMap(new ConcurrentHashMap<>()));
			listeners.get(plugin).addAll(Arrays.stream(listener.getClass().getMethods())
					.filter(method -> method.getAnnotation(WSListener.class) != null && method.getParameterTypes().length == 1 &&
							WSEvent.class.isAssignableFrom(method.getParameterTypes()[0]) && !method.isSynthetic() && !method.isBridge())
					.map(method -> new TimedEventListener(listener, method, plugin, (Class<? extends WSEvent>) method.getParameterTypes()[0])).collect(Collectors.toSet()));
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}


	public void unregisterListener(Object listener, WSPlugin plugin) {
		try {
			Validate.notNull(plugin, "Plugin cannot be null!");
			Validate.notNull(listener, "Listener cannot be null!");
			if (listeners.containsKey(plugin)) {
				listeners.get(plugin).removeIf(entry -> entry.getListener().equals(listener));
			}
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}


	public void unregisterListener(Class<?> listener, WSPlugin plugin) {
		try {
			Validate.notNull(plugin, "Plugin cannot be null!");
			Validate.notNull(listener, "Listener cannot be null!");
			if (listeners.containsKey(plugin)) {
				listeners.get(plugin).removeIf(entry -> listener.isInstance(entry.getListener()));
			}
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}


	public void unregisterListeners(WSPlugin plugin) {
		try {
			Validate.notNull(plugin, "Plugin cannot be null!");
			listeners.remove(plugin);
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}


	public void callEvent(WSEvent event) {
		WSPlugin caller = WetSponge.getTimings().getAssignedPlugin();
		try {

			Validate.notNull(event, "Event cannot be null!");
			List<TimedEventListener> entries = new ArrayList<>();
			listeners.values().forEach(list -> list.stream().filter(entry -> entry.getMethod().getParameterTypes()[0].isInstance(event)).forEach(entries::add));
			boolean cancelled = false, isCancellable = event instanceof WSCancellable;
			WSCancellable cancellable = isCancellable ? (WSCancellable) event : null;
			entries.sort(new EventListenerComparator());
			for (TimedEventListener listener : entries) {
				try {
					if (cancelled && !listener.isIgnoreCancelled()) continue;
				} catch (Throwable ex) {
					InternalLogger.printException(ex, "An error has occurred while calling the event " + event.getClass().getName() + ".");
				}
				try {

					WetSponge.getTimings().assignPluginToThread(listener.getPlugin());
					WetSponge.getTimings().startTiming("Event " + event.getClass() + "listener " +
							listener.getListener() + " method " + listener.getMethod().getName());

					listener.execute(event);

					WetSponge.getTimings().stopTiming();
					//listener.getMethod().invoke(listener.getListener(), event);
				} catch (Throwable ex) {
					InternalLogger.printException(ex,
							"An error has occurred while calling the method " + listener.getMethod().toString() + ". (Event: " + event.getClass().getName() +
									") (Listener: " + listener.getListener() + ") (Cause: " + ex.getCause() + ")");
				}
				if (isCancellable && !cancelled) cancelled = cancellable.isCancelled();
			}
		} catch (Throwable ex) {
			ex.printStackTrace();
		}

		WetSponge.getTimings().assignPluginToThread(caller);
	}

}

package com.degoos.wetsponge.nbt;

public interface WSNBTBase {

	public static final String[] NBT_TYPES = new String[]{"END", "BYTE", "SHORT", "INT", "LONG", "FLOAT", "DOUBLE", "BYTE[]", "STRING", "LIST", "COMPOUND", "INT[]",
		"LONG[]"};

	String toString();

	byte getId();

	WSNBTBase copy();

	boolean hasNoTags();

	int hashCode();

	Object getHandled();

}

package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.bridge.packet.BridgeServerPacket;
import com.degoos.wetsponge.entity.living.player.WSHuman;
import com.degoos.wetsponge.packet.WSPacket;
import com.flowpowered.math.vector.Vector3i;

public interface WSSPacketUseBed extends WSPacket {

	public static WSSPacketUseBed of(int entityId, Vector3i position) {
		return BridgeServerPacket.newWSSPacketUseBed(entityId, position);
	}

	public static WSSPacketUseBed of(WSHuman entity, Vector3i position) {
		return BridgeServerPacket.newWSSPacketUseBed(entity, position);
	}

	public static WSSPacketUseBed of(WSHuman entity) {
		return of(entity, entity.getLocation().toVector3i());
	}

	int getEntityId();

	void setEntityId(int entityId);

	Vector3i getPosition();

	void setPosition(Vector3i position);

}

package com.degoos.wetsponge.text.action.hover;

import com.degoos.wetsponge.bridge.text.action.BridgeTextAction;
import com.degoos.wetsponge.text.WSText;

public interface WSShowTextAction extends WSHoverAction {

	public static WSShowTextAction of(WSText text) {
		return BridgeTextAction.newWSShowTextAction(text);
	}

	WSText getText();

}

package com.degoos.wetsponge.event.world;

import com.degoos.wetsponge.world.WSChunk;
import com.degoos.wetsponge.world.WSWorld;

public class WSChunkLoadEvent extends WSChunkEvent{

	public WSChunkLoadEvent(WSWorld world, WSChunk chunk) {
		super(world, chunk);
	}
}

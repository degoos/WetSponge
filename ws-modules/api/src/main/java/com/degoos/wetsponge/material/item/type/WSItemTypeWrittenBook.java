package com.degoos.wetsponge.material.item.type;

import com.degoos.wetsponge.enums.item.EnumItemTypeBookGeneration;
import com.degoos.wetsponge.material.item.WSItemType;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;
import com.degoos.wetsponge.nbt.WSNBTTagList;
import com.degoos.wetsponge.nbt.WSNBTTagString;
import com.degoos.wetsponge.text.WSText;

import java.util.ArrayList;
import java.util.List;

public interface WSItemTypeWrittenBook extends WSItemType {

	WSText getAuthor();

	WSItemTypeWrittenBook setAuthor(WSText author);

	WSText getTitle();

	WSItemTypeWrittenBook setTitle(WSText title);

	List<WSText> getPages();

	WSItemTypeWrittenBook addPage(WSText pageText);

	WSItemTypeWrittenBook setPages(List<WSText> pages);

	EnumItemTypeBookGeneration getGeneration();

	void setGeneration(EnumItemTypeBookGeneration generation);

	@Override
	WSItemTypeWrittenBook clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setString("author", getAuthor().toFormattingText());
		compound.setString("title", getTitle().toFormattingText());
		compound.setString("generation", getGeneration().name());


		WSNBTTagList list = WSNBTTagList.of();
		getPages().forEach(target -> {
			list.appendTag(WSNBTTagString.of(target.toFormattingText()));
		});
		compound.setTag("pages", list);
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setAuthor(WSText.getByFormattingText(compound.getString("author")));
		setTitle(WSText.getByFormattingText(compound.getString("title")));
		setGeneration(EnumItemTypeBookGeneration.valueOf(compound.getString("generation")));

		WSNBTTagList list = compound.getTagList("pages", 8);

		List<WSText> pages = new ArrayList<>();

		for (int i = 0; i < list.tagCount(); i++) {
			pages.add(WSText.getByFormattingText(list.getStringAt(i)));
		}

		setPages(pages);
		return compound;
	}
}

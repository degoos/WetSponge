package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.material.block.WSBlockType;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSBlockTypeTurtleEgg extends WSBlockType {

	int getEggs();

	void setEggs(int eggs);

	int getMinimumEggs();

	int getMaximumEggs();

	int getHatch();

	void setHatch(int hatch);

	int getMaximumHatch();

	@Override
	WSBlockTypeTurtleEgg clone();


	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setInteger("eggs", getEggs());
		compound.setInteger("hatch", getHatch());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setEggs(compound.getInteger("eggs"));
		setHatch(compound.getInteger("hatch"));
		return compound;
	}
}

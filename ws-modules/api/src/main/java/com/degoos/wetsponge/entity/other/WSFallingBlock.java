package com.degoos.wetsponge.entity.other;

import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.material.block.WSBlockType;

/**
 * Represents a falling block.
 * WARNING! If you want to spawn a falling block you must
 * set {@link #getFallTime()} to 1!
 */
public interface WSFallingBlock extends WSEntity {

	/**
	 * Returns the {@link WSBlockType block item} of the {@link WSFallingBlock falling block}.
	 *
	 * @return the {@link WSBlockType block item} of the {@link WSFallingBlock falling block}.
	 */
	WSBlockType getBlockType();

	/**
	 * Sets the {@link WSBlockType block item} of the {@link WSFallingBlock falling block}.
	 *
	 * @param blockType the {@link WSBlockType block item} of the {@link WSFallingBlock falling block}.
	 */
	void setBlockType(WSBlockType blockType);


	/**
	 * Returns the fall damage per block of the {@link WSFallingBlock falling block}.
	 *
	 * @return the fall damage per block of the {@link WSFallingBlock falling block}.
	 */
	double getFallDamagePerBlock();

	/**
	 * Sets the fall damage per block of the {@link WSFallingBlock falling block}.
	 *
	 * @param fallDamagerPerBlock the fall damage.
	 */
	void setFallDamagerPerBlock(double fallDamagerPerBlock);

	/**
	 * Returns the maximum amount of damage the fall damage per block can reach.
	 *
	 * @return the maximum amount of damage the fall damage per block can reach.
	 */
	double getMaxFallDamage();

	/**
	 * Sets the maximum amount of damage the fall damage per block can reach.
	 *
	 * @param maxFallDamage the maximum amount of damage the fall damage per block can reach.
	 */
	void setMaxFallDamage(double maxFallDamage);

	/**
	 * Returns whether the {@link WSFallingBlock falling block} can be placed as a block.
	 *
	 * @return whether the {@link WSFallingBlock falling block} can be placed as a block.
	 */
	boolean canPlaceAsBlock();

	/**
	 * Sets whether the {@link WSFallingBlock falling block} can be placed as a block.
	 *
	 * @param canPlaceAsBlock whether the {@link WSFallingBlock falling block} can be placed as a block.
	 */
	void setCanPlaceAsBlock(boolean canPlaceAsBlock);

	/**
	 * Returns whether the {@link WSFallingBlock falling block} can be dropped as an {@link com.degoos.wetsponge.item.WSItemStack item}.
	 *
	 * @return whether the {@link WSFallingBlock falling block} can be dropped as an {@link com.degoos.wetsponge.item.WSItemStack item}.
	 */
	boolean canDropAsItem();

	/**
	 * Sets whether the {@link WSFallingBlock falling block} can be dropped as an {@link com.degoos.wetsponge.item.WSItemStack item}.
	 *
	 * @param canDropAsItem whether the {@link WSFallingBlock falling block} can be dropped as an {@link com.degoos.wetsponge.item.WSItemStack item}.
	 */
	void setCanDropAsItem(boolean canDropAsItem);

	/**
	 * Returns the fall time of the {@link WSFallingBlock falling block} in ticks.
	 *
	 * @return the fall time of the {@link WSFallingBlock falling block} in ticks.
	 */
	int getFallTime();

	/**
	 * Sets the fall time of the {@link WSFallingBlock falling block} in ticks.
	 *
	 * @param fallTime the fall time of the {@link WSFallingBlock falling block} in ticks.
	 */
	void setFallTime(int fallTime);

	/**
	 * Returns whether the {@link WSFallingBlock falling block} can hurt other {@link WSEntity entities}.
	 *
	 * @return whether the {@link WSFallingBlock falling block} can hurt other {@link WSEntity entities}.
	 */
	boolean canHurtEntities();

	/**
	 * Sets whether the {@link WSFallingBlock falling block} can hurt other {@link WSEntity entities}.
	 *
	 * @param canHurtEntities whether the {@link WSFallingBlock falling block} can hurt other {@link WSEntity entities}.
	 */
	void setCanHurtEntities(boolean canHurtEntities);
}

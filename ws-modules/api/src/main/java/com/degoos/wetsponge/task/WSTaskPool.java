package com.degoos.wetsponge.task;


import com.degoos.wetsponge.plugin.WSPlugin;
import com.degoos.wetsponge.plugin.WSPluginManager;
import com.degoos.wetsponge.util.Validate;

import java.util.*;

public class WSTaskPool {

    private static Map<WSPlugin, List<WSTask>> tasks = new HashMap<>();


    protected static boolean addTask(WSPlugin plugin, WSTask task) {
        Validate.notNull(plugin, "Plugin cannot be null!");
        Validate.notNull(task, "Task cannot be null!");
        if (!WSPluginManager.getInstance().getPlugins().contains(plugin) || !plugin.isEnabled()) return false;
        if (!tasks.containsKey(plugin)) tasks.put(plugin, new ArrayList<>());
        List<WSTask> pluginTasks = tasks.get(plugin);
        if (pluginTasks.contains(task)) return false;
        return pluginTasks.add(task);
    }


    protected static boolean removeTask(WSPlugin plugin, WSTask task) {
        Validate.notNull(plugin, "Plugin cannot be null!");
        Validate.notNull(task, "Task cannot be null!");
        if (!WSPluginManager.getInstance().getPlugins().contains(plugin) || !plugin.isEnabled()) return false;
        if (!tasks.containsKey(plugin)) return true;
        return tasks.get(plugin).remove(task);
    }


    public static boolean removeTasks(WSPlugin plugin) {
        Validate.notNull(plugin, "Plugin cannot be null!");
        if (tasks.containsKey(plugin)) {
            new HashSet<>(tasks.get(plugin)).forEach(WSTask::cancel);
            tasks.remove(plugin);
        }
        return true;
    }

}

package com.degoos.wetsponge.inventory.multiinventory;


import java.util.Arrays;

public enum InventoryRows {

    ONE(9, 18, 1), TWO(18, 27, 2), THREE(27, 36, 3), FOUR(36, 45, 4), FIVE(45, 54, 5);

    private final int slots, realSlots, rows;


    InventoryRows(int slots, int realSlots, int rows) {
        this.slots = slots;
        this.realSlots = realSlots;
        this.rows = rows;
    }


    public int getSlots() {
        return slots;
    }

    public int getRealSlots() {
        return realSlots;
    }


    public int getRows() {
        return rows;
    }


    public static InventoryRows getBySlots(int slots) {
        return Arrays.stream(values()).filter(row -> row.getSlots() == slots).findAny().orElse(FIVE);
    }

    public static InventoryRows getByRows(int rows) {
        return Arrays.stream(values()).filter(row -> row.getRows() == rows).findAny().orElse(FIVE);
    }

    public static InventoryRows getByRealSlots(int slots) {
        return Arrays.stream(values()).filter(row -> row.getRealSlots() == slots).findAny().orElse(FIVE);
    }
}

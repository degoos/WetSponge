package com.degoos.wetsponge.material.item.type;

public interface WSItemTypeDye extends WSItemTypeDyeColored {

	@Override
	WSItemTypeDye clone();
}

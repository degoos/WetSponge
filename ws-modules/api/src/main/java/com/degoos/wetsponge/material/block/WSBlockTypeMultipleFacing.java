package com.degoos.wetsponge.material.block;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;
import com.degoos.wetsponge.nbt.WSNBTTagList;
import com.degoos.wetsponge.nbt.WSNBTTagString;

import java.util.Set;

public interface WSBlockTypeMultipleFacing extends WSBlockType {

	boolean hasFace(EnumBlockFace face);

	void setFace(EnumBlockFace face, boolean enabled);

	Set<EnumBlockFace> getFaces();

	Set<EnumBlockFace> getAllowedFaces();

	@Override
	WSBlockTypeMultipleFacing clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		WSNBTTagList list = WSNBTTagList.of();
		getFaces().forEach(target -> list.appendTag(WSNBTTagString.of(target.name())));
		compound.setTag("faces", list);
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		getFaces().forEach(target -> setFace(target, false));
		WSNBTTagList list = compound.getTagList("faces", 8);
		for (int i = 0; i < list.tagCount(); i++)
			setFace(EnumBlockFace.valueOf(list.getStringAt(i)), true);
		return compound;
	}
}

package com.degoos.wetsponge.event;


import com.degoos.wetsponge.enums.EnumEventPriority;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface WSListener {

	EnumEventPriority priority() default EnumEventPriority.NORMAL;

	boolean ignoreCancelled() default false;

}

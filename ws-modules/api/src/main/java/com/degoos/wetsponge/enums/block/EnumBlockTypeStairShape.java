package com.degoos.wetsponge.enums.block;

public enum EnumBlockTypeStairShape {

	STRAIGHT,
	INNER_LEFT,
	INNER_RIGHT,
	OUTER_LEFT,
	OUTER_RIGHT
}

package com.degoos.wetsponge.event.block;

import com.degoos.wetsponge.block.WSBlockSnapshot;
import com.degoos.wetsponge.data.WSTransaction;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.world.WSLocation;

import java.util.Optional;

public class WSBlockPlaceEvent extends WSBlockChangeEvent {

    private Optional<WSPlayer> player;

    public WSBlockPlaceEvent(WSTransaction<WSBlockSnapshot> block, WSLocation location, Optional<WSPlayer> player) {
        super(block, location);
        this.player = player;
    }

    public Optional<WSPlayer> getPlayer() {
        return player;
    }
}

package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.material.block.WSBlockType;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSBlockTypeCake extends WSBlockType {

	int getBites();

	void setBites(int bites);

	int getMaximumBites();

	@Override
	WSBlockTypeCake clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setInteger("bites", getBites());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setBites(compound.getInteger("bites"));
		return compound;
	}
}

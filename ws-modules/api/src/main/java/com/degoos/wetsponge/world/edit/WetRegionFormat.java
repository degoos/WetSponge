package com.degoos.wetsponge.world.edit;

import com.degoos.wetsponge.util.Validate;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;

import java.io.File;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * WetSponge on 30/09/2017 by IhToN.
 */
public enum WetRegionFormat {
	WETSPONGE(new AbstractRegionFormat("WETSPONGE", "ws", "wet") {
	});

	private static final ConcurrentHashMap aliasMap = new ConcurrentHashMap(8, 0.9F, 1);
	private WSRegionFormat format;

	private WetRegionFormat() {
	}

	private WetRegionFormat(WSRegionFormat format) {
		this.format = format;
	}

	public Set<String> getAliases() {
		return this.format.getAliases();
	}

	public String getName() {
		return this.format.getName();
	}

	public boolean isFormat(File file) {
		return this.format.isFormat(file);
	}

	public static WetRegionFormat findByAlias(String alias) {
		Validate.notNull(alias);
		return (WetRegionFormat) aliasMap.get(alias.toLowerCase().trim());
	}

	public static WetRegionFormat findByFile(File file) {
		Validate.notNull(file);
		Iterator var1 = EnumSet.allOf(WetRegionFormat.class).iterator();

		WetRegionFormat format;
		do {
			if (!var1.hasNext()) {
				return null;
			}

			format = (WetRegionFormat) var1.next();
		} while (!format.isFormat(file));

		return format;
	}

	public static WetRegionFormat addFormat(WSRegionFormat instance) {
		WetRegionFormat newEnum = (WetRegionFormat) ReflectionUtils.addEnum(WetRegionFormat.class, instance.getName());
		newEnum.format = instance;
		Iterator var2 = newEnum.getAliases().iterator();

		while (var2.hasNext()) {
			String alias = (String) var2.next();
			aliasMap.put(alias.toLowerCase().trim(), newEnum);
		}

		return newEnum;
	}

	public static Class<?> inject() {
		return WetRegionFormat.class;
	}

	static {
		WetRegionFormat[] var0 = values();
		int var1 = var0.length;

		for (int var2 = 0; var2 < var1; ++var2) {
			WetRegionFormat emum = var0[var2];
			Iterator var4 = emum.getAliases().iterator();

			while (var4.hasNext()) {
				String alias = (String) var4.next();
				aliasMap.put(alias, emum);
			}
		}

	}
}

package com.degoos.wetsponge.event.world;

import com.degoos.wetsponge.event.WSCancellable;
import com.degoos.wetsponge.world.WSChunk;
import com.degoos.wetsponge.world.WSWorld;

public class WSChunkUnloadEvent extends WSChunkEvent implements WSCancellable {

	private boolean cancelled;

	public WSChunkUnloadEvent(WSWorld world, WSChunk chunk, boolean cancelled) {
		super(world, chunk);
		this.cancelled = cancelled;
	}

	@Override
	public boolean isCancelled() {
		return cancelled;
	}

	@Override
	public void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
	}
}

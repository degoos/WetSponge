package com.degoos.wetsponge.world;

import com.degoos.wetsponge.bridge.world.BridgeExplosion;
import com.degoos.wetsponge.entity.explosive.WSExplosive;

import java.util.Optional;

public interface WSExplosion extends WSLocatable {

	public static Builder builder() {
		return BridgeExplosion.builder();
	}

	/**
	 * Returns the {@link WSExplosive explosion source} of the {@link WSExplosion explosion}, if present.
	 *
	 * @return the {@link WSExplosive explosion source} of the {@link WSExplosion explosion}, if present.
	 */
	Optional<WSExplosive> getSourceExplosive();

	/**
	 * Returns the radius of the {@link WSExplosion explosion}.
	 *
	 * @return the radius.
	 */
	float getRadius();

	/**
	 * Returns whether the {@link WSExplosion explosion} will cause fire.
	 *
	 * @return whether the {@link WSExplosion explosion} will cause fire.
	 */
	boolean canCauseFire();

	/**
	 * Returns whether the {@link WSExplosion explosion} should play smoke.
	 * In {@link com.degoos.wetsponge.enums.EnumServerType#SPIGOT Spigot} this is always true.
	 *
	 * @return whether the {@link WSExplosion explosion} should play smoke.
	 */
	boolean shouldPlaySmoke();

	/**
	 * Returns whether the {@link WSExplosion explosion} should break blocks.
	 *
	 * @return whether the {@link WSExplosion explosion} should break blocks.
	 */
	boolean shouldBreakBlocks();

	/**
	 * Returns whether the {@link WSExplosion explosion} should damage entities.
	 * In {@link com.degoos.wetsponge.enums.EnumServerType#SPIGOT Spigot} this is always true.
	 *
	 * @return whether the {@link WSExplosion explosion} should damage entities.
	 */
	boolean shouldDamageEntities();

	/**
	 * Returns a new {@link Builder builder} with all the data of this {@link WSExplosion explosion}.
	 *
	 * @return a new {@link Builder builder}.
	 */
	Builder toBuilder();

	/**
	 * Returns the handled object.
	 *
	 * @return the handled object.
	 */
	Object getHandler();

	interface Builder {

		/**
		 * Sets the {@link WSLocation location} of the {@link WSExplosion explosion}.
		 *
		 * @param location the {@link WSLocation location}.
		 * @return the builder.
		 */
		WSExplosion.Builder location(WSLocation location);

		/**
		 * Sets the {@link WSExplosive explosion source} of the {@link WSExplosion explosion}, or null.
		 *
		 * @param explosive the {@link WSExplosive explosion source}, or null.
		 * @return the builder.
		 */
		WSExplosion.Builder sourceExplosive(WSExplosive explosive);

		/**
		 * Sets the radius of the {@link WSExplosion explosion}.
		 *
		 * @param radius the radius.
		 * @return the builder.
		 */
		WSExplosion.Builder radius(float radius);

		/**
		 * Sets whether the {@link WSExplosion explosion} can cause fire.
		 *
		 * @param canCauseFire the boolean.
		 * @return the builder.
		 */
		WSExplosion.Builder canCauseFire(boolean canCauseFire);

		/**
		 * Sets whether the {@link WSExplosion explosion} should damage entities.
		 * In {@link com.degoos.wetsponge.enums.EnumServerType#SPIGOT Spigot} this is always true.
		 *
		 * @param shouldDamageEntities the boolean.
		 * @return the builder.
		 */
		WSExplosion.Builder shouldDamageEntities(boolean shouldDamageEntities);

		/**
		 * Sets whether the {@link WSExplosion explosion} should play smoke.
		 * In {@link com.degoos.wetsponge.enums.EnumServerType#SPIGOT Spigot} this is always true.
		 *
		 * @param shouldPlaySmoke the boolean.
		 * @return the builder.
		 */
		WSExplosion.Builder shouldPlaySmoke(boolean shouldPlaySmoke);

		/**
		 * Sets whether the {@link WSExplosion explosion} should break blocks.
		 *
		 * @param shouldBreakBlocks the boolean.
		 * @return the builder.
		 */
		WSExplosion.Builder shouldBreakBlocks(boolean shouldBreakBlocks);

		/**
		 * Creates the {@link WSExplosion explosion}, based on the builder.
		 *
		 * @return the {@link WSExplosion explosion}.
		 */
		WSExplosion build();
	}

}

package com.degoos.wetsponge.enums.block;


import java.util.Arrays;
import java.util.Optional;

public enum EnumBlockTypeDirtType {

	DIRT(0), COARSE_DIRT(1), PODZOL(2);

	private int value;


	EnumBlockTypeDirtType(int value) {
		this.value = value;
	}


	public static Optional<EnumBlockTypeDirtType> getByValue (int value) {
		return Arrays.stream(values()).filter(dirtType -> dirtType.getValue() == value).findAny();
	}

	public static Optional<EnumBlockTypeDirtType> getByName (String name) {
		return Arrays.stream(values()).filter(dirtType -> dirtType.name().equalsIgnoreCase(name)).findAny();
	}


	public int getValue () {
		return value;
	}
}

package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.WSBlockTypeAgeable;
import com.degoos.wetsponge.material.block.WSBlockTypeMultipleFacing;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;
import com.degoos.wetsponge.nbt.WSNBTTagList;
import com.degoos.wetsponge.nbt.WSNBTTagString;

public interface WSBlockTypeFire extends WSBlockTypeAgeable, WSBlockTypeMultipleFacing {

	@Override
	WSBlockTypeFire clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setInteger("age", getAge());
		WSNBTTagList list = WSNBTTagList.of();
		getFaces().forEach(target -> list.appendTag(WSNBTTagString.of(target.name())));
		compound.setTag("faces", list);
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setAge(compound.getInteger("age"));
		getFaces().forEach(target -> setFace(target, false));
		WSNBTTagList list = compound.getTagList("faces", 8);
		for (int i = 0; i < list.tagCount(); i++)
			setFace(EnumBlockFace.valueOf(list.getStringAt(i)), true);
		return compound;
	}
}

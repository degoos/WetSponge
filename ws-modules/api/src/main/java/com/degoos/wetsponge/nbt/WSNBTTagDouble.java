package com.degoos.wetsponge.nbt;

import com.degoos.wetsponge.bridge.nbt.BridgeNBT;

public interface WSNBTTagDouble extends WSNBTPrimitive {

	public static WSNBTTagDouble of(double d) {
		return BridgeNBT.ofDouble(d);
	}

	@Override
	WSNBTTagDouble copy();
}

package com.degoos.wetsponge.enums.block;

public enum EnumBlockTypeRailShape {

	NORTH_SOUTH,
	EAST_WEST,
	ASCENDING_EAST,
	ASCENDING_WEST,
	ASCENDING_NORTH,
	ASCENDING_SOUTH,
	SOUTH_EAST,
	SOUTH_WEST,
	NORTH_WEST,
	NORTH_EAST

}

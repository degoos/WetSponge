package com.degoos.wetsponge.entity.living.animal;


import com.degoos.wetsponge.enums.EnumDyeColor;

import java.util.Optional;

/**
 * Represents a Sheep.
 */
public interface WSSheep extends WSAnimal {

	/**
	 * @return the {@link EnumDyeColor} of the sheep.
	 */
	Optional<EnumDyeColor> getColor();

	/**
	 * Sets an {@link EnumDyeColor} to the wool of the sheep.
	 * @param color the color.
	 */
	void setColor(EnumDyeColor color);

	/**
	 * @return true if the sheep is sheared.
	 */
	boolean isSheared();

	/**
	 * Sets the sheep sheared if the boolean is true.
	 * @param sheared the boolean.
	 */
	void setSheared(boolean sheared);

}

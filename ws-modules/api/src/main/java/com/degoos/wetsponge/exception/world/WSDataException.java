package com.degoos.wetsponge.exception.world;

/**
 * Thrown when there is an exception related to data handling.
 */

public class WSDataException extends Exception {

	public WSDataException(String msg) {
		super(msg);
	}

	public WSDataException() {
		super();
	}

}

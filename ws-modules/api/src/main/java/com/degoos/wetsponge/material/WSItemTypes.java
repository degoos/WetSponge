package com.degoos.wetsponge.material;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.block.tileentity.extra.WSBannerPattern;
import com.degoos.wetsponge.bridge.material.BridgeItemType;
import com.degoos.wetsponge.enums.EnumDyeColor;
import com.degoos.wetsponge.enums.EnumEntityType;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.enums.block.EnumBlockTypeSkullType;
import com.degoos.wetsponge.enums.item.*;
import com.degoos.wetsponge.firework.WSFireworkEffect;
import com.degoos.wetsponge.material.item.WSItemType;
import com.degoos.wetsponge.material.item.type.*;
import com.degoos.wetsponge.text.WSText;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;

public enum WSItemTypes {

	IRON_SHOVEL(256, "minecraft:iron_shovel", WSItemTypeDamageable.class, 0, 250),
	IRON_PICKAXE(257, "minecraft:iron_pickaxe", WSItemTypeDamageable.class, 0, 250),
	IRON_AXE(258, "minecraft:iron_axe", WSItemTypeDamageable.class, 0, 250),
	FLINT_AND_STEEL(259, "minecraft:flint_and_steel", WSItemTypeDamageable.class, 0, 64),
	APPLE(260, "minecraft:apple", 64),
	BOW(261, "minecraft:bow", WSItemTypeDamageable.class, 0, 384),
	ARROW(262, "minecraft:arrow", 64),
	COAL(WSItemTypeCoal.class, EnumItemTypeCoalType.COAL),
	CHARCOAL(WSItemTypeCoal.class, EnumItemTypeCoalType.CHARCOAL),
	DIAMOND(264, "minecraft:diamond", 64),
	IRON_INGOT(265, "minecraft:iron_ingot", 64),
	GOLD_INGOT(266, "minecraft:gold_ingot", 64),
	IRON_SWORD(267, "minecraft:iron_sword", WSItemTypeDamageable.class, 0, 251),
	WOODEN_SWORD(268, "minecraft:wooden_sword", WSItemTypeDamageable.class, 0, 60),
	WOODEN_SHOVEL(269, "minecraft:wooden_shovel", WSItemTypeDamageable.class, 0, 60),
	WOODEN_PICKAXE(270, "minecraft:wooden_pickaxe", WSItemTypeDamageable.class, 0, 60),
	WOODEN_AXE(271, "minecraft:wooden_axe", WSItemTypeDamageable.class, 0, 60),
	STONE_SWORD(272, "minecraft:stone_sword", WSItemTypeDamageable.class, 0, 132),
	STONE_SHOVEL(273, "minecraft:stone_shovel", WSItemTypeDamageable.class, 0, 132),
	STONE_PICKAXE(274, "minecraft:stone_pickaxe", WSItemTypeDamageable.class, 0, 132),
	STONE_AXE(275, "minecraft:stone_axe", WSItemTypeDamageable.class, 0, 132),
	DIAMOND_SWORD(276, "minecraft:diamond_sword", WSItemTypeDamageable.class, 0, 1562),
	DIAMOND_SHOVEL(277, "minecraft:diamond_shovel", WSItemTypeDamageable.class, 0, 1562),
	DIAMOND_PICKAXE(278, "minecraft:diamond_pickaxe", WSItemTypeDamageable.class, 0, 1562),
	DIAMOND_AXE(279, "minecraft:diamond_axe", WSItemTypeDamageable.class, 0, 1562),
	STICK(280, "minecraft:stick", 64),
	BOWL(281, "minecraft:bowl", 64),
	MUSHROOM_STEW(282, "minecraft:mushroom_stew", 64),
	GOLD_SWORD(283, "minecraft:golden_sword", WSItemTypeDamageable.class, 0, 33),
	GOLD_SHOVEL(284, "minecraft:golden_shovel", WSItemTypeDamageable.class, 0, 33),
	GOLD_PICKAXE(285, "minecraft:golden_pickaxe", WSItemTypeDamageable.class, 0, 33),
	GOLD_AXE(286, "minecraft:golden_axe", WSItemTypeDamageable.class, 0, 33),
	STRING(287, "minecraft:string", 64),
	FEATHER(288, "minecraft:feather", 64),
	GUNPOWDER(289, "minecraft:gunpowder", 64),
	WOODEN_HOE(290, "minecraft:wooden_hoe", WSItemTypeDamageable.class, 0, 60),
	STONE_HOE(291, "minecraft:stone_hoe", WSItemTypeDamageable.class, 0, 132),
	IRON_HOE(292, "minecraft:iron_hoe", WSItemTypeDamageable.class, 0, 251),
	DIAMOND_HOE(293, "minecraft:diamond_hoe", WSItemTypeDamageable.class, 0, 1562),
	GOLD_HOE(294, "minecraft:golden_hoe", WSItemTypeDamageable.class, 0, 33),
	WHEAT_SEEDS(295, "minecraft:wheat_seeds", 64),
	WHEAT(296, "minecraft:wheat", 64),
	BREAD(297, "minecraft:bread", 64),
	LEATHER_HELMET(298, "minecraft:leather_helmet", WSItemTypeLeatherArmor.class, 0, 56, null),
	LEATHER_CHESTPLATE(299, "minecraft:leather_chestplate", WSItemTypeLeatherArmor.class, 0, 81, null),
	LEATHER_LEGGINGS(300, "minecraft:leather_leggings", WSItemTypeLeatherArmor.class, 0, 76, null),
	LEATHER_BOOTS(301, "minecraft:leather_boots", WSItemTypeLeatherArmor.class, 0, 66, null),
	CHAINMAIL_HELMET(302, "minecraft:chainmail_helmet", 166),
	CHAINMAIL_CHESTPLATE(303, "minecraft:chainmail_chestplate", WSItemTypeDamageable.class, 0, 241),
	CHAINMAIL_LEGGINGS(304, "minecraft:chainmail_leggings", WSItemTypeDamageable.class, 0, 226),
	CHAINMAIL_BOOTS(305, "minecraft:chainmail_boots", WSItemTypeDamageable.class, 0, 196),
	IRON_HELMET(306, "minecraft:iron_helmet", WSItemTypeDamageable.class, 0, 166),
	IRON_CHESTPLATE(307, "minecraft:iron_chestplate", WSItemTypeDamageable.class, 0, 241),
	IRON_LEGGINGS(308, "minecraft:iron_leggings", WSItemTypeDamageable.class, 0, 226),
	IRON_BOOTS(309, "minecraft:iron_boots", WSItemTypeDamageable.class, 0, 196),
	DIAMOND_HELMET(310, "minecraft:diamond_helmet", WSItemTypeDamageable.class, 0, 364),
	DIAMOND_CHESTPLATE(311, "minecraft:diamond_chestplate", WSItemTypeDamageable.class, 0, 529),
	DIAMOND_LEGGINGS(312, "minecraft:diamond_leggings", WSItemTypeDamageable.class, 0, 496),
	DIAMOND_BOOTS(313, "minecraft:diamond_boots", WSItemTypeDamageable.class, 0, 430),
	GOLD_HELMET(314, "minecraft:golden_helmet", WSItemTypeDamageable.class, 0, 78),
	GOLD_CHESTPLATE(315, "minecraft:golden_chestplate", WSItemTypeDamageable.class, 0, 113),
	GOLD_LEGGINGS(316, "minecraft:golden_leggings", WSItemTypeDamageable.class, 0, 106),
	GOLD_BOOTS(317, "minecraft:golden_boots", WSItemTypeDamageable.class, 0, 92),
	FLINT(318, "minecraft:flint", 64),
	RAW_PORKCHOP(319, "minecraft:porkchop", 64),
	COOKED_PORKCHOP(320, "minecraft:cooked_porkchop", 64),
	PAINTING(321, "minecraft:painting", 64),
	GOLDEN_APPLE(WSItemTypeGoldenApple.class, EnumItemTypeGoldenAppleType.GOLDEN_APPLE),
	ENCHANTED_GOLDEN_APPLE(WSItemTypeGoldenApple.class, EnumItemTypeGoldenAppleType.GOLDEN_APPLE),
	SIGN(323, "minecraft:sign", 64),
	WOODEN_DOOR(324, "minecraft:wooden_door", 64),
	BUCKET(325, "minecraft:bucket", 16),
	WATER_BUCKET(326, "minecraft:water_bucket", 1),
	LAVA_BUCKET(327, "minecraft:lava_bucket", 1),
	MINECART(328, "minecraft:minecart", 1),
	SADDLE(329, "minecraft:saddle", 1),
	IRON_DOOR(330, "minecraft:iron_door", 64),
	REDSTONE(331, "minecraft:redstone", 64),
	SNOWBALL(332, "minecraft:snowball", 16),
	BOAT(333, "minecraft:boat", "minecraft:oak_boat", 1),
	LEATHER(334, "minecraft:leather", 64),
	MILK_BUCKET(335, "minecraft:milk_bucket", 1),
	BRICK(336, "minecraft:brick", 64),
	CLAY_BALL(337, "minecraft:clay_ball", 64),
	SUGAR_CANE(338, "minecraft:reeds", 64),
	PAPER(339, "minecraft:paper", 64),
	BOOK(340, "minecraft:book", 64),
	SLIME_BALL(341, "minecraft:slime_ball", 64),
	MINECART_WITH_CHEST(342, "minecraft:chest_minecart", 1),
	MINECART_WITH_FURNACE(343, "minecart:furnace_minecart", 1),
	EGG(344, "minecraft:egg", 16),
	COMPASS(345, "minecraft:compass", 1),
	FISHING_ROD(346, "minecraft:fishing_rod", 65),
	CLOCK(347, "minecraft:clock", 1),
	GLOWSTONE_DUST(348, "minecraft:glowstone_dust", 64),
	COD(WSItemTypeFish.class, EnumItemTypeFishType.COD),
	SALMON(WSItemTypeFish.class, EnumItemTypeFishType.SALMON),
	TROPICAL_FISH(WSItemTypeFish.class, EnumItemTypeFishType.TROPICAL_FISH),
	PUFFERFISH(WSItemTypeFish.class, EnumItemTypeFishType.PUFFERFISH),
	COOKED_COD(WSItemTypeCookedFish.class, EnumItemTypeCookedFishType.COD),
	COOKED_SALMON(WSItemTypeCookedFish.class, EnumItemTypeCookedFishType.SALMON),
	BONE_MEAL(WSItemTypeDye.class, EnumDyeColor.WHITE),
	ORANGE_DYE(WSItemTypeDye.class, EnumDyeColor.ORANGE),
	MAGENTA_DYE(WSItemTypeDye.class, EnumDyeColor.MAGENTA),
	LIGHT_BLUE_DYE(WSItemTypeDye.class, EnumDyeColor.LIGHT_BLUE),
	DANDELION_YELLOW(WSItemTypeDye.class, EnumDyeColor.YELLOW),
	LIME_DYE(WSItemTypeDye.class, EnumDyeColor.LIME),
	PINK_DYE(WSItemTypeDye.class, EnumDyeColor.PINK),
	GRAY_DYE(WSItemTypeDye.class, EnumDyeColor.GRAY),
	LIGHT_GRAY_DYE(WSItemTypeDye.class, EnumDyeColor.LIGHT_GRAY),
	CYAN_DYE(WSItemTypeDye.class, EnumDyeColor.CYAN),
	LAPIS_LAZULI(WSItemTypeDye.class, EnumDyeColor.BLUE),
	COCOA_BEANS(WSItemTypeDye.class, EnumDyeColor.BROWN),
	CACTUS_GREEN(WSItemTypeDye.class, EnumDyeColor.GREEN),
	ROSE_RED(WSItemTypeDye.class, EnumDyeColor.RED),
	INK_SAC(WSItemTypeDye.class, EnumDyeColor.BLACK),
	BONE(352, "minecraft:bone", 64),
	SUGAR(353, "minecraft:sugar", 64),
	CAKE(354, "minecraft:cake", 64),
	WHITE_BED(355, "minecraft:bed", "bed", 1, WSItemTypeDyeColored.class, EnumDyeColor.WHITE),
	ORANGE_BED(355, "minecraft:bed", "bed", 1, WSItemTypeDyeColored.class, EnumDyeColor.ORANGE),
	MAGENTA_BED(355, "minecraft:bed", "bed", 1, WSItemTypeDyeColored.class, EnumDyeColor.MAGENTA),
	LIGHT_BLUE_BED(355, "minecraft:bed", "bed", 1, WSItemTypeDyeColored.class, EnumDyeColor.LIGHT_BLUE),
	YELLOW_BED(355, "minecraft:bed", "bed", 1, WSItemTypeDyeColored.class, EnumDyeColor.YELLOW),
	LIME_BED(355, "minecraft:bed", "bed", 1, WSItemTypeDyeColored.class, EnumDyeColor.LIME),
	PINK_BED(355, "minecraft:bed", "bed", 1, WSItemTypeDyeColored.class, EnumDyeColor.PINK),
	GRAY_BED(355, "minecraft:bed", "bed", 1, WSItemTypeDyeColored.class, EnumDyeColor.GRAY),
	LIGHT_GRAY_BED(355, "minecraft:bed", "bed", 1, WSItemTypeDyeColored.class, EnumDyeColor.LIGHT_GRAY),
	CYAN_BED(355, "minecraft:bed", "bed", 1, WSItemTypeDyeColored.class, EnumDyeColor.CYAN),
	PURPLE_BED(355, "minecraft:bed", "bed", 1, WSItemTypeDyeColored.class, EnumDyeColor.PURPLE),
	BLUE_BED(355, "minecraft:bed", "bed", 1, WSItemTypeDyeColored.class, EnumDyeColor.BLUE),
	BROWN_BED(355, "minecraft:bed", "bed", 1, WSItemTypeDyeColored.class, EnumDyeColor.BROWN),
	GREEN_BED(355, "minecraft:bed", "bed", 1, WSItemTypeDyeColored.class, EnumDyeColor.GREEN),
	RED_BED(355, "minecraft:bed", "bed", 1, WSItemTypeDyeColored.class, EnumDyeColor.RED),
	BLACK_BED(355, "minecraft:bed", "bed", 1, WSItemTypeDyeColored.class, EnumDyeColor.BLACK),
	REDSTONE_REPEATER(356, "minecraft:repeater", 64),
	COOKIE(357, "minecraft:cookie", 64),
	FILLED_MAP(WSItemTypeMap.class, 0, null),
	SHEARS(359, "minecraft:shears", 1),
	MELON(360, "minecraft:melon", "minecraft:melon_slice", 64),
	PUMPKIN_SEEDS(361, "minecraft:pumpkin_seeds", 64),
	MELON_SEEDS(362, "minecraft:melon_seeds", 64),
	RAW_BEEF(363, "minecraft:beef", 64),
	COOKED_BEEF(364, "minecraft:cooked_beef", 64),
	RAW_CHICKEN(365, "minecraft:chicken", 64),
	COOKED_CHICKEN(366, "minecraft:cooked_chicken", 64),
	ROTTEN_FLESH(367, "minecraft:rotten_flesh", 64),
	ENDER_PEARL(368, "minecraft:ender_pearl", 16),
	BLAZE_ROD(369, "minecraft:blaze_rod", 64),
	GHAST_TEAR(370, "minecraft:ghast_tear", 64),
	GOLD_NUGGET(371, "minecraft:gold_nugget", 64),
	NETHER_WARTS(372, "minecraft:nether_wart", 64),
	GLASS_BOTTLE(374, "minecraft:glass_bottle", 64),
	SPIDER_EYE(375, "minecraft:spider_eye", 64),
	FERMENTED_SPIDER_EYE(376, "minecraft:fermented_spider_eye", 64),
	BLAZE_POWDER(377, "minecraft:blaze_powder", 64),
	MAGMA_CREAM(378, "minecraft:magma_cream", 64),
	BREWING_STAND(379, "minecraft:brewing_stand", 64),
	CAULDRON(380, "minecraft:cauldron", 64),
	ENDER_EYE(381, "minecraft:ender_eye", 64),
	SPECKLED_MELON(382, "minecraft:speckled_melon", "minecraft:glistering_melon_slice", 64),
	SPAWN_EGG(WSItemTypeSpawnEgg.class, EnumEntityType.PIG),
	BAT_SPAWN_EGG(WSItemTypeSpawnEgg.class, EnumEntityType.BAT),
	BLAZE_SPAWN_EGG(WSItemTypeSpawnEgg.class, EnumEntityType.BLAZE),
	CAVE_SPIDER_SPAWN_EGG(WSItemTypeSpawnEgg.class, EnumEntityType.CAVE_SPIDER),
	CHICKEN_SPAWN_EGG(WSItemTypeSpawnEgg.class, EnumEntityType.CHICKEN),
	COW_SPAWN_EGG(WSItemTypeSpawnEgg.class, EnumEntityType.COW),
	CREEPER_SPAWN_EGG(WSItemTypeSpawnEgg.class, EnumEntityType.CREEPER),
	DONKEY_SPAWN_EGG(WSItemTypeSpawnEgg.class, EnumEntityType.DONKEY),
	ELDER_GUARDIAN_SPAWN_EGG(WSItemTypeSpawnEgg.class, EnumEntityType.ELDER_GUARDIAN),
	ENDERMAN_SPAWN_EGG(WSItemTypeSpawnEgg.class, EnumEntityType.ENDERMAN),
	ENDERMITE_SPAWN_EGG(WSItemTypeSpawnEgg.class, EnumEntityType.ENDERMITE),
	EVOCATION_ILLAGER_SPAWN_EGG(WSItemTypeSpawnEgg.class, EnumEntityType.EVOCATION_ILLAGER),
	GHAST_SPAWN_EGG(WSItemTypeSpawnEgg.class, EnumEntityType.GHAST),
	GUARDIAN_SPAWN_EGG(WSItemTypeSpawnEgg.class, EnumEntityType.GUARDIAN),
	HORSE_SPAWN_EGG(WSItemTypeSpawnEgg.class, EnumEntityType.HORSE),
	HUSK_SPAWN_EGG(WSItemTypeSpawnEgg.class, EnumEntityType.HUSK),
	LLAMA_SPAWN_EGG(WSItemTypeSpawnEgg.class, EnumEntityType.LLAMA),
	MAGMA_CUBE_SPAWN_EGG(WSItemTypeSpawnEgg.class, EnumEntityType.MAGMA_CUBE),
	MUSHROOM_COW_SPAWN_EGG(WSItemTypeSpawnEgg.class, EnumEntityType.MUSHROOM_COW),
	MULE_SPAWN_EGG(WSItemTypeSpawnEgg.class, EnumEntityType.MULE),
	OCELOT_SPAWN_EGG(WSItemTypeSpawnEgg.class, EnumEntityType.OCELOT),
	//PARROT_SPAWN_EGG(WSItemTypeSpawnEgg.class, EnumEntityType.PARROT),
	PIG_SPAWN_EGG(WSItemTypeSpawnEgg.class, EnumEntityType.PIG),
	POLAR_BEAR_SPAWN_EGG(WSItemTypeSpawnEgg.class, EnumEntityType.POLAR_BEAR),
	RABBIT_SPAWN_EGG(WSItemTypeSpawnEgg.class, EnumEntityType.RABBIT),
	SHEEP_SPAWN_EGG(WSItemTypeSpawnEgg.class, EnumEntityType.SHEEP),
	SHULKER_SPAWN_EGG(WSItemTypeSpawnEgg.class, EnumEntityType.SHULKER),
	SILVERFISH_SPAWN_EGG(WSItemTypeSpawnEgg.class, EnumEntityType.SILVERFISH),
	SKELETON_SPAWN_EGG(WSItemTypeSpawnEgg.class, EnumEntityType.SKELETON),
	SKELETON_HORSE_SPAWN_EGG(WSItemTypeSpawnEgg.class, EnumEntityType.SKELETON_HORSE),
	SLIME_SPAWN_EGG(WSItemTypeSpawnEgg.class, EnumEntityType.SLIME),
	SPIDER_SPAWN_EGG(WSItemTypeSpawnEgg.class, EnumEntityType.SPIDER),
	SQUID_SPAWN_EGG(WSItemTypeSpawnEgg.class, EnumEntityType.SQUID),
	STRAY_SPAWN_EGG(WSItemTypeSpawnEgg.class, EnumEntityType.STRAY),
	VEX_SPAWN_EGG(WSItemTypeSpawnEgg.class, EnumEntityType.VEX),
	VILLAGER_SPAWN_EGG(WSItemTypeSpawnEgg.class, EnumEntityType.VILLAGER),
	VINDICATION_ILLAGER_SPAWN_EGG(WSItemTypeSpawnEgg.class, EnumEntityType.VINDICATION_ILLAGER),
	WITCH_SPAWN_EGG(WSItemTypeSpawnEgg.class, EnumEntityType.WITCH),
	WITHER_SKELETON_SPAWN_EGG(WSItemTypeSpawnEgg.class, EnumEntityType.WITHER_SKELETON),
	WOLF_SPAWN_EGG(WSItemTypeSpawnEgg.class, EnumEntityType.WOLF),
	ZOMBIE_SPAWN_EGG(WSItemTypeSpawnEgg.class, EnumEntityType.ZOMBIE),
	ZOMBIE_HORSE_SPAWN_EGG(WSItemTypeSpawnEgg.class, EnumEntityType.ZOMBIE_HORSE),
	PIG_ZOMBIE_SPAWN_EGG(WSItemTypeSpawnEgg.class, EnumEntityType.PIG_ZOMBIE),
	ZOMBIE_VILLAGER_SPAWN_EGG(WSItemTypeSpawnEgg.class, EnumEntityType.ZOMBIE_VILLAGER),

	EXP_BOTTLE(384, "minecraft:experience_bottle", 64),
	FIRE_CHARGE(385, "minecraft:fire_charge", 64),
	WRITABLE_BOOK(386, "minecraft:writable_book", "minecraft:writable_book", WSItemTypeWrittenBook.class, WSText.empty(), WSText
			.empty(), new ArrayList<>(), EnumItemTypeBookGeneration.ORIGINAL),
	WRITTEN_BOOK(387, "minecraft:written_book", "minecraft:written_book", WSItemTypeWrittenBook.class, WSText.empty(), WSText
			.empty(), new ArrayList<>(), EnumItemTypeBookGeneration.ORIGINAL),
	EMERALD(388, "minecraft:emerald", 64),
	ITEM_FRAME(389, "minecraft:item_frame", 64),
	FLOWER_POT(390, "minecraft:flower_pot", 64),
	CARROT(391, "minecraft:carrot", 64),
	POTATO(392, "minecraft:potato", 64),
	BAKED_POTATO(393, "minecraft:baked_potato", 64),
	POISONOUS_POTATO(394, "minecraft:poisonous_potato", 64),
	EMPTY_MAP(395, "minecraft:map", 64),
	GOLDEN_CARROT(396, "minecraft:golden_carrot", 64),
	SKULL(WSItemTypeSkull.class, null, EnumBlockTypeSkullType.SKELETON),
	CARROT_STICK(398, "minecraft:carrot_on_a_stick", 64),
	NETHER_STAR(399, "minecraft:nether_star", 64),
	PUMPKIN_PIE(400, "minecraft:pumpkin_pie", 64),
	FIREWORK(WSItemTypeFirework.class, 0, new ArrayList<WSFireworkEffect>()),
	FIREWORK_CHARGE(WSItemTypeFireworkCharge.class, (WSFireworkEffect) null),
	ENCHANTED_BOOK(403, "minecraft:enchanted_book", 1),
	REDSTONE_COMPARATOR(404, "minecraft:redstone_comparator", 64),
	NETHER_BRICK_ITEM(405, "minecraft:netherbrick", "minecraft:nether_brick", 64),
	QUARTZ(406, "minecraft:quartz", 64),
	MINECART_WITH_TNT(407, "minecraft:tnt_minecart", 64),
	MINECART_WITH_HOPPER(408, "minecraft:hopper_minecart", 64),
	PRISMARINE_SHARD(409, "minecraft:prismarine_shard", 64),
	PRISMARINE_CRISTALS(410, "minecraft:prismarine_cristals", 64),
	RAW_RABBIT(411, "minecraft:rabbit", 64),
	COOKED_RABBIT(412, "minecraft:cooked_rabbit", 64),
	RABBIT_STEW(413, "minecraft:rabbit_stew", 64),
	RABBIT_FOOT(414, "minecraft:rabbit_foot", 64),
	RABBIT_HIDE(415, "minecraft:rabbit_hide", 64),
	ARMOR_STAND(416, "minecraft:armor_stand", 64),
	IRON_HORSE_ARMOR(417, "minecraft:iron_horse_armor", 64),
	GOLD_HORSE_ARMOR(418, "minecraft:golden_horse_armor", 64),
	DIAMOND_HORSE_ARMOR(419, "minecraft:diamond_horse_armor", 64),
	LEAD(420, "minecraft:lead", 64),
	NAME_TAG(421, "minecraft:name_tag", 64),
	MINECART_WITH_COMMAND_BLOCK(422, "minecraft:command_block_minecart", 64),
	RAW_MUTTON(423, "minecraft:mutton", 64),
	COOKED_MUTTON(424, "minecraft:cooked_mutton", 64),
	BANNER(WSItemTypeBanner.class, EnumDyeColor.BLACK, new ArrayList<WSBannerPattern>()),
	END_CRYSTAL(426, "minecraft:end_crystal", 64),
	WOODEN_DOOR_SPRUCE(427, "minecraft:spruce_door", 64),
	WOODEN_DOOR_BIRCH(428, "minecraft:birch_door", 64),
	WOODEN_DOOR_JUNGLE(429, "minecraft:jungle_door", 64),
	WOODEN_DOOR_ACACIA(430, "minecraft:acacia_door", 64),
	WOODEN_DOOR_DARK_OAK(431, "minecraft:dark_oak_door", 64),
	CHORUS_FRUIT(432, "minecraft:chorus_fruit", 64),
	CHORUS_FRUIT_POPPED(433, "minecraft:chorus_fruit_popped", "minecraft:popped_chorus_fruit", 64),
	BEETROOT(434, "minecraft:beetroot", 64),
	BEETROOT_SEEDS(435, "minecraft:beetroot_seeds", 64),
	BEETROOT_SOUP(436, "minecraft:beetroot_soup", 64),
	DRAGON_BREATH(437, "minecraft:dragon_breath", 64),
	ARROW_SPECTRAL(439, "minecraft:spectral_arrow", 64),
	SHIELD(442, "minecraft:", 64),
	ELYTRA(443, "minecraft:elytra", 64),
	TOTEM(449, "minecraft:totem_of_undying", 64),
	SHULKER_SHELL(450, "minecraft:shulker_shell", 64),
	RECORD_13(2256, "minecraft:record_13", "minecraft:music_disc_13", 64),
	RECORD_CAT(2257, "minecraft:record_cat", "minecraft:music_disc_cat", 64),
	RECORD_BLOCKS(2258, "minecraft:record_blocks", "minecraft:music_disc_blocks", 64),
	RECORD_CHIRP(2259, "minecraft:record_chirp", "minecraft:music_disc_chirp", 64),
	RECORD_FAR(2260, "minecraft:record_far", "minecraft:music_disc_far", 64),
	RECORD_MALL(2261, "minecraft:record_mall", "minecraft:music_disc_mall", 64),
	RECORD_MELLOHI(2262, "minecraft:record_mellohi", "minecraft:music_disc_mellohi", 64),
	RECORD_STAL(2263, "minecraft:record_stal", "minecraft:music_disc_stal", 64),
	RECORD_STRAD(2264, "minecraft:record_strad", "minecraft:music_disc_strad", 64),
	RECORD_WARD(2265, "minecraft:record_ward", "minecraft:music_disc_ward", 64),
	RECORD_11(2266, "minecraft:record_11", "minecraft:music_disc_11", 64),
	RECORD_WAIT(2267, "minecraft:record_wait", "minecraft:music_disc_wait", 64),
	//MINECRAFT 1.13 ITEMS
	DEBUG_STICK(-1, "minecraft:debug_stick", 1),
	KELP(-1, "minecraft:kelp", 64),
	DRIED_KELP(-1, "minecraft:dried_kelp", 64),
	COD_BUCKET(WSItemTypeFishBucket.class, EnumItemTypeFishType.COD),
	SALMON_BUCKET(WSItemTypeFishBucket.class, EnumItemTypeFishType.SALMON),
	PUFFERFISH_BUCKET(WSItemTypeFishBucket.class, EnumItemTypeFishType.PUFFERFISH),
	TROPICAL_FISH_BUCKET(WSItemTypeFishBucket.class, EnumItemTypeFishType.TROPICAL_FISH),
	HEARTH_OF_THE_SEA(-1, "minecraft:heart_of_the_sea", 64),
	NAUTILUS_SHELL(-1, "minecraft:nautilus_shell", 64),
	PHANTOM_MEMBRANE(-1, "minecraft:phantom_membrane", 64),
	SCUTE(-1, "minecraft:scute", 64),
	TRIDENT(-1, "minecraft:trident", WSItemTypeDamageable.class, 0, 251),
	TURTLE_SHELL(-1, "minecraft:turtle_helmet", WSItemTypeDamageable.class, 0, 276);


	private Class<? extends WSItemType> materialClass;
	private WSItemType defaultState;

	WSItemTypes(int numericalId, String oldStringId, String newStringId, int maxStackSize, Class<? extends WSItemType> materialClass, Object... extra) {
		this.materialClass = materialClass;
		this.defaultState = BridgeItemType.getDefaultState(numericalId, oldStringId, newStringId, maxStackSize, materialClass, extra);
	}

	WSItemTypes(int numericalId, String oldStringId, String newStringId, Class<? extends WSItemType> materialClass, Object... extra) {
		this.materialClass = materialClass;
		this.defaultState = BridgeItemType.getDefaultState(numericalId, oldStringId, newStringId, 1, materialClass, extra);
	}

	WSItemTypes(int numericalId, String oldStringId, Class<? extends WSItemType> materialClass, Object... extra) {
		this.materialClass = materialClass;
		this.defaultState = BridgeItemType.getDefaultState(numericalId, oldStringId, oldStringId, 1, materialClass, extra);
	}

	WSItemTypes(int numericalId, String oldStringId, int maxStackSize, Class<? extends WSItemType> materialClass, Object... extra) {
		this.materialClass = materialClass;
		this.defaultState = BridgeItemType.getDefaultState(numericalId, oldStringId, oldStringId, maxStackSize, materialClass, extra);
	}

	WSItemTypes(int numericalId, String oldStringId, String newStringId, int maxStackSize) {
		this(numericalId, oldStringId, newStringId, maxStackSize, WSItemType.class);
	}

	WSItemTypes(int numericalId, String oldStringId, String newStringId) {
		this(numericalId, oldStringId, newStringId, 1, WSItemType.class);
	}

	WSItemTypes(int numericalId, String oldStringId) {
		this(numericalId, oldStringId, oldStringId, 1, WSItemType.class);
	}

	WSItemTypes(int numericalId, String oldStringId, int maxStackSize) {
		this(numericalId, oldStringId, oldStringId, maxStackSize, WSItemType.class);
	}


	WSItemTypes(Class<? extends WSItemType> materialClass, Object... extra) {
		this.materialClass = materialClass;
		this.defaultState = BridgeItemType.getDefaultState(-1, null, null, -1, materialClass, extra);
	}

	public int getNumericalId() {
		return defaultState.getNumericalId();
	}

	public String getStringId() {
		return WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13) ? getOldStringId() : getNewStringId();
	}

	public String getOldStringId() {
		return defaultState.getOldStringId();
	}

	public String getNewStringId() {
		return defaultState.getNewStringId();
	}

	public int getMaxStackSize() {
		return defaultState.getMaxStackSize();
	}

	public Class<? extends WSItemType> getMaterialClass() {
		return materialClass;
	}

	public WSItemType getDefaultState() {
		return defaultState.clone();
	}

	@SuppressWarnings("unchecked")
	public <T extends WSItemType> T getDefaultState(Class<T> expected) {
		return (T) defaultState.clone();
	}

	public static Optional<WSItemType> getById(String string) {
		return getItemTypesById(string).map(WSItemTypes::getDefaultState);
	}

	public static Optional<WSItemType> getByOldId(String string) {
		return Arrays.stream(values())
				.filter(target -> string.equalsIgnoreCase(target.getOldStringId())).map(WSItemTypes::getDefaultState).findAny();
	}

	public static Optional<WSItemType> getByNewId(String string) {
		return Arrays.stream(values())
				.filter(target -> string.equalsIgnoreCase(target.getNewStringId())).map(WSItemTypes::getDefaultState).findAny();
	}

	public static Optional<WSItemType> getById(int id) {
		return Arrays.stream(values()).filter(target -> target.getNumericalId() == id).map(WSItemTypes::getDefaultState).findAny();
	}

	public static Optional<WSItemTypes> getItemTypesById(String string) {
		Optional<WSItemTypes> optional = Arrays.stream(values()).filter(target -> WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13) ?
				string.equalsIgnoreCase(target.getOldStringId()) : string.equalsIgnoreCase(target.getNewStringId())).findAny();
		if (!optional.isPresent()) optional = Arrays.stream(values()).filter(target -> !WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13) ?
				string.equalsIgnoreCase(target.getOldStringId()) : string.equalsIgnoreCase(target.getNewStringId())).findAny();
		return optional;
	}

	public static Optional<WSItemTypes> getItemTypesByOldId(String string) {
		return Arrays.stream(values())
				.filter(target -> string.equalsIgnoreCase(target.getOldStringId())).findAny();
	}

	public static Optional<WSItemTypes> getItemTypesByNewId(String string) {
		return Arrays.stream(values())
				.filter(target -> string.equalsIgnoreCase(target.getNewStringId())).findAny();
	}

	public static Optional<WSItemTypes> getItemTypesById(int id) {
		return Arrays.stream(values()).filter(target -> target.getNumericalId() == id).findAny();
	}
}
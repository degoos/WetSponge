package com.degoos.wetsponge.enums;

import java.util.Arrays;
import java.util.Optional;

public enum EnumMapScale {

	CLOSEST(0), CLOSE(1), NORMAL(2), FAR(3), FARTHEST(4);

	private byte id;

	EnumMapScale(int id) {
		this.id = (byte) id;
	}

	public byte getId() {
		return id;
	}

	public static Optional<EnumMapScale> getById(int id) {
		return Arrays.stream(values()).filter(target -> target.id == id).findAny();
	}

}

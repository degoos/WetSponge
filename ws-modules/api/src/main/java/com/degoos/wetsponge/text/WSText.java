package com.degoos.wetsponge.text;


import com.degoos.wetsponge.bridge.text.BridgeText;
import com.degoos.wetsponge.enums.EnumTextColor;
import com.degoos.wetsponge.enums.EnumTextStyle;
import com.degoos.wetsponge.text.action.click.WSClickAction;
import com.degoos.wetsponge.text.action.hover.WSHoverAction;
import com.degoos.wetsponge.text.translation.WSTranslation;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * This class represents a text, with its colors, styles and actions. A text won't be modifiable. You can create a new builder from a text, but it will create a new
 * copy!
 */
public interface WSText {

	/**
	 * Creates a new empty {@link WSText.Builder builder}.
	 *
	 * @return the new {@link WSText.Builder builder}.
	 */
	public static Builder builder() {
		return BridgeText.builder();
	}

	/**
	 * Creates a new {@link WSText.Builder builder} from a {@link String string}.
	 *
	 * @param string the {@link String string}.
	 * @return the new {@link WSText.Builder builder}.
	 */
	public static Builder builder(String string) {
		return BridgeText.builder(string);
	}

	/**
	 * Creates a new {@link WSText.Builder builder} from a {@link WSText text}.
	 *
	 * @param text the {@link WSText text}.
	 * @return the new {@link WSText.Builder builder}.
	 */
	public static Builder builder(WSText text) {
		return BridgeText.builder(text);
	}

	/**
	 * Creates a new {@link WSText text} from a {@link String string}.
	 *
	 * @param string the {@link String string}.
	 * @return a new {@link WSText text}.
	 */
	public static WSText of(String string) {
		return BridgeText.of(string);
	}

	/**
	 * Creates a new {@link WSText text} from a {@link String string} and a {@link EnumTextColor color}.
	 *
	 * @param string the {@link String string}.
	 * @param color  the {@link EnumTextColor color}.
	 * @return a new {@link WSText text}.
	 */
	public static WSText of(String string, EnumTextColor color) {
		return builder(string).color(color).build();
	}

	/**
	 * Creates a new {@link WSText text} from a {@link String string} a {@link EnumTextColor color}, and a {@link EnumTextStyle style}.
	 *
	 * @param string the {@link String string}.
	 * @param color  the {@link EnumTextColor color}.
	 * @param style  the {@link EnumTextStyle style}.
	 * @return a new {@link WSText text}.
	 */
	public static WSText of(String string, EnumTextColor color, EnumTextStyle style) {
		return builder(string).color(color).style(Collections.singleton(style)).build();
	}

	/**
	 * Creates a new {@link WSText text} from a {@link String string} a {@link EnumTextColor color}, and {@link EnumTextStyle style}s.
	 *
	 * @param string the {@link String string}.
	 * @param color  the {@link EnumTextColor color}.
	 * @param styles the {@link EnumTextStyle style}s.
	 * @return a new {@link WSText text}.
	 */
	public static WSText of(String string, EnumTextColor color, Collection<EnumTextStyle> styles) {
		return builder(string).color(color).style(styles).build();
	}

	/**
	 * Creates a new {@link WSText text} from a {@link String string} and appends another {@link WSText text}.
	 *
	 * @param string the {@link String string}.
	 * @param append the {@link WSText text} to append.
	 * @return a new {@link WSText text}.
	 */
	public static WSText of(String string, WSText append) {
		return builder(string).append(append).build();
	}

	/**
	 * Creates a new {@link WSText text} from a {@link String string}, a {@link EnumTextColor color} and appends another {@link WSText text}.
	 *
	 * @param string the {@link String string}.
	 * @param color  the {@link EnumTextColor color}.
	 * @param append the {@link WSText text} to append.
	 * @return a new {@link WSText text}.
	 */
	public static WSText of(String string, EnumTextColor color, WSText append) {
		return builder(string).color(color).append(append).build();
	}

	/**
	 * Creates a new {@link WSText text} from a {@link String string}, a {@link EnumTextColor color}  and {@link EnumTextStyle style}s and appends another {@link WSText
	 * text}.
	 *
	 * @param string the {@link String string}.
	 * @param color  the {@link EnumTextColor color}.
	 * @param append the {@link WSText text} to append.
	 * @param style  the {@link EnumTextStyle style}.
	 * @return a new {@link WSText text}.
	 */
	public static WSText of(String string, EnumTextColor color, WSText append, EnumTextStyle style) {
		return builder(string).color(color).style(Collections.singleton(style)).append(append).build();
	}

	/**
	 * Creates a new {@link WSText text} from a {@link String string}, a {@link EnumTextColor color}  and {@link EnumTextStyle style}s and appends another {@link WSText
	 * text}.
	 *
	 * @param string the {@link String string}.
	 * @param color  the {@link EnumTextColor color}.
	 * @param append the {@link WSText text} to append.
	 * @param styles the {@link EnumTextStyle style}s.
	 * @return a new {@link WSText text}.
	 */
	public static WSText of(String string, EnumTextColor color, WSText append, Collection<EnumTextStyle> styles) {
		return builder(string).color(color).style(styles).append(append).build();
	}

	public static WSTranslatableText of(WSTranslation translation, Object... objects) {
		return BridgeText.of(translation, objects);
	}

	/**
	 * Creates an empty {@link WSText text}.
	 *
	 * @return the {@link WSText text}.
	 */
	public static WSText empty() {
		return WSText.of("");
	}

	/**
	 * Creates a new {@link WSText text} from a formatted text. <p>Formatted text are used by Minecraft to add colors and styles to its texts.</p> <p>Example:
	 * \u00A74Hi!</p>
	 *
	 * @param text the formatted text.
	 * @return a new {@link WSText text}.
	 */
	public static WSText getByFormattingText(String text) {
		return BridgeText.getByFormattingText(text);
	}

	/**
	 * Returns the {@link String string value} of the {@link WSText text}. <p>This method doesn't combine the text of the children. Just returns the text of the
	 * parent.</p>
	 *
	 * @return the {@link String string value} of the {@link WSText text}.
	 */
	String getText();

	/**
	 * @return the {@link EnumTextColor color} of the {@link WSText text}, if present.
	 */
	Optional<EnumTextColor> getColor();

	/**
	 * @return all {@link EnumTextStyle style}s of the {@link WSText text}.
	 */
	Collection<EnumTextStyle> getStyles();

	/**
	 * @return all children of the {@link WSText text}.
	 */
	Collection<? extends WSText> getChildren();

	/**
	 * @return the {@link WSClickAction click action} of the {@link WSText text}, if present.
	 * @see WSClickAction
	 */
	Optional<WSClickAction> getClickAction();

	/**
	 * @return the {@link WSHoverAction hover action} of the {@link WSText text}, if present.
	 * @see WSHoverAction
	 */
	Optional<WSHoverAction> getHoverAction();


	/**
	 * Returns whether the {@link WSText text} or one of its children contains the given {@link String string}.
	 *
	 * @param sequence the {@link String string}.
	 * @return whether the {@link WSText text} or one of its children contains the given {@link String string}.
	 */
	boolean contains(String sequence);

	/**
	 * Splits the {@link WSText text} around matches of the given {@link String regular expression}. <p>All new texts will have the same properties of their parents</p>
	 * <p>This method moves all children to the same inheritance level, like {@link #moveChildrenToOneList()} does.</p>
	 *
	 * @param regex the {@link String regular expression}.
	 * @return the {@link List list}.
	 */
	List<WSText> split(String regex);

	/**
	 * Creates a clone of the {@link WSText text} with all its children in just one inheritance level.
	 * <p>
	 * Example: The text Text (Child-1, Child-2 (Child-2.1, Child-2.2), Child-3 (Child-3.1)) will be converted to Text (Child-1, Child-2, Child-2.1, Child-2.2, Child-3,
	 * Child-3.1).
	 *
	 * @return the clone.
	 */
	WSText moveChildrenToOneList();

	/**
	 * Creates a clone of the {@link WSText text}, replacing the given string to a replacement in the {@link WSText text} and in all of its children.
	 *
	 * @param toReplace   the {@link String text} to replace.
	 * @param replacement the {@link String replacement}.
	 * @return the clone.
	 */
	WSText replace(String toReplace, String replacement);

	/**
	 * Creates a clone of the {@link WSText text}, replacing the given string to a replacement in the {@link WSText text} and in all of its children.
	 *
	 * @param toReplace   the {@link String text} to replace.
	 * @param replacement the {@link WSText replacement}.
	 * @return the clone.
	 */
	WSText replace(String toReplace, WSText replacement);

	/**
	 * Transforms the {@link WSText text} into a formatted text. <p>Formatted text are used by Minecraft to add colors and styles to its texts.</p> <p>Example:
	 * \u00A74Hi!</p>
	 *
	 * @return the formatted text.
	 */
	String toFormattingText();

	/**
	 * Returns a plain text without any format of the {@link WSText text} and all its children.
	 *
	 * @return a plain text.
	 */
	String toPlain();

	/**
	 * Clones the {@link WSText text}.
	 *
	 * @return the cloned {@link WSText text}.
	 */
	WSText clone();

	/**
	 * Creates a new {@link WSText text} with all formats of the {@link WSText text}.
	 *
	 * @param string the new text body.
	 * @return a new {@link WSText text}.
	 */
	WSText clone(String string);

	/**
	 * Clones the {@link WSText text}, but without children.
	 *
	 * @return the cloned {@link WSText text}.
	 */
	WSText cloneSingle();

	/**
	 * Creates a new {@link WSText text} with all formats of the {@link WSText text} but without children.
	 *
	 * @param string the new text body.
	 * @return a new {@link WSText text}.
	 */
	WSText cloneSingle(String string);

	/**
	 * @return the handled object of the {@link WSText text}.
	 */
	Object getHandled();

	/**
	 * Transforms the {@link WSText text} into a {@link Builder builder}. <p>The builder will not change anything of the target {@link WSText text}! It will create a new
	 * copy!</p>
	 *
	 * @return the {@link Builder builder}.
	 */
	default Builder toBuilder() {
		return builder(this);
	}

	interface Builder {

		/**
		 * Transforms this {@link Builder builder} into a {@link WSText text}.
		 *
		 * @return the new {@link WSText text}.
		 */
		WSText build();

		/**
		 * Sets the color of the text to build.
		 *
		 * @param color the color.
		 * @return the {@link Builder builder}.
		 */
		Builder color(EnumTextColor color);

		/**
		 * Sets the style of the text to build.
		 *
		 * @param style the style.
		 * @return the {@link Builder builder}.
		 */
		Builder style(EnumTextStyle style);

		/**
		 * Sets the styles of the text to build.
		 *
		 * @param styles the styles.
		 * @return the {@link Builder builder}.
		 */
		Builder style(Collection<EnumTextStyle> styles);

		/**
		 * Sets the click action of the text to build.
		 *
		 * @param action the action.
		 * @return the {@link Builder builder}.
		 */
		Builder clickAction(WSClickAction action);

		/**
		 * Sets the hover action of the text to build.
		 *
		 * @param action the action.
		 * @return the {@link Builder builder}.
		 */
		Builder hoverAction(WSHoverAction action);

		/**
		 * Appends a {@link WSText text}.
		 *
		 * @param children the {@link WSText text}.
		 * @return the {@link Builder builder}.
		 */
		Builder append(WSText... children);

		/**
		 * Appends a collection of {@link WSText text}.
		 *
		 * @param children the {@link WSText text}s.
		 * @return the {@link Builder builder}.
		 */
		Builder append(Collection<? extends WSText> children);

		/**
		 * Appends a {@link WSText text} which represents a new line. Similar to "\\n".
		 *
		 * @return the {@link Builder builder}.
		 */
		Builder newLine();

		/**
		 * Centers the message. WARNING! If you use this, all actions set before use this method will be lost!
		 *
		 * @return the {@link Builder builder}.
		 */
		Builder center();

		/**
		 * Removes all the colors of the text to build.
		 *
		 * @return the {@link Builder builder}
		 */
		Builder stripColors();

		/**
		 * Translates all the <em>Ampersand</em> symbols into colors and styles. WARNING! If you use this, all actions set before use this method will be lost!
		 *
		 * @return the {@link Builder builder}.
		 */
		Builder translateColors();

	}

}

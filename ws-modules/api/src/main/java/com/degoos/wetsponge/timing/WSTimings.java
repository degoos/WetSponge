package com.degoos.wetsponge.timing;

import co.aikar.wetspongeutils.JSONUtil;
import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.command.WSCommandSource;
import com.degoos.wetsponge.config.WetSpongeConfig;
import com.degoos.wetsponge.config.WetSpongeMessages;
import com.degoos.wetsponge.exception.timing.TimingsNotEnabledException;
import com.degoos.wetsponge.plugin.WSPlugin;
import com.degoos.wetsponge.task.WSTask;
import com.degoos.wetsponge.util.Validate;
import org.jooq.tools.json.JSONValue;

import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class WSTimings {

	private UploadThread uploadThread;

	private Map<Long, WSTimingsThread> threads;
	private boolean enabled;
	private long timingStart;


	public WSTimings() {
		threads = new ConcurrentHashMap<>();
		timingStart = -1;
		uploadThread = new UploadThread();
		uploadThread.start();
		setEnabled(WetSpongeConfig.getConfig().getBoolean("startTimingsOnStart"));
	}

	public WSTimingsThread getThread(Thread thread) {
		if (!enabled) throw new TimingsNotEnabledException("Timings are not enabled!");
		if (threads.containsKey(thread.getId())) return threads.get(thread.getId());

		WSTimingsThread timingsThread = new WSTimingsThread(thread);
		threads.put(thread.getId(), timingsThread);
		return timingsThread;
	}

	public WSPlugin getAssignedPlugin() {
		return getAssignedPlugin(Thread.currentThread());
	}

	public WSPlugin getAssignedPlugin(Thread thread) {
		if (!enabled) return null;
		if (threads.containsKey(thread.getId())) return threads.get(thread.getId()).getAssignedPlugin();
		return null;
	}

	public void assignPluginToThread(Thread thread, WSPlugin plugin) {
		if (!enabled) return;
		Validate.notNull(thread, "Thread cannot be null!");
		getThread(thread).assignPlugin(plugin);
	}

	public void assignPluginToThread(WSPlugin plugin) {
		if (!enabled) return;
		getThread(Thread.currentThread()).assignPlugin(plugin);
	}

	public void startTiming(String name) {
		if (!enabled) return;
		getThread(Thread.currentThread()).startTiming(name, Thread.currentThread().getStackTrace()[2]);
	}

	public void startTiming(WSTask task) {
		if (!enabled) return;
		getThread(Thread.currentThread()).startTiming(task);
	}

	public void stopTiming() {
		if (!enabled) return;
		getThread(Thread.currentThread()).stopTiming();
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		if (this.enabled == enabled) return;
		this.enabled = enabled;
		if (enabled) {
			timingStart = System.currentTimeMillis();
		} else {
			timingStart = -1;
			threads.clear();
		}
	}

	public void reset() {
		timingStart = System.currentTimeMillis();
		threads.clear();
	}

	public void report(WSCommandSource source) {
		WetSpongeMessages.getMessage("command.timings.creating").ifPresent(source::sendMessage);
		Map parent = JSONUtil.createObject(
				JSONUtil.pair("version", getServerVersion()),
				JSONUtil.pair("max_players", WetSponge.getServer().getServerInfo().getMaxPlayers()),
				JSONUtil.pair("start", timingStart / 1000),
				JSONUtil.pair("end", System.currentTimeMillis() / 1000),
				JSONUtil.pair("sample_time", (System.currentTimeMillis() - timingStart) / 1000),
				JSONUtil.pair("server", WetSponge.getServer().getServerInfo().getServerName()),
				JSONUtil.pair("motd", WetSponge.getServer().getServerInfo().getMotd().getText()),
				JSONUtil.pair("online_mode", WetSponge.getServer().getServerInfo().isOnlineMode()),
				JSONUtil.pair("icon", WetSponge.getServer().getServerInfo().getBase64ServerIcon()));

		Runtime runtime = Runtime.getRuntime();
		RuntimeMXBean runtimeBean = ManagementFactory.getRuntimeMXBean();

		parent.put("system", JSONUtil.createObject(
				JSONUtil.pair("name", System.getProperty("os.name")),
				JSONUtil.pair("version", System.getProperty("os.version")),
				JSONUtil.pair("jvm_version", System.getProperty("java.version")),
				JSONUtil.pair("arch", System.getProperty("os.arch")),
				JSONUtil.pair("max_memory", runtime.maxMemory()),
				JSONUtil.pair("cpu", runtime.availableProcessors()),
				JSONUtil.pair("runtime", ManagementFactory.getRuntimeMXBean().getUptime()),
				JSONUtil.pair("flags", String.join(" ", runtimeBean.getInputArguments())),
				JSONUtil.pair("gc", JSONUtil.toArrayMapper(ManagementFactory.getGarbageCollectorMXBeans(),
						input -> JSONUtil.createObject(JSONUtil.pair("name", input.getName()),
								JSONUtil.pair("collection_count", input.getCollectionCount()),
								JSONUtil.pair("collection_time", input.getCollectionTime()))))));

		parent.put("plugins", JSONUtil.toArrayMapper(WetSponge.getPluginManager().getPlugins(), plugin -> JSONUtil
				.createObject(
						JSONUtil.pair("name", plugin.getId()),
						JSONUtil.pair("version", plugin.getPluginDescription().getVersion()),
						JSONUtil.pair("description", plugin.getPluginDescription().getDescription()),
						JSONUtil.pair("website", plugin.getPluginDescription().getWebsite()),
						JSONUtil.pair("authors", plugin.getPluginDescription().getAuthors()))));


		parent.put("base_plugins", JSONUtil.toArrayMapper(WetSponge.getPluginManager().getBasePlugins(), plugin ->
				JSONUtil.createObject(
						JSONUtil.pair("name", plugin.getName()),
						JSONUtil.pair("version", plugin.getVersion()),
						JSONUtil.pair("description", plugin.getDescription()),
						JSONUtil.pair("website", plugin.getUrl()),
						JSONUtil.pair("authors", plugin.getAuthors()))));

		List timings = JSONUtil.toArray();

		WSTimingsThread asyncSchedulerThread = null;

		for (WSTimingsThread thread : threads.values()) {

			if (thread.getThreadName().startsWith("Craft Scheduler Thread")) {
				if (asyncSchedulerThread == null) asyncSchedulerThread = new WSTimingsThread(thread.getThreadId(), "Craft Scheduler Thread");
				asyncSchedulerThread.merge(thread);
				continue;
			}
			Map map = getThreadMap(thread);
			if (map != null) timings.add(map);
		}

		if (asyncSchedulerThread != null) {
			Map map = getThreadMap(asyncSchedulerThread);
			if (map != null) timings.add(map);
		}

		parent.put("timings", timings);

		String json = JSONValue.toJSONString(parent);

		uploadThread.addRequest(new UploadThread.UploadRequest(source, json));

	}

	private Map getThreadMap(WSTimingsThread thread) {
		if (thread.getAllPluginTimings().isEmpty()) return null;

		Map threadMap = JSONUtil.createObject();
		threadMap.put("name", thread.getThreadName());

		List recorders = JSONUtil.toArray();

		for (Map.Entry<WSPlugin, WSTimingRecorder> recorder : thread.getAllPluginTimings().entrySet()) {
			if (recorder.getValue().getTimings().isEmpty()) continue;
			recorders.add(recorder.getValue().export());
		}
		if (thread.getWetSpongeTiming() != null) recorders.add(thread.getWetSpongeTiming().export());
		threadMap.put("recorders", recorders);
		return threadMap;
	}

	private String getServerVersion() {
		return "WetSponge" + " for " + WetSponge.getServerType() + " (" + WetSponge.getVersion() + ")";
	}
}

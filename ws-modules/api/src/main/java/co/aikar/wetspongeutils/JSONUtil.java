package co.aikar.wetspongeutils;


import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Provides Utility methods that assist with generating JSON Objects
 */
@SuppressWarnings({"rawtypes", "SuppressionAnnotation"})
public final class JSONUtil {

	private JSONUtil() {}


	/**
	 * Creates a key/value "JSONPair" object
	 *
	 * @param key Key to use
	 * @param obj Value to use
	 *
	 * @return JSONPair
	 */
	public static JSONPair pair(String key, Object obj) {
		return new JSONPair(key, obj);
	}


	public static JSONPair pair(long key, Object obj) {
		return new JSONPair(String.valueOf(key), obj);
	}


	/**
	 * Creates a new JSON object from multiple JSONPair key/value pairs
	 *
	 * @param data JSONPairs
	 *
	 * @return Map
	 */
	public static Map createObject(JSONPair... data) {
		return appendObjectData(new LinkedHashMap(), data);
	}


	/**
	 * This appends multiple key/value Obj pairs into a JSON Object
	 *
	 * @param parent Map to be appended to
	 * @param data Data to append
	 *
	 * @return Map
	 */
	public static Map appendObjectData(Map parent, JSONPair... data) {
		for (JSONPair JSONPair : data) {
			parent.put(JSONPair.key, JSONPair.val);
		}
		return parent;
	}


	/**
	 * This builds a JSON array from a set of data
	 *
	 * @param data Data to build JSON array from
	 *
	 * @return List
	 */
	public static List toArray(Object... data) {
		return Lists.newArrayList(data);
	}


	/**
	 * These help build a single JSON array using a mapper function
	 *
	 * @param collection Collection to apply to
	 * @param mapper Mapper to apply
	 * @param <E> Element Type
	 *
	 * @return List
	 */
	public static <E> List toArrayMapper(E[] collection, Function<E, Object> mapper) {
		return toArrayMapper(Lists.newArrayList(collection), mapper);
	}


	public static <E> List toArrayMapper(Iterable<E> collection, Function<E, Object> mapper) {
		List array = Lists.newArrayList();
		for (E e : collection) {
			Object object = mapper.apply(e);
			if (object != null) {
				array.add(object);
			}
		}
		return array;
	}


	/**
	 * These help build a single JSON Object from a collection, using a mapper function
	 *
	 * @param collection Collection to apply to
	 * @param mapper Mapper to apply
	 * @param <E> Element Type
	 *
	 * @return Map
	 */
	public static <E> Map toObjectMapper(E[] collection, Function<E, JSONPair> mapper) {
		return toObjectMapper(Lists.newArrayList(collection), mapper);
	}


	public static <E> Map toObjectMapper(Iterable<E> collection, Function<E, JSONPair> mapper) {
		Map object = Maps.newLinkedHashMap();
		for (E e : collection) {
			JSONPair JSONPair = mapper.apply(e);
			if (JSONPair != null) {
				object.put(JSONPair.key, JSONPair.val);
			}
		}
		return object;
	}


	/**
	 * Simply stores a key and a value, used internally by many methods below.
	 */
	@SuppressWarnings("PublicInnerClass")
	public static class JSONPair {

		final String key;
		final Object val;


		JSONPair(String key, Object val) {
			this.key = key;
			this.val = val;
		}
	}

	public static class JsonObjectBuilder {

		private final Map<String, Object> elements = Maps.newHashMap();

		public JsonObjectBuilder add(int key, Object value) {
			return add(String.valueOf(key), value);
		}

		public JsonObjectBuilder add(String key, Object value) {
			if (value instanceof JsonObjectBuilder) {
				value = ((JsonObjectBuilder) value).build();
			}
			this.elements.put(key, value);
			return this;
		}

		public JsonObject build() {
			return new GsonBuilder().serializeNulls().create().toJsonTree(this.elements).getAsJsonObject();
		}
	}

	public static JsonObjectBuilder objectBuilder() {
		return new JsonObjectBuilder();
	}

	public static <E> JsonArray mapArray(E[] elements, java.util.function.Function<E, Object> function) {
		return mapArray(Lists.newArrayList(elements), function);
	}

	public static <E> JsonArray mapArray(Iterable<E> elements, java.util.function.Function<E, Object> function) {
		List<Object> list = Lists.newArrayList();
		for (E element : elements) {
			Object transformed = function.apply(element);
			if (transformed != null) {
				list.add(transformed);
			}
		}
		return new GsonBuilder().serializeNulls().create().toJsonTree(list).getAsJsonArray();
	}

	public static <E> JsonObject mapArrayToObject(E[] array, java.util.function.Function<E, JsonObject> function) {
		return mapArrayToObject(Lists.newArrayList(array), function);
	}

	public static <E> JsonObject mapArrayToObject(Iterable<E> iterable, java.util.function.Function<E, JsonObject> function) {
		JsonObjectBuilder builder = objectBuilder();
		for (E element : iterable) {
			JsonObject obj = function.apply(element);
			if (obj == null) {
				continue;
			}
			for (Entry<String, JsonElement> entry : obj.entrySet()) {
				builder.add(entry.getKey(), entry.getValue());
			}
		}
		return builder.build();
	}

	public static String toString(JsonElement element) {
		return new GsonBuilder().serializeNulls().create().toJson(element);
	}

	public static JsonElement toJsonElement(Object value) {
		return new GsonBuilder().serializeNulls().create().toJsonTree(value);
	}


	public static JsonObject singleObjectPair(String key, Object value) {
		return objectBuilder().add(key, value).build();
	}

	public static JsonObject singleObjectPair(int key, Object value) {
		return objectBuilder().add(key, value).build();
	}

	public static JsonArray arrayOf(Object... elements) {
		return new GsonBuilder().serializeNulls().create().toJsonTree(elements).getAsJsonArray();
	}
}
